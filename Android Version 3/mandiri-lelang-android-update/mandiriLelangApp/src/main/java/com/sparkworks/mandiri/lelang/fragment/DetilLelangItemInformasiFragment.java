package com.sparkworks.mandiri.lelang.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mandiri.lelang.aplikasi.ContactUsActivity;
import com.mandiri.lelang.aplikasi.DetilItemImageActivity;
import com.mandiri.lelang.aplikasi.MainActivity;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.base.BaseFragment;
import com.sparkworks.mandiri.lelang.model.AssetInquiry;
import com.sparkworks.mandiri.lelang.event.FavoriteEvent;
import com.sparkworks.mandiri.lelang.event.SetFavoriteUnfavoriteEvent;
import com.sparkworks.mandiri.lelang.event.UnfavoriteEvent;
import com.sparkworks.mandiri.lelang.model.Item;
import com.sparkworks.mandiri.lelang.utils.Constant;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Locale;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

public class DetilLelangItemInformasiFragment extends BaseFragment{
	
	public static String FRAGMENT_TAG = "DetilLelangItemInformasiFragment";
	public Item item;
	private TextView txtTitle, txtLimitLelang, txtHargaSebelumnya, tvEauction,
	txtLokasi, txtDokumen, txtHotPrice, txtEauctionIndicator, txtJualSukarelaIndicator,
	txtLelangIndicator;
	private LinearLayout lnExtraFields, lnMap;
	private ImageView imgDetilItem, imgFavorite;
	private Button btnBerminat;
	private MapView mapView;
	static LatLng VENUE = null;
	private GoogleMap map;
	
	private String KEY_DETIL_INFO = "DetilItem";
	
	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		EventBus.getDefault().register(this);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_detil_lelang_informasi, container,
				false);
		initializeViews(view);
		mapView = (MapView)view.findViewById(R.id.mapView);
		
		return view;
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		if (savedInstanceState != null) {
			setItem((Item)savedInstanceState.getSerializable(KEY_DETIL_INFO));
		}
		
		mapView.onCreate(savedInstanceState);
		mapView.getMapAsync(new OnMapReadyCallback() {
			@Override
			public void onMapReady(GoogleMap googleMap) {
				map = googleMap;

				map.getUiSettings().setMyLocationButtonEnabled(false);
				map.getUiSettings().setAllGesturesEnabled(false);

				initializeProcess();
				initializeActions();
			}
		});

	}
	
	@Override
	public void initializeViews(View view) {
		// TODO Auto-generated method stub
		super.initializeViews(view);
		txtDokumen = (TextView)view.findViewById(R.id.txtDetilItemDokumen);
		txtHargaSebelumnya = (TextView)view.findViewById(R.id.txtDetilItemHargaSebelumnya);
		txtLimitLelang = (TextView)view.findViewById(R.id.txtDetilItemLimitLelang);
		txtLokasi = (TextView)view.findViewById(R.id.txtDetilItemLokasi);
		txtTitle = (TextView)view.findViewById(R.id.txtDetilItemTitle);
		txtHotPrice = (TextView)view.findViewById(R.id.txtDetilLelangHotPriceIndicator);
		imgDetilItem = (ImageView)view.findViewById(R.id.imgDetilItem);
		lnExtraFields = (LinearLayout)view.findViewById(R.id.lnDetilItemExtraField);
		lnMap = (LinearLayout)view.findViewById(R.id.lnDetilItemPeta);
		tvEauction = (TextView)view.findViewById(R.id.tv_detil_eauction_link);
		imgFavorite = (ImageView)view.findViewById(R.id.img_favorite);
		txtEauctionIndicator = (TextView)view.findViewById(R.id.txtDetilLelangEAuctionIndicator);
		btnBerminat = (Button)view.findViewById(R.id.btnBerminat);
		txtJualSukarelaIndicator = (TextView)view.findViewById(R.id.txtJualSukarelaIndicator);
		txtLelangIndicator = (TextView)view.findViewById(R.id.txtLelangIndicator);
	}
	
	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		super.initializeProcess();

		if (getItem().isEmailAuction()){
			tvEauction.setText(Html.fromHtml("<u>Link e-Auction</u>"));
			Linkify.addLinks(tvEauction, Linkify.WEB_URLS);
		}else{
			tvEauction.setVisibility(View.GONE);
			txtEauctionIndicator.setVisibility(View.GONE);
		}

		imgFavorite.setColorFilter(ContextCompat.getColor(getActivity(), R.color.mandiri_grey));
		if (getItem().isFavorite()){
			imgFavorite.setColorFilter(ContextCompat.getColor(getActivity(), R.color.mandiri_yellow));
		}

		txtDokumen.setText(getItem().getDocumentTypeName());
		
		if (!getItem().getOldPrice().equals("")) {
			txtHargaSebelumnya.setVisibility(View.VISIBLE);
			txtHargaSebelumnya.setText(getItem().getOldPrice());
			txtHargaSebelumnya.setPaintFlags(txtHargaSebelumnya.getPaintFlags()|
					Paint.STRIKE_THRU_TEXT_FLAG);
		}else{
			txtHargaSebelumnya.setVisibility(View.GONE);
		}
		
		if (getItem().getNewPrice().toLowerCase(Locale.ENGLISH).equals(Constant.CALL)) {
			txtLimitLelang.setTextColor(Color.RED);
			txtLimitLelang.setTypeface(Typeface.DEFAULT_BOLD);
		}else{
			txtLimitLelang.setTextColor(Color.BLACK);
		}
		
		txtLimitLelang.setText(getItem().getNewPrice());
		txtLokasi.setText(getItem().getCompleteAddress());
		txtTitle.setText("Kode Agunan : "+getItem().getCode());
		
		if (getItem().getListExtraFields()!=null) {
			for (int i = 0; i < getItem().getListExtraFields().size(); i++) {
				View extraView = getActivity().getLayoutInflater().inflate(R.layout.item_extra_field, null);
				TextView txtExtItemTitle = (TextView)extraView.findViewById(R.id.txtItemDetilLelangTitle);
				TextView txtExtItemValue = (TextView)extraView.findViewById(R.id.txtItemDetilLelangValue);
				
				txtExtItemTitle.setText(getItem().getListExtraFields().get(i).getFieldName());
				txtExtItemValue.setText(getItem().getListExtraFields().get(i).getValue());
				
				lnExtraFields.addView(extraView);
			}
		}
		
		trustEveryone();
		
		//Picasso.with(getActivity()).load(getItem().getPhoto1Url()).into(imgDetilItem);

		Picasso picasso = new Picasso.Builder(getActivity())
				.listener(new Picasso.Listener() {
					@Override
					public void onImageLoadFailed(Picasso picasso, Uri uri, Exception e) {
						Log.d("LoadDetilImage", "Error");
					}
				}).build();
		picasso.load(getItem().getPhoto1Url()).fit().into(imgDetilItem);

		if (getItem().getLatitude().equals("")||getItem().getLongitude().equals("")) {
			lnMap.setVisibility(View.GONE);
		} else {
			lnMap.setVisibility(View.VISIBLE);
			VENUE = new LatLng(
					Double.parseDouble(getItem().getLatitude()),
					Double.parseDouble(getItem().getLongitude()));
			
			MapsInitializer.initialize(this.getActivity());

			
			Marker marker = map.addMarker(new MarkerOptions()
	        .position(VENUE)
	        .title(getItem().getAddress()));

		    // Move the camera instantly to hamburg with a zoom of 15.
		    map.moveCamera(CameraUpdateFactory.newLatLngZoom(VENUE, 9));
		
		    // Zoom in, animating the camera.
		    map.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
		}
	    
	    if (getItem().isHotPrice()) {
			txtHotPrice.setVisibility(View.VISIBLE);
		} else {
			txtHotPrice.setVisibility(View.GONE);
		}

		if (getItem().getTypeAgunan() == 1){
			txtJualSukarelaIndicator.setVisibility(View.VISIBLE);
		}else{
			txtLelangIndicator.setVisibility(View.VISIBLE);
		}
	}
	
	@Override
	public void initializeActions() {
		// TODO Auto-generated method stub
		super.initializeActions();

		imgDetilItem.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				DetilItemImageActivity.toDetilItemImageActivity(getActivity(), getItem().getListImages());
			}
		});
		
		map.setOnMapClickListener(new OnMapClickListener() {
			
			@Override
			public void onMapClick(LatLng arg0) {
				// TODO Auto-generated method stub
				openNativeGmaps();
			}
		});

		imgFavorite.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (getItem().isFavorite()){
					showRemoveFavoriteAlertDialog();
				}else{
					SetFavoriteUnfavoriteEvent e = new SetFavoriteUnfavoriteEvent();
					EventBus.getDefault().post(e);
				}
			}
		});

		txtEauctionIndicator.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getItem().getEmailAuctionUrl()));
				startActivity(intent);
			}
		});

		btnBerminat.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				ContactUsActivity.start(getActivity(), new AssetInquiry(getItem().getAssetId(),
						getItem().getBranch().getName()));
			}
		});
	}
	
	protected void openNativeGmaps() {
		// TODO Auto-generated method stub
		String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f", VENUE.latitude, VENUE.longitude);
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
		intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
		getActivity().startActivity(intent);
	}

	private void showRemoveFavoriteAlertDialog(){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
		alertDialogBuilder.setTitle("Konfirmasi");
		alertDialogBuilder
				.setMessage("Apakah anda yakin ingin menghapus item ini dari favorite?")
				.setCancelable(true)
				.setPositiveButton("Ya",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						dialog.cancel();
						SetFavoriteUnfavoriteEvent e = new SetFavoriteUnfavoriteEvent();
						EventBus.getDefault().post(e);

					}
				})
				.setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}


	@Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
		EventBus.getDefault().unregister(this);
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
    	// TODO Auto-generated method stub
    	outState.putSerializable(KEY_DETIL_INFO, getItem());
    	super.onSaveInstanceState(outState);
    }
    
    private void trustEveryone() {
		try {
			HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
	    			public boolean verify(String hostname, SSLSession session) {
	    				return true;
	    			}});
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, new X509TrustManager[]{new X509TrustManager(){
				public void checkClientTrusted(X509Certificate[] chain,
						String authType) throws CertificateException {}
				public void checkServerTrusted(X509Certificate[] chain,
						String authType) throws CertificateException {}
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[0];
				}}}, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(
					context.getSocketFactory());
		} catch (Exception e) { // should never happen
			e.printStackTrace();
		}
	}

	@Subscribe
	public void onEvent(FavoriteEvent favoriteEvent){
		if (favoriteEvent.getAssetId() == Integer.parseInt(getItem().getAssetId())){
			imgFavorite.setColorFilter(ContextCompat.getColor(getActivity(), R.color.mandiri_yellow));
		}
	}

	@Subscribe
	public void onEvent(UnfavoriteEvent unfavoriteEvent){
		if (unfavoriteEvent.getAssetId() == Integer.parseInt(getItem().getAssetId())){
			imgFavorite.setColorFilter(ContextCompat.getColor(getActivity(), R.color.mandiri_grey));
		}
	}
}
