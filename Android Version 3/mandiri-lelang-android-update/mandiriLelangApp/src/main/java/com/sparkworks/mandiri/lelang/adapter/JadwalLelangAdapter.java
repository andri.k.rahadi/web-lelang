package com.sparkworks.mandiri.lelang.adapter;

import java.util.LinkedList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.model.AuctionSchedule;
import com.sparkworks.mandiri.lelang.utils.Utils;

public class JadwalLelangAdapter extends BaseAdapter{
	
	Activity activity;
	LinkedList<AuctionSchedule> listItem;
	
	public JadwalLelangAdapter(Activity activity) {
		// TODO Auto-generated constructor stub
		this.activity = activity;
	}

	public LinkedList<AuctionSchedule> getListItem() {
		return listItem;
	}

	public void setListItem(LinkedList<AuctionSchedule> listItem) {
		this.listItem = listItem;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return getListItem().size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return getListItem().get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		View view = arg1;
		ViewHolder holder = null;
		
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			holder = new ViewHolder();
			view = inflater.inflate(R.layout.inf_item_jadwal_lelang, null);
			holder.txtAlamat = (TextView)view.findViewById(R.id.txt_inf_jadwal_kpknl_alamat);
			holder.txtName = (TextView)view.findViewById(R.id.txt_inf_jadwal_kpknl_name);
			holder.txtTanggal = (TextView)view.findViewById(R.id.txt_inf_jadwal_tanggal);
			holder.txtPhone1 = (TextView)view.findViewById(R.id.txt_inf_jadwal_kpknl_telp_1);
			holder.txtPhone2 = (TextView)view.findViewById(R.id.txt_inf_jadwal_kpknl_telp_2);
			view.setTag(holder);
		} else {
			holder = (ViewHolder)view.getTag();
		}
		
		holder.txtTanggal.setText(getListItem().get(arg0).getAuctionDateFormatted());
		holder.txtName.setText(getListItem().get(arg0).getKpknl().getName());
		holder.txtAlamat.setText(Utils.replaceNull(getListItem().get(arg0).getKpknl().getAddress()));
		holder.txtPhone1.setText(Utils.replaceNull(getListItem().get(arg0).getKpknl().getPhone1()));
		holder.txtPhone2.setText(Utils.replaceNull(getListItem().get(arg0).getKpknl().getPhone2()));
		
		return view;
	}
	
	static class ViewHolder{
		TextView txtTanggal, txtName, txtAlamat, txtPhone1, txtPhone2;
	}

}
