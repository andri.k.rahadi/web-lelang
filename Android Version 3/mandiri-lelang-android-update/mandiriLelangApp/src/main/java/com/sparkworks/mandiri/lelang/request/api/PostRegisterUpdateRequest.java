package com.sparkworks.mandiri.lelang.request.api;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.database.helper.MinMaxPriceHelper;
import com.sparkworks.mandiri.lelang.database.helper.SystemSettingHelper;
import com.sparkworks.mandiri.lelang.request.model.MemberParam;
import com.sparkworks.mandiri.lelang.request.model.Preference;
import com.sparkworks.mandiri.lelang.request.model.Profile;
import com.sparkworks.mandiri.lelang.request.response.ProfileResponse;
import com.sparkworks.mandiri.lelang.request.response.RegisterUpdateResponse;
import com.sparkworks.mandiri.lelang.utils.URLs;
import com.sparkworks.mandiri.lelang.utils.Utils;

import org.apache.http.Header;
import org.apache.http.client.CookieStore;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

/**
 * Created by sidiqpermana on 11/29/16.
 */

public class PostRegisterUpdateRequest extends BaseApi implements FormValidation{
    private MemberParam memberParam;
    public static final int POST_REGISTER = 1;
    public static final int POST_UPDATE = 2;
    private int postType;

    public static final String ERROR_FULLNAME = "error_fullname";
    public static final String ERROR_EMAIL = "error_email";
    public static final String ERROR_PLACE_OF_BIRTH = "error_place_of_birth";
    public static final String ERROR_DATE_OF_BIRTH = "error_date_of_birth";
    public static final String ERROR_MOBILE_PHONE_NUMBER = "error_mobile_phone_number";
    public static final String ERROR_ADDRESS = "error_address";
    public static final String ERROR_PROVINCE_ID = "error_province_id";
    public static final String ERROR_CITY_ID = "error_city_id";
    public static final String ERROR_IDENTIFICATION_NUMBER = "error_identification_number";
    public static final String ERROR_PASSWORD = "error_password";
    public static final String ERROR_CONFIRM_PASSWORD = "error_confirm_password";
    public static final String ERROR_PASSWORD_NOT_MATCH = "error_password_not_match";
    public static final String ERROR_PASSWORD_NOT_VALID = "error_password_not_valid";
    public static final String ERROR_MIN_PRICE_ID = "error_min_price_id";
    public static final String ERROR_MAX_PRICE_ID = "error_max_price_id";
    public static final String ERROR_CATEGORY_IDS = "error_category_ids";
    public static final String ERROR_CITY_IDS = "error_city_ids";
    public static final String ERROR_OLD_PASSWORD = "error_old_password";
    public static final String ERROR_MIN_PRICE_OVER_MAX_PRICE = "error_min_price_over_max_price";
    public static final String ERROR_INVALID_DATE_OF_BIRTH = "error_invalid_date_of_birth";

    public static final String PARAM_FULLNAME = "fullname";
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_PLACE_OF_BIRTH = "place_of_birth";
    public static final String PARAM_PLACE_OF_BIRTH_NOT_VALID = "place_of_birth_not_valid";
    public static final String PARAM_DATE_OF_BIRTH = "date_of_birth";
    public static final String PARAM_MOBILE_PHONE_NUMBER = "mobile_phone_number";
    public static final String PARAM_ADDRESS = "address";
    public static final String PARAM_PROVINCE_ID = "province_id";
    public static final String PARAM_CITY_ID = "city_id";
    public static final String PARAM_IDENTIFICATION_NUMBER = "identification_number";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_CONFIRM_PASSWORD = "confirm_password";
    public static final String PARAM_PASSWORD_NOT_MATCH = "password_not_match";
    public static final String PARAM_PASSWORD_NOT_VALID = "password_not_valid";
    public static final String PARAM_OLD_PASSWORD = "old_password_empty";
    public static final String PARAM_INVALID_DATE_OF_BIRTH = "invalid_date_of_birth";

    public static final String PARAM_MIN_PRICE_ID = "min_price_id";
    public static final String PARAM_MAX_PRICE_ID = "max_price_id";
    public static final String PARAM_CATEGORY_IDS = "category_ids";
    public static final String PARAM_CITY_IDS = "city_ids";
    public static final String PARAM_MIN_PRICE_OVER_MAX_PRICE = "min_price_over_max_price";

    private Context context;
    private AsyncHttpClient asyncHttpClient;
    private OnPostRegisterUpdateListener onPostRegisterUpdateListener;
    private SystemSettingHelper systemSettingHelper;

    public MemberParam getMemberParam() {
        return memberParam;
    }

    public void setMemberParam(MemberParam memberParam) {
        this.memberParam = memberParam;
    }

    public OnPostRegisterUpdateListener getOnPostRegisterUpdateListener() {
        return onPostRegisterUpdateListener;
    }

    public void setOnPostRegisterUpdateListener(OnPostRegisterUpdateListener onPostRegisterUpdateListener) {
        this.onPostRegisterUpdateListener = onPostRegisterUpdateListener;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public int getPostType() {
        return postType;
    }

    public void setPostType(int postType) {
        this.postType = postType;
    }

    @Override
    public void callApi() {
        super.callApi();
        if (isValidParams()){
            if (Utils.isConnectInet(getContext())){
                String url = getPostType() == POST_REGISTER ? URLs.SERVER_REGISTER : URLs.SERVER_UPDATE;
                StringEntity param = getBodyParams();
                Log.d("RegisterParam", param.toString());
                if (param != null){
                    asyncHttpClient = getAsyncHttpClient(getUserPreference(getContext()).getToken());
                    asyncHttpClient.post(getContext(), url, param, "application/json", new AsyncHttpResponseHandler(){
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            super.onSuccess(statusCode, headers, responseBody);
                            String s = new String(responseBody);
                            Log.d("Response "+getPostType(), s);
                            Gson gson = new Gson();

                            if (getPostType() == POST_REGISTER){
                                RegisterUpdateResponse registerUpdateResponse = gson.fromJson(s, RegisterUpdateResponse.class);
                                if (registerUpdateResponse.getResultCode().getCode() == CODE_SUCCESS){
                                    if (registerUpdateResponse.getResultCode().getMessage().toUpperCase(Locale.ENGLISH).contains("TOKEN")){
                                        getOnPostRegisterUpdateListener().onPostRegisterUpdateFailed("Sesi login anda sudah habis, mohon melakukan login ulang");
                                    }else{
                                        getOnPostRegisterUpdateListener().onPostRegisterSuccess(registerUpdateResponse);
                                    }
                                }else{
                                    getOnPostRegisterUpdateListener().onPostRegisterUpdateFailed(registerUpdateResponse.getResultCode().getMessage());
                                }
                            }else{
                                ProfileResponse profileResponse = gson.fromJson(s, ProfileResponse.class);
                                if (profileResponse.getResultCode().getCode() == CODE_SUCCESS){
                                    getOnPostRegisterUpdateListener().onPostUpdateSuccess(profileResponse);
                                }else{
                                    getOnPostRegisterUpdateListener().onPostRegisterUpdateFailed(profileResponse.getResultCode().getMessage());
                                }
                            }

                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            super.onFailure(statusCode, headers, responseBody, error);
                            Log.d("ErrorRegister", new String(responseBody));
                            int errorType = getPostType() == POST_REGISTER ? R.string.error_register :
                                    R.string.error_update;
                            getOnPostRegisterUpdateListener().onPostRegisterUpdateFailed(getContext().getString(errorType));
                        }
                    });
                }else {
                    getOnPostRegisterUpdateListener().onPostRegisterUpdateFailed(getContext().getString(R.string.error_param_invalid));
                }
            }else{
                getOnPostRegisterUpdateListener().onPostRegisterUpdateFailed(getContext().getString(R.string.no_internet));
            }
        }else{
            getOnPostRegisterUpdateListener().onParamsInvalid(invalidParams);
        }
    }

    public StringEntity getBodyParams(){
        StringEntity entity = null;
        Gson gson = new Gson();
        MemberParam m = getMemberParam();
        if (m.getProfileParam().getCityId().equalsIgnoreCase("0")){
            m.getProfileParam().setCityId("null");
        }

        if (m.getProfileParam().getProvinceId().equalsIgnoreCase("0")){
            m.getProfileParam().setProvinceId("null");
        }

        if (m.getpreference().getMaxPriceId().equalsIgnoreCase("0")){
            m.getpreference().setMaxPriceId("null");
        }

        if (m.getpreference().getMinPriceId().equalsIgnoreCase("0")){
            m.getpreference().setMinPriceId("null");
        }

        String bodyParam = gson.toJson(m);
        Log.d("BodyParam", bodyParam);
        try {
            entity = new StringEntity(bodyParam);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public boolean isValidParams() {
        return isValidMemberParam() && isValidPreferenceParam();
    }

    public void validateMemberParam(){
        if (!isValidMemberParam()){
            getOnPostRegisterUpdateListener().onParamsInvalid(invalidParams);
        }else{
            getOnPostRegisterUpdateListener().onValidateMemberParamSuccess();
        }
    }

    public boolean isValidMemberParam(){
        invalidParams.clear();
        boolean isValidParam = true;
        systemSettingHelper = new SystemSettingHelper(getContext());
        systemSettingHelper.open();

        Profile profileParam = getMemberParam().getProfileParam();
        if (TextUtils.isEmpty(profileParam.getFullName())){
            isValidParam = false;
            invalidParams.put(ERROR_FULLNAME, PARAM_FULLNAME);
        }

        if (!Utils.validEmail(profileParam.getEmail())){
            isValidParam = false;
            invalidParams.put(ERROR_EMAIL, PARAM_EMAIL);
        }

        if (systemSettingHelper.getDataById("MandatoryForMemberPlaceOfBirth").get(0).getName().equalsIgnoreCase("True")){
            if (TextUtils.isEmpty(profileParam.getPlaceOfBirth())){
                isValidParam = false;
                invalidParams.put(ERROR_PLACE_OF_BIRTH, PARAM_PLACE_OF_BIRTH);
            }

            if (!Utils.isValidBirthPlace(profileParam.getPlaceOfBirth())){
                isValidParam = false;
                invalidParams.put(ERROR_PLACE_OF_BIRTH, PARAM_PLACE_OF_BIRTH_NOT_VALID);
            }
        }

        /*if (systemSettingHelper.getDataById("MandatoryForMemberDateOfBirth").get(0).getName().equalsIgnoreCase("True")){

        }*/

        if (TextUtils.isEmpty(profileParam.getDateOfBirth())){
            isValidParam = false;
            invalidParams.put(ERROR_DATE_OF_BIRTH, PARAM_DATE_OF_BIRTH);
        }else{
            if (!Utils.isValidDate(profileParam.getDateOfBirth())){
                isValidParam = false;
                invalidParams.put(ERROR_INVALID_DATE_OF_BIRTH, PARAM_INVALID_DATE_OF_BIRTH);
            }
        }

        if (systemSettingHelper.getDataById("MandatoryForMemberMobilePhoneNumber").get(0).getName().equalsIgnoreCase("True")){
            if (TextUtils.isEmpty(profileParam.getMobilePhoneNumber())){
                isValidParam = false;
                invalidParams.put(ERROR_MOBILE_PHONE_NUMBER, PARAM_MOBILE_PHONE_NUMBER);
            }
        }

        if (systemSettingHelper.getDataById("MandatoryForMemberAddress").get(0).getName().equalsIgnoreCase("True")){
            if (TextUtils.isEmpty(profileParam.getAddress())){
                isValidParam = false;
                invalidParams.put(ERROR_ADDRESS, PARAM_ADDRESS);
            }
        }

        if (systemSettingHelper.getDataById("MandatoryForMemberProvince").get(0).getName().equalsIgnoreCase("True")){
            if (profileParam.getProvinceId().equalsIgnoreCase("0")){
                isValidParam = false;
                invalidParams.put(ERROR_PROVINCE_ID, PARAM_PROVINCE_ID);
            }
        }

        if (systemSettingHelper.getDataById("MandatoryForMemberCity").get(0).getName().equalsIgnoreCase("True")){
            if (profileParam.getCityId().equalsIgnoreCase("0")){
                isValidParam = false;
                invalidParams.put(ERROR_CITY_ID, PARAM_CITY_ID);
            }
        }

        if (systemSettingHelper.getDataById("MandatoryForMemberIdentificationNumber").get(0).getName().equalsIgnoreCase("True")){
            if (TextUtils.isEmpty(profileParam.getIdentificationNumber())){
                isValidParam = false;
                invalidParams.put(ERROR_IDENTIFICATION_NUMBER, PARAM_IDENTIFICATION_NUMBER);
            }
        }

        if (getPostType() == POST_REGISTER){
            if (TextUtils.isEmpty(profileParam.getPassword())){
                isValidParam = false;
                invalidParams.put(ERROR_PASSWORD, PARAM_PASSWORD);
            }else{
                if (!Utils.isValidPassword(profileParam.getPassword())){
                    isValidParam = false;
                    invalidParams.put(ERROR_PASSWORD_NOT_VALID, PARAM_PASSWORD_NOT_VALID);
                }
            }

            if (TextUtils.isEmpty(profileParam.getConfirmPassword())){
                isValidParam = false;
                invalidParams.put(ERROR_CONFIRM_PASSWORD, PARAM_CONFIRM_PASSWORD);
            }else{
                if (!Utils.isValidPassword(profileParam.getPassword())){
                    isValidParam = false;
                    invalidParams.put(ERROR_PASSWORD_NOT_VALID, PARAM_PASSWORD_NOT_VALID);
                }
            }
        }

        if (getPostType() == POST_UPDATE){
            if (!TextUtils.isEmpty(profileParam.getPassword())){
                if(TextUtils.isEmpty(profileParam.getOldPassword())){
                    isValidParam = false;
                    invalidParams.put(ERROR_OLD_PASSWORD, PARAM_OLD_PASSWORD);
                }

                if (!Utils.isValidPassword(profileParam.getPassword())){
                    isValidParam = false;
                    invalidParams.put(ERROR_PASSWORD_NOT_VALID, PARAM_PASSWORD_NOT_VALID);
                }

                if (TextUtils.isEmpty(profileParam.getConfirmPassword())){
                    isValidParam = false;
                    invalidParams.put(ERROR_CONFIRM_PASSWORD, PARAM_CONFIRM_PASSWORD);
                }
            }
        }

        if (!profileParam.getPassword().equals(profileParam.getConfirmPassword())){
            isValidParam = false;
            invalidParams.put(ERROR_PASSWORD_NOT_MATCH, PARAM_PASSWORD_NOT_MATCH);
        }

        systemSettingHelper.close();

        return isValidParam;
    }

    public void validatePreferenceParam(){
        if (!isValidPreferenceParam()){
            getOnPostRegisterUpdateListener().onParamsInvalid(invalidParams);
        }else{
            getOnPostRegisterUpdateListener().onValidatePreferenceSuccess();
        }
    }

    public boolean isValidPreferenceParam(){
        boolean isValidParam = true;

        systemSettingHelper = new SystemSettingHelper(getContext());
        systemSettingHelper.open();
        MinMaxPriceHelper minMaxPriceHelper = new MinMaxPriceHelper(getContext());
        minMaxPriceHelper.open();

        if (systemSettingHelper.getDataById("MandatoryForMemberPreference").get(0).getName().equalsIgnoreCase("True")){
            Preference preferenceParam = getMemberParam().getpreference();

            if (preferenceParam.getMaxPriceId().equalsIgnoreCase("0")){
                isValidParam = false;
                invalidParams.put(ERROR_MAX_PRICE_ID, PARAM_MAX_PRICE_ID);
            }

           if (preferenceParam.getMinPriceId().equalsIgnoreCase("0")){
                isValidParam = false;
                invalidParams.put(ERROR_MIN_PRICE_ID, PARAM_MIN_PRICE_ID);
            }

            if (!preferenceParam.getMinPriceId().equalsIgnoreCase("0") &&
                    !preferenceParam.getMaxPriceId().equalsIgnoreCase("0")){

                long maxAmount = Long.parseLong(minMaxPriceHelper.loadAllDataById(preferenceParam.getMaxPriceId()).get(0).getValue());
                long minAmount =  Long.parseLong(minMaxPriceHelper.loadAllDataById(preferenceParam.getMinPriceId()).get(0).getValue());


                if (maxAmount > 0 && minAmount > 0){
                    if (maxAmount < minAmount){
                        isValidParam = false;
                        invalidParams.put(ERROR_MIN_PRICE_OVER_MAX_PRICE, PARAM_MIN_PRICE_OVER_MAX_PRICE);
                    }
                }
            }

            if (preferenceParam.getCategoryIDs().size() == 0){
                isValidParam = false;
                invalidParams.put(ERROR_CATEGORY_IDS, PARAM_CATEGORY_IDS);
            }

            if (preferenceParam.getCityIDs().size() == 0){
                isValidParam = false;
                invalidParams.put(ERROR_CITY_IDS, PARAM_CITY_IDS);
            }

        }

        systemSettingHelper.close();

        return isValidParam;
    }

    @Override
    public void cancel() {
        super.cancel();
        if (asyncHttpClient != null){
            asyncHttpClient.cancelRequests(getContext(), true);
        }
    }

    @Override
    public void retry() {
        super.retry();
        callApi();
    }

    public interface OnPostRegisterUpdateListener extends BaseListener{
        void onPostRegisterSuccess(RegisterUpdateResponse registerUpdateResponse);
        void onPostUpdateSuccess(ProfileResponse profileResponse);
        void onPostRegisterUpdateFailed(String errorMessage);
        void onValidateMemberParamSuccess();
        void onValidatePreferenceSuccess();
    }
}
