package com.sparkworks.mandiri.lelang.fragment;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.mandiri.lelang.aplikasi.DetilLelangItemActivity;
import com.mandiri.lelang.aplikasi.LaunchscreenActivity;
import com.mandiri.lelang.aplikasi.R;
import com.mandiri.lelang.aplikasi.RegisterLoginActivity;
import com.sparkworks.mandiri.lelang.adapter.DetilLelangFragmentAdapter;
import com.sparkworks.mandiri.lelang.base.BaseFragment;
import com.sparkworks.mandiri.lelang.event.FavoriteEvent;
import com.sparkworks.mandiri.lelang.event.SetFavoriteUnfavoriteEvent;
import com.sparkworks.mandiri.lelang.event.UnfavoriteEvent;
import com.sparkworks.mandiri.lelang.model.Item;
import com.sparkworks.mandiri.lelang.model.MasterData;
import com.sparkworks.mandiri.lelang.request.api.FavoriteUnfavoriteRequest;
import com.sparkworks.mandiri.lelang.request.response.FavoriteUnfavoriteResponse;
import com.sparkworks.mandiri.lelang.utils.ApiHelper;
import com.sparkworks.mandiri.lelang.utils.Constant;
import com.sparkworks.mandiri.lelang.utils.LocationUtils;
import com.sparkworks.mandiri.lelang.utils.URLs;
import com.sparkworks.mandiri.lelang.utils.Utils;

public class DetilLelangItemFragment extends BaseFragment
	implements FavoriteUnfavoriteRequest.OnFavoriteUnfavoriteRequestListener{
	public static String FRAGMENT_TAG = "DetilLelangItemFragment";
	private PagerSlidingTabStrip tabPageIndicator;
	private LinearLayout lnDetilItem;
	private ProgressBar indicator;
	private ViewPager pager;
    private LocationUtils locationUtils;
    private LocationUtils.LocationResult locationResult;
    private String cityId = null, provinceId = null, cityName = null;

	private FavoriteUnfavoriteRequest favoriteUnfavoriteRequest;

	public DetilLelangItemFragment(){}
	
	public Item item, detItem;
	private int position;

	private AsyncHttpClient asyncHttpClient;

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	private ApiHelper.LelangType lelangType;

	public ApiHelper.LelangType getLelangType() {
		return lelangType;
	}

	public void setLelangType(ApiHelper.LelangType lelangType) {
		this.lelangType = lelangType;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		asyncHttpClient = new AsyncHttpClient(true, 80, 443);
		EventBus.getDefault().register(this);
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_detil_lelang_item, container, false);
         initializeViews(rootView);
         getActivity().getActionBar().setDisplayUseLogoEnabled(false);
         getActivity().getActionBar().setDisplayShowHomeEnabled(true);
         getActivity().getActionBar().setIcon(R.drawable.logo_home);
         getActivity().getActionBar().setTitle("");
         getActivity().getActionBar().setHomeButtonEnabled(true);
         getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        return rootView;
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		initializeLibs();
		initializeProcess();
		
		if (savedInstanceState != null) {
			setItem((Item)savedInstanceState.getSerializable(DetilLelangItemActivity.ITEM_KEY));
			setLelangType((ApiHelper.LelangType)savedInstanceState.getSerializable(DetilLelangItemActivity.LELANG_TYPE_KEY));
			setPosition(savedInstanceState.getInt(DetilLelangItemActivity.POSITION_KEY));
		}else{
			setLelangType((ApiHelper.LelangType) getArguments().getSerializable(DetilLelangItemActivity.LELANG_TYPE_KEY));
			setPosition(getArguments().getInt(DetilLelangItemActivity.POSITION_KEY));
		}
		
		getUserLocation();
	}

	@Override
	public void initializeLibs() {
		super.initializeLibs();
		favoriteUnfavoriteRequest = new FavoriteUnfavoriteRequest();
		favoriteUnfavoriteRequest.setContext(getActivity());
		favoriteUnfavoriteRequest.setAssetId(Integer.parseInt(getItem().getAssetId()));
		favoriteUnfavoriteRequest.setOnFavoriteUnfavoriteRequestListener(this);
	}

	@Override
	public void initializeViews(View view) {
		// TODO Auto-generated method stub
		super.initializeViews(view);
		setHasOptionsMenu(true);
		 tabPageIndicator = (PagerSlidingTabStrip)view.findViewById(R.id.tabs);
		 indicator = (ProgressBar)view.findViewById(R.id.indicator);
		 lnDetilItem = (LinearLayout)view.findViewById(R.id.ln_detil_item_fragment);
         pager = (ViewPager)view.findViewById(R.id.pager);
         final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
 				.getDisplayMetrics());
 		pager.setPageMargin(pageMargin);
	}
	
	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		super.initializeProcess();
		
	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		// TODO Auto-generated method stub
		super.onHiddenChanged(hidden);
		
		getActivity().getActionBar().setTitle("Detil Item");
	}
	
	public void getDetailItem(){
        List<NameValuePair> listParam = new ArrayList<>();
        listParam.add(new BasicNameValuePair("cityID", cityId));

		String request = ApiHelper.buildAPIURLMessage(URLs.ITEM_GET_DETAILS, getItem().getAssetId(), listParam);
		Log.d(Constant.APP_TAG, request);
		asyncHttpClient.addHeader("Authorization", "Bearer "+userPreference.getToken());
		asyncHttpClient.get(request, new AsyncHttpResponseHandler(){
			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				// TODO Auto-generated method stub
				super.onSuccess(arg0, arg1, arg2);
				indicator.setVisibility(View.GONE);
				lnDetilItem.setVisibility(View.VISIBLE);

				String res = new String(arg2);
				detItem = Item.getItemDetailData(res);
				
				DetilLelangFragmentAdapter detilLelangFragmentAdapter = new DetilLelangFragmentAdapter(getFragmentManager());
				detilLelangFragmentAdapter.setItem(detItem);
				pager.setAdapter(detilLelangFragmentAdapter);
				tabPageIndicator.setViewPager(pager);
			}
			
			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				// TODO Auto-generated method stub
				super.onFailure(arg0, arg1, arg2, arg3);
				indicator.setVisibility(View.GONE);
				lnDetilItem.setVisibility(View.GONE);
				//Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
			}
		});
	}


	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		outState.putSerializable(DetilLelangItemActivity.ITEM_KEY, getItem());
		outState.putSerializable(DetilLelangItemActivity.LELANG_TYPE_KEY, getLelangType());
		outState.putInt(DetilLelangItemActivity.POSITION_KEY, getPosition());
		super.onSaveInstanceState(outState);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_detil_lelang, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId()==R.id.action_share) {
			share();
		}

		if (item.getItemId()==android.R.id.home) {
			getActivity().finish();
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	public void share(){
		
		if (detItem != null) {
			String title = "";
			String text = "";
			
			try {
				title = URLDecoder.decode(detItem.getEmailSubject(), HTTP.UTF_8);
				text = URLDecoder.decode(detItem.getEmailText(), HTTP.UTF_8);
			} catch (Exception e) {
			}
			
			Intent intent=new Intent(android.content.Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

			// Add data to the intent, the receiving app will decide what to do with it.
			intent.putExtra(Intent.EXTRA_SUBJECT, title);
			intent.putExtra(Intent.EXTRA_TEXT, text);
			startActivity(Intent.createChooser(intent, "Share to"));
		}
	}

	@Override
	public void onFavoriteSuccess(FavoriteUnfavoriteResponse favoriteUnfavoriteResponse) {
		dismissProgressDialog();

		getItem().setFavorite(true);
		//Todo update the icon star in the DetailLelangItemInformasiFragment
		Log.d("Favorite", "Set Favorite Berhasil");
		FavoriteEvent favoriteEvent = new FavoriteEvent();
		favoriteEvent.setLelangType(getLelangType());
		favoriteEvent.setAssetId(Integer.parseInt(getItem().getAssetId()));
		favoriteEvent.setPosition(getPosition());
		favoriteEvent.setUpdatedItem(getItem());

		EventBus.getDefault().post(favoriteEvent);

		Toast.makeText(getActivity(), "Berhasil menambahkan ke favorite", Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onFavoriteFailed(String message) {
		dismissProgressDialog();
		Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
		if (message.toUpperCase(Locale.ENGLISH).contains("TOKEN")){
			LaunchscreenActivity.start(getActivity());
			getActivity().finish();
		}
	}

	@Override
	public void onUnfavoriteSuccess(FavoriteUnfavoriteResponse favoriteUnfavoriteResponse) {
		dismissProgressDialog();

		getItem().setFavorite(false);
		//Todo update the icon star in the DetailLelangItemInformasiFragment
		Log.d("Unfavorite", "Set Unfavorite Berhasil");
		UnfavoriteEvent unfavoriteEvent = new UnfavoriteEvent();
		unfavoriteEvent.setLelangType(getLelangType());
		unfavoriteEvent.setAssetId(Integer.parseInt(getItem().getAssetId()));
		unfavoriteEvent.setPosition(getPosition());
		unfavoriteEvent.setUpdatedItem(getItem());

		EventBus.getDefault().post(unfavoriteEvent);

		Toast.makeText(getActivity(), "Berhasil menghapus dari favorite", Toast.LENGTH_SHORT).show();

		getActivity().finish();

	}

	@Override
	public void onUnfavoriteFailed(String message) {
		dismissProgressDialog();
		Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
		if (message.toUpperCase(Locale.ENGLISH).contains("TOKEN")){
			LaunchscreenActivity.start(getActivity());
			getActivity().finish();
		}
	}

    private void getUserLocation(){
        indicator.setVisibility(View.VISIBLE);
        lnDetilItem.setVisibility(View.GONE);

        locationUtils = new LocationUtils(getActivity());
        locationResult = new LocationUtils.LocationResult() {
            @Override
            public void gotLocation(Location location) {
                new GetReverseAddress(location).execute();
            }
        };
        locationUtils.getLocation(getActivity(), locationResult);
    }

    private class GetReverseAddress extends AsyncTask<Void, Void, String> {

        Location location;

        public GetReverseAddress(Location location) {
            // TODO Auto-generated constructor stub
            this.location = location;
        }

        @Override
        protected String doInBackground(Void... params) {
            // TODO Auto-generated method stub
            String result = null;
            try {
                result = Utils.goToGeocoder(getActivity(),
                        location.getLatitude(),
                        location.getLongitude());
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String kota) {
            // TODO Auto-generated method stub
            super.onPostExecute(kota);


            if (kota != null) {
                ArrayList<MasterData> listCity = new ArrayList<MasterData>();
                listCity = cityHelper.loadAllCityByKeyword(kota.replace("Kota", "").trim());
                if (listCity.size()>0) {
                    cityId = listCity.get(0).getId();
                    provinceId = listCity.get(0).getProvinceId();
                    cityName = listCity.get(0).getName();
                } else {
                    cityId = "0";
                    provinceId = "0";
                    cityName = kota;
                }
            }else{
                cityId = "0";
                provinceId = "0";
                cityName = "";
            }
            Log.d(Constant.APP_TAG, kota+ " City ID : " + cityId + " PROVINCE ID : "+provinceId);

            getDetailItem();
        }
    }

	@Subscribe
	public void onSetFavoriteUnfavoriteEvent(SetFavoriteUnfavoriteEvent event){
		if (!TextUtils.isEmpty(userPreference.getToken())){
			if (getItem().isFavorite()){
				favoriteUnfavoriteRequest.setPostType(FavoriteUnfavoriteRequest.POST_UNFAVORITE);
			}else{
				favoriteUnfavoriteRequest.setPostType(FavoriteUnfavoriteRequest.POST_FAVORITE);
			}

			showProgressDialog(null, getString(R.string.general_progress_dialog_message));
			favoriteUnfavoriteRequest.callApi();
		}else{
			RegisterLoginActivity.start(getContext(), RegisterFragment.SOURCE_REGISTER);
		}
	}

	@Override
	public void onDestroy() {
		EventBus.getDefault().unregister(this);
		super.onDestroy();
	}
}
