package com.sparkworks.mandiri.lelang.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sparkworks.mandiri.lelang.fragment.DetilLelangItemInformasiFragment;
import com.sparkworks.mandiri.lelang.fragment.DetilLelangItemJadwalFragment;
import com.sparkworks.mandiri.lelang.fragment.DetilLelangItemPengelolaFragment;
import com.sparkworks.mandiri.lelang.fragment.DetilLelangItemSyaratFragment;
import com.sparkworks.mandiri.lelang.model.Item;

public class DetilLelangFragmentAdapter extends FragmentPagerAdapter{

	public static String[] title = new String[]{
		"Informasi", "Jadwal Lelang", "Pengelola", "Syarat dan Ketentuan"
	};
	
	Item item;

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	DetilLelangItemInformasiFragment informasiFragment;
	DetilLelangItemJadwalFragment jadwalFragment;
	DetilLelangItemPengelolaFragment pengelolaFragment;
	DetilLelangItemSyaratFragment syaratFragment;
	
	public DetilLelangFragmentAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int arg0) {
		// TODO Auto-generated method stub
		if (arg0==0) {
			if (informasiFragment == null) {
				informasiFragment = new DetilLelangItemInformasiFragment();
				informasiFragment.setItem(getItem());
			}
			return informasiFragment;
		}
		else if(arg0==1){
			if (jadwalFragment == null) {
				jadwalFragment = new DetilLelangItemJadwalFragment();
				jadwalFragment.setItem(getItem());
			}
			return jadwalFragment;
		}else if(arg0==2){
			if (pengelolaFragment == null) {
				pengelolaFragment = new DetilLelangItemPengelolaFragment();
				pengelolaFragment.setPengelola(getItem().getBranch());
				pengelolaFragment.setAuctionHall(getItem().getAuctionHall());
			}
			return pengelolaFragment;
		}else if(arg0==3){
			if (syaratFragment == null) {
				syaratFragment = new DetilLelangItemSyaratFragment();
			}
			return syaratFragment;
		}else{
			return null;
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return title.length;
	}
	
    public CharSequence getPageTitle(int position) {
      return title[position % title.length];
    }
	
}