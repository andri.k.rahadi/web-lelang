package com.sparkworks.mandiri.lelang.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class AppPreference {
	Context context;
	
	SharedPreferences sharedPreferences = null;
	Editor editor = null;
	String PREF_NAME = "mandiriprefs";
	String KEY_LASTTIMESTAMP = "lasttimestamp";
	String KEY_LATITUDE = "latitude";
	String KEY_LONGITUDE = "longitude";

	private final String KEY_ENTRY_SOURCE = "KEY_ENTRY_SOURCE";
	public static final String SOURCE_EMAIL = "source_email";
	public static final String SOURCE_FACEBOOK = "source_facebook";

	
	public AppPreference(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		editor = sharedPreferences.edit();
	}
	
	public void setTimestamp(String timestamp){
		editor.putString(KEY_LASTTIMESTAMP, timestamp);
		editor.commit();
	}
	
	public String getTimestamp(){
		return sharedPreferences.getString(KEY_LASTTIMESTAMP, "");
	}
	
	public void setLatitude(String latitude){
		editor.putString(KEY_LATITUDE, latitude);
		editor.commit();
	}
	
	public String getLatitude(){
		return sharedPreferences.getString(KEY_LATITUDE, "");
	}
	
	public void setLongitude(String longitude){
		editor.putString(KEY_LATITUDE, longitude);
		editor.commit();
	}
	
	public String getLongitude(){
		return sharedPreferences.getString(KEY_LONGITUDE, "");
	}


	public void setEntrySource(String source){
		editor.putString(KEY_ENTRY_SOURCE, source);
		editor.commit();
	}

	public String getEntrySource(){
		return sharedPreferences.getString(KEY_ENTRY_SOURCE, null);
	}

}
