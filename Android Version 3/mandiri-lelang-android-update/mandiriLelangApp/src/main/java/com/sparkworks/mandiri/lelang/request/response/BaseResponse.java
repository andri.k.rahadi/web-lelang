package com.sparkworks.mandiri.lelang.request.response;

import com.google.gson.annotations.SerializedName;
import com.sparkworks.mandiri.lelang.request.model.ResultCode;

/**
 * Created by sidiqpermana on 11/24/16.
 */

public class BaseResponse {
    @SerializedName("resultCode")
    private ResultCode resultCode;

    public ResultCode getResultCode() {
        return resultCode;
    }

    public void setResultCode(ResultCode resultCode) {
        this.resultCode = resultCode;
    }
}
