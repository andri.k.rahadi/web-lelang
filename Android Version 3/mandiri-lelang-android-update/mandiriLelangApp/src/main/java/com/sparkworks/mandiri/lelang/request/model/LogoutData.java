package com.sparkworks.mandiri.lelang.request.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sidiqpermana on 11/29/16.
 */

public class LogoutData {
    @SerializedName("memberID")
    private int memberId;

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }
}
