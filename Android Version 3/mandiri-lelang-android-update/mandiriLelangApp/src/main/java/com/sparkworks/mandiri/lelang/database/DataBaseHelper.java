package com.sparkworks.mandiri.lelang.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper{
	
	public static String DATABASE_NAME = "dbmandirilelang";
	public static String TABLE_CATEGORY_NAME = "tbl_category";
	
	public static String FIELD_CATEGORY_NAME = "name";
	public static String FIELD_CATEGORY_ID = "id";
	public static String FIELD_CATEGORY_IS_ACTIVE = "isActive";
	public static String FIELD_CATEGORY_DESCRIPTION = "description";
	public static String FIELD_CATEGORY_FLAG = "flag";
	
	public static String TABLE_PROVINCE_NAME = "tbl_province";
	
	public static String FIELD_PROVINCE_NAME = "name";
	public static String FIELD_PROVINCE_ID = "id";
	public static String FIELD_PROVINCE_IS_ACTIVE = "isActive";
	public static String FIELD_PROVINCE_FLAG = "flag";
	public static String FIELD_PROVINCE_DESCRIPTION = "description";
	
	public static String TABLE_CITY_NAME = "tbl_city";
	
	public static String FIELD_CITY_NAME = "name";
	public static String FIELD_CITY_ID = "id";
	public static String FIELD_CITY_IS_ACTIVE = "isActive";
	public static String FIELD_CITY_FLAG = "flag";
	public static String FIELD_CITY_PROVINCE_ID = "province_id";
	public static String FIELD_CITY_DESCRIPTION = "description";
	
	public static String TABLE_MINMAXPRICE_NAME = "tbl_minmaxprice";
	
	public static String FIELD_MINMAXPRICE_NAME = "name";
	public static String FIELD_MINMAXPRICE_ID = "id";
	public static String FIELD_MINMAXPRICE_IS_ACTIVE = "isActive";
	public static String FIELD_MINMAXPRICE_FLAG = "flag";
	public static String FIELD_MINMAXPRICE_VALUE = "value";
	public static String FIELD_MINMAXPRICE_DESCRIPTION = "description";
	
	public static String TABLE_DOCUMENTTYPE_NAME = "tbl_documenttype";
	
	public static String FIELD_DOCUMENTTYPE_NAME = "name";
	public static String FIELD_DOCUMENTTYPE_ID = "id";
	public static String FIELD_DOCUMENTTYPE_IS_ACTIVE = "isActive";
	public static String FIELD_DOCUMENTTYPE_FLAG = "flag";
	public static String FIELD_DOCUMENTTYPE_DESCRIPTION = "description";
	
	public static String TABLE_CONTACTUS_NAME = "tbl_contactus";
	
	public static String FIELD_CONTACTUS_NAME = "name";
	public static String FIELD_CONTACTUS_ID = "id";
	public static String FIELD_CONTACTUS_IS_ACTIVE = "isActive";
	public static String FIELD_CONTACTUS_FLAG = "flag";
	public static String FIELD_CONTACTUS_DESCRIPTION = "description";
	
	public static String TABLE_BRANCH_NAME = "tbl_branch";
	
	public static String FIELD_BRANCH_NAME = "name";
	public static String FIELD_BRANCH_ID = "id";
	public static String FIELD_BRANCH_IS_ACTIVE = "isActive";
	public static String FIELD_BRANCH_FLAG = "flag";
	public static String FIELD_BRANCH_DESCRIPTION = "description";

	public static String TABLE_SYSTEM_SETTING_NAME = "tbl_system_setting";

	public static String FIELD_SYSTEM_SETTING_NAME = "name";
	public static String FIELD_SYSTEM_SETTING_ID = "id";
	public static String FIELD_SYSTEM_SETTING_IS_ACTIVE = "isActive";
	public static String FIELD_SYSTEM_SETTING_DESCRIPTION = "description";
	public static String FIELD_SYSTEM_SETTING_FLAG = "flag";

	public static String TABLE_WEBCONTENT_NAME = "tbl_webcontent";

	public static String FIELD_WEBCONTENT_NAME = "name";
	public static String FIELD_WEBCONTENT_ID = "id";
	public static String FIELD_WEBCONTENT_IS_ACTIVE = "isActive";
	public static String FIELD_WEBCONTENT_DESCRIPTION = "description";
	public static String FIELD_WEBCONTENT_FLAG = "flag";

	public static String TABLE_KNPL_NAME = "tbl_knpl";

	public static String FIELD_KNPL_NAME = "name";
	public static String FIELD_KNPL_ID = "id";
	public static String FIELD_KNPL_IS_ACTIVE = "isActive";
	public static String FIELD_KNPL_DESCRIPTION = "description";
	public static String FIELD_KNPL_FLAG = "flag";
	
	private static final int DATABASE_VERSION = 3;
	
	public static String CREATE_TABLE_CATEGORY = "create table "+TABLE_CATEGORY_NAME+" (_id integer primary key autoincrement, " +
			FIELD_CATEGORY_ID+" text not null, " +
			FIELD_CATEGORY_NAME+" text not null, " +
			FIELD_CATEGORY_DESCRIPTION+" text not null, " +
			FIELD_CATEGORY_IS_ACTIVE+" text not null, " +
			FIELD_CATEGORY_FLAG+" text not null " +
			");";
	
	public static String CREATE_TABLE_PROVINCE = "create table "+TABLE_PROVINCE_NAME+" (_id integer primary key autoincrement, " +
			FIELD_PROVINCE_ID+" text not null, " +
			FIELD_PROVINCE_NAME+" text not null, " +
			FIELD_PROVINCE_DESCRIPTION+" text not null, " +
			FIELD_PROVINCE_IS_ACTIVE+" text not null, " +
			FIELD_PROVINCE_FLAG+" text not null " +
			");";
	
	public static String CREATE_TABLE_CITY = "create table "+TABLE_CITY_NAME+" (_id integer primary key autoincrement, " +
			FIELD_CITY_ID+" text not null, " +
			FIELD_CITY_NAME+" text not null, " +
			FIELD_CITY_DESCRIPTION+" text not null, " +
			FIELD_CITY_IS_ACTIVE+" text not null, " +
			FIELD_CITY_FLAG+" text not null, " +
			FIELD_CITY_PROVINCE_ID+" text not null);";
	
	public static String CREATE_TABLE_DOCUMENTTYPE = "create table "+TABLE_DOCUMENTTYPE_NAME+" (_id integer primary key autoincrement, " +
			FIELD_DOCUMENTTYPE_ID+" text not null, " +
			FIELD_DOCUMENTTYPE_NAME+" text not null, " +
			FIELD_DOCUMENTTYPE_DESCRIPTION+" text not null, " +
			FIELD_DOCUMENTTYPE_IS_ACTIVE+" text not null, " +
			FIELD_DOCUMENTTYPE_FLAG+" text not null);";
	
	public static String CREATE_TABLE_CONTACTUS = "create table "+TABLE_CONTACTUS_NAME+" (_id integer primary key autoincrement, " +
			FIELD_CONTACTUS_ID+" text not null, " +
			FIELD_CONTACTUS_NAME+" text not null, " +
			FIELD_CONTACTUS_DESCRIPTION+" text not null, " +
			FIELD_CONTACTUS_IS_ACTIVE+" text not null, " +
			FIELD_CONTACTUS_FLAG+" text not null);";
	
	public static String CREATE_TABLE_BRANCH = "create table "+TABLE_BRANCH_NAME+" (_id integer primary key autoincrement, " +
			FIELD_BRANCH_ID+" text not null, " +
			FIELD_BRANCH_NAME+" text not null, " +
			FIELD_BRANCH_DESCRIPTION+" text not null, " +
			FIELD_BRANCH_IS_ACTIVE+" text not null, " +
			FIELD_BRANCH_FLAG+" text not null);";
	
	public static String CREATE_TABLE_MINMAXPRICE = "create table "+TABLE_MINMAXPRICE_NAME+" (_id integer primary key autoincrement, " +
			FIELD_BRANCH_ID+" text not null, " +
			FIELD_MINMAXPRICE_NAME+" text not null, " +
			FIELD_MINMAXPRICE_DESCRIPTION+" text not null, " +
			FIELD_MINMAXPRICE_IS_ACTIVE+" text not null, " +
			FIELD_MINMAXPRICE_FLAG+" text not null, " +
			FIELD_MINMAXPRICE_VALUE+" number not null);";

	public static String CREATE_TABLE_SYSTEM_SETTING = "create table "+TABLE_SYSTEM_SETTING_NAME+" (_id integer primary key autoincrement, " +
			FIELD_SYSTEM_SETTING_ID+" text not null, " +
			FIELD_SYSTEM_SETTING_NAME+" text not null, " +
			FIELD_SYSTEM_SETTING_DESCRIPTION+" text not null, " +
			FIELD_SYSTEM_SETTING_IS_ACTIVE+" text not null, " +
			FIELD_SYSTEM_SETTING_FLAG+" text not null " +
			");";

	public static String CREATE_TABLE_WEBCONTENT = "create table "+TABLE_WEBCONTENT_NAME+" (_id integer primary key autoincrement, " +
			FIELD_WEBCONTENT_ID+" text not null, " +
			FIELD_WEBCONTENT_NAME+" text not null, " +
			FIELD_WEBCONTENT_DESCRIPTION+" text not null, " +
			FIELD_WEBCONTENT_IS_ACTIVE+" text not null, " +
			FIELD_WEBCONTENT_FLAG+" text not null " +
			");";

	public static String CREATE_TABLE_KNPL = "create table "+TABLE_KNPL_NAME+" (_id integer primary key autoincrement, " +
			FIELD_KNPL_ID+" text not null, " +
			FIELD_KNPL_NAME+" text not null, " +
			FIELD_KNPL_DESCRIPTION+" text not null, " +
			FIELD_KNPL_IS_ACTIVE+" text not null, " +
			FIELD_KNPL_FLAG+" text not null " +
			");";
	
	public DataBaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_TABLE_BRANCH);
		db.execSQL(CREATE_TABLE_CATEGORY);
		db.execSQL(CREATE_TABLE_CITY);
		db.execSQL(CREATE_TABLE_CONTACTUS);
		db.execSQL(CREATE_TABLE_DOCUMENTTYPE);
		db.execSQL(CREATE_TABLE_PROVINCE);
		db.execSQL(CREATE_TABLE_MINMAXPRICE);
		db.execSQL(CREATE_TABLE_SYSTEM_SETTING);
		db.execSQL(CREATE_TABLE_WEBCONTENT);
		db.execSQL(CREATE_TABLE_KNPL);
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_BRANCH_NAME);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_CATEGORY_NAME);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_CITY_NAME);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_CONTACTUS_NAME);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_DOCUMENTTYPE_NAME);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_MINMAXPRICE_NAME);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_PROVINCE_NAME);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_SYSTEM_SETTING_NAME);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_WEBCONTENT_NAME);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_KNPL_NAME);
		onCreate(db);
	}

}
