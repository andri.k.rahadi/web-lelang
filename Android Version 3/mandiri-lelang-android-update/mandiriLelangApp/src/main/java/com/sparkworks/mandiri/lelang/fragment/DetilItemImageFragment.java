package com.sparkworks.mandiri.lelang.fragment;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.base.BaseFragment;
import com.squareup.picasso.Picasso;

public class DetilItemImageFragment extends BaseFragment{
	public static String FRAGMENT_TAG = "DetilItemImageFragment";
	private PhotoView imgDetilItem;
	
	public static String KEY_URL = "url";
	
	private String url;
	
	PhotoViewAttacher photoViewAttacher;
	
	public static DetilItemImageFragment newInstance(String imageUrl) {
        final DetilItemImageFragment f = new DetilItemImageFragment();
        final Bundle args = new Bundle();
        args.putString(KEY_URL, imageUrl);
        f.setArguments(args);
        return f;
    }
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		if (savedInstanceState!=null) {
			url = savedInstanceState.getString(KEY_URL);
		} else {
			url = getArguments() != null ? getArguments().getString(KEY_URL) : "";
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_detil_photo_pager, container, false);
		initializeViews(view);
		return view;
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public void initializeViews(View view) {
		// TODO Auto-generated method stub
		super.initializeViews(view);
		
		imgDetilItem = (PhotoView)view.findViewById(R.id.img_detil_photo);
			
		trustEveryone();
		
		Picasso.with(getActivity()).load(url).into(imgDetilItem);
		photoViewAttacher = new PhotoViewAttacher(imgDetilItem);
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		photoViewAttacher.cleanup();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putString(KEY_URL, url);
	}
	
	private void trustEveryone() {
		try {
			HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
	    			public boolean verify(String hostname, SSLSession session) {
	    				return true;
	    			}});
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, new X509TrustManager[]{new X509TrustManager(){
				public void checkClientTrusted(X509Certificate[] chain,
						String authType) throws CertificateException {}
				public void checkServerTrusted(X509Certificate[] chain,
						String authType) throws CertificateException {}
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[0];
				}}}, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(
					context.getSocketFactory());
		} catch (Exception e) { // should never happen
			e.printStackTrace();
		}
	}
}
