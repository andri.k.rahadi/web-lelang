package com.sparkworks.mandiri.lelang.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.astuetz.PagerSlidingTabStrip;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.adapter.HomeFragmentAdapter;
import com.sparkworks.mandiri.lelang.base.BaseFragment;

public class HomeFragment extends BaseFragment {
	public static String FRAGMENT_TAG = "HomeFragment";
	private PagerSlidingTabStrip tabPageIndicator;
	private ViewPager pager;
	
	public HomeFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
         initializeViews(rootView);
         getActivity().getActionBar().setDisplayUseLogoEnabled(false);
         getActivity().getActionBar().setDisplayShowHomeEnabled(true);
         getActivity().getActionBar().setIcon(R.drawable.logo_home);
         getActivity().getActionBar().setTitle("");
         getActivity().getActionBar().setHomeButtonEnabled(true);

        return rootView;
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		initializeProcess();
	}
	
	@Override
	public void initializeViews(View view) {
		// TODO Auto-generated method stub
		super.initializeViews(view);
		 tabPageIndicator = (PagerSlidingTabStrip)view.findViewById(R.id.tabs);
         pager = (ViewPager)view.findViewById(R.id.pager);
         final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
 				.getDisplayMetrics());
 		pager.setPageMargin(pageMargin);
	}
	
	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		super.initializeProcess();
		HomeFragmentAdapter homePagerAdapter = new HomeFragmentAdapter(getChildFragmentManager());
		pager.setAdapter(homePagerAdapter);
		tabPageIndicator.setViewPager(pager);
	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		// TODO Auto-generated method stub
		super.onHiddenChanged(hidden);
		
		getActivity().getActionBar().setTitle("Home");
	}
}