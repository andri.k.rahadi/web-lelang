package com.sparkworks.mandiri.lelang.request.api;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.request.response.LoginResponse;
import com.sparkworks.mandiri.lelang.utils.URLs;
import com.sparkworks.mandiri.lelang.utils.Utils;

import org.apache.http.Header;

import java.util.HashMap;

/**
 * Created by sidiqpermana on 11/24/16.
 */

public class PostLoginRequest extends BaseApi implements FormValidation{
    private String email, password;
    private OnLoginRequestListener onLoginRequestListener;
    private Context context;
    private AsyncHttpClient asyncHttpClient;

    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_PASSWORD = "password";

    public static final String ERROR_PARAM_EMAIL = "error_param_email";
    public static final String ERROR_PARAM_PASSWORD = "error_param_password";

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public OnLoginRequestListener getOnLoginRequestListener() {
        return onLoginRequestListener;
    }

    public void setOnLoginRequestListener(OnLoginRequestListener onLoginRequestListener) {
        this.onLoginRequestListener = onLoginRequestListener;
    }


    public PostLoginRequest() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean isValidParams() {
        boolean isValid = true;
        if (TextUtils.isEmpty(getPassword())){
            isValid = false;
            invalidParams.put(ERROR_PARAM_PASSWORD, PARAM_PASSWORD);
        }

        if (!Utils.validEmail(getEmail())){
            isValid = false;
            invalidParams.put(ERROR_PARAM_EMAIL, PARAM_EMAIL);
        }

        return isValid;
    }

    @Override
    public void callApi() {
        super.callApi();
        if (isValidParams()){
            if (Utils.isConnectInet(getContext())){
                RequestParams params = new RequestParams();
                params.put(PARAM_EMAIL, getEmail());
                params.put(PARAM_PASSWORD, getPassword());

                asyncHttpClient = getAsyncHttpClient(getUserPreference(getContext()).getToken());
                asyncHttpClient.post(URLs.SERVER_LOGIN, params, new AsyncHttpResponseHandler(){
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        super.onSuccess(statusCode, headers, responseBody);

                        Gson gson = new Gson();
                        LoginResponse loginResponse = gson.fromJson(new String(responseBody), LoginResponse.class);
                        if (loginResponse.getResultCode().getCode() == CODE_SUCCESS){
                            getOnLoginRequestListener().onLoginRequestSuccess(loginResponse);
                        }else{
                            getOnLoginRequestListener().onLoginRequestFailed(loginResponse.getResultCode().getMessage());
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            super.onFailure(statusCode, headers, responseBody, error);
                        getOnLoginRequestListener().onLoginRequestFailed("Error login");
                    }
                });
            }else{
                getOnLoginRequestListener().onLoginRequestFailed(getContext().getString(R.string.no_internet));
            }
        }else{
            getOnLoginRequestListener().onParamsInvalid(invalidParams);
        }
    }

    @Override
    public void cancel() {
        super.cancel();
        asyncHttpClient.cancelRequests(getContext(), true);
    }

    @Override
    public void retry() {
        super.retry();
        callApi();
    }

    public interface OnLoginRequestListener extends BaseListener{
        void onLoginRequestSuccess(LoginResponse response);
        void onLoginRequestFailed(String message);
    }
}
