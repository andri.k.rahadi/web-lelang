package com.sparkworks.mandiri.lelang.database.helper;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.sparkworks.mandiri.lelang.database.DataBaseHelper;
import com.sparkworks.mandiri.lelang.model.MasterData;

public class CityHelper {
	
	public static String DATABASE_TABLE = DataBaseHelper.TABLE_CITY_NAME;
	
	private Context context;
	
	private DataBaseHelper dataBaseHelper;
	
	private SQLiteDatabase database;
	
	public CityHelper(Context context){
		this.context = context;
	}
	
	public CityHelper open() throws SQLException{
		dataBaseHelper = new DataBaseHelper(context);
		database = dataBaseHelper.getWritableDatabase();
		return this;
		
	}
	
	public void close(){
		dataBaseHelper.close();
	}
	
	public Cursor queryCity(){
		
		return database.rawQuery("SELECT * FROM "+DATABASE_TABLE+
				" WHERE "+DataBaseHelper.FIELD_CITY_IS_ACTIVE+"='1' ORDER BY "+DataBaseHelper.FIELD_CITY_NAME+" ASC", null);
	}
	
	public ArrayList<MasterData> loadAllData(){
		ArrayList<MasterData> arrayList = new ArrayList<MasterData>();
		Cursor cursor = queryCity();
		//startManagingCursor(cursor);
		cursor.moveToFirst();
		MasterData masterData;
		if (cursor.getCount()>0) {
			do {
				
				masterData = new MasterData();
				masterData.set_id(cursor.getInt(0));
				masterData.setId(cursor.getString(1));
				masterData.setName(cursor.getString(2));
				masterData.setDescription(cursor.getString(3));
				masterData.setIsActive(cursor.getString(4));
				masterData.setFlag(cursor.getString(5));
				masterData.setProvinceId(cursor.getString(6));
				
				arrayList.add(masterData);
				cursor.moveToNext();
				
			} while (!cursor.isAfterLast());
		} 
		cursor.close();
		return arrayList;
	}

	
	public boolean isCityExist(String id){
		Cursor cursor = database.rawQuery("SELECT "+DataBaseHelper.FIELD_CITY_ID+" FROM "+DATABASE_TABLE+" WHERE "+
				DataBaseHelper.FIELD_CITY_ID+"='"+id+"'", null);
		boolean exists = (cursor.getCount()>0);
		cursor.close();
		return exists;
	}
	
	public Cursor queryAllCityByProvince(String provinceId){
		
		return database.rawQuery("SELECT * FROM "+DATABASE_TABLE+" " +
				"WHERE " + DataBaseHelper.FIELD_CITY_PROVINCE_ID+"="+provinceId+" "+
				"ORDER BY "+DataBaseHelper.FIELD_CITY_NAME+" ASC", null);
	}
	
	public ArrayList<MasterData> loadAllCityByProvince(String provinceId){
		ArrayList<MasterData> arrayList = new ArrayList<MasterData>();
		Cursor cursor = queryAllCityByProvince(provinceId);
		//startManagingCursor(cursor);
		cursor.moveToFirst();
		MasterData masterData;
		if (cursor.getCount()>0) {
			do {
				
				masterData = new MasterData();
				masterData.set_id(cursor.getInt(0));
				masterData.setId(cursor.getString(1));
				masterData.setName(cursor.getString(2));
				masterData.setDescription(cursor.getString(3));
				masterData.setIsActive(cursor.getString(4));
				masterData.setFlag(cursor.getString(5));
				masterData.setProvinceId(cursor.getString(6));
				
				arrayList.add(masterData);
				cursor.moveToNext();
				
			} while (!cursor.isAfterLast());
		} 
		cursor.close();
		return arrayList;
	}
	
	public Cursor querySearchCityByKeyword(String keyword){
		
		return database.rawQuery("SELECT * FROM "+DATABASE_TABLE+" " +
				"WHERE " + DataBaseHelper.FIELD_CITY_NAME+" LIKE '%"+keyword+"%' "+
				"ORDER BY "+DataBaseHelper.FIELD_CITY_NAME+" ASC", null);
	}
	
	public ArrayList<MasterData> loadAllCityByKeyword(String keyword){
		ArrayList<MasterData> arrayList = new ArrayList<MasterData>();
		Cursor cursor = querySearchCityByKeyword(keyword);
		//startManagingCursor(cursor);
		cursor.moveToFirst();
		MasterData masterData;
		if (cursor.getCount()>0) {
			do {
				
				masterData = new MasterData();
				masterData.set_id(cursor.getInt(0));
				masterData.setId(cursor.getString(1));
				masterData.setName(cursor.getString(2));
				masterData.setDescription(cursor.getString(3));
				masterData.setIsActive(cursor.getString(4));
				masterData.setFlag(cursor.getString(5));
				masterData.setProvinceId(cursor.getString(6));
				
				arrayList.add(masterData);
				cursor.moveToNext();
				
			} while (!cursor.isAfterLast());
		} 
		cursor.close();
		return arrayList;
	}
	
	public long insert(String id, String name, String description, String isActive,
			String flag, String provinceId){
		ContentValues initialValues = createContent(id, name, description, isActive, flag, 
				provinceId);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}
	
	private ContentValues createContent(String id, String name, String description,
			String isActive, String flag, String provinceId){
		ContentValues values = new ContentValues();
		values.put(DataBaseHelper.FIELD_CITY_ID, id);
		values.put(DataBaseHelper.FIELD_CITY_NAME, name);
		values.put(DataBaseHelper.FIELD_CITY_DESCRIPTION, description);
		values.put(DataBaseHelper.FIELD_CITY_IS_ACTIVE, isActive);
		values.put(DataBaseHelper.FIELD_CITY_FLAG, flag);
		values.put(DataBaseHelper.FIELD_CITY_PROVINCE_ID, provinceId);
		
		return values;
	}
	
	public void update(String id, String name, String description, String isActive, 
			String flag, String provinceId){
		ContentValues args = new ContentValues();
	    args.put(DataBaseHelper.FIELD_CITY_ID, id);
	    args.put(DataBaseHelper.FIELD_CITY_NAME, name);
	    args.put(DataBaseHelper.FIELD_CITY_DESCRIPTION, description);
	    args.put(DataBaseHelper.FIELD_CITY_IS_ACTIVE, isActive);
	    args.put(DataBaseHelper.FIELD_CITY_FLAG, flag);
	    args.put(DataBaseHelper.FIELD_CITY_PROVINCE_ID, provinceId);
	    database.update(DATABASE_TABLE, args, DataBaseHelper.FIELD_CITY_ID + "=" + id, null);
	}
	
	public void hapusKata(int id){
		database.delete(DataBaseHelper.TABLE_CITY_NAME, "_id = '"+id+"'", null);
	}

	public Cursor queryCityById(int cityId){

		return database.rawQuery("SELECT * FROM "+DATABASE_TABLE+
				" WHERE "+DataBaseHelper.FIELD_CITY_IS_ACTIVE+"='1' AND "+DataBaseHelper.FIELD_CITY_ID+"='"+cityId+"' ORDER BY "+DataBaseHelper.FIELD_CITY_NAME+" ASC", null);
	}

	public ArrayList<MasterData> loadCityById(int cityId){
		ArrayList<MasterData> arrayList = new ArrayList<MasterData>();
		Cursor cursor = queryCityById(cityId);
		//startManagingCursor(cursor);
		cursor.moveToFirst();
		MasterData masterData;
		if (cursor.getCount()>0) {
			do {

				masterData = new MasterData();
				masterData.set_id(cursor.getInt(0));
				masterData.setId(cursor.getString(1));
				masterData.setName(cursor.getString(2));
				masterData.setDescription(cursor.getString(3));
				masterData.setIsActive(cursor.getString(4));
				masterData.setFlag(cursor.getString(5));
				masterData.setProvinceId(cursor.getString(6));

				arrayList.add(masterData);
				cursor.moveToNext();

			} while (!cursor.isAfterLast());
		}
		cursor.close();
		return arrayList;
	}
}
