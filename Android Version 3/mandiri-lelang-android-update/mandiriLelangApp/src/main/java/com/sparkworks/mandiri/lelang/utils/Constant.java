package com.sparkworks.mandiri.lelang.utils;

public class Constant {
	
	public static final String SECRET = "my1Mand1r1";
	public static final String PRIVATE = "86342850c8fbf6219ea754dfb37ab97fb27881daa391a3590d8c81430671d70e";
	public static final String APP_TAG = "MandiriLelangApp";
	public static final String YOUTUBE_KEY = "AIzaSyCGT8PINjFU0VwtKUjPUHG_E7wkTzc65CM";
	
	public static final String CALL = "call";
	
	public static final String DATA = "data";
	public static final String ITEM_ASSET_ID = "assetID";
	public static final String ITEM_ASSET_CODE = "assetCode";
	public static final String ITEM_POSTED_DATE = "postedDate";
	public static final String ITEM_POSTED_FORMATTED = "postedDateFormattedDateTime";
	public static final String ITEM_AUCTION_DATE = "auctionDate";
	public static final String ITEM_AUCTION_DATE_FORMATTED = "auctionDateFormattedDateTime";
	public static final String ITEM_CATEGORY_ID = "categoryID";
	public static final String ITEM_CATEGORY_NAME = "categoryName";
	public static final String ITEM_ADDRESS = "address";
	public static final String ITEM_PROVINCE_ID = "provinceID";
	public static final String ITEM_PROVINCE_NAME = "provinceName";
	public static final String ITEM_CITY_ID = "cityID";
	public static final String ITEM_CITY_NAME = "cityName";
	public static final String ITEM_COMPLETE_ADDRESS = "completeAddress";
	public static final String ITEM_OLD_PRICE = "oldPrice";
	public static final String ITEM_NEW_PRICE = "newPrice";
	public static final String ITEM_PHOTO_URL = "defaultPhotoURL";
	public static final String ITEM_DOCUMENT_TYPE_ID = "documentTypeID";
	public static final String ITEM_DOCUMENT_TYPE_NAME = "documentTypeName";
	public static final String ITEM_BRANCH_ID = "branchID";
	public static final String ITEM_BRANCH_NAME = "branchName";
	public static final String ITEM_BRANCH_ADDRESS = "branchAddress";
	public static final String ITEM_BRANCH_PHONE_1 = "branchPhone1";
	public static final String ITEM_BRANCH_PHONE_2 = "branchPhone2";
	public static final String ITEM_KPKNL_ID = "kpknlID";
	public static final String ITEM_KPKNL_NAME = "kpknlName";
	public static final String ITEM_KPKNL_ADDRESS = "kpknlAddress";
	public static final String ITEM_KPKNL_PHONE1 = "kpknlPhone1";
	public static final String ITEM_KPKNL_PHONE2 = "kpknlPhone2";
	public static final String ITEM_AUCTION_HALL_ID = "auctionHallID";
	public static final String ITEM_AUCTION_HALL_NAME = "auctionHallName";
	public static final String ITEM_AUCTION_HALL_ADDRESS = "auctionHallAddress";
	public static final String ITEM_AUCTION_HALL_PHONE1 = "auctionHallPhone1";
	public static final String ITEM_AUCTION_HALL_PHONE2 = "auctionHallPhone2";
	public static final String ITEM_TRANSFERINFORMATION = "transferInformation";
	public static final String ITEM_LATITUDE = "latitude";
	public static final String ITEM_LONGITUDE = "longitude";
	public static final String ITEM_PHOTO1URL = "photo1URL";
	public static final String ITEM_PHOTO2URL = "photo2URL";
	public static final String ITEM_PHOTO3URL = "photo3URL";
	public static final String ITEM_PHOTO4URL = "photo4URL";
	public static final String ITEM_PHOTO5URL = "photo5URL";
	public static final String ITEM_EXTRAFIELDS = "extraFields";
	public static final String ITEM_EXTRAFIELDS_FIELDNAME = "fieldName";
	public static final String ITEM_EXTRAFIELDS_VALUE = "value";
	public static final String ITEM_ISHOT_PRICE = "isHotPrice";
	public static final String ITEM_IS_EMAIL_AUCTION = "isEmailAuction";
	public static final String ITEM_EMAIL_AUCTION_URL = "emailAuctionURL";
	public static final String ITEM_IS_FAVORITE = "isFavorite";
	public static final String ITEM_TOTAL_RECORDS = "totalRecords";
	public static final String ITEM_TERMS = "Terms";
	public static final String ITEM_URL = "url";
	public static final String ITEM_WEB_URL = "webUrl";
	public static final String ITEM_TWITTER_TEXT = "TwitterText";
	public static final String ITEM_EMAIL_TEXT = "EmailText";
	public static final String ITEM_EMAIL_SUBJECT = "EmailSubject";
	public static final String ITEM_TYPE_1 = "type1";
	public static final String ITEM_TYPE_2 = "type2";
	public static final String ITEM_TYPE = "type";

	public static final int TYPE_JUAL_SUKARELA = 1;
	public static final int TYPE_LELANG = 2;
	
	public static final String AUCTION_SCHEDULE_ID = "auctionScheduleID";
	
	public static final String RESULT = "resultCode";
	public static final String RESULT_CODE = "code";
	public static final String RESULT_MESSAGE = "message";
	
	public static final String MASTER_DATA = "masterData";
	public static final String MASTER_DATA_TYPE = "masterDataType";
	public static final String MASTER_DATA_CONTENT = "data";
	public static final String MASTER_DATA_ID = "id";
	public static final String MASTER_DATA_NAME = "name";
	public static final String MASTER_DATA_DESCRIPTION = "description";
	public static final String MASTER_DATA_ISACTIVE = "isActive";
	public static final String MASTER_DATA_FLAG = "flag";
	public static final String MASTER_DATA_VALUE = "value";
	public static final String MASTER_DATA_PROVINCE_ID = "provinceID";
	public static final String MASTER_DATA_LASTTIMESTAMP = "lastTimestamp";
	public static final String MASTER_DATA_TOKEN = "token";
	public static final String MASTER_DATA_IS_TOKEN_EXPIRED = "isTokenExpired";
	public static final String MASTER_DATA_RESULT_CODE = "resultCode";
	public static final String MASTER_DATA_CODE = "code";
	public static final String MASTER_DATA_MESSAGE = "message";
	
	
	public static final String MASTER_DATA_CATEGORY = "Category";
	public static final String MASTER_DATA_PROVINCE = "Province";
	public static final String MASTER_DATA_CITY = "City";
	public static final String MASTER_DATA_MIN_MAX_PRICE = "MinMaxPrice";
	public static final String MASTER_DATA_DOCUMENT_TYPE = "DocumentType";
	public static final String MASTER_DATA_CONTACT_US_TYPE = "ContactUsType";
	public static final String MASTER_DATA_CONTACT_BRANCH = "Branch";
	public static final String MASTER_DATA_WEBCONTENT = "WebContent";
	public static final String MASTER_DATA_KPKNL = "KPKNL";
	public static final String MASTER_DATA_SYSTEM_SETTINGS = "SystemSetting";
	
	public static final String CONTACT_US_MESSAGE = "thankYouMessage";

	//SYSTEM SETTINGS IDS
	public static final String ID_MANDATORY_FOR_MEMBER_ACCESS = "MandatoryForMemberAddress";
	public static final String ID_MANDATORY_FOR_MEMBER_CITY = "MandatoryForMemberCity";
	public static final String ID_MANDATORY_FOR_MEMBER_DATE_OF_BIRTH = "MandatoryForMemberDateOfBirth";
	public static final String ID_MANDATORY_FOR_MEMBER_IDENTIFICATION_NUMBER = "MandatoryForMemberIdentificationNumber";
	public static final String ID_MANDATORY_FOR_MEMBER_PHONE_NUMBER = "MandatoryForMemberMobilePhoneNumber";
	public static final String ID_MANDATORY_FOR_MEMBER_PLACE_OF_BIRTH = "MandatoryForMemberPlaceOfBirth";
	public static final String ID_MANDATORY_FOR_MEMBER_PREFERENCE = "MandatoryForMemberPreference";
	public static final String ID_MANDATORY_FOR_MEMBER_PROVINCE = "MandatoryForMemberProvince";
	public static final String ID_MOBILE_NOTIFICATION_SYNC_PERIOD = "MobileNotificationSyncPeriod";
}
