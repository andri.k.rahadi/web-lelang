package com.sparkworks.mandiri.lelang.model;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ghiyats on 12/20/2015.
 */
public class FacebookProfile {

    @SerializedName("last_name")
    private String lastName;
    @SerializedName("id")
    private String id;
    @SerializedName("gender")
    private String gender;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("email")
    private String email;
    @SerializedName("user_birthday")
    private String birthday;
    @SerializedName("age_range")
    private String ageRange;
    @SerializedName("link")
    private String link;

    private String profilePicture;
    private String facebookToken;


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(String ageRange) {
        this.ageRange = ageRange;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getName() {
        return getFirstName() + " " + getLastName();
    }

    public String getFacebookToken() {
        return facebookToken;
    }

    public void setFacebookToken(String facebookToken) {
        this.facebookToken = facebookToken;
    }

    @Override
    public String toString() {
        return "FacebookProfile{" +
                "lastName='" + lastName + '\'' +
                ", id='" + id + '\'' +
                ", gender='" + gender + '\'' +
                ", firstName='" + firstName + '\'' +
                ", email='" + email + '\'' +
                ", birthday='" + birthday + '\'' +
                ", ageRange='" + ageRange + '\'' +
                ", link='" + link + '\'' +
                ", profilePicture='" + profilePicture + '\'' +
                '}';
    }

    public static FacebookProfile parseResponse(String response) {
        Gson gson = new Gson();
        FacebookProfile profile = gson.fromJson(response, FacebookProfile.class);
        profile.setProfilePicture("https://graph.facebook.com/" + profile.getId() + "/picture?width=200&height=200");
        Log.d("Facebook", profile.toString());
        return profile;
    }
}
