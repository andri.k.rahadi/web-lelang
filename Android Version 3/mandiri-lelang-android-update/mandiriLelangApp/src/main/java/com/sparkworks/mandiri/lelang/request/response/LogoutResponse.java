package com.sparkworks.mandiri.lelang.request.response;

import com.google.gson.annotations.SerializedName;
import com.sparkworks.mandiri.lelang.request.model.LogoutData;

/**
 * Created by sidiqpermana on 11/29/16.
 */

public class LogoutResponse extends BaseResponse {
    @SerializedName("data")
    private LogoutData data;

    public LogoutData getData() {
        return data;
    }

    public void setData(LogoutData data) {
        this.data = data;
    }
}
