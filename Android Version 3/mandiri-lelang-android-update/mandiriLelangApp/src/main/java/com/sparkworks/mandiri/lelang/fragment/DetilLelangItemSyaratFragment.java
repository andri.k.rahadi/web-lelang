package com.sparkworks.mandiri.lelang.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.base.BaseFragment;

public class DetilLelangItemSyaratFragment extends BaseFragment{
	public static String FRAGMENT_TAG = "DetilLelangItemSyaratFragment";
	private TextView txtSyaratKetentuan;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_detil_lelang_syarat, container, false);
		initializeViews(view);
		return view;
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initializeProcess();
	}
	
	@Override
	public void initializeViews(View view) {
		// TODO Auto-generated method stub
		super.initializeViews(view);
		
		txtSyaratKetentuan = (TextView)view.findViewById(R.id.txt_detil_lelang_syarat);
	}
	
	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		super.initializeProcess();
		
		String syaratKetentuan = "<p>1. Lokasi yang ditunjukan dalam google map atau peta hanya merupakan alat bantu perkiraan lokasi agunan.</p>" +
				"<p>2. Harga limit lelang yang tercantum hanya berlaku pada tanggal pelaksanaan lelang tersebut di atas. </p>" +
				"<p>3. Apabila terdapat perbedaan informasi yang terdapat dalam website dengan pengumuman lelang di media massa, maka yang digunakan adalah pengumuman lelang dalam media massa yang berlaku untuk pelaksanaan lelang tersebut.</p>" +
				"<p>4. Agunan dijual secara apa adanya (as is) sesuai kondisi pada saat pelaksanaan lelang</p>";
		
		txtSyaratKetentuan.setText(Html.fromHtml(syaratKetentuan));
	}
}
