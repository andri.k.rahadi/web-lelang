package com.sparkworks.mandiri.lelang.model.response;

import com.google.gson.annotations.SerializedName;
import com.sparkworks.mandiri.lelang.model.Video;

import java.util.ArrayList;

/**
 * Created by sidiqpermana on 9/24/17.
 */

public class VideoResponse extends BaseResponse{
    @SerializedName("data")
    private ArrayList<Video> videos = new ArrayList<>();

    public ArrayList<Video> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<Video> videos) {
        this.videos = videos;
    }
}
