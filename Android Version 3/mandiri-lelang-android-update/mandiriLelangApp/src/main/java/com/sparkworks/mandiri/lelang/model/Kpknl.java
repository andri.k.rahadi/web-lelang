package com.sparkworks.mandiri.lelang.model;

import java.io.Serializable;

public class Kpknl implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6348759778682420811L;
	String id, name, address, phone1, phone2;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
}
