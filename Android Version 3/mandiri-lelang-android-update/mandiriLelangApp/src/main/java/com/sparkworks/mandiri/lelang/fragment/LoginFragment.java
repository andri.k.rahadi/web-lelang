package com.sparkworks.mandiri.lelang.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.mandiri.lelang.aplikasi.ForgotPasswordActivity;
import com.mandiri.lelang.aplikasi.MainActivity;
import com.mandiri.lelang.aplikasi.ProfileActivity;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.base.BaseFragment;
import com.sparkworks.mandiri.lelang.model.FacebookProfile;
import com.sparkworks.mandiri.lelang.request.api.PostFacebookRequest;
import com.sparkworks.mandiri.lelang.request.api.PostLoginRequest;
import com.sparkworks.mandiri.lelang.request.model.FacebookData;
import com.sparkworks.mandiri.lelang.request.response.LoginResponse;
import com.sparkworks.mandiri.lelang.request.response.PostFacebookResponse;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends BaseFragment
        implements View.OnClickListener,
        PostLoginRequest.OnLoginRequestListener,
        PostFacebookRequest.OnPostFacebookRequestListener,
        CompoundButton.OnCheckedChangeListener{
    private Button btnLogin, btnFblogin;
    private EditText edtEmail, edtPassword;
    private LoginButton btnFbLoginButton;
    private CheckBox cbPassword;
    private boolean isFacebookLogin = false;

    private CallbackManager callbackManager;
    private PostFacebookRequest postFacebookRequest;
    private PostLoginRequest postLoginRequest;

    TextView tvForgotPassword;
    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        initializeViews(view);
        return view;
    }

    @Override
    public void initializeViews(View view) {
        super.initializeViews(view);
        btnLogin = (Button)view.findViewById(R.id.btn_login);
        tvForgotPassword = (TextView)view.findViewById(R.id.tv_forgot_password);
        btnLogin.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        edtEmail = (EditText)view.findViewById(R.id.edt_email);
        edtPassword = (EditText)view.findViewById(R.id.edt_password);
        btnFblogin = (Button)view.findViewById(R.id.btn_login_fb);
        btnFblogin.setOnClickListener(this);
        btnFbLoginButton = (LoginButton)view.findViewById(R.id.btn_fb_loginButton);
        btnFbLoginButton.setReadPermissions(Arrays.asList("email"));
        btnFbLoginButton.setOnClickListener(this);
        btnFbLoginButton.setFragment(this);
        cbPassword = (CheckBox)view.findViewById(R.id.cb_password);
        cbPassword.setOnCheckedChangeListener(this);

        tvForgotPassword.setText(Html.fromHtml("<u>Lupa Password?</u>"));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();

        postFacebookRequest = new PostFacebookRequest();
        postFacebookRequest.setContext(getActivity());
        postFacebookRequest.setOnPostFacebookRequestListener(this);

        btnFbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                final AccessToken accessToken = loginResult.getAccessToken();
                getFacebookUserProfile(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        FacebookProfile facebookProfile = FacebookProfile.parseResponse(object.toString());
                        facebookProfile.setFacebookToken(accessToken.getToken());

                        String firstName = !TextUtils.isEmpty(facebookProfile.getFirstName()) ? facebookProfile.getFirstName() : "";
                        String lastName = !TextUtils.isEmpty(facebookProfile.getLastName()) ? facebookProfile.getLastName() : "";
                        String name = firstName +" "+ lastName;

                        FacebookData facebookData = new FacebookData();
                        facebookData.setEmail(facebookProfile.getEmail());
                        facebookData.setFullName(name.trim());
                        facebookData.setId(facebookProfile.getId());
                        facebookData.setToken(accessToken.getToken());
                        facebookData.setUrl(facebookProfile.getLink());
                        postFacebookRequest.setData(facebookData);

                        showProgressDialog("Login", getString(R.string.general_progress_dialog_message));

                        postFacebookRequest.callApi();
                    }
                });
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        postLoginRequest = new PostLoginRequest();
        postLoginRequest.setOnLoginRequestListener(this);
        postLoginRequest.setContext(getActivity());
    }

    private void  getFacebookUserProfile(AccessToken accessToken, final GraphRequest.GraphJSONObjectCallback callback) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                Log.d("Facebook", "onCompleted: " + response.toString());
                callback.onCompleted(object, response);
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,last_name,email,gender,location,link,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_login:
                postLoginRequest.setEmail(edtEmail.getText().toString().trim());
                postLoginRequest.setPassword(edtPassword.getText().toString().trim());

                showProgressDialog(getString(R.string.process_login), getString(R.string.general_progress_dialog_message));
                postLoginRequest.callApi();
                break;

            case R.id.btn_login_fb:
                btnFbLoginButton.performClick();
                break;

            case R.id.tv_forgot_password:
                ForgotPasswordActivity.start(getActivity());
                break;
        }
    }

    @Override
    public void onLoginRequestSuccess(LoginResponse response) {
        Log.d("Login", "Success : email "+response.getLoginData().getEmail());
        dismissProgressDialog();

        userPreference.setEmail(response.getLoginData().getEmail());
        userPreference.setFullname(response.getLoginData().getFullName());
        userPreference.setMemberId(response.getLoginData().getMemberId());
        userPreference.setToken(response.getLoginData().getToken());

        if (response.getLoginData().getIsProfileComplete()){
            MainActivity.toMainActivity(getActivity());
        }else{
            ProfileActivity.start(getActivity(), RegisterFragment.SOURCE_LOGIN, response.getLoginData());
        }

        getActivity().finish();
    }

    @Override
    public void onLoginRequestFailed(String message) {
        Log.d("Login", "Failed :  "+message);
        dismissProgressDialog();
        showToast(message);
    }

    @Override
    public void onParamsInvalid(HashMap<String, String> invalidParams) {
        dismissProgressDialog();
        if (invalidParams.get(PostLoginRequest.ERROR_PARAM_EMAIL).equalsIgnoreCase(PostLoginRequest.PARAM_EMAIL)){
            edtEmail.setError(getString(R.string.error_email_empty));
        }

        if (invalidParams.get(PostLoginRequest.ERROR_PARAM_PASSWORD).equalsIgnoreCase(PostLoginRequest.PARAM_PASSWORD)){
            edtPassword.setError(getString(R.string.error_password_empty));
        }
    }

    @Override
    public void onPostFacebookSuccess(PostFacebookResponse response) {
        dismissProgressDialog();

        userPreference.setEmail(response.getLoginData().getEmail());
        userPreference.setFullname(response.getLoginData().getFullName());
        userPreference.setMemberId(response.getLoginData().getMemberId());
        userPreference.setToken(response.getLoginData().getToken());

        if (response.getLoginData().getIsProfileComplete()){
            MainActivity.toMainActivity(getActivity());
        }else{
            ProfileActivity.start(getActivity(), RegisterFragment.SOURCE_LOGIN, response.getLoginData(), true);
        }

        getActivity().finish();
    }

    @Override
    public void onPostFacebookFailed(String errorMessage) {
        dismissProgressDialog();
        Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (compoundButton.isChecked()){
            edtPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        }else{
            edtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
        edtPassword.setSelection(edtPassword.length());
    }
}
