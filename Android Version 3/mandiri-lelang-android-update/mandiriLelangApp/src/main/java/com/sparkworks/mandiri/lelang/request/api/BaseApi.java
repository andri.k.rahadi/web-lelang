package com.sparkworks.mandiri.lelang.request.api;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.SyncHttpClient;
import com.sparkworks.mandiri.lelang.utils.AppPreference;
import com.sparkworks.mandiri.lelang.utils.UserPreference;

import java.util.HashMap;

/**
 * Created by sidiqpermana on 11/24/16.
 */

public class BaseApi extends BaseApiMethod {
    public AsyncHttpClient asyncHttpClient;
    public HashMap<String, String> invalidParams = new HashMap<>();
    public UserPreference userPreference;
    public int CODE_SUCCESS = 0;
    public int CODE_FAILED = 1;

    public AsyncHttpClient getAsyncHttpClient(){
        return new AsyncHttpClient();
    }

    public AsyncHttpClient getAsyncHttpClient(String token){
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        asyncHttpClient.setTimeout(60000);
        if (!TextUtils.isEmpty(token)) {
            Log.d("Token", token);
            asyncHttpClient.addHeader("Authorization", "Bearer "+token);
        }else{
            asyncHttpClient.addHeader("Authorization", "Bearer");
        }
        return asyncHttpClient;
    }

    public AsyncHttpClient getAsyncHttpClientWithAppToken(String token){
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        if (!TextUtils.isEmpty(token)) {
            Log.d("Token", token);
            asyncHttpClient.addHeader("Authorization", "Bearer "+token);
        }else{
            asyncHttpClient.addHeader("Authorization", "Bearer");
        }
        return asyncHttpClient;
    }

    public SyncHttpClient getSyncHttpClient(String token){
        SyncHttpClient syncHttpClient = new SyncHttpClient();
        syncHttpClient.addHeader("Authorization", "Bearer "+token);
        return syncHttpClient;
    }

    public UserPreference getUserPreference(Context context){
        return new UserPreference(context);
    }

    public AppPreference getAppPreference(Activity activity){
        return new AppPreference(activity);
    }

    @Override
    public void callApi() {

    }

    @Override
    public void cancel() {

    }

    @Override
    public void retry() {

    }
}


