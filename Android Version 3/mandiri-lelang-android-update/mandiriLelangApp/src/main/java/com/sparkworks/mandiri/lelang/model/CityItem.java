package com.sparkworks.mandiri.lelang.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sidiqpermana on 11/11/16.
 */

public class CityItem implements Parcelable {
    private String id;
    private String name;
    private boolean checked;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeByte(this.checked ? (byte) 1 : (byte) 0);
    }

    public CityItem() {
    }

    protected CityItem(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.checked = in.readByte() != 0;
    }

    public static final Parcelable.Creator<CityItem> CREATOR = new Parcelable.Creator<CityItem>() {
        @Override
        public CityItem createFromParcel(Parcel source) {
            return new CityItem(source);
        }

        @Override
        public CityItem[] newArray(int size) {
            return new CityItem[size];
        }
    };
}
