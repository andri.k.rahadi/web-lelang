package com.sparkworks.mandiri.lelang.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.model.CityItem;

import java.util.ArrayList;

/**
 * Created by sidiqpermana on 11/11/16.
 */

public class CityAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<CityItem> listCity;
    private KpknlAdapter.OnCheckAllCallback onCheckAllCallback;

    public CityAdapter(Activity activity) {
        this.activity = activity;
    }

    public KpknlAdapter.OnCheckAllCallback getOnCheckAllCallback() {
        return onCheckAllCallback;
    }

    public void setOnCheckAllCallback(KpknlAdapter.OnCheckAllCallback onCheckAllCallback) {
        this.onCheckAllCallback = onCheckAllCallback;
    }

    public ArrayList<CityItem> getListCity() {
        return listCity;
    }

    public void setListCity(ArrayList<CityItem> listCity) {
        this.listCity = listCity;
    }

    @Override
    public int getCount() {
        return getListCity().size();
    }

    @Override
    public Object getItem(int i) {
        return getListCity().get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null){
            LayoutInflater inflater = LayoutInflater.from(activity);
            view = inflater.inflate(R.layout.item_province, null);
            final CityAdapter.ViewHolder holder = new CityAdapter.ViewHolder();
            holder.tvName = (TextView)view.findViewById(R.id.tv_item_province);
            holder.cbItem = (CheckBox)view.findViewById(R.id.cb_item_province);
            holder.cbItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    CityItem item = (CityItem) holder.cbItem.getTag();
                    item.setChecked(compoundButton.isChecked());

                    if (!compoundButton.isChecked()){
                        getOnCheckAllCallback().onCheckAll();
                    }
                }
            });
            view.setTag(holder);
            holder.cbItem.setTag(getListCity().get(i));
        }else{
            ((CityAdapter.ViewHolder)view.getTag()).cbItem.setTag(getListCity().get(i));
        }

        CityAdapter.ViewHolder holder = (CityAdapter.ViewHolder)view.getTag();
        holder.tvName.setText(getListCity().get(i).getName());
        holder.cbItem.setChecked(getListCity().get(i).isChecked());

        return view;
    }

    static class ViewHolder{
        CheckBox cbItem;
        TextView tvName;
    }
}
