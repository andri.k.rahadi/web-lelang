package com.sparkworks.mandiri.lelang.request.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sidiqpermana on 11/24/16.
 */

public class LoginData implements Parcelable {
    @SerializedName("token")
    private String token;

    @SerializedName("memberID")
    private int memberId;

    @SerializedName("fullName")
    private String fullName;

    @SerializedName("email")
    private String email;

    @SerializedName("isProfileComplete")
    private boolean isProfileComplete;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getIsProfileComplete() {
        return isProfileComplete;
    }

    public void setIsProfileComplete(boolean isProfileComplete) {
        this.isProfileComplete = isProfileComplete;
    }

    public boolean isProfileComplete() {
        return isProfileComplete;
    }

    public void setProfileComplete(boolean profileComplete) {
        isProfileComplete = profileComplete;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.token);
        dest.writeInt(this.memberId);
        dest.writeString(this.fullName);
        dest.writeString(this.email);
        dest.writeByte(this.isProfileComplete ? (byte) 1 : (byte) 0);
    }

    public LoginData() {
    }

    protected LoginData(Parcel in) {
        this.token = in.readString();
        this.memberId = in.readInt();
        this.fullName = in.readString();
        this.email = in.readString();
        this.isProfileComplete = in.readByte() != 0;
    }

    public static final Parcelable.Creator<LoginData> CREATOR = new Parcelable.Creator<LoginData>() {
        @Override
        public LoginData createFromParcel(Parcel source) {
            return new LoginData(source);
        }

        @Override
        public LoginData[] newArray(int size) {
            return new LoginData[size];
        }
    };
}
