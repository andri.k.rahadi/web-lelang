package com.sparkworks.mandiri.lelang.request.response;

import com.google.gson.annotations.SerializedName;
import com.sparkworks.mandiri.lelang.request.model.LoginData;

/**
 * Created by sidiqpermana on 11/24/16.
 */

public class LoginResponse extends BaseResponse{
    @SerializedName("data")
    private LoginData loginData;

    public LoginData getLoginData() {
        return loginData;
    }

    public void setLoginData(LoginData loginData) {
        this.loginData = loginData;
    }


}
