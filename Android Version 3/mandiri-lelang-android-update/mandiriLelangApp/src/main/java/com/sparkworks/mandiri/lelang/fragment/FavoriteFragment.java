package com.sparkworks.mandiri.lelang.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mandiri.lelang.aplikasi.DetilLelangItemActivity;
import com.mandiri.lelang.aplikasi.LaunchscreenActivity;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.adapter.LelangItemAdapter;
import com.sparkworks.mandiri.lelang.base.BaseFragment;
import com.sparkworks.mandiri.lelang.event.FavoriteEvent;
import com.sparkworks.mandiri.lelang.event.UnfavoriteEvent;
import com.sparkworks.mandiri.lelang.model.Item;
import com.sparkworks.mandiri.lelang.request.api.GetFavoritesRequest;
import com.sparkworks.mandiri.lelang.utils.ApiHelper;
import com.sparkworks.mandiri.lelang.utils.LoadMoreListView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.LinkedList;
import java.util.Locale;

public class FavoriteFragment extends BaseFragment
    implements GetFavoritesRequest.OnGetFavoriteRequestListener,
        AdapterView.OnItemClickListener{
    private LoadMoreListView lvFavorites;
    private ProgressBar progressBar;
    private TextView tvErrorMessage;

    private LelangItemAdapter adapter;
    private int pageIndex = 0;
    private boolean isFirstRequest = false, isLoadmore = false;
    private LinkedList<Item> listFavorites;
    private GetFavoritesRequest getFavoritesRequest;

    public FavoriteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);
        initializeViews(view);
        return view;
    }

    @Override
    public void initializeViews(View view) {
        super.initializeViews(view);
        lvFavorites = (LoadMoreListView)view.findViewById(R.id.lv_favorite);
        lvFavorites.setOnItemClickListener(this);
        tvErrorMessage = (TextView)view.findViewById(R.id.txtErrorMessage);
        progressBar = (ProgressBar)view.findViewById(R.id.pb_indicator);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        EventBus.getDefault().register(this);

        listFavorites = new LinkedList<>();

        getFavoritesRequest = new GetFavoritesRequest();
        getFavoritesRequest.setContext(getContext());
        getFavoritesRequest.setOnGetFavoriteRequestListener(this);

        adapter = new LelangItemAdapter(getActivity(), listFavorites, ApiHelper.LelangType.FAVORITE);
        lvFavorites.setAdapter(adapter);

        isFirstRequest = true;
        request();

        lvFavorites.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                isLoadmore = true;
                request();
            }
        });
    }

    private void request(){
        if (!isLoadmore){
            lvFavorites.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            tvErrorMessage.setVisibility(View.GONE);
        }

        getFavoritesRequest.setPageIndex(pageIndex);
        getFavoritesRequest.callApi();
    }

    @Override
    public void onGetFavoriteRequestSuccess(LinkedList<Item> list) {
        if (list != null){
            if (!isLoadmore){
                lvFavorites.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                tvErrorMessage.setVisibility(View.GONE);

            }

            int currentPos = 0;

            if (isFirstRequest){
                listFavorites.addAll(list);
                isFirstRequest = false;
            }else{
                for (Item item : list){
                    listFavorites.addLast(item);
                }
            }

            adapter.setListItem(listFavorites);
            adapter.notifyDataSetChanged();

            if (isLoadmore){
                currentPos = (pageIndex * 12) - 1;
                lvFavorites.setSelection(currentPos);
                pageIndex += 1;
                isLoadmore = false;
                lvFavorites.onLoadMoreComplete();
            }else{
                lvFavorites.setSelection(0);
                pageIndex += 1;
            }
        }else{
            lvFavorites.onLoadMoreComplete();
        }

    }

    @Override
    public void onGetFavoriteRequestFailed(String errorMessage) {
        if (errorMessage.toUpperCase(Locale.ENGLISH).contains("TOKEN")){
            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
            LaunchscreenActivity.start(getActivity());
            getActivity().finish();
        }else{
            if (!isLoadmore){
                lvFavorites.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                tvErrorMessage.setVisibility(View.VISIBLE);
                tvErrorMessage.setText(errorMessage);
            }

            lvFavorites.onLoadMoreComplete();
        }
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        DetilLelangItemActivity.toDetilLelangActivity(getActivity(), listFavorites.get(i), i, ApiHelper.LelangType.FAVORITE);
    }

    @Subscribe
    public void onEvent(FavoriteEvent event){
        listFavorites.add(event.getUpdatedItem());
        adapter.setListItem(listFavorites);
        adapter.notifyDataSetChanged();
    }

    @Subscribe
    public void onEvent(UnfavoriteEvent event){
        for (Item item : listFavorites){
            if (Integer.parseInt(item.getAssetId()) == event.getAssetId()){
                listFavorites.remove(item);
                break;
            }
        }

        adapter.setListItem(listFavorites);
        adapter.notifyDataSetChanged();
    }
}
