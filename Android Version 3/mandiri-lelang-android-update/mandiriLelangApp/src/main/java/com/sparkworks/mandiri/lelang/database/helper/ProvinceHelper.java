package com.sparkworks.mandiri.lelang.database.helper;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.sparkworks.mandiri.lelang.database.DataBaseHelper;
import com.sparkworks.mandiri.lelang.model.MasterData;

public class ProvinceHelper {
	
	public static String DATABASE_TABLE = DataBaseHelper.TABLE_PROVINCE_NAME;
	
	private Context context;
	
	private DataBaseHelper dataBaseHelper;
	
	private SQLiteDatabase database;
	
	public ProvinceHelper(Context context){
		this.context = context;
	}
	
	public ProvinceHelper open() throws SQLException{
		dataBaseHelper = new DataBaseHelper(context);
		database = dataBaseHelper.getWritableDatabase();
		return this;
		
	}
	
	public void close(){
		dataBaseHelper.close();
	}
	
	public Cursor queryProvince(){
		
		return database.rawQuery("SELECT * FROM "+DATABASE_TABLE+
				" WHERE "+DataBaseHelper.FIELD_PROVINCE_IS_ACTIVE+"='1' ORDER BY "+DataBaseHelper.FIELD_PROVINCE_NAME+" ASC", null);
	}
	
	public ArrayList<MasterData> loadAllData(){
		ArrayList<MasterData> arrayList = new ArrayList<MasterData>();
		Cursor cursor = queryProvince();
		//startManagingCursor(cursor);
		cursor.moveToFirst();
		MasterData masterData;
		if (cursor.getCount()>0) {
			do {
				
				masterData = new MasterData();
				masterData.set_id(cursor.getInt(0));
				masterData.setId(cursor.getString(1));
				masterData.setName(cursor.getString(2));
				masterData.setDescription(cursor.getString(3));
				masterData.setIsActive(cursor.getString(4));
				masterData.setFlag(cursor.getString(5));
				
				arrayList.add(masterData);
				cursor.moveToNext();
				
			} while (!cursor.isAfterLast());
		} 
		cursor.close();
		return arrayList;
	}
	
	public boolean isProvinceExist(String id){
		Cursor cursor = database.rawQuery("SELECT "+DataBaseHelper.FIELD_PROVINCE_ID+" FROM "+DATABASE_TABLE+" WHERE "+
				DataBaseHelper.FIELD_PROVINCE_ID+"='"+id+"'", null);
		boolean exists = (cursor.getCount()>0);
		cursor.close();
		return exists;
	}
	
	public long insert(String id, String name, String description, String isActive, String flag){
		ContentValues initialValues = createContent(id, name, description, isActive, flag);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}
	
	private ContentValues createContent(String id, String name, String description, String isActive, String flag){
		ContentValues values = new ContentValues();
		values.put(DataBaseHelper.FIELD_PROVINCE_ID, id);
		values.put(DataBaseHelper.FIELD_PROVINCE_NAME, name);
		values.put(DataBaseHelper.FIELD_PROVINCE_DESCRIPTION, description);
		values.put(DataBaseHelper.FIELD_PROVINCE_IS_ACTIVE, isActive);
		values.put(DataBaseHelper.FIELD_PROVINCE_FLAG, flag);
		
		return values;
	}
	
	public void update(String id, String name, String description, String isActive, String flag){
		ContentValues args = new ContentValues();
	    args.put(DataBaseHelper.FIELD_PROVINCE_ID, id);
	    args.put(DataBaseHelper.FIELD_PROVINCE_NAME, name);
	    args.put(DataBaseHelper.FIELD_PROVINCE_DESCRIPTION, description);
	    args.put(DataBaseHelper.FIELD_PROVINCE_IS_ACTIVE, isActive);
	    args.put(DataBaseHelper.FIELD_PROVINCE_FLAG, flag);
	    database.update(DATABASE_TABLE, args, DataBaseHelper.FIELD_PROVINCE_ID + "=" + id, null);
	}
	
	public void hapusKata(int id){
		database.delete(DataBaseHelper.TABLE_PROVINCE_NAME, "_id = '"+id+"'", null);
	}

	public Cursor queryGetProvinceName(int provinceId){

		return database.rawQuery("SELECT * FROM "+ProvinceHelper.DATABASE_TABLE+
				" WHERE "+DataBaseHelper.FIELD_PROVINCE_ID+"='"+provinceId+"' ORDER BY "+DataBaseHelper.FIELD_PROVINCE_NAME+" ASC", null);
	}

	public ArrayList<MasterData> loadProvinceById(int provinceId){
		ArrayList<MasterData> arrayList = new ArrayList<MasterData>();
		Cursor cursor = queryGetProvinceName(provinceId);
		//startManagingCursor(cursor);
		cursor.moveToFirst();
		MasterData masterData;
		if (cursor.getCount()>0) {
			do {

				masterData = new MasterData();
				masterData.set_id(cursor.getInt(0));
				masterData.setId(cursor.getString(1));
				masterData.setName(cursor.getString(2));
				masterData.setDescription(cursor.getString(3));
				masterData.setIsActive(cursor.getString(4));
				masterData.setFlag(cursor.getString(5));

				arrayList.add(masterData);
				cursor.moveToNext();

			} while (!cursor.isAfterLast());
		}
		cursor.close();
		return arrayList;
	}

	public Cursor queryGetProvinceByName(String name){

		return database.rawQuery("SELECT * FROM "+ProvinceHelper.DATABASE_TABLE+
				" WHERE "+DataBaseHelper.FIELD_PROVINCE_NAME+"='"+name+"' ORDER BY "+DataBaseHelper.FIELD_PROVINCE_NAME+" ASC", null);
	}

	public ArrayList<MasterData> loadProvinceByName(String name){
		ArrayList<MasterData> arrayList = new ArrayList<MasterData>();
		Cursor cursor = queryGetProvinceByName(name);
		//startManagingCursor(cursor);
		cursor.moveToFirst();
		MasterData masterData;
		if (cursor.getCount()>0) {
			do {

				masterData = new MasterData();
				masterData.set_id(cursor.getInt(0));
				masterData.setId(cursor.getString(1));
				masterData.setName(cursor.getString(2));
				masterData.setDescription(cursor.getString(3));
				masterData.setIsActive(cursor.getString(4));
				masterData.setFlag(cursor.getString(5));

				arrayList.add(masterData);
				cursor.moveToNext();

			} while (!cursor.isAfterLast());
		}
		cursor.close();
		return arrayList;
	}

}
