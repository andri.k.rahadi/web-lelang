package com.sparkworks.mandiri.lelang.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import android.util.Log;

import com.sparkworks.mandiri.lelang.model.Params;

public class ApiHelper {
	
	public static enum LelangType{
		ALL, SOON, HOT_PRICE, TERBARU, TERDEKAT, FILTER, FAVORITE, JUAL_SUKARELA
	}
	
	public static List<NameValuePair> getListParams(Params params){
		
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		list.add(new BasicNameValuePair("pageIndex", params.getPageIndex()));
		list.add(new BasicNameValuePair("categoryID", params.getCategoryId()));
		list.add(new BasicNameValuePair("cityID", params.getCityId()));
		list.add(new BasicNameValuePair("provinceID", params.getProvinceId()));
		list.add(new BasicNameValuePair("minPriceID", params.getMinPriceId()));
		list.add(new BasicNameValuePair("maxPriceID", params.getMaxPriceId()));
		list.add(new BasicNameValuePair("hotPrice", params.getHotPrice()));
		list.add(new BasicNameValuePair("auctionDate", params.getAuctionDate()));
		list.add(new BasicNameValuePair("documentTypeID", params.getDocumentTypeId()));
		list.add(new BasicNameValuePair("address", params.getAddress()));
		list.add(new BasicNameValuePair("orderBy", params.getOrderBy()));
		list.add(new BasicNameValuePair("keyword", params.getKeyword()));
		list.add(new BasicNameValuePair("type1", String.valueOf(params.isType1() ? 1 : 0)));
		list.add(new BasicNameValuePair("type2", String.valueOf(params.isType2() ? 1 : 0)));
		
		return list;
	}
	
	public static String buildAPIURLMessage(String url, String urlParams, List<NameValuePair> getParams){
		String params = "", token = "";
		if (getParams == null) {
			params = "";
			token = Utils.getHashedToken("");
		}else{
			params = URLEncodedUtils.format(getParams, HTTP.UTF_8);
			token = Utils.getHashedToken(params);
		}
		
		if (getParams == null || getParams.equals("")) {
			if (urlParams != null) {
				url += "/" + urlParams + "?" +"token=" + token;
			}else{
				url += "?token=" + token;
			}
		}else{
			if (urlParams != null && !urlParams.equals("")) {
				url += "/" + urlParams + "?" + params + "&token=" + token;
			} else {
				url += "?" + params + "&token=" + token;
			}
		}
		
		Log.d(Constant.APP_TAG, "url : "+url);
		
		return url;
	}	
	
	public static List<NameValuePair> getListDetilItemParams(String assetID){
		
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		list.add(new BasicNameValuePair("assetID", assetID));
		return list;
	}
	
	public static List<NameValuePair> getAuctionScheduleParams(int pageIndex){
		
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		list.add(new BasicNameValuePair("pageIndex", String.valueOf(pageIndex)));
		return list;
	}
	
	public static List<NameValuePair> getAuctionByScheduleParams(int auctionScheduleID){
		
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		list.add(new BasicNameValuePair("auctionScheduleID", String.valueOf(auctionScheduleID)));
		list.add(new BasicNameValuePair("pageIndex", "0"));
		return list;
	}	
	
	public static List<NameValuePair> getMasterDataParams(String lastTimeStamp){
		
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		list.add(new BasicNameValuePair("lastTimestamp", lastTimeStamp));
		return list;
	}
}
