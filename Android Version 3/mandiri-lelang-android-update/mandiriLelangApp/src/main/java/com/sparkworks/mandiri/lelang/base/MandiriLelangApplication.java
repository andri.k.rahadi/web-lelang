package com.sparkworks.mandiri.lelang.base;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import android.app.Application;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.mandiri.lelang.aplikasi.R;

public class MandiriLelangApplication extends Application{
	 @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault("fonts/MyriadPro-Regular.otf", R.attr.fontPath);

         FacebookSdk.sdkInitialize(getApplicationContext());
         AppEventsLogger.activateApp(this);
    }
}
