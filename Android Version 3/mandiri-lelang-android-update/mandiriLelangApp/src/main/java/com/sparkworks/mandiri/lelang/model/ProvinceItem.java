package com.sparkworks.mandiri.lelang.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sidiqpermana on 11/9/16.
 */

public class ProvinceItem implements Parcelable {
    private String id;
    private String name;
    private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeByte(this.checked ? (byte) 1 : (byte) 0);
    }

    public ProvinceItem() {
    }

    protected ProvinceItem(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.checked = in.readByte() != 0;
    }

    public static final Parcelable.Creator<ProvinceItem> CREATOR = new Parcelable.Creator<ProvinceItem>() {
        @Override
        public ProvinceItem createFromParcel(Parcel source) {
            return new ProvinceItem(source);
        }

        @Override
        public ProvinceItem[] newArray(int size) {
            return new ProvinceItem[size];
        }
    };
}
