package com.sparkworks.mandiri.lelang.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sparkworks.mandiri.lelang.fragment.LelangListingFragment;
import com.sparkworks.mandiri.lelang.fragment.PencarianFragment;
import com.sparkworks.mandiri.lelang.model.Params;
import com.sparkworks.mandiri.lelang.utils.ApiHelper.LelangType;
import com.sparkworks.mandiri.lelang.utils.URLs;

public class HomeFragmentAdapter extends FragmentPagerAdapter{

	public static String[] title = new String[]{
		"Semua", "Jual Sukarela", "Segera Dilelang", "Hot Price", "Terbaru", "Terdekat"
	};
	
	PencarianFragment pencarianFragment = null;
	LelangListingFragment hotListing = null;
	LelangListingFragment segeraListing = null;
	LelangListingFragment jualSukarela = null;
	LelangListingFragment terbaruListing = null;
	LelangListingFragment terdekatListing = null;
	
	public HomeFragmentAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int arg0) {
		// TODO Auto-generated method stub
		if (arg0==0) {
			if (pencarianFragment == null) {
				pencarianFragment = new PencarianFragment();
			}
			return pencarianFragment;
		}else if(arg0==1){
			if (jualSukarela == null) {
				Params paramSukarela = Params.getParams(LelangType.JUAL_SUKARELA,
						"0", "0", "0", "0", "0", "0", "0",
						"", "0", "", "0");
				jualSukarela = new LelangListingFragment();
				jualSukarela.setUrl(URLs.ITEM_GET_DIRECT_SELL);
				jualSukarela.setLelangType(LelangType.JUAL_SUKARELA);
				jualSukarela.setParams(paramSukarela);
				jualSukarela.setJualSukarela(true);
			}
			return jualSukarela;
		}
		else if(arg0==2){
			if (segeraListing == null) {
				Params paramsSegera = Params.getParams(LelangType.SOON,
						"0", "0", "0", "0", "0", "0", "0", 
						"", "0", "", "0");
				segeraListing = new LelangListingFragment();
				segeraListing.setUrl(URLs.ITEM_GET_ALL_SOON);
				segeraListing.setLelangType(LelangType.SOON);
				segeraListing.setParams(paramsSegera);
			}
			return segeraListing;
		}else if(arg0==3){
			if (hotListing == null) {
				Params paramsHotListing = Params.getParams(LelangType.HOT_PRICE, 
						"0", "0", "0", "0", "0", "0", "1", "", "0", "", "1");
				hotListing = new LelangListingFragment();
				hotListing.setUrl(URLs.ITEM_GET_HOT_PRICE);
				hotListing.setLelangType(LelangType.HOT_PRICE);
				hotListing.setParams(paramsHotListing);
			}
			return hotListing;
		}else if(arg0==4){
			if (terbaruListing == null) {
				Params terbaruParams = Params.getParams(LelangType.TERBARU, 
						"0","0", "0", "0", "0", "0", "0", "", "0", "", "1");
				terbaruListing = new LelangListingFragment();
				terbaruListing.setUrl(URLs.ITEM_GET_LATEST);
				terbaruListing.setLelangType(LelangType.TERBARU);
				terbaruListing.setParams(terbaruParams);
			}
			return terbaruListing;
		}
		else if(arg0==5){
			if (terdekatListing == null) {
				Params terdekatParams = Params.getParams(LelangType.TERDEKAT, 
						"0","0", "0", "0", "0", "0", "0", "", "0", "", "1");
				terdekatListing = new LelangListingFragment();
				terdekatListing.setUrl(URLs.ITEM_GET_NEAREST);
				terdekatListing.setLelangType(LelangType.TERDEKAT);
				terdekatListing.setParams(terdekatParams);
			}
			return terdekatListing;
		}else{
			return null;
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return title.length;
	}
	
    public CharSequence getPageTitle(int position) {
      return title[position % title.length];
    }
	
}
