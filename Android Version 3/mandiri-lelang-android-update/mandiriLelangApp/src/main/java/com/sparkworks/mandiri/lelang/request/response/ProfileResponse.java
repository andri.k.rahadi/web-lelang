package com.sparkworks.mandiri.lelang.request.response;

import com.google.gson.annotations.SerializedName;
import com.sparkworks.mandiri.lelang.request.model.ProfileData;

/**
 * Created by sidiqpermana on 11/29/16.
 */

public class ProfileResponse extends BaseResponse {
    @SerializedName("data")
    private ProfileData profileData;

    public ProfileData getProfileData() {
        return profileData;
    }

    public void setProfileData(ProfileData profileData) {
        this.profileData = profileData;
    }
}
