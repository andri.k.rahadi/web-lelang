package com.sparkworks.mandiri.lelang.fragment;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.mandiri.lelang.aplikasi.R;
import com.mandiri.lelang.aplikasi.SearchResultActivity;
import com.sparkworks.mandiri.lelang.adapter.MinMaxAdapter;
import com.sparkworks.mandiri.lelang.base.BaseFragment;
import com.sparkworks.mandiri.lelang.model.MasterData;
import com.sparkworks.mandiri.lelang.model.Params;

public class FilterFragment extends BaseFragment{
	
	public static String FRAGMENT_TAG = "FilterFragment";
	private Spinner spnSortBy, spnCategory, spnPropinsi, spnKota, spnHargaMax, spnHargaMin, 
	spnDokumen;
	private EditText edtAlamat, edtKeyword;
	private ToggleButton cbHotprice;
	private Button btnReset, btnApply, btnSelectDate;
	private TextView txtDateSelected;
	private CheckBox cbJualSukarela, cbLelang;
	
	private int mYear;
    private int mMonth;
    private int mDay;
    static final int DATE_DIALOG_ID = 0;
	
	private String[] sortBy = new String[]{
		"Terbaru", "Harga Tertinggi", "Harga Terendah", "Segera Dilelang"
	};

	public enum FilterType{
		ALL, SEGERA, HOT, TERBARU, TERDEKAT, JUAL_SUKARELA
		
	};
	
	public FilterType type;
	public String category, location;
	
	private String FilterTag = "Filter", CategoryTag = "category", LocationTag = "location";
	
	private ArrayList<MasterData> listCategory, listProvince, listCity, listMinPrice, listMaxPrice,  
	listDocumentType;
	private ArrayList<String> listMinPriceName, listMaxPriceName;

	private MinMaxAdapter minPriceAdapter, maxPriceAdapter;
	
	public FilterType getType() {
		return type;
	}

	public void setType(FilterType type) {
		this.type = type;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		initializeLibs();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_filter, container, false);
		
		getActivity().getActionBar().setDisplayUseLogoEnabled(false);
        getActivity().getActionBar().setDisplayShowHomeEnabled(true);
        getActivity().getActionBar().setIcon(R.drawable.logo_home);
        getActivity().getActionBar().setTitle("");
        getActivity().getActionBar().setHomeButtonEnabled(true);
		
		initializeViews(view);
		return view;
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initDate();
		
		if (savedInstanceState != null) {
			setType((FilterType)savedInstanceState.getSerializable(FilterTag));
			setLocation(savedInstanceState.getString(LocationTag));
			setCategory(savedInstanceState.getString(CategoryTag));
		}
		
		initializeProcess();
		initializeActions();
	}
	
	@Override
	public void initializeViews(View view) {
		// TODO Auto-generated method stub
		super.initializeViews(view);
		
		spnCategory = (Spinner)view.findViewById(R.id.spn_filter_sort_kategori);
		spnSortBy = (Spinner)view.findViewById(R.id.spn_filter_sort_by);
		spnDokumen = (Spinner)view.findViewById(R.id.spn_filter_sort_dokumen);
		spnHargaMax = (Spinner)view.findViewById(R.id.spn_filter_sort_harga_maksimal);
		spnHargaMin = (Spinner)view.findViewById(R.id.spn_filter_sort_harga_minimal);
		spnKota = (Spinner)view.findViewById(R.id.spn_filter_sort_kota);
		spnPropinsi = (Spinner)view.findViewById(R.id.spn_filter_sort_propinsi);
		cbJualSukarela = (CheckBox)view.findViewById(R.id.cb_jual_sukarela);
		cbLelang = (CheckBox)view.findViewById(R.id.cb_lelang);
		
		btnApply = (Button)view.findViewById(R.id.btn_filter_apply);
		btnReset = (Button)view.findViewById(R.id.btn_filter_reset);
		btnSelectDate = (Button)view.findViewById(R.id.btn_filter_select_date);
		
		edtAlamat = (EditText)view.findViewById(R.id.edt_filter_alamat);
		edtAlamat.clearFocus();
		edtKeyword = (EditText)view.findViewById(R.id.edt_keyword);
		edtKeyword.clearFocus();
		cbHotprice = (ToggleButton)view.findViewById(R.id.cb_filter_hot_price);
		
		txtDateSelected = (TextView)view.findViewById(R.id.txt_filter_date);

		spnHargaMin.setAdapter(minPriceAdapter);
		spnHargaMax.setAdapter(maxPriceAdapter);
		
	}
	
	@Override
	public void initializeLibs() {
		// TODO Auto-generated method stub
		super.initializeLibs();
		listCategory = new ArrayList<MasterData>();
		listCity = new ArrayList<MasterData>();
		listDocumentType = new ArrayList<MasterData>();
		listMinPrice = new ArrayList<MasterData>();
		listMaxPrice = new ArrayList<MasterData>();
		listProvince = new ArrayList<MasterData>();

		listMinPriceName = new ArrayList<>();
		listMaxPriceName = new ArrayList<>();

		minPriceAdapter = new MinMaxAdapter(getActivity());
		minPriceAdapter.setListPrice(listMinPrice);

		maxPriceAdapter = new MinMaxAdapter(getActivity());
		maxPriceAdapter.setListPrice(listMaxPrice);
	}
	
	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		super.initializeProcess();
		
		ArrayAdapter<String> adapterSortBy = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_dropdown_item_1line,
				android.R.id.text1, sortBy);
		spnSortBy.setAdapter(adapterSortBy);
		
		listCategory = categoryHelper.loadAllData();
		ArrayList<String> categories = new ArrayList<String>();
		categories.add("Semua");
		for (int i = 0; i < listCategory.size(); i++) {
			categories.add(listCategory.get(i).getName());
		}
		
		ArrayAdapter<String> adapterCategory = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_dropdown_item_1line,
				android.R.id.text1, categories);
		spnCategory.setAdapter(adapterCategory);
		
		listProvince = provinceHelper.loadAllData();
		ArrayList<String> propinsi = new ArrayList<String>();
		propinsi.add("Semua");
		for (int i = 0; i < listProvince.size(); i++) {
			propinsi.add(listProvince.get(i).getName());
		}
		ArrayAdapter<String> adapterPropinsi = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_dropdown_item_1line,
				android.R.id.text1, propinsi);
		spnPropinsi.setAdapter(adapterPropinsi);
		
		listCity = cityHelper.loadAllData();
		ArrayList<String> kota = new ArrayList<String>();
		kota.add("Semua");
		for (int i = 0; i < listCity.size(); i++) {
			kota.add(listCity.get(i).getName());
		}
		ArrayAdapter<String> adapterKota = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_dropdown_item_1line,
				android.R.id.text1, kota);
		spnKota.setAdapter(adapterKota);

		/*MasterData minOptionAll = new MasterData();
		minOptionAll.setName("Semua");
		listMinPrice.add(minOptionAll);*/

		listMinPriceName.add("Semua");
		listMinPrice.addAll(minMaxPriceHelper.loadAllData());

		for (int i = 0; i < listMinPrice.size(); i++) {
			listMinPriceName.add(listMinPrice.get(i).getName());
		}
		ArrayAdapter<String> adapterHargaMin = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_dropdown_item_1line,
				android.R.id.text1, listMinPriceName);
		spnHargaMin.setAdapter(adapterHargaMin);

		minPriceAdapter.setListPrice(listMinPrice);
		minPriceAdapter.notifyDataSetChanged();

		listDocumentType = documentTypeHelper.loadAllData();
		ArrayList<String> dokumen = new ArrayList<String>();
		dokumen.add("Semua");
		for (int i = 0; i < listDocumentType.size(); i++) {
			dokumen.add(listDocumentType.get(i).getName());
		}
		ArrayAdapter<String> adapterDokumen = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_dropdown_item_1line,
				android.R.id.text1, dokumen);
		spnDokumen.setAdapter(adapterDokumen);
		
		if (getType()==FilterType.ALL) {
			for (int i = 0; i < categories.size(); i++) {
				if (categories.get(i).toString().equals(getCategory())) {
					spnCategory.setSelection(i);
					break;
				}
				spnCategory.setEnabled(true);
			}
			
			for (int i = 0; i < propinsi.size(); i++) {
				if (propinsi.get(i).toString().equals(getLocation())) {
					spnPropinsi.setSelection(i);
					break;
				}
				spnPropinsi.setEnabled(true);
			}
		}
		
		if (getType()==FilterType.SEGERA) {
			spnSortBy.setSelection(3);
			spnSortBy.setEnabled(false);

			cbJualSukarela.setEnabled(false);
			cbJualSukarela.setChecked(false);
		}

		if (getType()==FilterType.JUAL_SUKARELA) {
			spnSortBy.setSelection(0);
			spnSortBy.setEnabled(false);

			cbLelang.setEnabled(false);
			cbLelang.setChecked(false);
		}
		
		if (getType()==FilterType.HOT) {
			cbHotprice.setChecked(true);
			cbHotprice.setSelected(true);
			cbHotprice.setEnabled(false);

			cbJualSukarela.setEnabled(false);
			cbJualSukarela.setChecked(false);
		}
		
		if (getType()==FilterType.TERBARU) {
			spnSortBy.setSelection(0);
			spnSortBy.setEnabled(false);
		}
		
		if (getType()==FilterType.TERDEKAT) {
			spnSortBy.setSelection(0);
			spnSortBy.setEnabled(false);
			
			spnPropinsi.setSelection(0);
			spnPropinsi.setEnabled(false);
			spnKota.setSelection(0);
			spnKota.setEnabled(false);
		}
	}
	
	@Override
	public void initializeActions() {
		// TODO Auto-generated method stub
		super.initializeActions();
		
		btnSelectDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				new DatePickerDialog(getActivity(),
	                    mDateSetListener,
	                    mYear, mMonth, mDay).show();
			}
		});
		
		btnApply.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String categoryId = spnCategory.getSelectedItemPosition()==0?"0":String.valueOf(listCategory.get(spnCategory.getSelectedItemPosition()-1).getId()); 
				String provinceId = spnPropinsi.getSelectedItemPosition()==0?"0":String.valueOf(listProvince.get(spnPropinsi.getSelectedItemPosition()-1).getId());
				String cityId = spnKota.getSelectedItemPosition()==0?"0":String.valueOf(listCity.get(spnKota.getSelectedItemPosition()-1).getId());
				String minPriceId = spnHargaMin.getSelectedItemPosition()==0?"0":String.valueOf(listMinPrice.get(spnHargaMin.getSelectedItemPosition()-1).getId());
				String maxPriceId = spnHargaMax.getSelectedItemPosition()==0?"0":String.valueOf(listMaxPrice.get(spnHargaMax.getSelectedItemPosition()-1).getId());
				String documentTypeId = spnDokumen.getSelectedItemPosition()==0?"0":String.valueOf(listDocumentType.get(spnDokumen.getSelectedItemPosition()-1).getId());
				
				setSearchParams(categoryId, provinceId, cityId, minPriceId, maxPriceId, documentTypeId);
			}
		});
		
		btnReset.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				edtAlamat.setText("");
			}
		});
		
		spnPropinsi.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				
				if (listCity.size()>0) {
					listCity.clear();
				}
				
				ArrayAdapter<String> adapterKota;
				if (arg2 == 0) {
					listCity = cityHelper.loadAllData();
					ArrayList<String> kota = new ArrayList<String>();
					kota.add("Semua");
					for (int i = 0; i < listCity.size(); i++) {
						kota.add(listCity.get(i).getName());
					}
					adapterKota = new ArrayAdapter<String>(getActivity(),
							android.R.layout.simple_dropdown_item_1line,
							android.R.id.text1, kota);
					spnKota.setAdapter(adapterKota);
					
				}else{
					String provinceId = listProvince.get(arg2-1).getId();
					listCity = cityHelper.loadAllCityByProvince(provinceId);
					ArrayList<String> kota = new ArrayList<String>();
					kota.add("Semua");
					for (int i = 0; i < listCity.size(); i++) {
						kota.add(listCity.get(i).getName());
					}
					adapterKota = new ArrayAdapter<String>(getActivity(),
							android.R.layout.simple_dropdown_item_1line,
							android.R.id.text1, kota);
					
				}
				spnKota.setAdapter(adapterKota);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});

		spnHargaMin.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				if (listMaxPrice.size() > 0){
					listMaxPrice.clear();
					listMaxPriceName.clear();
				}

				/*MasterData maxOptionAll = new MasterData();
				maxOptionAll.setName("Semua");
				listMaxPrice.add(maxOptionAll);*/

				listMaxPriceName.add("Semua");

				if (i == 0){
					listMaxPrice.addAll(minMaxPriceHelper.loadAllData());
				}else{
					listMaxPrice.addAll(minMaxPriceHelper
							.loadAllDataByRange(true, Long.parseLong(listMinPrice.get(spnHargaMin.getSelectedItemPosition() - 1).getValue())));
				}

				for (int j = 0; j < listMaxPrice.size(); j++) {
					listMaxPriceName.add(listMaxPrice.get(j).getName());
				}
				ArrayAdapter<String> adapterHargaMax = new ArrayAdapter<String>(getActivity(),
						android.R.layout.simple_dropdown_item_1line,
						android.R.id.text1, listMaxPriceName);
				spnHargaMax.setAdapter(adapterHargaMax);


				/*maxPriceAdapter.setListPrice(listMaxPrice);
				maxPriceAdapter.notifyDataSetChanged();*/
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});
	}
	
	protected void setSearchParams(String categoryId, String provinceId, String cityId, 
			String minPriceId, String maxPriceId, String documentTypeId) {
		// TODO Auto-generated method stub
		Params params = new Params();
		params.setPageIndex("0");
		params.setCategoryId(categoryId);
		params.setProvinceId(provinceId);
		params.setCityId(cityId);
		params.setMinPriceId(minPriceId);
		params.setMaxPriceId(maxPriceId);
		params.setType1(cbJualSukarela.isChecked());
		params.setType2(cbLelang.isChecked());
		
		if (cbHotprice.isChecked()) {
			params.setHotPrice("1");
		}else{
			params.setHotPrice("0");
		}
		String date[] = txtDateSelected.getText().toString().split("-");
		params.setAuctionDate(date[2]+date[1]+date[0]);
		params.setDocumentTypeId(documentTypeId);
		params.setAddress(edtAlamat.getText().toString().trim());
		params.setOrderBy(String.valueOf(spnSortBy.getSelectedItemPosition()+1));
		params.setKeyword(edtKeyword.getText().toString());
		
		SearchResultActivity.toSearchResultActivity(getActivity(), params);
	}

	private void initDate(){
		// get the current date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        updateDisplay();

	}
	
	private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, 
                                      int monthOfYear, int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                    updateDisplay();
                }
            };
     
    private void updateDisplay() {
    	String year,month,day;
    	year = mYear+"";
    	if(mMonth + 1 < 10)
    		month = "0" + (mMonth + 1);
    	else
    		month = (mMonth+1) + "";
    	if(mDay < 10)
    		day = "0" + mDay;
    	else
    		day = mDay + "";
                txtDateSelected.setText(
                    new StringBuilder()
                            // Month is 0 based so add 1
                    		.append(day).append("-").append(month).append("-")
                            .append(year));
            }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
    	// TODO Auto-generated method stub
    	outState.putSerializable(FilterTag, getType());
    	outState.putString(LocationTag, getLocation());
    	outState.putString(CategoryTag, getCategory());
    	super.onSaveInstanceState(outState);
    }
}
