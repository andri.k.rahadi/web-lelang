package com.sparkworks.mandiri.lelang.event;

import android.util.Log;

/**
 * Created by sidiqpermana on 1/18/17.
 */

public class ClearErrorInViewsEvent {
    public void printLog(){
        Log.d("Event", "Clear All Error in Views");
    }
}
