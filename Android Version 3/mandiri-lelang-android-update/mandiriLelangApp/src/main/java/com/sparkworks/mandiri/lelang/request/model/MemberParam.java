package com.sparkworks.mandiri.lelang.request.model;

/**
 * Created by sidiqpermana on 11/29/16.
 */

public class MemberParam {
    private Profile profile;
    private Preference preference;

    public Profile getProfileParam() {
        return profile;
    }

    public void setProfileParam(Profile profileParam) {
        this.profile = profileParam;
    }

    public Preference getpreference() {
        return preference;
    }

    public void setpreference(Preference preference) {
        this.preference = preference;
    }
}
