package com.sparkworks.mandiri.lelang.request.api;

import android.content.Context;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.request.response.FavoriteUnfavoriteResponse;
import com.sparkworks.mandiri.lelang.utils.URLs;
import com.sparkworks.mandiri.lelang.utils.Utils;

import org.apache.http.Header;

import java.util.Locale;

/**
 * Created by sidiqpermana on 11/29/16.
 */

public class FavoriteUnfavoriteRequest extends BaseApi {
    private AsyncHttpClient asyncHttpClient;
    private Context context;
    private int assetId;
    private OnFavoriteUnfavoriteRequestListener onFavoriteUnfavoriteRequestListener;
    public static final int POST_FAVORITE = 1;
    public static final int POST_UNFAVORITE = 2;
    private int postType;

    public int getPostType() {
        return postType;
    }

    public void setPostType(int postType) {
        this.postType = postType;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public int getAssetId() {
        return assetId;
    }

    public void setAssetId(int assetId) {
        this.assetId = assetId;
    }

    public OnFavoriteUnfavoriteRequestListener getOnFavoriteUnfavoriteRequestListener() {
        return onFavoriteUnfavoriteRequestListener;
    }

    public void setOnFavoriteUnfavoriteRequestListener(OnFavoriteUnfavoriteRequestListener onFavoriteUnfavoriteRequestListener) {
        this.onFavoriteUnfavoriteRequestListener = onFavoriteUnfavoriteRequestListener;
    }

    @Override
    public void callApi() {
        super.callApi();
        if (Utils.isConnectInet(getContext())){
            asyncHttpClient = getAsyncHttpClient(getUserPreference(getContext()).getToken());

            RequestParams param = new RequestParams("assetID", getAssetId());

            String url = getPostType() == POST_FAVORITE ? URLs.SERVER_FAVORITE : URLs.SERVER_UNFAVORITE;
            asyncHttpClient.post(getContext(), url, null, param, "application/x-www-form-urlencoded", new AsyncHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    super.onSuccess(statusCode, headers, responseBody);
                    String s = new String(responseBody);
                    Gson gson = new Gson();
                    FavoriteUnfavoriteResponse favoriteUnfavoriteResponse = gson.fromJson(s, FavoriteUnfavoriteResponse.class);
                    if (favoriteUnfavoriteResponse.getResultCode().getCode() == CODE_SUCCESS){
                        if (favoriteUnfavoriteResponse.getResultCode().getMessage().toUpperCase(Locale.ENGLISH).contains("TOKEN")){
                            getOnFavoriteUnfavoriteRequestListener().onFavoriteFailed("Sesi login anda sudah habis, mohon melakukan login ulang");
                        }else{
                            if (getPostType() == POST_FAVORITE){
                                getOnFavoriteUnfavoriteRequestListener().onFavoriteSuccess(favoriteUnfavoriteResponse);
                            }else{
                                getOnFavoriteUnfavoriteRequestListener().onUnfavoriteSuccess(favoriteUnfavoriteResponse);
                            }
                        }
                    }else{
                        if (getPostType() == POST_FAVORITE){
                            getOnFavoriteUnfavoriteRequestListener().onFavoriteFailed(favoriteUnfavoriteResponse.getResultCode().getMessage());
                        }else{
                            getOnFavoriteUnfavoriteRequestListener().onUnfavoriteFailed(favoriteUnfavoriteResponse.getResultCode().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    super.onFailure(statusCode, headers, responseBody, error);
                    if (getPostType() == POST_FAVORITE){
                        getOnFavoriteUnfavoriteRequestListener().onFavoriteFailed(getContext().getString(R.string.error_favorite));
                    }else{
                        getOnFavoriteUnfavoriteRequestListener().onUnfavoriteFailed(getContext().getString(R.string.error_unfavorite));
                    }
                }
            });
        }else{
            if (getPostType() == POST_FAVORITE){
                getOnFavoriteUnfavoriteRequestListener().onFavoriteFailed(getContext().getString(R.string.no_internet));
            }else{
                getOnFavoriteUnfavoriteRequestListener().onUnfavoriteFailed(getContext().getString(R.string.no_internet));
            }
        }
    }

    @Override
    public void cancel() {
        super.cancel();
        if (asyncHttpClient != null){
            asyncHttpClient.cancelRequests(getContext(), true);
        }
    }

    @Override
    public void retry() {
        super.retry();
        callApi();
    }

    public interface OnFavoriteUnfavoriteRequestListener{
        void onFavoriteSuccess(FavoriteUnfavoriteResponse favoriteUnfavoriteResponse);
        void onFavoriteFailed(String message);
        void onUnfavoriteSuccess(FavoriteUnfavoriteResponse favoriteUnfavoriteResponse);
        void onUnfavoriteFailed(String message);
    }
}
