package com.sparkworks.mandiri.lelang.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.base.BaseFragment;
import com.sparkworks.mandiri.lelang.model.AuctionHall;
import com.sparkworks.mandiri.lelang.model.Branch;
import com.sparkworks.mandiri.lelang.utils.Utils;

public class DetilLelangItemPengelolaFragment extends BaseFragment{
	public static String FRAGMENT_TAG = "DetilLelangItemPengelolaFragment";
	private TextView txtPengelola, txtAlamatPengelola, txtNoTelpPengelola, txtNoTelpPengelola2;
	
	public static String AUCTION_KEY = "auction";
	public static String BRANCH_KEY = "branch";
	
	public Branch pengelola;
	public AuctionHall auctionHall;
	
	public AuctionHall getAuctionHall() {
		return auctionHall;
	}

	public void setAuctionHall(AuctionHall auctionHall) {
		this.auctionHall = auctionHall;
	}

	public Branch getPengelola() {
		return pengelola;
	}

	public void setPengelola(Branch pengelola) {
		this.pengelola = pengelola;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_detil_lelang_pengelola, container, false);
		initializeViews(view);
		return view;
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		if (savedInstanceState != null) {
			setAuctionHall((AuctionHall)savedInstanceState.getSerializable(AUCTION_KEY));
			setPengelola((Branch)savedInstanceState.getSerializable(BRANCH_KEY));
		}
		
		initializeProcess();
		initializeActions();
	}
	
	@Override
	public void initializeViews(View view) {
		// TODO Auto-generated method stub
		super.initializeViews(view);
		txtAlamatPengelola = (TextView)view.findViewById(R.id.txtDetilItemALamat);
		txtNoTelpPengelola = (TextView)view.findViewById(R.id.txtDetilItemTelepon);
		txtPengelola = (TextView)view.findViewById(R.id.txtDetilItemNamaPengelola);
		txtNoTelpPengelola2 = (TextView)view.findViewById(R.id.txtDetilItemTelepon2);
	}
	
	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		super.initializeProcess();
		
		txtAlamatPengelola.setText(Utils.replaceNull(getPengelola().getAddress()));
		txtNoTelpPengelola.setText(Utils.replaceNull(getPengelola().getPhone1()));
		txtPengelola.setText(getPengelola().getName());
		txtNoTelpPengelola2.setText(Utils.replaceNull(getPengelola().getPhone2()));
	}
	
	@Override
	public void initializeActions() {
		// TODO Auto-generated method stub
		super.initializeActions();
		
		txtNoTelpPengelola.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Utils.dialPhone(getActivity(), getPengelola().getPhone1());
			}
		});
		
		txtNoTelpPengelola2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Utils.dialPhone(getActivity(), getPengelola().getPhone2());
			}
		});	
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		outState.putSerializable(AUCTION_KEY, getAuctionHall());
		outState.putSerializable(BRANCH_KEY, getPengelola());
		super.onSaveInstanceState(outState);
	}
}
