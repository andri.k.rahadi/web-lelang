package com.sparkworks.mandiri.lelang.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.model.Video;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by sidiqpermana on 9/24/17.
 */

public class VideoAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Video> videos;
    private LayoutInflater inflater;

    public VideoAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public ArrayList<Video> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<Video> videos) {
        this.videos = videos;
    }

    @Override
    public int getCount() {
        return getVideos().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Viewholder viewholder = null;
        if (view == null){
            view = inflater.inflate(R.layout.item_video, null);
            viewholder = new Viewholder();
            viewholder.imgThumbnail = (ImageView)view.findViewById(R.id.img_thumbnail);
            viewholder.tvTitle = (TextView)view.findViewById(R.id.tv_video_title);
            view.setTag(viewholder);
        }else{
            viewholder = (Viewholder)view.getTag();
        }
        Picasso.with(context).load(getVideos().get(i).getThumbnail()).into(viewholder.imgThumbnail);
        viewholder.tvTitle.setText(getVideos().get(i).getTitle());
        return view;
    }

    static class Viewholder{
        ImageView imgThumbnail;
        TextView tvTitle;
    }
}
