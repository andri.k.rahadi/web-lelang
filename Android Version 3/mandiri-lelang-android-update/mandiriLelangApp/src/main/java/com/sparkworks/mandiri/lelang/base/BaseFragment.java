package com.sparkworks.mandiri.lelang.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.sparkworks.mandiri.lelang.database.helper.BranchHelper;
import com.sparkworks.mandiri.lelang.database.helper.CategoryHelper;
import com.sparkworks.mandiri.lelang.database.helper.CityHelper;
import com.sparkworks.mandiri.lelang.database.helper.ContactUsHelper;
import com.sparkworks.mandiri.lelang.database.helper.DocumentTypeHelper;
import com.sparkworks.mandiri.lelang.database.helper.MinMaxPriceHelper;
import com.sparkworks.mandiri.lelang.database.helper.ProvinceHelper;
import com.sparkworks.mandiri.lelang.utils.AppPreference;
import com.sparkworks.mandiri.lelang.utils.UserPreference;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class BaseFragment extends Fragment implements DefaultMethods{
	
	public CategoryHelper categoryHelper = null;
	public ProvinceHelper provinceHelper = null;
	public CityHelper cityHelper = null;
	public MinMaxPriceHelper minMaxPriceHelper = null;
	public DocumentTypeHelper documentTypeHelper = null;
	public ContactUsHelper contactUsHelper = null;
	public BranchHelper branchHelper = null;
	public AppPreference appPreference = null;
	public UserPreference userPreference = null;

	private ProgressDialog progressDialog;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		categoryHelper = new CategoryHelper(getActivity());
		categoryHelper.open();
		provinceHelper = new ProvinceHelper(getActivity());
		provinceHelper.open();
		cityHelper = new CityHelper(getActivity());
		cityHelper.open();
		minMaxPriceHelper = new MinMaxPriceHelper(getActivity());
		minMaxPriceHelper.open();
		documentTypeHelper = new DocumentTypeHelper(getActivity());
		documentTypeHelper.open();
		contactUsHelper = new ContactUsHelper(getActivity());
		contactUsHelper.open();
		branchHelper = new BranchHelper(getActivity());
		branchHelper.open();
		
		appPreference = new AppPreference(getActivity());
		userPreference = new UserPreference(getActivity());
	
	}
	@Override
	public void initializeViews() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initializeViews(View view) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initializeLibs() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initializeActions() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initializeRequest() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		if (categoryHelper!=null) {
			categoryHelper.close();
		} 
		
		if (provinceHelper != null) {
			provinceHelper.close();
		}
		
		if (cityHelper != null) {
			cityHelper.close();
		}
		
		if (minMaxPriceHelper != null) {
			minMaxPriceHelper.close();
		}
		
		if (documentTypeHelper != null) {
			documentTypeHelper.close();
		}
		
		if (contactUsHelper != null) {
			contactUsHelper.close();
		}
		
		if (branchHelper != null) {
			branchHelper.close();
		}
	}

	public void showProgressDialog(String title, String message){
		if (progressDialog == null){
			progressDialog = new ProgressDialog(getActivity());
		}

		if (!TextUtils.isEmpty(title)){
			progressDialog.setTitle(title);
		}
		progressDialog.setMessage(message);
		progressDialog.show();
	}

	public void dismissProgressDialog(){
		if (progressDialog != null){
			progressDialog.cancel();
		}
	}

	public void showToast(String message){
		Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
	}
}
