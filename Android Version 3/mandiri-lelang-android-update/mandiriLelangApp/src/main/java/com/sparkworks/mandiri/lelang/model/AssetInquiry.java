package com.sparkworks.mandiri.lelang.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sidiqpermana on 9/24/17.
 */

public class AssetInquiry implements Parcelable {
    private String kodeAsset;
    private String namaPengelola;

    public AssetInquiry(String kodeAsset, String namaPengelola) {
        this.kodeAsset = kodeAsset;
        this.namaPengelola = namaPengelola;
    }

    public String getKodeAsset() {
        return kodeAsset;
    }

    public void setKodeAsset(String kodeAsset) {
        this.kodeAsset = kodeAsset;
    }

    public String getNamaPengelola() {
        return namaPengelola;
    }

    public void setNamaPengelola(String namaPengelolaPengelola) {
        this.namaPengelola = namaPengelola;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.kodeAsset);
        dest.writeString(this.namaPengelola);
    }

    protected AssetInquiry(Parcel in) {
        this.kodeAsset = in.readString();
        this.namaPengelola = in.readString();
    }

    public static final Parcelable.Creator<AssetInquiry> CREATOR = new Parcelable.Creator<AssetInquiry>() {
        @Override
        public AssetInquiry createFromParcel(Parcel source) {
            return new AssetInquiry(source);
        }

        @Override
        public AssetInquiry[] newArray(int size) {
            return new AssetInquiry[size];
        }
    };
}
