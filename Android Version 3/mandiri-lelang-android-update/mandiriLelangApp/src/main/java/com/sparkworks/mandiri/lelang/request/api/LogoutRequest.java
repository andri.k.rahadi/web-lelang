package com.sparkworks.mandiri.lelang.request.api;

import android.content.Context;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.request.response.LogoutResponse;
import com.sparkworks.mandiri.lelang.utils.URLs;
import com.sparkworks.mandiri.lelang.utils.Utils;

import org.apache.http.Header;

import java.util.Locale;

/**
 * Created by sidiqpermana on 11/29/16.
 */

public class LogoutRequest extends BaseApi {
    private Context context;
    private AsyncHttpClient asyncHttpClient;
    private OnLogoutRequestListener onLogoutRequestListener;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public OnLogoutRequestListener getOnLogoutRequestListener() {
        return onLogoutRequestListener;
    }

    public void setOnLogoutRequestListener(OnLogoutRequestListener onLogoutRequestListener) {
        this.onLogoutRequestListener = onLogoutRequestListener;
    }

    @Override
    public void callApi() {
        super.callApi();
        if (Utils.isConnectInet(getContext())){
            asyncHttpClient = getAsyncHttpClient(getUserPreference(getContext()).getToken());
            asyncHttpClient.post(URLs.SERVER_LOGOUT, new AsyncHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    super.onSuccess(statusCode, headers, responseBody);
                    String s = new String(responseBody);
                    Gson gson = new Gson();
                    LogoutResponse logoutResponse = gson.fromJson(s, LogoutResponse.class);
                    if (logoutResponse.getResultCode().getCode() == CODE_SUCCESS){
                        getOnLogoutRequestListener().onLogoutSuccess(logoutResponse);
                    }else{
                        if (logoutResponse.getResultCode().getMessage().toUpperCase(Locale.ENGLISH).contains("TOKEN")){
                            getOnLogoutRequestListener().onLogoutFailed(logoutResponse.getResultCode().getMessage(), true);
                        }else{
                            getOnLogoutRequestListener().onLogoutFailed(logoutResponse.getResultCode().getMessage(), false);
                        }
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    super.onFailure(statusCode, headers, responseBody, error);
                    getOnLogoutRequestListener().onLogoutFailed(getContext().getString(R.string.error_logout), false);
                }
            });
        }else{
            getOnLogoutRequestListener().onLogoutFailed(getContext().getString(R.string.no_internet), false);
        }
    }

    @Override
    public void cancel() {
        super.cancel();
        if (asyncHttpClient != null){
            asyncHttpClient.cancelRequests(getContext(), true);
        }
    }

    @Override
    public void retry() {
        super.retry();
        callApi();
    }

    public interface OnLogoutRequestListener{
        void onLogoutSuccess(LogoutResponse logoutResponse);
        void onLogoutFailed(String errorMessage, boolean shouldLogin);
    }
}
