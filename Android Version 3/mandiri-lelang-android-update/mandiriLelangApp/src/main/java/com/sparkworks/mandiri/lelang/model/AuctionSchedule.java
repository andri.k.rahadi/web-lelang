package com.sparkworks.mandiri.lelang.model;

import java.io.Serializable;
import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.sparkworks.mandiri.lelang.utils.Constant;

public class AuctionSchedule implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8943401721097699287L;
	
	String auctionScheduleId, auctionDate, auctionDateFormatted;
	Kpknl kpknl;
	AuctionHall acAuctionHall;
	public String getAuctionScheduleId() {
		return auctionScheduleId;
	}
	public void setAuctionScheduleId(String auctionScheduleId) {
		this.auctionScheduleId = auctionScheduleId;
	}
	public String getAuctionDate() {
		return auctionDate;
	}
	public void setAuctionDate(String auctionDate) {
		this.auctionDate = auctionDate;
	}
	public String getAuctionDateFormatted() {
		return auctionDateFormatted;
	}
	public void setAuctionDateFormatted(String auctionDateFormatted) {
		this.auctionDateFormatted = auctionDateFormatted;
	}
	public Kpknl getKpknl() {
		return kpknl;
	}
	public void setKpknl(Kpknl kpknl) {
		this.kpknl = kpknl;
	}
	public AuctionHall getAcAuctionHall() {
		return acAuctionHall;
	}
	public void setAcAuctionHall(AuctionHall acAuctionHall) {
		this.acAuctionHall = acAuctionHall;
	}
	
	public static LinkedList<AuctionSchedule> getListAuctionSchedule(String response){
		LinkedList<AuctionSchedule> list = null;
		try {
			JSONObject object = new JSONObject(response);
			if (object.getJSONObject(Constant.RESULT).getInt(Constant.RESULT_CODE)==0) {
				
				JSONArray items = object.getJSONArray(Constant.DATA);
				if (items.length()>0) {
					AuctionSchedule acAuctionSchedule = null;
					list = new LinkedList<AuctionSchedule>();
					for (int i = 0; i < items.length(); i++) {
						JSONObject jsonObject = items.getJSONObject(i);
						acAuctionSchedule = new AuctionSchedule();
						acAuctionSchedule.setAuctionScheduleId(jsonObject.getString(Constant.AUCTION_SCHEDULE_ID));
						acAuctionSchedule.setAuctionDate(jsonObject.getString(Constant.ITEM_AUCTION_DATE));
						acAuctionSchedule.setAuctionDateFormatted(jsonObject.getString(Constant.ITEM_AUCTION_DATE_FORMATTED));
						
						Kpknl kpknl = new Kpknl();
						kpknl.setId(jsonObject.getString(Constant.ITEM_KPKNL_ID));
						kpknl.setName(jsonObject.getString(Constant.ITEM_KPKNL_NAME));
						kpknl.setAddress(jsonObject.getString(Constant.ITEM_KPKNL_ADDRESS));
						kpknl.setPhone1(jsonObject.getString(Constant.ITEM_KPKNL_PHONE1));
						kpknl.setPhone2(jsonObject.getString(Constant.ITEM_KPKNL_PHONE2));
						
						acAuctionSchedule.setKpknl(kpknl);
						
//						AuctionHall auctionHall = new AuctionHall();
//						auctionHall.setId(jsonObject.getString(Constant.ITEM_AUCTION_HALL_ID));
//						auctionHall.setName(jsonObject.getString(Constant.ITEM_AUCTION_HALL_NAME));
//						auctionHall.setAddress(jsonObject.getString(Constant.ITEM_AUCTION_HALL_ADDRESS));
//						auctionHall.setPhone1(jsonObject.getString(Constant.ITEM_AUCTION_HALL_PHONE1));
//						auctionHall.setPhone2(jsonObject.getString(Constant.ITEM_AUCTION_HALL_PHONE2));
//						
//						acAuctionSchedule.setAcAuctionHall(auctionHall);
						
						list.add(acAuctionSchedule);
						
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			Log.d(Constant.APP_TAG, e.getMessage());
		}
		
		return list;
		
	}
	
	public static LinkedList<AuctionSchedule> getLoadmoreListAuctionSchedule(String response, LinkedList<AuctionSchedule> list){
		try {
			JSONObject object = new JSONObject(response);
			if (object.getJSONObject(Constant.RESULT).getInt(Constant.RESULT_CODE)==0) {
				
				JSONArray items = object.getJSONArray(Constant.DATA);
				if (items.length()>0) {
					AuctionSchedule acAuctionSchedule = null;
					for (int i = 0; i < items.length(); i++) {
						JSONObject jsonObject = items.getJSONObject(i);
						acAuctionSchedule = new AuctionSchedule();
						acAuctionSchedule.setAuctionScheduleId(jsonObject.getString(Constant.AUCTION_SCHEDULE_ID));
						acAuctionSchedule.setAuctionDate(jsonObject.getString(Constant.ITEM_AUCTION_DATE));
						acAuctionSchedule.setAuctionDateFormatted(jsonObject.getString(Constant.ITEM_AUCTION_DATE_FORMATTED));
						
						Kpknl kpknl = new Kpknl();
						kpknl.setId(jsonObject.getString(Constant.ITEM_KPKNL_ID));
						kpknl.setName(jsonObject.getString(Constant.ITEM_KPKNL_NAME));
						kpknl.setAddress(jsonObject.getString(Constant.ITEM_KPKNL_ADDRESS));
						kpknl.setPhone1(jsonObject.getString(Constant.ITEM_KPKNL_PHONE1));
						kpknl.setPhone2(jsonObject.getString(Constant.ITEM_KPKNL_PHONE2));
						
						acAuctionSchedule.setKpknl(kpknl);
						
//						AuctionHall auctionHall = new AuctionHall();
//						auctionHall.setId(jsonObject.getString(Constant.ITEM_AUCTION_HALL_ID));
//						auctionHall.setName(jsonObject.getString(Constant.ITEM_AUCTION_HALL_NAME));
//						auctionHall.setAddress(jsonObject.getString(Constant.ITEM_AUCTION_HALL_ADDRESS));
//						auctionHall.setPhone1(jsonObject.getString(Constant.ITEM_AUCTION_HALL_PHONE1));
//						auctionHall.setPhone2(jsonObject.getString(Constant.ITEM_AUCTION_HALL_PHONE2));
//						
//						acAuctionSchedule.setAcAuctionHall(auctionHall);
						
						list.addLast(acAuctionSchedule);
						
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			Log.d(Constant.APP_TAG, e.getMessage());
		}
		
		return list;
		
	}
	
	public static boolean getIsNeedLoadmore(String response){
		boolean loadMore = false;
		try {
			JSONObject object = new JSONObject(response);
			if (object.getJSONObject(Constant.RESULT).getInt(Constant.RESULT_CODE)==0) {
				
				JSONArray items = object.getJSONArray(Constant.DATA);
				if (items.length()>0) {
					loadMore = true;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			Log.d(Constant.APP_TAG, e.getMessage());
			loadMore = false;
		}
		
		return loadMore;
		
	}
}
