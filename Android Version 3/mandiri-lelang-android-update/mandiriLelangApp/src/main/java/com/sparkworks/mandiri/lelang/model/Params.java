package com.sparkworks.mandiri.lelang.model;

import java.io.Serializable;

import com.sparkworks.mandiri.lelang.utils.ApiHelper.LelangType;

public class Params implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8150269012723442916L;
	
	String pageIndex, categoryId, provinceId, cityId, minPriceId, maxPriceId, 
	hotPrice, auctionDate, documentTypeId, address, orderBy, assetID, keyword;

	boolean type1, type2;

	public boolean isType1() {
		return type1;
	}

	public void setType1(boolean type1) {
		this.type1 = type1;
	}

	public boolean isType2() {
		return type2;
	}

	public void setType2(boolean type2) {
		this.type2 = type2;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getAssetID() {
		return assetID;
	}

	public void setAssetID(String assetID) {
		this.assetID = assetID;
	}

	public String getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(String pageIndex) {
		this.pageIndex = pageIndex;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getMinPriceId() {
		return minPriceId;
	}

	public void setMinPriceId(String minPriceId) {
		this.minPriceId = minPriceId;
	}

	public String getMaxPriceId() {
		return maxPriceId;
	}

	public void setMaxPriceId(String maxPriceId) {
		this.maxPriceId = maxPriceId;
	}

	public String getHotPrice() {
		return hotPrice;
	}

	public void setHotPrice(String hotPrice) {
		this.hotPrice = hotPrice;
	}

	public String getAuctionDate() {
		return auctionDate;
	}

	public void setAuctionDate(String auctionDate) {
		this.auctionDate = auctionDate;
	}

	public String getDocumentTypeId() {
		return documentTypeId;
	}

	public void setDocumentTypeId(String documentTypeId) {
		this.documentTypeId = documentTypeId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public static Params getParams(LelangType type, String pageIndex,
			String categoryId, String provinceId, String cityId, String minPriceId,
			String maxPriceId, String hotPrice, String auctionDate, String documentTypeId,
			String address, String orderBy){
		
		Params params = new Params();
		params.setPageIndex(pageIndex);
		params.setCategoryId(categoryId);
		params.setProvinceId(provinceId);
		params.setCityId(cityId);
		params.setMinPriceId(minPriceId);
		params.setMaxPriceId(maxPriceId);
		params.setHotPrice(hotPrice);
		params.setAuctionDate(auctionDate);
		params.setDocumentTypeId(documentTypeId);
		params.setAddress(address);
		params.setOrderBy(orderBy);
		
		return params;
	}
}
