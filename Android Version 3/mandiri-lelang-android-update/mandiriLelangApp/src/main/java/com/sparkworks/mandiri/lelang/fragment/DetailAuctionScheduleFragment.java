package com.sparkworks.mandiri.lelang.fragment;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.mandiri.lelang.aplikasi.DetilLelangItemActivity;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.adapter.EndlessAdapter;
import com.sparkworks.mandiri.lelang.adapter.LelangItemAdapter;
import com.sparkworks.mandiri.lelang.base.BaseFragment;
import com.sparkworks.mandiri.lelang.model.AuctionSchedule;
import com.sparkworks.mandiri.lelang.model.Item;
import com.sparkworks.mandiri.lelang.utils.ApiHelper;
import com.sparkworks.mandiri.lelang.utils.ApiHelper.LelangType;
import com.sparkworks.mandiri.lelang.utils.Constant;
import com.sparkworks.mandiri.lelang.utils.URLs;
import com.sparkworks.mandiri.lelang.utils.Utils;

public class DetailAuctionScheduleFragment extends BaseFragment{
	private ListView lvItem;
	private ProgressBar pbIndicator;
	private TextView txtErrorMessage, txtTitle;
	private LinkedList<Item> listItem;
	private AsyncHttpClient client;
	
	public static String FRAGMENT_TAG = "DetailAuctionScheduleFragment";

	public AuctionSchedule acAuctionSchedule;
	public LelangType lelangType;
	
	public static String AUCTION_KEY = "auction";
	public static String LELANG_TYPE_KEY = "lelangType";
	public static String RESPONSE_KEY = "response";
	
	private int pageIndex = 0;
	private int currentSelection = 0;
	private boolean needLoadMore = false;
	private LoadMoreLelangItem loadMoreLelangItem;
	private List<NameValuePair> listParams;
	
	public LelangType getLelangType() {
		return lelangType;
	}

	public void setLelangType(LelangType lelangType) {
		this.lelangType = lelangType;
	}

	public AuctionSchedule getAcAuctionSchedule() {
		return acAuctionSchedule;
	}

	public void setAcAuctionSchedule(AuctionSchedule acAuctionSchedule) {
		this.acAuctionSchedule = acAuctionSchedule;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		initializeLibs();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_detail_auction_by_date, container, false);
		initializeViews(view);
		setHasOptionsMenu(true);
		
		getActivity().getActionBar().setDisplayUseLogoEnabled(false);
        getActivity().getActionBar().setDisplayShowHomeEnabled(true);
        getActivity().getActionBar().setIcon(R.drawable.logo_home);
        getActivity().getActionBar().setTitle("");
        getActivity().getActionBar().setHomeButtonEnabled(true);
		
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initializeProcess();
		
		if (savedInstanceState != null) {
			setAcAuctionSchedule((AuctionSchedule)savedInstanceState.getSerializable(AUCTION_KEY));
			setLelangType((LelangType)savedInstanceState.getSerializable(LELANG_TYPE_KEY));
		} 
		
		initializeRequest();
		initializeActions();
	}
	
	@Override
	public void initializeViews(View view) {
		// TODO Auto-generated method stub
		super.initializeViews(view);
		lvItem = (ListView)view.findViewById(R.id.lvLelangItem);
		pbIndicator = (ProgressBar)view.findViewById(R.id.pb_indicator);
		txtErrorMessage = (TextView)view.findViewById(R.id.txtErrorMessage);
		txtTitle = (TextView)view.findViewById(R.id.txt_title_page);
	}
	
	@Override
	public void initializeLibs() {
		// TODO Auto-generated method stub
		super.initializeLibs();
		client = new AsyncHttpClient(true, 80, 443);
		listItem = new LinkedList<Item>();
	}
	
	@Override
	public void initializeRequest() {
		// TODO Auto-generated method stub
		super.initializeRequest();
		if (Utils.isConnectInet(getActivity())) {
			pbIndicator.setVisibility(View.VISIBLE);
			lvItem.setVisibility(View.GONE);
			txtErrorMessage.setVisibility(View.GONE);
			
			currentSelection = 0;
			
			listParams = ApiHelper.
					getAuctionByScheduleParams(Integer.valueOf(getAcAuctionSchedule().
							getAuctionScheduleId()));
			String request = ApiHelper.buildAPIURLMessage(URLs.ASSET_AUCTION_SCHEDULE, 
					null, listParams);
			Log.d(Constant.APP_TAG, request);
			client.get(request, new AsyncHttpResponseHandler(){
				@Override
				public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
					// TODO Auto-generated method stub
					super.onSuccess(arg0, arg1, arg2);
					pbIndicator.setVisibility(View.GONE);
					lvItem.setVisibility(View.VISIBLE);
					
					String response = new String(arg2);
					
					bindDataToListView(response, false);
				}
				
				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2,
						Throwable arg3) {
					// TODO Auto-generated method stub
					super.onFailure(arg0, arg1, arg2, arg3);
					pbIndicator.setVisibility(View.GONE);
					lvItem.setVisibility(View.GONE);
					txtErrorMessage.setVisibility(View.VISIBLE);
					txtErrorMessage.setText(R.string.error_loading);
				}
			});
		} else {
			pbIndicator.setVisibility(View.GONE);
			lvItem.setVisibility(View.GONE);
			txtErrorMessage.setVisibility(View.VISIBLE);
			txtErrorMessage.setText(R.string.no_internet);
		}
	}
	
	protected void bindDataToListView(String result, boolean isLoadmore) {
		// TODO Auto-generated method stub
		if (result == null || result.equals("")) {
			initializeRequest();
		}else{
			if (isLoadmore) {
				listItem = Item.getListLoadMoreItem(result, listItem);
			}else{
				listItem = Item.getListItem(result);
			}
			
			needLoadMore = Item.getIsNeedLoadMore(result);
			
			setToAdapter(isLoadmore);
		}
	}

	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		super.initializeProcess();
		
		String date = getAcAuctionSchedule().getAuctionDateFormatted();
		//String auctionDate[] = date.split(",");
		
		txtTitle.setText("Lelang pada tanggal : "+date+" ("+getAcAuctionSchedule().getKpknl().getName()+")");
	}
	
	private void setToAdapter(boolean isLoadmore) {
		// TODO Auto-generated method stub
		if (listItem == null || listItem.size()<0) {
			getActivity().finish();
		} else {
			LelangItemAdapter lelangItemAdapter = new LelangItemAdapter(getActivity(), listItem, getLelangType());
			pageIndex+=1;
			
			if (needLoadMore) {
				loadMoreLelangItem = new LoadMoreLelangItem(lelangItemAdapter);
				lvItem.setAdapter(loadMoreLelangItem);
				loadMoreLelangItem.notifyDataSetChanged();
			} else {
				lvItem.setAdapter(lelangItemAdapter);
				lelangItemAdapter.notifyDataSetChanged();
				lvItem.setSelection(listItem.size());
				isLoadmore = false;
			}
			
			if (isLoadmore) {
				lvItem.setSelection(currentSelection);	
			}
		}
	}
	
	@Override
	public void initializeActions() {
		// TODO Auto-generated method stub
		super.initializeActions();
		lvItem.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				toDetilItem(arg2);
			}
		});
	}
	
	protected void toDetilItem(int position) {
		// TODO Auto-generated method stub
		DetilLelangItemActivity.toDetilLelangActivity(getActivity(), listItem.get(position));
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_search_result, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		if (item.getItemId()==R.id.action_refresh) {
			initializeRequest();
		}
		
		if (item.getItemId()==android.R.id.home) {
			getActivity().finish();
		}
		
		return true;
	}
	
	private void closeFragment(){
		
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.remove(this);
        transaction.commit();
        getFragmentManager().popBackStack();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		outState.putSerializable(AUCTION_KEY, getAcAuctionSchedule());
		outState.putSerializable(LELANG_TYPE_KEY, getLelangType());
		super.onSaveInstanceState(outState);
	}
	
	class LoadMoreLelangItem extends EndlessAdapter{
		
		String responseLoadMore = "";
		
		public LoadMoreLelangItem(LelangItemAdapter adapterLoadmore) {
			// TODO Auto-generated constructor stub
			super(adapterLoadmore);
		}

		@Override
		protected boolean cacheInBackground() throws Exception {
			// TODO Auto-generated method stub
			
			boolean isValid = true;
			listParams.set(0, new BasicNameValuePair("pageIndex", String.valueOf(pageIndex)));
			String loadmoreRequest = ApiHelper.buildAPIURLMessage(URLs.ASSET_AUCTION_SCHEDULE,
					null, listParams);
			Log.d(Constant.APP_TAG, loadmoreRequest);
			responseLoadMore = Utils.get(loadmoreRequest, userPreference.getToken());
			if (responseLoadMore == null || responseLoadMore.equals("")) {
				isValid = false;
			} else {
				try {
					JSONObject object = new JSONObject(responseLoadMore);
					if (object.getJSONArray(Constant.DATA).length()<0) {
						isValid = false;
					}else{
						currentSelection = listItem.size();
					}
				} catch (Exception e) {
					// TODO: handle exception
					isValid  = false;
				}
			}
			return isValid;
		}

		@Override
		protected void appendCachedData() {
			// TODO Auto-generated method stub
			bindDataToListView(responseLoadMore, true);
		}
		
		@Override
		protected View getPendingView(ViewGroup parent) {
			// TODO Auto-generated method stub
			View row = getActivity().getLayoutInflater().inflate(R.layout.loadmore_view, null);
			return row;
		}
	}
	
}