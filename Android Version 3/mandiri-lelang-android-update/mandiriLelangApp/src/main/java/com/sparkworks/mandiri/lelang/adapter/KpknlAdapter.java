package com.sparkworks.mandiri.lelang.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.model.KpknlItem;

import java.util.ArrayList;

/**
 * Created by sidiqpermana on 12/1/16.
 */

public class KpknlAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<KpknlItem> listKpknlItem;
    private OnCheckAllCallback onCheckAllCallback;

    public KpknlAdapter(Activity activity) {
        this.activity = activity;
    }

    public OnCheckAllCallback getOnCheckAllCallback() {
        return onCheckAllCallback;
    }

    public void setOnCheckAllCallback(OnCheckAllCallback onCheckAllCallback) {
        this.onCheckAllCallback = onCheckAllCallback;
    }

    public ArrayList<KpknlItem> getListKpknlItem() {
        return listKpknlItem;
    }

    public void setListKpknlItem(ArrayList<KpknlItem> listKpknlItem) {
        this.listKpknlItem = listKpknlItem;
    }

    @Override
    public int getCount() {
        return getListKpknlItem().size();
    }

    @Override
    public Object getItem(int i) {
        return getListKpknlItem().get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null){
            LayoutInflater inflater = LayoutInflater.from(activity);
            view = inflater.inflate(R.layout.item_province, null);
            final KpknlAdapter.ViewHolder holder = new KpknlAdapter.ViewHolder();
            holder.tvName = (TextView)view.findViewById(R.id.tv_item_province);
            holder.cbItem = (CheckBox)view.findViewById(R.id.cb_item_province);
            holder.cbItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    KpknlItem item = (KpknlItem) holder.cbItem.getTag();
                    item.setChecked(compoundButton.isChecked());

                    if (!compoundButton.isChecked()){
                        getOnCheckAllCallback().onCheckAll();
                    }
                }
            });
            view.setTag(holder);
            holder.cbItem.setTag(getListKpknlItem().get(i));
        }else{
            ((KpknlAdapter.ViewHolder)view.getTag()).cbItem.setTag(getListKpknlItem().get(i));
        }

        KpknlAdapter.ViewHolder holder = (KpknlAdapter.ViewHolder)view.getTag();
        holder.tvName.setText(getListKpknlItem().get(i).getName());
        holder.cbItem.setChecked(getListKpknlItem().get(i).isChecked());

        return view;
    }

    static class ViewHolder{
        CheckBox cbItem;
        TextView tvName;
    }

    public interface OnCheckAllCallback{
        void onCheckAll();
    }
}

