package com.sparkworks.mandiri.lelang.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.sparkworks.mandiri.lelang.utils.Constant;

public class Item implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String assetId, code, postedDate, postedDateFormatted,
	auctionDate, auctionDateFormatted, categoryId, categoryName, address, 
	provinceId, provinceName, cityId, cityName, completeAddress, oldPrice, newPrice, 
	photoUrl, documentTypeId, documentTypeName, transferInformation, latitude, longitude, photo1Url, 
	photo2Url, photo3Url, photo4Url, photo5Url, terms, url, webUrl, twitterText, emailSubject, 
	emailText, emailAuctionUrl;
	private boolean isHotPrice;
	private boolean isEmailAuction;
	private boolean isFavorite;
	private boolean type1; //Jual Sukarela
	private boolean type2; //Lelang
	private int typeAgunan;

	public Branch branch;
	public Kpknl kpknl;
	public AuctionHall auctionHall;
	public ArrayList<ExtraFields> listExtraFields;
	public ArrayList<String> listImages;

	public int getTypeAgunan() {
		return typeAgunan;
	}

	public void setTypeAgunan(int typeAgunan) {
		this.typeAgunan = typeAgunan;
	}

	public boolean isType1() {
		return type1;
	}

	public void setType1(boolean type1) {
		this.type1 = type1;
	}

	public boolean isType2() {
		return type2;
	}

	public void setType2(boolean type2) {
		this.type2 = type2;
	}

	public String getEmailAuctionUrl() {
		return emailAuctionUrl;
	}

	public void setEmailAuctionUrl(String emailAuctionUrl) {
		this.emailAuctionUrl = emailAuctionUrl;
	}

	public boolean isEmailAuction() {
		return isEmailAuction;
	}

	public void setEmailAuction(boolean emailAuction) {
		isEmailAuction = emailAuction;
	}

	public boolean isFavorite() {
		return isFavorite;
	}

	public void setFavorite(boolean favorite) {
		isFavorite = favorite;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getWebUrl() {
		return webUrl;
	}

	public void setWebUrl(String webUrl) {
		this.webUrl = webUrl;
	}

	public String getTwitterText() {
		return twitterText;
	}

	public void setTwitterText(String twitterText) {
		this.twitterText = twitterText;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getEmailText() {
		return emailText;
	}

	public void setEmailText(String emailText) {
		this.emailText = emailText;
	}

	public boolean isHotPrice() {
		return isHotPrice;
	}

	public void setHotPrice(boolean isHotPrice) {
		this.isHotPrice = isHotPrice;
	}

	public ArrayList<String> getListImages() {
		return listImages;
	}

	public void setListImages(ArrayList<String> listImages) {
		this.listImages = listImages;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getDocumentTypeId() {
		return documentTypeId;
	}

	public void setDocumentTypeId(String documentTypeId) {
		this.documentTypeId = documentTypeId;
	}

	public String getDocumentTypeName() {
		return documentTypeName;
	}

	public void setDocumentTypeName(String documentTypeName) {
		this.documentTypeName = documentTypeName;
	}

	public String getTransferInformation() {
		return transferInformation;
	}

	public void setTransferInformation(String transferInformation) {
		this.transferInformation = transferInformation;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPhoto1Url() {
		return photo1Url;
	}

	public void setPhoto1Url(String photo1Url) {
		this.photo1Url = photo1Url;
	}

	public String getPhoto2Url() {
		return photo2Url;
	}

	public void setPhoto2Url(String photo2Url) {
		this.photo2Url = photo2Url;
	}

	public String getPhoto3Url() {
		return photo3Url;
	}

	public void setPhoto3Url(String photo3Url) {
		this.photo3Url = photo3Url;
	}

	public String getPhoto4Url() {
		return photo4Url;
	}

	public void setPhoto4Url(String photo4Url) {
		this.photo4Url = photo4Url;
	}

	public String getPhoto5Url() {
		return photo5Url;
	}

	public void setPhoto5Url(String photo5Url) {
		this.photo5Url = photo5Url;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public Kpknl getKpknl() {
		return kpknl;
	}

	public void setKpknl(Kpknl kpknl) {
		this.kpknl = kpknl;
	}

	public AuctionHall getAuctionHall() {
		return auctionHall;
	}

	public void setAuctionHall(AuctionHall auctionHall) {
		this.auctionHall = auctionHall;
	}

	public ArrayList<ExtraFields> getListExtraFields() {
		return listExtraFields;
	}

	public void setListExtraFields(ArrayList<ExtraFields> listExtraFields) {
		this.listExtraFields = listExtraFields;
	}

	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	public String getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(String postedDate) {
		this.postedDate = postedDate;
	}

	public String getPostedDateFormatted() {
		return postedDateFormatted;
	}

	public void setPostedDateFormatted(String postedDateFormatted) {
		this.postedDateFormatted = postedDateFormatted;
	}

	public String getAuctionDate() {
		return auctionDate;
	}

	public void setAuctionDate(String auctionDate) {
		this.auctionDate = auctionDate;
	}

	public String getAuctionDateFormatted() {
		return auctionDateFormatted;
	}

	public void setAuctionDateFormatted(String auctionDateFormatted) {
		this.auctionDateFormatted = auctionDateFormatted;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCompleteAddress() {
		return completeAddress;
	}

	public void setCompleteAddress(String completeAddress) {
		this.completeAddress = completeAddress;
	}

	public String getOldPrice() {
		return oldPrice;
	}

	public void setOldPrice(String oldPrice) {
		this.oldPrice = oldPrice;
	}

	public String getNewPrice() {
		return newPrice;
	}

	public void setNewPrice(String newPrice) {
		this.newPrice = newPrice;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
	
	public static LinkedList<Item> getListItem(String response){
		LinkedList<Item> listItem = null;
		try {
			JSONObject jsonObject = new JSONObject(response);
			if (jsonObject.getJSONObject(Constant.RESULT).getInt(Constant.RESULT_CODE)==0) {
				JSONArray jsonArray = jsonObject.getJSONArray(Constant.DATA);
				if (jsonArray.length()>0) {
					listItem = new LinkedList<Item>();
					Item item = null;
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject object = jsonArray.getJSONObject(i);
						
						item = new Item();
						item.setAssetId(object.getString(Constant.ITEM_ASSET_ID));
						item.setPostedDate(object.getString(Constant.ITEM_POSTED_DATE));
						item.setPostedDateFormatted(object.getString(Constant.ITEM_POSTED_FORMATTED));
						item.setAuctionDate(object.getString(Constant.ITEM_AUCTION_DATE));
						item.setAuctionDateFormatted(object.getString(Constant.ITEM_AUCTION_DATE_FORMATTED));
						item.setCategoryId(object.getString(Constant.ITEM_CATEGORY_ID));
						item.setCategoryName(object.getString(Constant.ITEM_CATEGORY_NAME));
						item.setAddress(object.getString(Constant.ITEM_ADDRESS));
						item.setProvinceId(object.getString(Constant.ITEM_PROVINCE_ID));
						item.setProvinceName(object.getString(Constant.ITEM_PROVINCE_NAME));
						item.setCityId(object.getString(Constant.ITEM_CITY_ID));
						item.setCityName(object.getString(Constant.ITEM_CITY_NAME));
						item.setCompleteAddress(object.getString(Constant.ITEM_COMPLETE_ADDRESS));
						item.setOldPrice(object.getString(Constant.ITEM_OLD_PRICE));
						item.setPhotoUrl(object.getString(Constant.ITEM_PHOTO_URL));
						item.setNewPrice(object.getString(Constant.ITEM_NEW_PRICE));
						item.setHotPrice(object.getBoolean(Constant.ITEM_ISHOT_PRICE));
						item.setEmailAuction(object.getBoolean(Constant.ITEM_IS_EMAIL_AUCTION));
						item.setFavorite(object.getBoolean(Constant.ITEM_IS_FAVORITE));
						item.setType1(object.optBoolean(Constant.ITEM_TYPE_1));
						item.setType2(object.optBoolean(Constant.ITEM_TYPE_2));
						item.setTypeAgunan(object.optInt(Constant.ITEM_TYPE));
						listItem.add(item);
					}
				}
			} 
		} catch (Exception e) {
			// TODO: handle exception
			Log.d(Constant.APP_TAG, e.getMessage());
			listItem = null;
		}
		
		return listItem;
	}
	
	public static int getTotalRecords(String response){
		int total = 0;
		try {
			JSONObject jsonObject = new JSONObject(response);
			if (jsonObject.getJSONObject(Constant.RESULT).getInt(Constant.RESULT_CODE)==0) {
				total = jsonObject.getInt(Constant.ITEM_TOTAL_RECORDS);
			}
		}catch(Exception e){
			Log.d(Constant.APP_TAG, e.getMessage());
		}
		return total;
	}
	
	public static LinkedList<Item> getListLoadMoreItem(String response, 
			LinkedList<Item> list){
		LinkedList<Item> listItem = list;
		try {
			JSONObject jsonObject = new JSONObject(response);
			if (jsonObject.getJSONObject(Constant.RESULT).getInt(Constant.RESULT_CODE)==0) {
				JSONArray jsonArray = jsonObject.getJSONArray(Constant.DATA);
				if (jsonArray.length()>0) {
					Item item = null;
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject object = jsonArray.getJSONObject(i);
						
						item = new Item();
						item.setAssetId(object.getString(Constant.ITEM_ASSET_ID));
						item.setPostedDate(object.getString(Constant.ITEM_POSTED_DATE));
						item.setPostedDateFormatted(object.getString(Constant.ITEM_POSTED_FORMATTED));
						item.setAuctionDate(object.getString(Constant.ITEM_AUCTION_DATE));
						item.setAuctionDateFormatted(object.getString(Constant.ITEM_AUCTION_DATE_FORMATTED));
						item.setCategoryId(object.getString(Constant.ITEM_CATEGORY_ID));
						item.setCategoryName(object.getString(Constant.ITEM_CATEGORY_NAME));
						item.setAddress(object.getString(Constant.ITEM_ADDRESS));
						item.setProvinceId(object.getString(Constant.ITEM_PROVINCE_ID));
						item.setProvinceName(object.getString(Constant.ITEM_PROVINCE_NAME));
						item.setCityId(object.getString(Constant.ITEM_CITY_ID));
						item.setCityName(object.getString(Constant.ITEM_CITY_NAME));
						item.setCompleteAddress(object.getString(Constant.ITEM_COMPLETE_ADDRESS));
						item.setOldPrice(object.getString(Constant.ITEM_OLD_PRICE));
						item.setPhotoUrl(object.getString(Constant.ITEM_PHOTO_URL));
						item.setNewPrice(object.getString(Constant.ITEM_NEW_PRICE));
						item.setHotPrice(object.getBoolean(Constant.ITEM_ISHOT_PRICE));
						item.setEmailAuction(object.getBoolean(Constant.ITEM_IS_EMAIL_AUCTION));
						item.setFavorite(object.getBoolean(Constant.ITEM_IS_FAVORITE));
						item.setType1(object.optBoolean(Constant.ITEM_TYPE_1));
						item.setType2(object.optBoolean(Constant.ITEM_TYPE_2));
						item.setTypeAgunan(object.optInt(Constant.ITEM_TYPE));

						listItem.addLast(item);
						
					}
				}
			} 
		} catch (Exception e) {
			// TODO: handle exception
			Log.d(Constant.APP_TAG, e.getMessage());
		}
		
		return listItem;
	}
	
	public static boolean getIsNeedLoadMore(String response){
		boolean needLoadmore = false;
		try {
			JSONObject jsonObject = new JSONObject(response);
			if (jsonObject.getJSONObject(Constant.RESULT).getInt(Constant.RESULT_CODE)==0) {
				JSONArray jsonArray = jsonObject.getJSONArray(Constant.DATA);
				if (jsonArray.length()>0) {
					needLoadmore = true;
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			Log.d(Constant.APP_TAG, e.getMessage());
		}
		return needLoadmore;
	}
	
	public static Item getItemDetailData(String response){
		Item item = null;
		try {
			JSONObject jsonObject = new JSONObject(response);
			if (jsonObject.getJSONObject(Constant.RESULT).getInt(Constant.RESULT_CODE)==0) {
				JSONObject object = jsonObject.getJSONObject(Constant.DATA);
				item = new Item();
				item.setAssetId(object.getString(Constant.ITEM_ASSET_ID));
				item.setCode(object.getString(Constant.ITEM_ASSET_CODE));
				item.setPostedDate(object.getString(Constant.ITEM_POSTED_DATE));
				item.setPostedDateFormatted(object.getString(Constant.ITEM_POSTED_FORMATTED));
				item.setAuctionDate(object.getString(Constant.ITEM_AUCTION_DATE));
				item.setAuctionDateFormatted(object.getString(Constant.ITEM_AUCTION_DATE_FORMATTED));
				item.setCategoryId(object.getString(Constant.ITEM_CATEGORY_ID));
				item.setCategoryName(object.getString(Constant.ITEM_CATEGORY_NAME));
				item.setAddress(object.getString(Constant.ITEM_ADDRESS));
				item.setProvinceId(object.getString(Constant.ITEM_PROVINCE_ID));
				item.setProvinceName(object.getString(Constant.ITEM_PROVINCE_NAME));
				item.setCityId(object.getString(Constant.ITEM_CITY_ID));
				item.setCityName(object.getString(Constant.ITEM_CITY_NAME));
				item.setCompleteAddress(object.getString(Constant.ITEM_COMPLETE_ADDRESS));
				item.setOldPrice(object.getString(Constant.ITEM_OLD_PRICE));
				item.setNewPrice(object.getString(Constant.ITEM_NEW_PRICE));
				item.setDocumentTypeId(object.getString(Constant.ITEM_DOCUMENT_TYPE_ID));
				item.setDocumentTypeName(object.getString(Constant.ITEM_DOCUMENT_TYPE_NAME));
				item.setHotPrice(object.getBoolean(Constant.ITEM_ISHOT_PRICE));
				item.setTerms(object.getString(Constant.ITEM_TERMS));
				item.setUrl(object.getString(Constant.ITEM_URL));
				item.setWebUrl(object.getString(Constant.ITEM_WEB_URL));
				item.setTwitterText(object.getString(Constant.ITEM_TWITTER_TEXT));
				item.setEmailSubject(object.getString(Constant.ITEM_EMAIL_SUBJECT));
				item.setEmailText(object.getString(Constant.ITEM_EMAIL_TEXT));
                item.setFavorite(object.getBoolean(Constant.ITEM_IS_FAVORITE));
                item.setEmailAuction(object.getBoolean(Constant.ITEM_IS_EMAIL_AUCTION));
				item.setEmailAuctionUrl(object.getString(Constant.ITEM_EMAIL_AUCTION_URL));
				item.setTypeAgunan(object.optInt(Constant.ITEM_TYPE));
				
				Branch branch = new Branch();
				branch.setId(object.getString(Constant.ITEM_BRANCH_ID));
				branch.setName(object.getString(Constant.ITEM_BRANCH_NAME));
				branch.setAddress(object.getString(Constant.ITEM_BRANCH_ADDRESS));
				branch.setPhone1(object.getString(Constant.ITEM_BRANCH_PHONE_1));
				branch.setPhone2(object.getString(Constant.ITEM_BRANCH_PHONE_2));
				
				item.setBranch(branch);
				
				Kpknl kpknl = new Kpknl();
				kpknl.setId(object.getString(Constant.ITEM_KPKNL_ID));
				kpknl.setName(object.getString(Constant.ITEM_KPKNL_NAME));
				kpknl.setAddress(object.getString(Constant.ITEM_KPKNL_ADDRESS));
				kpknl.setPhone1(object.getString(Constant.ITEM_KPKNL_PHONE1));
				kpknl.setPhone2(object.getString(Constant.ITEM_KPKNL_PHONE2));
				
				item.setKpknl(kpknl);
				
				AuctionHall auctionHall = new AuctionHall();
				auctionHall.setId(object.getString(Constant.ITEM_AUCTION_HALL_ID));
				auctionHall.setName(object.getString(Constant.ITEM_AUCTION_HALL_NAME));
				auctionHall.setAddress(object.getString(Constant.ITEM_AUCTION_HALL_ADDRESS));
				auctionHall.setPhone1(object.getString(Constant.ITEM_AUCTION_HALL_PHONE1));
				auctionHall.setPhone2(object.getString(Constant.ITEM_AUCTION_HALL_PHONE2));
				
				item.setAuctionHall(auctionHall);
				
				item.setTransferInformation(object.getString(Constant.ITEM_TRANSFERINFORMATION));
				item.setLatitude(object.getString(Constant.ITEM_LATITUDE));
				item.setLongitude(object.getString(Constant.ITEM_LONGITUDE));
				
				item.setPhoto1Url(object.getString(Constant.ITEM_PHOTO1URL));
				item.setPhoto2Url(object.getString(Constant.ITEM_PHOTO2URL));
				item.setPhoto3Url(object.getString(Constant.ITEM_PHOTO3URL));
				item.setPhoto4Url(object.getString(Constant.ITEM_PHOTO4URL));
				item.setPhoto5Url(object.getString(Constant.ITEM_PHOTO5URL));
				
				ArrayList<String> images = new ArrayList<String>();
				
				if (!item.getPhoto1Url().equals("")) {
					images.add(item.getPhoto1Url());
				}
				
				if (!item.getPhoto2Url().equals("")) {
					images.add(item.getPhoto2Url());
				}
				
				if (!item.getPhoto3Url().equals("")) {
					images.add(item.getPhoto3Url());
				}
				
				if (!item.getPhoto4Url().equals("")) {
					images.add(item.getPhoto4Url());
				}
				
				if (!item.getPhoto5Url().equals("")) {
					images.add(item.getPhoto5Url());
				}
				
				
				item.setListImages(images);
				
				JSONArray arrayExtra = object.getJSONArray(Constant.ITEM_EXTRAFIELDS);
				if (arrayExtra.length()>0) {
					ArrayList<ExtraFields> listExtraFields = new ArrayList<ExtraFields>();
					ExtraFields extraFields = null;
					for (int i = 0; i < arrayExtra.length(); i++) {
						JSONObject exJsonObject = arrayExtra.getJSONObject(i);
						extraFields = new ExtraFields();
						extraFields.setFieldName(exJsonObject.getString(Constant.ITEM_EXTRAFIELDS_FIELDNAME));
						extraFields.setValue(exJsonObject.getString(Constant.ITEM_EXTRAFIELDS_VALUE));
						listExtraFields.add(extraFields);
					}
					
					item.setListExtraFields(listExtraFields);
				}						
				
			}
		} catch (Exception e) {
			// TODO: handle exception
			Log.d(Constant.APP_TAG, e.getMessage());
		}
		
		
		return item;
	}
}
