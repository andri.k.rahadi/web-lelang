package com.sparkworks.mandiri.lelang.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mandiri.lelang.aplikasi.MainActivity;
import com.mandiri.lelang.aplikasi.R;
import com.mandiri.lelang.aplikasi.StaticPageActivity;
import com.mandiri.lelang.aplikasi.VideoGalleryActivity;
import com.sparkworks.mandiri.lelang.model.NavDrawerItem;

public class NavDrawerListAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<NavDrawerItem> navDrawerItems;
	public static Typeface tf;
	private OnSubmenuClickedCallback callback;

	private boolean isSubMenuShown = false;

	public static String SYARAT_KETENTUAN = "Syarat & Ketentuan";
	public static String FAQ = "FAQ";
	public static String TENTANG_KAMI = "Tentang Kami";
	public static String VIDEO = "Video Gallery";

	public NavDrawerListAdapter(Context context, ArrayList<NavDrawerItem> navDrawerItems){
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	public OnSubmenuClickedCallback getCallback() {
		return callback;
	}

	public void setCallback(OnSubmenuClickedCallback callback) {
		this.callback = callback;
	}

	public boolean isSubMenuShown() {
		return isSubMenuShown;
	}

	public void setSubMenuShown(boolean subMenuShown) {
		isSubMenuShown = subMenuShown;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {		
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Viewholder viewholder = null;
		tf = Typeface.createFromAsset(context.getAssets(),
				"fonts/MyriadPro-Regular.otf");
		if (convertView == null) {
			viewholder = new Viewholder();
			LayoutInflater mInflater = (LayoutInflater)
					context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.drawer_list_item, null);
			viewholder.imgIcon = (ImageView) convertView.findViewById(R.id.icon);
			viewholder.tvMenuName = (TextView) convertView.findViewById(R.id.title);
			viewholder.lnSubmenu = (LinearLayout)convertView.findViewById(R.id.ln_submenu);
			viewholder.imgExpandCollapse = (ImageView)convertView.findViewById(R.id.img_expand_collapse);
			convertView.setTag(viewholder);
        }else{
			viewholder = (Viewholder)convertView.getTag();
		}

		viewholder.tvMenuName.setTypeface(tf);

		viewholder.imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
		viewholder.tvMenuName.setText(navDrawerItems.get(position).getTitle());

		viewholder.imgExpandCollapse.setVisibility(View.GONE);
		viewholder.imgIcon.setVisibility(View.VISIBLE);
		if (position == 3){
			viewholder.imgIcon.setVisibility(View.GONE);
			viewholder.imgExpandCollapse.setVisibility(View.VISIBLE);
			if (isSubMenuShown()){
				viewholder.lnSubmenu.removeAllViews();
				viewholder.lnSubmenu.setVisibility(View.VISIBLE);
				viewholder.imgExpandCollapse.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_arrow_drop_up));

				viewholder.lnSubmenu.addView(addSubmenu(SYARAT_KETENTUAN, R.drawable.ic_syarat_ketentuan));
				viewholder.lnSubmenu.addView(addSubmenu(FAQ, R.drawable.ic_faq));
				viewholder.lnSubmenu.addView(addSubmenu(TENTANG_KAMI, R.drawable.ic_user));
				viewholder.lnSubmenu.addView(addSubmenu(VIDEO, R.drawable.ic_video_library));
			}else{
				viewholder.lnSubmenu.removeAllViews();
				viewholder.lnSubmenu.setVisibility(View.GONE);
				viewholder.imgExpandCollapse.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_arrow_drop_down));
			}
		}

        return convertView;
	}

	private View addSubmenu(final String title, int icon){
		View view = LayoutInflater.from(context).inflate(R.layout.drawer_list_item, null);
		ImageView imgIcon = (ImageView)view.findViewById(R.id.icon);
		TextView tvTitle = (TextView)view.findViewById(R.id.title);

		imgIcon.setImageResource(icon);
		tvTitle.setText(title);

		view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (title.equalsIgnoreCase(SYARAT_KETENTUAN)){
					getCallback().onSyaratKetentuanClicked();
				}else if (title.equalsIgnoreCase(FAQ)){
					getCallback().onFaqClicked();
				}else if (title.equalsIgnoreCase(TENTANG_KAMI)){
					getCallback().onTentangKamiClicked();
				}else if (title.equalsIgnoreCase(VIDEO)){
					getCallback().onVideoGalleryClicked();
				}
			}
		});

		return view;
	}

	static class Viewholder{
		ImageView imgIcon;
		TextView tvMenuName;
		LinearLayout lnSubmenu;
		ImageView imgExpandCollapse;
	}

	public interface OnSubmenuClickedCallback{
		void onSyaratKetentuanClicked();
		void onFaqClicked();
		void onTentangKamiClicked();
		void onVideoGalleryClicked();
	}
}
