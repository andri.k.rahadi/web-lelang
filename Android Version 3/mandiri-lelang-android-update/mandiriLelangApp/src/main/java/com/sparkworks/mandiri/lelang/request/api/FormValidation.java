package com.sparkworks.mandiri.lelang.request.api;

/**
 * Created by sidiqpermana on 11/24/16.
 */

public interface FormValidation {
    boolean isValidParams();
}
