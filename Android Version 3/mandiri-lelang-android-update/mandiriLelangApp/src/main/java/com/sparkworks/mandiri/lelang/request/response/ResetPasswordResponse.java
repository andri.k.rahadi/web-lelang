package com.sparkworks.mandiri.lelang.request.response;

import com.google.gson.annotations.SerializedName;
import com.sparkworks.mandiri.lelang.request.model.ResetPasswordData;

/**
 * Created by sidiqpermana on 11/29/16.
 */

public class ResetPasswordResponse extends BaseResponse {
    @SerializedName("data")
    private ResetPasswordData data;

    public ResetPasswordData getData() {
        return data;
    }

    public void setData(ResetPasswordData data) {
        this.data = data;
    }
}
