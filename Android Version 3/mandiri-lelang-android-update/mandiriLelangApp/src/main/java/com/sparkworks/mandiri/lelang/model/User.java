package com.sparkworks.mandiri.lelang.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sidiqpermana on 11/11/16.
 */

public class User implements Parcelable {
    private String name;
    private String email;
    private String birthDate;
    private String birthPlace;
    private String phoneNo;
    private int provinceId;
    private int cityId;
    private String idNo;
    private String password;
    private String confirmPassword;
    private String address;
    private String oldPassword;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.birthDate);
        dest.writeString(this.birthPlace);
        dest.writeString(this.phoneNo);
        dest.writeInt(this.provinceId);
        dest.writeInt(this.cityId);
        dest.writeString(this.idNo);
        dest.writeString(this.password);
        dest.writeString(this.confirmPassword);
        dest.writeString(this.address);
        dest.writeString(this.oldPassword);
    }

    public User() {
    }

    protected User(Parcel in) {
        this.name = in.readString();
        this.email = in.readString();
        this.birthDate = in.readString();
        this.birthPlace = in.readString();
        this.phoneNo = in.readString();
        this.provinceId = in.readInt();
        this.cityId = in.readInt();
        this.idNo = in.readString();
        this.password = in.readString();
        this.confirmPassword = in.readString();
        this.address = in.readString();
        this.oldPassword = in.readString();
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
