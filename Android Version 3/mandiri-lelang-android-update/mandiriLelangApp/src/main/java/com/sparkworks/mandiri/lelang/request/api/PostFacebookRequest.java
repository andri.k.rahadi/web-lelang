package com.sparkworks.mandiri.lelang.request.api;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.request.model.FacebookData;
import com.sparkworks.mandiri.lelang.request.response.PostFacebookResponse;
import com.sparkworks.mandiri.lelang.utils.URLs;
import com.sparkworks.mandiri.lelang.utils.Utils;

import org.apache.http.Header;

/**
 * Created by sidiqpermana on 11/29/16.
 */

public class PostFacebookRequest extends BaseApi {
    private AsyncHttpClient asyncHttpClient;
    private Context context;
    private FacebookData data;
    private OnPostFacebookRequestListener onPostFacebookRequestListener;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public FacebookData getData() {
        return data;
    }

    public void setData(FacebookData data) {
        this.data = data;
    }

    public OnPostFacebookRequestListener getOnPostFacebookRequestListener() {
        return onPostFacebookRequestListener;
    }

    public void setOnPostFacebookRequestListener(OnPostFacebookRequestListener onPostFacebookRequestListener) {
        this.onPostFacebookRequestListener = onPostFacebookRequestListener;
    }

    @Override
    public void callApi() {
        super.callApi();
        if (Utils.isConnectInet(getContext())){
            asyncHttpClient = getAsyncHttpClient(getUserPreference(getContext()).getToken());

            RequestParams params = new RequestParams();
            params.put("fullName", getData().getFullName());
            params.put("email", getData().getEmail());
            params.put("id", getData().getId());
            params.put("url", getData().getUrl());
            params.put("token", getData().getToken());

            asyncHttpClient.post(URLs.SERVER_FB, params, new AsyncHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    super.onSuccess(statusCode, headers, responseBody);
                    String s = new String(responseBody);
                    Log.d("Facebook", s);
                    Gson gson = new Gson();
                    PostFacebookResponse postFacebookResponse = gson.fromJson(s, PostFacebookResponse.class);
                    if (postFacebookResponse.getResultCode().getCode() == CODE_SUCCESS){
                        if (!TextUtils.isEmpty(postFacebookResponse.getLoginData().getToken())){
                            getOnPostFacebookRequestListener().onPostFacebookSuccess(postFacebookResponse);
                        }else{
                            getOnPostFacebookRequestListener().onPostFacebookFailed("Tidak dapat melakukan authentikasi melalui facebook");
                        }
                    }else{
                        getOnPostFacebookRequestListener().onPostFacebookFailed(postFacebookResponse.getResultCode().getMessage());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    super.onFailure(statusCode, headers, responseBody, error);
                    getOnPostFacebookRequestListener().onPostFacebookFailed(getContext().getString(R.string.error_unable_connect_to_server));
                }
            });

        }else{
            getOnPostFacebookRequestListener().onPostFacebookFailed(getContext().getString(R.string.no_internet));
        }
    }

    @Override
    public void cancel() {
        super.cancel();
        if (asyncHttpClient != null){
            asyncHttpClient.cancelRequests(getContext(), true);
        }
    }

    @Override
    public void retry() {
        super.retry();
        callApi();
    }

    public interface OnPostFacebookRequestListener{
        void onPostFacebookSuccess(PostFacebookResponse postFacebookResponse);
        void onPostFacebookFailed(String errorMessage);
    }
}
