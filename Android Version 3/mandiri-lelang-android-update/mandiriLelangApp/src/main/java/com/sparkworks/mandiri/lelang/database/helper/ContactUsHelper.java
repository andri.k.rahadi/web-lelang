package com.sparkworks.mandiri.lelang.database.helper;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.sparkworks.mandiri.lelang.database.DataBaseHelper;
import com.sparkworks.mandiri.lelang.model.MasterData;

public class ContactUsHelper {
	
	private static String DATABASE_TABLE = DataBaseHelper.TABLE_CONTACTUS_NAME;
	
	private Context context;
	
	private DataBaseHelper dataBaseHelper;
	
	private SQLiteDatabase database;
	
	public ContactUsHelper(Context context){
		this.context = context;
	}
	
	public ContactUsHelper open() throws SQLException{
		dataBaseHelper = new DataBaseHelper(context);
		database = dataBaseHelper.getWritableDatabase();
		return this;
		
	}
	
	public void close(){
		dataBaseHelper.close();
	}
	
	public Cursor loadAllDatas(){
		
		return database.rawQuery("SELECT * FROM "+DATABASE_TABLE+
				" WHERE "+DataBaseHelper.FIELD_CONTACTUS_IS_ACTIVE+"='1' ORDER BY "+DataBaseHelper.FIELD_CONTACTUS_ID, null);
	}
	
	public ArrayList<MasterData> loadAllData(){
		ArrayList<MasterData> arrayList = new ArrayList<MasterData>();
		Cursor cursor = loadAllDatas();
		//startManagingCursor(cursor);
		cursor.moveToFirst();
		MasterData masterData;
		if (cursor.getCount()>0) {
			do {
				
				masterData = new MasterData();
				masterData.set_id(cursor.getInt(0));
				masterData.setId(cursor.getString(1));
				masterData.setName(cursor.getString(2));
				masterData.setDescription(cursor.getString(3));
				masterData.setIsActive(cursor.getString(4));
				masterData.setFlag(cursor.getString(5));
				
				arrayList.add(masterData);
				cursor.moveToNext();
				
			} while (!cursor.isAfterLast());
		} 
		cursor.close();
		return arrayList;
	}
	
	public boolean isContactUsExist(String id){
		Cursor cursor = database.rawQuery("SELECT "+DataBaseHelper.FIELD_CONTACTUS_ID+" FROM "+DATABASE_TABLE+" WHERE "+
				DataBaseHelper.FIELD_CONTACTUS_ID+"='"+id+"'", null);
		boolean exists = (cursor.getCount()>0);
		cursor.close();
		return exists;
	}
	
	public long insert(String id, String name, String description, String isActive, String flag){
		ContentValues initialValues = createContent(id, name, description, isActive, flag);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}
	
	private ContentValues createContent(String id, String name, String description, String isActive, String flag){
		ContentValues values = new ContentValues();
		values.put(DataBaseHelper.FIELD_CONTACTUS_ID, id);
		values.put(DataBaseHelper.FIELD_CONTACTUS_NAME, name);
		values.put(DataBaseHelper.FIELD_CONTACTUS_DESCRIPTION, description);
		values.put(DataBaseHelper.FIELD_CONTACTUS_IS_ACTIVE, isActive);
		values.put(DataBaseHelper.FIELD_CONTACTUS_FLAG, flag);
		
		return values;
	}
	
	public void update(String id, String name, String description, String isActive, String flag){
		ContentValues args = new ContentValues();
	    args.put(DataBaseHelper.FIELD_CONTACTUS_ID, id);
	    args.put(DataBaseHelper.FIELD_CONTACTUS_NAME, name);
	    args.put(DataBaseHelper.FIELD_CONTACTUS_DESCRIPTION, description);
	    args.put(DataBaseHelper.FIELD_CONTACTUS_IS_ACTIVE, isActive);
	    args.put(DataBaseHelper.FIELD_CONTACTUS_FLAG, flag);
	    database.update(DATABASE_TABLE, args, DataBaseHelper.FIELD_CONTACTUS_ID + "=" + id, null);
	}
	
	public void hapusKata(int id){
		database.delete(DataBaseHelper.TABLE_CONTACTUS_NAME, "_id = '"+id+"'", null);
	}
}
