package com.sparkworks.mandiri.lelang.utils;

import java.io.IOException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.net.Uri;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.webkit.URLUtil;

public class Utils {
	public static void phoneDial(Activity activity, String phoneNo){
		Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+phoneNo));
		activity.startActivity(intent);
	}
	
	public static String getHmacSha256(String value, String key)
			throws InvalidKeyException, NoSuchAlgorithmException {
		javax.crypto.Mac mac = javax.crypto.Mac.getInstance("HmacSHA256");
		javax.crypto.spec.SecretKeySpec secret = new javax.crypto.spec.SecretKeySpec(
				key.getBytes(), "HmacSHA256");
		mac.init(secret);
		byte[] digest = mac.doFinal(value.getBytes());
		int len = digest.length;
		StringBuilder sb = new StringBuilder(len << 1);
		for (int i = 0; i < len; i++) {
			sb.append(Character.forDigit((digest[i] & 0xf0) >> 4, 16));
			sb.append(Character.forDigit(digest[i] & 0x0f, 16));
		}
		return sb.toString();
	}

	public static String getUnHashesValue(String params) {
		return params + "|" + Constant.SECRET;
	}

	public static String getHashedToken(String data) {
		String token = "";
		try {
			token = getHmacSha256(getUnHashesValue(data.trim()),
					Constant.PRIVATE);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			token = "";
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			token = "";
		}

		return token;
	}

	public static String encode(String value) {
		String encodedString = "";
		try {
			encodedString = URLEncoder.encode(value, "UTF-8");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			encodedString = "";
		}

		return encodedString;
	}

	public static String getLocalTime() {
		String localTime = "";
		try {
			Calendar calendar = Calendar.getInstance(
					TimeZone.getTimeZone("GMT"), Locale.getDefault());
			Date currentLocalTime = calendar.getTime();
			SimpleDateFormat date = new SimpleDateFormat("Z");
			localTime = date.format(currentLocalTime);
			localTime = localTime.substring(0, 3) + ":"
					+ localTime.substring(3, localTime.length());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			localTime = "";
		}

		return localTime.substring(0, 3)+localTime.substring(3, localTime.length());
	}

	public static String formatCurrency(String price) {
		DecimalFormat myFormatter = new DecimalFormat("###,###.###");
		String output = myFormatter.format(Double.valueOf(price));
		System.out.println(output);
		return output;
	}

	public static String replaceNull(String value) {
		String updateValue = "";

		if (TextUtils.isEmpty(value)) {
			updateValue = "-";
		} else {
			updateValue = value;
		}

		return updateValue;
	}

	public static String dateItemFormat(String dateTime) throws java.text.ParseException {
		String formattedDateTime = "";

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

		try {
			Date date = (Date) dateFormat.parse("20130506000000");
			formattedDateTime = date.toString();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String[] dateTimeTemp = formattedDateTime.split(" ");

		return dateTimeTemp[1] + " " + dateTimeTemp[2] + " " + dateTimeTemp[3]
				+ " " + dateTimeTemp[5];
	}
	
	public static int getScreenWidth(Context context){
		int width = 0;
		if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.GINGERBREAD_MR1) {
		     // only for gingerbread and newer versions
			DisplayMetrics metrics = context.getResources().getDisplayMetrics();
			width = metrics.widthPixels;
		}else{
			WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
			Display display = wm.getDefaultDisplay();
//			Point size = new Point();
//			display.getSize(size);
			width = display.getWidth();
		}
		
		return width;
	}
	
	public static boolean urlChecker(String url){
		return URLUtil.isHttpsUrl(url);
	}
	
	public static String get(String address, String token) throws IOException {
		String response = null;
		HttpClient httpclient = null;
		try {
			HttpGet httpget = new HttpGet(address);
			httpget.addHeader("Authorization", "Bearer "+token);
			httpclient = new DefaultHttpClient();
			HttpResponse httpResponse = httpclient.execute(httpget);

			final int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				throw new Exception("Got HTTP " + statusCode + " ("
						+ httpResponse.getStatusLine().getReasonPhrase() + ')');
			}

			response = EntityUtils.toString(httpResponse.getEntity(),
					HTTP.UTF_8);

		
		} catch (Exception e) {
			throw new IOException("Error connection");

		} finally {
			if (httpclient != null) {
				httpclient.getConnectionManager().shutdown();
				httpclient = null;
			}
		}
		return response;
	}
	
	public static void dialPhone(Activity activity, String phoneNo){
		if (!phoneNo.equals("-")) {
			Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+phoneNo));
			activity.startActivity(intent);
		}
	}
	
	public static String goToGeocoder(Activity mContext, double lat, double lng) throws IOException{
		List<Address> almt;
		String city = null;
		try {
			Locale locale = new Locale("ID");
			Locale.setDefault(locale);
			Geocoder geocoder = new Geocoder(mContext, locale);
			almt = geocoder.getFromLocation(lat, lng, 5);
			if (almt.size() > 0 && almt !=null) {
				
				Address almtSkrng = almt.get(0);
				
				city = almtSkrng.getSubAdminArea();
				Log.d(Constant.APP_TAG, "City : "+city);
			}else{
				city = "";
			}
			
		} catch (IOException e) {
			// TODO: handle exception
			city = null;
			Log.e(Constant.APP_TAG, e.getMessage());
		}
		
		return city;
	}
	
	public final static boolean validEmail(String target) {
		return !TextUtils.isEmpty(target) && EMAIL_ADDRESS_PATTERN.matcher(target).matches();
	}

	public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern
			.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
					+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
					+ "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");
	
	public static boolean isConnectInet(Context context) {
		NetworkInfo networkInfo = ((ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE))
				.getActiveNetworkInfo();
		if (networkInfo == null || !networkInfo.isConnected()) {
			return false;
		}
		if (networkInfo.isRoaming()) {
			return true;
		}

		return true;
	}

	public static boolean isValidPassword(final String password) {
		Pattern pattern;
		Matcher matcher;
//		final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
		final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";

		pattern = Pattern.compile(PASSWORD_PATTERN);
		matcher = pattern.matcher(password);

		return matcher.matches();

	}

	public static boolean isValidBirthPlace(final String birthPlace) {
		Pattern pattern;
		Matcher matcher;
		final String BIRTPLACE_PATTERN = "^[a-zA-Z]+$";

		pattern = Pattern.compile(BIRTPLACE_PATTERN);
		matcher = pattern.matcher(birthPlace);

		return matcher.matches();

	}

	public static boolean isValidDate(String selectedDate){
		boolean isValid = false;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String currentDate=  sdf.format(new Date());

		Date d1 = null;
		Date d2 = null;

		try {
			d1 = sdf.parse(selectedDate);
			d2 = sdf.parse(currentDate);

			long diff = d2.getTime() - d1.getTime();

			long diffDays = diff / (24 * 60 * 60 * 1000);

			if (diffDays > 0){
				isValid = true;
			}

			Log.d("DayDiff", diffDays+"");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return isValid;
	}
}
