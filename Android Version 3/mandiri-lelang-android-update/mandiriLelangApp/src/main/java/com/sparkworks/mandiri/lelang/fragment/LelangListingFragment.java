package com.sparkworks.mandiri.lelang.fragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONObject;

import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.mandiri.lelang.aplikasi.DetilLelangItemActivity;
import com.mandiri.lelang.aplikasi.FilterActivity;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.adapter.EndlessAdapter;
import com.sparkworks.mandiri.lelang.adapter.LelangItemAdapter;
import com.sparkworks.mandiri.lelang.base.BaseFragment;
import com.sparkworks.mandiri.lelang.event.FavoriteEvent;
import com.sparkworks.mandiri.lelang.event.UnfavoriteEvent;
import com.sparkworks.mandiri.lelang.fragment.FilterFragment.FilterType;
import com.sparkworks.mandiri.lelang.model.Item;
import com.sparkworks.mandiri.lelang.model.MasterData;
import com.sparkworks.mandiri.lelang.model.Params;
import com.sparkworks.mandiri.lelang.utils.ApiHelper;
import com.sparkworks.mandiri.lelang.utils.ApiHelper.LelangType;
import com.sparkworks.mandiri.lelang.utils.Constant;
import com.sparkworks.mandiri.lelang.utils.LocationUtils;
import com.sparkworks.mandiri.lelang.utils.LocationUtils.LocationResult;
import com.sparkworks.mandiri.lelang.utils.Utils;

public class LelangListingFragment extends BaseFragment{
	private ListView lvItem;
	private LinearLayout lnContent, lnFooter;
	private ProgressBar pbIndicator;
	private TextView txtErrorMessage, txtLocation;
	private LinkedList<Item> listItem;
	private List<NameValuePair> listParams;
	private AsyncHttpClient client;
	public Params params;
	public String url;
	private boolean isJualSukarela = false;

	private String response = "";
	public static String RESPONSE_KEY = "response";
	public static String PARAMS_KEY = "params";
	public static String URLPOINT_KEY = "urlpoint";
	public static String LELANGTYPE_KEY = "lelang";
	
	private int pageIndex = 0;
	private int currentSelection = 0;
	private boolean needLoadMore = false;
	
	public static String FRAGMENT_TAG = "LelangItemFragment";
	
	public LelangType lelangType;
	
	public boolean isFromJadwalLelang = false;
	
	private LoadMoreLelangItem loadMoreLelangItem;
	
	private LocationUtils locationUtils;
	private LocationManager locationManager;
	private LocationResult locationResult;

    private LelangItemAdapter lelangItemAdapter = null;
	
	private String cityId = null, provinceId = null, cityName = null;

	public boolean isJualSukarela() {
		return isJualSukarela;
	}

	public void setJualSukarela(boolean jualSukarela) {
		isJualSukarela = jualSukarela;
	}

	public boolean isFromJadwalLelang() {
		return isFromJadwalLelang;
	}

	public void setFromJadwalLelang(boolean isFromJadwalLelang) {
		this.isFromJadwalLelang = isFromJadwalLelang;
	}

	public LelangType getLelangType() {
		return lelangType;
	}

	public void setLelangType(LelangType lelangType) {
		this.lelangType = lelangType;
	}

	public Params getParams() {
		return params;
	}

	public void setParams(Params params) {
		this.params = params;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		EventBus.getDefault().register(this);
		initializeLibs();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_lelang_item, container, false);
		initializeViews(view);
		setHasOptionsMenu(true);
		
		if (isFromJadwalLelang()) {
			getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
			getActivity().getActionBar().setDisplayShowHomeEnabled(true);
		}
		
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState == null) {
			if (getLelangType() == LelangType.TERDEKAT) {
				getLocation();
			} else {
				initializeRequest();
			}
			
		}else{
			//response = savedInstanceState.getString(RESPONSE_KEY);
			setParams((Params)savedInstanceState.getSerializable(PARAMS_KEY));
			setUrl(savedInstanceState.getString(URLPOINT_KEY));
			setLelangType((LelangType)savedInstanceState.getSerializable(LELANGTYPE_KEY));
			//bindDataToListView(response, false);
			initializeRequest();
		}
		
		initializeActions();
	}
	
	@Override
	public void initializeViews(View view) {
		// TODO Auto-generated method stub
		super.initializeViews(view);
		lvItem = (ListView)view.findViewById(R.id.lvLelangItem);
		pbIndicator = (ProgressBar)view.findViewById(R.id.pb_indicator);
		txtErrorMessage = (TextView)view.findViewById(R.id.txtErrorMessage);
		lnContent = (LinearLayout)view.findViewById(R.id.lnContent);
		lnFooter = (LinearLayout)view.findViewById(R.id.ln_footer);
		txtLocation = (TextView)view.findViewById(R.id.txt_location);
	}
	
	@Override
	public void initializeLibs() {
		// TODO Auto-generated method stub
		super.initializeLibs();
		client = new AsyncHttpClient(true, 80, 443);
		client.setTimeout(60000);
		listItem = new LinkedList<Item>();
	}
	
	@Override
	public void initializeRequest() {
		// TODO Auto-generated method stub
		super.initializeRequest();
		if (Utils.isConnectInet(getActivity())) {
			pbIndicator.setVisibility(View.VISIBLE);
			lnContent.setVisibility(View.GONE);
			txtErrorMessage.setVisibility(View.GONE);
			
			currentSelection = 0;
			
			if (getLelangType()==LelangType.TERDEKAT) {
				getParams().setCityId(cityId);
				getParams().setProvinceId(provinceId);
				if (cityName != null && !cityName.equals("")) {
					lnFooter.setVisibility(View.VISIBLE);
					txtLocation.setText("Lokasi anda saat ini "+cityName);
				}else{
					lnFooter.setVisibility(View.GONE);
				}
			}

			listParams = ApiHelper.getListParams(getParams());
			String request = "";
			
			request = ApiHelper.buildAPIURLMessage(getUrl(), null, listParams);
			
			Log.d(Constant.APP_TAG, request);
			client.addHeader("Authorization", "Bearer "+userPreference.getToken());
			client.get(request, new AsyncHttpResponseHandler(){
				@Override
				public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
					// TODO Auto-generated method stub
					super.onSuccess(arg0, arg1, arg2);
					pbIndicator.setVisibility(View.GONE);
					lnContent.setVisibility(View.VISIBLE);
					
					bindDataToListView(new String(arg2), false);
				}
				
				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2,
						Throwable arg3) {
					// TODO Auto-generated method stub
					super.onFailure(arg0, arg1, arg2, arg3);
					pbIndicator.setVisibility(View.GONE);
					lnContent.setVisibility(View.GONE);
					txtErrorMessage.setVisibility(View.VISIBLE);
					txtErrorMessage.setText(R.string.error_loading);	
				}
				
			});
		} else {
			pbIndicator.setVisibility(View.GONE);
			lnContent.setVisibility(View.GONE);
			txtErrorMessage.setVisibility(View.VISIBLE);
			txtErrorMessage.setText(R.string.no_internet);
		}
		
	}
	
	protected void bindDataToListView(String result, boolean isLoadMore) {
		// TODO Auto-generated method stub
		Log.d(Constant.APP_TAG, result);
		if (result == null || result.equals("")) {
			initializeRequest();
		}else{
			response = result;
			
			if (isLoadMore) {
				listItem = Item.getListLoadMoreItem(result, listItem);
			}else{
				listItem = Item.getListItem(result);
			}
			
			if (getLelangType()==LelangType.TERDEKAT) {
				if (listItem != null && listItem.size()<12) {
					needLoadMore = false;
				}
			}else{
				needLoadMore = Item.getIsNeedLoadMore(result);
			}
			
			setToAdapter(isLoadMore);
		}
	}

	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		super.initializeProcess();
	}
	
	private void setToAdapter(boolean isLoadmore) {
		// TODO Auto-generated method stub
		if (listItem != null && listItem.size()>0) {
			lelangItemAdapter = new LelangItemAdapter(getActivity(), listItem, getLelangType());
			pageIndex+=1;

			lelangItemAdapter.setJualSukarela(isJualSukarela());
			
			if (needLoadMore) {
				loadMoreLelangItem = new LoadMoreLelangItem(lelangItemAdapter);
				lvItem.setAdapter(loadMoreLelangItem);
				loadMoreLelangItem.notifyDataSetChanged();
			} else {
				lvItem.setAdapter(lelangItemAdapter);
				lelangItemAdapter.notifyDataSetChanged();
				lvItem.setSelection(currentSelection);
				isLoadmore = false;
			}
			
			if (isLoadmore) {
				lvItem.setSelection(currentSelection);	
			}
			
			
			
		} else {
			lvItem.setVisibility(View.GONE);
			txtErrorMessage.setVisibility(View.VISIBLE);
			txtErrorMessage.setText(R.string.error_loading);
		}
	}

	@Override
	public void initializeActions() {
		// TODO Auto-generated method stub
		super.initializeActions();
		lvItem.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				toDetilItem(arg2);
			}
		});
	}
	
	protected void toDetilItem(int position) {
		DetilLelangItemActivity.toDetilLelangActivity(getActivity(), listItem.get(position), position, getLelangType());
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_lelang_listing, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		if (item.getItemId()==R.id.action_filter) {
			
			FilterType type = null;
			
			if (getLelangType()==LelangType.ALL) {
				type = FilterType.ALL;
			}else if (getLelangType()==LelangType.SOON) {
				type = FilterType.SEGERA;
			}else if (getLelangType()==LelangType.HOT_PRICE) {
				type = FilterType.HOT;
			}else if (getLelangType()==LelangType.TERBARU) {
				type = FilterType.TERBARU;
			}else if (getLelangType()==LelangType.TERDEKAT) {
				type = FilterType.TERDEKAT;
			}else if (getLelangType() == LelangType.JUAL_SUKARELA){
				type = FilterType.JUAL_SUKARELA;
			}
			
			FilterActivity.toFilterFragmentActivity(getActivity(), type);
		}
		
		if (item.getItemId()==R.id.action_refresh) {
			if (getLelangType() == LelangType.TERDEKAT) {
				appPreference.setLatitude("");
				appPreference.setLongitude("");
				getLocation();
			}else{
				initializeRequest();
			}
		}
		
		if (item.getItemId()==android.R.id.home) {
			closeFragment();
		}
		
		return true;
	}
	
	private void closeFragment(){
		
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.remove(this);
        transaction.commit();
        getFragmentManager().popBackStack();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		outState.putSerializable(PARAMS_KEY, getParams());
		outState.putString(RESPONSE_KEY, response);
		outState.putString(URLPOINT_KEY, getUrl());
		outState.putSerializable(LELANGTYPE_KEY, getLelangType());
		super.onSaveInstanceState(outState);
	}
	
	class LoadMoreLelangItem extends EndlessAdapter{
		
		String responseLoadMore = "";
		
		public LoadMoreLelangItem(LelangItemAdapter adapterLoadmore) {
			// TODO Auto-generated constructor stub
			super(adapterLoadmore);
		}

		@Override
		protected boolean cacheInBackground() throws Exception {
			// TODO Auto-generated method stub
			
			boolean isValid = true;
			listParams.set(0, new BasicNameValuePair("pageIndex", String.valueOf(pageIndex)));
			String loadmoreRequest = ApiHelper.buildAPIURLMessage(getUrl(),
					null, listParams);
			Log.d(Constant.APP_TAG, loadmoreRequest);
			
			responseLoadMore = Utils.get(loadmoreRequest, userPreference.getToken());
			
			if (responseLoadMore == null || responseLoadMore.equals("")) {
				isValid = false;
			} else {
				try {
					JSONObject object = new JSONObject(responseLoadMore);
					if (object.getJSONArray(Constant.DATA).length()<0) {
						isValid = false;
					}else{
						currentSelection = listItem.size();
					}
				} catch (Exception e) {
					// TODO: handle exception
					isValid  = false;
				}
			}
			return isValid;
		}

		@Override
		protected void appendCachedData() {
			// TODO Auto-generated method stub
			
			bindDataToListView(responseLoadMore, true);
		}
		
		@Override
		protected View getPendingView(ViewGroup parent) {
			// TODO Auto-generated method stub
			View row = getActivity().getLayoutInflater().inflate(R.layout.loadmore_view, null);
			return row;
		}
	}
	
	public void getLocation(){
		pbIndicator.setVisibility(View.VISIBLE);
		lnContent.setVisibility(View.GONE);
		txtErrorMessage.setVisibility(View.GONE);
		locationResult = new LocationResult() {
			
			@Override
			public void gotLocation(Location location) {
				// TODO Auto-generated method stub
				if (appPreference.getLatitude().equals("")&&
						appPreference.getLongitude().equals("")) {
					if (location != null) {
						Log.d(Constant.APP_TAG, "LL : "+location.getLatitude()+" "+location.getLongitude());
						appPreference.setLatitude(String.valueOf(location.getLatitude()));
						appPreference.setLongitude(String.valueOf(location.getLongitude()));
						
						new GetReverseAddress(location).execute();
						
					} else {
						pbIndicator.setVisibility(View.GONE);
						lnContent.setVisibility(View.GONE);
						txtErrorMessage.setVisibility(View.VISIBLE);
						txtErrorMessage.setText(R.string.error_loading);
						txtErrorMessage.setGravity(Gravity.CENTER);
						lnFooter.setVisibility(View.GONE);
					}
				}
			}
		};
		
		locationUtils = new LocationUtils(getActivity());
		locationUtils.getLocation(getActivity(), locationResult);
	}
	
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
		client.cancelRequests(getActivity(), true);
		if (getLelangType()==LelangType.TERDEKAT) {
			appPreference.setLatitude("");
			appPreference.setLongitude("");
		}
		Log.d(Constant.APP_TAG, "On Stop Lelang Fragment");
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		// TODO Auto-generated method stub
		super.onHiddenChanged(hidden);
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		EventBus.getDefault().unregister(this);
		super.onDestroy();
		if (getLelangType()==LelangType.TERDEKAT) {
			appPreference.setLatitude("");
			appPreference.setLongitude("");
		}
	}
	
	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (getLelangType()==LelangType.TERDEKAT) {
			appPreference.setLatitude("");
			appPreference.setLongitude("");
		}
	}
	
	private class GetReverseAddress extends AsyncTask<Void, Void, String>{
		
		Location location;
		
		public GetReverseAddress(Location location) {
			// TODO Auto-generated constructor stub
			this.location = location;
		}
		
		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			String result = null;
			try {
				result = Utils.goToGeocoder(getActivity(),
						location.getLatitude(), 
						location.getLongitude());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return result;
		}
		
		@Override
		protected void onPostExecute(String kota) {
			// TODO Auto-generated method stub
			super.onPostExecute(kota);
			
			
			if (kota != null) {
				ArrayList<MasterData> listCity = new ArrayList<MasterData>();
				listCity = cityHelper.loadAllCityByKeyword(kota.replace("Kota", "").trim());
				if (listCity.size()>0) {
					cityId = listCity.get(0).getId();
					provinceId = listCity.get(0).getProvinceId();
					cityName = listCity.get(0).getName();
				} else {
					cityId = "0";
					provinceId = "0";
					cityName = kota;
				}
			}else{
				cityId = "0";
				provinceId = "0";
				cityName = "";
			}
			Log.d(Constant.APP_TAG, kota+ " City ID : " + cityId + " PROVINCE ID : "+provinceId);
			if (kota==null || kota.equals("")) {
				txtErrorMessage.setVisibility(View.VISIBLE);
				txtErrorMessage.setText(R.string.error_loading);
				lnFooter.setVisibility(View.GONE);
			}
			
			if (cityId.equals("")&&provinceId.equals("")) {
				pbIndicator.setVisibility(View.GONE);
				lnContent.setVisibility(View.GONE);
				txtErrorMessage.setVisibility(View.VISIBLE);
				txtErrorMessage.setText(R.string.error_loading);
				txtErrorMessage.setGravity(Gravity.CENTER);
			} else {
				initializeRequest();
			}
			
		}
	}

    @Subscribe
    public void onEvent(FavoriteEvent favoriteEvent){
        if (getLelangType() == favoriteEvent.getLelangType()){
            lelangItemAdapter.updateItem(favoriteEvent.getPosition(), favoriteEvent.getUpdatedItem());
        }
    }

    @Subscribe
    public void onEvent(UnfavoriteEvent unfavoriteEvent){
        if (getLelangType() == unfavoriteEvent.getLelangType()){
            Item updateItem = (Item) lelangItemAdapter.getItem(unfavoriteEvent.getPosition());
            lelangItemAdapter.updateItem(unfavoriteEvent.getPosition(), updateItem);
        }
    }
	
}
