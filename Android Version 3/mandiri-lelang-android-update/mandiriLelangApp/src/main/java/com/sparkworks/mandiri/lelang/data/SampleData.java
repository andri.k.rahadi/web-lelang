package com.sparkworks.mandiri.lelang.data;

import com.mandiri.lelang.aplikasi.R;

public class SampleData {
	/** 
	 * Format : title, kode_asset, limit_lelang, harga_sebelumnya, alamat, latitude, longitude, 
	 * dokumen, luas tanah, luas bangunan, kondisi_terkini, tahun_lelang, 
	 * nama_pengelola, alamat_pengelola, telepon_pengelola 
	 * **/
	
	public static String[][] allListing = new String[][]{
		{"Aset dilelang limit Rp.683.000.000", 
			"SMG1011", 
			"RP. 683.000.000", 
			"Rp. 700.000.000", 
			"Jl. Desa Rt 0019/07Desa Senenan, Kec.Tahunan, Kota Jepara",
			"",
			"", 
			"SHM NO.1470", 
			"1,054 m2", 
			"910 m2", 
			"", 
			"12-Aug-2014", 
			"Bank Mandiri RRCC Semarang", 
			"Jl. Raya Kepodang No. 32-34 Semarang", 
			"024-3521522, 024-3514809"},
			
		{"Aset dilelang limit Rp.396.500.000", 
		"SMG1220", 
		"RP. 396.500.000",
		"Rp. 523.000.000", 
		"Jl. Desa Larangan Rt. 07 Rw. 03 Desa Larangan Kec. Larangan Kota Brebes",
		"",
		"",
		"SHM NO. 1262", 
		"1,437 m2", 
		"0 m2", 
		"", 
		"12-Aug-2014", 
		"Bank Mandiri RRCC Semarang",
		"Jl. Raya Kepodang No. 32-34 Semarang", 
		"024-3521522, 024-3514809"}, 
		
		{"Aset dilelang limit Rp.997.500.000",
		"SMG1219",
		"RP. 997.500.000",
		"Rp. 1.305.000.000", 
		"Jl. Raya Barat No. 40 Rt. 03 Rw. 07 Desa Larangan Kec. Larangan Kota Brebes",
		"",
		"",
		"SHM NO. 1281",
		"1,110 m2",
		"215 m2",
		"",
		"12-Aug-2014",
		"Bank Mandiri RRCC Semarang", 
		"Jl. Raya Kepodang No. 32-34 Semarang",
		"024-3521522, 024-3514809"}, 
		
		{"Aset dilelang limit Rp.575.650.000", 
		"SMG1217",
		"RP. 575.650.000",
		"Rp. 643.000.000", 
		"Jl. Teuku Umar Rt. 02 Rw. 03 Desa Dukuhsalam Kec. Slawi Kota Tegal",
		"",
		"",
		"SHM NO. 103",
		"435 m2",
		"401 m2",
		"",
		"12-Aug-2014",
		"Bank Mandiri RRCC Semarang", 
		"Jl. Raya Kepodang No. 32-34 Semarang",
		"024-3521522, 024-3514809"}, 
		
		{"Aset dilelang limit Rp.119.250.000", 
			"SMG1216",
			"RP. 119.250.000", 
			"Rp. 128.000.000",
			"Desa Dukuhsalam Kec. Slawi Kota Tegal", 
			"", 
			"", 
			"SHM NO. 1170", 
			"2,131 m2", 
			"0 m2", 
			"", 
			"12-Aug-2014", 
			"Bank Mandiri RRCC Semarang", 
			"Jl. Raya Kepodang No. 32-34 Semarang", 
			"024-3521522, 024-3514809"},
			
		{"Aset dilelang limit Rp.164.000.000", 
				"SMG1272", 
				"RP. 164.000.000", 
				"", 
				"RT/RW. 03/04, Desa Semampir, Kec. Banjar Negara, Kota Banjarnegara", 
				"",
				"", 
				"SHM No. 607", 
				"285 m2", 
				"96 m2",
				"", 
				"", 
				"Bank Mandiri RRCC Semarang", 
				"Jl. Raya Kepodang No. 32-34 Semarang", 
				"024-3521522, 024-3514809"},
				
		{"Aset dilelang limit Rp.230.000.000", 
					"SMG1358",
					"RP. 230.000.000",
					"",
					"Perum Griya, Candi Asri No. 5, Desa Candirejo, Kec. Kutang Kota Semarang",
					"",
					"", 
					"SHM No. 639", 
					"203 m2",
					"78 m2", 
					"",
					"",
					"Bank Mandiri RRCC Semarang",
					"Jl. Raya Kepodang No. 32-34 Semarang",
					"024-3521522, 024-3514809"},
					
		{"Aset dilelang limit Rp.297.000.000",
						"SMG1357", 
						"RP. 297.000.000",
						"", 
						"Dk. Bancaan Lor RT/RW 03/11 Kel. Sidorejo, Kota Salatiga", 
						"",
						"",
						"SHM No. 1140",
						"298 m2",
						"136 m2",
						"",
						"",
						"Bank Mandiri RRCC Semarang",
						"Jl. Raya Kepodang No. 32-34 Semarang", 
						"024-3521522, 024-3514809"},
						
		{"Aset dilelang limit Rp.687.000.000", 
							"JKT1332",
							"RP. 687.000.000", 
							"",
							"The Address Blok A. 76 Kel. Leuwinanggung Kec. Cimanggis Kota Depok",
							"", 
							"",
							"SHGB NO. 665", 
							"160 m2",
							"172 m2",
							"",
							"",
							"Bank Mandiri RRCC Jakarta",
							"Wisma Mandiri II, Lt.18, Jl. Kebon Sirih No. 83 Jakarta Pusat",
							"021-30023002 ext 7129286"}, 
							
		{"Aset dilelang limit Rp.1.667.000.000", 
								"JKT1144", 
								"RP. 1.667.000.000", 
								"", 
								"Jl. Darul Qur'An No. 319 Rt. 001 / 010 Kel. Loji Kec. Bogor Barat, Kota Bogor", 
								"",
								"",
								"SHM NO. 1033 & 1034",
								"1,530 m2",
								"604 m2",
								"",
								"",
								"Bank Mandiri RRCC Jakarta", 
								"Wisma Mandiri II, Lt.18, Jl. Kebon Sirih No. 83 Jakarta Pusat",
								"021-30023002 ext 7129286"}
	};
	
	public static int[] allListingImage = new int[]{
		R.drawable.sample_image_a,
		R.drawable.sample_image_b, 
		R.drawable.sample_image_c, 
		R.drawable.sample_image_d, 
		R.drawable.sample_image_e,
		R.drawable.sample_image_f,
		R.drawable.sample_image_g,
		R.drawable.sample_image_h,
		R.drawable.sample_image_i,
		R.drawable.sample_image_j
	};
	
	public static String[][] hotListing = new String[][]{
		{"Aset dilelang limit Rp.683.000.000", "SMG1011", 
			"RP. 683.000.000", "Rp. 700.000.000", 
			"Jl. Desa Rt 0019/07Desa Senenan, Kec.Tahunan, Kota Jepara","","", 
			"SHM NO.1470", "1,054 m2", "910 m2", "", "12-Aug-2014", "Bank Mandiri RRCC Semarang", 
			"Jl. Raya Kepodang No. 32-34 Semarang", "024-3521522, 024-3514809"},
		{"Aset dilelang limit Rp.396.500.000", "SMG1220", "RP. 396.500.000", "Rp. 523.000.000", 
				"Jl. Desa Larangan Rt. 07 Rw. 03 Desa Larangan Kec. Larangan Kota Brebes", "", "",
				"SHM NO. 1262", "1,437 m2", "0 m2", "", "12-Aug-2014", "Bank Mandiri RRCC Semarang",
				"Jl. Raya Kepodang No. 32-34 Semarang", "024-3521522, 024-3514809"}, 
		{"Aset dilelang limit Rp.997.500.000", "SMG1219", "RP. 997.500.000", "Rp. 1.305.000.000", 
					"Jl. Raya Barat No. 40 Rt. 03 Rw. 07 Desa Larangan Kec. Larangan Kota Brebes", "", "",
			"SHM NO. 1281", "1,110 m2", "215 m2", "", "12-Aug-2014", "Bank Mandiri RRCC Semarang", 
			"Jl. Raya Kepodang No. 32-34 Semarang", "024-3521522, 024-3514809"}, 
		{"Aset dilelang limit Rp.575.650.000", "SMG1217", "RP. 575.650.000", "Rp. 643.000.000", 
				"Jl. Teuku Umar Rt. 02 Rw. 03 Desa Dukuhsalam Kec. Slawi Kota Tegal", "", "",
				"SHM NO. 103", "435 m2", "401 m2", "", "12-Aug-2014", "Bank Mandiri RRCC Semarang", 
				"Jl. Raya Kepodang No. 32-34 Semarang", "024-3521522, 024-3514809"}
	};
	
	public static int[] hotListingImage = new int[]{
		R.drawable.sample_image_a,
		R.drawable.sample_image_b, 
		R.drawable.sample_image_c, 
		R.drawable.sample_image_d
	};
	
	public static String[][] segeraListing = new String[][]{
		{"Aset dilelang limit Rp.119.250.000", "SMG1216", "RP. 119.250.000", "Rp. 128.000.000", "Desa Dukuhsalam Kec. Slawi Kota Tegal", 
			"", "", "SHM NO. 1170", "2,131 m2", "0 m2", "", "12-Aug-2014", "Bank Mandiri RRCC Semarang", "Jl. Raya Kepodang No. 32-34 Semarang", 
			"024-3521522, 024-3514809"}, 
		{"Aset dilelang limit Rp.164.000.000", "SMG1272", "RP. 164.000.000", "", "RT/RW. 03/04, Desa Semampir, Kec. Banjar Negara, Kota Banjarnegara", "","", 
				"SHM No. 607", "285 m2", "96 m2", "", "", "Bank Mandiri RRCC Semarang", "Jl. Raya Kepodang No. 32-34 Semarang", 
				"024-3521522, 024-3514809"}
	};
	
	public static int[] segeraListingImage = new int[]{
		R.drawable.sample_image_e, 
		R.drawable.sample_image_f
	};
	
	public static String[][] terbaruListing = new String[][]{
		{"Aset dilelang limit Rp.230.000.000", "SMG1358", "RP. 230.000.000", "", "Perum Griya, Candi Asri No. 5, Desa Candirejo, Kec. Kutang Kota Semarang", "", "", 
			"SHM No. 639", "203 m2", "78 m2", "", "", "Bank Mandiri RRCC Semarang", "Jl. Raya Kepodang No. 32-34 Semarang", "024-3521522, 024-3514809"}, 
		{"Aset dilelang limit Rp.297.000.000", "SMG1357", "RP. 297.000.000", "", "Dk. Bancaan Lor RT/RW 03/11 Kel. Sidorejo, Kota Salatiga", 
				"", "", "SHM No. 1140", "298 m2", "136 m2", "", "", "Bank Mandiri RRCC Semarang", "Jl. Raya Kepodang No. 32-34 Semarang", 
				"024-3521522, 024-3514809"}
	};
	
	public static int[] terbaruListingImage = new int[]{
		R.drawable.sample_image_g, 
		R.drawable.sample_image_h
	};
	
	public static String[][] terdekatListing = new String[][]{
		{"Aset dilelang limit Rp.687.000.000", "JKT1332", "RP. 687.000.000", "", "The Address Blok A. 76 Kel. Leuwinanggung Kec. Cimanggis Kota Depok", "", "", "SHGB NO. 665", 
			"160 m2", "172 m2", "", "", "Bank Mandiri RRCC Jakarta", "Wisma Mandiri II, Lt.18, Jl. Kebon Sirih No. 83 Jakarta Pusat", "021-30023002 ext 7129286"}, 
		{"Aset dilelang limit Rp.1.667.000.000", "JKT1144", "RP. 1.667.000.000", "", "Jl. Darul Qur'An No. 319 Rt. 001 / 010 Kel. Loji Kec. Bogor Barat, Kota Bogor", 
				"", "",
				"SHM NO. 1033 & 1034", "1,530 m2", "604 m2", "", "", "Bank Mandiri RRCC Jakarta", 
				"Wisma Mandiri II, Lt.18, Jl. Kebon Sirih No. 83 Jakarta Pusat", "021-30023002 ext 7129286"}
	};
	
	public static int[] terdekatListingImage = new int[]{
		R.drawable.sample_image_i, 
		R.drawable.sample_image_j
	};
	
	public static String[] kategori = new String[]{
		"Semua",
		"Tanah", 
		"Rumah", 
		"Ruko", 
		"Gedung"
	};
	
	public static String[] lokasi = new String[]{
		"Semua",
		"Jakarta", 
		"Bandung", 
		"Semarang", 
		"Bali",
		"Aceh"
	};
	
	public static String[] harga = new String[]{
		"Semua", 
		"Kurang dari 500 Juta", 
		"Lebih dari 500 Juta - 1 M",
		"Lebih dari 1 M"
	};
}
