package com.sparkworks.mandiri.lelang.adapter;

import java.util.ArrayList;

import com.sparkworks.mandiri.lelang.fragment.DetilItemImageFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class DetilItemImageFragmentAdapter extends FragmentPagerAdapter {
	public ArrayList<String> images;
    
    public ArrayList<String> getImages() {
		return images;
	}

	public void setImages(ArrayList<String> images) {
		this.images = images;
	}

	public DetilItemImageFragmentAdapter(FragmentManager fm) {
        super(fm);

    }
    
    @Override
    public int getCount() {
        return getImages().size();
    }

    @Override
    public Fragment getItem(int position) {
        return DetilItemImageFragment.newInstance(getImages().get(position).toString());
    }
}
