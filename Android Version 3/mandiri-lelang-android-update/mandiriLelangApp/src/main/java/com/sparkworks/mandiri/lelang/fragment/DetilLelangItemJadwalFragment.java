package com.sparkworks.mandiri.lelang.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mandiri.lelang.aplikasi.DetilLelangItemActivity;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.base.BaseFragment;
import com.sparkworks.mandiri.lelang.model.Item;
import com.sparkworks.mandiri.lelang.utils.Utils;

public class DetilLelangItemJadwalFragment extends BaseFragment{
	public static String FRAGMENT_TAG = "DetilLelangItemJadwalFragment";
	private TextView txtTanggal, txtKpknlName, txtKpknlAlamat, txtKpknlTelp1, txtKpknlTelp2, 
	txtBalaiLelangName, txtBalaiLelangAddress, 
	txtBalaiLelangTelp1, txtBalaiLelangTelp2, txtInfoTransfer;
	
	Item item;
	
	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_detil_lelang_jadwal, container, false);
		initializeViews(view);
		return view;
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		if (savedInstanceState != null) {
			setItem((Item)savedInstanceState.getSerializable(DetilLelangItemActivity.ITEM_KEY));
		}
		
		initializeProcess();
		initializeActions();
	}
	
	@Override
	public void initializeViews(View view) {
		// TODO Auto-generated method stub
		super.initializeViews(view);
		txtTanggal = (TextView)view.findViewById(R.id.txt_detil_lelang_jadwal_tanggal);
		txtKpknlName = (TextView)view.findViewById(R.id.txt_detil_lelang_jadwal_kpknl_name);
		txtKpknlAlamat = (TextView)view.findViewById(R.id.txt_detil_lelang_jadwal_kpknl_alamat);
		txtKpknlTelp1 = (TextView)view.findViewById(R.id.txt_detil_lelang_jadwal_kpknl_telp_1);
		txtKpknlTelp2 = (TextView)view.findViewById(R.id.txt_detil_lelang_jadwal_kpknl_telp_2);
		
		txtBalaiLelangName = (TextView)view.findViewById(R.id.txtDetilItemNamaBalaiLelang);
		txtBalaiLelangAddress = (TextView)view.findViewById(R.id.txtDetilItemBalaiLelangALamat);
		txtBalaiLelangTelp1 = (TextView)view.findViewById(R.id.txtDetilItemBalaiLelangTelepon1);
		txtBalaiLelangTelp2 = (TextView)view.findViewById(R.id.txtDetilItemBalaiLelangTelepon2);
		
		txtInfoTransfer = (TextView)view.findViewById(R.id.txtDetilItemInformationTransfer);
	}
	
	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		super.initializeProcess();
		
		txtTanggal.setText(getItem().getAuctionDateFormatted());
		txtKpknlName.setText(Utils.replaceNull(getItem().getKpknl().getName()));
		txtKpknlAlamat.setText(Utils.replaceNull(getItem().getKpknl().getAddress()));
		txtKpknlTelp1.setText(Utils.replaceNull(getItem().getKpknl().getPhone1()));
		txtKpknlTelp2.setText(Utils.replaceNull(getItem().getKpknl().getPhone2()));
		
		txtBalaiLelangName.setText(Utils.replaceNull(getItem().getAuctionHall().getName()));
		txtBalaiLelangAddress.setText(Utils.replaceNull(getItem().getAuctionHall().getAddress()));
		txtBalaiLelangTelp1.setText(Utils.replaceNull(getItem().getAuctionHall().getPhone1()));
		txtBalaiLelangTelp2.setText(Utils.replaceNull(getItem().getAuctionHall().getPhone2()));
		txtInfoTransfer.setText(Utils.replaceNull(getItem().getTransferInformation()));
	}
	
	@Override
	public void initializeActions() {
		// TODO Auto-generated method stub
		super.initializeActions();
		
		txtBalaiLelangTelp1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Utils.dialPhone(getActivity(), getItem().getAuctionHall().getPhone1());
			}
		});
		
		txtBalaiLelangTelp2.setOnClickListener(new OnClickListener() {
	
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Utils.dialPhone(getActivity(), getItem().getAuctionHall().getPhone2());
			}
		});
		
		txtKpknlTelp1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Utils.dialPhone(getActivity(), getItem().getKpknl().getPhone1());
			}
		});
		
		txtKpknlTelp2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Utils.dialPhone(getActivity(), getItem().getKpknl().getPhone2());
			}
		});
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		outState.putSerializable(DetilLelangItemActivity.ITEM_KEY, getItem());
		super.onSaveInstanceState(outState);
	}
}
