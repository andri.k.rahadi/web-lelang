package com.sparkworks.mandiri.lelang.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sidiqpermana on 12/1/16.
 */

public class KpknlItem implements Parcelable {
    private int id;
    private String name;
    private boolean checked;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeByte(this.checked ? (byte) 1 : (byte) 0);
    }

    public KpknlItem() {
    }

    protected KpknlItem(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.checked = in.readByte() != 0;
    }

    public static final Parcelable.Creator<KpknlItem> CREATOR = new Parcelable.Creator<KpknlItem>() {
        @Override
        public KpknlItem createFromParcel(Parcel source) {
            return new KpknlItem(source);
        }

        @Override
        public KpknlItem[] newArray(int size) {
            return new KpknlItem[size];
        }
    };
}
