package com.sparkworks.mandiri.lelang.request.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sidiqpermana on 11/29/16.
 */

public class ResetPasswordData {
    @SerializedName("email")
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
