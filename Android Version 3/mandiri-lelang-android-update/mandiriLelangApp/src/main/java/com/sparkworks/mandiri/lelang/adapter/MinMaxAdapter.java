package com.sparkworks.mandiri.lelang.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.model.MasterData;

import java.util.ArrayList;

/**
 * Created by sidiqpermana on 1/19/17.
 */

public class MinMaxAdapter extends BaseAdapter implements SpinnerAdapter{
    private ArrayList<MasterData> listPrice;
    private Context context;
    private LayoutInflater inflater;

    public MinMaxAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public ArrayList<MasterData> getListPrice() {
        return listPrice;
    }

    public void setListPrice(ArrayList<MasterData> listPrice) {
        this.listPrice = listPrice;
    }

    @Override
    public int getCount() {
        return getListPrice().size();
    }

    @Override
    public Object getItem(int i) {
        return getListPrice().get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null){
            view = inflater.inflate(R.layout.item_spinner_min_max, null);
            holder = new ViewHolder();
            holder.tvPrice = (TextView)view.findViewById(R.id.tv_item_value);
            view.setTag(holder);
        }else{
            holder = (ViewHolder)view.getTag();
        }

        holder.tvPrice.setText(getListPrice().get(i).getName());

        return view;
    }

    static class ViewHolder{
        TextView tvPrice;
    }
}
