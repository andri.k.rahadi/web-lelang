package com.sparkworks.mandiri.lelang.model;

import java.io.Serializable;

public class ExtraFields implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String fieldName, value;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
