package com.sparkworks.mandiri.lelang.fragment;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONObject;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.mandiri.lelang.aplikasi.DetilLelangItemActivity;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.adapter.EndlessAdapter;
import com.sparkworks.mandiri.lelang.adapter.LelangItemAdapter;
import com.sparkworks.mandiri.lelang.base.BaseFragment;
import com.sparkworks.mandiri.lelang.event.FavoriteEvent;
import com.sparkworks.mandiri.lelang.event.UnfavoriteEvent;
import com.sparkworks.mandiri.lelang.model.Item;
import com.sparkworks.mandiri.lelang.model.Params;
import com.sparkworks.mandiri.lelang.utils.ApiHelper;
import com.sparkworks.mandiri.lelang.utils.ApiHelper.LelangType;
import com.sparkworks.mandiri.lelang.utils.Constant;
import com.sparkworks.mandiri.lelang.utils.Utils;

public class SearchResultFragment extends BaseFragment{
	private ListView lvItem;
	private ProgressBar pbIndicator;
	private TextView txtErrorMessage, txtTitle;
	private LinkedList<Item> listItem;
	private List<NameValuePair> listParams;
	private AsyncHttpClient client;
	public Params params;
	public String url, response;
	private LoadMoreLelangItem loadMoreLelangItem;
	private int pageIndex = 0, totalRecords = 0;
	private int currentSelection = 0;
	public static String FRAGMENT_TAG = "SearchResultFragment";
	private boolean needLoadmore = false;
	
	private String RESPONSE_KEY = "response";
	private String PARAMS_KEY = "params";
	private String URLPOINT_KEY = "urlpoint";
	private String LELANGTYPE_KEY = "lelang";
	
	public LelangType lelangType;
	private LelangItemAdapter lelangItemAdapter;
	public LelangType getLelangType() {
		return lelangType;
	}

	public void setLelangType(LelangType lelangType) {
		this.lelangType = lelangType;
	}

	public Params getParams() {
		return params;
	}

	public void setParams(Params params) {
		this.params = params;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		initializeLibs();
		EventBus.getDefault().register(this);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_search_result, container, false);
		initializeViews(view);
		setHasOptionsMenu(true);
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		if (savedInstanceState == null) {
			initializeRequest();
		}else{
			response = savedInstanceState.getString(RESPONSE_KEY);
			setParams((Params)savedInstanceState.getSerializable(PARAMS_KEY));
			setUrl(savedInstanceState.getString(URLPOINT_KEY));
			setLelangType((LelangType)savedInstanceState.getSerializable(LELANGTYPE_KEY));
			
			if (response.equals("")&&getParams()==null&&getUrl()==null&&getLelangType()==null) {
				initializeRequest();
			} else {
				bindDataToListView(response, false);
			}
		}
		initializeActions();
	}
	
	@Override
	public void initializeViews(View view) {
		// TODO Auto-generated method stub
		super.initializeViews(view);
		lvItem = (ListView)view.findViewById(R.id.lvLelangItem);
		pbIndicator = (ProgressBar)view.findViewById(R.id.pb_indicator);
		txtErrorMessage = (TextView)view.findViewById(R.id.txtErrorMessage);
		txtTitle = (TextView)view.findViewById(R.id.txt_title_page);
	}
	
	@Override
	public void initializeLibs() {
		// TODO Auto-generated method stub
		super.initializeLibs();
		client = new AsyncHttpClient(true, 80, 443);
		client.setTimeout(120000);
		client.addHeader("Authorization", "Bearer "+userPreference.getToken());
		listItem = new LinkedList<Item>();
		listParams = new LinkedList<NameValuePair>();
	}
	
	@Override
	public void initializeRequest() {
		// TODO Auto-generated method stub
		super.initializeRequest();
		
		if (Utils.isConnectInet(getActivity())) {
			pbIndicator.setVisibility(View.VISIBLE);
			lvItem.setVisibility(View.GONE);
			txtErrorMessage.setVisibility(View.GONE);
			
			currentSelection = 0;
			
			listParams = ApiHelper.getListParams(getParams());
			String request = ApiHelper.buildAPIURLMessage(getUrl(), null, listParams);
			client.get(request, new AsyncHttpResponseHandler(){
				@Override
				public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
					// TODO Auto-generated method stub
					super.onSuccess(arg0, arg1, arg2);
					pbIndicator.setVisibility(View.GONE);
					lvItem.setVisibility(View.VISIBLE);
					
					bindDataToListView(new String(arg2), false);
				}
				
				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2,
						Throwable arg3) {
					// TODO Auto-generated method stub
					super.onFailure(arg0, arg1, arg2, arg3);
					pbIndicator.setVisibility(View.GONE);
					lvItem.setVisibility(View.GONE);
					txtErrorMessage.setVisibility(View.VISIBLE);
					txtErrorMessage.setText(R.string.error_pencarian_lelang);
				}
			});
		} else {
			pbIndicator.setVisibility(View.GONE);
			lvItem.setVisibility(View.GONE);
			txtErrorMessage.setVisibility(View.VISIBLE);
			txtErrorMessage.setText(R.string.no_internet);
		}
	}
	
	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		super.initializeProcess();
		
		LelangItemAdapter lelangItemAdapter = new LelangItemAdapter(getActivity(), listItem, getLelangType());
		lvItem.setAdapter(lelangItemAdapter);
	}
	
	@Override
	public void initializeActions() {
		// TODO Auto-generated method stub
		super.initializeActions();
		lvItem.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				toDetilItem(arg2);
			}
		});
	}
	
	protected void toDetilItem(int position) {
		DetilLelangItemActivity.toDetilLelangActivity(getActivity(), listItem.get(position));
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_search_result, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		if (item.getItemId()==R.id.action_refresh) {
			initializeRequest();
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	class LoadMoreLelangItem extends EndlessAdapter{
		
		String responseLoadMore = "";
		
		public LoadMoreLelangItem(LelangItemAdapter adapterLoadmore) {
			// TODO Auto-generated constructor stub
			super(adapterLoadmore);
		}

		@Override
		protected boolean cacheInBackground() throws Exception {
			// TODO Auto-generated method stub
			
			boolean isValid = true;
			listParams.set(0, new BasicNameValuePair("pageIndex", String.valueOf(pageIndex)));
			String loadmoreRequest = ApiHelper.buildAPIURLMessage(getUrl(),
					null, listParams);
			responseLoadMore = Utils.get(loadmoreRequest, userPreference.getToken());
			if (responseLoadMore == null || responseLoadMore.equals("")) {
				isValid = false;
			} else {
				try {
					JSONObject object = new JSONObject(responseLoadMore);
					if (object.getJSONArray(Constant.DATA).length()<0) {
						isValid = false;
					}else{
						currentSelection = listItem.size();
					}
				} catch (Exception e) {
					// TODO: handle exception
					isValid  = false;
				}
			}
			return isValid;
		}

		@Override
		protected void appendCachedData() {
			// TODO Auto-generated method stub
			bindDataToListView(responseLoadMore, true);
		}
		
		@Override
		protected View getPendingView(ViewGroup parent) {
			// TODO Auto-generated method stub
			View row = getActivity().getLayoutInflater().inflate(R.layout.loadmore_view, null);
			return row;
		}
	}
	
	protected void bindDataToListView(String result, boolean isLoadMore) {
		// TODO Auto-generated method stub
		if (result == null || result.equals("")) {
			initializeRequest();
		}else{
			response = result;
			
			if (isLoadMore) {
				listItem = Item.getListLoadMoreItem(result, listItem);
			} else {
				listItem = Item.getListItem(response);
				totalRecords = Item.getTotalRecords(response);
				txtTitle.setText("Hasil pencarian ("+totalRecords+" Agunan)");
			}
			
			if (listItem == null) {
				needLoadmore = false;
				pbIndicator.setVisibility(View.GONE);
				lvItem.setVisibility(View.GONE);
				txtErrorMessage.setVisibility(View.VISIBLE);
				txtErrorMessage.setText(R.string.error_pencarian_lelang);
			}else{
				needLoadmore = Item.getIsNeedLoadMore(result);
			}
			setToAdapter(isLoadMore);
		}
	}
	
	private void setToAdapter(boolean isLoadmore) {
		// TODO Auto-generated method stub
		if (listItem!=null) {
			lelangItemAdapter = new LelangItemAdapter(getActivity(), listItem, getLelangType());
			pageIndex+=1;
			
			if (needLoadmore) {
				loadMoreLelangItem = new LoadMoreLelangItem(lelangItemAdapter);
				lvItem.setAdapter(loadMoreLelangItem);
				loadMoreLelangItem.notifyDataSetChanged();
			} else {
				lvItem.setAdapter(lelangItemAdapter);
				lelangItemAdapter.notifyDataSetChanged();
				lvItem.setSelection(currentSelection);
				isLoadmore = false;
			}
			
			if (isLoadmore) {
				lvItem.setSelection(currentSelection);
			}
		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		outState.putSerializable(PARAMS_KEY, getParams());
		outState.putString(RESPONSE_KEY, response);
		outState.putString(URLPOINT_KEY, getUrl());
		outState.putSerializable(LELANGTYPE_KEY, getLelangType());
		super.onSaveInstanceState(outState);
	}

	@Subscribe
	public void onEvent(FavoriteEvent favoriteEvent){
		lelangItemAdapter.updateItem(favoriteEvent.getPosition(), favoriteEvent.getUpdatedItem());
	}

	@Subscribe
	public void onEvent(UnfavoriteEvent unfavoriteEvent){
		//Item updateItem = (Item) lelangItemAdapter.getItem(unfavoriteEvent.getPosition());
		lelangItemAdapter.updateItem(unfavoriteEvent.getPosition(), unfavoriteEvent.getUpdatedItem());
	}

	@Override
	public void onDestroy() {
		EventBus.getDefault().unregister(this);
		super.onDestroy();
	}
}
