package com.sparkworks.mandiri.lelang.request.api;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.request.response.ProfileResponse;
import com.sparkworks.mandiri.lelang.utils.URLs;
import com.sparkworks.mandiri.lelang.utils.Utils;

import org.apache.http.Header;

/**
 * Created by sidiqpermana on 11/29/16.
 */

public class GetProfileRequest extends BaseApi{
    private Context context;
    private AsyncHttpClient asyncHttpClient;
    private OnGetProfileRequestListener onGetProfileRequestListener;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public OnGetProfileRequestListener getOnGetProfileRequestListener() {
        return onGetProfileRequestListener;
    }

    public void setOnGetProfileRequestListener(OnGetProfileRequestListener onGetProfileRequestListener) {
        this.onGetProfileRequestListener = onGetProfileRequestListener;
    }

    @Override
    public void callApi() {
        super.callApi();
        if (Utils.isConnectInet(getContext())){
            asyncHttpClient = getAsyncHttpClient(getUserPreference(getContext()).getToken());
            asyncHttpClient.get(URLs.SERVER_PROFILE, new AsyncHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    super.onSuccess(statusCode, headers, responseBody);
                    String s = new String(responseBody);
                    Log.d("Profile", s);
                    Gson gson = new Gson();
                    ProfileResponse profileResponse = gson.fromJson(s, ProfileResponse.class);
                    if (profileResponse.getResultCode().getCode() == CODE_SUCCESS){
                        if (profileResponse.getResultCode().getMessage().contains("TOKEN")){
                            getOnGetProfileRequestListener().onGetProfileFailed("Sesi login anda sudah habis, mohon melakukan login ulang");
                        }else{
                            getOnGetProfileRequestListener().onGetProfileSuccess(profileResponse);
                        }
                    }else{
                        getOnGetProfileRequestListener().onGetProfileFailed(profileResponse.getResultCode().getMessage());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    super.onFailure(statusCode, headers, responseBody, error);
                    getOnGetProfileRequestListener().onGetProfileFailed(getContext().getString(R.string.error_profile));
                }
            });
        }else{
            getOnGetProfileRequestListener().onGetProfileFailed(getContext().getString(R.string.no_internet));
        }
    }

    @Override
    public void cancel() {
        super.cancel();
        if (asyncHttpClient != null){
            asyncHttpClient.cancelRequests(getContext(), true);
        }
    }

    @Override
    public void retry() {
        super.retry();
        callApi();
    }

    public interface OnGetProfileRequestListener{
        void onGetProfileSuccess(ProfileResponse profileResponse);
        void onGetProfileFailed(String errorMessage);
    }
}
