package com.sparkworks.mandiri.lelang.model;

import java.io.Serializable;

import android.app.Activity;

public class MasterData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3661306353858548966L;
	String id, name, description, isActive, flag, value, 
	provinceId;

	int _id;
	
	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}
	
	public static void insertIntoDatabase(Activity activity, String response){
		
		try {
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
