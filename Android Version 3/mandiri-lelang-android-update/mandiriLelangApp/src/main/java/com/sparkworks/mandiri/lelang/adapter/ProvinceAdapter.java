package com.sparkworks.mandiri.lelang.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.model.ProvinceItem;

import java.util.ArrayList;

/**
 * Created by sidiqpermana on 11/9/16.
 */

public class ProvinceAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<ProvinceItem> listProvince;
    private KpknlAdapter.OnCheckAllCallback onCheckAllCallback;

    public ProvinceAdapter(Activity activity) {
        this.activity = activity;
    }

    public KpknlAdapter.OnCheckAllCallback getOnCheckAllCallback() {
        return onCheckAllCallback;
    }

    public void setOnCheckAllCallback(KpknlAdapter.OnCheckAllCallback onCheckAllCallback) {
        this.onCheckAllCallback = onCheckAllCallback;
    }

    public ArrayList<ProvinceItem> getListProvince() {
        return listProvince;
    }

    public void setListProvince(ArrayList<ProvinceItem> listProvince) {
        this.listProvince = listProvince;
    }

    @Override
    public int getCount() {
        return getListProvince().size();
    }

    @Override
    public Object getItem(int i) {
        return getListProvince().get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null){
            LayoutInflater inflater = LayoutInflater.from(activity);
            view = inflater.inflate(R.layout.item_province, null);
            final ViewHolder holder = new ViewHolder();
            holder.tvName = (TextView)view.findViewById(R.id.tv_item_province);
            holder.cbItem = (CheckBox)view.findViewById(R.id.cb_item_province);
            holder.cbItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    ProvinceItem item = (ProvinceItem)holder.cbItem.getTag();
                    item.setChecked(compoundButton.isChecked());

                    if (!compoundButton.isChecked()){
                        getOnCheckAllCallback().onCheckAll();
                    }
                }
            });
            view.setTag(holder);
            holder.cbItem.setTag(getListProvince().get(i));
        }else{
            ((ViewHolder)view.getTag()).cbItem.setTag(listProvince.get(i));
        }

        ViewHolder holder = (ViewHolder)view.getTag();
        holder.tvName.setText(getListProvince().get(i).getName());
        holder.cbItem.setChecked(getListProvince().get(i).isChecked());

        return view;
    }

    static class ViewHolder{
        CheckBox cbItem;
        TextView tvName;
    }
}
