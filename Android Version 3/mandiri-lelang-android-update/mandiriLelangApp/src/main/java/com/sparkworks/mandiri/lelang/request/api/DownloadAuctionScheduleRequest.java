package com.sparkworks.mandiri.lelang.request.api;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.model.AuctionScheduleParam;
import com.sparkworks.mandiri.lelang.utils.URLs;
import com.sparkworks.mandiri.lelang.utils.Utils;

import org.apache.http.entity.StringEntity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

/**
 * Created by sidiqpermana on 11/30/16.
 */

public class DownloadAuctionScheduleRequest extends BaseApi {
    public static final String MANDIRI_LELANG_DIR = "/mandiri-lelang/";
    private Context context;
    private AuctionScheduleParam auctionScheduleParam;
    private OnDownloadAuctionScheduleListener onDownloadAuctionScheduleListener;
    private SyncHttpClient syncHttpClient;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public AuctionScheduleParam getAuctionScheduleParam() {
        return auctionScheduleParam;
    }

    public void setAuctionScheduleParam(AuctionScheduleParam auctionScheduleParam) {
        this.auctionScheduleParam = auctionScheduleParam;
    }

    public OnDownloadAuctionScheduleListener getOnDownloadAuctionScheduleListener() {
        return onDownloadAuctionScheduleListener;
    }

    @Override
    public void callApi() {
        super.callApi();
        if (Utils.isConnectInet(getContext())){
            Gson gson = new Gson();
            String bodyParam = gson.toJson(getAuctionScheduleParam());
            StringEntity entity = null;
            try {
                entity = new StringEntity(bodyParam);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            if (entity != null){
                syncHttpClient = getSyncHttpClient(getUserPreference(getContext()).getToken());
                syncHttpClient.post(getContext(), URLs.DOWNLOAD_AUCTION_SCHEDULE, entity, "application/json", new FileAsyncHttpResponseHandler(getContext()){
                    @Override
                    public void onSuccess(File file) {
                        super.onSuccess(file);

                        if (file != null){
                            File newFile = createNewFile(file);
                            if (newFile != null){
                                getOnDownloadAuctionScheduleListener().onDownloadAuctionScheduleSuccess(newFile);
                            }else{
                                getOnDownloadAuctionScheduleListener().onDownloadAuctionScheduleFailed("Tidak dapat membuat file");
                            }
                        }else{
                            getOnDownloadAuctionScheduleListener().onDownloadAuctionScheduleFailed("File tidak valid");
                        }
                    }

                    @Override
                    public void onFailure(Throwable e, File response) {
                        super.onFailure(e, response);
                        getOnDownloadAuctionScheduleListener().onDownloadAuctionScheduleFailed("Tidak dapat mendownload file saat ini");
                    }
                });
            }
        }else{
            getOnDownloadAuctionScheduleListener().onDownloadAuctionScheduleFailed(getContext().getString(R.string.no_internet));
        }
    }

    public static boolean isAbleToWriteTheDisk() {
        // get the state of your external storage
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // if storage is mounted return true
            Log.v("WriteFile", "Yes, can write to external storage.");
            return true;
        }
        return false;
    }

    private File createNewFile(File downloadedFile){
        File newFile = null;
        try{
            File sdcard = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            File dir = new File(sdcard.getAbsolutePath() + "/" + MANDIRI_LELANG_DIR);
            dir.mkdir();

            String fileName = null;
            if (TextUtils.isEmpty(getAuctionScheduleParam().getFromDate()) && TextUtils.isEmpty(getAuctionScheduleParam().getToDate())){
                fileName = "autionschedule_"+System.currentTimeMillis()+".csv";
            }else{
                fileName = "autionschedule_"+getAuctionScheduleParam().getFromDate()+"_"+getAuctionScheduleParam().getToDate()+".csv";
            }

            newFile = new File(dir, fileName);
            FileOutputStream out = new FileOutputStream(newFile);

            InputStream in = new FileInputStream(downloadedFile);


            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }

            in.close();
            out.close();
        }catch (Exception e){
            Log.d("Create file", e.getMessage());
        }

        return newFile;
    }

    @Override
    public void cancel() {
        super.cancel();
        if (asyncHttpClient != null){
            asyncHttpClient.cancelRequests(getContext(), true);
        }
    }

    @Override
    public void retry() {
        super.retry();
        callApi();
    }

    public void setOnDownloadAuctionScheduleListener(OnDownloadAuctionScheduleListener onDownloadAuctionScheduleListener) {
        this.onDownloadAuctionScheduleListener = onDownloadAuctionScheduleListener;
    }

    public interface OnDownloadAuctionScheduleListener{
        void onDownloadAuctionScheduleSuccess(File file);
        void onDownloadAuctionScheduleFailed(String message);
    }
}
