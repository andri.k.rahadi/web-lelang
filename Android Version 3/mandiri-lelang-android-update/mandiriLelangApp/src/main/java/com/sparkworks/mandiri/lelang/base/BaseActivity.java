package com.sparkworks.mandiri.lelang.base;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.google.analytics.tracking.android.EasyTracker;
import com.sparkworks.mandiri.lelang.utils.UserPreference;

public class BaseActivity extends FragmentActivity implements DefaultMethods{

	private ProgressDialog progressDialog;
	public UserPreference userPreference;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		userPreference = new UserPreference(this);
	}

	public void showProgressDialog(String title, String message){
		if (progressDialog == null){
			progressDialog = new ProgressDialog(BaseActivity.this);
			progressDialog.setTitle(title);
			progressDialog.setMessage(message);
		}

		progressDialog.show();
	}

	public void cancelProgressDialog(){
		if (progressDialog != null){
			if (progressDialog.isShowing()){
				progressDialog.cancel();
			}
		}
	}

	@Override
	public void initializeViews() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initializeViews(View view) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initializeLibs() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initializeActions() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initializeRequest() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		// TODO Auto-generated method stub
		super.attachBaseContext(new CalligraphyContextWrapper(newBase));
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}

}
