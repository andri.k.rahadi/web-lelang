package com.sparkworks.mandiri.lelang.fragment;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.mandiri.lelang.aplikasi.FilterActivity;
import com.mandiri.lelang.aplikasi.R;
import com.mandiri.lelang.aplikasi.SearchResultActivity;
import com.sparkworks.mandiri.lelang.base.BaseFragment;
import com.sparkworks.mandiri.lelang.fragment.FilterFragment.FilterType;
import com.sparkworks.mandiri.lelang.model.MasterData;
import com.sparkworks.mandiri.lelang.model.Params;

public class PencarianFragment extends BaseFragment{
	private Spinner spnKategori, spnHarga, spnLokasi;
	private EditText edtKeyword;
	private Button btnCari;
	private CheckBox cbJualSukarela, cbLelang;
	
	public static String FRAGMENT_TAG = "PencarianFragment";
	
	private ArrayList<MasterData> listCategory, listCity, listPrice, listProvince;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initializeLibs();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_pencarian, container, false);
		initializeViews(view);
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initializeProcess();
		initializeActions();
	}
	
	@Override
	public void initializeLibs() {
		// TODO Auto-generated method stub
		super.initializeLibs();
		
		listCategory = new ArrayList<MasterData>();
		listCity = new ArrayList<MasterData>();
		listPrice = new ArrayList<MasterData>();
		listProvince = new ArrayList<>();
	}
	
	@Override
	public void initializeViews(View view) {
		// TODO Auto-generated method stub
		super.initializeViews(view);
		
		setHasOptionsMenu(true);
		
		spnHarga = (Spinner)view.findViewById(R.id.spnPencarianHargaMaksimal);
		spnKategori = (Spinner)view.findViewById(R.id.spnPencarianKategori);
		spnLokasi = (Spinner)view.findViewById(R.id.spnPencarianLokasi);
		btnCari = (Button)view.findViewById(R.id.btnPencarian);
		edtKeyword = (EditText)view.findViewById(R.id.edt_keyword);
		cbJualSukarela = (CheckBox)view.findViewById(R.id.cb_jual_sukarela);
		cbLelang = (CheckBox)view.findViewById(R.id.cb_lelang);
	}
	
	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		super.initializeProcess();

		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		listCategory = categoryHelper.loadAllData();
		listCity = cityHelper.loadAllData();
		listPrice = minMaxPriceHelper.loadAllData();
		listProvince = provinceHelper.loadAllData();
		
		ArrayList<String> category = new ArrayList<String>();
		category.add("Semua");
		for (int i = 0; i < listCategory.size(); i++) {
			category.add(listCategory.get(i).getName());
		}
		
		ArrayAdapter<String> adapterKategori = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_dropdown_item_1line,
				android.R.id.text1, category);
		spnKategori.setAdapter(adapterKategori);
		
		ArrayList<String> price = new ArrayList<String>();
		price.add("Semua");
		for (int i = 0; i < listPrice.size(); i++) {
			price.add(listPrice.get(i).getName());
		}
		
		ArrayAdapter<String> adapterHarga = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_dropdown_item_1line,
				android.R.id.text1, price);
		spnHarga.setAdapter(adapterHarga);
		
		
		ArrayList<String> lokasi = new ArrayList<String>();
		lokasi.add("Semua");
		for (int i = 0; i < listProvince.size(); i++) {
			lokasi.add(listProvince.get(i).getName());
		}
		
		ArrayAdapter<String> adapterLokasi = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_dropdown_item_1line,
				android.R.id.text1, lokasi);
		spnLokasi.setAdapter(adapterLokasi);
		
	}
	
	@Override
	public void initializeActions() {
		// TODO Auto-generated method stub
		super.initializeActions();
		
		btnCari.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String categoryId = spnKategori.getSelectedItemPosition()==0?"0":
					listCategory.get(spnKategori.getSelectedItemPosition()-1).getId();
				String provinceId = spnLokasi.getSelectedItemPosition()==0?"0":
					listProvince.get(spnLokasi.getSelectedItemPosition()-1).getId();
				String maxPriceId = spnHarga.getSelectedItemPosition()==0?"0":
					listPrice.get(spnHarga.getSelectedItemPosition()-1).getId();
                String keyword = edtKeyword.getText().toString().trim();
				toSearchResult(categoryId, provinceId, maxPriceId, keyword);
			}
		});
	}
	
	protected void toSearchResult(String categoryId, String provinceId, String maxPriceId, String keyword) {
		// TODO Auto-generated method stub
		Params params = new Params();
		params.setPageIndex("0");
		params.setCategoryId(categoryId);
		params.setProvinceId(provinceId);
		params.setCityId("0");
		params.setMinPriceId("0");
		params.setMaxPriceId(maxPriceId);
		params.setHotPrice("0");
		params.setAuctionDate("");
		params.setDocumentTypeId("0");
		params.setAddress("");
		params.setOrderBy("1");
        params.setKeyword(keyword);
		params.setType1(cbJualSukarela.isChecked());
		params.setType2(cbLelang.isChecked());
		
		SearchResultActivity.toSearchResultActivity(getActivity(), params);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_pencarian, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		if (item.getItemId()==R.id.action_filter) {
			FilterActivity.toFilterFragmentActivity(getActivity(), FilterType.ALL);
		}
		
		return super.onOptionsItemSelected(item);
	}
}
