package com.sparkworks.mandiri.lelang.fragment;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;
import com.mandiri.lelang.aplikasi.DetilJadwalLelangActivity;
import com.mandiri.lelang.aplikasi.FilterJadwalLelangActivity;
import com.mandiri.lelang.aplikasi.KpknlActivity;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.adapter.EndlessAdapter;
import com.sparkworks.mandiri.lelang.adapter.JadwalLelangAdapter;
import com.sparkworks.mandiri.lelang.base.BaseFragment;
import com.sparkworks.mandiri.lelang.model.AuctionSchedule;
import com.sparkworks.mandiri.lelang.model.AuctionScheduleParam;
import com.sparkworks.mandiri.lelang.request.api.DownloadAuctionScheduleRequest;
import com.sparkworks.mandiri.lelang.service.DownloadScheduleService;
import com.sparkworks.mandiri.lelang.utils.ApiHelper;
import com.sparkworks.mandiri.lelang.utils.Constant;
import com.sparkworks.mandiri.lelang.utils.LoadMoreListView;
import com.sparkworks.mandiri.lelang.utils.URLs;
import com.sparkworks.mandiri.lelang.utils.Utils;

public class AuctionScheduleFragment extends BaseFragment
	implements View.OnClickListener{
	public static String FRAGMENT_TAG = "JadwalLelangFragment";
	
	private LoadMoreListView lvJadwalLelang;
	private LinearLayout lnList;
	private Button btnDownloadCSV;
	private LinkedList<AuctionSchedule> listItem;
	private List<NameValuePair> listParams;
	private TextView txtError;
	private ProgressBar indicator;
	private int pageIndex = 0;
	
	private boolean isLoadmore = false, isFirstRequest = false;
	private boolean isFilter = false;

	private AsyncHttpClient asyncHttpClient;
	private JadwalLelangAdapter jadwalLelangAdapter;
	private AuctionScheduleParam mAuctionScheduleParam = null;

	public AuctionScheduleFragment(){}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		listItem = new LinkedList<>();
		asyncHttpClient = new AsyncHttpClient(true, 80, 443);
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_jadwal_lelang, container, false);
        initializeViews(rootView);
        
        return rootView;
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		initializeLibs();
		initializeRequest();
		initializeProcess();
		initializeActions();
	}

	@Override
	public void initializeLibs() {
		super.initializeLibs();
		mAuctionScheduleParam = new AuctionScheduleParam();
		mAuctionScheduleParam.setListKnpl(new ArrayList<Integer>());
	}

	@Override
	public void initializeViews(View view) {
		// TODO Auto-generated method stub
		super.initializeViews(view);
		lvJadwalLelang = (LoadMoreListView) view.findViewById(R.id.lv_jadwal_lelang);
		indicator = (ProgressBar)view.findViewById(R.id.indicator);
		txtError = (TextView)view.findViewById(R.id.txtErrorMessage);
		lnList = (LinearLayout)view.findViewById(R.id.ln_list);
		btnDownloadCSV = (Button)view.findViewById(R.id.btn_download);
		btnDownloadCSV.setOnClickListener(this);
	}
	
	@Override
	public void initializeRequest() {
		// TODO Auto-generated method stub
		super.initializeRequest();
		
		isFirstRequest = true;
		pageIndex = 0;
		getActionSchedule(pageIndex);
	}
	
	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		super.initializeProcess();

		jadwalLelangAdapter = new JadwalLelangAdapter(getActivity());
		jadwalLelangAdapter.setListItem(listItem);
		lvJadwalLelang.setAdapter(jadwalLelangAdapter);
	}
	
	@Override
	public void initializeActions() {
		// TODO Auto-generated method stub
		super.initializeActions();
		
		lvJadwalLelang.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				toJadwalListDetil(arg2);
			}
		});

		lvJadwalLelang.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
			@Override
			public void onLoadMore() {
				isLoadmore = true;
				getActionSchedule(pageIndex);
			}
		});
	}

	
	private void toJadwalListDetil(int position) {
		// TODO Auto-generated method stub
		
		DetilJadwalLelangActivity.toDetilJadwalLelang(getActivity(), listItem.get(position));
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_jadwal_lelang, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		if (item.getItemId() == R.id.action_filter){
			Intent intent = new Intent(getActivity(), FilterJadwalLelangActivity.class);
			startActivityForResult(intent, FilterJadwalLelangActivity.REQUEST_CODE);
		}

		if (item.getItemId()==R.id.action_refresh) {
			initializeRequest();
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	public void getActionSchedule(int position){
		if (Utils.isConnectInet(getActivity())) {
			if (isFilter){
				listItem.clear();
			}

			if (!isLoadmore){
				indicator.setVisibility(View.VISIBLE);
				lnList.setVisibility(View.GONE);
				txtError.setVisibility(View.GONE);
			}
			
			listParams = ApiHelper.getAuctionScheduleParams(position);
			String url = ApiHelper.buildAPIURLMessage(URLs.AUCTION_SCHEDULE, null, listParams);
			Gson gson = new Gson();



			String bodyParam = gson.toJson(mAuctionScheduleParam);
			StringEntity s = null;
			try {
				s = new StringEntity(bodyParam);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

			if (s != null){
				asyncHttpClient.post(getActivity(), url, s, "application/json", new AsyncHttpResponseHandler(){
					@Override
					public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
						super.onSuccess(statusCode, headers, responseBody);

						if (!isLoadmore){
							indicator.setVisibility(View.GONE);
							lnList.setVisibility(View.VISIBLE);
						}

						bindToListview(new String(responseBody));
					}


					@Override
					public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
						super.onFailure(statusCode, headers, responseBody, error);

						if (!isLoadmore){
							indicator.setVisibility(View.GONE);
							lnList.setVisibility(View.VISIBLE);
							txtError.setVisibility(View.VISIBLE);
							txtError.setText(R.string.error_jadwal_lelang);
						}else{
							lvJadwalLelang.onLoadMoreComplete();
						}
					}
				});
			}

		} else {
			if (!isLoadmore){
				indicator.setVisibility(View.GONE);
				lnList.setVisibility(View.VISIBLE);
				txtError.setVisibility(View.VISIBLE);
				txtError.setText(R.string.no_internet);
			}else{
				lvJadwalLelang.onLoadMoreComplete();
				Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
			}
		}
		
	}

	private void bindToListview(String response){
		Log.d("AuctionSchedule", response);
		LinkedList<AuctionSchedule> list = AuctionSchedule.getListAuctionSchedule(response);
		if (list != null) {
			int currentPos = 0;
			if (isFirstRequest){
				isFirstRequest = false;
				listItem.addAll(list);

			}else{
				for (AuctionSchedule i : list){
					listItem.addLast(i);
				}
			}

			jadwalLelangAdapter.setListItem(listItem);
			jadwalLelangAdapter.notifyDataSetChanged();

			if (isLoadmore){
				currentPos = (pageIndex * 12) - 1;
				lvJadwalLelang.setSelection(currentPos);
				pageIndex += 1;
				isLoadmore = false;
				lvJadwalLelang.onLoadMoreComplete();
			}else{
				lvJadwalLelang.setSelection(0);
				pageIndex += 1;
			}

		}else{
			lvJadwalLelang.onLoadMoreComplete();
		}
	}

	@Override
	public void onClick(View view) {
		if (view.getId() == R.id.btn_download){
			if (mAuctionScheduleParam != null){
				Intent intent = new Intent(getActivity(), DownloadScheduleService.class);
				intent.putExtra(DownloadScheduleService.EXTRA_AUCTION_PARAM, mAuctionScheduleParam);
				getActivity().startService(intent);
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == FilterJadwalLelangActivity.REQUEST_CODE){
			if (resultCode == FilterJadwalLelangActivity.RESULT_CODE){
				pageIndex = 0;
				isFilter = true;
				isFirstRequest = true;
				mAuctionScheduleParam = data.getParcelableExtra(FilterJadwalLelangActivity.EXTRA_AUCTION_PARAM);
				getActionSchedule(pageIndex);
			}
		}
	}
}
