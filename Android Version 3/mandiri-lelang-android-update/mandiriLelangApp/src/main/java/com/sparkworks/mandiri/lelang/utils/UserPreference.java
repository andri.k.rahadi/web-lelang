package com.sparkworks.mandiri.lelang.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by sidiqpermana on 11/24/16.
 */

public class UserPreference {
    private final String PREF_NAME = "MandiriLelangUser.Prefs";
    private final String KEY_TOKEN = "token";
    private final String KEY_MEMBER_ID = "memberId";
    private final String KEY_FULLNAME = "fullName";
    private final String KEY_EMAIL = "email";
    private final String KEY_OPEN_LAUNCHSCREEN = "launchscreen";
    private final String KEY_CHECK_TOC = "check_toc";
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public UserPreference(Context context) {
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void setToken(String token){
        editor.putString(KEY_TOKEN, token);
        editor.apply();
    }

    public String getToken(){
        return preferences.getString(KEY_TOKEN, null);
    }

    public void setMemberId(int memberId){
        editor.putInt(KEY_MEMBER_ID, memberId);
        editor.apply();
    }

    public int getMemberId(){
        return preferences.getInt(KEY_MEMBER_ID, 0);
    }

    public void setFullname(String fullName){
        editor.putString(KEY_FULLNAME, fullName);
        editor.apply();
    }

    public String getFullname(){
        return preferences.getString(KEY_FULLNAME, null);
    }

    public void setEmail(String email){
        editor.putString(KEY_EMAIL, email);
        editor.commit();
    }

    public String getEmail(){
        return preferences.getString(KEY_EMAIL, null);
    }

    public void setOpenLaunchscreen(boolean isAlreadyOpen){
        editor.putBoolean(KEY_OPEN_LAUNCHSCREEN, isAlreadyOpen);
        editor.apply();
    }

    public boolean isOpenLaunchscreen(){
        return preferences.getBoolean(KEY_OPEN_LAUNCHSCREEN, false);
    }

    public void setCheckToc(boolean isAlreadyChecked){
        editor.putBoolean(KEY_CHECK_TOC, isAlreadyChecked);
        editor.apply();
    }

    public boolean isCheckToc(){
        return preferences.getBoolean(KEY_CHECK_TOC, false);
    }

    public void clear(){
        editor.clear();
        editor.apply();
    }
}
