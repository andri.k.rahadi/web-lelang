package com.sparkworks.mandiri.lelang.request.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.sparkworks.mandiri.lelang.model.User;

/**
 * Created by sidiqpermana on 11/29/16.
 */

public class Profile implements Parcelable {
    @SerializedName("fullName")
    private String fullName;
    @SerializedName("email")
    private String email;
    @SerializedName("placeOfBirth")
    private String placeOfBirth;
    @SerializedName("dateOfBirth")
    private String dateOfBirth;
    @SerializedName("mobilePhoneNumber")
    private String mobilePhoneNumber;
    @SerializedName("address")
    private String address;
    @SerializedName("provinceID")
    private String provinceId;
    @SerializedName("cityID")
    private String cityId;
    @SerializedName("identificationNumber")
    private String identificationNumber;
    @SerializedName("memberID")
    private int memberId;
    private String password;
    private String confirmPassword;

    @SerializedName("oldPassword")
    private String oldPassword;
    /*@SerializedName("oldPassword")
    private String prevPassword;*/

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    public void setMobilePhoneNumber(String mobilePhoneNumber) {
        this.mobilePhoneNumber = mobilePhoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

   /* public String getPrevPassword() {
        return prevPassword;
    }

    public void setPrevPassword(String prevPassword) {
        this.prevPassword = prevPassword;
    }*/

    public static Profile getProfileParam(User user){
        Profile profileParam = new Profile();
        profileParam.setFullName(user.getName());
        profileParam.setCityId(String.valueOf(user.getCityId()));
        profileParam.setConfirmPassword(user.getConfirmPassword());
        profileParam.setDateOfBirth(user.getBirthDate());
        profileParam.setPlaceOfBirth(user.getBirthPlace());
        profileParam.setEmail(user.getEmail());
        profileParam.setPassword(user.getPassword());
        profileParam.setIdentificationNumber(user.getIdNo());
        profileParam.setProvinceId(String.valueOf(user.getProvinceId()));
        profileParam.setAddress(user.getAddress());
        profileParam.setMobilePhoneNumber(user.getPhoneNo());
        profileParam.setOldPassword(user.getOldPassword());
        //profileParam.setPrevPassword(user.getOldPassword());

        return profileParam;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fullName);
        dest.writeString(this.email);
        dest.writeString(this.placeOfBirth);
        dest.writeString(this.dateOfBirth);
        dest.writeString(this.mobilePhoneNumber);
        dest.writeString(this.address);
        dest.writeString(this.provinceId);
        dest.writeString(this.cityId);
        dest.writeString(this.identificationNumber);
        dest.writeInt(this.memberId);
        dest.writeString(this.password);
        dest.writeString(this.confirmPassword);
        dest.writeString(this.oldPassword);
        //dest.writeString(this.prevPassword);
    }

    public Profile() {
    }

    protected Profile(Parcel in) {
        this.fullName = in.readString();
        this.email = in.readString();
        this.placeOfBirth = in.readString();
        this.dateOfBirth = in.readString();
        this.mobilePhoneNumber = in.readString();
        this.address = in.readString();
        this.provinceId = in.readString();
        this.cityId = in.readString();
        this.identificationNumber = in.readString();
        this.memberId = in.readInt();
        this.password = in.readString();
        this.confirmPassword = in.readString();
        this.oldPassword = in.readString();
        //this.prevPassword = in.readString();
    }

    public static final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel source) {
            return new Profile(source);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };
}
