package com.sparkworks.mandiri.lelang.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sparkworks.mandiri.lelang.fragment.LoginFragment;
import com.sparkworks.mandiri.lelang.fragment.RegisterFragment;

/**
 * Created by sidiqpermana on 10/29/16.
 */

public class RegisterLoginFragmentPagerAdapter extends FragmentPagerAdapter {
    private String[] titles = new String[]{
            "Register", "Login"
    };

    private RegisterFragment registerFragment;
    private LoginFragment loginFragment;
    private String source;
    private boolean isProfile = false;

    public boolean isProfile() {
        return isProfile;
    }

    public void setProfile(boolean profile) {
        isProfile = profile;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public RegisterLoginFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                if (registerFragment == null){
                    Bundle bundle = new Bundle();
                    bundle.putString(RegisterFragment.EXTRA_SOURCE, getSource());

                    registerFragment = new RegisterFragment();
                    registerFragment.setArguments(bundle);
                }
                return registerFragment;

            case 1:
                if (loginFragment == null){
                    loginFragment = new LoginFragment();
                }
                return loginFragment;

            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
