package com.sparkworks.mandiri.lelang.fragment;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.mandiri.lelang.aplikasi.KuesionerActivity;
import com.mandiri.lelang.aplikasi.LaunchscreenActivity;
import com.mandiri.lelang.aplikasi.MainActivity;
import com.mandiri.lelang.aplikasi.ProfileActivity;
import com.mandiri.lelang.aplikasi.R;
import com.mandiri.lelang.aplikasi.StaticPageActivity;
import com.sparkworks.mandiri.lelang.base.BaseFragment;
import com.sparkworks.mandiri.lelang.database.helper.CityHelper;
import com.sparkworks.mandiri.lelang.database.helper.ProvinceHelper;
import com.sparkworks.mandiri.lelang.database.helper.SystemSettingHelper;
import com.sparkworks.mandiri.lelang.event.ClearErrorInViewsEvent;
import com.sparkworks.mandiri.lelang.model.FacebookProfile;
import com.sparkworks.mandiri.lelang.model.MasterData;
import com.sparkworks.mandiri.lelang.model.User;
import com.sparkworks.mandiri.lelang.request.api.GetProfileRequest;
import com.sparkworks.mandiri.lelang.request.api.PostFacebookRequest;
import com.sparkworks.mandiri.lelang.request.api.PostRegisterUpdateRequest;
import com.sparkworks.mandiri.lelang.request.model.FacebookData;
import com.sparkworks.mandiri.lelang.request.model.LoginData;
import com.sparkworks.mandiri.lelang.request.model.MemberParam;
import com.sparkworks.mandiri.lelang.request.model.Preference;
import com.sparkworks.mandiri.lelang.request.model.Profile;
import com.sparkworks.mandiri.lelang.request.response.PostFacebookResponse;
import com.sparkworks.mandiri.lelang.request.response.ProfileResponse;
import com.sparkworks.mandiri.lelang.request.response.RegisterUpdateResponse;
import com.sparkworks.mandiri.lelang.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends BaseFragment
        implements View.OnClickListener,
        PostRegisterUpdateRequest.OnPostRegisterUpdateListener,
        GetProfileRequest.OnGetProfileRequestListener,
        PostFacebookRequest.OnPostFacebookRequestListener,
        CompoundButton.OnCheckedChangeListener{
    private Spinner spnProvince, spnCity;
    private Button btnRegister, btnRegisterFb, btnNext, btnSkipCancel;
    private TextView tvTermCondition, tvPassword, tvConfirmPassword,
        tvName, tvEmail, tvBirthPlace, tvBirthDate, tvPhoneNo, tvAddress, tvProvince, tvCity,
        tvID;
    private EditText edtName, edtEmail, edtBirthPlace, edtBirthDate, edtPhoneNo,
    edtIDno, edtPassword, edtConfirmPassword, edtAddress, edtOldPassword;
    private ImageView imgChooseDate;
    private LinearLayout lnNextRegisterFb, lnCancelNext, lnOldPassword;
    private LoginButton btnFbLoginButton;
    private LinearLayout lnPassword;
    private CheckBox cbPassword, cbConfirmPassword, cbOldPassword;

    private int mYear;
    private int mMonth;
    private int mDay;
    static final int DATE_DIALOG_ID = 0;

    private User user = null;

    private String source;
    private boolean isProfile = false, isFacebookLogin = false;
    private LoginData loginData = null;
    private Preference userPreferenceParam = null;
    private Profile userProfileParam = null;
    private CallbackManager callbackManager;

    public static final String EXTRA_SOURCE = "extra_source";
    public static final String EXTRA_LOGIN_DATA = "extra_login_data";
    public static final String EXTRA_IS_FACEBOOK_LOGIN = "extra_is_facebook_login";

    public static final String SOURCE_REGISTER = "source_register";
    public static final String SOURCE_LOGIN = "source_login";
    public static final String SOURCE_HOME = "source_home";

    private ProvinceHelper provinceHelper;
    private CityHelper cityHelper;
    private PostRegisterUpdateRequest postRegisterUpdateRequest;
    private GetProfileRequest getProfileRequest;
    private PostFacebookRequest postFacebookRequest;

    private ArrayList<MasterData> listProvince, listCity;
    private int selectedProvinceId = 0, selectedCityId = 0;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        spnCity = (Spinner)view.findViewById(R.id.spn_city);
        spnProvince = (Spinner)view.findViewById(R.id.spn_province);
        btnRegister = (Button)view.findViewById(R.id.btn_register);
        btnRegisterFb = (Button)view.findViewById(R.id.btn_register_fb);
        btnNext = (Button)view.findViewById(R.id.btn_next);
        btnSkipCancel = (Button)view.findViewById(R.id.btn_skip_cancel);
        btnFbLoginButton = (LoginButton)view.findViewById(R.id.btn_fb_loginButton);
        btnFbLoginButton.setReadPermissions(Arrays.asList("email"));
        btnFbLoginButton.setOnClickListener(this);
        btnFbLoginButton.setFragment(this);

        tvTermCondition = (TextView)view.findViewById(R.id.tv_termscondition);
        btnNext.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        btnRegisterFb.setOnClickListener(this);
        btnSkipCancel.setOnClickListener(this);
        edtBirthDate = (EditText)view.findViewById(R.id.edt_birth_date);
        edtBirthPlace = (EditText)view.findViewById(R.id.edt_birth_place);
        edtConfirmPassword = (EditText)view.findViewById(R.id.edt_confirm_password);
        edtEmail = (EditText)view.findViewById(R.id.edt_email);
        edtIDno = (EditText)view.findViewById(R.id.edt_id_no);
        edtPassword = (EditText)view.findViewById(R.id.edt_password);
        edtName = (EditText)view.findViewById(R.id.edt_name);
        edtPhoneNo = (EditText)view.findViewById(R.id.edt_phone_np);
        edtAddress = (EditText)view.findViewById(R.id.edt_alamat);
        imgChooseDate = (ImageView)view.findViewById(R.id.img_select_date);
        imgChooseDate.setOnClickListener(this);
        lnCancelNext = (LinearLayout)view.findViewById(R.id.ln_cancel_next);
        lnNextRegisterFb = (LinearLayout)view.findViewById(R.id.ln_next_register_fb);
        lnPassword = (LinearLayout)view.findViewById(R.id.ln_password);

        tvTermCondition.setText(Html.fromHtml("<u>Lihat Syarat & Ketentuan</u>"));
        tvTermCondition.setOnClickListener(this);

        lnOldPassword = (LinearLayout)view.findViewById(R.id.ln_old_password);
        tvPassword = (TextView)view.findViewById(R.id.tv_password);
        tvConfirmPassword = (TextView)view.findViewById(R.id.tv_confirm_password);
        edtOldPassword = (EditText)view.findViewById(R.id.edt_old_password);

        tvName = (TextView)view.findViewById(R.id.tv_name);
        tvEmail = (TextView)view.findViewById(R.id.tv_email);
        tvBirthPlace = (TextView)view.findViewById(R.id.tv_birthplace);
        tvBirthDate = (TextView)view.findViewById(R.id.tv_birthdate);
        tvPhoneNo = (TextView)view.findViewById(R.id.tv_phoneno);
        tvAddress = (TextView)view.findViewById(R.id.tv_address);
        tvProvince = (TextView)view.findViewById(R.id.tv_province);
        tvCity = (TextView)view.findViewById(R.id.tv_city);
        tvID = (TextView)view.findViewById(R.id.tv_citizen_id);

        cbConfirmPassword = (CheckBox)view.findViewById(R.id.cb_confirm_password);
        cbPassword = (CheckBox)view.findViewById(R.id.cb_password);
        cbOldPassword = (CheckBox)view.findViewById(R.id.cb_old_password);

        cbOldPassword.setOnCheckedChangeListener(this);
        cbPassword.setOnCheckedChangeListener(this);
        cbConfirmPassword.setOnCheckedChangeListener(this);
        edtConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                edtPassword.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edtBirthDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                edtBirthDate.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        tvName.setText("Nama Lengkap *");
        tvEmail.setText("Email *");

        String[] mandatoryFields = new String[]{
                "Tempat Lahir", "Tanggal Lahir", "No Handphone",
                "Alamat Tempat Tinggal", "Provinsi", "Kota", "Nomor KTP",
        };

        TextView[] mandatoryLabels = new TextView[]{
                tvBirthPlace, tvBirthDate, tvPhoneNo, tvAddress, tvProvince, tvCity, tvID,
        };

        SystemSettingHelper systemSettingHelper = new SystemSettingHelper(getActivity());
        systemSettingHelper.open();

        ArrayList<MasterData> listSystemSettings = new ArrayList<>();
        for (MasterData systemSetting : systemSettingHelper.loadAllData()){
            if (!systemSetting.getId().equalsIgnoreCase("LastEmailSentDateTime")){
                if (!systemSetting.getId().equalsIgnoreCase("MobileNotificationSyncPeriod")){
                    if (!systemSetting.getId().equalsIgnoreCase("MandatoryForMemberPreference")){
                        listSystemSettings.add(systemSetting);
                    }
                }
            }
        }

        for (int i = 0; i < listSystemSettings.size(); i++) {
            if (listSystemSettings.get(i).getId().equalsIgnoreCase("MandatoryForMemberPlaceOfBirth")){
                if (listSystemSettings.get(i).getName().equalsIgnoreCase("True")){
                    mandatoryLabels[0].setText(mandatoryFields[0]+" *");
                }
            }

            if (listSystemSettings.get(i).getId().equalsIgnoreCase("MandatoryForMemberDateOfBirth")){
                if (listSystemSettings.get(i).getName().equalsIgnoreCase("True")){
                    mandatoryLabels[1].setText(mandatoryFields[1]+" *");
                }
            }

            if (listSystemSettings.get(i).getId().equalsIgnoreCase("MandatoryForMemberMobilePhoneNumber")){
                if (listSystemSettings.get(i).getName().equalsIgnoreCase("True")){
                    mandatoryLabels[2].setText(mandatoryFields[2]+" *");
                }
            }

            if (listSystemSettings.get(i).getId().equalsIgnoreCase("MandatoryForMemberAddress")){
                if (listSystemSettings.get(i).getName().equalsIgnoreCase("True")){
                    mandatoryLabels[3].setText(mandatoryFields[3]+" *");
                }
            }

            if (listSystemSettings.get(i).getId().equalsIgnoreCase("MandatoryForMemberProvince")){
                if (listSystemSettings.get(i).getName().equalsIgnoreCase("True")){
                    mandatoryLabels[4].setText(mandatoryFields[4]+" *");
                }
            }

            if (listSystemSettings.get(i).getId().equalsIgnoreCase("MandatoryForMemberCity")){
                if (listSystemSettings.get(i).getName().equalsIgnoreCase("True")){
                    mandatoryLabels[5].setText(mandatoryFields[5]+" *");
                }
            }

            if (listSystemSettings.get(i).getId().equalsIgnoreCase("MandatoryForMemberIdentificationNumber")){
                if (listSystemSettings.get(i).getName().equalsIgnoreCase("True")){
                    mandatoryLabels[6].setText(mandatoryFields[6]+" *");
                }
            }
        }

        if (getArguments() != null){
            source = getArguments().getString(EXTRA_SOURCE);
            isFacebookLogin = getArguments().getBoolean(EXTRA_IS_FACEBOOK_LOGIN, false);

            if (source.equalsIgnoreCase(SOURCE_REGISTER)){
                lnNextRegisterFb.setVisibility(View.VISIBLE);
                lnCancelNext.setVisibility(View.GONE);

                btnRegister.setText("Next");
                btnRegisterFb.setText("Daftar dengan facebook");
            }else if(source.equalsIgnoreCase(SOURCE_LOGIN)){
                lnCancelNext.setVisibility(View.VISIBLE);
                lnNextRegisterFb.setVisibility(View.GONE);

                isProfile = true;

                btnNext.setText("Next");
                btnSkipCancel.setText("Skip");

                loginData = getArguments().getParcelable(EXTRA_LOGIN_DATA);

                edtName.setText(loginData.getFullName());
                edtEmail.setText(loginData.getEmail());

                if (isFacebookLogin){
                    lnPassword.setVisibility(View.GONE);
                }

            }else if (source.equalsIgnoreCase(SOURCE_HOME)){
                lnCancelNext.setVisibility(View.VISIBLE);
                lnNextRegisterFb.setVisibility(View.GONE);

                isProfile = true;

                lnOldPassword.setVisibility(View.VISIBLE);
                tvPassword.setText("Password Baru");
                tvConfirmPassword.setText("Konfirmasi Password Baru");

                btnSkipCancel.setText("Cancel");
                btnNext.setText("Next");
            }
        }

        return view;
    }

    private void clearAllErrorInViews() {
        edtName.setError(null);
        edtEmail.setError(null);
        edtBirthPlace.setError(null);
        edtBirthDate.setError(null);
        edtPhoneNo.setError(null);
        edtAddress.setText(null);
        if ((TextView)spnProvince.getSelectedView() != null){
            ((TextView)spnProvince.getSelectedView()).setError(null);
        }

        if ((TextView)spnCity.getSelectedView() != null){
            ((TextView)spnCity.getSelectedView()).setError(null);
        }
        edtPhoneNo.setError(null);
        edtConfirmPassword.setError(null);
        edtPassword.setError(null);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();

        postRegisterUpdateRequest = new PostRegisterUpdateRequest();
        postRegisterUpdateRequest.setContext(getActivity());
        postRegisterUpdateRequest.setOnPostRegisterUpdateListener(this);

        getProfileRequest = new GetProfileRequest();
        getProfileRequest.setContext(getActivity());
        getProfileRequest.setOnGetProfileRequestListener(this);

        postFacebookRequest = new PostFacebookRequest();
        postFacebookRequest.setContext(getActivity());
        postFacebookRequest.setOnPostFacebookRequestListener(this);

        provinceHelper = new ProvinceHelper(getActivity());
        provinceHelper.open();

        cityHelper = new CityHelper(getActivity());
        cityHelper.open();

        listCity = new ArrayList<>();
        listProvince = new ArrayList<>();

        listProvince.addAll(provinceHelper.loadAllData());

        ArrayList<String> provinces = new ArrayList<>();
        provinces.add("");
        for (MasterData data : listProvince){
            provinces.add(data.getName());
        }

        spnProvince.setAdapter(new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line,
                android.R.id.text1,
                provinces));

        initializeActions();

        if (isProfile){
            //to do load user data
            showProgressDialog("Profile", getString(R.string.general_progress_dialog_message));
            getProfileRequest.callApi();
        }

        btnFbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                final AccessToken accessToken = loginResult.getAccessToken();
                getFacebookUserProfile(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        FacebookProfile facebookProfile = FacebookProfile.parseResponse(object.toString());
                        facebookProfile.setFacebookToken(accessToken.getToken());

                        String firstName = !TextUtils.isEmpty(facebookProfile.getFirstName()) ? facebookProfile.getFirstName() : "";
                        String lastName = !TextUtils.isEmpty(facebookProfile.getLastName()) ? facebookProfile.getLastName() : "";
                        String name = firstName +" "+ lastName;

                        FacebookData facebookData = new FacebookData();
                        facebookData.setEmail(facebookProfile.getEmail());
                        facebookData.setFullName(name.trim());
                        facebookData.setId(facebookProfile.getId());
                        facebookData.setToken(accessToken.getToken());
                        facebookData.setUrl(facebookProfile.getLink());
                        postFacebookRequest.setData(facebookData);

                        showProgressDialog("Login", getString(R.string.general_progress_dialog_message));

                        postFacebookRequest.callApi();
                    }
                });
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        initDate();

    }

    @Override
    public void initializeActions() {
        super.initializeActions();

        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (listCity.size() > 0){
                    listCity.clear();
                }

                ArrayList<String> cities = new ArrayList<>();
                int selectedCityPos = 0;

                if (i == 0){
                    cities.add("");
                }else{
                    if (listProvince.size() > 0){
                        String provinceId = selectedProvinceId > 0 ? String.valueOf(selectedProvinceId) : listProvince.get(i - 1).getId();
                        listCity.addAll(cityHelper.loadAllCityByProvince(provinceId));
                        cities.add("");
                        for (int j = 0; j < listCity.size(); j++){
                            cities.add(listCity.get(j).getName());
                            if (selectedCityId > 0){
                                if (Integer.parseInt(listCity.get(j).getId()) == selectedCityId){
                                    selectedCityPos = j;
                                }
                            }
                        }

                        if (isProfile){
                            selectedCityPos +=1;
                        }
                    }

                }

                spnCity.setAdapter(new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_dropdown_item_1line,
                        android.R.id.text1,
                        cities));
                spnCity.setSelection(selectedCityPos, true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void  getFacebookUserProfile(AccessToken accessToken, final GraphRequest.GraphJSONObjectCallback callback) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                Log.d("Facebook", "onCompleted: " + response.toString());
                callback.onCompleted(object, response);
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,last_name,email,gender,location,link,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_register:
                submit();
                break;

            case R.id.btn_next:
                submit();
                break;

            case R.id.btn_register_fb:
                //todo register
                btnFbLoginButton.performClick();
                break;

            case R.id.btn_skip_cancel:
                MainActivity.toMainActivity(getActivity());
                getActivity().finish();
                break;

            case R.id.tv_termscondition:
                StaticPageActivity.toStaticPageActivity(getActivity(), StaticPageActivity.PAGE_TYPE.SYARAT);
                break;

            case R.id.img_select_date:
                new DatePickerDialog(getActivity(),
                        mDateSetListener,
                        mYear, mMonth, mDay).show();
                break;
        }
    }

    private void submit() {
        String name = edtName.getText().toString().trim();
        String email = edtEmail.getText().toString().trim();
        String birthDate = edtBirthDate.getText().toString().trim();
        String birthPlace = edtBirthPlace.getText().toString().trim();
        String phoneNo = edtPhoneNo.getText().toString().trim();
        int provinceId = 0;
        int cityId = 0;

        if (listProvince.size() > 0){
            if (!TextUtils.isEmpty((String)spnProvince.getSelectedItem())){
                if (Integer.parseInt(listProvince.get(spnProvince.getSelectedItemPosition() - 1).getId()) > 0){
                    provinceId = Integer.parseInt(listProvince.get(spnProvince.getSelectedItemPosition() - 1).getId());
                }
            }
        }

        if (listCity.size() > 0){
            if (!TextUtils.isEmpty((String)spnCity.getSelectedItem())){
                if (Integer.parseInt(listCity.get(spnCity.getSelectedItemPosition() - 1).getId()) > 0){
                    cityId = Integer.parseInt(listCity.get(spnCity.getSelectedItemPosition() - 1).getId());
                }
            }
        }

        String idNo = edtIDno.getText().toString().trim();
        String password = edtPassword.getText().toString().trim();
        String confirmPassword = edtConfirmPassword.getText().toString().trim();
        String address = edtAddress.getText().toString().trim();
        String fixBirthDate = edtBirthDate.getText().toString().trim();
        String oldPassword = edtOldPassword.getText().toString().trim();

        user = new User();
        user.setName(name);
        user.setBirthDate(fixBirthDate);
        user.setBirthPlace(birthPlace);
        user.setCityId(cityId);
        user.setConfirmPassword(confirmPassword);
        user.setPassword(password);
        user.setEmail(email);
        user.setPhoneNo(phoneNo);
        user.setProvinceId(provinceId);
        user.setIdNo(idNo);
        user.setAddress(address);
        user.setOldPassword(oldPassword);

       /* if (lnOldPassword.getVisibility() == View.VISIBLE){
            String oldPassword = edtOldPassword.getText().toString().trim();
            user.setOldPassword(oldPassword);
        }*/

        Profile profileParam = Profile.getProfileParam(user);

        MemberParam memberParam = new MemberParam();
        memberParam.setProfileParam(profileParam);

        if (source.equalsIgnoreCase(SOURCE_REGISTER)){
            postRegisterUpdateRequest.setPostType(PostRegisterUpdateRequest.POST_REGISTER);
        }else{
            postRegisterUpdateRequest.setPostType(PostRegisterUpdateRequest.POST_UPDATE);
        }
        postRegisterUpdateRequest.setMemberParam(memberParam);
        postRegisterUpdateRequest.validateMemberParam();
    }

    private void initDate(){
        // get the current date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        updateDisplay();

    }

    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                    updateDisplay();
                }
            };

    private void updateDisplay() {
        String year,month,day;
        year = mYear+"";
        if(mMonth + 1 < 10)
            month = "0" + (mMonth + 1);
        else
            month = (mMonth+1) + "";
        if(mDay < 10)
            day = "0" + mDay;
        else
            day = mDay + "";
        edtBirthDate.setText(
                new StringBuilder()
                        // Month is 0 based so add 1
                        .append(year).append("-").append(month).append("-")
                        .append(day));
    }

    @Override
    public void onDestroy() {
        if (cityHelper != null){
            cityHelper.close();
        }

        if (provinceHelper != null){
            provinceHelper.close();
        }

        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onPostRegisterSuccess(RegisterUpdateResponse registerUpdateResponse) {

    }

    @Override
    public void onPostUpdateSuccess(ProfileResponse profileResponse) {

    }

    @Override
    public void onPostRegisterUpdateFailed(String errorMessage) {

    }

    @Override
    public void onValidateMemberParamSuccess() {
        if (user != null){
            if (isProfile){
                KuesionerActivity.start(getActivity(), user, isProfile, source, userPreferenceParam);
            }else{
                KuesionerActivity.start(getActivity(), user, isProfile, source);
            }
        }
    }

    @Override
    public void onValidatePreferenceSuccess() {

    }

    @Override
    public void onParamsInvalid(HashMap<String, String> invalidParams) {

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_FULLNAME)){
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_FULLNAME).equalsIgnoreCase(PostRegisterUpdateRequest.PARAM_FULLNAME)){
                edtName.setError(getString(R.string.error_field_empty));
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_MOBILE_PHONE_NUMBER)){
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_MOBILE_PHONE_NUMBER).equalsIgnoreCase(PostRegisterUpdateRequest.PARAM_MOBILE_PHONE_NUMBER)){
                edtPhoneNo.setError(getString(R.string.error_field_empty));
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_EMAIL)){
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_EMAIL).equalsIgnoreCase(PostRegisterUpdateRequest.PARAM_EMAIL)){
                edtEmail.setError(getString(R.string.error_email_empty));
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_PLACE_OF_BIRTH)) {
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_PLACE_OF_BIRTH).equalsIgnoreCase(PostRegisterUpdateRequest.PARAM_PLACE_OF_BIRTH)) {
                edtBirthPlace.setError(getString(R.string.error_field_empty));
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_PLACE_OF_BIRTH)) {
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_PLACE_OF_BIRTH).equalsIgnoreCase(PostRegisterUpdateRequest.PARAM_PLACE_OF_BIRTH_NOT_VALID)) {
                edtBirthPlace.setError(getString(R.string.error_birtplace_not_valid));
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_DATE_OF_BIRTH)) {
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_DATE_OF_BIRTH).equalsIgnoreCase(PostRegisterUpdateRequest.PARAM_DATE_OF_BIRTH)) {
                edtBirthDate.setError(getString(R.string.error_field_empty));
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_INVALID_DATE_OF_BIRTH)) {
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_INVALID_DATE_OF_BIRTH).equalsIgnoreCase(PostRegisterUpdateRequest.PARAM_INVALID_DATE_OF_BIRTH)) {
                edtBirthDate.setError(getString(R.string.error_invalid_date_of_birth));
                Toast.makeText(getActivity(), getString(R.string.error_invalid_date_of_birth), Toast.LENGTH_SHORT).show();
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_IDENTIFICATION_NUMBER)) {
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_IDENTIFICATION_NUMBER).equalsIgnoreCase(PostRegisterUpdateRequest.PARAM_IDENTIFICATION_NUMBER)) {
                edtIDno.setError(getString(R.string.error_field_empty));
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_ADDRESS)) {
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_ADDRESS).equalsIgnoreCase(PostRegisterUpdateRequest.PARAM_ADDRESS)) {
                edtAddress.setError(getString(R.string.error_field_empty));
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_PASSWORD)) {
            if (!isProfile){
                if (invalidParams.get(PostRegisterUpdateRequest.ERROR_PASSWORD).equalsIgnoreCase(PostRegisterUpdateRequest.PARAM_PASSWORD)) {
                    edtPassword.setError(getString(R.string.error_field_empty));
                }
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_CONFIRM_PASSWORD)) {
            if (!isProfile){
                if (invalidParams.get(PostRegisterUpdateRequest.ERROR_CONFIRM_PASSWORD).equalsIgnoreCase(PostRegisterUpdateRequest.PARAM_CONFIRM_PASSWORD)) {
                    edtConfirmPassword.setError(getString(R.string.error_field_empty));
                }
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_PASSWORD_NOT_MATCH)){
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_PASSWORD_NOT_MATCH)
                    .equalsIgnoreCase(PostRegisterUpdateRequest.PARAM_PASSWORD_NOT_MATCH)){
                edtPassword.setError(getString(R.string.error_password_not_match));
                edtConfirmPassword.setError(getString(R.string.error_password_not_match));
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_PASSWORD_NOT_VALID)){
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_PASSWORD_NOT_VALID).equalsIgnoreCase(PostRegisterUpdateRequest.PARAM_PASSWORD_NOT_VALID)){
                edtPassword.setError(getString(R.string.error_password_validation));
                edtConfirmPassword.setError(getString(R.string.error_password_validation));
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_OLD_PASSWORD)) {
            if (lnOldPassword.getVisibility() == View.VISIBLE){
                if (invalidParams.get(PostRegisterUpdateRequest.ERROR_OLD_PASSWORD)
                        .equalsIgnoreCase(PostRegisterUpdateRequest.PARAM_OLD_PASSWORD)) {
                    edtOldPassword.setError(getString(R.string.error_field_empty));
                }
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_PROVINCE_ID)){
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_PROVINCE_ID).equalsIgnoreCase(PostRegisterUpdateRequest.PARAM_PROVINCE_ID)){
                ((TextView)spnProvince.getSelectedView()).setError(getString(R.string.error_field_empty));
                Toast.makeText(getActivity(), "Provinsi harus diisi", Toast.LENGTH_SHORT).show();
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_CITY_ID)){
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_CITY_ID).equalsIgnoreCase(PostRegisterUpdateRequest.PARAM_CITY_ID)){
                ((TextView)spnCity.getSelectedView()).setError(getString(R.string.error_field_empty));
                Toast.makeText(getActivity(), "Kota harus diisi", Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onGetProfileSuccess(ProfileResponse profileResponse) {
        dismissProgressDialog();
        userPreferenceParam = profileResponse.getProfileData().getPreferenceParam();
        userProfileParam = profileResponse.getProfileData().getProfileParam();

        edtEmail.setText(userProfileParam.getEmail());
        edtName.setText(userProfileParam.getFullName());
        edtAddress.setText(userProfileParam.getAddress());
        edtIDno.setText(userProfileParam.getIdentificationNumber());
        edtBirthDate.setText(userProfileParam.getDateOfBirth());
        edtBirthPlace.setText(userProfileParam.getPlaceOfBirth());
        edtPhoneNo.setText(userProfileParam.getMobilePhoneNumber());

        if(!TextUtils.isEmpty(userProfileParam.getProvinceId())){
            selectedProvinceId = Integer.parseInt(userProfileParam.getProvinceId());
        }

        if (!TextUtils.isEmpty(userProfileParam.getCityId())){
            selectedCityId = Integer.parseInt(userProfileParam.getCityId());
        }

        int selectedProvincePos = 0;

        if (selectedCityId > 0 && selectedProvinceId > 0){
            for (int i = 0; i < listProvince.size(); i++){
                if (Integer.parseInt(listProvince.get(i).getId()) == selectedProvinceId){
                    selectedProvincePos = i;
                    break;
                }
            }
            spnProvince.setSelection((selectedProvincePos + 1), true);
        }

        String birthDate = userProfileParam.getDateOfBirth();
        if(!TextUtils.isEmpty(birthDate)){
            String d[] = birthDate.split("-");
            mYear = Integer.parseInt(d[0]);
            mMonth = Integer.parseInt(d[1]);
            mDay = Integer.parseInt(d[2]);
        }
    }

    @Override
    public void onGetProfileFailed(String errorMessage) {
        dismissProgressDialog();
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
        if (errorMessage.toUpperCase(Locale.ENGLISH).contains("TOKEN")){
            userPreference.clear();
            LaunchscreenActivity.start(getActivity());
            getActivity().finish();
        }
    }

    @Override
    public void onPostFacebookSuccess(PostFacebookResponse response) {
        dismissProgressDialog();

        Log.d("Login", "Success : email "+response.getLoginData().getEmail());
        dismissProgressDialog();

        userPreference.setEmail(response.getLoginData().getEmail());
        userPreference.setFullname(response.getLoginData().getFullName());
        userPreference.setMemberId(response.getLoginData().getMemberId());
        userPreference.setToken(response.getLoginData().getToken());

        if (response.getLoginData().getIsProfileComplete()){
            MainActivity.toMainActivity(getActivity());
        }else{
            ProfileActivity.start(getActivity(), RegisterFragment.SOURCE_LOGIN, response.getLoginData(), true);
        }

        getActivity().finish();

    }

    @Override
    public void onPostFacebookFailed(String errorMessage) {
        dismissProgressDialog();
        Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onClearAllErrorInViews(ClearErrorInViewsEvent e){
        clearAllErrorInViews();
        e.printLog();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()){
            case R.id.cb_confirm_password:
                showHidePassword(compoundButton, edtConfirmPassword);
                break;

            case R.id.cb_password:
                showHidePassword(compoundButton, edtPassword);
                break;

            case R.id.cb_old_password:
                showHidePassword(compoundButton, edtOldPassword);
                break;
        }
    }

    private void showHidePassword(CompoundButton compoundButton, EditText editText) {
        if (compoundButton.isChecked()){
            editText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        }else{
            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
        editText.setSelection(editText.length());
    }

}
