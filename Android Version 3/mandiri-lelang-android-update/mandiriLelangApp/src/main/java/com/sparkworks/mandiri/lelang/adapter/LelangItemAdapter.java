package com.sparkworks.mandiri.lelang.adapter;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import java.util.Locale;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.model.Item;
import com.sparkworks.mandiri.lelang.utils.ApiHelper.LelangType;
import com.sparkworks.mandiri.lelang.utils.Constant;
import com.sparkworks.mandiri.lelang.utils.PicassoTrustAll;
import com.squareup.picasso.Picasso;

public class LelangItemAdapter extends BaseAdapter{
	private Activity activity;
	private LinkedList<Item> listItem;
	private LelangType lelangType;
	private boolean isJualSukarela = false;
	
	public LelangItemAdapter(Activity activity, LinkedList<Item> listItem, LelangType lelangType) {
		// TODO Auto-generated constructor stub
		this.listItem = listItem;
		this.activity = activity;
		this.lelangType = lelangType;
		setListItem(listItem);
	}

	public boolean isJualSukarela() {
		return isJualSukarela;
	}

	public void setJualSukarela(boolean jualSukarela) {
		isJualSukarela = jualSukarela;
	}

	public LinkedList<Item> getListItem() {
		return listItem;
	}

	public void setListItem(LinkedList<Item> listItem) {
		this.listItem = listItem;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listItem.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return getListItem().get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void updateItem(int position, Item item){
		getListItem().set(position, item);
		notifyDataSetChanged();
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		View view = arg1;
		ViewHolder holder = null;
		
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.inf_item_lelang, null);
			holder = new ViewHolder();
			holder.txtItemAddress = (TextView)view.findViewById(R.id.txtInfItemLelangAlamat);
			holder.txtItemNewPrice = (TextView)view.findViewById(R.id.txtInfItemLelangHargaBaru);
			holder.txtItemOldPrice = (TextView)view.findViewById(R.id.txtInfItemlelangHargaLama);
			holder.txtTglLelang = (TextView)view.findViewById(R.id.txtInfItemLelangTanggalLelang);
			holder.txtTglPosting = (TextView)view.findViewById(R.id.txtInfItemLelangTanggal);
			holder.txtCategory = (TextView)view.findViewById(R.id.txtInfItemLelangCategory);
			holder.txtHotPriceIndicator = (TextView)view.findViewById(R.id.txtInfItemLelangHotPriceIndicator);
			holder.imgItem = (ImageView)view.findViewById(R.id.imgInfItemLelang);
			holder.imgItemFavorite = (ImageView)view.findViewById(R.id.img_item_favorite);
			holder.txtEAuction = (TextView)view.findViewById(R.id.txtInfItemEauction);
			view.setTag(holder);
		} else {
			holder = (ViewHolder)view.getTag();
		}
		
		trustEveryone();
		
		holder.txtItemAddress.setText(listItem.get(arg0).getCompleteAddress());
		
		holder.txtItemNewPrice.setTextColor(Color.BLACK);
		if (listItem.get(arg0).getNewPrice().toLowerCase(Locale.ENGLISH).equals(Constant.CALL)) {
			holder.txtItemNewPrice.setTextColor(Color.RED);
			holder.txtItemNewPrice.setTypeface(Typeface.DEFAULT_BOLD);
		}
		
		holder.txtItemNewPrice.setText(listItem.get(arg0).getNewPrice());
		
		holder.txtItemOldPrice.setVisibility(View.GONE);
		
		if (!listItem.get(arg0).getOldPrice().equals("")) {
			holder.txtItemOldPrice.setVisibility(View.VISIBLE);
			holder.txtItemOldPrice.setText(listItem.get(arg0).getOldPrice());
			holder.txtItemOldPrice.setPaintFlags(holder.txtItemOldPrice.getPaintFlags()|
					Paint.STRIKE_THRU_TEXT_FLAG);
		}else{
			holder.txtItemOldPrice.setVisibility(View.GONE);
		}
		
		holder.txtTglLelang.setVisibility(View.VISIBLE);
		if (!isJualSukarela()){
			if (listItem.get(arg0).getTypeAgunan() == Constant.TYPE_JUAL_SUKARELA ||
					listItem.get(arg0).isType1()){
				holder.txtTglLelang.setText("Jual Sukarela");
			}else{
				if (!listItem.get(arg0).getAuctionDateFormatted().equals("")) {
					holder.txtTglLelang.setText("Tanggal Lelang : "+listItem.get(arg0).getAuctionDateFormatted());
				} else {
					holder.txtTglLelang.setVisibility(View.GONE);
				}
			}
		}else{
			holder.txtTglLelang.setText("Jual Sukarela");
		}
		
		holder.txtTglPosting.setText(listItem.get(arg0).getPostedDateFormatted());
		holder.txtCategory.setText(listItem.get(arg0).getCategoryName());
		
		PicassoTrustAll.getInstance(activity)
				.load(listItem.get(arg0)
						.getPhotoUrl()).placeholder(R.drawable.default_image_item) .into(holder.imgItem);

		/*Picasso.with(activity)
				.load(listItem.get(arg0)
						.getPhotoUrl()).placeholder(R.drawable.default_image_item) .into(holder.imgItem);*/

		if (!isJualSukarela()){
			holder.txtHotPriceIndicator.setVisibility(View.GONE);
			if (listItem.get(arg0).isHotPrice()) {
				holder.txtHotPriceIndicator.setVisibility(View.VISIBLE);
			}
		}

		holder.imgItemFavorite.setColorFilter(ContextCompat.getColor(activity, R.color.mandiri_grey));
		if (listItem.get(arg0).isFavorite()){
			holder.imgItemFavorite.setColorFilter(ContextCompat.getColor(activity, R.color.mandiri_yellow));
		}

		if (!isJualSukarela()){
			holder.txtEAuction.setVisibility(View.GONE);
			if (listItem.get(arg0).isEmailAuction()){
				holder.txtEAuction.setVisibility(View.VISIBLE);
			}
		}

		return view;
	}
	
	static class ViewHolder{
		ImageView imgItem, imgItemFavorite;
		TextView txtItemAddress, txtItemOldPrice, txtItemNewPrice, 
		txtTglLelang, txtTglPosting, txtCategory, txtHotPriceIndicator, txtEAuction;
	}
	
	private void trustEveryone() {
		try {
			HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
	    			public boolean verify(String hostname, SSLSession session) {
	    				return true;
	    			}});
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, new X509TrustManager[]{new X509TrustManager(){
				public void checkClientTrusted(X509Certificate[] chain,
						String authType) throws CertificateException {}
				public void checkServerTrusted(X509Certificate[] chain,
						String authType) throws CertificateException {}
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[0];
				}}}, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(
					context.getSocketFactory());
		} catch (Exception e) { // should never happen
			e.printStackTrace();
		}
	}
}
