package com.sparkworks.mandiri.lelang.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sidiqpermana on 11/30/16.
 */

public class AuctionScheduleParam implements Parcelable {
    @SerializedName("fromDate")
    private String fromDate;
    @SerializedName("toDate")
    private String toDate;
    @SerializedName("kpknlIDs")
    private ArrayList<Integer> listKnpl = new ArrayList<>();

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public ArrayList<Integer> getListKnpl() {
        return listKnpl;
    }

    public void setListKnpl(ArrayList<Integer> listKnpl) {
        this.listKnpl = listKnpl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fromDate);
        dest.writeString(this.toDate);
        dest.writeList(this.listKnpl);
    }

    public AuctionScheduleParam() {
    }

    protected AuctionScheduleParam(Parcel in) {
        this.fromDate = in.readString();
        this.toDate = in.readString();
        this.listKnpl = new ArrayList<Integer>();
        in.readList(this.listKnpl, Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<AuctionScheduleParam> CREATOR = new Parcelable.Creator<AuctionScheduleParam>() {
        @Override
        public AuctionScheduleParam createFromParcel(Parcel source) {
            return new AuctionScheduleParam(source);
        }

        @Override
        public AuctionScheduleParam[] newArray(int size) {
            return new AuctionScheduleParam[size];
        }
    };
}
