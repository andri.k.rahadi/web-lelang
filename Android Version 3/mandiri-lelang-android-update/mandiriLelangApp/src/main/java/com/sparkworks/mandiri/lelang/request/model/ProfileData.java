package com.sparkworks.mandiri.lelang.request.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sidiqpermana on 12/1/16.
 */

public class ProfileData {
    @SerializedName("profile")
    private Profile profileParam;
    @SerializedName("preference")
    private Preference preferenceParam;

    public Profile getProfileParam() {
        return profileParam;
    }

    public void setProfileParam(Profile profileParam) {
        this.profileParam = profileParam;
    }

    public Preference getPreferenceParam() {
        return preferenceParam;
    }

    public void setPreferenceParam(Preference preferenceParam) {
        this.preferenceParam = preferenceParam;
    }
}
