package com.sparkworks.mandiri.lelang.request.api;

import java.util.HashMap;

/**
 * Created by sidiqpermana on 11/29/16.
 */

public interface BaseListener {
    void onParamsInvalid(HashMap<String, String> invalidParams);
}
