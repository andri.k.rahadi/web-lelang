package com.sparkworks.mandiri.lelang.model.response;

import com.sparkworks.mandiri.lelang.model.ResultCode;

/**
 * Created by sidiqpermana on 9/24/17.
 */

public class BaseResponse {
    private ResultCode resultCode;

    public ResultCode getResultCode() {
        return resultCode;
    }

    public void setResultCode(ResultCode resultCode) {
        this.resultCode = resultCode;
    }
}
