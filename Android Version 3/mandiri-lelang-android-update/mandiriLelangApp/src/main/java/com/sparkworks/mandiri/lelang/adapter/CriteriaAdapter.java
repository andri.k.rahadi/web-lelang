package com.sparkworks.mandiri.lelang.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.model.CriteriaItem;
import com.sparkworks.mandiri.lelang.model.ProvinceItem;

import java.util.ArrayList;

/**
 * Created by sidiqpermana on 11/11/16.
 */

public class CriteriaAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<CriteriaItem> listCriteria;
    private KpknlAdapter.OnCheckAllCallback onCheckAllCallback;

    public KpknlAdapter.OnCheckAllCallback getOnCheckAllCallback() {
        return onCheckAllCallback;
    }

    public void setOnCheckAllCallback(KpknlAdapter.OnCheckAllCallback onCheckAllCallback) {
        this.onCheckAllCallback = onCheckAllCallback;
    }

    public CriteriaAdapter(Activity activity) {
        this.activity = activity;
    }

    public ArrayList<CriteriaItem> getListCriteria() {
        return listCriteria;
    }

    public void setListCriteria(ArrayList<CriteriaItem> listCriteria) {
        this.listCriteria = listCriteria;
    }

    @Override
    public int getCount() {
        return getListCriteria().size();
    }

    @Override
    public Object getItem(int i) {
        return getListCriteria().get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null){
            LayoutInflater inflater = LayoutInflater.from(activity);
            view = inflater.inflate(R.layout.item_province, null);
            final CriteriaAdapter.ViewHolder holder = new CriteriaAdapter.ViewHolder();
            holder.tvName = (TextView)view.findViewById(R.id.tv_item_province);
            holder.cbItem = (CheckBox)view.findViewById(R.id.cb_item_province);
            holder.cbItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    CriteriaItem item = (CriteriaItem) holder.cbItem.getTag();
                    item.setChecked(compoundButton.isChecked());

                    if (!compoundButton.isChecked()){
                        getOnCheckAllCallback().onCheckAll();
                    }
                }
            });
            view.setTag(holder);
            holder.cbItem.setTag(getListCriteria().get(i));
        }else{
            ((CriteriaAdapter.ViewHolder)view.getTag()).cbItem.setTag(getListCriteria().get(i));
        }

        CriteriaAdapter.ViewHolder holder = (CriteriaAdapter.ViewHolder)view.getTag();
        holder.tvName.setText(getListCriteria().get(i).getName());
        holder.cbItem.setChecked(getListCriteria().get(i).isChecked());

        return view;
    }

    static class ViewHolder{
        CheckBox cbItem;
        TextView tvName;
    }
}
