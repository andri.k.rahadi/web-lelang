package com.sparkworks.mandiri.lelang.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.base.BaseFragment;
import com.sparkworks.mandiri.lelang.model.AuctionSchedule;

public class OldDetilAuctionScheduleFragment extends BaseFragment{
	public static String FRAGMENT_TAG = "DetilJadwalLelangFragment";
	private RelativeLayout rlTanggal, rlPhone1, rlPhone2, rlKpnkpl;
	private TextView txtTanggal, txtAlamat, txtKpnkpl, txtPhone1, txtPhone2;
	
	public AuctionSchedule jadwalLelangItem;
	
	public AuctionSchedule getJadwalLelangItem() {
		return jadwalLelangItem;
	}

	public void setJadwalLelangItem(AuctionSchedule jadwalLelangItem) {
		this.jadwalLelangItem = jadwalLelangItem;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_detil_jadwal_lelang, container, false);
		initializeViews(view);
	
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initializeProcess();
		initializeActions();
	}
	
	@Override
	public void initializeViews(View view) {
		// TODO Auto-generated method stub
		super.initializeViews(view);
		
		txtTanggal = (TextView)view.findViewById(R.id.txt_detil_jadwal_tanggal);
		txtAlamat = (TextView)view.findViewById(R.id.txt_detil_jadwal_alamat);
		txtKpnkpl = (TextView)view.findViewById(R.id.txt_detil_jadwal_kpknl);
		txtPhone1 = (TextView)view.findViewById(R.id.txt_detil_jadwal_no_telp_1);
		txtPhone2 = (TextView)view.findViewById(R.id.txt_detil_jadwal_no_telp_2);
		
		rlTanggal = (RelativeLayout)view.findViewById(R.id.rl_detil_jadwal_tanggal);
		rlKpnkpl = (RelativeLayout)view.findViewById(R.id.rl_detil_jadwal_kpknl);
		rlPhone1 = (RelativeLayout)view.findViewById(R.id.rl_detil_jadwal_no_telp_1);
		rlPhone2 = (RelativeLayout)view.findViewById(R.id.rl_detil_jadwal_no_telp_2);
	}
	
	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		super.initializeProcess();
		
//		txtTanggal.setText(getJadwalLelangItem().getTanggal());
//		txtAlamat.setText(getJadwalLelangItem().getAlamat());
//		txtKpnkpl.setText("KPKNL : "+getJadwalLelangItem().getKpknl());
//		
//		if (getJadwalLelangItem().getPhone1().equals("")) {
//			rlPhone1.setVisibility(View.GONE);
//		} else {
//			txtPhone1.setText(getJadwalLelangItem().getPhone1());
//		}
//		
//		if (getJadwalLelangItem().getPhone2().equals("")) {
//			rlPhone2.setVisibility(View.GONE);
//		} else {
//			txtPhone2.setText(getJadwalLelangItem().getPhone2());
//		}
	}
	
	@Override
	public void initializeActions() {
		// TODO Auto-generated method stub
		super.initializeActions();
		rlTanggal.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				toJadwalListDetil();
			}
		});
		
		rlKpnkpl.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				toJadwalListDetil();
			}
		});
		
		rlPhone1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				Utils.phoneDial(getActivity(), getJadwalLelangItem().getPhone1());
			}
		});
		
		rlPhone2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				Utils.phoneDial(getActivity(), getJadwalLelangItem().getPhone2());
			}
		});
	}
	
	private void toJadwalListDetil() {
		// TODO Auto-generated method stub
//		Params params = Params.getParams("0",
//				"0", "0", "0", "0", "0", "0", "0", "0", "", "1");
//		LelangListingFragment lelangListingFragment = new LelangListingFragment();
//		lelangListingFragment.setUrl(URLs.GET_ALL_ASSET);
//		lelangListingFragment.setLelangType(LelangType.ALL);
//		lelangListingFragment.setParams(params);
//		
//		FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//		fragmentTransaction.hide((DetilJadwalLelangFragment)getFragmentManager().findFragmentByTag(DetilJadwalLelangFragment.FRAGMENT_TAG));
//		fragmentTransaction.add(R.id.frame_container, lelangListingFragment, LelangListingFragment.FRAGMENT_TAG);
//		fragmentTransaction.addToBackStack(null);
//		fragmentTransaction.commit();
	}
}
