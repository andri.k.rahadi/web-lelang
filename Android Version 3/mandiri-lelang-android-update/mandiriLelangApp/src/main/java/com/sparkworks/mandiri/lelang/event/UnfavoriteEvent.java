package com.sparkworks.mandiri.lelang.event;

import com.sparkworks.mandiri.lelang.model.Item;
import com.sparkworks.mandiri.lelang.utils.ApiHelper;

/**
 * Created by sidiqpermana on 12/2/16.
 */

public class UnfavoriteEvent {
    private int assetId;
    private ApiHelper.LelangType lelangType;
    private int position;
    private Item updatedItem;

    public Item getUpdatedItem() {
        return updatedItem;
    }

    public void setUpdatedItem(Item updatedItem) {
        this.updatedItem = updatedItem;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public ApiHelper.LelangType getLelangType() {
        return lelangType;
    }

    public void setLelangType(ApiHelper.LelangType lelangType) {
        this.lelangType = lelangType;
    }

    public int getAssetId() {
        return assetId;
    }

    public void setAssetId(int assetId) {
        this.assetId = assetId;
    }
}
