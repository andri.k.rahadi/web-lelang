package com.sparkworks.mandiri.lelang.service;

import android.app.DownloadManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.model.AuctionScheduleParam;
import com.sparkworks.mandiri.lelang.request.api.DownloadAuctionScheduleRequest;

import java.io.File;


public class DownloadScheduleService extends IntentService
    implements DownloadAuctionScheduleRequest.OnDownloadAuctionScheduleListener{

    private DownloadAuctionScheduleRequest downloadAuctionScheduleRequest;
    public static final String EXTRA_AUCTION_PARAM = "extra_auction_param";
    private AuctionScheduleParam mAuctionScheduleParam = null;
    private int notifId = 0;

    public DownloadScheduleService() {
        super("DownloadScheduleService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        downloadAuctionScheduleRequest = new DownloadAuctionScheduleRequest();
        downloadAuctionScheduleRequest.setContext(getApplicationContext());
        downloadAuctionScheduleRequest.setOnDownloadAuctionScheduleListener(this);

        mAuctionScheduleParam = intent.getParcelableExtra(EXTRA_AUCTION_PARAM);

        downloadAuctionScheduleRequest.setAuctionScheduleParam(mAuctionScheduleParam);

        String preMessage = null;

        if (!TextUtils.isEmpty(mAuctionScheduleParam.getToDate()) && !TextUtils.isEmpty(mAuctionScheduleParam.getFromDate())){
            preMessage = "File jadwal lelang "+ mAuctionScheduleParam.getFromDate() +" hingga "+mAuctionScheduleParam.getToDate();
        }else{
            preMessage = "File jadwal lelang yang dibutuhkan";
        }

        notifId = (int)System.currentTimeMillis();

        showNotification(getBaseContext(), "Download File", preMessage, null, notifId);
        downloadAuctionScheduleRequest.callApi();

    }

    private void showNotification(Context context, String title, String message, File outputFile, int notifId){
        PendingIntent pendingIntent = null;
        if (outputFile != null){
            Uri selectedUri = Uri.parse(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + "/"
                    + DownloadAuctionScheduleRequest.MANDIRI_LELANG_DIR + "/" + outputFile.getName()+".csv");
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setDataAndType(selectedUri, "text/csv");

            pendingIntent = PendingIntent.getActivity(context, notifId, intent, PendingIntent.FLAG_ONE_SHOT);
        }

        NotificationManager notificationManagerCompat = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(title)
                .setAutoCancel(true)
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                .setSound(alarmSound);

        notificationManagerCompat.notify(notifId, builder.build());
    }

    @Override
    public void onDownloadAuctionScheduleSuccess(File file) {
        String postMessage = "File download terdapat di Download/mandiri-lelang";
        showNotification(getBaseContext(), "Download Selesai", postMessage, file, notifId);
    }

    @Override
    public void onDownloadAuctionScheduleFailed(String message) {
        String postMessage = "File gagal didownload";
        showNotification(getBaseContext(), "Download Selesai", postMessage, null, notifId);
    }
}
