package com.sparkworks.mandiri.lelang.utils;

public class URLs {
//	public static String BASE_URL = "http://mandirilelang.rixdev.net/api/v1";
	public static String BASE_IP = "http://ricardoalexander-001-site2.etempurl.com/v3";
//	public static String BASE_IP = "https://lelang.bankmandiri.co.id/";
	// static String BASE_IP = "https://3188-16056.el-alt.com";
	//public static String BASE_IP = "https://10.204.35.147";
//	public static String BASE_URL = BASE_IP+"/api/v2";
	public static String BASE_URL = BASE_IP;
	
	public static String BASE_ASSET = BASE_URL + "/asset/";
	public static String GET_ALL_ASSET = BASE_ASSET + "all";
	public static final String ITEM_GET_ALL_SOON = BASE_ASSET + "soon"; 
	public static final String ITEM_GET_HOT_PRICE = BASE_ASSET + "hot";
	public static final String ITEM_GET_LATEST = BASE_ASSET + "latest";
	public static final String ITEM_GET_NEAREST = BASE_ASSET + "nearest";
	public static final String ITEM_GET_DETAILS = BASE_ASSET + "get";
	public static final String ITEM_GET_FAVORITE = BASE_ASSET + "favorite";
	public static final String ITEM_GET_DIRECT_SELL = BASE_ASSET + "direct-sell";

	public static final String MASTER_DATA = BASE_URL + "/master/data";
	
	public static final String AUCTION_SCHEDULE = BASE_ASSET + "auctionschedule";
	public static final String DOWNLOAD_AUCTION_SCHEDULE = BASE_ASSET + "download-auctionschedule";
	public static final String ASSET_AUCTION_SCHEDULE = BASE_ASSET + "byschedule";
	public static final String CONTACT_US = BASE_URL + "/contact/contactus";


	public static final String SERVER_BASE_URL = BASE_IP;
//	public static final String SERVER_BASE_URL = BASE_IP + "/api";
//	public static final String SERVER_API_VERSION = SERVER_BASE_URL + "/v2";
	public static final String SERVER_API_VERSION = SERVER_BASE_URL;
	public static final String SERVER_MASTER_DATA = SERVER_API_VERSION + "/master/data";
    public static final String SERVER_API_GROUP_MEMBER = SERVER_API_VERSION + "/Member";
    public static final String SERVER_LOGIN = SERVER_API_GROUP_MEMBER + "/login";
    public static final String SERVER_REGISTER = SERVER_API_GROUP_MEMBER + "/register";
    public static final String SERVER_UPDATE = SERVER_API_GROUP_MEMBER + "/update";
    public static final String SERVER_PROFILE = SERVER_API_GROUP_MEMBER + "/profile";
    public static final String SERVER_FB = SERVER_API_GROUP_MEMBER + "/fb";
    public static final String SERVER_LOGOUT = SERVER_API_GROUP_MEMBER + "/logout";
	public static final String SERVER_RESET_PASSWORD = SERVER_API_GROUP_MEMBER + "/reset-password";
	public static final String SERVER_FAVORITE = SERVER_API_GROUP_MEMBER + "/favorite";
	public static final String SERVER_UNFAVORITE = SERVER_API_GROUP_MEMBER + "/unfavorite";

	public static final String URL_VIDEO = "http://ricardoalexander-001-site2.etempurl.com/v3/video/all";

}
