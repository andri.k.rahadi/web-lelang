package com.sparkworks.mandiri.lelang.request.api;

/**
 * Created by sidiqpermana on 11/24/16.
 */

public abstract class BaseApiMethod{
    public abstract void callApi();
    public abstract void cancel();
    public abstract void retry();
}