package com.sparkworks.mandiri.lelang.base;

import android.view.View;

public interface DefaultMethods {
	public void initializeViews();
	public void initializeViews(View view);
	public void initializeLibs();
	public void initializeActions();
	public void initializeRequest();
	public void initializeProcess();
}
