package com.sparkworks.mandiri.lelang.request.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sidiqpermana on 11/24/16.
 */

public class ResultCode {
    @SerializedName("code")
    private int code;
    @SerializedName("message")
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
