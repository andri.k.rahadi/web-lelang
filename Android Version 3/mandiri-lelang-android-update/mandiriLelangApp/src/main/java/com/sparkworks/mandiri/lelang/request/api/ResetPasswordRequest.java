package com.sparkworks.mandiri.lelang.request.api;

import android.content.Context;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.request.response.ResetPasswordResponse;
import com.sparkworks.mandiri.lelang.utils.ApiHelper;
import com.sparkworks.mandiri.lelang.utils.URLs;
import com.sparkworks.mandiri.lelang.utils.Utils;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sidiqpermana on 11/29/16.
 */

public class ResetPasswordRequest extends BaseApi implements FormValidation {
    private Context context;
    private AsyncHttpClient asyncHttpClient;
    private String email;
    private OnResetPasswordRequestListener onResetPasswordRequestListener;

    public static final String ERROR_EMAIL = "error_email";
    public static final String PARAM_EMAIL = "email";

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public OnResetPasswordRequestListener getOnResetPasswordRequestListener() {
        return onResetPasswordRequestListener;
    }

    public void setOnResetPasswordRequestListener(OnResetPasswordRequestListener onResetPasswordRequestListener) {
        this.onResetPasswordRequestListener = onResetPasswordRequestListener;
    }

    @Override
    public boolean isValidParams() {
        boolean isValidParam = true;
        if (!Utils.validEmail(getEmail())){
            isValidParam = false;
            invalidParams.put(PARAM_EMAIL, ERROR_EMAIL);
        }

        return isValidParam;
    }

    @Override
    public void callApi() {
        super.callApi();
        if (isValidParams()){
            if (Utils.isConnectInet(getContext())){
                asyncHttpClient = getAsyncHttpClient(getUserPreference(getContext()).getToken());

                RequestParams params = new RequestParams();
                params.put(PARAM_EMAIL, getEmail());

                List<NameValuePair> list = new ArrayList<>();
                list.add(new BasicNameValuePair(PARAM_EMAIL, getEmail()));
                String url = ApiHelper.buildAPIURLMessage(URLs.SERVER_RESET_PASSWORD, null, list);

                asyncHttpClient.post(url, params, new AsyncHttpResponseHandler(){
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        super.onSuccess(statusCode, headers, responseBody);

                        String s = new String(responseBody);
                        Gson gson = new Gson();
                        ResetPasswordResponse resetPasswordResponse = gson.fromJson(s, ResetPasswordResponse.class);
                        if (resetPasswordResponse.getResultCode().getCode() == CODE_SUCCESS){
                            getOnResetPasswordRequestListener().onResetPasswordSuccess(resetPasswordResponse);
                        }else{
                            getOnResetPasswordRequestListener().onResetPasswordFailed(resetPasswordResponse.getResultCode().getMessage());
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        super.onFailure(statusCode, headers, responseBody, error);
                        getOnResetPasswordRequestListener().onResetPasswordFailed(getContext().getString(R.string.error_reset_password));
                    }
                });
            }else{
                getOnResetPasswordRequestListener().onResetPasswordFailed(getContext().getString(R.string.no_internet));
            }
        }else{
            getOnResetPasswordRequestListener().onParamsInvalid(invalidParams);
        }
    }

    @Override
    public void cancel() {
        super.cancel();
        if (asyncHttpClient != null){
            asyncHttpClient.cancelRequests(getContext(), true);
        }
    }

    @Override
    public void retry() {
        super.retry();
        callApi();
    }

    public interface OnResetPasswordRequestListener extends BaseListener{
        void onResetPasswordSuccess(ResetPasswordResponse resetPasswordResponse);
        void onResetPasswordFailed(String errorMessage);
        @Override
        void onParamsInvalid(HashMap<String, String> invalidParams);
    }
}
