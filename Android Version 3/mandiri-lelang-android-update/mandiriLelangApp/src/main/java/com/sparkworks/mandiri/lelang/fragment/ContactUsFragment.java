package com.sparkworks.mandiri.lelang.fragment;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.base.BaseFragment;
import com.sparkworks.mandiri.lelang.database.helper.WebContentHelper;
import com.sparkworks.mandiri.lelang.model.MasterData;
import com.sparkworks.mandiri.lelang.utils.Constant;
import com.sparkworks.mandiri.lelang.utils.URLs;
import com.sparkworks.mandiri.lelang.utils.Utils;

public class ContactUsFragment extends BaseFragment {
	
	public static String FRAGMENT_TAG = "ContactUsFragment";
	private EditText edtName, edtPhone, edtEmail, edtAssetCode, edtMessage;
	private Spinner spnCategoryId, spnBranchId;
	private Button btnSubmit;
	private WebView tvContactUs;

	private WebContentHelper webContentHelper;
	private ArrayList<MasterData> listCategory, listBranch;
	private AsyncHttpClient asyncHttpClient;

	public static String KEY_KODE_AGUNAN = "kode_agunan";
	public static String KEY_NAMA_PENGELOLA = "nama_pengelola";

	private String kodeAgunan, namaPengelola;

	public ContactUsFragment(){}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		initializeLibs();
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_contactus, container, false);
        
        initializeViews(rootView);
        return rootView;
    }
	
	@Override
	public void initializeViews(View view) {
		// TODO Auto-generated method stub
		super.initializeViews(view);
		edtName = (EditText)view.findViewById(R.id.edt_contactus_name);
        edtEmail = (EditText)view.findViewById(R.id.edt_contactus_email);
        edtMessage = (EditText)view.findViewById(R.id.edt_contactus_message);
        edtAssetCode = (EditText)view.findViewById(R.id.edt_contactus_assetcode);
        edtPhone = (EditText)view.findViewById(R.id.edt_contactus_phone);
        spnBranchId = (Spinner)view.findViewById(R.id.spn_contact_us_branch);
        spnCategoryId = (Spinner)view.findViewById(R.id.spn_contact_us_category);
        btnSubmit = (Button)view.findViewById(R.id.btn_contact_us_submit);
		tvContactUs = (WebView) view.findViewById(R.id.tv_contact_us_text);
	}
	
	@Override
	public void initializeLibs() {
		// TODO Auto-generated method stub
		super.initializeLibs();
		
		listBranch = new ArrayList<MasterData>();
		listCategory = new ArrayList<MasterData>();
		asyncHttpClient = new AsyncHttpClient(true, 80, 443);
		webContentHelper = new WebContentHelper(getActivity());
		webContentHelper.open();
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		initializeIntent();
		initializeProcess();
		initializeActions();
	}

	private void initializeIntent() {
		if (getArguments() != null){
			kodeAgunan = getArguments().getString(KEY_KODE_AGUNAN);
			namaPengelola = getArguments().getString(KEY_NAMA_PENGELOLA);
		}
	}

	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		super.initializeProcess();

		ArrayList<MasterData> list = new ArrayList<>();
		list.addAll(webContentHelper.queryDataById("7"));
		String htmlText = list.get(0).getName();
		if (list.size() > 0){
			tvContactUs.loadDataWithBaseURL(URLs.BASE_URL, htmlText, "text/html", "UTF-8", null);
		}

		listBranch = branchHelper.loadAllData();
		listCategory = contactUsHelper.loadAllData();
		
		ArrayList<String> branch = new ArrayList<String>();
		branch.add("");
		for (int i = 0; i < listBranch.size(); i++) {
			branch.add(listBranch.get(i).getName());
		}
		ArrayAdapter<String> branchAdapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1,
				android.R.id.text1,
				branch);
		spnBranchId.setAdapter(branchAdapter);
		
		ArrayList<String> category = new ArrayList<String>();
		category.add("");
		for (int i = 0; i < listCategory.size(); i++) {
			category.add(listCategory.get(i).getName());
		}
		ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1,
				android.R.id.text1,
				category);
		spnCategoryId.setAdapter(categoryAdapter);

		if (!TextUtils.isEmpty(userPreference.getToken())){
			edtName.setText(userPreference.getFullname());
			edtEmail.setText(userPreference.getEmail());
		}

		if (!TextUtils.isEmpty(kodeAgunan) && !TextUtils.isEmpty(namaPengelola)){
			edtAssetCode.setText(kodeAgunan);

			int position = 0;
			for (int i = 0; i < listBranch.size(); i++) {
				if (namaPengelola.equalsIgnoreCase(listBranch.get(i).getName())){
					position = i;
					break;
				}
			}
			spnBranchId.setSelection((position+1), true);
		}


	}
	
	@Override
	public void initializeActions() {
		// TODO Auto-generated method stub
		super.initializeActions();
		
		btnSubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String name = edtName.getText().toString().trim();
				String email = edtEmail.getText().toString().trim();
				String message = edtMessage.getText().toString().trim();
				String phone = edtPhone.getText().toString().trim();
				String assetCode = edtAssetCode.getText().toString().trim();
				String categoryId = "";
				
				if (listCategory.size()>0) {
					categoryId = spnCategoryId.getSelectedItemPosition()==0?"0":listCategory.get(spnCategoryId.getSelectedItemPosition()-1).getId();
				} else {
					categoryId = "0";
				}
				
				String branchId = spnBranchId.getSelectedItemPosition()==0?"0":listBranch.get(spnBranchId.getSelectedItemPosition()-1).getId();

				boolean isEmptyField = false;

				if (name.equals("")) {
					isEmptyField = true;
					//Toast.makeText(getActivity(), "Field nama harus terisi", Toast.LENGTH_LONG).show();
					edtName.setError(getString(R.string.error_field_empty));
				}

				if(email.equals("")&&Utils.validEmail(email)){
					isEmptyField = true;
					//Toast.makeText(getActivity(), "Field email harus terisi dan harus dengan format email yang valid", Toast.LENGTH_LONG).show();
					edtEmail.setError(getString(R.string.error_email_empty));
				}

				if(phone.equals("")){
					isEmptyField = true;
					//Toast.makeText(getActivity(), "Field phone harus terisi", Toast.LENGTH_LONG).show();
					edtPhone.setError(getString(R.string.error_field_empty));
				}

				if(message.equals("")){
					isEmptyField = true;
					//Toast.makeText(getActivity(), "Field message harus terisi", Toast.LENGTH_LONG).show();
					edtMessage.setError(getString(R.string.error_field_empty));
				}

				if(categoryId.equalsIgnoreCase("0")){
					isEmptyField = true;
					((TextView)spnCategoryId.getSelectedView()).setError(getString(R.string.error_field_empty));
				}

				if (!isEmptyField){
					postContactData(name, email, message, phone, assetCode,
							categoryId, branchId);
				}
			}
		});
	}

	protected void postContactData(String name, String email, String message,
			String phone, String assetCode, String categoryId, String branchId) {
		// TODO Auto-generated method stub
		if (Utils.isConnectInet(getActivity())) {
			final ProgressDialog dialog = new ProgressDialog(getActivity());
			dialog.setMessage("Harap tunggu...");
			dialog.show();
			
			List<NameValuePair> listParams = new ArrayList<NameValuePair>();
			listParams.add(new BasicNameValuePair("name", name));
			listParams.add(new BasicNameValuePair("phone", phone));
			listParams.add(new BasicNameValuePair("email", email));
			listParams.add(new BasicNameValuePair("messageCategoryTypeID", categoryId));
			listParams.add(new BasicNameValuePair("assetCode", assetCode));
			listParams.add(new BasicNameValuePair("branchID", branchId));
			listParams.add(new BasicNameValuePair("textMessage", message));
			
			String requestedParams = URLEncodedUtils.format(listParams, HTTP.UTF_8);
			String token = Utils.getHashedToken(requestedParams);
			
			Log.d(Constant.APP_TAG, requestedParams);
			
			RequestParams params = new RequestParams();
			params.add("name", name);
			params.add("phone", phone);
			params.add("email", email);
			params.add("messageCategoryTypeID", categoryId);
			params.add("assetCode", assetCode);
			params.add("branchID", branchId);
			params.add("textMessage", message);
			params.add("token", token);
			
			Log.d(Constant.APP_TAG, params.toString());
			
			asyncHttpClient.post(URLs.CONTACT_US, params, new AsyncHttpResponseHandler(){
				@Override
				public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
					// TODO Auto-generated method stub
					super.onSuccess(arg0, arg1, arg2);
					dialog.dismiss();
					String response = new String(arg2);
					Log.d(Constant.APP_TAG, response);
					fetchResponse(response);
				}
				
				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2,
						Throwable arg3) {
					// TODO Auto-generated method stub
					super.onFailure(arg0, arg1, arg2, arg3);
					dialog.dismiss();
					Toast.makeText(getActivity(), "Gagal mengirimkan data. Silakan coba lagi", Toast.LENGTH_LONG).show();
				}
			});
		} else {
			Toast.makeText(getActivity(), R.string.no_internet, Toast.LENGTH_LONG).show();
		}
		
	}

	protected void fetchResponse(String response) {
		// TODO Auto-generated method stub
		try {
			JSONObject object = new JSONObject(response);
			if (object.getJSONObject(Constant.RESULT).getInt(Constant.RESULT_CODE)==0) {
				emptyAllFields();
				showSuccessDialog(object.getString(Constant.CONTACT_US_MESSAGE));
			} else {
				Toast.makeText(getActivity(), "Gagal mengirimkan data. Silakan coba lagi", Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			// TODO: handle exception
			Log.d(Constant.APP_TAG, e.getMessage());
		}
	}
	
	private void emptyAllFields() {
		// TODO Auto-generated method stub
		edtName.setText("");
		edtEmail.setText("");
		edtAssetCode.setText("");
		edtMessage.setText("");
		edtPhone.setText("");
		spnBranchId.setSelection(0);
		spnCategoryId.setSelection(0);
	}

	@Override
	public void onDestroy() {
		if (webContentHelper != null){
			webContentHelper.close();
		}
		super.onDestroy();
	}

	private void showSuccessDialog(String message){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
 
		// set title
		alertDialogBuilder.setTitle("Hubungi kami");
 
			// set dialog message
		alertDialogBuilder
			.setMessage(message)
			.setCancelable(false)
			.setPositiveButton("Tutup",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					// current activity
					dialog.dismiss();
				}
			  });
			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();
			// show it
			alertDialog.show();	
	}
	
}
