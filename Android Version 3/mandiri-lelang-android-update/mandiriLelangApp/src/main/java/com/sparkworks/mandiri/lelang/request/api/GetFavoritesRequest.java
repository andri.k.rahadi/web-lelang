package com.sparkworks.mandiri.lelang.request.api;

import android.content.Context;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.mandiri.lelang.aplikasi.R;
import com.sparkworks.mandiri.lelang.model.Item;
import com.sparkworks.mandiri.lelang.request.response.BaseResponse;
import com.sparkworks.mandiri.lelang.utils.URLs;
import com.sparkworks.mandiri.lelang.utils.Utils;

import org.apache.http.Header;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.Locale;

/**
 * Created by sidiqpermana on 12/2/16.
 */

public class GetFavoritesRequest extends BaseApi {
    private AsyncHttpClient asyncHttpClient;
    private Context context;
    private OnGetFavoriteRequestListener onGetFavoriteRequestListener;
    private int pageIndex;

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    @Override
    public AsyncHttpClient getAsyncHttpClient() {
        return asyncHttpClient;
    }

    public void setAsyncHttpClient(AsyncHttpClient asyncHttpClient) {
        this.asyncHttpClient = asyncHttpClient;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public OnGetFavoriteRequestListener getOnGetFavoriteRequestListener() {
        return onGetFavoriteRequestListener;
    }

    public void setOnGetFavoriteRequestListener(OnGetFavoriteRequestListener onGetFavoriteRequestListener) {
        this.onGetFavoriteRequestListener = onGetFavoriteRequestListener;
    }

    @Override
    public void callApi() {
        super.callApi();
        if (Utils.isConnectInet(getContext())){
            asyncHttpClient = getAsyncHttpClient(getUserPreference(getContext()).getToken());
            String url = URLs.ITEM_GET_FAVORITE + "?pageIndex="+getPageIndex();
            asyncHttpClient.get(url, new AsyncHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    super.onSuccess(statusCode, headers, responseBody);
                    String s = new String(responseBody);
                    try {
                        Gson gson = new Gson();
                        BaseResponse b = gson.fromJson(s, BaseResponse.class);

                        if (b.getResultCode().getMessage().toUpperCase(Locale.ENGLISH).equalsIgnoreCase("TOKEN")){
                            getOnGetFavoriteRequestListener().onGetFavoriteRequestFailed("Sesi login anda sudah habis, mohon melakukan login ulang");
                        }else{
                            LinkedList<Item> list = Item.getListItem(s);
                            if (list != null){
                                if (list.size() > 0){
                                    getOnGetFavoriteRequestListener().onGetFavoriteRequestSuccess(list);
                                }else{
                                    getOnGetFavoriteRequestListener().onGetFavoriteRequestFailed(getContext().getString(R.string.error_no_favorite));
                                }
                            }else{
                                getOnGetFavoriteRequestListener().onGetFavoriteRequestFailed(getContext().getString(R.string.error_no_favorite));
                            }
                        }
                    }catch (Exception e){
                        getOnGetFavoriteRequestListener().onGetFavoriteRequestFailed(getContext().getString(R.string.error_unable_connect_to_server));
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    super.onFailure(statusCode, headers, responseBody, error);
                    getOnGetFavoriteRequestListener().onGetFavoriteRequestFailed(getContext().getString(R.string.error_unable_connect_to_server));
                }
            });
        }else{
            getOnGetFavoriteRequestListener().onGetFavoriteRequestFailed(getContext().getString(R.string.no_internet));
        }
    }

    @Override
    public void cancel() {
        super.cancel();
    }

    @Override
    public void retry() {
        super.retry();
    }

    public interface OnGetFavoriteRequestListener{
        void onGetFavoriteRequestSuccess(LinkedList<Item> list);
        void onGetFavoriteRequestFailed(String errorMessage);
    }
}
