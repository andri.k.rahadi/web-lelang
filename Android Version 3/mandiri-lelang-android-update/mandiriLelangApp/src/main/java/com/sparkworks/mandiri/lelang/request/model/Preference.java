package com.sparkworks.mandiri.lelang.request.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sidiqpermana on 11/29/16.
 */

public class Preference implements Parcelable {
    @SerializedName("minPriceID")
    private String minPriceId;
    @SerializedName("maxPriceID")
    private String maxPriceId;
    @SerializedName("categoryIDs")
    private ArrayList<String> categoryIDs = new ArrayList<>();
    @SerializedName("cityIDs")
    private ArrayList<String> cityIDs = new ArrayList<>();


    public String getMinPriceId() {
        return minPriceId;
    }

    public void setMinPriceId(String minPriceId) {
        this.minPriceId = minPriceId;
    }

    public String getMaxPriceId() {
        return maxPriceId;
    }

    public void setMaxPriceId(String maxPriceId) {
        this.maxPriceId = maxPriceId;
    }

    public ArrayList<String> getCategoryIDs() {
        return categoryIDs;
    }

    public void setCategoryIDs(ArrayList<String> categoryIDs) {
        this.categoryIDs = categoryIDs;
    }

    public ArrayList<String> getCityIDs() {
        return cityIDs;
    }

    public void setCityIDs(ArrayList<String> cityIDs) {
        this.cityIDs = cityIDs;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.minPriceId);
        dest.writeString(this.maxPriceId);
        dest.writeStringList(this.categoryIDs);
        dest.writeStringList(this.cityIDs);
    }

    public Preference() {
    }

    protected Preference(Parcel in) {
        this.minPriceId = in.readString();
        this.maxPriceId = in.readString();
        this.categoryIDs = in.createStringArrayList();
        this.cityIDs = in.createStringArrayList();
    }

    public static final Parcelable.Creator<Preference> CREATOR = new Parcelable.Creator<Preference>() {
        @Override
        public Preference createFromParcel(Parcel source) {
            return new Preference(source);
        }

        @Override
        public Preference[] newArray(int size) {
            return new Preference[size];
        }
    };
}
