package com.mandiri.lelang.aplikasi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.fragment.DetailAuctionScheduleFragment;
import com.sparkworks.mandiri.lelang.model.AuctionSchedule;
import com.sparkworks.mandiri.lelang.utils.ApiHelper.LelangType;

public class DetilJadwalLelangActivity extends BaseActivity{
	public static String KEY_JADWAL_LELANG = "JadwalLelang";
	public AuctionSchedule jadwalLelangItem;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detil_lelang_item);
		
		getActionBar().setDisplayUseLogoEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.logo_home);
        getActionBar().setTitle("");
        getActionBar().setHomeButtonEnabled(true);
		
		if (savedInstanceState == null) {
			jadwalLelangItem = (AuctionSchedule)getIntent().getSerializableExtra(KEY_JADWAL_LELANG);
		}else{
			jadwalLelangItem = (AuctionSchedule)savedInstanceState.getSerializable(KEY_JADWAL_LELANG);
		}
		
		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		
		if (savedInstanceState==null) {
			
			DetailAuctionScheduleFragment detailAuctionScheduleFragment = new DetailAuctionScheduleFragment();
			detailAuctionScheduleFragment.setLelangType(LelangType.ALL);
			detailAuctionScheduleFragment.setAcAuctionSchedule(jadwalLelangItem);
			
			FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
			fragmentTransaction.replace(R.id.fr_detil_item_container, detailAuctionScheduleFragment, DetailAuctionScheduleFragment.FRAGMENT_TAG);
			fragmentTransaction.commit();
		}
	}
	
	public static void toDetilJadwalLelang(Activity activity, AuctionSchedule auctionSchedule){
		Intent intent = new Intent(activity, DetilJadwalLelangActivity.class);
		intent.putExtra(KEY_JADWAL_LELANG, auctionSchedule);
		activity.startActivityForResult(intent, 0);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		outState.putSerializable(KEY_JADWAL_LELANG, jadwalLelangItem);
		super.onSaveInstanceState(outState);
	}
}
