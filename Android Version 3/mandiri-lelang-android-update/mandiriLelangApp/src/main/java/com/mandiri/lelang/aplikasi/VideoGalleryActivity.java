package com.mandiri.lelang.aplikasi;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.sparkworks.mandiri.lelang.adapter.VideoAdapter;
import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.model.Video;
import com.sparkworks.mandiri.lelang.model.response.VideoResponse;
import com.sparkworks.mandiri.lelang.utils.URLs;
import com.sparkworks.mandiri.lelang.utils.Utils;

import org.apache.http.Header;

import java.util.ArrayList;

public class VideoGalleryActivity extends BaseActivity {
    private GridView gvVideo;
    private LinearLayout lnErrorHandler;
    private ProgressBar progressBar;
    private TextView tvErrorMessage;
    private Button btnRetry;

    private VideoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_gallery);

        initializeLibs();
        initializeViews();
        initializeRequest();
        initializeActions();
    }

    @Override
    public void initializeLibs() {
        super.initializeLibs();
        adapter = new VideoAdapter(this);
        adapter.setVideos(new ArrayList<Video>());
    }

    @Override
    public void initializeViews() {
        super.initializeViews();

        getActionBar().setDisplayUseLogoEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.logo_home);
        getActionBar().setTitle("");
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        gvVideo = (GridView)findViewById(R.id.gv_video);
        progressBar = (ProgressBar)findViewById(R.id.progressbar);
        lnErrorHandler = (LinearLayout)findViewById(R.id.ln_error_handler);
        tvErrorMessage = (TextView)findViewById(R.id.tv_error_message);
        btnRetry = (Button)findViewById(R.id.btn_retry);

        gvVideo.setAdapter(adapter);

    }

    @Override
    public void initializeRequest() {
        super.initializeRequest();

        if (Utils.isConnectInet(this)){
            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            asyncHttpClient.get(URLs.URL_VIDEO, new AsyncHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    super.onSuccess(statusCode, headers, responseBody);
                    progressBar.setVisibility(View.GONE);

                    Gson gson = new Gson();
                    VideoResponse videoResponse = gson.fromJson(new String(responseBody), VideoResponse.class);

                    adapter.getVideos().addAll(videoResponse.getVideos());
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    super.onFailure(statusCode, headers, responseBody, error);
                    showError(getString(R.string.error_unable_connect_to_server));
                }
            });
        }else{
            showError(getString(R.string.no_internet));
        }
    }

    @Override
    public void initializeActions() {
        super.initializeActions();
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                lnErrorHandler.setVisibility(View.GONE);
                initializeRequest();
            }
        });

        gvVideo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                VideoPlayerActivity.start(VideoGalleryActivity.this,
                        adapter.getVideos().get(i));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showError(String message) {
        lnErrorHandler.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        tvErrorMessage.setText(message);
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, VideoGalleryActivity.class);
        context.startActivity(starter);
    }
}
