package com.mandiri.lelang.aplikasi;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;

import com.astuetz.PagerSlidingTabStrip;
import com.sparkworks.mandiri.lelang.adapter.RegisterLoginFragmentPagerAdapter;
import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.event.ClearErrorInViewsEvent;
import com.sparkworks.mandiri.lelang.fragment.RegisterFragment;

import org.greenrobot.eventbus.EventBus;

public class RegisterLoginActivity extends BaseActivity {
    private ViewPager pager;
    private RegisterLoginFragmentPagerAdapter adapter;
    private PagerSlidingTabStrip tabs;

    private String source = null;
    private boolean isUpdate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_login);

        source = getIntent().getStringExtra(RegisterFragment.EXTRA_SOURCE);

        getActionBar().setDisplayUseLogoEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.logo_home);
        getActionBar().setTitle("");
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        tabs = (PagerSlidingTabStrip)findViewById(R.id.tabs);
        pager = (ViewPager)findViewById(R.id.pager);
        adapter = new RegisterLoginFragmentPagerAdapter(getSupportFragmentManager());
        adapter.setProfile(false);
        if (source != null){
            adapter.setSource(source);
        }

        pager.setAdapter(adapter);
        tabs.setViewPager(pager);

        if (source.equalsIgnoreCase(RegisterFragment.SOURCE_REGISTER)){
            pager.setCurrentItem(1);
        }

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0){
                    EventBus.getDefault().post(new ClearErrorInViewsEvent());
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, RegisterLoginActivity.class);
        context.startActivity(starter);
    }

    public static void start(Context context, String source) {
        Intent starter = new Intent(context, RegisterLoginActivity.class);
        starter.putExtra(RegisterFragment.EXTRA_SOURCE, source);
        context.startActivity(starter);
    }

    public static void startOver(Context context, String source) {
        Intent starter = new Intent(context, RegisterLoginActivity.class);
        starter.putExtra(RegisterFragment.EXTRA_SOURCE, source);
        starter.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(starter);
    }
}
