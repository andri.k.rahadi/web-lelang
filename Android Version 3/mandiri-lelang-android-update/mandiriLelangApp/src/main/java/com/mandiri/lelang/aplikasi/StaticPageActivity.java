package com.mandiri.lelang.aplikasi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.utils.URLs;

public class StaticPageActivity extends BaseActivity{
	private WebView wbContent;
	private ProgressBar indicator;
	private String[] titles = new String[]{
		"Syarat dan Ketentuan", 
		"FAQ", 
		"Tentang Kami"
	};
	
	private String[] urls = new String[]{
		URLs.BASE_IP+"/content/terms?mobile=1",
		URLs.BASE_IP+"/content/faq?mobile=1",
		URLs.BASE_IP+"/content/about?mobile=1"
	};
	
	public enum PAGE_TYPE{
		SYARAT, FAQ, TENTANG
	};
	
	private PAGE_TYPE type;
	
	public static String KEY_TYPE = "pageTypeKey";
	
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.activity_static_page);
		
		getActionBar().setDisplayUseLogoEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.logo_home);
        getActionBar().setTitle("");
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
		
		type = (PAGE_TYPE)getIntent().getSerializableExtra(KEY_TYPE);
		
		initializeViews();
		initializeProcess();
	}
	
	@Override
	public void initializeViews() {
		// TODO Auto-generated method stub
		super.initializeViews();
		
		wbContent = (WebView)findViewById(R.id.wb_content);
		indicator = (ProgressBar)findViewById(R.id.indicator);
	}
	
	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		super.initializeProcess();
		
		String url = "", title = "";
		
		if (type == PAGE_TYPE.SYARAT) {
			url = urls[0];
			title = titles[0];
		}else if (type == PAGE_TYPE.FAQ) {
			url = urls[1];
			title = titles[1];
		}else if (type == PAGE_TYPE.TENTANG) {
			url = urls[2];
			title = titles[2];
		}
		
		//getActionBar().setTitle(title);
		wbContent.setWebChromeClient(new WebChromeClient(){
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				// TODO Auto-generated method stub
				super.onProgressChanged(view, newProgress);
				
				if(newProgress < 100 && indicator.getVisibility() == ProgressBar.GONE){
					indicator.setVisibility(ProgressBar.VISIBLE);
                }
				indicator.setProgress(newProgress);
                if(newProgress == 100) {
                	indicator.setVisibility(ProgressBar.GONE);
                }
			}
		});
		wbContent.setWebViewClient(new WebViewClient()       
        {
             @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) 
            {
                return false;
            }
             
            @Override
            public void onReceivedSslError(WebView view,
            		SslErrorHandler handler, SslError error) {
            	// TODO Auto-generated method stub
            	if ( SslError.SSL_UNTRUSTED == error.getPrimaryError() ){
    	            handler.proceed();
    	      } else {
    	            super.onReceivedSslError(view, handler, error);
    	      }
            }
        });

		wbContent.loadUrl(url.trim());
	}
	
	public static void toStaticPageActivity(Context context, PAGE_TYPE type){
		Intent intent = new Intent(context, StaticPageActivity.class);
		intent.putExtra(KEY_TYPE, type);
		context.startActivity(intent);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId()==android.R.id.home) {
			finish();
		}
		return true;
	}
}
