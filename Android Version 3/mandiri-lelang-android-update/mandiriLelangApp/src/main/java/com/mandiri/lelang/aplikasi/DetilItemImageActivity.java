package com.mandiri.lelang.aplikasi;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.sparkworks.mandiri.lelang.adapter.DetilItemImageFragmentAdapter;
import com.sparkworks.mandiri.lelang.base.BaseActivity;

public class DetilItemImageActivity extends BaseActivity{
	private ViewPager pager;
	private ArrayList<String> images;
	public static String KEY_IMAGES = "listOfImages";
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.activity_detil_image_pager);
//		
//		getActionBar().setHomeButtonEnabled(true);
//		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		if (arg0!=null) {
			images = arg0.getStringArrayList(KEY_IMAGES);
		} else {
			images = getIntent().getStringArrayListExtra(KEY_IMAGES);
		}
		
		initializeViews();
		initializeProcess();
	}
	
	@Override
	public void initializeViews() {
		// TODO Auto-generated method stub
		super.initializeViews();
		
		pager = (ViewPager)findViewById(R.id.pager);
	}
	
	@Override
	public void initializeProcess() {
		// TODO Auto-generated method stub
		super.initializeProcess();
		
		DetilItemImageFragmentAdapter detilItemImageFragmentAdapter = new DetilItemImageFragmentAdapter(getSupportFragmentManager());
		detilItemImageFragmentAdapter.setImages(images);
		pager.setAdapter(detilItemImageFragmentAdapter);
	}
	
	public static void toDetilItemImageActivity(Activity activity, ArrayList<String> images){
		Intent intent = new Intent(activity, DetilItemImageActivity.class);
		intent.putStringArrayListExtra(KEY_IMAGES, images);
		activity.startActivityForResult(intent, 10);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId()==android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putStringArrayList(KEY_IMAGES, images);
	}
}
