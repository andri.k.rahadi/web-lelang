package com.mandiri.lelang.aplikasi;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.model.Video;
import com.sparkworks.mandiri.lelang.utils.Constant;
import com.sparkworks.mandiri.lelang.utils.Utils;

public class VideoPlayerActivity extends YouTubeBaseActivity {

    private YouTubePlayerView youTubePlayerView;
    public static final String KEY_VIDEO = "video";
    private Video video;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        getActionBar().hide();

        video = getIntent().getParcelableExtra(KEY_VIDEO);

        youTubePlayerView = (YouTubePlayerView)findViewById(R.id.player);
        youTubePlayerView.initialize(Constant.YOUTUBE_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.cueVideo(video.getVideoId());
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Toast.makeText(VideoPlayerActivity.this, "Tidak dapat memainkan video saat ini. Silakan coba kembali", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    public static void start(Context context, Video video) {
        Intent starter = new Intent(context, VideoPlayerActivity.class);
        starter.putExtra(KEY_VIDEO, video);
        context.startActivity(starter);
    }
}
