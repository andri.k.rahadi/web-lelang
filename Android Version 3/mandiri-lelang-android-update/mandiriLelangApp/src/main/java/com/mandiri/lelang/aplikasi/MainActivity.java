package com.mandiri.lelang.aplikasi;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mandiri.lelang.aplikasi.StaticPageActivity.PAGE_TYPE;
import com.sparkworks.mandiri.lelang.adapter.NavDrawerListAdapter;
import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.fragment.AuctionScheduleFragment;
import com.sparkworks.mandiri.lelang.fragment.ContactUsFragment;
import com.sparkworks.mandiri.lelang.fragment.FavoriteFragment;
import com.sparkworks.mandiri.lelang.fragment.HomeFragment;
import com.sparkworks.mandiri.lelang.fragment.RegisterFragment;
import com.sparkworks.mandiri.lelang.model.NavDrawerItem;
import com.sparkworks.mandiri.lelang.request.api.LogoutRequest;
import com.sparkworks.mandiri.lelang.request.response.LogoutResponse;
import com.sparkworks.mandiri.lelang.utils.UserPreference;
import com.sparkworks.mandiri.lelang.utils.Utils;

public class MainActivity extends BaseActivity
	implements LogoutRequest.OnLogoutRequestListener,
		NavDrawerListAdapter.OnSubmenuClickedCallback{
	public DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	public ActionBarDrawerToggle mDrawerToggle;
	private TextView tvUsername;
	private LinearLayout lnProfile;

	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	private View header;
	private UserPreference userPreference;
	private LogoutRequest logoutRequest;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mTitle = mDrawerTitle = getTitle();

		userPreference = new UserPreference(this);

		logoutRequest = new LogoutRequest();
		logoutRequest.setContext(this);
		logoutRequest.setOnLogoutRequestListener(this);

		getActionBar().setDisplayUseLogoEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.logo_home);
        getActionBar().setTitle("");
        getActionBar().setHomeButtonEnabled(true);
		
		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		addHeaderOnTopMenuList();

		navDrawerItems = new ArrayList<NavDrawerItem>();
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], R.drawable.ic_home));

		if (TextUtils.isEmpty(userPreference.getToken())){
			navDrawerItems.add(new NavDrawerItem("Daftar/Masuk", R.drawable.ic_exit_to_app_black_36dp));
		}else{
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], R.drawable.ic_grade_black));
		}
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], R.drawable.ic_jadwal_lelang));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], R.drawable.ic_jadwal_lelang));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], R.drawable.ic_hubungi));
		if (!TextUtils.isEmpty(userPreference.getToken())){
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], R.drawable.ic_lock_black));
		}
		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		adapter.setCallback(this);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, //nav menu toggle icon
				R.string.app_name, // nav drawer open - description for accessibility
				R.string.app_name // nav drawer close - description for accessibility
		) {
			public void onDrawerClosed(View view) {
				//getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				//getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			//displayView(1);
			HomeFragment fragment = new HomeFragment();
			String fragmentTag = HomeFragment.FRAGMENT_TAG;

			int position;
			if (!TextUtils.isEmpty(userPreference.getToken())){
				position = 0;
			}else{
				position = 1;
			}

			setFragmentPage(fragment, fragmentTag, position);
		}
	}

	private void addHeaderOnTopMenuList() {
		if (!TextUtils.isEmpty(userPreference.getFullname())){
			header = getLayoutInflater().inflate(R.layout.header_navdrawer, null);
			tvUsername = (TextView)header.findViewById(R.id.tv_username);
			tvUsername.setText(userPreference.getFullname());
			lnProfile = (LinearLayout)header.findViewById(R.id.ln_profile);
			lnProfile.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					ProfileActivity.start(MainActivity.this, RegisterFragment.SOURCE_HOME);
				}
			});
			mDrawerList.addHeaderView(header);
		}
	}

	@Override
	public void onLogoutSuccess(LogoutResponse logoutResponse) {
		cancelProgressDialog();
		userPreference.clear();
		startActivity(new Intent(MainActivity.this, LaunchscreenActivity.class));
		finish();
	}

	@Override
	public void onLogoutFailed(String errorMessage, boolean shouldLogin) {
		cancelProgressDialog();
		if (shouldLogin){
			userPreference.clear();
			startActivity(new Intent(MainActivity.this, LaunchscreenActivity.class));
			finish();
		}else{
			Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onSyaratKetentuanClicked() {
		StaticPageActivity.toStaticPageActivity(this, StaticPageActivity.PAGE_TYPE.SYARAT);
	}

	@Override
	public void onFaqClicked() {
		StaticPageActivity.toStaticPageActivity(this, StaticPageActivity.PAGE_TYPE.FAQ);
	}

	@Override
	public void onTentangKamiClicked() {
		StaticPageActivity.toStaticPageActivity(this, StaticPageActivity.PAGE_TYPE.TENTANG);
	}

	@Override
	public void onVideoGalleryClicked() {
		VideoGalleryActivity.start(this);
	}

	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);
		}
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		//getMenuInflater().inflate(R.menu.main, menu);
//		return true;
//	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action bar actions click
		switch (item.getItemId()) {
		case R.id.action_settings:
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
//		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	private void displayView(int position) {
		Log.d("MenuPosition", position+"");
		// update the main content by replacing fragments
		Fragment fragment = null;
		String fragmentTag = null;

		if (!TextUtils.isEmpty(userPreference.getToken())){
			position -= 1;
		}

		if (navMenuTitles[position].equalsIgnoreCase("Home")){
			fragment = new HomeFragment();
			fragmentTag = HomeFragment.FRAGMENT_TAG;

		}else if(navMenuTitles[position].equalsIgnoreCase("Favorite")){
			if (TextUtils.isEmpty(userPreference.getToken())){
				RegisterLoginActivity.start(this, RegisterFragment.SOURCE_REGISTER);
			}else{
				fragment = new FavoriteFragment();
				fragmentTag = FavoriteFragment.class.getSimpleName();
			}
		}else if(navMenuTitles[position].equalsIgnoreCase("Hubungi Kami")){
			fragment = new ContactUsFragment();
			fragmentTag = ContactUsFragment.FRAGMENT_TAG;

		}else if(navMenuTitles[position].equalsIgnoreCase("Logout")){
			showLogoutAlertDialog();
		}else if(navMenuTitles[position].equalsIgnoreCase("Informasi Lelang")){
			if (adapter != null){
				if (adapter.isSubMenuShown()){
					adapter.setSubMenuShown(false);
					adapter.notifyDataSetChanged();
				}else{
					adapter.setSubMenuShown(true);
					adapter.notifyDataSetChanged();
				}
			}
		}else if (navMenuTitles[position].equalsIgnoreCase("Jadwal Lelang")){
			fragment = new AuctionScheduleFragment();
			fragmentTag = AuctionScheduleFragment.FRAGMENT_TAG;

		}

		setFragmentPage(fragment, fragmentTag, position);
	}

	private void setFragmentPage(Fragment fragment, String fragmentTag, int position) {
		if (fragment != null) {

			clearBackStack();

			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.frame_container, fragment, fragmentTag).commit();

			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
			//setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
	
	public static void toMainActivity(Activity activity){
		Intent intent = new Intent(activity, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		activity.startActivity(intent);
	}
	
	private void clearBackStack() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
             manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }
	
	@Override
	protected void attachBaseContext(Context newBase) {
		// TODO Auto-generated method stub
		super.attachBaseContext(new CalligraphyContextWrapper(newBase));
	}

	private void showLogoutAlertDialog(){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setTitle("Logout");
		alertDialogBuilder
				.setMessage("Apakah anda yakin ingin logout?")
				.setCancelable(false)
				.setPositiveButton("Ya",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						dialog.cancel();
						showProgressDialog("Logout", getString(R.string.general_progress_dialog_message));
						logoutRequest.callApi();
					}
				})
				.setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
}
