package com.mandiri.lelang.aplikasi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.view.WindowManager;

import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.fragment.FilterFragment;
import com.sparkworks.mandiri.lelang.fragment.FilterFragment.FilterType;

public class FilterActivity extends BaseActivity{
	
	public FilterType type;
	public String category, location;
	
	public static String KEY_TYPE = "Type";
	public static String KEY_CATEGORY = "Category";
	public static String KEY_LOCATION = "Location";
	
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.activity_detil_lelang_item);
		
		getActionBar().setTitle("Filter");
		
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		Intent intent = getIntent();
		type = (FilterType)intent.getSerializableExtra(KEY_TYPE);
		category = intent.getStringExtra(KEY_CATEGORY);
		location = intent.getStringExtra(KEY_LOCATION);
		
		if (arg0 == null) {
			FilterFragment filterFragment = new FilterFragment();
			filterFragment.setLocation(location);
			filterFragment.setType(type);
			filterFragment.setLocation(location);
			
			FragmentManager manager = getSupportFragmentManager();
			FragmentTransaction transaction = manager.beginTransaction();
			transaction.add(R.id.fr_detil_item_container, filterFragment, FilterFragment.FRAGMENT_TAG);
			transaction.commit();
		}
	}
	
	public static void toFilterFragmentActivity(Activity activity, FilterType type){
		Intent intent = new Intent(activity, FilterActivity.class);
		intent.putExtra(KEY_TYPE, type);
		activity.startActivityForResult(intent, 0);
	}
	
	public static void toFilterFragmentActivity(Activity activity, FilterType type, 
			String category, String location){
		Intent intent = new Intent(activity, FilterActivity.class);
		intent.putExtra(KEY_CATEGORY, category);
		intent.putExtra(KEY_LOCATION, location);
		activity.startActivity(intent);
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId()==android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}
