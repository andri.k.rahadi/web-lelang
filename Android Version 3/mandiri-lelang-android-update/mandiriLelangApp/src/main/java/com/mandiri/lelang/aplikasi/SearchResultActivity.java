package com.mandiri.lelang.aplikasi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;

import com.sparkworks.mandiri.lelang.fragment.SearchResultFragment;
import com.sparkworks.mandiri.lelang.model.Params;
import com.sparkworks.mandiri.lelang.utils.ApiHelper.LelangType;
import com.sparkworks.mandiri.lelang.utils.URLs;

public class SearchResultActivity extends MainActivity{
	public static String SEARCH_PARAM_KEY = "searchParam";
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		
		mDrawerToggle.setDrawerIndicatorEnabled(false);
		
		getActionBar().setTitle("Pencarian");
		
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		if (arg0 == null) {
			Params params = (Params)getIntent().getSerializableExtra(SEARCH_PARAM_KEY);
			
			SearchResultFragment searchResultFragment = new SearchResultFragment();
			searchResultFragment.setUrl(URLs.GET_ALL_ASSET);
			searchResultFragment.setLelangType(LelangType.ALL);
			searchResultFragment.setParams(params);

			FragmentManager manager = getSupportFragmentManager();
			FragmentTransaction transaction = manager.beginTransaction();
			transaction.replace(R.id.frame_container, searchResultFragment, SearchResultFragment.FRAGMENT_TAG);
			transaction.commit();
		}
	}
	
	public static void toSearchResultActivity(Activity activity, Params params){
		Intent intent = new Intent(activity, SearchResultActivity.class);
		intent.putExtra(SEARCH_PARAM_KEY, params);
		activity.startActivityForResult(intent, 10);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId()==android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
	
}
