package com.mandiri.lelang.aplikasi;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;

import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.fragment.RegisterFragment;
import com.sparkworks.mandiri.lelang.request.model.LoginData;

public class ProfileActivity extends BaseActivity {
    public static final String EXTRA_SOURCE = "extra_source";
    public static final String EXTRA_LOGIN_DATA = "extra_login_data";
    public static final String EXTRA_IS_FACEBOOK_LOGIN = "extra_is_facebook_login";
    private String source;
    private LoginData loginData;
    private boolean isFacebookLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getActionBar().setDisplayUseLogoEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.logo_home);
        getActionBar().setTitle("");
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        source = getIntent().getStringExtra(EXTRA_SOURCE);
        loginData = getIntent().getParcelableExtra(EXTRA_LOGIN_DATA);
        isFacebookLogin = getIntent().getBooleanExtra(EXTRA_IS_FACEBOOK_LOGIN, false);

        if (savedInstanceState == null){
            Bundle b = new Bundle();
            b.putString(EXTRA_SOURCE, source);
            b.putBoolean(EXTRA_IS_FACEBOOK_LOGIN, isFacebookLogin);

            if (loginData != null){
                b.putParcelable(EXTRA_LOGIN_DATA, loginData);
            }

            RegisterFragment r = new RegisterFragment();
            r.setArguments(b);

            getSupportFragmentManager().beginTransaction().replace(R.id.activity_profile,
                    r, r.getClass().getSimpleName()).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public static void start(Context context, String source, LoginData loginData) {
        Intent starter = new Intent(context, ProfileActivity.class);
        starter.putExtra(EXTRA_LOGIN_DATA, loginData);
        starter.putExtra(EXTRA_SOURCE, source);
        context.startActivity(starter);
    }

    public static void start(Context context, String source, LoginData loginData, boolean isFacebookLogin) {
        Intent starter = new Intent(context, ProfileActivity.class);
        starter.putExtra(EXTRA_LOGIN_DATA, loginData);
        starter.putExtra(EXTRA_SOURCE, source);
        starter.putExtra(EXTRA_IS_FACEBOOK_LOGIN, isFacebookLogin);
        context.startActivity(starter);
    }

    public static void start(Context context, String source) {
        Intent starter = new Intent(context, ProfileActivity.class);
        starter.putExtra(EXTRA_SOURCE, source);
        context.startActivity(starter);
    }
}
