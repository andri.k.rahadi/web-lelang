package com.mandiri.lelang.aplikasi;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.fragment.ContactUsFragment;
import com.sparkworks.mandiri.lelang.model.AssetInquiry;

public class ContactUsActivity extends BaseActivity {
    public static final String KEY_ASSET_INQUIRY = "asset_inquiry";
    private AssetInquiry assetInquiry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        assetInquiry = getIntent().getParcelableExtra(KEY_ASSET_INQUIRY);

        getActionBar().setDisplayUseLogoEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.logo_home);
        getActionBar().setTitle("");
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null){
            Bundle bundle = new Bundle();
            bundle.putString(ContactUsFragment.KEY_KODE_AGUNAN, assetInquiry.getKodeAsset());
            bundle.putString(ContactUsFragment.KEY_NAMA_PENGELOLA, assetInquiry.getNamaPengelola());

            ContactUsFragment contactUsFragment = new ContactUsFragment();
            contactUsFragment.setArguments(bundle);

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame_container, contactUsFragment, ContactUsFragment.class.getSimpleName())
                    .commitNow();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public static void start(Context context, AssetInquiry mAssetInquiry) {
        Intent starter = new Intent(context, ContactUsActivity.class);
        starter.putExtra(KEY_ASSET_INQUIRY, mAssetInquiry);
        context.startActivity(starter);
    }
}
