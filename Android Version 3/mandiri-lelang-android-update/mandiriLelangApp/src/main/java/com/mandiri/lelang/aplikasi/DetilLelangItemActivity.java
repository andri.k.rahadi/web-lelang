package com.mandiri.lelang.aplikasi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.fragment.DetilLelangItemFragment;
import com.sparkworks.mandiri.lelang.model.Item;
import com.sparkworks.mandiri.lelang.utils.ApiHelper;

public class DetilLelangItemActivity extends BaseActivity{
	public static String DETIL_ITEM_KEY = "DetilLelangItem";
	public static String ITEM_KEY = "item";
	public static String LELANG_TYPE_KEY = "DelangTypeKey";
	public static String POSITION_KEY = "PositionKey";

	Item item;
	private ApiHelper.LelangType lelangType;
	private int position = 0;
	
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.activity_detil_lelang_item);
		//getActionBar().setTitle("Detil Item");
		
		if (arg0 == null) {
			item = (Item)getIntent().getSerializableExtra(DETIL_ITEM_KEY);
			lelangType = (ApiHelper.LelangType)getIntent().getSerializableExtra(LELANG_TYPE_KEY);
			position = getIntent().getIntExtra(POSITION_KEY, 0);
		} else {
			item = (Item)arg0.getSerializable(ITEM_KEY);
			lelangType = (ApiHelper.LelangType)arg0.getSerializable(LELANG_TYPE_KEY);
			position = arg0.getInt(POSITION_KEY);
		}
		
		// enabling action bar app icon and behaving it as toggle button
//		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		
		if (arg0 == null) {

			Bundle bundle = new Bundle();
			bundle.putSerializable(LELANG_TYPE_KEY, lelangType);
			bundle.putInt(POSITION_KEY, position);

			DetilLelangItemFragment detilLelangItemFragment = new DetilLelangItemFragment();
			detilLelangItemFragment.setItem(item);
			detilLelangItemFragment.setArguments(bundle);
			
			FragmentManager manager = getSupportFragmentManager();
			FragmentTransaction transaction = manager.beginTransaction();
			transaction.replace(R.id.fr_detil_item_container, detilLelangItemFragment, DetilLelangItemFragment.FRAGMENT_TAG);
			transaction.commit();
		}
	}
	
	public static void toDetilLelangActivity(Activity activity, Item item){
		Intent intent = new Intent(activity, DetilLelangItemActivity.class);
		intent.putExtra(DETIL_ITEM_KEY, item);
		activity.startActivityForResult(intent, 0);
	}

	public static void toDetilLelangActivity(Activity activity, Item item, int mPosition, ApiHelper.LelangType mLelangType){
		Intent intent = new Intent(activity, DetilLelangItemActivity.class);
		intent.putExtra(DETIL_ITEM_KEY, item);
		intent.putExtra(LELANG_TYPE_KEY, mLelangType);
		intent.putExtra(POSITION_KEY, mPosition);
		activity.startActivityForResult(intent, 0);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		outState.putSerializable(ITEM_KEY, item);
		outState.putSerializable(LELANG_TYPE_KEY, lelangType);
		outState.putInt(POSITION_KEY, position);
		super.onSaveInstanceState(outState);
	}
}
