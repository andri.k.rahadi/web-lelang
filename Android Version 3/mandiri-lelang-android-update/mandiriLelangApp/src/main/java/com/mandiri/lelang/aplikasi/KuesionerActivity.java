package com.mandiri.lelang.aplikasi;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sparkworks.mandiri.lelang.adapter.MinMaxAdapter;
import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.database.helper.CategoryHelper;
import com.sparkworks.mandiri.lelang.database.helper.CityHelper;
import com.sparkworks.mandiri.lelang.database.helper.MinMaxPriceHelper;
import com.sparkworks.mandiri.lelang.database.helper.ProvinceHelper;
import com.sparkworks.mandiri.lelang.database.helper.SystemSettingHelper;
import com.sparkworks.mandiri.lelang.database.helper.WebContentHelper;
import com.sparkworks.mandiri.lelang.event.SetFavoriteUnfavoriteEvent;
import com.sparkworks.mandiri.lelang.fragment.RegisterFragment;
import com.sparkworks.mandiri.lelang.model.CityItem;
import com.sparkworks.mandiri.lelang.model.CriteriaItem;
import com.sparkworks.mandiri.lelang.model.MasterData;
import com.sparkworks.mandiri.lelang.model.ProvinceItem;
import com.sparkworks.mandiri.lelang.model.User;
import com.sparkworks.mandiri.lelang.request.api.PostRegisterUpdateRequest;
import com.sparkworks.mandiri.lelang.request.model.MemberParam;
import com.sparkworks.mandiri.lelang.request.model.Preference;
import com.sparkworks.mandiri.lelang.request.model.Profile;
import com.sparkworks.mandiri.lelang.request.response.ProfileResponse;
import com.sparkworks.mandiri.lelang.request.response.RegisterUpdateResponse;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;

public class KuesionerActivity extends BaseActivity
        implements View.OnClickListener,
        PostRegisterUpdateRequest.OnPostRegisterUpdateListener,
        AdapterView.OnItemSelectedListener, CompoundButton.OnCheckedChangeListener{
    private Spinner spnMin, spnMax;
    private Button btnSave, btnProvince, btnCity, btnCriteria;
    private TextView tvQuesionerToc;
    private EditText edtProvince, edtCity, edtKriteria;
    private CheckBox cbEmailInfo;

    private WebContentHelper webContentHelper = null;
    private CityHelper cityHelper = null;
    private CategoryHelper categoryHelper = null;
    private MinMaxPriceHelper minMaxPriceHelper;
    private SystemSettingHelper systemSettingHelper = null;
    private ProvinceHelper provinceHelper = null;

    public static String EXTRA_USER = "extra_user";
    public static String EXTRA_IS_UPDATE = "extra_is_update";
    public static String EXTRA_SOURCE = "extra_source";
    public static String EXTRA_PREFERENCE_DATA = "extra_preference_data";

    private User user = null;
    private boolean isUpdate = false, isPriceInUpdate = false;
    private boolean isAllProvinceChecked = false;
    private boolean isAllCityChecked = false;
    private boolean isAllCriteriaChecked = false;

    private String source = null;
    private Preference preferenceParam = null;

    private ArrayList<ProvinceItem> listSelectedProvince;
    private ArrayList<CriteriaItem> listSelectedCriteria;
    private ArrayList<CityItem> listSelectedCity;
    private ArrayList<MasterData> listMinPrice, listMaxPrice;
    private ArrayList<String> ranges;

    private PostRegisterUpdateRequest postRegisterUpdateRequest;
    private MinMaxAdapter minPriceAdapter, maxPriceAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kuesioner);

        getActionBar().setDisplayUseLogoEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.logo_home);
        getActionBar().setTitle("");
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        user = getIntent().getParcelableExtra(EXTRA_USER);
        isUpdate = getIntent().getBooleanExtra(EXTRA_IS_UPDATE, false);
        source = getIntent().getStringExtra(EXTRA_SOURCE);

        minMaxPriceHelper = new MinMaxPriceHelper(KuesionerActivity.this);
        minMaxPriceHelper.open();

        postRegisterUpdateRequest = new PostRegisterUpdateRequest();
        postRegisterUpdateRequest.setContext(this);
        postRegisterUpdateRequest.setOnPostRegisterUpdateListener(this);

        spnMax = (Spinner)findViewById(R.id.spn_max);
        spnMin = (Spinner)findViewById(R.id.spn_min);
        btnSave = (Button)findViewById(R.id.btn_save);
        btnProvince = (Button)findViewById(R.id.btn_add_province);
        btnCity = (Button)findViewById(R.id.btn_add_city);
        btnCriteria = (Button)findViewById(R.id.btn_add_criteria);
        tvQuesionerToc = (TextView)findViewById(R.id.tv_quesioner_toc);
        edtProvince = (EditText)findViewById(R.id.edt_province);
        edtKriteria = (EditText)findViewById(R.id.edt_selected_criteria);
        edtCity = (EditText)findViewById(R.id.edt_city);
        cbEmailInfo = (CheckBox)findViewById(R.id.cb_email);
        cbEmailInfo.setOnCheckedChangeListener(this);
        cbEmailInfo.setChecked(userPreference.isCheckToc());

        btnSave.setOnClickListener(this);
        btnProvince.setOnClickListener(this);
        btnCity.setOnClickListener(this);
        btnCriteria.setOnClickListener(this);

        webContentHelper = new WebContentHelper(KuesionerActivity.this);
        webContentHelper.open();

        categoryHelper = new CategoryHelper(this);
        categoryHelper.open();

        cityHelper = new CityHelper(this);
        cityHelper.open();

        systemSettingHelper = new SystemSettingHelper(this);
        systemSettingHelper.open();

        provinceHelper = new ProvinceHelper(this);
        provinceHelper.open();

        listSelectedProvince = new ArrayList<>();
        listSelectedCity = new ArrayList<>();
        listSelectedCriteria = new ArrayList<>();
        listMinPrice = new ArrayList<>();
        listMaxPrice = new ArrayList<>();

        ArrayList<MasterData> list = new ArrayList<>();
        list.addAll(webContentHelper.queryDataById("10"));

        if (list.size() > 0){
            tvQuesionerToc.setText(list.get(0).getName());
        }

        minPriceAdapter = new MinMaxAdapter(this);
        minPriceAdapter.setListPrice(listMinPrice);
        spnMin.setAdapter(minPriceAdapter);
        spnMin.setOnItemSelectedListener(this);

        listMinPrice.addAll(minMaxPriceHelper.loadAllData());
        minPriceAdapter.setListPrice(listMinPrice);
        minPriceAdapter.notifyDataSetChanged();

        maxPriceAdapter = new MinMaxAdapter(this);
        maxPriceAdapter.setListPrice(listMaxPrice);
        spnMax.setAdapter(maxPriceAdapter);
        spnMax.setOnItemSelectedListener(this);

        /*ranges = new ArrayList<>();
        for (MasterData data : listMinMaxPrice){
            ranges.add(data.getName());
        }
        */

        /*spnMin.setAdapter(new ArrayAdapter<String>(
                this,
                android.R.layout.simple_dropdown_item_1line,
                android.R.id.text1,
                ranges
        ));*/
        spnMin.setSelection(0, true);

        /*spnMax.setAdapter(new ArrayAdapter<String>(
                this,
                android.R.layout.simple_dropdown_item_1line,
                android.R.id.text1,
                ranges
        ));
        spnMax.setSelection((ranges.size()-1), true);*/

        if (source.equalsIgnoreCase(RegisterFragment.SOURCE_HOME) ||
                source.equalsIgnoreCase(RegisterFragment.SOURCE_LOGIN)){
            preferenceParam = getIntent().getParcelableExtra(EXTRA_PREFERENCE_DATA);
            if (preferenceParam != null){
                isPriceInUpdate = true;
                ArrayList<String> listCriteria = preferenceParam.getCategoryIDs();
                ArrayList<String> listCity = preferenceParam.getCityIDs();
                int minPriceId = Integer.parseInt(preferenceParam.getMinPriceId());

                ArrayList<MasterData> listMasterDataCity = cityHelper.loadAllData();
                ArrayList<MasterData> listMasterDataCategory = categoryHelper.loadAllData();
                ArrayList<MasterData> listMaxMin = minMaxPriceHelper.loadAllData();
                ArrayList<MasterData> listProvince = provinceHelper.loadAllData();

                for (int i = 0; i < listCriteria.size(); i++){
                    for (int j = 0; j < listMasterDataCategory.size(); j++){
                        if (listCriteria.get(i).equalsIgnoreCase(listMasterDataCategory.get(j).getId())){
                            CriteriaItem item = new CriteriaItem();
                            item.setId(listMasterDataCategory.get(i).getId());
                            item.setName(listMasterDataCategory.get(j).getName());
                            item.setChecked(true);

                            listSelectedCriteria.add(item);
                        }
                    }
                }

                setUpCriteriaOnEditText(listSelectedCriteria);

                ArrayList<MasterData> listAllProvince = provinceHelper.loadAllData();
                ArrayList<MasterData> listAllCity = cityHelper.loadAllData();

                //add province
                if (listAllCity.size() == listCity.size()){
                    isAllProvinceChecked = true;
                    for (MasterData m : listAllProvince){
                        ProvinceItem pi = new ProvinceItem();
                        pi.setId(m.getId());
                        pi.setName(m.getName());
                        pi.setChecked(true);

                        listSelectedProvince.add(pi);
                    }
                }else{
                    for (String s : listCity){
                        ArrayList<MasterData> city = cityHelper.loadCityById(Integer.parseInt(s));
                        if (city != null){
                            if (city.size() > 0){
                                ArrayList<MasterData> provinces = provinceHelper.loadProvinceById(Integer.parseInt(city.get(0).getProvinceId()));
                                if (provinces != null){
                                    if (provinces.size() > 0){
                                        ProvinceItem provinceItem = new ProvinceItem();
                                        provinceItem.setId(provinces.get(0).getId());
                                        provinceItem.setName(provinces.get(0).getName());
                                        provinceItem.setChecked(true);

                                        listSelectedProvince.add(provinceItem);
                                    }
                                }
                            }
                        }
                    }

                    ArrayList<String> provinces = new ArrayList<>();
                    for (int i = 0; i < listSelectedProvince.size(); i++){
                        provinces.add(listSelectedProvince.get(i).getName());
                    }

                    LinkedHashSet<String> ls = new LinkedHashSet<>();
                    ls.addAll(provinces);
                    provinces.clear();
                    provinces.addAll(ls);

                    listSelectedProvince.clear();

                    for (String s : provinces){
                        ArrayList<MasterData> prov = provinceHelper.loadProvinceByName(s);
                        if (prov != null){
                            if (prov.size() > 0){
                                ProvinceItem provinceItem = new ProvinceItem();
                                provinceItem.setId(prov.get(0).getId());
                                provinceItem.setName(prov.get(0).getName());
                                provinceItem.setChecked(true);

                                listSelectedProvince.add(provinceItem);
                            }
                        }
                    }

                }

                if (listSelectedProvince.size() > 0){
                    setUpProvinceOnEditText(listSelectedProvince);
                }

                int tempCityCount = 0;
                if (listAllCity.size() == listCity.size()){
                    isAllCityChecked = true;
                }else{
                    for (int i = 0; i < listSelectedProvince.size(); i++) {
                        tempCityCount +=  cityHelper.loadAllCityByProvince(listSelectedProvince.get(i).getId()).size();
                    }

                    if (tempCityCount == listCity.size()){
                        isAllCityChecked = true;
                    }
                }

                for (int i = 0; i < listCity.size(); i++){
                    for (int j = 0; j < listMasterDataCity.size(); j++){
                        if (listCity.get(i).equalsIgnoreCase(listMasterDataCity.get(j).getId())){
                            CityItem item = new CityItem();
                            item.setId(listMasterDataCity.get(j).getId());
                            item.setName(listMasterDataCity.get(j).getName());
                            item.setChecked(true);

                            listSelectedCity.add(item);
                        }
                    }
                }

                setUpCityOnEditText(listSelectedCity);

                int minPos = 0;
                for (int i = 0; i < listMaxMin.size(); i++){
                    if (minPriceId == Integer.parseInt(listMaxMin.get(i).getId())){
                        minPos = i;
                        break;
                    }
                }

                spnMin.setSelection(minPos, true);

                btnSave.setText("Simpan");
                btnCity.setText("Ubah Kota");
                btnProvince.setText("Ubah Provinsi");
            }
        }

    }

    public static void start(Context context) {
        Intent starter = new Intent(context, KuesionerActivity.class);
        context.startActivity(starter);
    }

    public static void start(Context context, User user, boolean isUpdate, String source) {
        Intent starter = new Intent(context, KuesionerActivity.class);
        starter.putExtra(EXTRA_USER, user);
        starter.putExtra(EXTRA_IS_UPDATE, isUpdate);
        starter.putExtra(EXTRA_SOURCE, source);
        context.startActivity(starter);
    }

    public static void start(Context context, User user, boolean isUpdate, String source, Preference preferenceParam) {
        Intent starter = new Intent(context, KuesionerActivity.class);
        starter.putExtra(EXTRA_USER, user);
        starter.putExtra(EXTRA_IS_UPDATE, isUpdate);
        starter.putExtra(EXTRA_SOURCE, source);
        starter.putExtra(EXTRA_PREFERENCE_DATA, preferenceParam);
        context.startActivity(starter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_save:
                    if (cbEmailInfo.isChecked()){
                        postData();
                    }else{
                        Toast.makeText(KuesionerActivity.this, "Anda harus menyetujui syarat dan ketentuan", Toast.LENGTH_SHORT).show();
                    }
                break;

            case R.id.btn_add_province:
                if (listSelectedProvince.size() > 0){
                    ProvinceActivity.start(this, isAllProvinceChecked, listSelectedProvince);
                }else{
                    ProvinceActivity.start(this, false, null);
                }
                break;

            case R.id.btn_add_criteria:
                if (listSelectedCriteria != null){
                    CriteriaActivity.start(this, isAllCriteriaChecked, listSelectedCriteria);
                }else{
                    CriteriaActivity.start(this, isAllCriteriaChecked, null);
                }
                break;

            case R.id.btn_add_city:
                if (listSelectedProvince.size() > 0 && listSelectedCity.size() > 0){
                    CityActivity.start(this, isAllCityChecked, listSelectedProvince, listSelectedCity);
                }else{
                    CityActivity.start(this, isAllCityChecked, listSelectedProvince, null);
                }
                break;
        }
    }

    private void postData() {
        ArrayList<String> categories = new ArrayList<>();
        for (CriteriaItem criteriaItem : listSelectedCriteria){
            categories.add(criteriaItem.getId());
        }

        ArrayList<String> cities = new ArrayList<>();
        for (CityItem cityItem : listSelectedCity){
            cities.add(cityItem.getId());
        }

        int minPriceId = Integer.parseInt(listMinPrice.get(spnMin.getSelectedItemPosition()).getId());
        int maxPriceId = Integer.parseInt(listMaxPrice.get(spnMax.getSelectedItemPosition()).getId());

        Preference preferenceParam = new Preference();
        preferenceParam.setCategoryIDs(categories);
        preferenceParam.setCityIDs(cities);
        preferenceParam.setMaxPriceId(String.valueOf(maxPriceId));
        preferenceParam.setMinPriceId(String.valueOf(minPriceId));

        Profile profileParam = Profile.getProfileParam(user);
        if (userPreference.getMemberId() != 0){
            profileParam.setMemberId(userPreference.getMemberId());
        }

        MemberParam memberParam = new MemberParam();
        memberParam.setProfileParam(profileParam);
        memberParam.setpreference(preferenceParam);

        postRegisterUpdateRequest.setMemberParam(memberParam);

        if (isUpdate){
            postRegisterUpdateRequest.setPostType(PostRegisterUpdateRequest.POST_UPDATE);
        }else{
            postRegisterUpdateRequest.setPostType(PostRegisterUpdateRequest.POST_REGISTER);
        }

        postRegisterUpdateRequest.validatePreferenceParam();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ProvinceActivity.REQUEST_CODE){
            if (resultCode == ProvinceActivity.RESULT_CODE){
                edtProvince.setError(null);
                if (listSelectedCity.size() > 0){
                    listSelectedCity.clear();
                    edtCity.setText("");
                }

                if (listSelectedProvince.size() > 0){
                    listSelectedProvince.clear();
                }

                isAllProvinceChecked = data.getBooleanExtra(ProvinceActivity.EXTRA_IS_ALL_CHECKED, false);
                ArrayList<ProvinceItem> list = data.getParcelableArrayListExtra(ProvinceActivity.EXTRA_SELECTED_PROVINCES);
                listSelectedProvince.addAll(list);

                isAllCityChecked = false;

                setUpProvinceOnEditText(list);
            }
        }

        if (requestCode == CriteriaActivity.REQUEST_CODE){
            if (resultCode == CriteriaActivity.RESULT_CODE){
                edtKriteria.setError(null);
                if (listSelectedCriteria.size() > 0){
                    listSelectedCriteria.clear();
                }
                ArrayList<CriteriaItem> list = data.getParcelableArrayListExtra(CriteriaActivity.EXTRA_SELECTED_CRITERIA);
                isAllCriteriaChecked = data.getBooleanExtra(CriteriaActivity.EXTRA_IS_ALL_CHECKED, false);
                listSelectedCriteria.addAll(list);

                setUpCriteriaOnEditText(list);
            }
        }

        if (requestCode == CityActivity.REQUEST_CODE){
            if (resultCode == CityActivity.RESULT_CODE){
                edtCity.setError(null);
                if (listSelectedCity.size() > 0){
                    listSelectedCity.clear();
                    edtCity.setText("");
                }

                ArrayList<CityItem> list = data.getParcelableArrayListExtra(CityActivity.EXTRA_SELECTED_CITY);
                isAllCityChecked = data.getBooleanExtra(CityActivity.EXTRA_IS_ALL_CITY_CHECKED, false);

                listSelectedCity.addAll(list);

                setUpCityOnEditText(list);
            }
        }
    }

    private void setUpProvinceOnEditText(ArrayList<ProvinceItem> list) {
        String provinces = "";
        for (int i = 0; i < list.size(); i++){
            if (i == 0){
                provinces += list.get(i).getName() +",";
            }else{
                if (i % 2 == 0){
                    provinces += "," + list.get(i).getName();
                }else{
                    provinces += list.get(i).getName();
                }
            }
        }

        edtProvince.setText(provinces);
    }

    private void setUpCityOnEditText(ArrayList<CityItem> list) {
        String city = "";
        if (list.size() == 1){
            city += list.get(0).getName();
        }else{
            for (int i = 0; i < list.size(); i++){
                if (i == 0){
                    city += list.get(i).getName() +",";
                }else{
                    if (i % 2 == 0){
                        city += "," + list.get(i).getName();
                    }else{
                        city += list.get(i).getName();
                    }
                }
            }
        }

        edtCity.setText(city);
    }

    private void setUpCriteriaOnEditText(ArrayList<CriteriaItem> list) {
        String criteria = "";
        if (list.size() == 1){
            criteria += list.get(0).getName();
        }else{
            for (int i = 0; i < list.size(); i++){
                if (i == 0){
                    criteria += list.get(i).getName() +",";
                }else{
                    if (i % 2 == 0){
                        criteria += "," + list.get(i).getName();
                    }else{
                        criteria += list.get(i).getName();
                    }
                }
            }
        }

        edtKriteria.setText(criteria);
    }

    @Override
    protected void onDestroy() {
        if (minMaxPriceHelper != null){
            minMaxPriceHelper.close();
        }

        if (cityHelper != null){
            cityHelper.close();
        }

        if (categoryHelper != null){
            categoryHelper.close();
        }

        if (webContentHelper != null){
            webContentHelper.close();
        }

        if (systemSettingHelper != null){
            systemSettingHelper.close();
        }

        if (provinceHelper != null){
            provinceHelper.close();
        }

        super.onDestroy();
    }

    @Override
    public void onPostRegisterSuccess(RegisterUpdateResponse registerUpdateResponse) {
        cancelProgressDialog();

        userPreference.setToken(registerUpdateResponse.getLoginData().getToken());
        userPreference.setMemberId(registerUpdateResponse.getLoginData().getMemberId());
        userPreference.setFullname(registerUpdateResponse.getLoginData().getFullName());
        userPreference.setEmail(registerUpdateResponse.getLoginData().getEmail());

        showRegisterSuccessAlertDialog();
    }

    private void showRegisterSuccessAlertDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Register Berhasil");
        alertDialogBuilder
                .setMessage("Proses registrasi berhasil, silahkan cek email anda untuk melakukan aktivasi")
                .setCancelable(false)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                        RegisterLoginActivity.startOver(KuesionerActivity.this, RegisterFragment.SOURCE_REGISTER);
                        //MainActivity.toMainActivity(this);

                        finish();

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    @Override
    public void onPostUpdateSuccess(ProfileResponse profileResponse) {
        Toast.makeText(this, "Update Berhasil", Toast.LENGTH_SHORT).show();

        userPreference.setMemberId(profileResponse.getProfileData().getProfileParam().getMemberId());
        userPreference.setFullname(profileResponse.getProfileData().getProfileParam().getFullName());
        userPreference.setEmail(profileResponse.getProfileData().getProfileParam().getEmail());

        MainActivity.toMainActivity(this);
        finish();
    }

    @Override
    public void onPostRegisterUpdateFailed(String errorMessage) {
        cancelProgressDialog();
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();

        if (errorMessage.toUpperCase(Locale.ENGLISH).equalsIgnoreCase("TOKEN")){
            LaunchscreenActivity.start(this);
            finish();
        }
    }

    @Override
    public void onValidateMemberParamSuccess() {

    }

    @Override
    public void onValidatePreferenceSuccess() {
        String dialogTitle = null;
        if (isUpdate){
            dialogTitle = "Update";
        }else{
            dialogTitle = "Register";
        }

        showProgressDialog(dialogTitle, getString(R.string.general_progress_dialog_message));
        postRegisterUpdateRequest.callApi();
    }

    @Override
    public void onParamsInvalid(HashMap<String, String> invalidParams) {
        cancelProgressDialog();
        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_CATEGORY_IDS)) {
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_CATEGORY_IDS).equals(PostRegisterUpdateRequest.PARAM_CATEGORY_IDS)) {
                if (TextUtils.isEmpty(edtKriteria.getText().toString().trim())){
                    edtKriteria.setError(getString(R.string.error_field_empty));
                }
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_CITY_IDS)) {
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_CITY_IDS).equals(PostRegisterUpdateRequest.PARAM_CITY_IDS)) {
                if (TextUtils.isEmpty(edtCity.getText().toString().trim())){
                    edtCity.setError(getString(R.string.error_field_empty));
                }
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_MIN_PRICE_ID)) {
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_MIN_PRICE_ID).equals(PostRegisterUpdateRequest.PARAM_MIN_PRICE_ID)) {
                if (spnMin.getSelectedItemPosition() == 0){
                    Toast.makeText(this, "Minimum price tidak boleh kosong", Toast.LENGTH_SHORT).show();
                }
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_MAX_PRICE_ID)) {
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_MAX_PRICE_ID).equals(PostRegisterUpdateRequest.PARAM_MAX_PRICE_ID)) {
                if (spnMax.getSelectedItemPosition() == 0){
                    Toast.makeText(this, "Maximum price tidak boleh kosong", Toast.LENGTH_SHORT).show();
                }
            }
        }

        if (invalidParams.containsKey(PostRegisterUpdateRequest.ERROR_MIN_PRICE_OVER_MAX_PRICE)) {
            if (invalidParams.get(PostRegisterUpdateRequest.ERROR_MIN_PRICE_OVER_MAX_PRICE).equals(PostRegisterUpdateRequest.PARAM_MIN_PRICE_OVER_MAX_PRICE)) {
                Toast.makeText(this, "Harga minimal tidak boleh lebih besar dibanding harga maksimal yang dipilih", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()){
            case R.id.spn_min:
                if (listMaxPrice.size() > 0){
                    listMaxPrice.clear();
                }
                listMaxPrice.addAll(minMaxPriceHelper.loadAllDataByRange(true, Long.parseLong(listMinPrice.get(spnMin.getSelectedItemPosition()).getValue())));
                maxPriceAdapter.setListPrice(listMaxPrice);
                maxPriceAdapter.notifyDataSetChanged();

                break;

            case R.id.spn_max:
                if (isPriceInUpdate){
                    int maxPos = 0;
                    int maxPriceId = Integer.parseInt(preferenceParam.getMaxPriceId());
                    for (int j = 0; j < listMaxPrice.size(); j++){
                        if (maxPriceId == Integer.parseInt(listMaxPrice.get(j).getId())){
                            maxPos = j;
                            break;
                        }
                    }

                    spnMax.setSelection(maxPos, true);
                    isPriceInUpdate = false;
                }

                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        userPreference.setCheckToc(compoundButton.isChecked());
    }
}
