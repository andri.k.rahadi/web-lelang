package com.mandiri.lelang.aplikasi;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.request.api.ResetPasswordRequest;
import com.sparkworks.mandiri.lelang.request.response.ResetPasswordResponse;

import java.util.HashMap;

public class ForgotPasswordActivity extends BaseActivity
    implements ResetPasswordRequest.OnResetPasswordRequestListener,
        View.OnClickListener{
    private EditText edtEmail;
    private Button btnResetPassword;

    private ResetPasswordRequest resetPasswordRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        getActionBar().setDisplayUseLogoEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.logo_home);
        getActionBar().setTitle("");
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        edtEmail = (EditText)findViewById(R.id.edt_email);
        btnResetPassword = (Button)findViewById(R.id.btn_reset_password);
        btnResetPassword.setOnClickListener(this);

        resetPasswordRequest = new ResetPasswordRequest();
        resetPasswordRequest.setContext(this);
        resetPasswordRequest.setOnResetPasswordRequestListener(this);
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, ForgotPasswordActivity.class);
        context.startActivity(starter);
    }

    @Override
    public void onResetPasswordSuccess(ResetPasswordResponse resetPasswordResponse) {
        cancelProgressDialog();
        String message = resetPasswordResponse.getResultCode().getMessage()+". Silakan cek email anda";
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResetPasswordFailed(String errorMessage) {
        cancelProgressDialog();
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onParamsInvalid(HashMap<String, String> invalidParams) {
        cancelProgressDialog();
        if (invalidParams.containsKey(ResetPasswordRequest.PARAM_EMAIL)){
            if (invalidParams.get(ResetPasswordRequest.PARAM_EMAIL).equalsIgnoreCase(ResetPasswordRequest.ERROR_EMAIL)){
                edtEmail.setError(getString(R.string.error_email_empty));
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_reset_password){
            showProgressDialog("Lupa Password", getString(R.string.general_progress_dialog_message));
            String email = edtEmail.getText().toString().trim();
            resetPasswordRequest.setEmail(email);
            resetPasswordRequest.callApi();
        }
    }
}
