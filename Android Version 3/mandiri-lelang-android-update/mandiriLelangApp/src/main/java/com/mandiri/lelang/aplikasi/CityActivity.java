package com.mandiri.lelang.aplikasi;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.sparkworks.mandiri.lelang.adapter.CityAdapter;
import com.sparkworks.mandiri.lelang.adapter.CriteriaAdapter;
import com.sparkworks.mandiri.lelang.adapter.KpknlAdapter;
import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.database.helper.CategoryHelper;
import com.sparkworks.mandiri.lelang.database.helper.CityHelper;
import com.sparkworks.mandiri.lelang.database.helper.ProvinceHelper;
import com.sparkworks.mandiri.lelang.model.CityItem;
import com.sparkworks.mandiri.lelang.model.CriteriaItem;
import com.sparkworks.mandiri.lelang.model.MasterData;
import com.sparkworks.mandiri.lelang.model.ProvinceItem;

import java.util.ArrayList;

public class CityActivity extends BaseActivity implements View.OnClickListener,
        KpknlAdapter.OnCheckAllCallback, CompoundButton.OnCheckedChangeListener{
    private ListView lvCity;
    private Button btnSelect;
    private CheckBox cbAll;
    private LinearLayout lnAll;
    private ProgressBar progressBar;

    private CityHelper cityHelper;
    private ProvinceHelper provinceHelper;
    private boolean isAllCityChecked = false;

    private CityAdapter adapter;
    private ArrayList<CityItem> selectedCityList, listCity;
    private ArrayList<ProvinceItem> listSelectedProvince;

    public static final String EXTRA_SELECTED_PROVINCE = "extra_selected_province";
    public static final String EXTRA_SELECTED_CITY = "extra_selected_city";
    public static final String EXTRA_IS_ALL_CITY_CHECKED = "extra_is_all_city_checked";

    public static final int REQUEST_CODE = 140;
    public static final int RESULT_CODE = 150;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        getActionBar().setDisplayUseLogoEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.logo_home);
        getActionBar().setTitle("");
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        lvCity = (ListView)findViewById(R.id.lv_province);
        btnSelect = (Button)findViewById(R.id.btn_select);
        btnSelect.setOnClickListener(this);
        cbAll = (CheckBox)findViewById(R.id.cb_item_all);
        cbAll.setOnCheckedChangeListener(this);
        lnAll = (LinearLayout)findViewById(R.id.ln_option_all);
        progressBar = (ProgressBar)findViewById(R.id.progressbar);

        listSelectedProvince = getIntent().getParcelableArrayListExtra(EXTRA_SELECTED_PROVINCE);
        isAllCityChecked = getIntent().getBooleanExtra(EXTRA_IS_ALL_CITY_CHECKED, false);
        ArrayList<CityItem> listCurrentCity = getIntent().getParcelableArrayListExtra(EXTRA_SELECTED_CITY);

        provinceHelper = new ProvinceHelper(CityActivity.this);
        provinceHelper.open();

        adapter = new CityAdapter(CityActivity.this);
        adapter.setOnCheckAllCallback(this);
        selectedCityList = new ArrayList<>();

        if (listCurrentCity != null){
            if (listCurrentCity.size() > 0){
                selectedCityList.addAll(listCurrentCity);
            }
        }

        listCity = new ArrayList<>();
        adapter.setListCity(listCity);
        lvCity.setAdapter(adapter);

        cityHelper = new CityHelper(this);
        cityHelper.open();

        new GetCityAsync().execute();

    }

    private class GetCityAsync extends AsyncTask<Void, Void, Void>{
        ArrayList<MasterData> cities = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            lnAll.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (int i = 0; i < listSelectedProvince.size(); i++){
                cities.addAll(cityHelper.loadAllCityByProvince(listSelectedProvince.get(i).getId()));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            lnAll.setVisibility(View.VISIBLE);
            lvCity.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            cbAll.setChecked(isAllCityChecked);

            for (MasterData data : cities){
                CityItem item = new CityItem();
                item.setId(data.getId());
                item.setName(data.getName());

                if (selectedCityList != null){
                    for (CityItem cityItem : selectedCityList){
                        if (data.getId().equalsIgnoreCase(cityItem.getId())){
                            item.setChecked(true);
                        }
                    }
                }else{
                    item.setChecked(false);
                }

                listCity.add(item);
            }

            adapter.setListCity(listCity);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_select){
            if (selectedCityList.size() > 0){
                selectedCityList.clear();
            }

            for (CityItem item : adapter.getListCity()){
                if (item.isChecked()){
                    selectedCityList.add(item);
                }
            }

            Intent intent = new Intent();
            intent.putExtra(EXTRA_SELECTED_CITY, selectedCityList);
            intent.putExtra(EXTRA_IS_ALL_CITY_CHECKED, isAllCityChecked);

            setResult(RESULT_CODE, intent);

            finish();
        }
    }

    public static void start(Activity activity, boolean isAllChecked, ArrayList<ProvinceItem> listProvince, ArrayList<CityItem> listCity) {
        Intent starter = new Intent(activity, CityActivity.class);
        starter.putExtra(EXTRA_SELECTED_PROVINCE, listProvince);
        starter.putExtra(EXTRA_IS_ALL_CITY_CHECKED, isAllChecked);
        starter.putExtra(EXTRA_SELECTED_CITY, listCity);
        activity.startActivityForResult(starter, REQUEST_CODE);
    }

    @Override
    protected void onDestroy() {
        if (cityHelper != null){
            cityHelper.close();
        }

        if (provinceHelper != null){
            provinceHelper.close();
        }
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (compoundButton.isChecked()){
            isAllCityChecked = true;
            for (CityItem item : listCity){
                item.setChecked(true);
            }
        }else{
            isAllCityChecked = false;
            for (CityItem item : listCity){
                item.setChecked(false);
            }
        }
        adapter.setListCity(listCity);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCheckAll() {
        if (cbAll.isChecked()){
            cbAll.setChecked(false);
        }
    }
}
