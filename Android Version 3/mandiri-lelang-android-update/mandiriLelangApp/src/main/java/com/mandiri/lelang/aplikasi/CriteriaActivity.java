package com.mandiri.lelang.aplikasi;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.sparkworks.mandiri.lelang.adapter.CriteriaAdapter;
import com.sparkworks.mandiri.lelang.adapter.KpknlAdapter;
import com.sparkworks.mandiri.lelang.adapter.ProvinceAdapter;
import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.database.helper.CategoryHelper;
import com.sparkworks.mandiri.lelang.database.helper.ProvinceHelper;
import com.sparkworks.mandiri.lelang.model.CriteriaItem;
import com.sparkworks.mandiri.lelang.model.MasterData;
import com.sparkworks.mandiri.lelang.model.ProvinceItem;

import java.util.ArrayList;

public class CriteriaActivity extends BaseActivity
        implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, KpknlAdapter.OnCheckAllCallback{
    private ListView lvCriteria;
    private Button btnSelect;
    private CheckBox cbAll;
    private LinearLayout lnAll;

    private CategoryHelper categoryHelper;
    private CriteriaAdapter adapter;
    private ArrayList<CriteriaItem> selectedCriteriaList, listCriteria, listCurrentCriteria;
    private boolean isAllChecked = false;

    public static final String EXTRA_SELECTED_CRITERIA = "extra_selected_criteria";
    public static final String EXTRA_CURRENT_CRITERIA = "extra_current_criteria";
    public static final String EXTRA_IS_ALL_CHECKED = "extra_is_all_checked";

    public static final int REQUEST_CODE = 120;
    public static final int RESULT_CODE = 130;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_criteria);

        getActionBar().setDisplayUseLogoEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.logo_home);
        getActionBar().setTitle("");
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        listCurrentCriteria = getIntent().getParcelableArrayListExtra(EXTRA_CURRENT_CRITERIA);
        isAllChecked = getIntent().getBooleanExtra(EXTRA_IS_ALL_CHECKED, false);

        lvCriteria = (ListView)findViewById(R.id.lv_province);
        btnSelect = (Button)findViewById(R.id.btn_select);
        btnSelect.setOnClickListener(this);
        cbAll = (CheckBox)findViewById(R.id.cb_item_all);
        cbAll.setOnCheckedChangeListener(this);
        lnAll = (LinearLayout)findViewById(R.id.ln_option_all);

        adapter = new CriteriaAdapter(CriteriaActivity.this);
        adapter.setOnCheckAllCallback(this);

        selectedCriteriaList = new ArrayList<>();
        listCriteria = new ArrayList<>();
        adapter.setListCriteria(listCriteria);
        lvCriteria.setAdapter(adapter);

        categoryHelper = new CategoryHelper(this);
        categoryHelper.open();

        ArrayList<MasterData> criterias = categoryHelper.loadAllData();
        for (MasterData data : criterias){
            CriteriaItem item = new CriteriaItem();
            item.setId(data.getId());
            item.setName(data.getName());

            if (listCurrentCriteria != null){
                for (int i = 0; i < listCurrentCriteria.size(); i++){
                    if (data.getId().equalsIgnoreCase(listCurrentCriteria.get(i).getId())){
                        item.setChecked(true);
                    }
                }
            }else{
                item.setChecked(false);
            }

            listCriteria.add(item);
        }

        adapter.setListCriteria(listCriteria);
        adapter.notifyDataSetChanged();

        cbAll.setChecked(isAllChecked);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_select){
            for (CriteriaItem item : adapter.getListCriteria()){
                if (item.isChecked()){
                    selectedCriteriaList.add(item);
                }
            }

            Intent intent = new Intent();
            intent.putExtra(EXTRA_SELECTED_CRITERIA, selectedCriteriaList);
            intent.putExtra(EXTRA_IS_ALL_CHECKED, isAllChecked);

            setResult(RESULT_CODE, intent);

            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public static void start(Activity activity, boolean isAllChecked, ArrayList<CriteriaItem> listCriteria) {
        Intent starter = new Intent(activity, CriteriaActivity.class);
        starter.putExtra(EXTRA_CURRENT_CRITERIA, listCriteria);
        starter.putExtra(EXTRA_IS_ALL_CHECKED, isAllChecked);
        activity.startActivityForResult(starter, REQUEST_CODE);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (compoundButton.isChecked()){
            isAllChecked = true;
            for (CriteriaItem item : listCriteria){
                item.setChecked(true);
            }
        }else{
            isAllChecked = false;
            for (CriteriaItem item : listCriteria){
                item.setChecked(false);
            }
        }
        adapter.setListCriteria(listCriteria);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCheckAll() {
        if (cbAll.isChecked()){
            cbAll.setChecked(false);
        }
    }
}
