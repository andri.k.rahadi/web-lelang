package com.mandiri.lelang.aplikasi;

import java.util.List;
import java.util.Locale;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.database.helper.BranchHelper;
import com.sparkworks.mandiri.lelang.database.helper.CategoryHelper;
import com.sparkworks.mandiri.lelang.database.helper.CityHelper;
import com.sparkworks.mandiri.lelang.database.helper.ContactUsHelper;
import com.sparkworks.mandiri.lelang.database.helper.DocumentTypeHelper;
import com.sparkworks.mandiri.lelang.database.helper.KpknlHelper;
import com.sparkworks.mandiri.lelang.database.helper.MinMaxPriceHelper;
import com.sparkworks.mandiri.lelang.database.helper.ProvinceHelper;
import com.sparkworks.mandiri.lelang.database.helper.SystemSettingHelper;
import com.sparkworks.mandiri.lelang.database.helper.WebContentHelper;
import com.sparkworks.mandiri.lelang.fragment.RegisterFragment;
import com.sparkworks.mandiri.lelang.utils.ApiHelper;
import com.sparkworks.mandiri.lelang.utils.AppPreference;
import com.sparkworks.mandiri.lelang.utils.Constant;
import com.sparkworks.mandiri.lelang.utils.URLs;

public class SplashscreenActivity extends BaseActivity{
    public static final String MASTER_DATA = "MASTER_DATA";
    private CategoryHelper categoryHelper = null;
	private ProvinceHelper provinceHelper = null;
	private CityHelper cityHelper = null;
	private MinMaxPriceHelper minMaxPriceHelper = null;
	private DocumentTypeHelper documentTypeHelper = null;
	private ContactUsHelper contactUsHelper = null;
	private BranchHelper branchHelper = null;
	private AppPreference appPreference = null;
	private SystemSettingHelper systemSettingHelper = null;
    private WebContentHelper webContentHelper = null;
	private KpknlHelper knplHelper = null;
	
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.activity_splashscreen);
		
		initializeLibs();
		
		getMasterData();
		//route();
		
	}
	
	@Override
	public void initializeLibs() {
		// TODO Auto-generated method stub
		super.initializeLibs();
		categoryHelper = new CategoryHelper(SplashscreenActivity.this);
		categoryHelper.open();
		provinceHelper = new ProvinceHelper(SplashscreenActivity.this);
		provinceHelper.open();
		cityHelper = new CityHelper(SplashscreenActivity.this);
		cityHelper.open();
		minMaxPriceHelper = new MinMaxPriceHelper(SplashscreenActivity.this);
		minMaxPriceHelper.open();
		documentTypeHelper = new DocumentTypeHelper(SplashscreenActivity.this);
		documentTypeHelper.open();
		contactUsHelper = new ContactUsHelper(SplashscreenActivity.this);
		contactUsHelper.open();
		branchHelper = new BranchHelper(SplashscreenActivity.this);
		branchHelper.open();
		systemSettingHelper = new SystemSettingHelper(SplashscreenActivity.this);
		systemSettingHelper.open();
        webContentHelper = new WebContentHelper(SplashscreenActivity.this);
        webContentHelper.open();
		knplHelper = new KpknlHelper(SplashscreenActivity.this);
		knplHelper.open();
		
		appPreference = new AppPreference(SplashscreenActivity.this);
	}
	
	private void getMasterData() {
		// TODO Auto-generated method stub
		//AsyncHttpClient asyncHttpClient = new AsyncHttpClient(true, 80, 443);
		AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
		asyncHttpClient.setTimeout(60000);
        List<NameValuePair> listParam = ApiHelper.getMasterDataParams("0");
		String request = ApiHelper.buildAPIURLMessage(URLs.SERVER_MASTER_DATA, null, listParam);
		asyncHttpClient.get(request, new AsyncHttpResponseHandler(){
			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				// TODO Auto-generated method stub
				super.onSuccess(arg0, arg1, arg2);
				
				String response = new String(arg2);
				new InsertToLocalStorage(response).execute();
			}
			
			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				// TODO Auto-generated method stub
				super.onFailure(arg0, arg1, arg2, arg3);
				route();
			}
		});
		
	}

	protected void fetchResponse(String response) {
		// TODO Auto-generated method stub
		Log.d("master_response", response);
		try {
			JSONObject object = new JSONObject(response);
			JSONArray arrayMasterData = object.getJSONArray(Constant.MASTER_DATA);
			
			for (int i = 0; i < arrayMasterData.length(); i++) {
				JSONObject item = arrayMasterData.getJSONObject(i);
				if (item.getString(Constant.MASTER_DATA_TYPE).equals(Constant.MASTER_DATA_CATEGORY)) {
					JSONArray arrCategory = item.getJSONArray(Constant.MASTER_DATA_CONTENT);
					if (arrCategory.length()>0) {
						for (int i1 = 0; i1 < arrCategory.length(); i1++) {
							JSONObject objItemCategory = arrCategory.getJSONObject(i1);
							if (objItemCategory.getString(Constant.MASTER_DATA_FLAG).
									toLowerCase(Locale.ENGLISH).equals("add")) {
								if (!categoryHelper.isCategoryExist(objItemCategory.getString(Constant.MASTER_DATA_ID))) {
									categoryHelper.insert(objItemCategory.getString(Constant.MASTER_DATA_ID),
											objItemCategory.getString(Constant.MASTER_DATA_NAME), 
											objItemCategory.getString(Constant.MASTER_DATA_DESCRIPTION), 
											objItemCategory.getString(Constant.MASTER_DATA_ISACTIVE), 
											objItemCategory.getString(Constant.MASTER_DATA_FLAG));
								}
								Log.d(MASTER_DATA, "CategoryInserted");
							} else {
								categoryHelper.update(objItemCategory.getString(Constant.MASTER_DATA_ID),
										objItemCategory.getString(Constant.MASTER_DATA_NAME), 
										objItemCategory.getString(Constant.MASTER_DATA_DESCRIPTION), 
										objItemCategory.getString(Constant.MASTER_DATA_ISACTIVE), 
										objItemCategory.getString(Constant.MASTER_DATA_FLAG));
								Log.d(MASTER_DATA, "CategoryUpdate");
							}
						}
					}


				}
				
				if (item.getString(Constant.MASTER_DATA_TYPE).equals(Constant.MASTER_DATA_PROVINCE)) {
					JSONArray arrProvince = item.getJSONArray(Constant.MASTER_DATA_CONTENT);
					if (arrProvince.length()>0) {
						for (int i1 = 0; i1 < arrProvince.length(); i1++) {
							JSONObject objItemProvince = arrProvince.getJSONObject(i1);
							if (objItemProvince.getString(Constant.MASTER_DATA_FLAG).toLowerCase(Locale.ENGLISH).equals("add")) {
								if (!provinceHelper.isProvinceExist(objItemProvince.getString(Constant.MASTER_DATA_ID))) {
									provinceHelper.insert(objItemProvince.getString(Constant.MASTER_DATA_ID),
											objItemProvince.getString(Constant.MASTER_DATA_NAME), 
											objItemProvince.getString(Constant.MASTER_DATA_DESCRIPTION), 
											objItemProvince.getString(Constant.MASTER_DATA_ISACTIVE), 
											objItemProvince.getString(Constant.MASTER_DATA_FLAG));
								}
								Log.d(MASTER_DATA, "ProvinceInserted");
							} else {
								provinceHelper.update(objItemProvince.getString(Constant.MASTER_DATA_ID),
										objItemProvince.getString(Constant.MASTER_DATA_NAME), 
										objItemProvince.getString(Constant.MASTER_DATA_DESCRIPTION), 
										objItemProvince.getString(Constant.MASTER_DATA_ISACTIVE), 
										objItemProvince.getString(Constant.MASTER_DATA_FLAG));
								Log.d(MASTER_DATA, "ProvinceUpdate");
							}
						}
					}
				}
				
				if (item.getString(Constant.MASTER_DATA_TYPE).equals(Constant.MASTER_DATA_CITY)) {
					JSONArray arrCity = item.getJSONArray(Constant.MASTER_DATA_CONTENT);
					if (arrCity.length()>0) {
						for (int i1 = 0; i1 < arrCity.length(); i1++) {
							JSONObject objItemCity = arrCity.getJSONObject(i1);
							if (objItemCity.getString(Constant.MASTER_DATA_FLAG).toLowerCase(Locale.ENGLISH).equals("add")) {
								if (!cityHelper.isCityExist(objItemCity.getString(Constant.MASTER_DATA_ID))) {
									cityHelper.insert(objItemCity.getString(Constant.MASTER_DATA_ID),
											objItemCity.getString(Constant.MASTER_DATA_NAME), 
											objItemCity.getString(Constant.MASTER_DATA_DESCRIPTION), 
											objItemCity.getString(Constant.MASTER_DATA_ISACTIVE), 
											objItemCity.getString(Constant.MASTER_DATA_FLAG), 
											objItemCity.getString(Constant.MASTER_DATA_PROVINCE_ID));
								}
								Log.d(MASTER_DATA, "CityInserted");
							} else {
								cityHelper.update(objItemCity.getString(Constant.MASTER_DATA_ID),
										objItemCity.getString(Constant.MASTER_DATA_NAME), 
										objItemCity.getString(Constant.MASTER_DATA_DESCRIPTION), 
										objItemCity.getString(Constant.MASTER_DATA_ISACTIVE), 
										objItemCity.getString(Constant.MASTER_DATA_FLAG), 
										objItemCity.getString(Constant.MASTER_DATA_PROVINCE_ID));
								Log.d(MASTER_DATA, "CityUpdated");
							}
						}
					}
				}
				
				if (item.getString(Constant.MASTER_DATA_TYPE).equals(Constant.MASTER_DATA_MIN_MAX_PRICE)) {
					JSONArray arrMinMaxPrice = item.getJSONArray(Constant.MASTER_DATA_CONTENT);
					if (arrMinMaxPrice.length()>0) {
						for (int i1 = 0; i1 < arrMinMaxPrice.length(); i1++) {
							JSONObject objItemMinMaxPrice = arrMinMaxPrice.getJSONObject(i1);
							if (objItemMinMaxPrice.getString(Constant.MASTER_DATA_FLAG).toLowerCase(Locale.ENGLISH).equals("add")) {
								if (!minMaxPriceHelper.isMinMaxPriceExist(objItemMinMaxPrice.getString(Constant.MASTER_DATA_ID))) {
									minMaxPriceHelper.insert(objItemMinMaxPrice.getString(Constant.MASTER_DATA_ID),
											objItemMinMaxPrice.getString(Constant.MASTER_DATA_NAME), 
											objItemMinMaxPrice.getString(Constant.MASTER_DATA_DESCRIPTION), 
											objItemMinMaxPrice.getString(Constant.MASTER_DATA_ISACTIVE), 
											objItemMinMaxPrice.getString(Constant.MASTER_DATA_FLAG), 
											objItemMinMaxPrice.optString(Constant.MASTER_DATA_VALUE));
								}
								Log.d(MASTER_DATA, "PriceInserted");
							} else {
								minMaxPriceHelper.update(objItemMinMaxPrice.getString(Constant.MASTER_DATA_ID),
										objItemMinMaxPrice.getString(Constant.MASTER_DATA_NAME), 
										objItemMinMaxPrice.getString(Constant.MASTER_DATA_DESCRIPTION), 
										objItemMinMaxPrice.getString(Constant.MASTER_DATA_ISACTIVE), 
										objItemMinMaxPrice.getString(Constant.MASTER_DATA_FLAG), 
										objItemMinMaxPrice.optString(Constant.MASTER_DATA_VALUE));
								Log.d(MASTER_DATA, "PriceUpdated");
							}
						}
					}

				}
				
				if (item.getString(Constant.MASTER_DATA_TYPE).equals(Constant.MASTER_DATA_CONTACT_BRANCH)) {
					JSONArray arrBranch = item.getJSONArray(Constant.MASTER_DATA_CONTENT);
					if (arrBranch.length()>0) {
						for (int i1 = 0; i1 < arrBranch.length(); i1++) {
							JSONObject objItemBranch = arrBranch.getJSONObject(i1);
							if (objItemBranch.getString(Constant.MASTER_DATA_FLAG).toLowerCase(Locale.ENGLISH).equals("add")) {
								if (!branchHelper.isBranchExist(objItemBranch.getString(Constant.MASTER_DATA_ID))) {
									branchHelper.insert(objItemBranch.getString(Constant.MASTER_DATA_ID),
											objItemBranch.getString(Constant.MASTER_DATA_NAME), 
											objItemBranch.getString(Constant.MASTER_DATA_DESCRIPTION), 
											objItemBranch.getString(Constant.MASTER_DATA_ISACTIVE), 
											objItemBranch.getString(Constant.MASTER_DATA_FLAG));
								}
								Log.d(MASTER_DATA, "BranchInserted");
							} else {
								branchHelper.update(objItemBranch.getString(Constant.MASTER_DATA_ID),
										objItemBranch.getString(Constant.MASTER_DATA_NAME), 
										objItemBranch.getString(Constant.MASTER_DATA_DESCRIPTION), 
										objItemBranch.getString(Constant.MASTER_DATA_ISACTIVE), 
										objItemBranch.getString(Constant.MASTER_DATA_FLAG));
								Log.d(MASTER_DATA, "BranchUpdated");
							}
						}
					}

				}
				
				if (item.getString(Constant.MASTER_DATA_TYPE).equals(Constant.MASTER_DATA_CONTACT_US_TYPE)) {
					JSONArray arrContactUs = item.getJSONArray(Constant.MASTER_DATA_CONTENT);
					if (arrContactUs.length()>0) {
						for (int i1 = 0; i1 < arrContactUs.length(); i1++) {
							JSONObject objItemContactUs = arrContactUs.getJSONObject(i1);
							if (objItemContactUs.getString(Constant.MASTER_DATA_FLAG).toLowerCase(Locale.ENGLISH).equals("add")) {
								if (!contactUsHelper.isContactUsExist(objItemContactUs.getString(Constant.MASTER_DATA_ID))) {
									contactUsHelper.insert(objItemContactUs.getString(Constant.MASTER_DATA_ID),
											objItemContactUs.getString(Constant.MASTER_DATA_NAME), 
											objItemContactUs.getString(Constant.MASTER_DATA_DESCRIPTION), 
											objItemContactUs.getString(Constant.MASTER_DATA_ISACTIVE), 
											objItemContactUs.getString(Constant.MASTER_DATA_FLAG));
								}
								Log.d(MASTER_DATA, "ContactUsInserted");
							} else {
								contactUsHelper.update(objItemContactUs.getString(Constant.MASTER_DATA_ID),
										objItemContactUs.getString(Constant.MASTER_DATA_NAME), 
										objItemContactUs.getString(Constant.MASTER_DATA_DESCRIPTION), 
										objItemContactUs.getString(Constant.MASTER_DATA_ISACTIVE), 
										objItemContactUs.getString(Constant.MASTER_DATA_FLAG));
								Log.d(MASTER_DATA, "ContactUsUpdated");
							}	
						}
					}
				}
				
				if (item.getString(Constant.MASTER_DATA_TYPE).equals(Constant.MASTER_DATA_DOCUMENT_TYPE)) {
					JSONArray arrDocument = item.getJSONArray(Constant.MASTER_DATA_CONTENT);
					if (arrDocument.length()>0) {
						for (int i1 = 0; i1 < arrDocument.length(); i1++) {
							JSONObject objItemDocument = arrDocument.getJSONObject(i1);
							if (objItemDocument.getString(Constant.MASTER_DATA_FLAG).toLowerCase(Locale.ENGLISH).equals("add")) {
								if (!documentTypeHelper.isDocumentExist(objItemDocument.getString(Constant.MASTER_DATA_ID))) {
									documentTypeHelper.insert(objItemDocument.getString(Constant.MASTER_DATA_ID),
											objItemDocument.getString(Constant.MASTER_DATA_NAME), 
											objItemDocument.getString(Constant.MASTER_DATA_DESCRIPTION), 
											objItemDocument.getString(Constant.MASTER_DATA_ISACTIVE), 
											objItemDocument.getString(Constant.MASTER_DATA_FLAG));
								}
								Log.d(MASTER_DATA, "DocumentInserted");
							} else {
								documentTypeHelper.insert(objItemDocument.getString(Constant.MASTER_DATA_ID),
										objItemDocument.getString(Constant.MASTER_DATA_NAME), 
										objItemDocument.getString(Constant.MASTER_DATA_DESCRIPTION), 
										objItemDocument.getString(Constant.MASTER_DATA_ISACTIVE), 
										objItemDocument.getString(Constant.MASTER_DATA_FLAG));
								Log.d(MASTER_DATA, "DocumentUpdated");
							}
						}
					}

				}

				if (item.getString(Constant.MASTER_DATA_TYPE).equals(Constant.MASTER_DATA_SYSTEM_SETTINGS)) {
					JSONArray arrSystemSetting = item.getJSONArray(Constant.MASTER_DATA_CONTENT);
					if (arrSystemSetting.length()>0) {
						for (int i1 = 0; i1 < arrSystemSetting.length(); i1++) {
							JSONObject objItemSetting = arrSystemSetting.getJSONObject(i1);
							if (objItemSetting.getString(Constant.MASTER_DATA_FLAG).
									toLowerCase(Locale.ENGLISH).equals("add")) {
								if (!systemSettingHelper.isSystemSettingExist(objItemSetting.getString(Constant.MASTER_DATA_ID))) {
									systemSettingHelper.insert(objItemSetting.getString(Constant.MASTER_DATA_ID),
											objItemSetting.getString(Constant.MASTER_DATA_NAME),
											objItemSetting.getString(Constant.MASTER_DATA_DESCRIPTION),
											objItemSetting.getString(Constant.MASTER_DATA_ISACTIVE),
											objItemSetting.getString(Constant.MASTER_DATA_FLAG));
								}
								Log.d(MASTER_DATA, "SystemSettingInserted");
							} else {
								systemSettingHelper.update(objItemSetting.getString(Constant.MASTER_DATA_ID),
										objItemSetting.getString(Constant.MASTER_DATA_NAME),
										objItemSetting.getString(Constant.MASTER_DATA_DESCRIPTION),
										objItemSetting.getString(Constant.MASTER_DATA_ISACTIVE),
										objItemSetting.getString(Constant.MASTER_DATA_FLAG));
								Log.d(MASTER_DATA, "SystemSettingUpdated");
							}
						}
					}

				}

				if (item.getString(Constant.MASTER_DATA_TYPE).equals(Constant.MASTER_DATA_WEBCONTENT)) {
					JSONArray arrWebContent = item.getJSONArray(Constant.MASTER_DATA_CONTENT);
					if (arrWebContent.length()>0) {
						for (int i1 = 0; i1 < arrWebContent.length(); i1++) {
							JSONObject objItemContent = arrWebContent.getJSONObject(i1);
							if (objItemContent.getString(Constant.MASTER_DATA_FLAG).
									toLowerCase(Locale.ENGLISH).equals("add")) {
								if (!webContentHelper.isSystemSettingExist(objItemContent.getString(Constant.MASTER_DATA_ID))) {
                                    webContentHelper.insert(objItemContent.getString(Constant.MASTER_DATA_ID),
											objItemContent.getString(Constant.MASTER_DATA_NAME),
											objItemContent.getString(Constant.MASTER_DATA_DESCRIPTION),
											objItemContent.getString(Constant.MASTER_DATA_ISACTIVE),
											objItemContent.getString(Constant.MASTER_DATA_FLAG));
								}
								Log.d(MASTER_DATA, "WebContentInserted");
							} else {
                                webContentHelper.update(objItemContent.getString(Constant.MASTER_DATA_ID),
										objItemContent.getString(Constant.MASTER_DATA_NAME),
										objItemContent.getString(Constant.MASTER_DATA_DESCRIPTION),
										objItemContent.getString(Constant.MASTER_DATA_ISACTIVE),
										objItemContent.getString(Constant.MASTER_DATA_FLAG));
								Log.d(MASTER_DATA, "WebContentUpdated");
							}
						}
					}
				}

				if (item.getString(Constant.MASTER_DATA_TYPE).equals(Constant.MASTER_DATA_KPKNL)) {
					JSONArray arrKpknl = item.getJSONArray(Constant.MASTER_DATA_CONTENT);
					if (arrKpknl.length()>0) {
						for (int i1 = 0; i1 < arrKpknl.length(); i1++) {
							JSONObject objItemContent = arrKpknl.getJSONObject(i1);
							if (objItemContent.getString(Constant.MASTER_DATA_FLAG).
									toLowerCase(Locale.ENGLISH).equals("add")) {
								if (!knplHelper.isKNPLExist(objItemContent.getString(Constant.MASTER_DATA_ID))) {
									knplHelper.insert(objItemContent.getString(Constant.MASTER_DATA_ID),
											objItemContent.getString(Constant.MASTER_DATA_NAME),
											objItemContent.getString(Constant.MASTER_DATA_DESCRIPTION),
											objItemContent.getString(Constant.MASTER_DATA_ISACTIVE),
											objItemContent.getString(Constant.MASTER_DATA_FLAG));
								}
								Log.d(MASTER_DATA, "KpknlInserted");
							} else {
								knplHelper.update(objItemContent.getString(Constant.MASTER_DATA_ID),
										objItemContent.getString(Constant.MASTER_DATA_NAME),
										objItemContent.getString(Constant.MASTER_DATA_DESCRIPTION),
										objItemContent.getString(Constant.MASTER_DATA_ISACTIVE),
										objItemContent.getString(Constant.MASTER_DATA_FLAG));
								Log.d(MASTER_DATA, "KpknlUpdated");
							}
						}
					}

				}
			}
			
			String timestamp = object.optString(Constant.MASTER_DATA_LASTTIMESTAMP);
			appPreference.setTimestamp(timestamp);
			String token = object.optString(Constant.MASTER_DATA_TOKEN);
			int isTokenExpired = object.optInt(Constant.MASTER_DATA_IS_TOKEN_EXPIRED);
			JSONObject objResultCode = object.optJSONObject(Constant.MASTER_DATA_RESULT_CODE);
			if (objResultCode.optInt(Constant.MASTER_DATA_CODE) == 1 && isTokenExpired == 1){
				Toast.makeText(SplashscreenActivity.this, "Session sudah expired. Silakan login kembali", Toast.LENGTH_SHORT).show();
				userPreference.clear();
				LaunchscreenActivity.start(SplashscreenActivity.this);
				finish();
			}else{
				route();
			}
		} catch (Exception e) {
			// TODO: handle exception
			Log.d(Constant.APP_TAG, e.getMessage());
		}
	}

	class Delay extends AsyncTask<Void, Void, Void>{
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(5000);
			} catch (Exception e) {
				// TODO: handle exception
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
			route();
		}
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		if (categoryHelper!=null) {
			categoryHelper.close();
		} 
		
		if (provinceHelper != null) {
			provinceHelper.close();
		}
		
		if (cityHelper != null) {
			cityHelper.close();
		}
		
		if (minMaxPriceHelper != null) {
			minMaxPriceHelper.close();
		}
		
		if (documentTypeHelper != null) {
			documentTypeHelper.close();
		}
		
		if (contactUsHelper != null) {
			contactUsHelper.close();
		}
		
		if (branchHelper != null) {
			branchHelper.close();
		}

        if (webContentHelper != null) {
            webContentHelper.close();
        }

        if (systemSettingHelper != null) {
            systemSettingHelper.close();
        }

		if (knplHelper != null){
			knplHelper.close();
		}
	}

	public void route() {
		// TODO Auto-generated method stub
		if (TextUtils.isEmpty(userPreference.getFullname())){
			if (userPreference.isOpenLaunchscreen()){
				MainActivity.toMainActivity(this);
			}else{
				LaunchscreenActivity.start(SplashscreenActivity.this);
			}
		}else{
			MainActivity.toMainActivity(this);
		}
		finish();
	}
	
	class InsertToLocalStorage extends AsyncTask<Void, Void, Void>{
		
		String response = "";
		
		public InsertToLocalStorage(String response) {
			// TODO Auto-generated constructor stub
			this.response = response;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			fetchResponse(response);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		}
	}
}
