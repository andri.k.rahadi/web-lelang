package com.mandiri.lelang.aplikasi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.sparkworks.mandiri.lelang.adapter.KpknlAdapter;
import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.database.helper.KpknlHelper;
import com.sparkworks.mandiri.lelang.model.KpknlItem;
import com.sparkworks.mandiri.lelang.model.MasterData;

import java.util.ArrayList;

public class KpknlActivity extends BaseActivity implements View.OnClickListener,
        CompoundButton.OnCheckedChangeListener, KpknlAdapter.OnCheckAllCallback{
    private ListView lvKpknl;
    private Button btnSelect;
    private CheckBox cbAll;
    private LinearLayout lnAll;

    private KpknlHelper kpknlHelper;
    private KpknlAdapter adapter;
    private ArrayList<KpknlItem> selectedKpknlList, listKpknl, listCurrentKpknl;

    public static final String EXTRA_SELECTED_KPKNL = "extra_selected_kpkln";
    public static final String EXTRA_CURRENT_KPKNL = "extra_current_kpknl";

    public static final int REQUEST_CODE = 100;
    public static final int RESULT_CODE = 110;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kpknl);

        getActionBar().setDisplayUseLogoEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.logo_home);
        getActionBar().setTitle("");
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        listCurrentKpknl = getIntent().getParcelableArrayListExtra(EXTRA_CURRENT_KPKNL);

        lvKpknl = (ListView)findViewById(R.id.lv_province);
        btnSelect = (Button)findViewById(R.id.btn_select);
        btnSelect.setOnClickListener(this);
        cbAll = (CheckBox)findViewById(R.id.cb_item_all);
        cbAll.setOnCheckedChangeListener(this);
        lnAll = (LinearLayout)findViewById(R.id.ln_option_all);

        adapter = new KpknlAdapter(KpknlActivity.this);
        adapter.setOnCheckAllCallback(this);
        selectedKpknlList = new ArrayList<>();
        listKpknl = new ArrayList<>();
        adapter.setListKpknlItem(listKpknl);
        lvKpknl.setAdapter(adapter);

        kpknlHelper = new KpknlHelper(this);
        kpknlHelper.open();

        ArrayList<MasterData> kpknls = kpknlHelper.loadAllData();

        if (kpknls != null){
            if (kpknls.size() > 0){
                for (MasterData data : kpknls){
                    KpknlItem item = new KpknlItem();
                    item.setId(Integer.parseInt(data.getId()));
                    item.setName(data.getName());
                    if (listCurrentKpknl.size() > 0){
                        for (int i = 0; i < listCurrentKpknl.size(); i++){
                            if (Integer.parseInt(data.getId()) == listCurrentKpknl.get(i).getId()){
                                item.setChecked(true);
                            }
                        }
                    }else{
                        item.setChecked(false);
                    }
                    listKpknl.add(item);
                }

                adapter.setListKpknlItem(listKpknl);
                adapter.notifyDataSetChanged();
            }else{
                lnAll.setVisibility(View.GONE);
            }
        }else{
            lnAll.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_select){
            for (KpknlItem item : adapter.getListKpknlItem()){
                if (item.isChecked()){
                    selectedKpknlList.add(item);
                }
            }

            Intent intent = new Intent();
            intent.putExtra(EXTRA_SELECTED_KPKNL, selectedKpknlList);

            setResult(RESULT_CODE, intent);

            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public static void start(Activity activity, ArrayList<KpknlItem> kpknlItems) {
        Intent starter = new Intent(activity, KpknlActivity.class);
        starter.putExtra(EXTRA_CURRENT_KPKNL, kpknlItems);
        activity.startActivityForResult(starter, REQUEST_CODE);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (compoundButton.isChecked()){
            for (KpknlItem item : listKpknl){
                item.setChecked(b);
            }
        }
        adapter.setListKpknlItem(listKpknl);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onCheckAll() {
        if (cbAll.isChecked()){
            cbAll.setChecked(false);
        }
    }
}
