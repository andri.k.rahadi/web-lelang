package com.mandiri.lelang.aplikasi;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.database.helper.WebContentHelper;
import com.sparkworks.mandiri.lelang.fragment.RegisterFragment;
import com.sparkworks.mandiri.lelang.model.MasterData;

import java.util.ArrayList;

public class LaunchscreenActivity extends BaseActivity
    implements View.OnClickListener{
    private Button btnLoginRegister, btnSkip;
    private TextView tvLaunchscreenText;
    private WebContentHelper webContentHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launchscreen);

        btnLoginRegister = (Button)findViewById(R.id.btn_login_register);
        btnSkip = (Button)findViewById(R.id.btn_skip);

        tvLaunchscreenText = (TextView)findViewById(R.id.tv_launchscreen_text);

        btnSkip.setOnClickListener(this);
        btnLoginRegister.setOnClickListener(this);

        webContentHelper = new WebContentHelper(LaunchscreenActivity.this);
        webContentHelper.open();

        ArrayList<MasterData> list = new ArrayList<>();
        list.addAll(webContentHelper.queryDataById("9"));

        if (list.size() > 0){
            tvLaunchscreenText.setText(list.get(0).getName());
        }

    }

    public static void start(Context context) {
        Intent starter = new Intent(context, LaunchscreenActivity.class);
        context.startActivity(starter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_skip:
                userPreference.setOpenLaunchscreen(true);
                MainActivity.toMainActivity(this);
                finish();
                break;

            case R.id.btn_login_register:
                userPreference.setOpenLaunchscreen(true);
                RegisterLoginActivity.start(this, RegisterFragment.SOURCE_REGISTER);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (webContentHelper != null){
            webContentHelper.close();
        }
    }
}
