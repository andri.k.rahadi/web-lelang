package com.mandiri.lelang.aplikasi;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.database.helper.KpknlHelper;
import com.sparkworks.mandiri.lelang.model.AuctionScheduleParam;
import com.sparkworks.mandiri.lelang.model.CriteriaItem;
import com.sparkworks.mandiri.lelang.model.KpknlItem;
import com.sparkworks.mandiri.lelang.model.MasterData;

import java.util.ArrayList;
import java.util.Calendar;

public class FilterJadwalLelangActivity extends BaseActivity implements View.OnClickListener {
    private Button btnCari, btnStartDate, btnEndDate, btnKpnl, btnReset;
    private TextView tvStartDate, tvEndDate;
    private EditText edtKpknls;

    private int fromYear;
    private int fromMonth;
    private int fromDay;
    static final int DATE_DIALOG_FROM = 0;

    private int toYear;
    private int toMonth;
    private int toDay;

    private String fromDate = null, toDate = null;

    static final int DATE_DIALOG_TO = 1;

    public static final int REQUEST_CODE = 10;
    public static final int RESULT_CODE = 20;
    public static final String EXTRA_AUCTION_PARAM = "extra_auction_param";

    private ArrayList<KpknlItem> listSelectedKpknl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_jadwal_lelang);

        getActionBar().setDisplayUseLogoEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.logo_home);
        getActionBar().setTitle("");
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        listSelectedKpknl = new ArrayList<>();

        btnCari = (Button)findViewById(R.id.btn_filter_apply);
        btnEndDate = (Button)findViewById(R.id.btn_filter_select_end_date);
        btnStartDate = (Button)findViewById(R.id.btn_filter_select_start_date);
        btnKpnl = (Button)findViewById(R.id.btn_add_kpnl);
        btnReset = (Button)findViewById(R.id.btn_filter_reset);

        tvEndDate = (TextView)findViewById(R.id.txt_filter_end_date);
        tvStartDate = (TextView)findViewById(R.id.txt_filter_start_date);
        edtKpknls = (EditText)findViewById(R.id.edt_kpnl);

        btnCari.setOnClickListener(this);
        btnReset.setOnClickListener(this);
        btnKpnl.setOnClickListener(this);
        btnStartDate.setOnClickListener(this);
        btnEndDate.setOnClickListener(this);

        initStartDate();
        initEndDate();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_filter_apply:
                ArrayList<Integer> selectedKpknl = new ArrayList<>();
                if (!TextUtils.isEmpty(fromDate) && !TextUtils.isEmpty(toDate)){
                    if (listSelectedKpknl.size() > 0){
                        for (KpknlItem item : listSelectedKpknl){
                            selectedKpknl.add(item.getId());
                        }
                    }else{
                        KpknlHelper helper = new KpknlHelper(this);
                        for (MasterData data : helper.loadAllData()){
                            selectedKpknl.add(Integer.parseInt(data.getId()));
                        }
                    }

                    AuctionScheduleParam param = new AuctionScheduleParam();
                    param.setListKnpl(selectedKpknl);
                    param.setToDate(toDate);
                    param.setFromDate(fromDate);

                    Intent intent = new Intent();
                    intent.putExtra(EXTRA_AUCTION_PARAM, param);

                    setResult(RESULT_CODE, intent);

                    finish();
                }else{
                    Toast.makeText(this, "Parameter harus terisi", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.btn_filter_select_start_date:
                new DatePickerDialog(this,
                        mStartDateSetListener,
                        fromYear, fromMonth, fromDay).show();
                break;

            case R.id.btn_filter_select_end_date:
                new DatePickerDialog(this,
                        mEndDateSetListener,
                        toYear, toMonth, toDay).show();
                break;

            case R.id.btn_add_kpnl:
                KpknlActivity.start(this, listSelectedKpknl);
                break;

            case R.id.btn_filter_reset:
                edtKpknls.setText("");
                listSelectedKpknl.clear();
                tvStartDate.setText("");
                tvEndDate.setText("");
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public static void start(Activity activity) {
        Intent starter = new Intent(activity, FilterJadwalLelangActivity.class);
        activity.startActivityForResult(starter, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == KpknlActivity.REQUEST_CODE){
            if (resultCode == KpknlActivity.RESULT_CODE){
                ArrayList<KpknlItem> listKpknl = data.getParcelableArrayListExtra(KpknlActivity.EXTRA_SELECTED_KPKNL);
                if (listKpknl.size() > 0){
                    listSelectedKpknl.addAll(listKpknl);
                    setUpKpknlOnEditText(listSelectedKpknl);
                }
            }
        }
    }

    private void setUpKpknlOnEditText(ArrayList<KpknlItem> list) {
        String kpknl = "";
        if (list.size() == 1){
            kpknl += list.get(0).getName();
        }else{
            for (int i = 0; i < list.size(); i++){
                if (i == 0){
                    kpknl += list.get(i).getName() +",";
                }else{
                    if (i % 2 == 0){
                        kpknl += "," + list.get(i).getName();
                    }else{
                        kpknl += list.get(i).getName();
                    }
                }
            }
        }

        edtKpknls.setText(kpknl.replace("Semua", "").trim());
    }

    private void initStartDate(){
        // get the current date
        final Calendar c = Calendar.getInstance();
        fromYear = c.get(Calendar.YEAR);
        fromMonth = c.get(Calendar.MONTH);
        fromDay = c.get(Calendar.DAY_OF_MONTH);

        updateStartDisplay();

    }

    private DatePickerDialog.OnDateSetListener mStartDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    fromYear = year;
                    fromMonth = monthOfYear;
                    fromDay = dayOfMonth;
                    updateStartDisplay();
                }
            };

    private void updateStartDisplay() {
        String year,month,day;
        year = fromYear+"";
        if(fromMonth + 1 < 10)
            month = "0" + (fromMonth + 1);
        else
            month = (fromMonth+1) + "";
        if(fromDay < 10)
            day = "0" + fromDay;
        else
            day = fromDay + "";

        fromDate = year + "-" + month + "-" + day;

        tvStartDate.setText(
                new StringBuilder()
                        // Month is 0 based so add 1
                        .append(day).append("-").append(month).append("-")
                        .append(year));
    }

    private void initEndDate(){
        // get the current date
        final Calendar c = Calendar.getInstance();
        toYear = c.get(Calendar.YEAR);
        toMonth = c.get(Calendar.MONTH);
        toDay = c.get(Calendar.DAY_OF_MONTH);

        updateEndDisplay();

    }

    private DatePickerDialog.OnDateSetListener mEndDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    toYear = year;
                    toMonth = monthOfYear;
                    toDay = dayOfMonth;
                    updateEndDisplay();
                }
            };

    private void updateEndDisplay() {
        String year,month,day;
        year = toYear+"";
        if(toMonth + 1 < 10)
            month = "0" + (toMonth + 1);
        else
            month = (toMonth+1) + "";
        if(toDay < 10)
            day = "0" + toDay;
        else
            day = toDay + "";

        toDate = year + "-" + month + "-" + day;

        tvEndDate.setText(
                new StringBuilder()
                        // Month is 0 based so add 1
                        .append(day).append("-").append(month).append("-")
                        .append(year));
    }
}
