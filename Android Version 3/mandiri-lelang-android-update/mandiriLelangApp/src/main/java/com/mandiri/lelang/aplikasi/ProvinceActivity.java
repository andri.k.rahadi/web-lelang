package com.mandiri.lelang.aplikasi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.sparkworks.mandiri.lelang.adapter.KpknlAdapter;
import com.sparkworks.mandiri.lelang.adapter.ProvinceAdapter;
import com.sparkworks.mandiri.lelang.base.BaseActivity;
import com.sparkworks.mandiri.lelang.database.helper.ProvinceHelper;
import com.sparkworks.mandiri.lelang.model.KpknlItem;
import com.sparkworks.mandiri.lelang.model.MasterData;
import com.sparkworks.mandiri.lelang.model.ProvinceItem;

import java.util.ArrayList;

public class ProvinceActivity extends BaseActivity implements View.OnClickListener,
        KpknlAdapter.OnCheckAllCallback, CompoundButton.OnCheckedChangeListener{
    private ListView lvProvince;
    private Button btnSelect;
    private CheckBox cbAll;
    private LinearLayout lnAll;

    private ProvinceHelper provinceHelper;
    private ProvinceAdapter adapter;
    private ArrayList<ProvinceItem> selectedProvinceList, listProvince, listCurrentProvinces;
    private boolean isAllChecked = false;

    public static final String EXTRA_SELECTED_PROVINCES = "extra_selected_provinces";
    public static final String EXTRA_CURRENT_PROVINCES = "extra_current_provinces";
    public static final String EXTRA_IS_ALL_CHECKED = "extra_is_all_checked";

    public static final int REQUEST_CODE = 100;
    public static final int RESULT_CODE = 110;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_province);

        getActionBar().setDisplayUseLogoEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.logo_home);
        getActionBar().setTitle("");
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        listCurrentProvinces = getIntent().getParcelableArrayListExtra(EXTRA_CURRENT_PROVINCES);
        isAllChecked = getIntent().getBooleanExtra(EXTRA_IS_ALL_CHECKED, false);

        lvProvince = (ListView)findViewById(R.id.lv_province);
        btnSelect = (Button)findViewById(R.id.btn_select);
        btnSelect.setOnClickListener(this);
        cbAll = (CheckBox)findViewById(R.id.cb_item_all);
        cbAll.setOnCheckedChangeListener(this);
        lnAll = (LinearLayout)findViewById(R.id.ln_option_all);

        adapter = new ProvinceAdapter(ProvinceActivity.this);
        adapter.setOnCheckAllCallback(this);
        selectedProvinceList = new ArrayList<>();
        listProvince = new ArrayList<>();
        adapter.setListProvince(listProvince);
        lvProvince.setAdapter(adapter);

        provinceHelper = new ProvinceHelper(this);
        provinceHelper.open();

        ArrayList<MasterData> provinces = provinceHelper.loadAllData();
        for (MasterData data : provinces){
            ProvinceItem item = new ProvinceItem();
            item.setId(data.getId());
            item.setName(data.getName());
            if (listCurrentProvinces != null){
                for (int i = 0; i < listCurrentProvinces.size(); i++){
                    if (data.getId().equalsIgnoreCase(listCurrentProvinces.get(i).getId())){
                        item.setChecked(true);
                    }
                }
            }else{
                item.setChecked(false);
            }
            listProvince.add(item);
        }

        adapter.setListProvince(listProvince);
        adapter.notifyDataSetChanged();

        cbAll.setChecked(isAllChecked);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_select){
            for (ProvinceItem item : adapter.getListProvince()){
                if (item.isChecked()){
                    selectedProvinceList.add(item);
                }
            }

            Intent intent = new Intent();
            intent.putExtra(EXTRA_SELECTED_PROVINCES, selectedProvinceList);
            intent.putExtra(EXTRA_IS_ALL_CHECKED, isAllChecked);

            setResult(RESULT_CODE, intent);

            finish();
        }
    }

    public static void start(Activity activity, boolean isAllChecked, ArrayList<ProvinceItem> provinces) {
        Intent starter = new Intent(activity, ProvinceActivity.class);
        starter.putExtra(EXTRA_CURRENT_PROVINCES, provinces);
        starter.putExtra(EXTRA_IS_ALL_CHECKED, isAllChecked);
        activity.startActivityForResult(starter, REQUEST_CODE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (compoundButton.isChecked()){
            isAllChecked = true;
            for (ProvinceItem item : listProvince){
                item.setChecked(true);
            }
        }else{
            isAllChecked = false;
            for (ProvinceItem item : listProvince){
                item.setChecked(false);
            }
        }
        adapter.setListProvince(listProvince);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCheckAll() {
        if (cbAll.isChecked()){
            cbAll.setChecked(false);
        }
    }
}
