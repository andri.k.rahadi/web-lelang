﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.Helper.Extension
{
    /// <summary>
    /// Extension method for type class
    /// </summary>
    public static class TypeExtensionMethod
    {
        /// <summary>
        /// Get an attribute from an object
        /// </summary>
        /// <typeparam name="T">The type of the attribute</typeparam>
        /// <param name="instance"></param>
        /// <param name="propertyName">The target object public property name</param>
        /// <returns>The attribute instance</returns>
        public static T GetAttributeFrom<T>(this Type instance, string propertyName) where T : Attribute
        {
            var attrType = typeof(T);
            var property = instance.GetProperty(propertyName);
            return (T)property.GetCustomAttributes(attrType, false).First();
        }

        /// <summary>
        /// Get the property maximum length from the string length attribute
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="propertyName">The target object public property name</param>
        /// <returns>The property maximum length</returns>
        public static int GetPropertyStringMaxLength(this Type instance, string propertyName)
        {
            var attrType = typeof(StringLengthAttribute);
            var property = instance.GetProperty(propertyName);
            StringLengthAttribute attribute = (StringLengthAttribute)property.GetCustomAttributes(attrType, false).First();
            if (attribute != null)
            {
                return attribute.MaximumLength;
            }
            else
            {
                return -1;
            }
        }
    }
}
