﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace Mandiri.Lelang.Helper.Extension
{
    /// <summary>
    /// Extension method for controller class
    /// </summary>
    public static class ControllerExtensionMethod
    {
        /// <summary>
        /// Helper method to redirect to a local url only
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="url">The url</param>
        /// <returns>The redirected view</returns>
        public static ActionResult RedirectToLocal(this Controller controller, String url)
        {
            if (controller.Url.IsLocalUrl(url))
            {
                RedirectResult result = new RedirectResult(url);
                return result;
            }
            else
            {
                RedirectToRouteResult result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "Index", controller = "Home" }));
                return result;
            }
        }

        /// <summary>
        /// Helper method to do a javascript redirection
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="actionName">The action name</param>
        /// <param name="controllerName">The controller name</param>
        /// <returns>The javascript result</returns>
        public static JavaScriptResult JavaScriptRedirect(this Controller controller, String actionName, String controllerName)
        {
            JavaScriptResult result = new JavaScriptResult();
            result.Script = String.Format("window.location = '{0}'", controller.Url.Action(actionName, controllerName));
            return result;
        }

        /// <summary>
        /// Helper method to do a javascript redirection
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="actionName">The action name</param>
        /// <returns>The javascript result</returns>
        public static JavaScriptResult JavaScriptRedirect(this Controller controller, String actionName)
        {
            JavaScriptResult result = new JavaScriptResult();
            result.Script = String.Format("window.location = '{0}'", controller.Url.Action(actionName));
            return result;
        }

        /// <summary>
        /// Helper method to do a local javascript redirection
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="url">The url</param>
        /// <returns>The javascript result</returns>
        public static JavaScriptResult JavaScriptRedirectToLocal(this Controller controller, String url)
        {
            JavaScriptResult result = new JavaScriptResult();
            if (controller.Url.IsLocalUrl(url))
            {
                result.Script = String.Format("window.location = '{0}'", url);
            }
            else
            {
                result.Script = String.Format("window.location = '{0}'", controller.Url.Action("Index", "Home"));
            }
            return result;
        }

        /// <summary>
        /// Show javascript alert
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="alertMessage"></param>
        /// <returns></returns>
        public static JavaScriptResult JavaScriptAlert(this Controller controller, String alertMessage)
        {
            JavaScriptResult result = new JavaScriptResult();
            result.Script = String.Format("alert('{0}');", alertMessage);
            return result;
        }

        /// <summary>
        /// Show javascript alert and redirect
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="alertMessage"></param>
        /// <param name="actionName">The action name</param>
        /// <param name="controllerName">The controller name</param>
        /// <returns></returns>
        public static JavaScriptResult JavaScriptAlertAndRedirect(this Controller controller, String alertMessage, String actionName, String controllerName)
        {
            JavaScriptResult result = new JavaScriptResult();
            result.Script = String.Format("alert('{0}');window.location = '{1}'", alertMessage, controller.Url.Action(actionName, controllerName));
            return result;
        }

        /// <summary>
        /// Helper method to get insensitive contains
        /// </summary>
        /// <param name="text">The text</param>
        /// <param name="value">The Value to find</param>
        /// <param name="stringComparison">Comparison Method</param>
        /// <returns>The boolean result</returns>
        public static bool CaseInsensitiveContains(this string text, string value,
                StringComparison stringComparison = StringComparison.CurrentCultureIgnoreCase)
        {
            return text.IndexOf(value, stringComparison) >= 0;
        }

        public static string ReplaceCaseInsensitive(this string input, string search, string replacement)
        {
            string result = Regex.Replace(
                input,
                Regex.Escape(search),
                replacement.Replace("$", "$$"),
                RegexOptions.IgnoreCase
            );
            return result;
        }
    }
}