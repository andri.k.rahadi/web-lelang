﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Mandiri.Lelang.Helper.Extension
{
    //Extension method for html helper class
    public static class HtmlHelperExtensionMethod
    {
        /// <summary>
        /// Generate a li menu item link that set an active class if matches the current action and controller
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="text">The menu item text</param>
        /// <param name="action">The action that will be called</param>
        /// <param name="controller">The controller that contains the action</param>
        /// <returns>The generated html</returns>
        public static MvcHtmlString MenuItemLink(this HtmlHelper htmlHelper, String text, String action, String controller)
        {
            var li = new TagBuilder("li");
            var routeData = htmlHelper.ViewContext.RouteData;
            var currentAction = routeData.GetRequiredString("action");
            var currentController = routeData.GetRequiredString("controller");
            if (string.Equals(currentAction, action, StringComparison.OrdinalIgnoreCase) &&
                string.Equals(currentController, controller, StringComparison.OrdinalIgnoreCase))
            {
                li.AddCssClass("active");
            }
            li.InnerHtml = htmlHelper.ActionLink(text, action, controller).ToHtmlString();
            return MvcHtmlString.Create(li.ToString());
        }
    }
}
