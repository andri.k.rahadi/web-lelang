﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Mandiri.Lelang.Helper
{
    public class AssetTimeZone
    {
        private IList<SelectListItem> listTimeZone;
        public AssetTimeZone(){}
        
        public IList<SelectListItem> GetListTimeZone()
        {
            listTimeZone = new List<SelectListItem>();
            listTimeZone.Add(new SelectListItem() { Text = "WIB", Value = "1" });
            listTimeZone.Add(new SelectListItem() { Text = "WIT", Value = "2" });
            listTimeZone.Add(new SelectListItem() { Text = "WITA", Value = "3" });
            return listTimeZone;
        }
        

    }
}
