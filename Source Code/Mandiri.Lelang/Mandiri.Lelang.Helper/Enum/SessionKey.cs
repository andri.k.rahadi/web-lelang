﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.Helper.Enum
{
    /// <summary>
    /// 
    /// </summary>
    public enum SessionKey
    {
        CurrentLoggedInUser,
        FirstLevelMenu,
        AuthorizedMenu,
        AssetPhoto1,
        AssetPhoto2,
        AssetPhoto3,
        AssetPhoto4,
        AssetPhoto5,
        FileWebsitePopupImage,
        FileMobileWebsitePopupImage
    }
}
