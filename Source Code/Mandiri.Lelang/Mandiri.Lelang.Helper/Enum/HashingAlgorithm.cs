﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.Helper.Enum
{
    /// <summary>
    /// The hashing algorith enumeration
    /// </summary>
    public enum HashingAlgorithm
    {
        /// <summary>
        /// MD5
        /// </summary>
        MD5,
        /// <summary>
        /// SHA256
        /// </summary>
        SHA256
    }
}
