﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace Mandiri.Lelang.Helper
{
    /// <summary>
    /// Helper methods for mvc controller
    /// </summary>
    public class ControllerHelper
    {
        /// <summary>
        /// Helper method to redirect to a local url only
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="url">The url</param>
        /// <returns>The redirected view</returns>
        private static ActionResult RedirectToLocal(Controller controller, String url)
        {
            if (controller.Url.IsLocalUrl(url))
            {
                RedirectResult result = new RedirectResult(url);
                return result;
            }
            else
            {
                RedirectToRouteResult result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "Index", controller = "Home" }));
                return result;
            }
        }

        /// <summary>
        /// Helper method to do a javascript redirection
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="actionName">The action name</param>
        /// <param name="controllerName">The controller name</param>
        /// <returns>The javascript result</returns>
        private static JavaScriptResult JavaScriptRedirect(Controller controller, String actionName, String controllerName)
        {
            JavaScriptResult result = new JavaScriptResult();
            result.Script = String.Format("window.location = '{0}'", controller.Url.Action("Index", "Home"));
            return result;
        }

        /// <summary>
        /// Helper method to do a local javascript redirection
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="url">The url</param>
        /// <returns>The javascript result</returns>
        private static JavaScriptResult JavaScriptRedirectToLocal(Controller controller, String url)
        {
            JavaScriptResult result = new JavaScriptResult();
            if (controller.Url.IsLocalUrl(url))
            {
                result.Script = String.Format("window.location = '{0}'", url);
            }
            else
            {
                result.Script = String.Format("window.location = '{0}'", controller.Url.Action("Index", "Home"));
            }
            return result;
        }
    }
}