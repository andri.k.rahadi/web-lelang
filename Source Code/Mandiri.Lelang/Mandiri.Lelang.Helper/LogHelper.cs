﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.Helper
{
    /// <summary>
    /// Helper class for handling logging
    /// </summary>
    public class LogHelper
    {
        /// <summary>
        /// The logger object
        /// </summary>
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Log the exception error message with log level error
        /// </summary>
        /// <param name="errorException">The error exception</param>
        public static void LogException(Exception errorException)
        {
            if (errorException != null && bool.Parse(ConfigurationManager.AppSettings["LogErrorMessage"]))
            {
                String innerExceptionMessage = String.Empty;
                if (errorException.InnerException != null)
                {
                    innerExceptionMessage = errorException.InnerException.Message;
                    if (errorException.InnerException.InnerException != null)
                    {
                        innerExceptionMessage += "|" + errorException.InnerException.InnerException.Message;
                    }
                }
                _logger.Error(errorException.Source + "|" + errorException.Message + "|" + errorException.StackTrace + "|" + innerExceptionMessage);

            }
        }
    }
}
