﻿using Mandiri.Lelang.Helper.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Mandiri.Lelang.Helper
{
    /// <summary>
    /// Helper class for handling session
    /// </summary>
    public class SessionHelper
    {
        /// <summary>
        /// Add or update session value based on the key provided
        /// </summary>
        /// <typeparam name="ValueType">The value type</typeparam>
        /// <param name="key">The key of the session</param>
        /// <param name="value">The value that will be set</param>
        public static void SetValue<ValueType>(SessionKey key, ValueType value)
        {
            if (HttpContext.Current.Session != null)
            {
                try
                {
                    bool flagFound = false;
                    for (int i = 0; i < HttpContext.Current.Session.Keys.Count; i++)
                    {
                        if (HttpContext.Current.Session.Keys[i] == key.ToString())
                        {
                            flagFound = true;
                            break;
                        }
                    }
                    if (flagFound)
                    {
                        HttpContext.Current.Session[key.ToString()] = value;
                    }
                    else
                    {
                        HttpContext.Current.Session.Add(key.ToString(), value);
                    }
                }
                catch
                {
                    //Do nothing
                }
            }
        }

        /// <summary>
        /// Get session value based on the key provided
        /// </summary>
        /// <typeparam name="ValueType">The value type</typeparam>
        /// <param name="key">The key of the session</param>
        /// <returns>The value of the session</returns>
        public static ValueType GetValue<ValueType>(SessionKey key)
        {
            if (HttpContext.Current.Session != null)
            {
                try
                {
                    bool flagFound = false;
                    for (int i = 0; i < HttpContext.Current.Session.Keys.Count; i++)
                    {
                        if (HttpContext.Current.Session.Keys[i] == key.ToString())
                        {
                            flagFound = true;
                            break;
                        }
                    }
                    if (flagFound)
                    {
                        return (ValueType)HttpContext.Current.Session[key.ToString()];
                    }
                    else
                    {
                        return default(ValueType);
                    }
                }
                catch
                {
                    return default(ValueType);
                }
            }
            else
            {
                return default(ValueType);
            }
        }

        /// <summary>
        /// Remove session value based on the key provided
        /// </summary>
        /// <param name="key">The key of the session</param>
        /// <returns>Success flag</returns>
        public static bool Remove(SessionKey key)
        {
            if (HttpContext.Current.Session != null)
            {
                try
                {
                    HttpContext.Current.Session.Remove(key.ToString());
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Clear all available session
        /// </summary>
        public static void Clear()
        {
            if (HttpContext.Current.Session != null)
            {
                try
                {
                    HttpContext.Current.Session.Clear();
                }
                catch
                {
                }
            }
        }
    }
}
