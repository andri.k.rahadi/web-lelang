﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.Helper
{
    /// <summary>
    /// A helper class for handling email operation
    /// </summary>
    public class MailHelper
    {
        /// <summary>
        /// A helper function to send an email
        /// </summary>
        /// <param name="from"></param>
        /// <param name="recipients"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isHtmlMessage"></param>
        public static void SendEmail(String smtpServer, String user, String password, String from, String recipients, String subject, String body, bool isHtmlMessage)
        {
            if (!String.IsNullOrEmpty(smtpServer))
            {
                SmtpClient smtpClient = new SmtpClient();
                MailMessage message = new MailMessage();
                message.Subject = subject;
                message.IsBodyHtml = isHtmlMessage;
                message.Body = body;
                //message.Sender = new MailAddress(from);
                message.From = new MailAddress(from);
                message.To.Add(recipients);
                smtpClient.Host = smtpServer;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                //smtpClient.Port = 587;
                //smtpClient.EnableSsl = true;
                smtpClient.Credentials = new NetworkCredential(user, password);
                //smtpClient.SendCompleted += smtpClient_SendCompleted;
                smtpClient.Send(message);
                //smtpClient.SendAsync(message, DateTime.Now.Ticks);
            }
        }

        /// <summary>
        /// Async method completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void smtpClient_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            //Do nothing
        }
    }
}
