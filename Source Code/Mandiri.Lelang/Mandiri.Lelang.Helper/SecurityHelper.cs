﻿using Mandiri.Lelang.Helper.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Mandiri.Lelang.Helper
{
    /// <summary>
    /// Helper class for handling security
    /// </summary>
    public class SecurityHelper
    {
        /// <summary>
        /// Salt length
        /// </summary>
        private static int _saltValueSize = 7;

        /// <summary>
        /// Generate salt to be used in hashing data
        /// </summary>
        /// <returns>The salt in string</returns>
        public static string GenerateSaltValue()
        {
            //Generate a cryptographic random number.
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[_saltValueSize];
            rng.GetBytes(buff);

            // Return a Base64 string representation of the random number.
            return Convert.ToBase64String(buff);
        }

        /// <summary>
        /// The method to generate hash
        /// </summary>
        /// <param name="clearData">The data to be hashed</param>
        /// <param name="saltValue">The additional salt</param>
        /// <param name="hashingAlgorithm">The hashing algorithm</param>
        /// <returns>Hashed data</returns>
        public static string Hash(string clearData, string saltValue, HashingAlgorithm hashingAlgorithm)
        {
            UnicodeEncoding encoding = new UnicodeEncoding();
            HashAlgorithm hash = null;
            String hashingResult = String.Empty;

            if (clearData != null && encoding != null)
            {
                // If the salt string is null or the length is invalid then
                // create a new valid salt value.

                if (saltValue == null)
                {
                    // Generate a salt string.
                    saltValue = GenerateSaltValue();
                }

                if (hashingAlgorithm == HashingAlgorithm.MD5)
                {
                    hash = MD5.Create();
                }
                else if (hashingAlgorithm == HashingAlgorithm.SHA256)
                {
                    hash = SHA256Managed.Create();
                }

                byte[] crypto = hash.ComputeHash(Encoding.UTF8.GetBytes(String.Concat(clearData, saltValue)));

                foreach (byte bit in crypto)
                {
                    hashingResult += bit.ToString("x2");
                }

                return hashingResult;
            }

            return null;
        }

        /// <summary>
        /// Validate for SQL injection in params
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool ValidateSQLInjection(String text)
        {
            bool isSQLInjection = false;            
            string[] sqlCheckList = {  "--",
                                       ";--",
                                       ";",
                                       "/*",
                                       "*/",
                                        //"@@",
                                        //"@",
                                        "char",
                                       "nchar",
                                       "varchar",
                                       "nvarchar",
                                       "alter",
                                       "begin",
                                       "cast",
                                       "create",
                                       "cursor",
                                       "declare",
                                       "delete",
                                       "drop",
                                       //"end",
                                       "exec",
                                       "execute",
                                       "fetch",
                                       "insert",
                                       "kill",
                                       "select",
                                       "sys",
                                       "sysobjects",
                                       "syscolumns",
                                       "table",
                                       "update"
                                       };
            string CheckString = text.Replace("'", "''");
            for (int i = 0; i <= sqlCheckList.Length - 1; i++)
            {
                if ((CheckString.IndexOf(sqlCheckList[i], StringComparison.OrdinalIgnoreCase) >= 0))
                {
                    isSQLInjection = true;
                }
            }
            return isSQLInjection;
        }

        /// <summary>
        /// Get the current logged in user id from forms authentication object
        /// </summary>
        /// <returns>The current logged in user id</returns>
        public static int GetCurrentLoggedInUserID()
        {
            return int.Parse(HttpContext.Current.User.Identity.Name);
        }

        /// <summary>
        /// Get the current logged in member id from forms authentication object
        /// </summary>
        /// <returns>The current logged in member id</returns>
        public static long? GetCurrentLoggedInMemberID()
        {
            if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity != null && !String.IsNullOrWhiteSpace(HttpContext.Current.User.Identity.Name))
            {
                try
                {
                    return long.Parse(HttpContext.Current.User.Identity.Name);
                }
                catch
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Check whether the current user is authenticated or not
        /// </summary>
        /// <returns>Authenticated flag</returns>
        public static bool IsAuthenticated()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }

        public static String GenerateJWTToken(IDictionary<String, Object> payload, DateTime expiredDateTimeInUtc, String secretKey)
        {
            var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var now = Math.Round((expiredDateTimeInUtc - unixEpoch).TotalSeconds);
            payload.Add("exp", now);
            string token = JWT.JsonWebToken.Encode(payload, secretKey, JWT.JwtHashAlgorithm.HS256);
            return token;
        }

        public static IDictionary<String, object> VerifyJWTToken(String token, String secretKey, bool checkForExpiry = false)
        {
            try
            {
                var payload = JWT.JsonWebToken.DecodeToObject(token, secretKey, checkForExpiry) as IDictionary<string, object>;
                return payload;
            }
            catch (JWT.SignatureVerificationException e)
            {
                if (e.Message.ToUpper().Contains("EXPIRED") || e.InnerException.Message.ToUpper().Contains("EXPIRED"))
                {
                    throw new Exception("Token has expired");
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
