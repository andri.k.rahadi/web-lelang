﻿using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.DataRepository;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.Helper.Enum;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insight.Database;
using Mandiri.Lelang.Helper.Extension;
using System.Configuration;

namespace Mandiri.Lelang.BusinessFacade
{
    public class AdministrationFacade : BaseFacade
    {
        #region Get

        #region Group
        /// <summary>
        /// Get active group  by id
        /// </summary>
        /// <param name="groupID">The group id</param>
        /// <returns>Facade result containing the group object</returns>
        public static FacadeResult<Group> GetActiveGroupByGroupID(int groupID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<Group> result = new FacadeResult<Group>();

            Group group = context.GetActiveGroupByID(groupID);

            result.ReturnData = group;
            result.IsSuccess = group == null ? false : true;
            result.ErrorMessage = group == null ? String.Format(TextResources.NotFound_ErrorMessage, "User") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active group
        /// </summary>
        /// <returns>Facade result containing the group object</returns>
        public static FacadeResult<IList<Group>> GetAllActiveGroups()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<Group>> result = new FacadeResult<IList<Group>>();

            IList<Group> groups = context.GetAllActiveGroups();

            result.ReturnData = groups;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active group list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the group object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveGroupsList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "GroupID", "Name", "Description" };

            IList<dynamic> groups = context.QuerySql<dynamic>(
               String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                GroupID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]
                FROM [Group] WHERE IsActive = 1 AND (Name LIKE @Keywords OR Description LIKE @Keywords)) TBL WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
               , sortColumnNames[sortIndex], sortDirection)
               , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = groups;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active groups count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the groups count</returns>
        public static FacadeResult<long> GetActiveGroupsCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(GroupID) 
                FROM [Group] 
                WHERE (Name LIKE @Keywords OR Description LIKE @Keywords) AND IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active groups count
        /// </summary>
        /// <returns>Facade result containing the groups count</returns>
        public static FacadeResult<long> GetActiveGroupsCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveGroupsCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        public static FacadeResult<int> GetGroupIDByUserID(int userID, int approvalGroupID, int inputterGroupID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<int> result = new FacadeResult<int>();
            int groupID = 0;

            IList<int> listGroupID = context.GetGroupIDByUserID(userID, approvalGroupID, inputterGroupID);

            if (listGroupID != null)
            {
                if (listGroupID.Count > 1)
                {
                    groupID = -1; // inputter and approval
                }
                else if (listGroupID.Count == 1)
                {
                    groupID = listGroupID[0];
                }
            }

            result.ReturnData = groupID;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        #endregion

        #region User Group Mapping
        /// <summary>
        /// Get active UserGroupMapping  by id
        /// </summary>
        /// <param name="userGroupMappingID">The UserGroupMapping id</param>
        /// <returns>Facade result containing the UserGroupMapping object</returns>
        public static FacadeResult<UserGroupMapping> GetActiveUserGroupMappingByUserGroupMappingID(int userGroupMappingID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<UserGroupMapping> result = new FacadeResult<UserGroupMapping>();

            UserGroupMapping userGroupMapping = context.GetActiveUserGroupMappingByID(userGroupMappingID);

            result.ReturnData = userGroupMapping;
            result.IsSuccess = userGroupMapping == null ? false : true;
            result.ErrorMessage = userGroupMapping == null ? String.Format(TextResources.NotFound_ErrorMessage, "User") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active UserGroupMapping
        /// </summary>
        /// <returns>Facade result containing the UserGroupMapping object</returns>
        public static FacadeResult<IList<UserGroupMapping>> GetAllActiveUserGroupMappings()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<UserGroupMapping>> result = new FacadeResult<IList<UserGroupMapping>>();

            IList<UserGroupMapping> userGroupMappings = context.GetAllActiveUserGroupMappings();

            result.ReturnData = userGroupMappings;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active userGroupMappings list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the userGroupMappings object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveUserGroupMappingsList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "UserGroupMappingID", "UGM.GroupID", "UGM.UserID" };

            IList<dynamic> userGroupMappings = context.QuerySql<dynamic>(
               String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                UserGroupMappingID, G.Name, U.UserName, G.[IsActive], G.[DateTimeCreated], G.[DateTimeUpdated], G.[CreatedBy], G.[LastUpdatedBy]
                FROM UserGroupMapping UGM JOIN [Group] G ON UGM.GroupID = G.GroupID JOIN [User] U ON UGM.UserID = U.UserID 
                WHERE UGM.IsActive = 1 AND 
                (G.Name LIKE @Keywords OR U.UserName LIKE @Keywords)) TBL WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
               , sortColumnNames[sortIndex], sortDirection)
               , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = userGroupMappings;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active userGroupMappings count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the userGroupMappings count</returns>
        public static FacadeResult<long> GetActiveUserGroupMappingsCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(UserGroupMappingID) 
                FROM [UserGroupMapping] UGM JOIN [Group] G ON UGM.GroupID = G.GroupID JOIN [User] U ON UGM.UserID = U.UserID
                WHERE (G.Name LIKE @Keywords OR U.UserName LIKE @Keywords) AND UGM.IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active userGroupMappings count
        /// </summary>
        /// <returns>Facade result containing the userGroupMappings count</returns>
        public static FacadeResult<long> GetActiveUserGroupMappingsCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveUserGroupMappingsCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        #endregion

        #region User
        /// <summary>
        /// Get all active user
        /// </summary>
        /// <returns>Facade result containing the user object</returns>
        public static FacadeResult<IList<User>> GetAllActiveUsers()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<User>> result = new FacadeResult<IList<User>>();

            IList<User> users = context.GetAllActiveUsers();

            result.ReturnData = users;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get active user  by id
        /// </summary>
        /// <param name="userID">The user id</param>
        /// <returns>Facade result containing the user object</returns>
        public static FacadeResult<User> GetActiveUserByUserID(int userID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<User> result = new FacadeResult<User>();

            User user = context.GetActiveUserByID(userID);

            result.ReturnData = user;
            result.IsSuccess = user == null ? false : true;
            result.ErrorMessage = user == null ? String.Format(TextResources.NotFound_ErrorMessage, "User") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active user list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the user object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveUsersList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "UserID", "Username", "FullName", "B.Name", "IsLocked" };

            IList<dynamic> users = context.QuerySql<dynamic>(
               String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                UserID, Username, FullName, ISNULL(B.Name, 'Semua') AS BranchName, IsLocked
                FROM [User] U LEFT JOIN Branch B ON B.BranchID = U.BranchID WHERE U.IsActive = 1 
                AND (Username LIKE @Keywords OR FullName LIKE @Keywords OR ISNULL(B.Name, 'Semua') LIKE @Keywords)) TBL WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
               , sortColumnNames[sortIndex], sortDirection)
               , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = users;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active users count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the users count</returns>
        public static FacadeResult<long> GetActiveUsersCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(UserID) 
                FROM [User] U LEFT JOIN Branch B ON B.BranchID = U.BranchID
                WHERE (Username LIKE @Keywords OR FullName LIKE @Keywords OR ISNULL(B.Name, 'Semua') LIKE @Keywords) AND U.IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active users count
        /// </summary>
        /// <returns>Facade result containing the users count</returns>
        public static FacadeResult<long> GetActiveUsersCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveUsersCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        #endregion

        #region Message
        /// <summary>
        /// Get all active message list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the message object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveMessagesList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection, int currentUserID, int isPending = 0)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "MessageID", "MCT.Name", "M.Name", "Phone", "Email", "TextMessage", "M.AssetCode", "B.Name", "M.DateTimeCreated", "Remarks", "Status"};
            int branchID;
            branchID = context.GetActiveUserByID(currentUserID).BranchID.Value;

            if (branchID == -1)
            {
                IList<dynamic> groups = context.QuerySql<dynamic>(
               String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                MessageID, MCT.Name AS CategoryName , M.Name AS Name, Phone, Email, TextMessage, M.[IsActive]
				, M.[DateTimeCreated], ISNULL(B.Name, '') AS BranchName, M.AssetCode
				, ISNULL(m.Remarks,'') as Remarks, ISNULL(MS.Name,'') AS Status
                FROM [Message] M 
                    JOIN [MessageCategoryType] MCT ON M.MessageCategoryTypeID = MCT.MessageCategoryTypeID 
                    LEFT JOIN Branch B ON M.BranchID = B.BranchID
					LEFT JOIN MessageStatus MS ON m.MessageStatusID = ms.MessageStatusID
                WHERE M.IsActive = 1
                AND( (@Pending = 1 AND MS.IsComplete  IN (0,1)) OR
                     (@Pending = 0 AND MS.IsComplete IN (0))
                   )             
                AND 
                (M.AssetCode LIKE @Keywords OR MCT.Name LIKE @Keywords OR M.Name LIKE @Keywords OR Phone LIKE @Keywords OR Email LIKE @Keywords OR TextMessage LIKE @Keywords OR ISNULL(B.Name, '') LIKE @Keywords)) TBL 
                WHERE NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
               , sortColumnNames[sortIndex], sortDirection)
               , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()), Pending = isPending });

                result.ReturnData = groups;
            }
            else
            {
                IList<dynamic> groups = context.QuerySql<dynamic>(
               String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                MessageID, MCT.Name AS CategoryName , M.Name AS Name, Phone, Email, TextMessage, M.[IsActive]
				, M.[DateTimeCreated], ISNULL(B.Name, '') AS BranchName, M.AssetCode
				, ISNULL(m.Remarks,'') as Remarks, ISNULL(MS.Name,'') AS Status
                FROM [Message] M 
                    JOIN [MessageCategoryType] MCT ON M.MessageCategoryTypeID = MCT.MessageCategoryTypeID 
                    LEFT JOIN Branch B ON M.BranchID = B.BranchID
					LEFT JOIN MessageStatus MS ON m.MessageStatusID = ms.MessageStatusID
                WHERE M.IsActive = 1
                AND M.BranchID = @BranchID 
                 AND( (@Pending = 1 AND MS.IsComplete IN (0,1)) OR
                     (@Pending = 0 AND MS.IsComplete IN (0))
                   )   
                AND
                (M.AssetCode LIKE @Keywords OR MCT.Name LIKE @Keywords OR M.Name LIKE @Keywords OR Phone LIKE @Keywords OR Email LIKE @Keywords OR TextMessage LIKE @Keywords OR ISNULL(B.Name, '') LIKE @Keywords)) TBL 
                WHERE NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
               , sortColumnNames[sortIndex], sortDirection)
               , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()), BranchID = branchID, Pending = isPending });

                result.ReturnData = groups;
            }

            
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active Message count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the Message count</returns>
        public static FacadeResult<long> GetActiveMessagesCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(MessageID) 
                FROM [Message] M 
                    JOIN [MessageCategoryType] MCT ON M.MessageCategoryTypeID = MCT.MessageCategoryTypeID 
                    LEFT JOIN Branch B ON M.BranchID = B.BranchID
                WHERE (M.AssetCode LIKE @Keywords OR MCT.Name LIKE @Keywords OR M.Name LIKE @Keywords OR Phone LIKE @Keywords OR Email LIKE @Keywords OR TextMessage LIKE @Keywords OR ISNULL(B.Name, '') LIKE @Keywords) AND M.IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active Message count
        /// </summary>
        /// <returns>Facade result containing the Message count</returns>
        public static FacadeResult<long> GetActiveMessagesCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveMessagesCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get active message by message id
        /// </summary>
        /// <param name="messageID">The message id</param>
        /// <returns>Facade result containing the message object</returns>
        public static FacadeResult<Message> GetActiveMessageByMessageID(int messageID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<Message> result = new FacadeResult<Message>();

            Message message = context.GetActiveMessageByID(messageID);

            result.ReturnData = message;
            result.IsSuccess = message == null ? false : true;
            result.ErrorMessage = message == null ? String.Format(TextResources.NotFound_ErrorMessage, "Message") : String.Empty;

            return result;
        }

        /// <summary>
        /// Update message
        /// </summary>
        /// <param name="message">The message object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated city</returns>
        public static FacadeResult<Message> UpdateMessage(Message message, int currentUserID)
        {
            FacadeResult<Message> result = new FacadeResult<Message>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;
            bool BranchChanged = false;
            bool EmailChanged = false;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Message updatedMessage = context.GetActiveMessageByID(message.MessageID);

                    BranchChanged = updatedMessage.BranchID != message.BranchID ? true : false;
                    EmailChanged = updatedMessage.Email != message.Email? true : false;

                    if (updatedMessage != null)
                    {
                        updatedMessage.MessageCategoryTypeID = message.MessageCategoryTypeID;
                        updatedMessage.Name = message.Name;
                        updatedMessage.Phone = message.Phone;
                        updatedMessage.Email = message.Email;
                        updatedMessage.AssetCode = message.AssetCode;
                        updatedMessage.BranchID = message.BranchID;
                        updatedMessage.TextMessage = message.TextMessage;
                        updatedMessage.MessageStatusID = message.MessageStatusID;
                        updatedMessage.Remarks = message.Remarks;
                        updatedMessage.DateTimeUpdated = DateTime.Now;
                        updatedMessage.LastUpdatedBy = currentUserID;
                        context.UpdateMessage(updatedMessage);

                        userActivity.UserActivityTypeID = "UpdateMessage";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = message.MessageCategoryTypeID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updatedMessage;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;

                        
                            //add for CR2 send email to all user in same branch and use template
                            IList<User> user = UserFacade.GetAllActiveUserByBranchID(message.BranchID ?? 0).ReturnData;
                            string templateMessage = MasterFacade.GetActiveWebContentByWebContentTypeID(11).ReturnData.Content;

                            if (templateMessage.CaseInsensitiveContains("{Name}"))
                            {
                                templateMessage = templateMessage.ReplaceCaseInsensitive("{Name}", message.Name);
                            }

                            if (templateMessage.CaseInsensitiveContains("{Email}"))
                            {
                                if (message.Email != null)
                                {
                                    templateMessage = templateMessage.ReplaceCaseInsensitive("{Email}", message.Email);
                                }
                                else
                                {
                                    templateMessage = templateMessage.ReplaceCaseInsensitive("{Email}", "");
                                }
                            }

                            if (templateMessage.CaseInsensitiveContains("{Contact}"))
                            {
                                templateMessage = templateMessage.ReplaceCaseInsensitive("{Contact}", message.Phone);
                            }

                            if (templateMessage.CaseInsensitiveContains("{Category}"))
                            {
                                if (message.MessageCategoryTypeID != null)
                                {
                                    string messageCategoryTypeName = MasterFacade.GetActiveMessageCategoryTypeByMessageCategoryTypeID((byte)message.MessageCategoryTypeID).ReturnData.Name;
                                    templateMessage = templateMessage.ReplaceCaseInsensitive("{Category}", messageCategoryTypeName);
                                }
                                else
                                {
                                    templateMessage = templateMessage.ReplaceCaseInsensitive("{Category}", "");
                                }
                            }

                            if (templateMessage.CaseInsensitiveContains("{AssetCode}"))
                            {
                                templateMessage = templateMessage.ReplaceCaseInsensitive("{AssetCode}", message.AssetCode);
                            }

                            if (templateMessage.CaseInsensitiveContains("{Branch}"))
                            {
                                if (message.BranchID != null)
                                {
                                    string branchName = MasterFacade.GetActiveBranchByBranchID((int)message.BranchID).ReturnData.Name;
                                    templateMessage = templateMessage.ReplaceCaseInsensitive("{Branch}", branchName);
                                }
                                else
                                {
                                    templateMessage = templateMessage.ReplaceCaseInsensitive("{Branch}", "");
                                }
                            }

                            if (templateMessage.CaseInsensitiveContains("{Message}"))
                            {
                                if (message.TextMessage != null)
                                {
                                    templateMessage = templateMessage.ReplaceCaseInsensitive("{Message}", message.TextMessage);
                                }
                                else
                                {
                                    templateMessage = templateMessage.ReplaceCaseInsensitive("{Message}", "" );
                                }
                            }

                            if (EmailChanged && message.Email != null)
                            {
                                MailHelper.SendEmail(ConfigurationManager.AppSettings["MailSmtpServer"], ConfigurationManager.AppSettings["MailUser"], ConfigurationManager.AppSettings["MailPassword"],
                                ConfigurationManager.AppSettings["MailSender"], message.Email, "Bank Mandiri Web Lelang - Terima Kasih",
                                templateMessage, true);
                            }

                             if (BranchChanged && user != null)
                                {
                                    foreach (User u in user)
                                    {
                                        if (u.Email != null && u.Email != string.Empty)
                                        {
                                            MailHelper.SendEmail(ConfigurationManager.AppSettings["MailSmtpServer"], ConfigurationManager.AppSettings["MailUser"], ConfigurationManager.AppSettings["MailPassword"],
                                                ConfigurationManager.AppSettings["MailSender"], u.Email, "Bank Mandiri Web Lelang - Terima Kasih",
                                                templateMessage, true);
                                        }
                                    }
                                }      
                        }
                    
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Message");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        public static FacadeResult<long> GetUnCompletedMessageCountbyUserBranchID(int currentUserID) 
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            int branchID;
            long count = 0;
            branchID = context.GetActiveUserByID(currentUserID).BranchID.Value;
            if (branchID == -1)
            {
                count = context.ExecuteScalarSql<dynamic>(@"select COUNT(messageid) from Message a inner join MessageStatus b 
                           on a.MessageStatusID = b.MessageStatusID
                           inner join Branch c
                           on a.BranchID = c.BranchID
                           where b.IsComplete = 0");
            }
            else
            {
                count = context.ExecuteScalarSql<dynamic>(@"select COUNT(messageid) from Message a inner join MessageStatus b 
                           on a.MessageStatusID = b.MessageStatusID
                           inner join Branch c
                           on a.BranchID = c.BranchID
                           where b.IsComplete = 0 and a.BranchID = @BranchID or a.BranchID = -1", new
                                                                                                {
                                                                                                    BranchID = branchID
                                                                                                });
            }
            
            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        public static FacadeResult<long> GetExpiredDateCountbyUserBranchID(int currentUserID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            int branchID;
            long count = 0;
            branchID = context.GetActiveUserByID(currentUserID).BranchID.Value;
            if (branchID == -1)
            {
                count = context.ExecuteScalarSql<dynamic>(@"select COUNT(AssetID) from Asset A Join AssetStatus Ast 
                            ON A.CurrentStatusID = Ast.AssetStatusID
                            Where A.ExpiredDate < GETDATE() AND A.CurrentStatusID <> 5 AND a.Type = 1");
            }
            else
            {
                count = context.ExecuteScalarSql<dynamic>(@"select COUNT(AssetID) from Asset A Join AssetStatus Ast 
                            ON A.CurrentStatusID = Ast.AssetStatusID
                            Where A.ExpiredDate < GETDATE() AND A.CurrentStatusID <> 5 AND a.Type = 1 AND A.BranchID=@BranchID", new
                                                                                                {
                                                                                                    BranchID = branchID
                                                                                                });
            }

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        #endregion

        #region User Activity
        /// <summary>
        /// Get all active UserActivity list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the UserActivity object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveUserActivitiesList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "UserActivityID", "UserActivityTypeID", "ExtraDescription", "ReferenceID", "UA.CreatedBy", "UA.DateTimeCreated" };

            IList<dynamic> groups = context.QuerySql<dynamic>(
               String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                UserActivityID, UserActivityTypeID, ExtraDescription, ReferenceID, UA.[IsActive], UA.[DateTimeCreated], UA.[DateTimeUpdated],
                U.Username AS Created, UA.[LastUpdatedBy]
                FROM [UserActivity] UA JOIN [User] U ON UA.CreatedBy = U.UserID WHERE UA.IsActive = 1 AND 
                (UserActivityTypeID LIKE @Keywords OR ExtraDescription LIKE @Keywords OR ReferenceID LIKE @Keywords OR U.Username LIKE @Keywords)) TBL 
                WHERE NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
               , sortColumnNames[sortIndex], sortDirection)
               , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = groups;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active UserActivity count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the UserActivity count</returns>
        public static FacadeResult<long> GetActiveUserActivitiesByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(UserActivityID) 
                FROM [UserActivity] UA JOIN [User] U ON UA.CreatedBy = U.UserID 
                WHERE (UserActivityTypeID LIKE @Keywords OR ExtraDescription LIKE @Keywords OR ReferenceID LIKE @Keywords OR U.Username LIKE @Keywords) AND UA.IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active UserActivity count
        /// </summary>
        /// <returns>Facade result containing the UserActivity count</returns>
        public static FacadeResult<long> GetActiveUserActivitiesCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveUserActivitiesCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        #endregion

        #region Group Authorization

        /// <summary>
        /// Get all current group authorization by group id
        /// </summary>
        /// <param name="groupID"></param>
        /// <returns>List of all active current group authorizations</returns>
        public static FacadeResult<IList<GroupAuthorization>> GetCurrentGroupAuthorizationByGroupID(int groupID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<GroupAuthorization>> result = new FacadeResult<IList<GroupAuthorization>>();
            GroupAuthorization groupAuthorization;

            IList<GroupAuthorization> groupAuthorizations = context.GetActiveGroupAuthorizationByGroupID(groupID);
            IEnumerable<SiteMap> siteMaps = context.GetActiveSiteMaps().AsEnumerable();


            if (groupID != 0)
            {
                foreach (var siteMap in siteMaps.AsEnumerable())
                {
                    if (groupAuthorizations.Count(m => m.SiteMapID == siteMap.SiteMapID) == 0)
                    {
                        groupAuthorization = new GroupAuthorization();
                        groupAuthorization.SiteMap = siteMap;
                        groupAuthorization.SiteMapID = siteMap.SiteMapID;
                        groupAuthorization.GroupID = groupID;
                        groupAuthorizations.Add(groupAuthorization);
                    }
                }
                foreach (GroupAuthorization g in groupAuthorizations)
                {
                    if (g.SiteMap == null)
                    {
                        g.SiteMap = siteMaps.Where(m => m.SiteMapID == g.SiteMapID).SingleOrDefault();
                    }
                }
            }

            result.ReturnData = groupAuthorizations.OrderBy(m => m.SiteMapID).ToList();
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Check whether the user has access to menu or not
        /// </summary>
        /// <param name="siteMapID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static bool IsUserAllowedAccess(String siteMapID, int userID)
        {
            FacadeResult<IList<SiteMap>> siteMaps = MenuFacade.GetAllowedAccessMenusByUserID(userID);
            if (siteMaps.IsSuccess && siteMaps.ReturnData.Count > 0)
            {
                if (siteMaps.ReturnData.Count(m => m.SiteMapID == siteMapID) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check whether the user has delete access to menu or not
        /// </summary>
        /// <param name="siteMapID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static bool IsUserAllowedDelete(String siteMapID, int userID)
        {
            FacadeResult<IList<SiteMap>> siteMaps = MenuFacade.GetAllowedDeleteMenusByUserID(userID);
            if (siteMaps.IsSuccess && siteMaps.ReturnData.Count > 0)
            {
                if (siteMaps.ReturnData.Count(m => m.SiteMapID == siteMapID) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check whether the user has add access to menu or not
        /// </summary>
        /// <param name="siteMapID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static bool IsUserAllowedAdd(String siteMapID, int userID)
        {
            FacadeResult<IList<SiteMap>> siteMaps = MenuFacade.GetAllowedAddMenusByUserID(userID);
            if (siteMaps.IsSuccess && siteMaps.ReturnData.Count > 0)
            {
                if (siteMaps.ReturnData.Count(m => m.SiteMapID == siteMapID) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check whether the user has update access to menu or not
        /// </summary>
        /// <param name="siteMapID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static bool IsUserAllowedUpdate(String siteMapID, int userID)
        {
            FacadeResult<IList<SiteMap>> siteMaps = MenuFacade.GetAllowedUpdateMenusByUserID(userID);
            if (siteMaps.IsSuccess && siteMaps.ReturnData.Count > 0)
            {
                if (siteMaps.ReturnData.Count(m => m.SiteMapID == siteMapID) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Add New Group Authorization
        /// </summary>
        /// <param name="groupID"></param>
        /// <param name="groupAuthorizations"></param>
        /// <param name="currentUserID"></param>
        /// <returns></returns>
        public static FacadeResult<IList<GroupAuthorization>> AddNewGroupAuthorizations(int groupID, IList<GroupAuthorization> groupAuthorizations, int currentUserID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<GroupAuthorization>> result = new FacadeResult<IList<GroupAuthorization>>();
            UserActivity userActivity = new UserActivity();

            context.QuerySql("DELETE FROM GroupAuthorization WHERE GroupID = @GroupID", new { GroupID = groupID });

            int count = 0;
            foreach (GroupAuthorization groupAuthorization in groupAuthorizations)
            {
                groupAuthorization.CreatedBy = currentUserID;
                context.InsertGroupAuthorization(groupAuthorization);
                count++;
            }

            if (count > 0)
            {
                userActivity.UserActivityTypeID = "UpdateGroupAuthorization";
                userActivity.CreatedBy = currentUserID;
                userActivity.ReferenceID = groupID.ToString();
                context.InsertUserActivity(userActivity);

                result.ReturnData = groupAuthorizations;
                result.IsSuccess = true;
                result.ErrorMessage = String.Empty;
            }
            else
            {
                result.ReturnData = null;
                result.IsSuccess = false;
                result.ErrorMessage = TextResources.SaveOrAddFailed_ErrorMessage;
            }



            return result;
        }


        #endregion

        #region Total Visitor

        /// <summary>
        /// Get total visitor
        /// </summary>
        /// <returns>Facade result containing the total visitor object</returns>
        public static FacadeResult<TotalVisitor> GetTotalVisitor()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<TotalVisitor> result = new FacadeResult<TotalVisitor>();

            TotalVisitor totalVisitor = context.GetTotalVisitorByID(1);

            result.ReturnData = totalVisitor;
            result.IsSuccess = totalVisitor == null ? false : true;
            result.ErrorMessage = totalVisitor == null ? String.Format(TextResources.NotFound_ErrorMessage, "Total Visitor") : String.Empty;

            return result;
        }

        #endregion

        #region SystemSetting

        /// <summary>
        /// Get all active system settings
        /// </summary>
        /// <returns>Facade result containing the system setting object</returns>
        public static FacadeResult<IList<SystemSetting>> GetAllActiveSystemSettings()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<SystemSetting>> result = new FacadeResult<IList<SystemSetting>>();

            IList<SystemSetting> settings = context.GetAllActiveSystemSettings();

            result.ReturnData = settings;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        public static FacadeResult<SystemSetting> GetActiveSystemSettingByID(String systemSettingID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<SystemSetting> result = new FacadeResult<SystemSetting>();

            SystemSetting systemSetting = context.GetActiveSystemSettingByID(systemSettingID);

            result.ReturnData = systemSetting;
            result.IsSuccess = systemSetting == null ? false : true;
            result.ErrorMessage = systemSetting == null ? String.Format(TextResources.NotFound_ErrorMessage, "SystemSetting") : String.Empty;

            return result;
        }
        #endregion

        #endregion

        #region Action

        #region Group
        /// <summary>
        /// Add new group
        /// </summary>
        /// <param name="group">The group object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the added group</returns>
        public static FacadeResult<Group> AddNewGroup(Group group, int currentUserID)
        {

            FacadeResult<Group> result = new FacadeResult<Group>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    group.CreatedBy = currentUserID;
                    context.InsertGroup(group);

                    userActivity.UserActivityTypeID = "AddNewGroup";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = group.GroupID.ToString();
                    context.InsertUserActivity(userActivity);

                    result.ReturnData = group;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update group
        /// </summary>
        /// <param name="group">The group object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated group</returns>
        public static FacadeResult<Group> UpdateGroup(Group group, int currentUserID)
        {
            FacadeResult<Group> result = new FacadeResult<Group>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Group updateGroup = context.GetActiveGroupByID(group.GroupID.Value);
                    if (updateGroup != null)
                    {
                        updateGroup.Name = group.Name;
                        updateGroup.Description = group.Description;
                        updateGroup.LastUpdatedBy = currentUserID;
                        updateGroup.DateTimeUpdated = DateTime.Now;
                        context.UpdateGroup(updateGroup);

                        userActivity.UserActivityTypeID = "UpdateGroup";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = updateGroup.GroupID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updateGroup;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Group");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Delete group by id (set as inactive)
        /// </summary>
        /// <param name="groupID">int group id</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the deleted group</returns>
        public static FacadeResult<Group> DeleteGroupByID(int groupID, int currentUserID)
        {
            FacadeResult<Group> result = new FacadeResult<Group>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Group group = context.GetGroupByID(groupID);
                    if (group != null)
                    {
                        group.LastUpdatedBy = currentUserID;
                        group.DateTimeUpdated = DateTime.Now;
                        group.IsActive = false;
                        context.UpdateGroup(group);

                        userActivity.UserActivityTypeID = "DeleteGroup";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = groupID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = group;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Group");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }
        #endregion

        #region User Group Mapping
        /// <summary>
        /// Add new user group mapping
        /// </summary>
        /// <param name="UserGroupMapping">The user group mapping object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the added user group mapping</returns>
        public static FacadeResult<UserGroupMapping> AddNewUserGroupMapping(UserGroupMapping userGroupMapping, int currentUserID)
        {

            FacadeResult<UserGroupMapping> result = new FacadeResult<UserGroupMapping>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    userGroupMapping.CreatedBy = currentUserID;
                    context.InsertUserGroupMapping(userGroupMapping);

                    userActivity.UserActivityTypeID = "AddNewUserGroupMapping";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = userGroupMapping.GroupID.ToString();
                    context.InsertUserActivity(userActivity);

                    result.ReturnData = userGroupMapping;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update userGroupMapping
        /// </summary>
        /// <param name="userGroupMapping">The userGroupMapping object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated userGroupMapping</returns>
        public static FacadeResult<UserGroupMapping> UpdateUserGroupMapping(UserGroupMapping userGroupMapping, int currentUserID)
        {
            FacadeResult<UserGroupMapping> result = new FacadeResult<UserGroupMapping>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    UserGroupMapping updateUserGroupMapping = context.GetActiveUserGroupMappingByID(userGroupMapping.UserGroupMappingID.Value);
                    if (updateUserGroupMapping != null)
                    {
                        updateUserGroupMapping.UserID = userGroupMapping.UserID;
                        updateUserGroupMapping.GroupID = userGroupMapping.GroupID;
                        updateUserGroupMapping.LastUpdatedBy = currentUserID;
                        updateUserGroupMapping.DateTimeUpdated = DateTime.Now;
                        context.UpdateUserGroupMapping(updateUserGroupMapping);

                        userActivity.UserActivityTypeID = "UpdateUserGroupMapping";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = updateUserGroupMapping.UserGroupMappingID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updateUserGroupMapping;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "User Group Mapping");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Delete UserGroupMapping by id (set as inactive)
        /// </summary>
        /// <param name="UserGroupMappingID">int group id</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the deleted UserGroupMapping</returns>
        public static FacadeResult<UserGroupMapping> DeleteUserGroupMappingByID(int userGroupMappingID, int currentUserID)
        {
            FacadeResult<UserGroupMapping> result = new FacadeResult<UserGroupMapping>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    UserGroupMapping userGroupMapping = context.GetUserGroupMappingByID(userGroupMappingID);
                    if (userGroupMapping != null)
                    {
                        userGroupMapping.LastUpdatedBy = currentUserID;
                        userGroupMapping.DateTimeUpdated = DateTime.Now;
                        userGroupMapping.IsActive = false;
                        context.UpdateUserGroupMapping(userGroupMapping);

                        userActivity.UserActivityTypeID = "DeleteUserGroupMapping";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = userGroupMappingID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = userGroupMapping;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "User Group Mapping");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }
        #endregion

        #region User
        /// <summary>
        /// Add new user
        /// </summary>
        /// <param name="user">The user object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the added user</returns>
        public static FacadeResult<User> AddNewUser(User user, String password, int currentUserID)
        {
            FacadeResult<User> result = new FacadeResult<User>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;
            PasswordHistory passwordHistory = new PasswordHistory();

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    user.PasswordSalt = SecurityHelper.GenerateSaltValue();
                    user.HashPassword = SecurityHelper.Hash(password, user.PasswordSalt, HashingAlgorithm.SHA256);
                    user.CreatedBy = currentUserID;
                    user.FailedLoginAttempt = 0;
                    user.IsLocked = false;
                    user.LastPasswordChanged = null;
                    context.InsertUser(user);

                    passwordHistory.CreatedBy = currentUserID;
                    passwordHistory.HashPassword = user.HashPassword;
                    passwordHistory.UserID = user.UserID;
                    context.InsertPasswordHistory(passwordHistory);

                    userActivity.UserActivityTypeID = "AddNewUser";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = user.UserID.ToString();
                    context.InsertUserActivity(userActivity);

                    result.ReturnData = user;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user">The user object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated user</returns>
        public static FacadeResult<User> UpdateUser(User user, String password, int currentUserID)
        {
            FacadeResult<User> result = new FacadeResult<User>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;
            PasswordHistory passwordHistory = new PasswordHistory();

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    User updateUser = context.GetActiveUserByID(user.UserID.Value);
                    if (updateUser != null)
                    {
                        updateUser.Username = user.Username;
                        updateUser.FullName = user.FullName;
                        updateUser.Email = user.Email;
                        updateUser.BranchID = user.BranchID;
                        updateUser.LastUpdatedBy = currentUserID;
                        updateUser.DateTimeUpdated = DateTime.Now;
                        context.UpdateUser(updateUser);

                        userActivity.UserActivityTypeID = "UpdateUser";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = updateUser.UserID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updateUser;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;

                        if (!String.IsNullOrWhiteSpace(password))
                        {
                            //Change password
                            String hashPassword = SecurityHelper.Hash(password, updateUser.PasswordSalt, HashingAlgorithm.SHA256);

                            if (context.GetExistingPasswordHistoryCountByUserIDAndHashPassword(updateUser.UserID.Value, hashPassword) > 0)
                            {
                                result.ReturnData = null;
                                result.IsSuccess = false;
                                result.ErrorMessage = TextResources.PasswordComplexity_ErrorMessage;
                            }
                            else
                            {
                                //updateUser.PasswordSalt = SecurityHelper.GenerateSaltValue();
                                updateUser.HashPassword = hashPassword;
                                updateUser.LastPasswordChanged = DateTime.Now;
                                context.UpdateUser(updateUser);

                                //Delete the lowest password history if exceeds 4 (FIFO)
                                if (context.GetPasswordHistoryCountByUserID(updateUser.UserID.Value) >= 4)
                                {
                                    context.DeletePasswordHistoryByID(context.GetLowestPasswordHistoryIDByUserID(updateUser.UserID.Value));
                                }

                                passwordHistory.CreatedBy = currentUserID;
                                passwordHistory.HashPassword = updateUser.HashPassword;
                                passwordHistory.UserID = updateUser.UserID;
                                context.InsertPasswordHistory(passwordHistory);

                                userActivity = new UserActivity();
                                userActivity.UserActivityTypeID = "ChangePassword";
                                userActivity.ReferenceID = updateUser.UserID.Value.ToString();
                                userActivity.CreatedBy = currentUserID;
                                context.InsertUserActivity(userActivity);
                            }
                        }
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "User");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update user last activity date time
        /// </summary>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated user</returns>
        public static FacadeResult<User> UpdateUserLastActivity(int currentUserID)
        {
            FacadeResult<User> result = new FacadeResult<User>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    User updateUser = context.GetActiveUserByID(currentUserID);
                    if (updateUser != null)
                    {
                        updateUser.LastActivity = DateTime.Now;
                        context.UpdateUser(updateUser);

                        result.ReturnData = updateUser;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "User");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Delete user by id (set as inactive)
        /// </summary>
        /// <param name="userID">int user id</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the deleted user</returns>
        public static FacadeResult<User> DeleteUserByID(int userID, int currentUserID)
        {
            FacadeResult<User> result = new FacadeResult<User>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    User user = context.GetUserByID(userID);
                    if (user != null)
                    {
                        user.LastUpdatedBy = currentUserID;
                        user.DateTimeUpdated = DateTime.Now;
                        user.IsActive = false;
                        context.UpdateUser(user);

                        userActivity.UserActivityTypeID = "DeleteUser";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = userID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = user;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "User");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Unlock user by id
        /// </summary>
        /// <param name="userID">int user id</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the unlocked user</returns>
        public static FacadeResult<User> UnlockUserByID(int userID, int currentUserID)
        {
            FacadeResult<User> result = new FacadeResult<User>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    User user = context.GetUserByID(userID);
                    if (user != null)
                    {
                        if (user.IsLocked == true)
                        {
                            user.LastUpdatedBy = currentUserID;
                            user.DateTimeUpdated = DateTime.Now;
                            user.IsLocked = false;
                            user.FailedLoginAttempt = 0;
                            //Reset the last logged in
                            user.LastLoggedIn = null;
                            user.IsLoggedIn = false;
                            context.UpdateUser(user);

                            userActivity.UserActivityTypeID = "UnlockUser";
                            userActivity.CreatedBy = currentUserID;
                            userActivity.ReferenceID = userID.ToString();
                            context.InsertUserActivity(userActivity);

                            result.ReturnData = user;
                            result.IsSuccess = true;
                            result.ErrorMessage = String.Empty;
                        }
                        else
                        {
                            result.ReturnData = null;
                            result.IsSuccess = false;
                            result.ErrorMessage = TextResources.UnlockUnlockedUser_ErrorMessage;
                        }
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "User");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        #endregion

        #region Total Visitor

        /// <summary>
        /// Update total visitor
        /// </summary>        
        /// <returns>Facade result containing the success flag</returns>
        public static FacadeResult<bool> UpdateTotalVisitor()
        {
            FacadeResult<bool> result = new FacadeResult<bool>();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    context.UpdateTotalVisitorCount(1);

                    result.ReturnData = true;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = false;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        #endregion

        #region SystemSetting

        /// <summary>
        /// Update system settings
        /// </summary>
        /// <param name="systemSettings"></param>
        /// <param name="currentUserID"></param>
        /// <returns>List of system setting</returns>
        public static FacadeResult<IList<SystemSetting>> UpdateSystemSettings(IList<SystemSetting> systemSettings, int currentUserID)
        {
            FacadeResult<IList<SystemSetting>> result = new FacadeResult<IList<SystemSetting>>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;
            IList<SystemSetting> successSettings = new List<SystemSetting>();

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    foreach (var systemSetting in systemSettings)
                    {
                        SystemSetting updateSystemSetting = context.GetActiveSystemSettingByID(systemSetting.SystemSettingID);
                        if (updateSystemSetting != null)
                        {
                            updateSystemSetting.Value = systemSetting.Value;
                            updateSystemSetting.LastUpdatedBy = currentUserID;
                            updateSystemSetting.DateTimeUpdated = DateTime.Now;
                            context.UpdateSystemSetting(updateSystemSetting);

                            userActivity.UserActivityTypeID = "UpdateSystemSetting";
                            userActivity.CreatedBy = currentUserID;
                            userActivity.ReferenceID = systemSetting.SystemSettingID;
                            context.InsertUserActivity(userActivity);

                            successSettings.Add(updateSystemSetting);

                            result.IsSuccess = true;
                            result.ErrorMessage = String.Empty;
                        }
                        else
                        {
                            result.ReturnData = null;
                            result.IsSuccess = false;
                            result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "SystemSetting");
                        }
                    }

                    if (result.IsSuccess)
                    {
                        result.ReturnData = successSettings;
                        context.Commit();
                    }
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }
        #endregion

        #region Page Visit

        /// <summary>
        /// Add new page visit
        /// </summary>
        /// <param name="pageVisit">The page visit object</param>
        /// <returns>Facade result containing the added page visit</returns>
        public static FacadeResult<PageVisit> AddNewPageVisit(PageVisit pageVisit)
        {

            FacadeResult<PageVisit> result = new FacadeResult<PageVisit>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    context.InsertPageVisit(pageVisit);

                    /*userActivity.UserActivityTypeID = "AddNewPageVisit";
                    userActivity.ReferenceID = pageVisit.PageVisitID.ToString();
                    context.InsertUserActivity(userActivity);*/

                    result.ReturnData = pageVisit;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        #endregion

        #endregion
    }
}
