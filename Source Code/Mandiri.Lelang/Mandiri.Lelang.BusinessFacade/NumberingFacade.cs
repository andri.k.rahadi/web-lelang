﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insight.Database;
using Mandiri.Lelang.DataRepository;
using Mandiri.Lelang.DataModel;

namespace Mandiri.Lelang.BusinessFacade
{
    public class NumberingFacade : BaseFacade
    {
        /// <summary>
        /// Generate asset code with sequence
        /// </summary>
        /// <param name="segmentID">The segment id</param>
        /// <param name="branchID">The branch id</param>
        /// <returns>Generated asset code</returns>
        public static String GenerateAssetCode(String segmentID, int branchID)
        {
            int sequenceDigit = 4;
            int autoNumberingTypeID = 1;
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            //Kode alias/wilayan pengelola, kode segmen, tahun input, sequence
            String format = "{0}{1}{2}{3}";
            String assetCode = String.Empty;
            IList<long> runningNumbers;
            long runningNumber = 0;

            Segment segment = context.GetActiveSegmentByID(segmentID);
            Branch branch = context.GetActiveBranchByID(branchID);

            runningNumbers = context.QuerySql<long>("UPDATE AutoNumbering SET RunningNumber = RunningNumber + 1 OUTPUT INSERTED.RunningNumber WHERE AutoNumberingTypeID = @AutoNumberingTypeID AND SegmentID = @SegmentID AND BranchID = @BranchID",
                parameters: new { AutoNumberingTypeID = autoNumberingTypeID, SegmentID = segmentID, BranchID = branchID });

            if (runningNumbers.Count > 0)
            {
                runningNumber = runningNumbers[0];
            }
            assetCode = String.Format(format, branch.Alias.ToUpper(), segment.SegmentID.PadLeft(2, '0'), DateTime.Today.ToString("yy"), runningNumber.ToString().PadLeft(sequenceDigit, '0'));

            return assetCode;
        }
    }
}
