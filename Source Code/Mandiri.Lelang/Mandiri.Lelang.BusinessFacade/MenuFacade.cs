﻿using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.DataRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.BusinessFacade
{
    /// <summary>
    /// Facade for generating menu
    /// </summary>
    public class MenuFacade : BaseFacade
    {
        /// <summary>
        /// Get all active menus by level
        /// </summary>
        /// <param name="level">The level parameter</param>
        /// <returns>Facade result containing the active menus</returns>
        public static FacadeResult<IList<SiteMap>> GetAllActiveMenusByLevel(int level)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<SiteMap>> result = new FacadeResult<IList<SiteMap>>();

            result.ReturnData = context.GetAllActiveMenusByLevel(level);
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all child menus by parent id
        /// </summary>
        /// <param name="siteMapParentID">The parent id parameter</param>
        /// <returns>Facade result containing the active menus</returns>
        public static FacadeResult<IList<SiteMap>> GetAllChildMenusBySiteMapParentID(String siteMapParentID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<SiteMap>> result = new FacadeResult<IList<SiteMap>>();

            result.ReturnData = context.GetAllChildMenusBySiteMapParentID(siteMapParentID);
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all child menus by multiple parent id
        /// </summary>
        /// <param name="siteMapParentID">The multiple parent id parameter</param>
        /// <returns>Facade result containing the active menus</returns>
        public static FacadeResult<IList<SiteMap>> GetAllChildMenusBySiteMapParentIDs(IList<String> siteMapParentIDs)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<SiteMap>> result = new FacadeResult<IList<SiteMap>>();

            result.ReturnData = context.GetAllChildMenusBySiteMapParentID(siteMapParentIDs);
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all menus that are allowed for access for this user
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static FacadeResult<IList<SiteMap>> GetAllowedAccessMenusByUserID(int userID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<SiteMap>> result = new FacadeResult<IList<SiteMap>>();

            result.ReturnData = context.GetAllowedAccessMenusByUserID(userID);
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all menus that are allowed for delete for this user
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static FacadeResult<IList<SiteMap>> GetAllowedDeleteMenusByUserID(int userID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<SiteMap>> result = new FacadeResult<IList<SiteMap>>();

            result.ReturnData = context.GetAllowedDeleteMenusByUserID(userID);
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all menus that are allowed for add for this user
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static FacadeResult<IList<SiteMap>> GetAllowedAddMenusByUserID(int userID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<SiteMap>> result = new FacadeResult<IList<SiteMap>>();

            result.ReturnData = context.GetAllowedAddMenusByUserID(userID);
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all menus that are allowed for update for this user
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static FacadeResult<IList<SiteMap>> GetAllowedUpdateMenusByUserID(int userID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<SiteMap>> result = new FacadeResult<IList<SiteMap>>();

            result.ReturnData = context.GetAllowedUpdateMenusByUserID(userID);
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
    }
}
