﻿using Mandiri.Lelang.DataRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insight.Database;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.Helper;
using System.IO;
using System.Configuration;
using Mandiri.Lelang.Helper.Enum;
using System.Web.Helpers;

namespace Mandiri.Lelang.BusinessFacade
{
    public partial class AssetFacade : BaseFacade
    {
        #region Get

        /// <summary>
        /// Get all active asset list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the asset object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveAssetsList(int? branchID, String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection, int updatePrice = 0)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            //String[] sortColumnNames = new String[] { "A.AssetID", "AssetCode", "A.SegmentID", "A.CategoryID", "C.Name", "A.Address", "P.Name", "CT.Name", "OldPrice", "NewPrice", "Photo1", "AU.Name", "A.AdditionalNotes" };
            String[] sortColumnNames = new String[] { "A.AssetID", "AssetCode", "A.SegmentID", "A.CategoryID", "C.Name", "P.Name", "CT.Name", "OldPrice", "NewPrice", "Photo1", "AU.Name", "A.AdditionalNotes", "Type", "Price" };

            IList<dynamic> assets = context.QuerySql<dynamic>(
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                A.AssetID, AssetCode,A.SegmentID,SG.Name AS SegmentName, A.CategoryID, C.Name AS CategoryName, A.Address, A.ProvinceID, P.Name AS ProvinceName, A.CityID,
                CT.Name AS CityName, OldPrice, NewPrice, Photo1 , AU.Name AS Status, A.AdditionalNotes, A.DateTimeCreated,
                Case when A.Type = '1' THEN
                        'Jual Sukarela'
			        when A.Type ='2' THEN
					    'Lelang'
					ELSE '' end as Type,
					ISNULL(A.Price,0) AS Price
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN AssetStatus AU ON A.CurrentStatusID = AU.AssetStatusID 
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                INNER JOIN Segment SG ON A.SegmentID = SG.SegmentID
                LEFT JOIN AuctionSchedule S ON A.AuctionScheduleID = S.AuctionScheduleID
                WHERE A.IsActive = 1 
                AND (A.BranchID = @BranchID OR @BranchID = -1) 
                AND (AssetCode LIKE @Keywords OR CT.Name LIKE @Keywords OR C.Name LIKE @Keywords OR A.Address LIKE @Keywords)
                AND (@UpdatePrice = 0 OR (@UpdatePrice = 1 AND DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) > DATEADD(dd, 0, DATEDIFF(dd, 0, S.ScheduleDate)) AND A.CurrentStatusID IN (4) AND (A.NewPrice = 0 OR A.NewPrice IS NULL)))) TBL
                WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortDirection)
                , new
                {
                    BranchID = branchID,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()),
                    UpdatePrice = updatePrice
                });
            result.ReturnData = assets;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active assets count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the assets count</returns>
        public static FacadeResult<long> GetActiveAssetsCountByKeywords(int branchID, String keywords, int updatePrice = 0)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(AssetID) FROM (SELECT 
                A.AssetID, AssetCode,A.SegmentID,SG.Name AS SegmentName, A.CategoryID, C.Name AS CategoryName, A.Address, A.ProvinceID, P.Name AS ProvinceName, A.CityID,
                CT.Name AS CityName, OldPrice, NewPrice, Photo1 , A.CurrentStatusID, A.AdditionalNotes, A.DateTimeCreated
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN AssetStatus AU ON A.CurrentStatusID = AU.AssetStatusID 
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                INNER JOIN Segment SG ON A.SegmentID = SG.SegmentID
                LEFT JOIN AuctionSchedule S ON A.AuctionScheduleID = S.AuctionScheduleID
                WHERE A.IsActive = 1 
                AND (A.BranchID = @BranchID OR @BranchID = -1) 
                AND (AssetCode LIKE @Keywords OR CT.Name LIKE @Keywords OR C.Name LIKE @Keywords OR A.Address LIKE @Keywords)
                AND (@UpdatePrice = 0 OR (@UpdatePrice = 1 AND DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) > DATEADD(dd, 0, DATEDIFF(dd, 0, S.ScheduleDate)) AND A.CurrentStatusID IN (4) AND (A.NewPrice = 0 OR A.NewPrice IS NULL)))) TBL",
                new { BranchID = branchID, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()), UpdatePrice = updatePrice });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active assets count
        /// </summary>
        /// <returns>Facade result containing the assets count</returns>
        public static FacadeResult<long> GetActiveAssetsCount(int branchID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveAssetsCount(branchID);

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assetID"></param>
        /// <returns></returns>
        public static FacadeResult<Asset> GetActiveAssetByAssetID(long assetID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<Asset> result = new FacadeResult<Asset>();

            Asset asset = context.GetActiveAssetByID(assetID);

            result.ReturnData = asset;
            result.IsSuccess = asset == null ? false : true;
            result.ErrorMessage = asset == null ? String.Format(TextResources.NotFound_ErrorMessage, "Asset") : String.Empty;

            return result;
        }

        /// <summary>
        /// Acution schedule By schedule ID
        /// </summary>
        /// <param name="auctionScheduleID"></param>
        /// <returns></returns>
        public static FacadeResult<AuctionSchedule> GetActiveAuctionScheduleByAuctionScheduleID(long auctionScheduleID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<AuctionSchedule> result = new FacadeResult<AuctionSchedule>();
            AuctionSchedule auctionSchedule = context.GetActiveAuctionScheduleByAuctionScheduleID(auctionScheduleID);
            if (auctionSchedule != null)
            {
                result.ReturnData = auctionSchedule;
            }

            return result;
        }

        /// <summary>
        /// get all active auction schedule
        /// </summary>
        /// <returns>dynamic object </returns>
        public static FacadeResult<IList<dynamic>> GetAllActiveAuctionSchedules()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();

            IList<dynamic> auctionSchedule = context.QuerySql<dynamic>(@"SELECT AuctionScheduleID, AU.KPKNLID, AU.Timezone As TimezoneID, 
                            K.Name ,  ScheduleDate, ScheduleTime, AU.AuctionAddress, AU.Phone1, AU.Phone2,
	                            CASE 
		                            WHEN AU.Timezone = 1 THEN 'WIB' 
		                            WHEN AU.Timezone = 2 THEN 'WIT' 
		                            WHEN AU.Timezone = 3 THEN 'WITA' 
	                            END AS Timezone
                            FROM AuctionSchedule AU INNER JOIN KPKNL K ON AU.KPKNLID = K.KPKNLID
                            WHERE AU.IsActive = 1 AND CAST(ScheduleDate AS DATE) >=  CAST(GETDATE() AS DATE)", null);



            result.ReturnData = auctionSchedule;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Auction schedules
        /// </summary>
        /// <param name="auctionScheduleID"></param>
        /// <returns></returns>
        public static FacadeResult<IList<dynamic>> GetAllActiveAuctionSchedulesByID(int auctionScheduleID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();

            IList<dynamic> auctionSchedule = context.QuerySql<dynamic>(@"SELECT AuctionScheduleID, AU.KPKNLID, AU.Timezone As TimezoneID, 
                            K.Name ,  ScheduleDate, ScheduleTime, AU.AuctionAddress, AU.Phone1, AU.Phone2,
	                            CASE 
		                            WHEN AU.Timezone = 1 THEN 'WIB' 
		                            WHEN AU.Timezone = 2 THEN 'WIT' 
		                            WHEN AU.Timezone = 3 THEN 'WITA' 
	                            END AS Timezone
                            FROM AuctionSchedule AU INNER JOIN KPKNL K ON AU.KPKNLID = K.KPKNLID
                            WHERE AU.IsActive = 1 AND CAST(ScheduleDate AS DATE) >= CAST(GETDATE() AS DATE) AND AuctionScheduleID = @auctionScheduleID", new { AuctionScheduleID = auctionScheduleID });



            result.ReturnData = auctionSchedule;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Asset changes set null for approval status if want to get the 0 status 
        /// </summary>
        /// <param name="assetID"></param>
        /// <returns></returns>
        public static FacadeResult<AssetChanges> GetActiveAssetChangeByAssetIDAndApprovalStatus(long? assetID, int? approvalStatus)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<AssetChanges> result = new FacadeResult<AssetChanges>();

            AssetChanges asset = context.GetActiveAssetChangeByAssetIDAndApprovalStatus(assetID, approvalStatus.Value);

            result.ReturnData = asset;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;

        }

        /// <summary>
        /// Get active asset change and current approval status = 0
        /// </summary>
        /// <param name="assetID"></param>
        /// <returns></returns>
        public static FacadeResult<AssetChanges> GetActiveAssetChangeByAssetID(long? assetID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<AssetChanges> result = new FacadeResult<AssetChanges>();

            AssetChanges asset = context.GetActiveAssetChangeByAssetIDAndApprovalStatus(assetID, 0);

            result.ReturnData = asset;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;

        }

        /// <summary>
        /// Asset Changes History
        /// </summary>
        /// <param name="assetID"></param>
        /// <returns></returns>
        public static FacadeResult<IList<dynamic>> GetAllActiveAssetChangeHistoryByAssetID(long assetID)
        {

            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();

            IList<dynamic> assetChanges = context.QuerySql<dynamic>(
                @"SELECT AC.AssetID, A.AssetCode, IsNew, IsUpdated, IsDeleted, UA.Username AS UserAdmin,
                AC.DateTimeCreated AS [Date], 
                CASE 
	                WHEN W.IsApproved IS NULL AND W.IsRejected IS NULL THEN '' 
	                WHEN W.IsApproved = 1 THEN 'Approve'
	                WHEN W.IsRejected = 1 THEN 'Reject' 
                END AS ApproveOrReject,
                CASE 
	                WHEN W.IsApproved IS NULL AND W.IsRejected IS NULL THEN '' 
	                WHEN W.IsApproved = 1 THEN UAP.Username
	                WHEN W.IsRejected = 1 THEN UAR.Username 
                END AS ApproveOrRejectBy,
                W.DateTimeCreated AS ApproveOrRejectDate, ISNULL(W.Reason,'') AS ApproveReason
                FROM AssetChanges AC JOIN Asset A ON A.AssetID = AC.AssetID
                LEFT JOIN AssetWorkflowHistory W ON AC.AssetChangesID = W.AssetChangesID
                JOIN [User] UA ON AC.CreatedBy = UA.UserID
                LEFT JOIN [User] UAP ON W.ApprovedBy = UAP.UserID
                LEFT JOIN [User] UAR ON W.RejectedBy = UAR.UserID
                WHERE A.IsActive = 1 AND AC.IsActive = 1 AND W.IsActive = 1 AND A.AssetID = @assetID
                ORDER BY AC.DateTimeCreated DESC, W.DateTimeCreated ASC", new { AssetID = assetID });

            result.ReturnData = assetChanges;
            result.IsSuccess = assetChanges == null ? false : true;
            result.ErrorMessage = assetChanges == null ? String.Format(TextResources.NotFound_ErrorMessage, "AssetChangeList") : String.Empty;

            return result;

        }

        /// <summary>
        /// get count of asset change history
        /// </summary>
        /// <param name="assetID"></param>
        /// <returns></returns>
        public static FacadeResult<long> GetAllActiveAssetChangeHistoryByAssetIDAndKeywordsCount(long assetID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(AssetID) FROM (SELECT AC.AssetID, A.AssetCode, IsNew, IsUpdated, IsDeleted, UA.Username AS UserAdmin,
                AC.DateTimeCreated AS [Date], 
                CASE 
	                WHEN W.IsApproved IS NULL AND W.IsRejected IS NULL THEN '' 
	                WHEN W.IsApproved = 1 THEN 'Approve'
	                WHEN W.IsRejected = 1 THEN 'Reject' 
                END AS ApproveOrReject,
                CASE 
	                WHEN W.IsApproved IS NULL AND W.IsRejected IS NULL THEN '' 
	                WHEN W.IsApproved = 1 THEN UAP.Username
	                WHEN W.IsRejected = 1 THEN UAR.Username 
                END AS ApproveOrRejectBy,
                W.DateTimeCreated AS ApproveOrRejectDate, ISNULL(W.Reason,'') AS ApproveReason
                FROM AssetChanges AC JOIN Asset A ON A.AssetID = AC.AssetID
                LEFT JOIN AssetWorkflowHistory W ON AC.AssetChangesID = W.AssetChangesID
                JOIN [User] UA ON AC.CreatedBy = UA.UserID
                LEFT JOIN [User] UAP ON W.ApprovedBy = UAP.UserID
                LEFT JOIN [User] UAR ON W.RejectedBy = UAR.UserID
                WHERE A.IsActive = 1 AND AC.IsActive = 1 AND W.IsActive = 1 AND A.AssetID = @assetID ) TBL", new { AssetID = assetID });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get asset change history count
        /// </summary>
        /// <param name="assetID"></param>
        /// <returns></returns>
        public static FacadeResult<long> GetAllActiveAssetChangeHistoryCount(long assetID)
        {

            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveAssetAssetChangeHistoryCount(assetID);

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;

        }

        /// <summary>
        /// Get active asset changes list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        public static FacadeResult<IList<dynamic>> GetActiveAssetChangesList(int branchID, int userType, String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "AC.AssetChangesID", "AC.AssetID", "A.AssetCode", "S.Name", "C.Name", "P.Name", "CT.Name", "A.AdditionalNotes", "A.Photo1", "A.Photo2", "A.Photo3", "A.Photo4", "A.Photo5", "A.CurrentStatusID", "CurrentApprovalStatus" };

            IList<dynamic> assets = new List<dynamic>();

            if (userType == 0)
            {

                assets = context.QuerySql<dynamic>(
                    String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER, 
                C.Name As Category, AssetChangesID, AC.AssetID, IsNew, IsUpdated, IsDeleted, CurrentApprovalStatus, OriginalAssetStatusID, A.AssetCode, 
                CT.Name AS City, P.Name AS Province , A.Photo1, S.Name AS Segment, A.AdditionalNotes, A.CurrentStatusID
                FROM AssetChanges AC 
                INNER JOIN Asset A ON A.AssetID = AC.AssetID
                INNER JOIN Category C ON A.CategoryID = C.CategoryID
                INNER JOIN Province P ON A.ProvinceID = P.ProvinceID
                INNER JOIN City CT ON A.CityID =  CT.CityID 
                INNER JOIN Segment S ON A.SegmentID = S.SegmentID
                WHERE AC.IsActive = 1 AND CurrentApprovalStatus = @UserType AND A.CurrentStatusID >= 2
                AND ( A.BranchID = @BranchID OR @BranchID = -1)
                AND (A.AssetCode LIKE @Keywords OR P.Name LIKE @Keywords OR CT.Name LIKE @Keywords OR C.Name LIKE @Keywords OR A.CurrentStatusID LIKE @Keywords )) TBL
                WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                    , sortColumnNames[sortIndex], sortDirection)
                    , new
                    {
                        BranchID = branchID,
                        UserType = userType,
                        PageIndex = pageIndex,
                        PageSize = pageSize,
                        Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim())
                    });
            }
            else
            {
                assets = context.QuerySql<dynamic>(
                    String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER, 
                C.Name As Category, AssetChangesID, AC.AssetID, IsNew, IsUpdated, IsDeleted, CurrentApprovalStatus, OriginalAssetStatusID, A.AssetCode, 
                CT.Name AS City, P.Name AS Province , A.Photo1, S.Name AS Segment, A.AdditionalNotes, A.CurrentStatusID
                FROM AssetChanges AC 
                INNER JOIN Asset A ON A.AssetID = AC.AssetID
                INNER JOIN Category C ON A.CategoryID = C.CategoryID
                INNER JOIN Province P ON A.ProvinceID = P.ProvinceID
                INNER JOIN City CT ON A.CityID =  CT.CityID 
                INNER JOIN Segment S ON A.SegmentID = S.SegmentID
                WHERE AC.IsActive = 1 AND CurrentApprovalStatus = @UserType 
                AND ( A.BranchID = @BranchID OR @BranchID = -1)
                AND (A.AssetCode LIKE @Keywords OR P.Name LIKE @Keywords OR CT.Name LIKE @Keywords OR C.Name LIKE @Keywords OR A.CurrentStatusID LIKE @Keywords )) TBL
                WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                    , sortColumnNames[sortIndex], sortDirection)
                    , new
                    {
                        BranchID = branchID,
                        UserType = userType,
                        PageIndex = pageIndex,
                        PageSize = pageSize,
                        Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim())
                    });
            }
            result.ReturnData = assets;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get asset change count
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public static FacadeResult<long> GetActiveAssetChangesCountByKeywords(int branchID, int userType, String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(@"SELECT COUNT_BIG(AssetChangesID) FROM (SELECT AssetChangesID, AC.AssetID, IsNew, IsUpdated, IsDeleted, CurrentApprovalStatus, OriginalAssetStatusID, 
                C.Name As Category, A.AssetCode, CT.Name AS City, P.Name AS Province , A.Photo1, S.Name AS Segment, A.AdditionalNotes, A.CurrentStatusID
                FROM AssetChanges AC 
                INNER JOIN Asset A ON A.AssetID = AC.AssetID
                INNER JOIN Category C ON A.CategoryID = C.CategoryID
                INNER JOIN Province P ON A.ProvinceID = P.ProvinceID
                INNER JOIN City CT ON A.CityID =  CT.CityID 
                INNER JOIN Segment S ON A.SegmentID = S.SegmentID
                WHERE AC.IsActive = 1 AND CurrentApprovalStatus = @UserType 
                AND ( A.BranchID = @BranchID OR @BranchID = -1)
                AND (A.AssetCode LIKE @Keywords OR P.Name LIKE @Keywords OR CT.Name LIKE @Keywords OR C.Name LIKE @Keywords OR A.CurrentStatusID LIKE @Keywords )) TBL",
                new { BranchID = branchID, UserType = userType, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// get asset change count
        /// </summary>
        /// <returns></returns>
        public static FacadeResult<long> GetActiveAssetChangesCount(int branchID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveAssetChangesCount(branchID);

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get ActiveAsset Changes ForInputerCount
        /// </summary>
        /// <returns></returns>
        public static FacadeResult<long> GetActiveAssetChangesForInputterCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveAssetChangesForInputterCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get active asset changes list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        public static FacadeResult<IList<dynamic>> GetActiveAssetChangesForInputerAndApproverList(int branchID, String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "AC.AssetID", "A.AssetCode", "S.Name", "C.Name", "P.Name", "CT.Name", "A.AdditionalNotes", "A.Photo1", "A.CurrentStatusID", "CurrentApprovalStatus" };


            IList<dynamic> assets = context.QuerySql<dynamic>(
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER, AssetChangesID, AC.AssetID, IsNew, IsUpdated, IsDeleted, CurrentApprovalStatus, OriginalAssetStatusID,
                C.Name As Category, A.AssetCode, CT.Name AS City, P.Name AS Province , A.Photo1, S.Name AS Segment, A.AdditionalNotes, A.CurrentStatusID
                FROM AssetChanges AC 
                INNER JOIN Asset A ON A.AssetID = AC.AssetID
                INNER JOIN Category C ON A.CategoryID = C.CategoryID
                INNER JOIN Province P ON A.ProvinceID = P.ProvinceID
                INNER JOIN City CT ON A.CityID =  CT.CityID 
                INNER JOIN Segment S ON A.SegmentID = S.SegmentID
                WHERE AC.IsActive = 1 AND CurrentApprovalStatus in (0, 2) 
                AND ( A.BranchID = @BranchID OR @BranchID = -1)
                AND (A.AssetCode LIKE @Keywords OR P.Name LIKE @Keywords OR CT.Name LIKE @Keywords OR C.Name LIKE @Keywords OR A.CurrentStatusID LIKE @Keywords)) TBL
                WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortDirection)
                , new
                {
                    BranchID = branchID,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim())
                });
            result.ReturnData = assets;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get asset change count
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public static FacadeResult<long> GetActiveAssetChangesForInputerAndApproverCountByKeywords(int branchID, String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(@"SELECT COUNT_BIG(AssetChangesID) FROM (SELECT AssetChangesID, AC.AssetID, IsNew, IsUpdated, IsDeleted, CurrentApprovalStatus, OriginalAssetStatusID,
                C.Name As Category, A.AssetCode, CT.Name AS City, P.Name AS Province , A.Photo1, S.Name AS Segment, A.AdditionalNotes, A.CurrentStatusID
                FROM AssetChanges AC 
                INNER JOIN Asset A ON A.AssetID = AC.AssetID
                INNER JOIN Category C ON A.CategoryID = C.CategoryID
                INNER JOIN Province P ON A.ProvinceID = P.ProvinceID
                INNER JOIN City CT ON A.CityID =  CT.CityID 
                INNER JOIN Segment S ON A.SegmentID = S.SegmentID
                WHERE AC.IsActive = 1 AND CurrentApprovalStatus in (0, 2) 
                AND ( A.BranchID = @BranchID OR @BranchID = -1)
                AND (A.AssetCode LIKE @Keywords OR P.Name LIKE @Keywords OR CT.Name LIKE @Keywords OR C.Name LIKE @Keywords OR A.CurrentStatusID LIKE @Keywords )) TBL",
                new { BranchID = branchID, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get Active AssetChangesFor Inputter And Approval Count
        /// </summary>
        /// <returns></returns>
        public static FacadeResult<long> GetActiveAssetChangesForInputterAndApproverCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveAssetChangesForInputterAndApproverCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="videoID"></param>
        /// <returns></returns>
        public static FacadeResult<Video> GetActiveVideoByVideoID(int videoID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<Video> result = new FacadeResult<Video>();

            Video video = context.GetActiveVideoByID(videoID);

            result.ReturnData = video;
            result.IsSuccess = video == null ? false : true;
            result.ErrorMessage = video == null ? String.Format(TextResources.NotFound_ErrorMessage, "Asset") : String.Empty;

            return result;
        }

        #endregion

        #region Action
        /// <summary>
        /// Add new asset
        /// </summary>
        /// <param name="asset">The asset object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the added asset</returns>
        public static FacadeResult<Asset> AddNewAsset(Asset asset, AuctionSchedule auctionSchedule, IList<AssetCategoryTemplateValue> listCategoryTemplateValues, int currentUserID)
        {

            FacadeResult<Asset> result = new FacadeResult<Asset>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;
            bool duplicateSchedule = false;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    // Inserting Auction Schedule
                    if (asset.Type == 2)
                    {
                        if (auctionSchedule != null)
                        {
                            AuctionSchedule existingSchedule = context.GetActiveAuctionScheduleBySchedule(auctionSchedule.ScheduleDate.Value, auctionSchedule.ScheduleTime.Value, auctionSchedule.Timezone.Value, auctionSchedule.KPKNLID.Value);

                            if (existingSchedule == null)
                            {
                                auctionSchedule.CreatedBy = currentUserID;
                                auctionSchedule.IsActive = true;
                                context.InsertAuctionSchedule(auctionSchedule);

                                asset.AuctionScheduleID = auctionSchedule.AuctionScheduleID;
                            }
                            else
                            {
                                duplicateSchedule = true;
                            }
                        }
                    }

                    if (!duplicateSchedule)
                    {
                        asset.CreatedBy = currentUserID;
                        asset.CurrentStatusID = 1; // Save As Draft
                        if (asset.Type == 1)
                        {
                            asset.OldPrice = 0;
                            asset.NewPrice = 0;
                            asset.AuctionScheduleID = null;
                            asset.EmailAuctionURL = null;
                            asset.IsEmailAuction = false;
                        }
                        else if (asset.Type == 2)
                        {
                            asset.Price = 0;
                            asset.ExpiredDate = null;
                        }
                        context.InsertAsset(asset);

                        // Rename The Photo 
                        String photo1 = SessionHelper.GetValue<String>(SessionKey.AssetPhoto1);
                        String photo2 = SessionHelper.GetValue<String>(SessionKey.AssetPhoto2);
                        String photo3 = SessionHelper.GetValue<String>(SessionKey.AssetPhoto3);
                        String photo4 = SessionHelper.GetValue<String>(SessionKey.AssetPhoto4);
                        String photo5 = SessionHelper.GetValue<String>(SessionKey.AssetPhoto5);

                        if (!String.IsNullOrEmpty(photo1))
                        {
                            asset.Photo1 = RenameUploadedFile(photo1, asset.AssetID.ToString(), SessionKey.AssetPhoto1);
                        }
                        if (!String.IsNullOrEmpty(photo2))
                        {
                            asset.Photo2 = RenameUploadedFile(photo2, asset.AssetID.ToString(), SessionKey.AssetPhoto2);
                        }
                        if (!String.IsNullOrEmpty(photo3))
                        {
                            asset.Photo3 = RenameUploadedFile(photo3, asset.AssetID.ToString(), SessionKey.AssetPhoto3);
                        }
                        if (!String.IsNullOrEmpty(photo4))
                        {
                            asset.Photo4 = RenameUploadedFile(photo4, asset.AssetID.ToString(), SessionKey.AssetPhoto4);
                        }
                        if (!String.IsNullOrEmpty(photo5))
                        {
                            asset.Photo5 = RenameUploadedFile(photo5, asset.AssetID.ToString(), SessionKey.AssetPhoto5);
                        }

                        context.UpdateAsset(asset);

                        ClearPhotoSession();

                        userActivity.UserActivityTypeID = "AddNewAsset";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = asset.AssetID.ToString();
                        context.InsertUserActivity(userActivity);

                        if (listCategoryTemplateValues != null)
                        {
                            foreach (AssetCategoryTemplateValue template in listCategoryTemplateValues)
                            {
                                context.DeleteAssetCategoryTemplateValueByID(asset.AssetID.Value, template.FieldName);

                                AssetCategoryTemplateValue templateToUpdate = new AssetCategoryTemplateValue();
                                templateToUpdate.AssetID = asset.AssetID;
                                templateToUpdate.FieldName = template.FieldName;
                                templateToUpdate.Value = template.Value;
                                templateToUpdate.IsActive = true;
                                templateToUpdate.DateTimeCreated = DateTime.Now;
                                templateToUpdate.CreatedBy = currentUserID;
                                context.InsertAssetCategoryTemplateValue(templateToUpdate);
                            }
                        }

                        IList<AssetChangesHistory> listHistory = new List<AssetChangesHistory>();

                        // Set To AssetChange 
                        AssetChanges changes = GetActiveAssetChangeByAssetID(asset.AssetID).ReturnData;
                        if (changes == null)
                        {
                            changes = new AssetChanges();
                            changes.AssetID = asset.AssetID;
                            changes.IsNew = true;
                            changes.IsDeleted = false;
                            changes.IsUpdated = false;
                            changes.IsActive = true;
                            changes.CurrentApprovalStatus = 0;
                            changes.OriginalAssetStatusID = 1;
                            changes.CreatedBy = currentUserID;
                            changes.DateTimeCreated = DateTime.Now;
                            context.InsertAssetChanges(changes);

                            listHistory = CompareAssetForChangesHistory(asset, changes.AssetChangesID.Value, true, context);

                            if (listHistory.Count > 0)
                            {
                                foreach (AssetChangesHistory changesHistory in listHistory)
                                {
                                    context.InsertAssetChangesHistory(changesHistory);
                                }
                            }

                        }

                        result.ReturnData = asset;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = TextResources.DuplicateSchedule_ErrorMessage;
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update asset
        /// </summary>
        /// <param name="group">The asset object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated asset</returns>
        public static FacadeResult<Asset> UpdateAsset(Asset asset, int approvalType, AuctionSchedule auctionSchedule, IList<AssetCategoryTemplateValue> listCategoryTemplateValues, int currentUserID, int buttonType)
        {
            FacadeResult<Asset> result = new FacadeResult<Asset>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;


            IList<AssetChangesHistory> listChangesHistory = new List<AssetChangesHistory>(); // List for changes
            int tmpOriginalAssetStatus = 0;
            bool duplicateSchedule = false;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {

                    Asset updateAsset = context.GetActiveAssetByID((int)asset.AssetID.Value);

                    if (updateAsset != null)
                    {
                        //Fix lunas/terjual to live issue, add validation
                        //By Ricardo Alexander
                        //Date 3 July 2015
                        if (updateAsset.CurrentStatusID != 5 && updateAsset.CurrentStatusID != 7)
                        {
                            // Inserting Auction Schedule
                            if (asset.Type == 2)
                            {
                                if (auctionSchedule != null)
                                {
                                    AuctionSchedule existingSchedule = context.GetActiveAuctionScheduleBySchedule(auctionSchedule.ScheduleDate.Value, auctionSchedule.ScheduleTime.Value, auctionSchedule.Timezone.Value, auctionSchedule.KPKNLID.Value);

                                    if (existingSchedule == null)
                                    {
                                        auctionSchedule.CreatedBy = currentUserID;
                                        auctionSchedule.IsActive = true;
                                        context.InsertAuctionSchedule(auctionSchedule);

                                        updateAsset.AuctionScheduleID = auctionSchedule.AuctionScheduleID;
                                    }
                                    else
                                    {
                                        duplicateSchedule = true;
                                    }
                                }
                                else
                                {
                                    updateAsset.AuctionScheduleID = asset.AuctionScheduleID;
                                }
                            }

                            if (!duplicateSchedule)
                            {
                                updateAsset.AssetID = asset.AssetID;
                                updateAsset.AssetCode = asset.AssetCode;
                                updateAsset.AccountNumber = asset.AccountNumber;
                                updateAsset.AccountName = asset.AccountName;
                                updateAsset.CategoryID = asset.CategoryID;
                                updateAsset.Address = asset.Address;
                                updateAsset.ProvinceID = asset.ProvinceID;
                                updateAsset.CityID = asset.CityID;
                                updateAsset.OldPrice = asset.OldPrice;
                                updateAsset.NewPrice = asset.NewPrice;
                                updateAsset.BranchID = asset.BranchID;
                                updateAsset.MarketValue = asset.MarketValue.HasValue ? asset.MarketValue.Value : 0;
                                updateAsset.LiquidationValue = asset.LiquidationValue.HasValue ? asset.LiquidationValue.Value : 0;
                                updateAsset.SecurityRightsValue = asset.SecurityRightsValue.HasValue ? asset.SecurityRightsValue.Value : 0;
                                updateAsset.ValuationDate = asset.ValuationDate;
                                updateAsset.SoldValue = asset.SoldValue;
                                updateAsset.KPKNLID = asset.KPKNLID;
                                updateAsset.KPKNLAddress = asset.KPKNLAddress;
                                updateAsset.AuctionAddress = asset.AuctionAddress;
                                updateAsset.IsEmailAuction = asset.IsEmailAuction;
                                updateAsset.EmailAuctionURL = asset.EmailAuctionURL;
                                updateAsset.KPKNLPhone1 = asset.KPKNLPhone1;
                                updateAsset.KPKNLPhone2 = asset.KPKNLPhone2;
                                updateAsset.AuctionHallID = asset.AuctionHallID;
                                updateAsset.AuctionHallAddress = asset.AuctionHallAddress;
                                updateAsset.AuctionHallPhone1 = asset.AuctionHallPhone1;
                                updateAsset.AuctionHallPhone2 = asset.AuctionHallPhone2;
                                updateAsset.SegmentID = asset.SegmentID;
                                updateAsset.OwnershipDocumentTypeID = asset.OwnershipDocumentTypeID;
                                updateAsset.DocumentNumber = asset.DocumentNumber;
                                updateAsset.Latitude = asset.Latitude;
                                updateAsset.Longitude = asset.Longitude;
                                updateAsset.TransferInformation = asset.TransferInformation;
                                updateAsset.AdditionalNotes = asset.AdditionalNotes;                                
                                // Rename The Photo 
                                String photo1 = SessionHelper.GetValue<String>(SessionKey.AssetPhoto1);
                                String photo2 = SessionHelper.GetValue<String>(SessionKey.AssetPhoto2);
                                String photo3 = SessionHelper.GetValue<String>(SessionKey.AssetPhoto3);
                                String photo4 = SessionHelper.GetValue<String>(SessionKey.AssetPhoto4);
                                String photo5 = SessionHelper.GetValue<String>(SessionKey.AssetPhoto5);
                                updateAsset.Type = asset.Type;
                                updateAsset.Price = asset.Price;
                                updateAsset.ExpiredDate = asset.ExpiredDate;
                                if (updateAsset.Type == 1)
                                {
                                    updateAsset.OldPrice = 0;
                                    updateAsset.NewPrice = 0;
                                    updateAsset.AuctionScheduleID = null;
                                    asset.EmailAuctionURL = null;
                                    asset.IsEmailAuction = false;
                                }
                                else if (updateAsset.Type == 2)
                                {
                                    updateAsset.Price = 0;
                                    updateAsset.ExpiredDate = null;
                                }

                                if (!String.IsNullOrEmpty(photo1) && photo1.Contains("tmp_"))
                                {
                                    asset.Photo1 = RenameUploadedFile(photo1, asset.AssetID.ToString(), SessionKey.AssetPhoto1);
                                }
                                if (!String.IsNullOrEmpty(photo2) && photo2.Contains("tmp_"))
                                {
                                    asset.Photo2 = RenameUploadedFile(photo2, asset.AssetID.ToString(), SessionKey.AssetPhoto2);
                                }
                                if (!String.IsNullOrEmpty(photo3) && photo3.Contains("tmp_"))
                                {
                                    asset.Photo3 = RenameUploadedFile(photo3, asset.AssetID.ToString(), SessionKey.AssetPhoto3);
                                }
                                if (!String.IsNullOrEmpty(photo4) && photo4.Contains("tmp_"))
                                {
                                    asset.Photo4 = RenameUploadedFile(photo4, asset.AssetID.ToString(), SessionKey.AssetPhoto4);
                                }
                                if (!String.IsNullOrEmpty(photo5) && photo5.Contains("tmp_"))
                                {
                                    asset.Photo5 = RenameUploadedFile(photo5, asset.AssetID.ToString(), SessionKey.AssetPhoto5);
                                }

                                updateAsset.Photo1 = asset.Photo1 == null ? photo1 : asset.Photo1;
                                updateAsset.Photo2 = asset.Photo2 == null ? photo2 : asset.Photo2;
                                updateAsset.Photo3 = asset.Photo3 == null ? photo3 : asset.Photo3;
                                updateAsset.Photo4 = asset.Photo4 == null ? photo4 : asset.Photo4;
                                updateAsset.Photo5 = asset.Photo5 == null ? photo5 : asset.Photo5;

                                // Add Asset Status History If CurrentStatusID is different
                                if (updateAsset.CurrentStatusID != asset.CurrentStatusID)
                                {
                                    AssetStatusHistory statusHistory = new AssetStatusHistory();
                                    statusHistory.AssetID = updateAsset.AssetID;
                                    statusHistory.DateTimeCreated = DateTime.Now;
                                    statusHistory.CreatedBy = currentUserID;
                                    statusHistory.PrevAssetStatusID = updateAsset.CurrentStatusID;
                                    statusHistory.NewAssetStatusID = asset.CurrentStatusID;
                                    statusHistory.IsActive = true;
                                    statusHistory.Notes = String.IsNullOrEmpty(asset.AdditionalNotes) ? String.Empty : asset.AdditionalNotes;

                                    context.InsertAssetStatusHistory(statusHistory);
                                }

                                byte newStatus = asset.CurrentStatusID.HasValue ? asset.CurrentStatusID.Value : (byte)0;
                                byte oldStatus = updateAsset.CurrentStatusID.HasValue ? updateAsset.CurrentStatusID.Value : (byte)0;
                                if (oldStatus == 9 && (oldStatus != newStatus)) // Changes From Draft Update To New Status [ Ditarik, Live, Terjual, Lunas]
                                {
                                    // buttonType = 1; // trigger the submit button
                                    tmpOriginalAssetStatus = newStatus;
                                    if (newStatus == 4)
                                    {
                                        updateAsset.CurrentStatusID = 11; // Pending live

                                    }
                                    else if (newStatus == 5)
                                    {
                                        updateAsset.CurrentStatusID = 13; // Pending Terjual
                                    }
                                    else if (newStatus == 6)
                                    {
                                        updateAsset.CurrentStatusID = 12; // Pending Ditarik
                                    }
                                    else if (newStatus == 7)
                                    {
                                        updateAsset.CurrentStatusID = 14; // Pending Lunas
                                    }
                                }

                                // if Submit Clicked
                                if (buttonType == 1)
                                {
                                    if (updateAsset.CurrentStatusID == 1)
                                    {
                                        updateAsset.CurrentStatusID = 2;
                                        tmpOriginalAssetStatus = 1;
                                    }

                                    if (oldStatus == 4 && newStatus == 4) // from live to live
                                    {
                                        updateAsset.CurrentStatusID = 2; // set to pending
                                        tmpOriginalAssetStatus = 4;
                                    }
                                    else if (oldStatus == 4 && newStatus == 5) // from live to terjual 
                                    {
                                        updateAsset.CurrentStatusID = 13; //set to pending terjual
                                        tmpOriginalAssetStatus = 5;
                                    }
                                    else if (oldStatus == 4 && newStatus == 6) // from live to ditarik
                                    {
                                        updateAsset.CurrentStatusID = 12; // set to pending ditarik
                                        tmpOriginalAssetStatus = 6;
                                    }
                                    else if (oldStatus == 4 && newStatus == 7) // from live to lunas
                                    {
                                        updateAsset.CurrentStatusID = 14; // set to pending lunas
                                        tmpOriginalAssetStatus = 7;
                                    }
                                    else if (oldStatus == 6 && newStatus == 4)
                                    {
                                        updateAsset.CurrentStatusID = 11; // set to pending live
                                        tmpOriginalAssetStatus = 4;
                                    }
                                    else if (oldStatus == 6 && newStatus == 6) // from ditarik to ditarik
                                    {
                                        updateAsset.CurrentStatusID = 2; // set to pending 
                                        tmpOriginalAssetStatus = 6;
                                    }


                                    long assetChangesID = 0;

                                    AssetChanges currentAsset = GetActiveAssetChangeByAssetID(asset.AssetID).ReturnData;
                                    if (approvalType == 3 && currentAsset == null) // edit from rejected
                                    {
                                        currentAsset = GetActiveAssetChangeByAssetIDAndApprovalStatus(asset.AssetID, 2).ReturnData;
                                        if (currentAsset != null)
                                        {
                                            if (oldStatus == 9 && (oldStatus != newStatus))
                                            {
                                                currentAsset.OriginalAssetStatusID = (byte)tmpOriginalAssetStatus;
                                            }
                                            currentAsset.CurrentApprovalStatus = 0;
                                            context.UpdateAssetChanges(currentAsset);

                                        }
                                    }

                                    if (currentAsset == null)
                                    {
                                        AssetChanges assetChanges = GetNewAssetChangeAndSetStatus(asset.AssetID.Value, 0, tmpOriginalAssetStatus, true, false, false);
                                        if (oldStatus == 4 || oldStatus == 6) // if di tarik or live 
                                        {
                                            assetChanges.IsUpdated = true;
                                            assetChanges.IsNew = false;
                                            assetChanges.IsDeleted = false;
                                        }
                                        context.InsertAssetChanges(assetChanges);
                                        assetChangesID = assetChanges.AssetChangesID.Value;
                                        listChangesHistory = CompareAssetForChangesHistory(updateAsset, assetChanges.AssetChangesID.Value, false, context);
                                    }
                                    else
                                    {
                                        listChangesHistory = CompareAssetForChangesHistory(updateAsset, currentAsset.AssetChangesID.Value, false, context);
                                        assetChangesID = currentAsset.AssetChangesID.Value;
                                        if (oldStatus == 9)
                                        {
                                            currentAsset.OriginalAssetStatusID = (byte)tmpOriginalAssetStatus;
                                        }
                                    }

                                    //Fix 11 Apr 2016 - Delete all asset category template first before adding the new one, to avoid unrelated asset category template
                                    context.DeleteAssetCategoryTemplateValueByAssetID(asset.AssetID.Value);

                                    // Update AssetCategoryTemplateValue
                                    if (listCategoryTemplateValues != null)
                                    {
                                        foreach (AssetCategoryTemplateValue template in listCategoryTemplateValues)
                                        {
                                            AssetCategoryTemplateValue templateValue = context.GetAssetCategoryTemplateValueByID(asset.AssetID.Value, template.FieldName);

                                            //context.DeleteAssetCategoryTemplateValueByID(asset.AssetID.Value, template.FieldName);

                                            AssetCategoryTemplateValue templateToUpdate = new AssetCategoryTemplateValue();
                                            templateToUpdate.AssetID = asset.AssetID;
                                            templateToUpdate.FieldName = template.FieldName;
                                            templateToUpdate.Value = template.Value;
                                            templateToUpdate.IsActive = true;
                                            templateToUpdate.DateTimeCreated = DateTime.Now;
                                            templateToUpdate.CreatedBy = currentUserID;
                                            context.InsertAssetCategoryTemplateValue(templateToUpdate);

                                            // Detect the changes
                                            if (templateValue != null)
                                            {
                                                if (templateValue.Value != templateToUpdate.Value)
                                                {
                                                    AssetChangesHistory changesHistory = new AssetChangesHistory();
                                                    changesHistory.FieldOrColumnName = templateValue.FieldName;
                                                    changesHistory.LabelName = templateValue.FieldName;
                                                    changesHistory.NewValue = templateToUpdate.Value;
                                                    changesHistory.OldValue = templateValue.Value;
                                                    changesHistory.AssetChangesID = assetChangesID;
                                                    changesHistory.IsActive = true;
                                                    changesHistory.CreatedBy = SecurityHelper.GetCurrentLoggedInUserID();
                                                    changesHistory.DateTimeCreated = DateTime.Now;

                                                    listChangesHistory.Add(changesHistory);
                                                }
                                            }
                                        }
                                    }

                                    if (listChangesHistory.Count > 0)
                                    {
                                        foreach (AssetChangesHistory changesHistory in listChangesHistory)
                                        {
                                            AssetChangesHistory assetHistory = context.GetActiveAssetChangesHistory(changesHistory.AssetChangesID.Value, changesHistory.FieldOrColumnName);
                                            if (assetHistory != null)
                                            {
                                                context.UpdateAssetChangesHistoryNewValue(changesHistory.AssetChangesID.Value, changesHistory.FieldOrColumnName, changesHistory.NewValue);
                                            }
                                            else
                                            {
                                                context.InsertAssetChangesHistory(changesHistory);
                                            }
                                        }
                                    }
                                }
                                else // Button Save Clicked
                                {
                                    //Fix 11 Apr 2016 - Delete all asset category template first before adding the new one, to avoid unrelated asset category template
                                    context.DeleteAssetCategoryTemplateValueByAssetID(asset.AssetID.Value);

                                    // Update AssetCategoryTemplateValue
                                    if (listCategoryTemplateValues != null)
                                    {
                                        foreach (AssetCategoryTemplateValue template in listCategoryTemplateValues)
                                        {
                                            AssetCategoryTemplateValue templateValue = context.GetAssetCategoryTemplateValueByID(asset.AssetID.Value, template.FieldName);

                                            //context.DeleteAssetCategoryTemplateValueByID(asset.AssetID.Value, template.FieldName);

                                            AssetCategoryTemplateValue templateToUpdate = new AssetCategoryTemplateValue();
                                            templateToUpdate.AssetID = asset.AssetID;
                                            templateToUpdate.FieldName = template.FieldName;
                                            templateToUpdate.Value = template.Value;
                                            templateToUpdate.IsActive = true;
                                            templateToUpdate.DateTimeCreated = DateTime.Now;
                                            templateToUpdate.CreatedBy = currentUserID;
                                            context.InsertAssetCategoryTemplateValue(templateToUpdate);
                                        }
                                    }
                                }


                                updateAsset.LastUpdatedBy = currentUserID;
                                updateAsset.DateTimeUpdated = DateTime.Now;

                                context.UpdateAsset(updateAsset);

                                ClearPhotoSession();

                                userActivity.UserActivityTypeID = "UpdateAsset";
                                userActivity.CreatedBy = currentUserID;
                                userActivity.ReferenceID = updateAsset.AssetID.ToString();
                                context.InsertUserActivity(userActivity);

                                result.ReturnData = updateAsset;
                                result.IsSuccess = true;
                                result.ErrorMessage = String.Empty;
                            }
                            else
                            {
                                result.ReturnData = null;
                                result.IsSuccess = false;
                                result.ErrorMessage = TextResources.DuplicateSchedule_ErrorMessage;
                            }
                        }
                        else
                        {
                            result.ReturnData = null;
                            result.IsSuccess = false;
                            result.ErrorMessage = String.Format(TextResources.CannotUpdate_ErrorMessage, "Asset");
                        }
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Asset");
                    }

                    context.Commit();

                    try
                    {
                        bool isCounterRun = AssetFacade.UpdateAssetCategoryCount(updateAsset.CategoryID.Value).ReturnData;
                    }
                    catch
                    {
                    }

                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Delete asset by id (set as inactive)
        /// </summary>
        /// <param name="assetID">int group id</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the deleted asset</returns>
        public static FacadeResult<Asset> DeleteAssetByID(int assetID, int currentUserID)
        {
            FacadeResult<Asset> result = new FacadeResult<Asset>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Asset asset = context.GetAssetByID(assetID);

                    if (asset != null)
                    {
                        if (asset.CurrentStatusID == 1) // if draf 
                        {
                            asset.IsActive = false;
                        }
                        else if (asset.CurrentStatusID == 4 || asset.CurrentStatusID == 5 || asset.CurrentStatusID == 6 || asset.CurrentStatusID == 7)
                        {
                            AssetChanges assetChanges = GetActiveAssetChangeByAssetID(assetID).ReturnData;
                            if (assetChanges == null)
                            {
                                AssetChanges changes = GetNewAssetChangeAndSetStatus(assetID, 0, asset.CurrentStatusID.Value, false, false, true);
                                changes.OriginalAssetStatusID = asset.CurrentStatusID;
                                context.InsertAssetChanges(changes);

                                asset.IsActive = true;
                                asset.CurrentStatusID = 2;
                            }
                        }

                        asset.LastUpdatedBy = currentUserID;
                        asset.DateTimeUpdated = DateTime.Now;

                        context.UpdateAsset(asset);

                        userActivity.UserActivityTypeID = "DeleteAsset";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = assetID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = asset;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Asset");
                    }

                    context.Commit();

                    bool isCounterRun = UpdateAssetCategoryCount(asset.CategoryID.Value).ReturnData;

                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Approve asset
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="changes"></param>
        /// <param name="workflow"></param>
        /// <param name="currentUserID"></param>
        /// <returns>Asset that status live</returns>
        public static FacadeResult<Asset> ApproveAsset(Asset asset, AssetChanges changes, AssetWorkflowHistory workflow, int currentUserID)
        {
            FacadeResult<Asset> result = new FacadeResult<Asset>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {

                    if (changes != null)
                    {
                        context.UpdateAssetChanges(changes);
                        workflow.AssetChangesID = changes.AssetChangesID;
                        context.InsertAssetWorkflowHistory(workflow);
                    }

                    // Add asset status history 
                    if (asset.CurrentStatusID != 4)
                    {
                        AssetStatusHistory statusHistory = new AssetStatusHistory();
                        statusHistory.AssetID = asset.AssetID;
                        statusHistory.DateTimeCreated = DateTime.Now;
                        statusHistory.CreatedBy = currentUserID;
                        statusHistory.PrevAssetStatusID = asset.CurrentStatusID;
                        statusHistory.NewAssetStatusID = 4;
                        statusHistory.IsActive = true;
                        statusHistory.Notes = String.IsNullOrEmpty(asset.AdditionalNotes) ? String.Empty : asset.AdditionalNotes;

                        context.InsertAssetStatusHistory(statusHistory);
                    }


                    asset.LastUpdatedBy = currentUserID;
                    asset.DateTimeUpdated = DateTime.Now;
                    context.UpdateAsset(asset);

                    userActivity.UserActivityTypeID = "ApproveAsset";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = asset.AssetID.ToString();
                    context.InsertUserActivity(userActivity);

                    context.Commit();

                    bool isCounterRun = UpdateAssetCategoryCount(asset.CategoryID.Value).ReturnData;

                    result.ReturnData = asset;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Reject an asset if
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="changes"></param>
        /// <param name="workflow"></param>
        /// <param name="currentUserID"></param>
        /// <returns></returns>
        public static FacadeResult<Asset> RejectAsset(Asset asset, AssetChanges changes, AssetWorkflowHistory workflow, IList<AssetRejectReason> listReason, int currentUserID)
        {
            FacadeResult<Asset> result = new FacadeResult<Asset>();
            UserActivity userActivity = new UserActivity();

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {

                    context.UpdateAssetChanges(changes);
                    workflow.AssetChangesID = changes.AssetChangesID;
                    context.InsertAssetWorkflowHistory(workflow);

                    if (listReason.Count > 0)
                    {
                        for (int i = 0; i < listReason.Count; i++)
                        {
                            AssetRejectReason tmpReason = listReason[i];
                            tmpReason.AssetWorkflowHistoryID = workflow.AssetWorkflowHistoryID;
                            context.InsertAssetRejectReasonNew(tmpReason);
                        }
                    }

                    // Add asset status history 
                    if (asset.CurrentStatusID != 4)
                    {
                        AssetStatusHistory statusHistory = new AssetStatusHistory();
                        statusHistory.AssetID = asset.AssetID;
                        statusHistory.DateTimeCreated = DateTime.Now;
                        statusHistory.CreatedBy = currentUserID;
                        statusHistory.PrevAssetStatusID = changes.OriginalAssetStatusID;
                        statusHistory.NewAssetStatusID = asset.CurrentStatusID;
                        statusHistory.IsActive = true;
                        statusHistory.Notes = String.IsNullOrEmpty(asset.AdditionalNotes) ? String.Empty : asset.AdditionalNotes;

                        context.InsertAssetStatusHistory(statusHistory);
                    }

                    asset.IsActive = true;
                    asset.LastUpdatedBy = currentUserID;
                    asset.DateTimeUpdated = DateTime.Now;
                    context.UpdateAsset(asset);

                    userActivity.UserActivityTypeID = "RejectAsset";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = asset.AssetID.ToString();
                    context.InsertUserActivity(userActivity);


                    context.Commit();

                    result.ReturnData = asset;
                    result.IsSuccess = true;
                }
                catch (Exception exception)
                {
                    LogHelper.LogException(exception);
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }
            return result;
        }

        /// <summary>
        /// Cancel deleting the Asset
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="changes"></param>
        /// <param name="currentUserID"></param>
        /// <returns></returns>
        public static FacadeResult<Asset> CancelDelete(Asset asset, AssetChanges changes, int currentUserID)
        {
            FacadeResult<Asset> result = new FacadeResult<Asset>();
            UserActivity userActivity = new UserActivity();

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {

                    context.UpdateAssetChanges(changes);

                    asset.IsActive = true;
                    asset.LastUpdatedBy = currentUserID;
                    asset.DateTimeUpdated = DateTime.Now;
                    context.UpdateAsset(asset);

                    userActivity.UserActivityTypeID = "CancelDeleteAsset";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = asset.AssetID.ToString();
                    context.InsertUserActivity(userActivity);


                    context.Commit();

                    result.ReturnData = asset;
                    result.IsSuccess = true;
                }
                catch (Exception exception)
                {
                    LogHelper.LogException(exception);
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }



            return result;
        }

        #endregion

        #region Photo Upload Rename
        public static String RenameUploadedFile(String fileUploaded, String assetID, SessionKey key)
        {

            String result = assetID;
            switch (key)
            {
                case SessionKey.AssetPhoto1:
                    result = result + "_1";
                    break;
                case SessionKey.AssetPhoto2:
                    result = result + "_2";
                    break;
                case SessionKey.AssetPhoto3:
                    result = result + "_3";
                    break;
                case SessionKey.AssetPhoto4:
                    result = result + "_4";
                    break;
                case SessionKey.AssetPhoto5:
                    result = result + "_5";
                    break;
            }
            String path = ConfigurationManager.AppSettings["ImageUploadPath"].ToString();
            FileInfo fileInfo = new FileInfo(path + fileUploaded);
            String ext = fileInfo.Extension;
            result += ext;
            if (fileInfo.Exists)
            {
                fileInfo.CopyTo(path + result, true);
                WebImage image = new WebImage(path + result);
                WebImage resizeImage = image.Resize(640, 480, false, false);
                image.Save(path + "RESIZE_" + result);

                // Delete Old File After Copying
                FileInfo fileToDelete = new FileInfo(path + fileUploaded);
                if (fileToDelete.Exists)
                {
                    fileToDelete.Delete();
                }
            }
            return result;
        }

        public static void ClearPhotoSession()
        {
            SessionHelper.Remove(SessionKey.AssetPhoto1);
            SessionHelper.Remove(SessionKey.AssetPhoto2);
            SessionHelper.Remove(SessionKey.AssetPhoto3);
            SessionHelper.Remove(SessionKey.AssetPhoto4);
            SessionHelper.Remove(SessionKey.AssetPhoto5);
        }
        #endregion

        #region Compare Asset To DB
        public static IList<AssetChangesHistory> CompareAssetForChangesHistory(Asset assetNew, long assetChangesID, bool isAssetIsNew, IMandiriLelangDataRepository context)
        {
            IList<AssetChangesHistory> result = new List<AssetChangesHistory>();

            int userID = SecurityHelper.GetCurrentLoggedInUserID();

            if (isAssetIsNew == false)
            {
                #region AssetIsUpdated
                Asset current = context.GetActiveAssetByID(assetNew.AssetID.Value);
                // Account Name
                if (current.AccountName != assetNew.AccountName)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AccountName";
                    changes.LabelName = TextResources.AssetAccountName_DisplayName;
                    changes.NewValue = assetNew.AccountName;
                    changes.OldValue = current.AccountName;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Account Number
                if (current.AccountNumber != assetNew.AccountNumber)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AccountNumber";
                    changes.LabelName = TextResources.AssetAccountNumber_DisplayName;
                    changes.NewValue = assetNew.AccountNumber;
                    changes.OldValue = current.AccountNumber;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Email Auction
                if (current.IsEmailAuction != assetNew.IsEmailAuction)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "IsEmailAuction";
                    changes.LabelName = TextResources.IsEmailAuction_DisplayName;
                    changes.NewValue = assetNew.IsEmailAuction.HasValue ? assetNew.IsEmailAuction.Value.ToString() : String.Empty;
                    changes.OldValue = current.IsEmailAuction.HasValue ? current.IsEmailAuction.Value.ToString() : String.Empty;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Email Auction URL
                if (current.EmailAuctionURL != assetNew.EmailAuctionURL)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "EmailAuctionURL";
                    changes.LabelName = TextResources.EmailAuctionURL_DisplayName;
                    changes.NewValue = assetNew.EmailAuctionURL;
                    changes.OldValue = current.EmailAuctionURL;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // CategoryID
                if (current.CategoryID != assetNew.CategoryID)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "CategoryID";
                    changes.LabelName = TextResources.Category_DisplayName;
                    changes.NewValue = context.GetCategoryNameByCategoryID(assetNew.CategoryID.HasValue ? assetNew.CategoryID.Value : -1);
                    changes.OldValue = context.GetCategoryNameByCategoryID(current.CategoryID.HasValue ? current.CategoryID.Value : -1);
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Address
                if (current.Address != assetNew.Address)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Address";
                    changes.LabelName = TextResources.AssetAddress_DisplayName;
                    changes.NewValue = assetNew.Address;
                    changes.OldValue = current.Address;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // ProvinceID
                if (current.ProvinceID != assetNew.ProvinceID)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "ProvinceID";
                    changes.LabelName = TextResources.Province_DisplayName;
                    changes.NewValue = context.GetProvinceNameByProvinceID(assetNew.ProvinceID.HasValue ? assetNew.ProvinceID.Value : -1);
                    changes.OldValue = context.GetProvinceNameByProvinceID(current.ProvinceID.HasValue ? current.ProvinceID.Value : -1);
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // CityID
                if (current.CityID != assetNew.CityID)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "CityID";
                    changes.LabelName = TextResources.City_DisplayName;
                    changes.NewValue = context.GetCityNameByCityID(assetNew.CityID.HasValue ? assetNew.CityID.Value : -1);
                    changes.OldValue = context.GetCityNameByCityID(current.CityID.HasValue ? current.CityID.Value : -1);
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // OldPrice
                if (current.OldPrice != assetNew.OldPrice)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "OldPrice";
                    changes.LabelName = TextResources.AssetOldPrice_DisplayName;
                    changes.NewValue = assetNew.OldPrice.HasValue ? assetNew.OldPrice.Value.ToString() : String.Empty;
                    changes.OldValue = current.OldPrice.HasValue ? current.OldPrice.Value.ToString() : String.Empty;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // NewPrice
                if (current.NewPrice != assetNew.NewPrice)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "NewPrice";
                    changes.LabelName = TextResources.AssetNewPrice_DisplayName;
                    changes.NewValue = assetNew.NewPrice.HasValue ? assetNew.NewPrice.Value.ToString() : String.Empty;
                    changes.OldValue = current.NewPrice.HasValue ? current.NewPrice.Value.ToString() : String.Empty;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // MarketValue
                if (current.MarketValue != assetNew.MarketValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "MarketValue";
                    changes.LabelName = TextResources.AssetMarketValue_DisplayName;
                    changes.NewValue = assetNew.MarketValue.HasValue ? assetNew.MarketValue.Value.ToString() : String.Empty;
                    changes.OldValue = current.MarketValue.HasValue ? current.MarketValue.Value.ToString() : String.Empty;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // LiquidationValue
                if (current.LiquidationValue != assetNew.LiquidationValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "LiquidationValue";
                    changes.LabelName = TextResources.AssetLiquidationValue_DisplayName;
                    changes.NewValue = assetNew.LiquidationValue.HasValue ? assetNew.LiquidationValue.Value.ToString() : String.Empty;
                    changes.OldValue = current.LiquidationValue.HasValue ? current.LiquidationValue.Value.ToString() : String.Empty;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // SecurityRightsValue
                if (current.SecurityRightsValue != assetNew.SecurityRightsValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "SecurityRightsValue";
                    changes.LabelName = TextResources.AssetSecurityRightsValue_DisplayName;
                    changes.NewValue = assetNew.SecurityRightsValue.HasValue ? assetNew.SecurityRightsValue.Value.ToString() : String.Empty;
                    changes.OldValue = current.SecurityRightsValue.HasValue ? current.SecurityRightsValue.Value.ToString() : String.Empty;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // SoldValue
                if (current.SoldValue != assetNew.SoldValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "SoldValue";
                    changes.LabelName = TextResources.AssetSoldValue_DisplayName;
                    changes.NewValue = assetNew.SoldValue.HasValue ? assetNew.SoldValue.Value.ToString() : String.Empty;
                    changes.OldValue = current.SoldValue.HasValue ? current.SoldValue.Value.ToString() : String.Empty;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // BranchID
                if (current.BranchID != assetNew.BranchID)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "BranchID";
                    changes.LabelName = TextResources.Branch_DisplayName;
                    changes.NewValue = context.GetBranchNameByID(assetNew.BranchID.HasValue ? assetNew.BranchID.Value : -1);
                    changes.OldValue = context.GetBranchNameByID(current.BranchID.HasValue ? current.BranchID.Value : -1);
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // ValuationDate
                if (current.ValuationDate != assetNew.ValuationDate)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "ValuationDate";
                    changes.LabelName = TextResources.AssetValuationDate_DisplayName;
                    changes.NewValue = assetNew.ValuationDate.HasValue ? assetNew.ValuationDate.Value.ToString() : String.Empty;
                    changes.OldValue = current.ValuationDate.HasValue ? current.ValuationDate.Value.ToString() : String.Empty;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // AuctionScheduleID
                if (current.AuctionScheduleID != assetNew.AuctionScheduleID)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AuctionScheduleID";
                    changes.LabelName = TextResources.AuctionSchedule_DisplayName;
                    if (assetNew.AuctionScheduleID.HasValue)
                    {
                        changes.NewValue = context.GetAuctionScheduleNameByID(assetNew.AuctionScheduleID.Value);
                    }
                    else
                    {
                        changes.NewValue = String.Empty;
                    }
                    //changes.NewValue = context.GetAuctionScheduleNameByID(assetNew.AuctionScheduleID.HasValue ? assetNew.AuctionScheduleID.Value : -1);
                    //changes.OldValue = context.GetAuctionScheduleNameByID(current.AuctionScheduleID.HasValue ? current.AuctionScheduleID.Value : -1);
                    if (current.AuctionScheduleID.HasValue)
                    {
                        changes.OldValue = context.GetAuctionScheduleNameByID(current.AuctionScheduleID.Value);
                    }
                    else
                    {
                        changes.OldValue = String.Empty;
                    }
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // AuctionHallID
                if (current.AuctionHallID != assetNew.AuctionHallID)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AuctionHallID";
                    changes.LabelName = TextResources.AuctionHall_DisplayName;
                    //changes.NewValue = context.GetAuctionHallNameByID(assetNew.AuctionHallID.HasValue ? assetNew.AuctionHallID.Value : -1);
                    //changes.OldValue = context.GetAuctionHallNameByID(current.AuctionHallID.HasValue ? current.AuctionHallID.Value : -1);
                    if (assetNew.AuctionHallID.HasValue)
                    {
                        changes.NewValue = context.GetAuctionHallNameByID(assetNew.AuctionHallID.Value);
                    }
                    else
                    {
                        changes.NewValue = String.Empty;
                    }
                    if (current.AuctionHallID.HasValue)
                    {
                        changes.OldValue = context.GetAuctionHallNameByID(current.AuctionHallID.Value);
                    }
                    else
                    {
                        changes.OldValue = String.Empty;
                    }
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // AuctionHallAddress
                if (current.AuctionHallAddress != assetNew.AuctionHallAddress)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AuctionHallAddress";
                    changes.LabelName = TextResources.AuctionHallAddress_DisplayName;
                    changes.NewValue = assetNew.AuctionHallAddress;
                    changes.OldValue = current.AuctionHallAddress;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // AuctionHallPhone1
                if (current.AuctionHallPhone1 != assetNew.AuctionHallPhone1)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AuctionHallPhone1";
                    changes.LabelName = TextResources.AssetAuctionHallPhone1_DisplayName;
                    changes.NewValue = assetNew.AuctionHallPhone1;
                    changes.OldValue = current.AuctionHallPhone1;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // AuctionHallPhone2
                if (current.AuctionHallPhone2 != assetNew.AuctionHallPhone2)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AuctionHallPhone2";
                    changes.LabelName = TextResources.AssetAuctionHallPhone2_DisplayName;
                    changes.NewValue = assetNew.AuctionHallPhone2;
                    changes.OldValue = current.AuctionHallPhone2;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // KPKNLID
                if (current.KPKNLID != assetNew.KPKNLID)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "KPKNLID";
                    changes.LabelName = TextResources.KPKNL_DisplayName;
                    //changes.NewValue = context.GetKPKNLNameByKPKNLID(assetNew.KPKNLID.HasValue ? assetNew.KPKNLID.Value : -1);
                    //changes.OldValue = context.GetKPKNLNameByKPKNLID(current.KPKNLID.HasValue ? current.KPKNLID.Value : -1);
                    if (assetNew.KPKNLID.HasValue)
                    {
                        changes.NewValue = context.GetKPKNLNameByKPKNLID(assetNew.KPKNLID.Value);
                    }
                    else
                    {
                        changes.NewValue = String.Empty;
                    }
                    if (current.KPKNLID.HasValue)
                    {
                        changes.OldValue = context.GetKPKNLNameByKPKNLID(current.KPKNLID.Value);
                    }
                    else
                    {
                        changes.OldValue = String.Empty;
                    }
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // KPKNLAddress
                if (current.KPKNLAddress != assetNew.KPKNLAddress)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "KPKNLAddress";
                    changes.LabelName = TextResources.KPKNLAddress_DisplayName;
                    changes.NewValue = assetNew.KPKNLAddress;
                    changes.OldValue = current.KPKNLAddress;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // AuctionAddress
                if (current.AuctionAddress != assetNew.AuctionAddress)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AuctionAddress";
                    changes.LabelName = TextResources.AssetAuctionAddress_DisplayName;
                    changes.NewValue = assetNew.AuctionAddress;
                    changes.OldValue = current.AuctionAddress;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // KPKNLPhone1
                if (current.KPKNLPhone1 != assetNew.KPKNLPhone1)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "KPKNLPhone1";
                    changes.LabelName = TextResources.AssetKPKNLPhone1_DisplayName;
                    changes.NewValue = assetNew.KPKNLPhone1;
                    changes.OldValue = current.KPKNLPhone1;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // KPKNLPhone2
                if (current.KPKNLPhone2 != assetNew.KPKNLPhone2)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "KPKNLPhone2";
                    changes.LabelName = TextResources.AssetKPKNLPhone2_DisplayName;
                    changes.NewValue = assetNew.KPKNLPhone2;
                    changes.OldValue = current.KPKNLPhone2;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }


                // SegmentID
                if (current.SegmentID != assetNew.SegmentID)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "SegmentID";
                    changes.LabelName = TextResources.Segment_DisplayName;
                    changes.NewValue = context.GetSegmentNameBySegmentID(assetNew.SegmentID);
                    changes.OldValue = context.GetSegmentNameBySegmentID(current.SegmentID);
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Photo1
                if (current.Photo1 != assetNew.Photo1)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Photo1";
                    changes.LabelName = TextResources.Photo1_DisplayName;
                    changes.NewValue = assetNew.Photo1;
                    changes.OldValue = current.Photo1;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Photo2
                if (current.Photo2 != assetNew.Photo2)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Photo2";
                    changes.LabelName = TextResources.Photo2_DisplayName;
                    changes.NewValue = assetNew.Photo2;
                    changes.OldValue = current.Photo2;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Photo3
                if (current.Photo3 != assetNew.Photo3)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Photo3";
                    changes.LabelName = TextResources.Photo3_DisplayName;
                    changes.NewValue = assetNew.Photo3;
                    changes.OldValue = current.Photo3;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Photo4
                if (current.Photo4 != assetNew.Photo4)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Photo4";
                    changes.LabelName = TextResources.Photo4_DisplayName;
                    changes.NewValue = assetNew.Photo4;
                    changes.OldValue = current.Photo4;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Photo5
                if (current.Photo5 != assetNew.Photo5)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Photo5";
                    changes.LabelName = TextResources.Photo5_DisplayName;
                    changes.NewValue = assetNew.Photo5;
                    changes.OldValue = current.Photo5;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // OwnershipDocumentTypeID
                if (current.OwnershipDocumentTypeID != assetNew.OwnershipDocumentTypeID)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "OwnershipDocumentTypeID";
                    changes.LabelName = TextResources.OwnershipDocumentType_DisplayName;
                    changes.NewValue = context.GetOwnershipDocumentTypeNameByID(assetNew.OwnershipDocumentTypeID.HasValue ? assetNew.OwnershipDocumentTypeID.Value : -1);
                    changes.OldValue = context.GetOwnershipDocumentTypeNameByID(current.OwnershipDocumentTypeID.HasValue ? current.OwnershipDocumentTypeID.Value : -1);
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Latitude
                if (current.Latitude != assetNew.Latitude)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Latitude";
                    changes.LabelName = TextResources.AssetLatitue_DisplayName;
                    changes.NewValue = assetNew.Latitude.HasValue ? assetNew.Latitude.Value.ToString() : String.Empty;
                    changes.OldValue = current.Latitude.HasValue ? current.Latitude.Value.ToString() : String.Empty;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Longitude
                if (current.Longitude != assetNew.Longitude)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Longitude";
                    changes.LabelName = TextResources.AssetLongitude_DisplayName;
                    changes.NewValue = assetNew.Longitude.HasValue ? assetNew.Longitude.Value.ToString() : String.Empty;
                    changes.OldValue = current.Longitude.HasValue ? current.Longitude.Value.ToString() : String.Empty;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // TransferInformation
                if (current.TransferInformation != assetNew.TransferInformation)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "TransferInformation";
                    changes.LabelName = TextResources.AssetTransferInformation_DisplayName;
                    changes.NewValue = assetNew.TransferInformation;
                    changes.OldValue = current.TransferInformation;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // AdditionalNotes
                if (current.AdditionalNotes != assetNew.AdditionalNotes)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AdditionalNotes";
                    changes.LabelName = TextResources.AssetAdditionalNote_DisplayName;
                    changes.NewValue = assetNew.AdditionalNotes;
                    changes.OldValue = current.AdditionalNotes;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // DocumentNumber
                if (current.DocumentNumber != assetNew.DocumentNumber)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "DocumentNumber";
                    changes.LabelName = TextResources.DocumentNumber_DisplayName;
                    changes.NewValue = assetNew.DocumentNumber;
                    changes.OldValue = current.DocumentNumber;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Type
                if (current.Type != assetNew.Type)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Type";
                    changes.LabelName = TextResources.Type_DisplayName;
                    changes.NewValue = assetNew.Type.ToString();
                    changes.OldValue = current.Type.ToString();
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Price
                if (current.Price != assetNew.Price)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Price";
                    changes.LabelName = TextResources.Price_DisplayName;
                    changes.NewValue = assetNew.Price.HasValue ? assetNew.Price.Value.ToString() : String.Empty;
                    changes.OldValue = current.Price.HasValue ? current.Price.Value.ToString() : String.Empty;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Expired Date
                if (current.ExpiredDate != assetNew.ExpiredDate)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "ExpiredDate";
                    changes.LabelName = TextResources.ExpiredDate_DisplayName;
                    changes.NewValue = assetNew.ExpiredDate.HasValue ? assetNew.ExpiredDate.Value.ToString() : String.Empty;
                    changes.OldValue = current.ExpiredDate.HasValue ? current.ExpiredDate.Value.ToString() : String.Empty;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }
                #endregion
            }
            else
            {
                #region AssetIsNew
                // Account Name
                if (!String.IsNullOrEmpty(assetNew.AccountName))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AccountName";
                    changes.LabelName = TextResources.AssetAccountName_DisplayName;
                    changes.NewValue = assetNew.AccountName;
                    changes.OldValue = assetNew.AccountName;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Account Number
                if (!String.IsNullOrEmpty(assetNew.AccountNumber))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AccountNumber";
                    changes.LabelName = TextResources.AssetAccountNumber_DisplayName;
                    changes.NewValue = assetNew.AccountNumber;
                    changes.OldValue = assetNew.AccountNumber;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Email Auction
                if (assetNew.IsEmailAuction.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "IsEmailAuction";
                    changes.LabelName = TextResources.IsEmailAuction_DisplayName;
                    changes.NewValue = assetNew.IsEmailAuction.HasValue ? assetNew.IsEmailAuction.Value.ToString() : String.Empty;
                    changes.OldValue = assetNew.IsEmailAuction.HasValue ? assetNew.IsEmailAuction.Value.ToString() : String.Empty;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Email Auction URL
                if (!String.IsNullOrEmpty(assetNew.EmailAuctionURL))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "EmailAuctionURL";
                    changes.LabelName = TextResources.EmailAuctionURL_DisplayName;
                    changes.NewValue = assetNew.EmailAuctionURL;
                    changes.OldValue = assetNew.EmailAuctionURL;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // CategoryID
                if (assetNew.CategoryID.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "CategoryID";
                    changes.LabelName = TextResources.Category_DisplayName;
                    changes.NewValue = context.GetCategoryNameByCategoryID(assetNew.CategoryID.Value);
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Address
                if (!String.IsNullOrEmpty(assetNew.Address))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Address";
                    changes.LabelName = TextResources.AssetAddress_DisplayName;
                    changes.NewValue = assetNew.Address;
                    changes.OldValue = assetNew.Address;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // ProvinceID
                if (assetNew.ProvinceID.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "ProvinceID";
                    changes.LabelName = TextResources.Province_DisplayName;
                    changes.NewValue = context.GetProvinceNameByProvinceID(assetNew.ProvinceID.Value);
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // CityID
                if (assetNew.CityID.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "CityID";
                    changes.LabelName = TextResources.City_DisplayName;
                    changes.NewValue = context.GetCityNameByCityID(assetNew.CityID.Value);
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // OldPrice
                if (assetNew.OldPrice.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "OldPrice";
                    changes.LabelName = TextResources.AssetOldPrice_DisplayName;
                    changes.NewValue = assetNew.OldPrice.HasValue ? assetNew.OldPrice.Value.ToString() : String.Empty;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // NewPrice
                if (assetNew.NewPrice.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "NewPrice";
                    changes.LabelName = TextResources.AssetNewPrice_DisplayName;
                    changes.NewValue = assetNew.NewPrice.HasValue ? assetNew.NewPrice.Value.ToString() : String.Empty;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // BranchID
                if (assetNew.BranchID.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "BranchID";
                    changes.LabelName = TextResources.Branch_DisplayName;
                    changes.NewValue = context.GetBranchNameByID(assetNew.BranchID.Value);
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // MarketValue
                if (assetNew.MarketValue.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "MarketValue";
                    changes.LabelName = TextResources.AssetMarketValue_DisplayName;
                    changes.NewValue = assetNew.MarketValue.HasValue ? assetNew.MarketValue.Value.ToString() : String.Empty;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // LiquidationValue
                if (assetNew.LiquidationValue.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "LiquidationValue";
                    changes.LabelName = TextResources.AssetLiquidationValue_DisplayName;
                    changes.NewValue = assetNew.LiquidationValue.HasValue ? assetNew.LiquidationValue.Value.ToString() : String.Empty;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // SecurityRightsValue
                if (assetNew.LiquidationValue.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "SecurityRightsValue";
                    changes.LabelName = TextResources.AssetSecurityRightsValue_DisplayName;
                    changes.NewValue = assetNew.SecurityRightsValue.HasValue ? assetNew.SecurityRightsValue.Value.ToString() : String.Empty;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // SoldValue
                if (assetNew.SoldValue.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "SoldValue";
                    changes.LabelName = TextResources.AssetSoldValue_DisplayName;
                    changes.OldValue = changes.NewValue;
                    changes.OldValue = assetNew.SoldValue.HasValue ? assetNew.SoldValue.Value.ToString() : String.Empty;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // ValuationDate
                if (assetNew.ValuationDate.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "ValuationDate";
                    changes.LabelName = TextResources.AssetValuationDate_DisplayName;
                    changes.NewValue = assetNew.ValuationDate.HasValue ? assetNew.ValuationDate.Value.ToString() : String.Empty;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // AuctionScheduleID
                if (assetNew.AuctionScheduleID.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AuctionScheduleID";
                    changes.LabelName = TextResources.AuctionSchedule_DisplayName;
                    changes.NewValue = context.GetAuctionScheduleNameByID(assetNew.AuctionScheduleID.Value);
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // AuctionHallID
                if (assetNew.AuctionHallID.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AuctionHallID";
                    changes.LabelName = TextResources.AuctionHall_DisplayName;
                    changes.NewValue = context.GetAuctionHallNameByID(assetNew.AuctionHallID.Value);
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // AuctionHallAddress
                if (!String.IsNullOrEmpty(assetNew.AuctionHallAddress))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AuctionHallAddress";
                    changes.LabelName = TextResources.AuctionHallAddress_DisplayName;
                    changes.NewValue = assetNew.AuctionHallAddress;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // AuctionHallPhone1
                if (!String.IsNullOrEmpty(assetNew.AuctionHallPhone1))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AuctionHallPhone1";
                    changes.LabelName = TextResources.AssetAuctionHallPhone1_DisplayName;
                    changes.NewValue = assetNew.AuctionHallPhone1;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // AuctionHallPhone2
                if (!String.IsNullOrEmpty(assetNew.AuctionHallPhone2))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AuctionHallPhone2";
                    changes.LabelName = TextResources.AssetAuctionHallPhone2_DisplayName;
                    changes.NewValue = assetNew.AuctionHallPhone2;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // KPKNLID
                if (assetNew.KPKNLID.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "KPKNLID";
                    changes.LabelName = TextResources.KPKNL_DisplayName;
                    changes.NewValue = context.GetKPKNLNameByKPKNLID(assetNew.KPKNLID.Value);
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // KPKNLAddress
                if (!String.IsNullOrEmpty(assetNew.KPKNLAddress))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "KPKNLAddress";
                    changes.LabelName = TextResources.KPKNLAddress_DisplayName;
                    changes.NewValue = assetNew.KPKNLAddress;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // AuctionLAddress
                if (!String.IsNullOrEmpty(assetNew.AuctionAddress))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AuctionAddress";
                    changes.LabelName = TextResources.AssetAuctionAddress_DisplayName;
                    changes.NewValue = assetNew.AuctionAddress;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // KPKNLPhone1
                if (!String.IsNullOrEmpty(assetNew.KPKNLPhone1))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "KPKNLPhone1";
                    changes.LabelName = TextResources.AssetKPKNLPhone1_DisplayName;
                    changes.NewValue = assetNew.KPKNLPhone1;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // KPKNLPhone2
                if (!String.IsNullOrEmpty(assetNew.KPKNLPhone2))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "KPKNLPhone2";
                    changes.LabelName = TextResources.AssetKPKNLPhone2_DisplayName;
                    changes.NewValue = assetNew.KPKNLPhone2;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }


                // SegmentID
                if (!String.IsNullOrEmpty(assetNew.SegmentID))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "SegmentID";
                    changes.LabelName = TextResources.Segment_DisplayName;
                    changes.NewValue = context.GetSegmentNameBySegmentID(assetNew.SegmentID);
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Photo1
                if (!String.IsNullOrEmpty(assetNew.Photo1))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Photo1";
                    changes.LabelName = TextResources.Photo1_DisplayName;
                    changes.NewValue = assetNew.Photo1;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Photo2
                if (!String.IsNullOrEmpty(assetNew.Photo2))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Photo2";
                    changes.LabelName = TextResources.Photo2_DisplayName;
                    changes.NewValue = assetNew.Photo2;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Photo3
                if (!String.IsNullOrEmpty(assetNew.Photo3))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Photo3";
                    changes.LabelName = TextResources.Photo3_DisplayName;
                    changes.NewValue = assetNew.Photo3;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Photo4
                if (!String.IsNullOrEmpty(assetNew.Photo4))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Photo4";
                    changes.LabelName = TextResources.Photo4_DisplayName;
                    changes.NewValue = assetNew.Photo4;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Photo5
                if (!String.IsNullOrEmpty(assetNew.Photo5))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Photo5";
                    changes.LabelName = TextResources.Photo5_DisplayName;
                    changes.NewValue = assetNew.Photo5;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // OwnershipDocumentTypeID
                if (assetNew.OwnershipDocumentTypeID.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "OwnershipDocumentTypeID";
                    changes.LabelName = TextResources.OwnershipDocumentType_DisplayName;
                    changes.NewValue = context.GetOwnershipDocumentTypeNameByID(assetNew.OwnershipDocumentTypeID.Value);
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Latitude
                if (assetNew.Latitude.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Latitude";
                    changes.LabelName = TextResources.AssetLatitue_DisplayName;
                    changes.NewValue = assetNew.Latitude.HasValue ? assetNew.Latitude.Value.ToString() : String.Empty;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Longitude
                if (assetNew.Longitude.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Longitude";
                    changes.LabelName = TextResources.AssetLongitude_DisplayName;
                    changes.NewValue = assetNew.Longitude.HasValue ? assetNew.Longitude.Value.ToString() : String.Empty;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // TransferInformation
                if (!String.IsNullOrEmpty(assetNew.TransferInformation))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "TransferInformation";
                    changes.LabelName = TextResources.AssetTransferInformation_DisplayName;
                    changes.NewValue = assetNew.TransferInformation;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // AdditionalNotes
                if (!String.IsNullOrEmpty(assetNew.AdditionalNotes))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "AdditionalNotes";
                    changes.LabelName = TextResources.AssetAdditionalNote_DisplayName;
                    changes.NewValue = assetNew.AdditionalNotes;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // DocumentNumber
                if (!String.IsNullOrEmpty(assetNew.DocumentNumber))
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "DocumentNumber";
                    changes.LabelName = TextResources.DocumentNumber_DisplayName;
                    changes.NewValue = assetNew.DocumentNumber;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Type
                if (assetNew.Type != 0)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Type";
                    changes.LabelName = TextResources.Type_DisplayName;
                    changes.NewValue = assetNew.Type.ToString();
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Price
                if (assetNew.Price.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "Price";
                    changes.LabelName = TextResources.Price_DisplayName;
                    changes.NewValue = assetNew.Price.HasValue ? assetNew.Price.Value.ToString() : String.Empty;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }

                // Expired Date
                if (assetNew.ExpiredDate.HasValue)
                {
                    AssetChangesHistory changes = new AssetChangesHistory();
                    changes.FieldOrColumnName = "ExpiredDate";
                    changes.LabelName = TextResources.ExpiredDate_DisplayName;
                    changes.NewValue = assetNew.ExpiredDate.HasValue ? assetNew.ExpiredDate.Value.ToString() : String.Empty;
                    changes.OldValue = changes.NewValue;
                    changes.DateTimeCreated = DateTime.Now;
                    changes.CreatedBy = userID;
                    changes.AssetChangesID = assetChangesID;
                    result.Add(changes);
                }
                #endregion
            }
            return result;
        }
        #endregion

        #region Get New Asset Changes Where CurrentStatusApproval != 0 and CurrentStatusApproval != 2
        /// <summary>
        /// Get the new Asset Changes And set the status
        /// </summary>
        /// <param name="assetID"></param>
        /// <param name="currentApprovalStatus"></param>
        /// <param name="originalAssetStatusID"></param>
        /// <param name="isNew"></param>
        /// <param name="isUpdated"></param>
        /// <param name="isDeleted"></param>
        /// <returns>null if asset changes with status 0 or 2 exists</returns>
        public static AssetChanges GetNewAssetChangeAndSetStatus(long? assetID, int currentApprovalStatus, int originalAssetStatusID, bool? isNew, bool? isUpdated, bool? isDeleted)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();

            AssetChanges checkingStatusZero = context.GetActiveAssetChangeByAssetIDAndApprovalStatus(assetID, 0);
            AssetChanges checkingStatusTwo = context.GetActiveAssetChangeByAssetIDAndApprovalStatus(assetID, 2);

            AssetChanges assetChanges = new AssetChanges();
            if (checkingStatusTwo == null || checkingStatusZero == null)
            {
                assetChanges.AssetID = assetID;
                assetChanges.IsDeleted = isDeleted.HasValue ? isDeleted.Value : false;
                assetChanges.IsNew = isNew.HasValue ? isNew.Value : false;
                assetChanges.IsUpdated = isUpdated.HasValue ? isUpdated.Value : false;
                assetChanges.IsActive = true;
                assetChanges.CurrentApprovalStatus = currentApprovalStatus;
                assetChanges.OriginalAssetStatusID = (byte)originalAssetStatusID;
                assetChanges.CreatedBy = SecurityHelper.GetCurrentLoggedInUserID();
                assetChanges.DateTimeCreated = DateTime.Now;
            }
            return assetChanges;
        }
        #endregion

        #region Get Reason For Edit As List<String[FieldName, Value]>

        public static FacadeResult<IList<dynamic>> GetActiveAssetRejectReasonList(long assetID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();

            IList<dynamic> assets = context.QuerySql<dynamic>(@"SELECT ARR.FieldOrColumnName, ARR.Reason
                FROM AssetChanges AC 
                INNER JOIN AssetWorkflowHistory AWH ON  AC.AssetChangesID = AWH.AssetChangesID
                INNER JOIN AssetRejectReason ARR ON AWH.AssetWorkflowHistoryID = ARR.AssetWorkflowHistoryID
                WHERE AC.AssetID = @AssetID AND AWH.IsRejected = 1", new { AssetID = assetID });
            result.ReturnData = assets;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        #endregion

        #region Get WorkflowHistory
        /// <summary>
        /// GetActiveAssetWorkFlowHistoryRejectedByAssetChangesID
        /// </summary>
        /// <param name="assetChangesID"></param>
        /// <returns></returns>
        public static FacadeResult<AssetWorkflowHistory> GetActiveAssetWorkFlowHistoryRejectedByAssetChangesID(long assetChangesID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<AssetWorkflowHistory> result = new FacadeResult<AssetWorkflowHistory>();

            AssetWorkflowHistory assetWorkFlow = context.GetActiveAssetWorkFlowHistoryRejectedByAssetChangesID(assetChangesID);
            result.ReturnData = assetWorkFlow;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        #endregion

        public static FacadeResult<IList<dynamic>> GetAllActiveAssetChangesHistoryValueByAssetChangesID(int assetChangesID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();

            IList<dynamic> assets = context.QuerySql<dynamic>(@"SELECT LabelName, OldValue, NewValue FROM AssetChangesHistory WHERE AssetChangesID = @AssetChangesID Order By AssetChangesHistoryID Desc", new { AssetChangesID = assetChangesID });

            result.ReturnData = assets;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        public static FacadeResult<String> GetActiveAssetStatusByStatusID(int statusID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<String> result = new FacadeResult<String>();

            String state = String.Empty;
            if (statusID > 0)
            {
                state = context.GetActiveAssetStatusByStatusID(statusID);
            }
            result.ReturnData = state;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

    }
}
