﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insight.Database;
using Mandiri.Lelang.DataRepository;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.Helper;
//add for CR2
using Mandiri.Lelang.Helper.Extension;
using System.Configuration;

namespace Mandiri.Lelang.BusinessFacade
{
    public class ContactFacade : BaseFacade
    {
        /// <summary>
        /// Add new message
        /// </summary>
        /// <param name="message">The message object</param>
        /// <returns>Facade result containing the added message</returns>
        public static FacadeResult<Message> AddNewMessage(Message message, int currentUserID = 0)
        {

            FacadeResult<Message> result = new FacadeResult<Message>();
            Exception logException = null;
            UserActivity userActivity = new UserActivity();

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    message.CreatedBy = currentUserID;
                    context.InsertMessage(message);

                    if (currentUserID != 0)
                    {
                        userActivity.UserActivityTypeID = "AddNewMessage";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = message.MessageID.ToString();
                        context.InsertUserActivity(userActivity);
                    } 

                    result.ReturnData = message;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();

                    //add for CR2 send email to all user in same branch and use template
                    IList<User> user = UserFacade.GetAllActiveUserByBranchID(message.BranchID ?? 0).ReturnData;
                    string templateMessage = MasterFacade.GetActiveWebContentByWebContentTypeID(11).ReturnData.Content;

                    if (templateMessage.CaseInsensitiveContains("{Name}"))
                    {
                        templateMessage = templateMessage.ReplaceCaseInsensitive("{Name}", message.Name);
                    }

                    if (templateMessage.CaseInsensitiveContains("{Email}"))
                    {
                        if (message.Email != null)
                        {
                            templateMessage = templateMessage.ReplaceCaseInsensitive("{Email}", message.Email);
                        }
                        else
                        {
                            templateMessage = templateMessage.ReplaceCaseInsensitive("{Email}", "");
                        }
                    }

                    if (templateMessage.CaseInsensitiveContains("{Contact}"))
                    {
                        templateMessage = templateMessage.ReplaceCaseInsensitive("{Contact}", message.Phone);
                    }

                    if (templateMessage.CaseInsensitiveContains("{Category}"))
                    {
                        if (message.MessageCategoryTypeID != null)
                        {
                            string messageCategoryTypeName = MasterFacade.GetActiveMessageCategoryTypeByMessageCategoryTypeID((byte)message.MessageCategoryTypeID).ReturnData.Name;
                            templateMessage = templateMessage.ReplaceCaseInsensitive("{Category}", messageCategoryTypeName);
                        }
                        else
                        {
                            templateMessage = templateMessage.ReplaceCaseInsensitive("{Category}", "");
                        }
                    }

                    if (templateMessage.CaseInsensitiveContains("{AssetCode}"))
                    {
                        if (message.AssetCode != null)
                        {
                            templateMessage = templateMessage.ReplaceCaseInsensitive("{AssetCode}", message.AssetCode);
                        }
                        else
                        {
                            templateMessage = templateMessage.ReplaceCaseInsensitive("{AssetCode}", "");
                        }
                        
                    }

                    if (templateMessage.CaseInsensitiveContains("{Branch}"))
                    {
                        if (message.BranchID != null)
                        {
                            string branchName = MasterFacade.GetActiveBranchByBranchID((int)message.BranchID).ReturnData.Name;
                            templateMessage = templateMessage.ReplaceCaseInsensitive("{Branch}", branchName);
                        }
                        else
                        {
                            templateMessage = templateMessage.ReplaceCaseInsensitive("{Branch}", "");
                        }
                    }

                    if (templateMessage.CaseInsensitiveContains("{Message}"))
                    {
                        if (message.TextMessage != null)
                        {
                            templateMessage = templateMessage.ReplaceCaseInsensitive("{Message}", message.TextMessage);
                        }
                        else
                        {
                            templateMessage = templateMessage.ReplaceCaseInsensitive("{Message}", "");
                        }
                    }

                    if (message.Email != null)
                    {
                        MailHelper.SendEmail(ConfigurationManager.AppSettings["MailSmtpServer"], ConfigurationManager.AppSettings["MailUser"], ConfigurationManager.AppSettings["MailPassword"],
                        ConfigurationManager.AppSettings["MailSender"], message.Email, "Bank Mandiri Web Lelang - Terima Kasih",
                        templateMessage, true);
                    }

                    if (user != null)
                    {
                        foreach (User u in user)
                        {
                            if (u.Email != null && u.Email != string.Empty)
                            {
                                MailHelper.SendEmail(ConfigurationManager.AppSettings["MailSmtpServer"], ConfigurationManager.AppSettings["MailUser"], ConfigurationManager.AppSettings["MailPassword"],
                                    ConfigurationManager.AppSettings["MailSender"], u.Email, "Bank Mandiri Web Lelang - Pesan Baru",
                                    templateMessage, true);
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }
    }
}
