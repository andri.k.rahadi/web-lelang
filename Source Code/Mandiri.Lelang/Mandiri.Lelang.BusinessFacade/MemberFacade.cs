﻿using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.DataRepository;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;

namespace Mandiri.Lelang.BusinessFacade
{
    public class MemberFacade : BaseFacade
    {
        public enum Source
        {
            EMAIL,
            FACEBOOK
        }

        private static string GetSourceName(Source source)
        {
            return Enum.GetName(typeof(Source), source);
        }

        #region Get

        #region Member

        /// <summary>
        /// Get active member by member id
        /// </summary>
        /// <param name="memberID">The member id</param>
        /// <returns>Facade result containing the member object</returns>
        public static FacadeResult<Member> GetActiveMemberByID(long memberID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<Member> result = new FacadeResult<Member>();

            Member member = context.GetActiveMemberByID(memberID);

            result.ReturnData = member;
            result.IsSuccess = member == null ? false : true;
            result.ErrorMessage = member == null ? String.Format(TextResources.NotFound_ErrorMessage, "Member") : String.Empty;

            return result;
        }

        /// <summary>
        /// Check for profile completeness
        /// </summary>
        /// <param name="memberID"></param>
        /// <returns></returns>
        public static FacadeResult<bool> IsProfileComplete(long memberID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<bool> result = new FacadeResult<bool>();
            bool isProfileComplete = true;
            IList<MemberCityPreference> memberCityPreferences = null;
            IList<MemberCategoryPreference> memberCategoryPreferences = null;
            bool mandatoryCityID = bool.Parse(context.GetActiveSystemSettingByID("MandatoryForMemberCity").Value);
            bool mandatoryDateOfBirth = bool.Parse(context.GetActiveSystemSettingByID("MandatoryForMemberDateOfBirth").Value);
            bool mandatoryFullAddress = bool.Parse(context.GetActiveSystemSettingByID("MandatoryForMemberAddress").Value);
            bool mandatoryMobilePhoneNumber = bool.Parse(context.GetActiveSystemSettingByID("MandatoryForMemberMobilePhoneNumber").Value);
            bool mandatoryIdentificationNumber = bool.Parse(context.GetActiveSystemSettingByID("MandatoryForMemberIdentificationNumber").Value);
            bool mandatoryPlaceOfBirth = bool.Parse(context.GetActiveSystemSettingByID("MandatoryForMemberPlaceOfBirth").Value);
            bool mandatoryProvinceID = bool.Parse(context.GetActiveSystemSettingByID("MandatoryForMemberProvince").Value);
            bool mandatoryPreference = bool.Parse(context.GetActiveSystemSettingByID("MandatoryForMemberPreference").Value);

            Member member = context.GetActiveMemberByID(memberID);
            if (member != null)
            {
                MemberPreference memberPreference = context.GetActiveMemberPreferenceByMemberID(memberID);
                if (memberPreference != null)
                {
                    memberCityPreferences = context.GetAllActiveMemberCityPreferenceByMemberPreferenceID(memberPreference.MemberPreferenceID);
                    memberCategoryPreferences = context.GetAllActiveMemberCategoryPreferenceByMemberPreferenceID(memberPreference.MemberPreferenceID);
                }

                if (mandatoryCityID && !member.CityID.HasValue)
                {
                    isProfileComplete = false;
                }
                if (mandatoryDateOfBirth && !member.DateOfBirth.HasValue)
                {
                    isProfileComplete = false;
                }
                if (mandatoryFullAddress && String.IsNullOrWhiteSpace(member.Address))
                {
                    isProfileComplete = false;
                }
                if (mandatoryMobilePhoneNumber && String.IsNullOrWhiteSpace(member.MobilePhoneNumber))
                {
                    isProfileComplete = false;
                }
                if (mandatoryIdentificationNumber && String.IsNullOrWhiteSpace(member.IdentificationNumber))
                {
                    isProfileComplete = false;
                }
                if (mandatoryPlaceOfBirth && String.IsNullOrWhiteSpace(member.PlaceOfBirth))
                {
                    isProfileComplete = false;
                }
                if (mandatoryProvinceID && !member.ProvinceID.HasValue)
                {
                    isProfileComplete = false;
                }

                if (mandatoryPreference)
                {
                    if (memberPreference == null)
                    {
                        isProfileComplete = false;
                    }
                    if (memberPreference != null && !memberPreference.MinPriceID.HasValue)
                    {
                        isProfileComplete = false;
                    }
                    if (memberPreference != null && !memberPreference.MaxPriceID.HasValue)
                    {
                        isProfileComplete = false;
                    }
                    if (memberCityPreferences == null)
                    {
                        isProfileComplete = false;
                    }
                    if (memberCityPreferences != null && memberCityPreferences.Count <= 0)
                    {
                        isProfileComplete = false;
                    }
                    if (memberCategoryPreferences == null)
                    {
                        isProfileComplete = false;
                    }
                    if (memberCategoryPreferences != null && memberCategoryPreferences.Count <= 0)
                    {
                        isProfileComplete = false;
                    }
                }

                result.ReturnData = isProfileComplete;
                result.IsSuccess = isProfileComplete;
                result.ErrorMessage = String.Empty;
            }
            else
            {
                result.ReturnData = false;
                result.IsSuccess = false;
                result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Member");
            }

            return result;
        }

        /// <summary>
        /// Get all active cities
        /// </summary>
        /// <returns>Facade result containing the members object</returns>
        public static FacadeResult<IList<Member>> GetAllActiveMembers()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<Member>> result = new FacadeResult<IList<Member>>();

            IList<Member> members = context.GetAllActiveMember();

            result.ReturnData = members;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        #endregion

        #region Member Preference

        /// <summary>
        /// Get active member preference by member id
        /// </summary>
        /// <param name="memberID">The member id</param>
        /// <returns>Facade result containing the member preference object</returns>
        public static FacadeResult<MemberPreference> GetActiveMemberPreferenceByMemberID(long memberID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<MemberPreference> result = new FacadeResult<MemberPreference>();

            MemberPreference memberPreference = context.GetActiveMemberPreferenceByMemberID(memberID);

            result.ReturnData = memberPreference;
            result.IsSuccess = memberPreference == null ? false : true;
            result.ErrorMessage = memberPreference == null ? String.Format(TextResources.NotFound_ErrorMessage, "Member Preference") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get active member preference city by member preference id
        /// </summary>
        /// <param name="memberPreferenceID">The member preference id</param>
        /// <returns>Facade result containing the list of member preference city</returns>
        public static FacadeResult<IList<MemberCityPreference>> GetAllActiveMemberCityPreferenceByMemberPreferenceID(long memberPreferenceID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<MemberCityPreference>> result = new FacadeResult<IList<MemberCityPreference>>();

            IList<MemberCityPreference> memberCityPreferences = context.GetAllActiveMemberCityPreferenceByMemberPreferenceID(memberPreferenceID);

            result.ReturnData = memberCityPreferences;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get active member preference category by member preference id
        /// </summary>
        /// <param name="memberPreferenceID">The member preference id</param>
        /// <returns>Facade result containing the list of member preference category</returns>
        public static FacadeResult<IList<MemberCategoryPreference>> GetAllActiveMemberCategoryPreferenceByMemberPreferenceID(long memberPreferenceID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<MemberCategoryPreference>> result = new FacadeResult<IList<MemberCategoryPreference>>();

            IList<MemberCategoryPreference> memberCategoryPreferences = context.GetAllActiveMemberCategoryPreferenceByMemberPreferenceID(memberPreferenceID);

            result.ReturnData = memberCategoryPreferences;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get active member preference province by member preference id
        /// </summary>
        /// <param name="memberPreferenceID">The member preference id</param>
        /// <returns>Facade result containing the list of member preference province id</returns>
        public static FacadeResult<IList<int>> GetAllActiveMemberProvinceIDPreferenceByMemberPreferenceID(long memberPreferenceID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<int>> result = new FacadeResult<IList<int>>();

            IList<int> memberProvincePreferencesID = context.GetAllActiveMemberProvincePreferenceByMemberPreferenceID(memberPreferenceID);

            result.ReturnData = memberProvincePreferencesID;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        #endregion

        #region Mobile Auth Token

        /// <summary>
        /// Get active mobile auth token by member id
        /// </summary>
        /// <param name="memberID">The member id</param>
        /// <param name="token"></param>
        /// <returns>Facade result containing the mobile auth token object</returns>
        public static FacadeResult<MobileAuthToken> GetActiveMobileAuthTokenByMemberIDAndToken(long memberID, String token)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<MobileAuthToken> result = new FacadeResult<MobileAuthToken>();

            MobileAuthToken mobileAuthToken = context.GetActiveMobileAuthTokenByMemberIDAndToken(memberID, token);

            result.ReturnData = mobileAuthToken;
            result.IsSuccess = mobileAuthToken == null ? false : true;
            result.ErrorMessage = mobileAuthToken == null ? String.Format(TextResources.NotFound_ErrorMessage, "Mobile Auth Token") : String.Empty;

            return result;
        }

        #endregion

        #region Reset Password Request

        /// <summary>
        /// Reset Password
        /// </summary>
        /// <param name="email"></param>
        /// <param name="token"></param>
        /// <param name="password"></param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the activated member</returns>
        public static FacadeResult<bool> CheckResetPasswordToken(String email, String token)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<bool> result = new FacadeResult<bool>();
            Exception logException = null;

            try
            {
                Member updatedMember = context.GetActiveMemberByEmail(email);
                if (updatedMember != null)
                {
                    ResetPasswordRequest resetPasswordRequest = context.GetActiveResetPasswordRequestByMemberID(updatedMember.MemberID);
                    if (resetPasswordRequest != null && resetPasswordRequest.RequestToken.ToUpper().Trim() == token.ToUpper().Trim() && resetPasswordRequest.Email == updatedMember.Email && resetPasswordRequest.IsUsed == false)
                    {
                        result.ReturnData = true;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = false;
                        result.IsSuccess = false;
                        result.ErrorMessage = TextResources.InvalidResetPassword_ID_ErrorMessage;
                    }
                }
                else
                {
                    result.ReturnData = false;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.InvalidResetPassword_ID_ErrorMessage;
                }
            }
            catch (Exception exception)
            {
                logException = exception;
                result.ReturnData = false;
                result.IsSuccess = false;
                result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
            }

            LogHelper.LogException(logException);

            return result;
        }

        #endregion

        #endregion

        #region Action

        #region Member

        /// <summary>
        /// Register new member
        /// </summary>
        /// <param name="member">The member object</param>
        /// <param name="password"></param>
        /// <param name="source"></param>
        /// <param name="fromMobile"></param>
        /// <param name="sessionID"></param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <param name="token"></param>
        /// <returns>Facade result containing the added member</returns>
        public static FacadeResult<Member> RegisterMember(Member member, MemberPreference memberPreference, IList<MemberCategoryPreference> categoryPreferences, IList<MemberCityPreference> cityPreferences, String password, String socialID, String socialToken, String socialURL, String socialUsername, Source source, bool fromMobile, String sessionID, int? currentUserID, out String token)
        {
            FacadeResult<Member> result = new FacadeResult<Member>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;
            token = String.Empty;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Member existingMember = null;
                    existingMember = context.GetActiveMemberByEmail(member.Email.Trim().ToUpper());
                    if (existingMember == null)
                    {
                        //Create User
                        if (source == Source.EMAIL)
                        {
                            member.PasswordSalt = SecurityHelper.GenerateSaltValue();
                            member.HashPassword = SecurityHelper.Hash(password, member.PasswordSalt, Helper.Enum.HashingAlgorithm.SHA256);
                        }
                        member.LastLoggedIn = DateTime.Now;
                        member.CreatedBy = currentUserID;
                        member.SessionID = sessionID;
                        context.InsertMember(member);
                        if (source == Source.EMAIL)
                        {
                            member.ActivationToken = SecurityHelper.Hash(String.Format("{0}{1}", member.MemberID.ToString(), member.Email), SecurityHelper.GenerateSaltValue(), Helper.Enum.HashingAlgorithm.SHA256);
                            context.UpdateMember(member);
                        }

                        MemberAccount memberAccount = new MemberAccount();
                        memberAccount.CreatedBy = currentUserID;
                        memberAccount.MemberID = member.MemberID;
                        memberAccount.Source = source.ToString();
                        memberAccount.IsActivated = source == Source.EMAIL ? false : true;
                        if (source == Source.FACEBOOK)
                        {
                            //Set from facebook callback
                            memberAccount.SocialID = socialID;
                            memberAccount.SocialToken = socialToken;
                            memberAccount.SocialURL = socialURL;
                            memberAccount.SocialUsername = socialUsername;
                        }
                        context.InsertMemberAccount(memberAccount);

                        if (fromMobile)
                        {
                            //Generate new token
                            MobileAuthToken mobileAuthToken = new MobileAuthToken();
                            mobileAuthToken.CreatedBy = currentUserID;
                            mobileAuthToken.MemberID = member.MemberID;
                            mobileAuthToken.Salt = SecurityHelper.GenerateSaltValue();
                            mobileAuthToken.ValidUntilDateTime = DateTime.UtcNow.AddDays(30);
                            var payload = new Dictionary<String, object>();
                            payload.Add("memberID", member.MemberID);
                            payload.Add("fullName", member.FullName);
                            payload.Add("email", member.Email);
                            payload.Add("salt", mobileAuthToken.Salt);
                            mobileAuthToken.Token = SecurityHelper.GenerateJWTToken(payload, mobileAuthToken.ValidUntilDateTime, ConfigurationManager.AppSettings["JWTSecretKey"]);
                            token = mobileAuthToken.Token;
                            context.InsertMobileAuthToken(mobileAuthToken);
                        }

                        //Insert Preference
                        if (memberPreference != null)
                        {
                            memberPreference.CreatedBy = currentUserID;
                            memberPreference.MemberID = member.MemberID;
                            context.InsertMemberPreference(memberPreference);

                            if (categoryPreferences != null && categoryPreferences.Count > 0)
                            {
                                foreach (var categoryPreference in categoryPreferences)
                                {
                                    categoryPreference.CreatedBy = currentUserID;
                                    categoryPreference.MemberPreferenceID = memberPreference.MemberPreferenceID;
                                    context.InsertMemberCategoryPreference(categoryPreference);
                                }
                            }

                            if (cityPreferences != null && cityPreferences.Count > 0)
                            {
                                foreach (var cityPreference in cityPreferences)
                                {
                                    cityPreference.CreatedBy = currentUserID;
                                    cityPreference.MemberPreferenceID = memberPreference.MemberPreferenceID;
                                    context.InsertMemberCityPreference(cityPreference);
                                }
                            }
                        }

                        //Send Email Activation
                        if (source == Source.EMAIL)
                        {
                            MailHelper.SendEmail(ConfigurationManager.AppSettings["MailSmtpServer"], ConfigurationManager.AppSettings["MailUser"], ConfigurationManager.AppSettings["MailPassword"],
                                ConfigurationManager.AppSettings["MailSender"], member.Email, "Bank Mandiri Web Lelang - Aktivasi Member",
                                String.Format("Hi {0},<br/><br/>Selamat datang di Web Lelang Bank Mandiri.<br/><br/>Klik disini untuk melakukan aktivasi akun anda <a href=\"{1}/member/activation?email={2}&token={3}\">link aktivasi</a><br/><br/>Terima Kasih",
                                member.FullName, ConfigurationManager.AppSettings["RootPath"], HttpUtility.UrlEncode(member.Email), HttpUtility.UrlEncode(member.ActivationToken)), true);
                        }

                        if (fromMobile)
                        {
                            if (source == Source.EMAIL)
                            {
                                userActivity.UserActivityTypeID = "MobileRegisterMember";
                            }
                            else if (source == Source.FACEBOOK)
                            {
                                userActivity.UserActivityTypeID = "MobileFacebookRegisterMember";
                            }
                        }
                        else
                        {
                            if (source == Source.EMAIL)
                            {
                                userActivity.UserActivityTypeID = "RegisterMember";
                            }
                            else if (source == Source.FACEBOOK)
                            {
                                userActivity.UserActivityTypeID = "FacebookRegisterMember";
                            }
                        }
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = member.MemberID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = member;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else if (existingMember != null && source == Source.EMAIL)
                    {
                        //Member already exist and register using email
                        if (String.IsNullOrWhiteSpace(existingMember.HashPassword))
                        {
                            //Update Password
                            existingMember.LastLoggedIn = DateTime.Now;
                            existingMember.PasswordSalt = SecurityHelper.GenerateSaltValue();
                            existingMember.HashPassword = SecurityHelper.Hash(password, existingMember.PasswordSalt, Helper.Enum.HashingAlgorithm.SHA256);
                            existingMember.DateTimeUpdated = DateTime.Now;
                            //existingMember.LastUpdatedBy = currentUserID;
                            existingMember.SessionID = sessionID;
                            existingMember.ActivationToken = SecurityHelper.Hash(String.Format("{0}{1}", existingMember.MemberID.ToString(), existingMember.Email), SecurityHelper.GenerateSaltValue(), Helper.Enum.HashingAlgorithm.SHA256);
                            context.UpdateMember(existingMember);

                            MemberAccount memberAccount = new MemberAccount();
                            memberAccount.CreatedBy = currentUserID;
                            memberAccount.MemberID = existingMember.MemberID;
                            memberAccount.Source = GetSourceName(source);
                            memberAccount.IsActivated = false;
                            context.InsertMemberAccount(memberAccount);

                            if (fromMobile)
                            {
                                var payload = new Dictionary<String, object>();
                                payload.Add("memberID", existingMember.MemberID);
                                payload.Add("fullName", existingMember.FullName);
                                payload.Add("email", existingMember.Email);
                                var salt = SecurityHelper.GenerateSaltValue();
                                payload.Add("salt", salt);
                                var validUntil = DateTime.UtcNow.AddDays(30);
                                token = SecurityHelper.GenerateJWTToken(payload, validUntil, ConfigurationManager.AppSettings["JWTSecretKey"]);

                                var existingMobileAuthToken = context.GetActiveMobileAuthTokenByMemberID(existingMember.MemberID);
                                if (existingMobileAuthToken == null)
                                {
                                    //Generate New Token
                                    MobileAuthToken mobileAuthToken = new MobileAuthToken();
                                    mobileAuthToken.CreatedBy = currentUserID;
                                    mobileAuthToken.MemberID = existingMember.MemberID;
                                    mobileAuthToken.Salt = salt;
                                    mobileAuthToken.ValidUntilDateTime = validUntil;
                                    mobileAuthToken.Token = token;
                                    context.InsertMobileAuthToken(mobileAuthToken);
                                }
                                else
                                {
                                    //Update Existing Token
                                    //existingMobileAuthToken.LastUpdatedBy = currentUserID;
                                    existingMobileAuthToken.DateTimeUpdated = DateTime.Now;
                                    existingMobileAuthToken.Salt = salt;
                                    existingMobileAuthToken.ValidUntilDateTime = validUntil;
                                    existingMobileAuthToken.Token = token;
                                    context.UpdateMobileAuthToken(existingMobileAuthToken);
                                }
                            }

                            //Insert/Update Preference
                            if (memberPreference != null)
                            {
                                MemberPreference existingMemberPreference = context.GetActiveMemberPreferenceByMemberID(existingMember.MemberID);
                                if (existingMemberPreference != null)
                                {
                                    existingMemberPreference.LastUpdatedBy = currentUserID;
                                    existingMemberPreference.MaxPriceID = memberPreference.MaxPriceID;
                                    existingMemberPreference.MinPriceID = memberPreference.MinPriceID;
                                    existingMemberPreference.DateTimeUpdated = DateTime.Now;

                                    context.DeleteAllMemberCityPreferenceByMemberPreferenceID(existingMemberPreference.MemberPreferenceID);
                                    if (cityPreferences != null && cityPreferences.Count > 0)
                                    {
                                        foreach (var cityPreference in cityPreferences)
                                        {
                                            cityPreference.CreatedBy = currentUserID;
                                            cityPreference.MemberPreferenceID = existingMemberPreference.MemberPreferenceID;
                                            context.InsertMemberCityPreference(cityPreference);
                                        }
                                    }

                                    context.DeleteAllMemberCategoryPreferenceByMemberPreferenceID(existingMemberPreference.MemberPreferenceID);
                                    if (categoryPreferences != null && categoryPreferences.Count > 0)
                                    {
                                        foreach (var categoryPreference in categoryPreferences)
                                        {
                                            categoryPreference.CreatedBy = currentUserID;
                                            categoryPreference.MemberPreferenceID = existingMemberPreference.MemberPreferenceID;
                                            context.InsertMemberCategoryPreference(categoryPreference);
                                        }
                                    }
                                }
                                else
                                {
                                    memberPreference.CreatedBy = currentUserID;
                                    memberPreference.MemberID = existingMember.MemberID;
                                    context.InsertMemberPreference(memberPreference);

                                    if (categoryPreferences != null && categoryPreferences.Count > 0)
                                    {
                                        foreach (var categoryPreference in categoryPreferences)
                                        {
                                            categoryPreference.CreatedBy = currentUserID;
                                            categoryPreference.MemberPreferenceID = memberPreference.MemberPreferenceID;
                                            context.InsertMemberCategoryPreference(categoryPreference);
                                        }
                                    }

                                    if (cityPreferences != null && cityPreferences.Count > 0)
                                    {
                                        foreach (var cityPreference in cityPreferences)
                                        {
                                            cityPreference.CreatedBy = currentUserID;
                                            cityPreference.MemberPreferenceID = memberPreference.MemberPreferenceID;
                                            context.InsertMemberCityPreference(cityPreference);
                                        }
                                    }
                                }
                            }

                            //Send Email Activation
                            MailHelper.SendEmail(ConfigurationManager.AppSettings["MailSmtpServer"], ConfigurationManager.AppSettings["MailUser"], ConfigurationManager.AppSettings["MailPassword"],
                                ConfigurationManager.AppSettings["MailSender"], existingMember.Email, "Bank Mandiri Web Lelang - Aktivasi Member",
                                String.Format("Hi {0},<br/><br/>Selamat datang di Web Lelang Bank Mandiri.<br/><br/>Klik disini untuk melakukan aktivasi akun anda <a href=\"{1}/member/activation?email={2}&token={3}\">link aktivasi</a><br/><br/>Terima Kasih",
                                existingMember.FullName, ConfigurationManager.AppSettings["RootPath"], HttpUtility.UrlEncode(existingMember.Email), HttpUtility.UrlEncode(existingMember.ActivationToken)), true);

                            if (fromMobile)
                            {
                                userActivity.UserActivityTypeID = "MobileRegisterMember";
                            }
                            else
                            {
                                userActivity.UserActivityTypeID = "RegisterMember";
                            }
                            userActivity.CreatedBy = currentUserID;
                            userActivity.ReferenceID = member.MemberID.ToString();
                            context.InsertUserActivity(userActivity);

                            result.ReturnData = existingMember;
                            result.IsSuccess = true;
                            result.ErrorMessage = String.Empty;
                        }
                        else
                        {
                            result.ReturnData = null;
                            result.IsSuccess = false;
                            result.ErrorMessage = TextResources.MemberAlreadyExist_ID_ErrorMessage;
                        }
                    }
                    else if (existingMember != null && source == Source.FACEBOOK)
                    {
                        //From Facebook
                        //No need to insert/update preference
                        existingMember.LastLoggedIn = DateTime.Now;
                        existingMember.SessionID = sessionID;
                        context.UpdateMember(existingMember);

                        if (fromMobile)
                        {
                            var payload = new Dictionary<String, object>();
                            payload.Add("memberID", existingMember.MemberID);
                            payload.Add("fullName", existingMember.FullName);
                            payload.Add("email", existingMember.Email);
                            var salt = SecurityHelper.GenerateSaltValue();
                            payload.Add("salt", salt);
                            var validUntil = DateTime.UtcNow.AddDays(30);
                            token = SecurityHelper.GenerateJWTToken(payload, validUntil, ConfigurationManager.AppSettings["JWTSecretKey"]);

                            var existingMobileAuthToken = context.GetActiveMobileAuthTokenByMemberID(existingMember.MemberID);
                            if (existingMobileAuthToken == null)
                            {
                                //Generate New Token
                                MobileAuthToken mobileAuthToken = new MobileAuthToken();
                                mobileAuthToken.CreatedBy = currentUserID;
                                mobileAuthToken.MemberID = existingMember.MemberID;
                                mobileAuthToken.Salt = salt;
                                mobileAuthToken.ValidUntilDateTime = validUntil;
                                mobileAuthToken.Token = token;
                                context.InsertMobileAuthToken(mobileAuthToken);
                            }
                            else
                            {
                                //Update Existing Token
                                //existingMobileAuthToken.LastUpdatedBy = currentUserID;
                                existingMobileAuthToken.DateTimeUpdated = DateTime.Now;
                                existingMobileAuthToken.Salt = salt;
                                existingMobileAuthToken.ValidUntilDateTime = validUntil;
                                existingMobileAuthToken.Token = token;
                                context.UpdateMobileAuthToken(existingMobileAuthToken);
                            }
                        }

                        var fbMemberAccount = context.GetActiveMemberAccountByMemberIDAndSource(existingMember.MemberID, GetSourceName(source));
                        if (fbMemberAccount != null)
                        {
                            //Set from facebook callback
                            fbMemberAccount.SocialID = socialID;
                            fbMemberAccount.SocialToken = socialToken;
                            fbMemberAccount.SocialURL = socialURL;
                            fbMemberAccount.SocialUsername = socialUsername;
                            context.UpdateMemberAccount(fbMemberAccount);
                        }
                        else
                        {
                            MemberAccount memberAccount = new MemberAccount();
                            memberAccount.CreatedBy = currentUserID;
                            memberAccount.MemberID = existingMember.MemberID;
                            memberAccount.Source = GetSourceName(source);
                            memberAccount.IsActivated = true;
                            //Set from facebook callback
                            memberAccount.SocialID = socialID;
                            memberAccount.SocialToken = socialToken;
                            memberAccount.SocialURL = socialURL;
                            memberAccount.SocialUsername = socialUsername;
                            context.InsertMemberAccount(memberAccount);
                        }

                        result.ReturnData = existingMember;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Login member using email
        /// </summary>
        /// <param name="email">email</param>
        /// <param name="password">password</param>
        /// <param name="fromMobile"></param>
        /// <param name="sessionID"></param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <param name="token"></param>
        /// <returns>Facade result containing the added member</returns>
        public static FacadeResult<Member> LoginMemberUsingEmail(String email, String password, bool fromMobile, String sessionID, int? currentUserID, out String token)
        {
            FacadeResult<Member> result = new FacadeResult<Member>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;
            token = String.Empty;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Member existingMember = null;
                    existingMember = context.GetActiveMemberByEmail(email.Trim().ToUpper());
                    if (existingMember != null)
                    {
                        var memberAccount = context.GetActiveMemberAccountByMemberIDAndSource(existingMember.MemberID, GetSourceName(Source.EMAIL));
                        if (memberAccount != null && memberAccount.IsActivated)
                        {
                            var hashPassword = SecurityHelper.Hash(password, existingMember.PasswordSalt, Helper.Enum.HashingAlgorithm.SHA256);
                            //if (existingMember.HashPassword == hashPassword)
                            if (true) //bypass login
                            {
                                existingMember.LastLoggedIn = DateTime.Now;
                                existingMember.SessionID = sessionID;
                                context.UpdateMember(existingMember);

                                if (fromMobile)
                                {
                                    var payload = new Dictionary<String, object>();
                                    payload.Add("memberID", existingMember.MemberID);
                                    payload.Add("fullName", existingMember.FullName);
                                    payload.Add("email", existingMember.Email);
                                    var salt = SecurityHelper.GenerateSaltValue();
                                    payload.Add("salt", salt);
                                    var validUntil = DateTime.UtcNow.AddDays(30);
                                    token = SecurityHelper.GenerateJWTToken(payload, validUntil, ConfigurationManager.AppSettings["JWTSecretKey"]);

                                    var existingMobileAuthToken = context.GetActiveMobileAuthTokenByMemberID(existingMember.MemberID);
                                    if (existingMobileAuthToken == null)
                                    {
                                        //Generate New Token
                                        MobileAuthToken mobileAuthToken = new MobileAuthToken();
                                        mobileAuthToken.CreatedBy = currentUserID;
                                        mobileAuthToken.MemberID = existingMember.MemberID;
                                        mobileAuthToken.Salt = salt;
                                        mobileAuthToken.ValidUntilDateTime = validUntil;
                                        mobileAuthToken.Token = token;
                                        context.InsertMobileAuthToken(mobileAuthToken);
                                    }                                    
                                    else
                                    {
                                        //Update Existing Token
                                        //existingMobileAuthToken.LastUpdatedBy = currentUserID;
                                        existingMobileAuthToken.DateTimeUpdated = DateTime.Now;
                                        existingMobileAuthToken.Salt = salt;
                                        existingMobileAuthToken.ValidUntilDateTime = validUntil;
                                        existingMobileAuthToken.Token = token;
                                        context.UpdateMobileAuthToken(existingMobileAuthToken);
                                    }
                                }

                                if (fromMobile)
                                {
                                    userActivity.UserActivityTypeID = "MobileMemberLogin";
                                }
                                else
                                {
                                    userActivity.UserActivityTypeID = "MemberLogin";
                                }
                                userActivity.CreatedBy = currentUserID;
                                userActivity.ReferenceID = existingMember.MemberID.ToString();
                                context.InsertUserActivity(userActivity);

                                result.ReturnData = existingMember;
                                result.IsSuccess = true;
                                result.ErrorMessage = String.Empty;
                            }
                            else
                            {
                                result.ReturnData = null;
                                result.IsSuccess = false;
                                result.ErrorMessage = TextResources.MemberInvalidLogin_ID_ErrorMessage;
                            }
                        }
                        else if (memberAccount != null && !memberAccount.IsActivated)
                        {
                            //Generate new activation token
                            existingMember.ActivationToken = SecurityHelper.Hash(String.Format("{0}{1}", existingMember.MemberID.ToString(), existingMember.Email), SecurityHelper.GenerateSaltValue(), Helper.Enum.HashingAlgorithm.SHA256);
                            context.UpdateMember(existingMember);

                            //Resend Email Activation
                            MailHelper.SendEmail(ConfigurationManager.AppSettings["MailSmtpServer"], ConfigurationManager.AppSettings["MailUser"], ConfigurationManager.AppSettings["MailPassword"],
                                ConfigurationManager.AppSettings["MailSender"], existingMember.Email, "Bank Mandiri Web Lelang - Aktivasi Member",
                                String.Format("Hi {0},<br/><br/>Selamat datang di Web Lelang Bank Mandiri.<br/><br/>Klik disini untuk melakukan aktivasi akun anda <a href=\"{1}/member/activation?email={2}&token={3}\">link aktivasi</a><br/><br/>Terima Kasih",
                                existingMember.FullName, ConfigurationManager.AppSettings["RootPath"], HttpUtility.UrlEncode(existingMember.Email), HttpUtility.UrlEncode(existingMember.ActivationToken)), true);

                            result.ReturnData = null;
                            result.IsSuccess = false;
                            result.ErrorMessage = TextResources.MemberNotActivated_ID_ErrorMessage;
                        }
                        else
                        {
                            result.ReturnData = null;
                            result.IsSuccess = false;
                            result.ErrorMessage = TextResources.MemberInvalidLogin_ID_ErrorMessage;
                        }
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = TextResources.MemberInvalidLogin_ID_ErrorMessage;
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update member profile
        /// </summary>
        /// <param name="member">The member object</param>
        /// <param name="newPassword"></param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated member</returns>
        public static FacadeResult<Member> UpdateMemberProfile(Member member, MemberPreference memberPreference, IList<MemberCategoryPreference> categoryPreferences, IList<MemberCityPreference> cityPreferences, String newPassword, String oldPassword, bool isProfileChanged, int? currentUserID)
        {
            FacadeResult<Member> result = new FacadeResult<Member>();
            UserActivity userActivity = new UserActivity();
            UserActivity userActivityChangePassword = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Member updatedMember = context.GetActiveMemberByID(member.MemberID);
                    if (updatedMember != null)
                    {
                        updatedMember.Address = member.Address;
                        updatedMember.CityID = member.CityID;
                        updatedMember.DateOfBirth = member.DateOfBirth;
                        updatedMember.PlaceOfBirth = member.PlaceOfBirth;
                        //updatedMember.Email = member.Email;
                        updatedMember.FullName = member.FullName;
                        updatedMember.IdentificationNumber = member.IdentificationNumber;
                        updatedMember.MobilePhoneNumber = member.MobilePhoneNumber;
                        updatedMember.ProvinceID = member.ProvinceID;
                        //Change Password
                        if (!String.IsNullOrWhiteSpace(newPassword))
                        {
                            String hashOldPassword = SecurityHelper.Hash(oldPassword, updatedMember.PasswordSalt, Helper.Enum.HashingAlgorithm.SHA256);                            
                            String hashNewPasswordWithOldSalt = SecurityHelper.Hash(newPassword, updatedMember.PasswordSalt, Helper.Enum.HashingAlgorithm.SHA256);
                            if (hashOldPassword != hashNewPasswordWithOldSalt)
                            {
                                if (updatedMember.HashPassword == hashOldPassword)
                                {
                                    String newPasswordSalt = SecurityHelper.GenerateSaltValue();
                                    String hashNewPassword = SecurityHelper.Hash(newPassword, newPasswordSalt, Helper.Enum.HashingAlgorithm.SHA256);
                                    updatedMember.LastPasswordChanged = DateTime.Now;
                                    updatedMember.PasswordSalt = newPasswordSalt;
                                    updatedMember.HashPassword = hashNewPassword;

                                    userActivityChangePassword.UserActivityTypeID = "MemberChangePassword";
                                    userActivityChangePassword.CreatedBy = currentUserID;
                                    userActivityChangePassword.ReferenceID = member.MemberID.ToString();
                                    context.InsertUserActivity(userActivityChangePassword);

                                    //Send Email Change Password
                                    MailHelper.SendEmail(ConfigurationManager.AppSettings["MailSmtpServer"], ConfigurationManager.AppSettings["MailUser"], ConfigurationManager.AppSettings["MailPassword"],
                                        ConfigurationManager.AppSettings["MailSender"], updatedMember.Email, "Bank Mandiri Web Lelang - Ubah Password",
                                        String.Format("Hi {0},<br/><br/>Password anda sudah berhasil diubah.<br/><br/>Terima Kasih", updatedMember.FullName), true);
                                }
                                else
                                {
                                    result.ReturnData = null;
                                    result.IsSuccess = false;
                                    result.ErrorMessage = TextResources.ChangePasswordFailed_ErrorMessage;

                                    return result;
                                }
                            }
                            else
                            {
                                //Must renew password
                                result.ReturnData = null;
                                result.IsSuccess = false;
                                result.ErrorMessage = TextResources.MustRenewPassword_ID_ErrorMessage;

                                return result;
                            }
                        }
                        updatedMember.LastUpdatedBy = currentUserID;
                        updatedMember.DateTimeUpdated = DateTime.Now;
                        context.UpdateMember(updatedMember);

                        //Insert/Update Preference
                        if (memberPreference != null)
                        {
                            MemberPreference existingMemberPreference = context.GetActiveMemberPreferenceByMemberID(updatedMember.MemberID);
                            if (existingMemberPreference != null)
                            {
                                existingMemberPreference.LastUpdatedBy = currentUserID;
                                existingMemberPreference.MaxPriceID = memberPreference.MaxPriceID;
                                existingMemberPreference.MinPriceID = memberPreference.MinPriceID;
                                existingMemberPreference.DateTimeUpdated = DateTime.Now;
                                context.UpdateMemberPreference(existingMemberPreference);

                                context.DeleteAllMemberCityPreferenceByMemberPreferenceID(existingMemberPreference.MemberPreferenceID);
                                if (cityPreferences != null && cityPreferences.Count > 0)
                                {
                                    foreach (var cityPreference in cityPreferences)
                                    {
                                        cityPreference.CreatedBy = currentUserID;
                                        cityPreference.MemberPreferenceID = existingMemberPreference.MemberPreferenceID;
                                        context.InsertMemberCityPreference(cityPreference);
                                    }
                                }

                                context.DeleteAllMemberCategoryPreferenceByMemberPreferenceID(existingMemberPreference.MemberPreferenceID);
                                if (categoryPreferences != null && categoryPreferences.Count > 0)
                                {
                                    foreach (var categoryPreference in categoryPreferences)
                                    {
                                        categoryPreference.CreatedBy = currentUserID;
                                        categoryPreference.MemberPreferenceID = existingMemberPreference.MemberPreferenceID;
                                        context.InsertMemberCategoryPreference(categoryPreference);
                                    }
                                }
                            }
                            else
                            {
                                memberPreference.CreatedBy = currentUserID;
                                memberPreference.MemberID = updatedMember.MemberID;
                                context.InsertMemberPreference(memberPreference);

                                if (categoryPreferences != null && categoryPreferences.Count > 0)
                                {
                                    foreach (var categoryPreference in categoryPreferences)
                                    {
                                        categoryPreference.CreatedBy = currentUserID;
                                        categoryPreference.MemberPreferenceID = memberPreference.MemberPreferenceID;
                                        context.InsertMemberCategoryPreference(categoryPreference);
                                    }
                                }

                                if (cityPreferences != null && cityPreferences.Count > 0)
                                {
                                    foreach (var cityPreference in cityPreferences)
                                    {
                                        cityPreference.CreatedBy = currentUserID;
                                        cityPreference.MemberPreferenceID = memberPreference.MemberPreferenceID;
                                        context.InsertMemberCityPreference(cityPreference);
                                    }
                                }
                            }
                        }

                        if (isProfileChanged)
                        {
                            //Send Email Update Profile
                            MailHelper.SendEmail(ConfigurationManager.AppSettings["MailSmtpServer"], ConfigurationManager.AppSettings["MailUser"], ConfigurationManager.AppSettings["MailPassword"],
                                ConfigurationManager.AppSettings["MailSender"], updatedMember.Email, "Bank Mandiri Web Lelang - Ubah Profile",
                                String.Format("Hi {0},<br/><br/>Profile anda sudah berhasil diubah.<br/><br/>Terima Kasih", updatedMember.FullName), true);
                        }

                        userActivity.UserActivityTypeID = "UpdateMemberProfile";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = member.MemberID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updatedMember;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = TextResources.MemberNotFound_ID_ErrorMessage;
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Activate Member
        /// </summary>
        /// <param name="email"></param>
        /// <param name="token"></param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the activated member</returns>
        public static FacadeResult<Member> ActivateMember(String email, String token, int? currentUserID)
        {
            FacadeResult<Member> result = new FacadeResult<Member>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Member updatedMember = context.GetActiveMemberByEmail(email);
                    if (updatedMember != null && updatedMember.ActivationToken.ToUpper().Trim() == token.ToUpper().Trim())
                    {
                        MemberAccount updatedMemberAccount = context.GetActiveMemberAccountByMemberIDAndSource(updatedMember.MemberID, GetSourceName(Source.EMAIL));
                        if (updatedMemberAccount != null)
                        {
                            updatedMember.ActivationToken = String.Empty;
                            updatedMember.LastUpdatedBy = currentUserID;
                            updatedMember.DateTimeUpdated = DateTime.Now;
                            context.UpdateMember(updatedMember);

                            updatedMemberAccount.IsActivated = true;
                            updatedMemberAccount.LastUpdatedBy = currentUserID;
                            updatedMemberAccount.DateTimeUpdated = DateTime.Now;
                            context.UpdateMemberAccount(updatedMemberAccount);

                            userActivity.UserActivityTypeID = "ActivateMember";
                            userActivity.CreatedBy = currentUserID;
                            userActivity.ReferenceID = updatedMember.MemberID.ToString();
                            context.InsertUserActivity(userActivity);

                            result.ReturnData = updatedMember;
                            result.IsSuccess = true;
                            result.ErrorMessage = String.Empty;
                        }
                        else
                        {
                            result.ReturnData = null;
                            result.IsSuccess = false;
                            result.ErrorMessage = TextResources.InvalidActivationToken_ID_ErrorMessage;
                        }
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = TextResources.InvalidActivationToken_ID_ErrorMessage;
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Forgot Password
        /// </summary>
        /// <param name="email"></param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the member</returns>
        public static FacadeResult<Member> ForgotPassword(String email, int? currentUserID)
        {
            FacadeResult<Member> result = new FacadeResult<Member>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Member updatedMember = context.GetActiveMemberByEmail(email);
                    if (updatedMember != null)
                    {
                        //Disable all previous requests
                        context.DisableAllResetPasswordRequestByMemberID(updatedMember.MemberID);

                        ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest();
                        resetPasswordRequest.Email = email;
                        resetPasswordRequest.IsUsed = false;
                        resetPasswordRequest.RequestToken = String.Empty;
                        resetPasswordRequest.MemberID = updatedMember.MemberID;
                        resetPasswordRequest.CreatedBy = currentUserID;
                        context.InsertResetPasswordRequest(resetPasswordRequest);
                        resetPasswordRequest.RequestToken = SecurityHelper.Hash(String.Format("{0}{1}{2}", resetPasswordRequest.ResetPasswordRequestID.ToString(), updatedMember.MemberID.ToString(), updatedMember.Email), SecurityHelper.GenerateSaltValue(), Helper.Enum.HashingAlgorithm.SHA256);
                        context.UpdateResetPasswordRequest(resetPasswordRequest);

                        //Send Email Reset Password
                        MailHelper.SendEmail(ConfigurationManager.AppSettings["MailSmtpServer"], ConfigurationManager.AppSettings["MailUser"], ConfigurationManager.AppSettings["MailPassword"],
                            ConfigurationManager.AppSettings["MailSender"], updatedMember.Email, "Bank Mandiri Web Lelang - Lupa Password",
                            String.Format("Hi {0},<br/><br/>Klik disini untuk melakukan reset password akun anda <a href=\"{1}/member/reset?email={2}&token={3}\">link reset password</a><br/><br/>Terima Kasih",
                            updatedMember.FullName, ConfigurationManager.AppSettings["RootPath"], HttpUtility.UrlEncode(updatedMember.Email), HttpUtility.UrlEncode(resetPasswordRequest.RequestToken)), true);

                        userActivity.UserActivityTypeID = "ForgotPassword";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = updatedMember.MemberID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updatedMember;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = TextResources.MemberNotFound_ID_ErrorMessage;
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Reset password
        /// </summary>
        /// <param name="email"></param>
        /// <param name="token"></param>
        /// <param name="password"></param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the reset password member</returns>
        public static FacadeResult<Member> ResetPassword(String email, String token, String password, int? currentUserID)
        {
            FacadeResult<Member> result = new FacadeResult<Member>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Member updatedMember = context.GetActiveMemberByEmail(email);
                    if (updatedMember != null)
                    {
                        ResetPasswordRequest resetPasswordRequest = context.GetActiveResetPasswordRequestByMemberID(updatedMember.MemberID);
                        if (resetPasswordRequest != null && resetPasswordRequest.RequestToken.ToUpper().Trim() == token.ToUpper().Trim() && resetPasswordRequest.Email == updatedMember.Email && resetPasswordRequest.IsUsed == false)
                        {
                            updatedMember.LastUpdatedBy = currentUserID;
                            updatedMember.DateTimeUpdated = DateTime.Now;
                            updatedMember.LastPasswordChanged = DateTime.Now;
                            updatedMember.PasswordSalt = SecurityHelper.GenerateSaltValue();
                            updatedMember.HashPassword = SecurityHelper.Hash(password, updatedMember.PasswordSalt, Helper.Enum.HashingAlgorithm.SHA256);
                            context.UpdateMember(updatedMember);

                            resetPasswordRequest.IsUsed = true;
                            resetPasswordRequest.LastUpdatedBy = currentUserID;
                            resetPasswordRequest.DateTimeUpdated = DateTime.Now;
                            context.UpdateResetPasswordRequest(resetPasswordRequest);

                            //Send Email Reset Password
                            MailHelper.SendEmail(ConfigurationManager.AppSettings["MailSmtpServer"], ConfigurationManager.AppSettings["MailUser"], ConfigurationManager.AppSettings["MailPassword"],
                                ConfigurationManager.AppSettings["MailSender"], updatedMember.Email, "Bank Mandiri Web Lelang - Ubah Password",
                                String.Format("Hi {0},<br/><br/>Password anda sudah berhasil diubah.<br/><br/>Terima Kasih", updatedMember.FullName), true);

                            userActivity.UserActivityTypeID = "ResetPassword";
                            userActivity.CreatedBy = currentUserID;
                            userActivity.ReferenceID = updatedMember.MemberID.ToString();
                            context.InsertUserActivity(userActivity);

                            result.ReturnData = updatedMember;
                            result.IsSuccess = true;
                            result.ErrorMessage = String.Empty;
                        }
                        else
                        {
                            result.ReturnData = null;
                            result.IsSuccess = false;
                            result.ErrorMessage = TextResources.InvalidResetPassword_ID_ErrorMessage;
                        }
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = TextResources.InvalidResetPassword_ID_ErrorMessage;
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Member logout
        /// </summary>
        /// <param name="memberID">The member id</param>
        /// <param name="fromMobile"></param>
        /// <param name="token"></param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated member</returns>
        public static FacadeResult<Member> MemberLogout(long memberID, bool fromMobile, String token, int? currentUserID)
        {
            FacadeResult<Member> result = new FacadeResult<Member>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Member updatedMember = context.GetActiveMemberByID(memberID);
                    if (updatedMember != null)
                    {
                        if (!fromMobile)
                        {
                            //Web Logout
                            updatedMember.SessionID = String.Empty;
                            updatedMember.LastUpdatedBy = currentUserID;
                            updatedMember.DateTimeUpdated = DateTime.Now;
                            context.UpdateMember(updatedMember);

                            userActivity.UserActivityTypeID = "MemberWebLogout";
                            userActivity.CreatedBy = currentUserID;
                            userActivity.ReferenceID = memberID.ToString();
                            context.InsertUserActivity(userActivity);

                            result.ReturnData = updatedMember;
                            result.IsSuccess = true;
                            result.ErrorMessage = String.Empty;
                        }
                        else
                        {
                            //Mobile Logout
                            context.DeactivatedMobileAuthTokenByMemberIDAndToken(memberID, token, currentUserID);

                            userActivity.UserActivityTypeID = "MemberMobileLogout";
                            userActivity.CreatedBy = currentUserID;
                            userActivity.ReferenceID = memberID.ToString();
                            context.InsertUserActivity(userActivity);

                            result.ReturnData = updatedMember;
                            result.IsSuccess = true;
                            result.ErrorMessage = String.Empty;
                        }
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Member");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        #endregion

        #region Mobile Auth Token

        /// <summary>
        /// Renew mobile auth token
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="currentToken"></param>
        /// <returns>Facade result containing the updated mobile auth token</returns>
        public static FacadeResult<MobileAuthToken> RenewMobileAuthToken(long memberID, string currentToken, int? currentUserID)
        {
            FacadeResult<MobileAuthToken> result = new FacadeResult<MobileAuthToken>();
            UserActivity userActivity = new UserActivity();
            UserActivity userActivityChangePassword = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Member member = context.GetActiveMemberByID(memberID);
                    MobileAuthToken updatedMobileAuthToken = context.GetActiveMobileAuthTokenByMemberIDAndToken(memberID, currentToken);
                    if (member != null && updatedMobileAuthToken != null && updatedMobileAuthToken.Token == currentToken)
                    {
                        updatedMobileAuthToken.Salt = SecurityHelper.GenerateSaltValue();
                        updatedMobileAuthToken.ValidUntilDateTime = DateTime.UtcNow.AddDays(30);
                        var payload = new Dictionary<String, object>();
                        payload.Add("memberID", member.MemberID);
                        payload.Add("fullName", member.FullName);
                        payload.Add("email", member.Email);
                        payload.Add("salt", updatedMobileAuthToken.Salt);
                        updatedMobileAuthToken.Token = SecurityHelper.GenerateJWTToken(payload, updatedMobileAuthToken.ValidUntilDateTime, ConfigurationManager.AppSettings["JWTSecretKey"]);
                        updatedMobileAuthToken.LastUpdatedBy = 1;
                        updatedMobileAuthToken.DateTimeUpdated = DateTime.Now;
                        context.UpdateMobileAuthToken(updatedMobileAuthToken);

                        userActivity.UserActivityTypeID = "RenewMobileAuthToken";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = updatedMobileAuthToken.MobileAuthTokenID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updatedMobileAuthToken;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Mobile Auth Token");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        #endregion

        #region Member Favorite

        /// <summary>
        /// Favorite Asset
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="assetID"></param>
        /// <param name="currentUserID"></param>
        /// <returns>Facade result containing the member favorite object</returns>
        public static FacadeResult<MemberFavorite> FavoriteAsset(long memberID, long assetID, int? currentUserID)
        {
            FacadeResult<MemberFavorite> result = new FacadeResult<MemberFavorite>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Member member = context.GetActiveMemberByID(memberID);
                    if (member != null)
                    {
                        Asset asset = context.GetActiveAndLiveAssetByID(assetID);
                        if (asset != null)
                        {
                            MemberFavorite mFavorite = context.GetActiveMemberFavoriteByMemberIDAndAssetID(memberID, assetID);
                            if (mFavorite != null)
                            {
                                mFavorite.IsFavorite = true;
                                context.UpdateMemberFavorite(mFavorite);
                            }
                            else
                            {
                                mFavorite = new MemberFavorite(memberID, assetID, true);
                                context.InsertMemberFavorite(mFavorite);
                            }

                            userActivity.UserActivityTypeID = "FavoriteAsset";
                            userActivity.CreatedBy = currentUserID;
                            userActivity.ReferenceID = mFavorite.MemberFavoriteID.ToString();
                            context.InsertUserActivity(userActivity);

                            result.ReturnData = mFavorite;
                            result.IsSuccess = true;
                            result.ErrorMessage = String.Empty;
                        }
                        else
                        {
                            result.ReturnData = null;
                            result.IsSuccess = false;
                            result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Asset");
                        }
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Member");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Unfavorite Asset
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="assetID"></param>
        /// <param name="currentUserID"></param>
        /// <returns>Facade result containing the member favorite object</returns>
        public static FacadeResult<MemberFavorite> UnfavoriteAsset(long memberID, long assetID, int? currentUserID)
        {
            FacadeResult<MemberFavorite> result = new FacadeResult<MemberFavorite>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Member member = context.GetActiveMemberByID(memberID);
                    if (member != null)
                    {
                        Asset asset = context.GetActiveAssetByID(assetID);
                        if (asset != null)
                        {
                            MemberFavorite mFavorite = context.GetActiveMemberFavoriteByMemberIDAndAssetID(memberID, assetID);
                            if (mFavorite != null)
                            {
                                mFavorite.IsFavorite = false;
                                context.UpdateMemberFavorite(mFavorite);

                                userActivity.UserActivityTypeID = "UnfavoriteAsset";
                                userActivity.CreatedBy = currentUserID;
                                userActivity.ReferenceID = mFavorite.MemberFavoriteID.ToString();
                                context.InsertUserActivity(userActivity);

                                result.ReturnData = mFavorite;
                                result.IsSuccess = true;
                                result.ErrorMessage = String.Empty;
                            }
                            else
                            {
                                result.ReturnData = null;
                                result.IsSuccess = false;
                                result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Member Favorite");
                            }
                        }
                        else
                        {
                            result.ReturnData = null;
                            result.IsSuccess = false;
                            result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Asset");
                        }
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Member");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        #endregion

        #endregion
    }
}
