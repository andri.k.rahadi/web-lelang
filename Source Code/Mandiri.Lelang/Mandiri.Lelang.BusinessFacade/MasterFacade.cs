﻿using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.DataRepository;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.Helper.Enum;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insight.Database;
using Mandiri.Lelang.ViewModel;

namespace Mandiri.Lelang.BusinessFacade
{
    public class MasterFacade : BaseFacade
    {
        #region Get

        #region All

        /// <summary>
        /// Get all master data
        /// </summary>
        /// <param name="lastTimeStamp"></param>
        /// <returns>Facade result containing the master data list object</returns>
        public static FacadeResult<IList<dynamic>> GetAllMasterData(String lastTimestamp = "1900-01-01")
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();

            if (String.IsNullOrWhiteSpace(lastTimestamp) || lastTimestamp == "0")
            {
                lastTimestamp = "1900-01-01";
            }

            IList<dynamic> masters = context.QuerySql<dynamic>(
                @"
                SELECT 'Category' AS Type, CAST(CategoryID AS VARCHAR) AS ID, '' AS ProvinceID, Name, ISNULL(Description, '') AS Description, 0 AS Value, IsActive,
	                CASE WHEN @LastTimeStamp = '1900-01-01' OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime) THEN 'Add'
	                ELSE 'Edit' END AS Flag
                FROM Category
                WHERE 
	                @LastTimeStamp = '1900-01-01' 
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime)
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeUpdated) > CAST(@LastTimeStamp AS DateTime)
                UNION ALL
                SELECT 'Province' AS Type, CAST(ProvinceID AS VARCHAR) AS ID, '' AS ProvinceID, Name, ISNULL(Description, '') AS Description, 0 AS Value, IsActive,
	                CASE WHEN @LastTimeStamp = '1900-01-01' OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime) THEN 'Add'
	                ELSE 'Edit' END AS Flag
                FROM Province
                WHERE 
	                @LastTimeStamp = '1900-01-01' 
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime)
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeUpdated) > CAST(@LastTimeStamp AS DateTime)
                UNION ALL
                SELECT 'City' AS Type, CAST(CityID AS VARCHAR) AS ID, ProvinceID, Name, ISNULL(Description, '') AS Description, 0 AS Value, IsActive,
	                CASE WHEN @LastTimeStamp = '1900-01-01' OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime) THEN 'Add'
	                ELSE 'Edit' END AS Flag
                FROM City
                WHERE 
	                @LastTimeStamp = '1900-01-01' 
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime)
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeUpdated) > CAST(@LastTimeStamp AS DateTime)
                UNION ALL
                SELECT 'MinMaxPrice' AS Type, CAST(MinMaxPriceID AS VARCHAR) AS ID, '' AS ProvinceID, Name, ISNULL(Description, '') AS Description, Value, IsActive,
	                CASE WHEN @LastTimeStamp = '1900-01-01' OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime) THEN 'Add'
	                ELSE 'Edit' END AS Flag
                FROM MinMaxPrice
                WHERE 
	                @LastTimeStamp = '1900-01-01' 
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime)
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeUpdated) > CAST(@LastTimeStamp AS DateTime)
                UNION ALL
                SELECT 'DocumentType' AS Type, CAST(OwnershipDocumentTypeID AS VARCHAR) AS ID, '' AS ProvinceID, Name, ISNULL(Description, '') AS Description, 0 AS Value, IsActive,
	                CASE WHEN @LastTimeStamp = '1900-01-01' OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime) THEN 'Add'
	                ELSE 'Edit' END AS Flag
                FROM OwnershipDocumentType
                WHERE 
	                @LastTimeStamp = '1900-01-01' 
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime)
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeUpdated) > CAST(@LastTimeStamp AS DateTime)
                UNION ALL
                SELECT 'ContactUsType' AS Type, CAST(MessageCategoryTypeID AS VARCHAR) AS ID, '' AS ProvinceID, Name, ISNULL(Description, '') AS Description, 0 AS Value, IsActive,
	                CASE WHEN @LastTimeStamp = '1900-01-01' OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime) THEN 'Add'
	                ELSE 'Edit' END AS Flag
                FROM MessageCategoryType
                WHERE 
	                @LastTimeStamp = '1900-01-01' 
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime)
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeUpdated) > CAST(@LastTimeStamp AS DateTime)
                UNION ALL
                SELECT 'Branch' AS Type, CAST(BranchID AS VARCHAR) AS ID, '' AS ProvinceID, Name, ISNULL(Description, '') AS Description, 0 AS Value, IsActive,
	                CASE WHEN @LastTimeStamp = '1900-01-01' OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime) THEN 'Add'
	                ELSE 'Edit' END AS Flag
                FROM Branch
                WHERE 
	                @LastTimeStamp = '1900-01-01' 
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime)
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeUpdated) > CAST(@LastTimeStamp AS DateTime)
                UNION ALL
                SELECT 'KPKNL' AS Type, CAST(KPKNLID AS VARCHAR) AS ID, '' AS ProvinceID, Name, ISNULL(Description, '') AS Description, 0 AS Value, IsActive,
	                CASE WHEN @LastTimeStamp = '1900-01-01' OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime) THEN 'Add'
	                ELSE 'Edit' END AS Flag
                FROM KPKNL                
                WHERE 
	                @LastTimeStamp = '1900-01-01' 
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime)
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeUpdated) > CAST(@LastTimeStamp AS DateTime)
                UNION ALL
                SELECT 'WebContent' AS Type, CAST(WebContentTypeID AS VARCHAR) AS ID, '' AS ProvinceID, Content AS Name, '' AS Description, 0 AS Value, IsActive,
	                CASE WHEN @LastTimeStamp = '1900-01-01' OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime) THEN 'Add'
	                ELSE 'Edit' END AS Flag
                FROM WebContent
                WHERE 
                    WebContentTypeID IN (7, 9, 10) AND
	                (@LastTimeStamp = '1900-01-01' 
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime)
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeUpdated) > CAST(@LastTimeStamp AS DateTime))  
                UNION ALL
                SELECT 'SystemSetting' AS Type, SystemSettingID AS ID, '' AS ProvinceID, CAST(Value AS VARCHAR) AS Name, CASE WHEN SystemSettingID LIKE '%Mandatory%' THEN 'Field Harus Diisi' ELSE '' END AS Description, 0 AS Value, IsActive,
	                CASE WHEN @LastTimeStamp = '1900-01-01' OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime) THEN 'Add'
	                ELSE 'Edit' END AS Flag
                FROM SystemSetting
                WHERE 
                    SystemSettingID NOT IN ('EmailReminderPeriod', 'MobileWebsitePopupImage', 'WebsitePopupImage', 'VisibilityDaysPeriodForPaidAndSold') AND
	                (@LastTimeStamp = '1900-01-01' 
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeCreated) > CAST(@LastTimeStamp AS DateTime)
	                OR DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DateTimeUpdated) > CAST(@LastTimeStamp AS DateTime))   
                ORDER BY Type, Name       
                "
                , new { LastTimeStamp = lastTimestamp });

            result.ReturnData = masters;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        #endregion

        #region Province

        /// <summary>
        /// Get active province by province id
        /// </summary>
        /// <param name="provinceID">The province id</param>
        /// <returns>Facade result containing the province object</returns>
        public static FacadeResult<Province> GetActiveProvinceByProvinceID(int provinceID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<Province> result = new FacadeResult<Province>();

            Province province = context.GetActiveProvinceByID(provinceID);

            result.ReturnData = province;
            result.IsSuccess = province == null ? false : true;
            result.ErrorMessage = province == null ? String.Format(TextResources.NotFound_ErrorMessage, "Province") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active provinces
        /// </summary>
        /// <returns>Facade result containing the provinces object</returns>
        public static FacadeResult<IList<Province>> GetAllActiveProvinces()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<Province>> result = new FacadeResult<IList<Province>>();

            IList<Province> provinces = context.GetAllActiveProvinces();

            result.ReturnData = provinces;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active provinces
        /// </summary>
        /// <returns>Facade result containing the provinces object</returns>
        public static FacadeResult<IList<Province>> GetAllActiveProvincesWithAll()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<Province>> result = new FacadeResult<IList<Province>>();

            IList<Province> provinces = context.GetAllActiveProvincesWithAll();

            result.ReturnData = provinces;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active provinces list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the provinces object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveProvincesList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "ProvinceID", "Name", "Description" };

            IList<dynamic> provinces = context.QuerySql<dynamic>(
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                ProvinceID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]
                FROM [Province] WHERE IsActive = 1 AND (Name LIKE @Keywords OR Description LIKE @Keywords)) TBL WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortDirection)
                , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = provinces;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active provinces count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the provinces count</returns>
        public static FacadeResult<long> GetActiveProvincesCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(ProvinceID) 
                FROM [Province] 
                WHERE (Name LIKE @Keywords OR Description LIKE @Keywords) AND IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active provinces count
        /// </summary>
        /// <returns>Facade result containing the provinces count</returns>
        public static FacadeResult<long> GetActiveProvincesCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveProvincesCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        #endregion

        #region City
        /// <summary>
        /// Get active city by city id
        /// </summary>
        /// <param name="cityID">The city id</param>
        /// <returns>Facade result containing the city object</returns>
        public static FacadeResult<City> GetActiveCityByCityID(int cityID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<City> result = new FacadeResult<City>();

            City city = context.GetActiveCityByID(cityID);

            result.ReturnData = city;
            result.IsSuccess = city == null ? false : true;
            result.ErrorMessage = city == null ? String.Format(TextResources.NotFound_ErrorMessage, "City") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active cities
        /// </summary>
        /// <returns>Facade result containing the cities object</returns>
        public static FacadeResult<IList<City>> GetAllActiveCitiesWithAll()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<City>> result = new FacadeResult<IList<City>>();

            IList<City> cities = context.GetAllActiveCitiesWithAll();

            result.ReturnData = cities;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active cities
        /// </summary>
        /// <returns>Facade result containing the cities object</returns>
        public static FacadeResult<IList<City>> GetAllActiveCitiesByProvinceIDs(IEnumerable<int> provinceIDs)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<City>> result = new FacadeResult<IList<City>>();

            IList<City> cities = context.GetAllActiveCitiesByProvinceIDs(provinceIDs);

            result.ReturnData = cities != null ? cities : new List<City>();
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active cities
        /// </summary>
        /// <returns>Facade result containing the citys object</returns>
        public static FacadeResult<IList<City>> GetAllActiveCities()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<City>> result = new FacadeResult<IList<City>>();

            IList<City> cities = context.GetAllActiveCities();

            result.ReturnData = cities;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active cities list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the citys object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveCitiesList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();

            String[] sortColumnNames = new String[] { "CityID", "C.Name", "C.Description", "P.Name" };

            IList<dynamic> cities = context.QuerySql<dynamic>(
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                CityID, C.Name AS CityName, C.Description, P.Name AS ProvinceName, C.[IsActive], C.[DateTimeCreated], C.[DateTimeUpdated], C.[CreatedBy], C.[LastUpdatedBy]
                FROM City C JOIN Province P ON C.ProvinceID = P.ProvinceID
                WHERE C.IsActive = 1 AND 
                (C.Name LIKE @Keywords OR C.Description LIKE @Keywords OR P.Name LIKE @Keywords)) TBL WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortDirection)
                , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = cities;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get city by city name
        /// </summary>
        /// <param name="cityName"></param>
        /// <returns>Facade result containing the city object</returns>
        public static FacadeResult<City> GetActiveCityByName(String cityName)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<City> result = new FacadeResult<City>();

            String[] sortColumnNames = new String[] { "CityID", "C.Name", "C.Description", "P.Name" };

            City city = context.SingleSql<City>(
                @"SELECT CityID, C.ProvinceID, C.Name, C.Description, C.[IsActive], C.[DateTimeCreated], C.[DateTimeUpdated], C.[CreatedBy], C.[LastUpdatedBy]
                FROM City C
                WHERE C.IsActive = 1 AND C.Name = @CityName"
                , new { CityName = cityName });

            result.ReturnData = city;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active cities count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the cities count</returns>
        public static FacadeResult<long> GetActiveCitiesCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(CityID) 
                FROM [City] C JOIN Province P ON C.ProvinceID = P.ProvinceID
                WHERE (C.Name LIKE @Keywords OR C.Description LIKE @Keywords OR P.Name LIKE @Keywords) AND C.IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active cities count
        /// </summary>
        /// <returns>Facade result containing the cities count</returns>
        public static FacadeResult<long> GetActiveCitiesCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveCitiesCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        #endregion

        #region MessageStatus

        /// <summary>
        /// Get all active messagestatus list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the messagestatus object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveMessageStatusList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();

            String[] sortColumnNames = new String[] { "MessageStatusID", "Name", "Description" };

            IList<dynamic> MessageStatus = context.QuerySql<dynamic>(
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                MessageStatusID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated],[DateTimeInActive], [CreatedBy], [LastUpdatedBy]
                FROM [MessageStatus] WHERE IsActive = 1 AND (Name LIKE @Keywords OR Description LIKE @Keywords)) TBL WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortDirection)
                , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });



            result.ReturnData = MessageStatus;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active messagestatus count
        /// </summary>
        /// <returns>Facade result containing the messagestatus count</returns>
        public static FacadeResult<long> GetActiveMessageStatusCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveMessageStatusCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active messagestatus count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the messagestatus count</returns>
        public static FacadeResult<long> GetActiveMessageStatusCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(MessageStatusID) 
                FROM [MessageStatus] 
                WHERE (Name LIKE @Keywords OR Description LIKE @Keywords) AND IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get active messagestatus by messagestatus id
        /// </summary>
        /// <param name="messageStatusID">The message status id</param>
        /// <returns>Facade result containing the video object</returns>
        public static FacadeResult<MessageStatus> GetActiveMessageStatusByMessageStatusID(int MessageStatusID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<MessageStatus> result = new FacadeResult<MessageStatus>();

            MessageStatus messageStatus = context.GetActiveMessageStatusByID(MessageStatusID);

            result.ReturnData = messageStatus;
            result.IsSuccess = messageStatus == null ? false : true;
            result.ErrorMessage = messageStatus == null ? String.Format(TextResources.NotFound_ErrorMessage, "MessageStatus") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active messageStatus
        /// </summary>
        /// <returns>Facade result containing the messagestatus object</returns>
        public static FacadeResult<IList<MessageStatus>> GetAllActiveMessageStatus()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<MessageStatus>> result = new FacadeResult<IList<MessageStatus>>();

            IList<MessageStatus> messageStatusList = context.GetAllActiveMessageStatus();

            result.ReturnData = messageStatusList;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        #endregion

        #region Video

        /// <summary>
        /// Get all active video list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the messagestatus object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveVideoList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();

            String[] sortColumnNames = new String[] { "VideoID", "Title", "Description" };

            IList<dynamic> Video = context.QuerySql<dynamic>(
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                VideoID, Title,ThumbnailPath, Description, VideoURL, ShowInHome, [IsActive], [DateTimeCreated], [DateTimeUpdated],[DateTimeInActive], [CreatedBy], [LastUpdatedBy]
                FROM [Video] WHERE IsActive = 1 AND (Title LIKE @Keywords OR Description LIKE @Keywords)) TBL WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortDirection)
                , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });



            result.ReturnData = Video;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        ///Get VideoViewModelList
        public static FacadeResult<List<VideoViewModel>> GetVideoViewModelActiveList()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<List<VideoViewModel>> result = new FacadeResult<List<VideoViewModel>>();

            List<VideoViewModel> VideoViewModelList = context.GetVideoActiveList();

            result.ReturnData = VideoViewModelList;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active video count
        /// </summary>
        /// <returns>Facade result containing the video count</returns>
        public static FacadeResult<long> GetActiveVideoCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveVideoCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active video count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the video count</returns>
        public static FacadeResult<long> GetActiveVideoCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(VideoID) 
                FROM [Video] 
                WHERE (Title LIKE @Keywords OR Description LIKE @Keywords) AND IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active video count by showInhome
        /// </summary>
        /// <param name=""></param>
        /// <returns>Facade result containing the video count</returns>
        public static FacadeResult<long> GetActiveVideoCountByShowInHome()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>("select COUNT_BIG(VideoID) from Video where ShowInHome = 1 and IsActive = 1");

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        /// <summary>
        /// Get active video by video id
        /// </summary>
        /// <param name="videoID">The video id</param>
        /// <returns>Facade result containing the video object</returns>
        public static FacadeResult<Video> GetActiveVideoByVideoID(int videoID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<Video> result = new FacadeResult<Video>();

            Video video = context.GetActiveVideoByID(videoID);

            result.ReturnData = video;
            result.IsSuccess = video == null ? false : true;
            result.ErrorMessage = video == null ? String.Format(TextResources.NotFound_ErrorMessage, "Video") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active Video
        /// for CR2 20-09-2017 by khalid
        /// </summary>
        /// <returns>Facade result containing the video object</returns>
        public static FacadeResult<IList<Video>> GetAllActiveVideos()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<Video>> result = new FacadeResult<IList<Video>>();

            IList<Video> videos = context.GetAllActiveVideo();

            result.ReturnData = videos;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get active showInhome video 
        /// </summary>
        /// <param name=""></param>
        /// <returns>Facade result containing the showInhome video</returns>
        public static FacadeResult<Video> GetActiveShowInHomeVideo()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<Video> result = new FacadeResult<Video>();

            Video video = context.GetActiveShowInHomeVideo();

            result.ReturnData = video;
            result.IsSuccess = video == null ? false : true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        #endregion

        #region Category
        /// <summary>
        /// Get active category by category id
        /// </summary>
        /// <param name="categoryID">The category id</param>
        /// <returns>Facade result containing the category object</returns>
        public static FacadeResult<Category> GetActiveCategoryByCategoryID(int categoryID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<Category> result = new FacadeResult<Category>();

            Category category = context.GetActiveCategoryByID(categoryID);

            result.ReturnData = category;
            result.IsSuccess = category == null ? false : true;
            result.ErrorMessage = category == null ? String.Format(TextResources.NotFound_ErrorMessage, "Category") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active categories
        /// </summary>
        /// <returns>Facade result containing the categories object</returns>
        public static FacadeResult<IList<Category>> GetAllActiveCategories()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<Category>> result = new FacadeResult<IList<Category>>();

            IList<Category> categories = context.GetAllActiveCategories();

            result.ReturnData = categories;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active categories
        /// </summary>
        /// <returns>Facade result containing the categories object</returns>
        public static FacadeResult<IList<Category>> GetAllActiveCategoriesWithAll()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<Category>> result = new FacadeResult<IList<Category>>();

            IList<Category> categories = context.GetAllActiveCategoriesWithAll();

            result.ReturnData = categories;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active categories list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the categories object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveCategoriesList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();

            String[] sortColumnNames = new String[] { "CategoryID", "Name", "Description" };

            IList<dynamic> categories = context.QuerySql<dynamic>(
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                CategoryID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]
                FROM [Category] WHERE IsActive = 1 AND (Name LIKE @Keywords OR Description LIKE @Keywords)) TBL WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortDirection)
                , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });



            result.ReturnData = categories;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }


        /// <summary>
        /// Get all active categories count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the categories count</returns>
        public static FacadeResult<long> GetActiveCategoriesCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(CategoryID) 
                FROM [Category] 
                WHERE (Name LIKE @Keywords OR Description LIKE @Keywords) AND IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active categories count
        /// </summary>
        /// <returns>Facade result containing the categories count</returns>
        public static FacadeResult<long> GetActiveCategoriesCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveCategoriesCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        #endregion

        #region Segment
        /// <summary>
        /// Get active segment by segment id
        /// </summary>
        /// <param name="segmentID">The segment id</param>
        /// <returns>Facade result containing the segment object</returns>
        public static FacadeResult<Segment> GetActiveSegmentBySegmentID(String segmentID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<Segment> result = new FacadeResult<Segment>();

            Segment segment = context.GetActiveSegmentByID(segmentID);

            result.ReturnData = segment;
            result.IsSuccess = segment == null ? false : true;
            result.ErrorMessage = segment == null ? String.Format(TextResources.NotFound_ErrorMessage, "Segment") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active segments
        /// </summary>
        /// <returns>Facade result containing the segments object</returns>
        public static FacadeResult<IList<Segment>> GetAllActiveSegments()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<Segment>> result = new FacadeResult<IList<Segment>>();

            IList<Segment> segments = context.GetAllActiveSegments();

            result.ReturnData = segments;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active segments list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the segments object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveSegmentsList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();

            String[] sortColumnNames = new String[] { "SegmentID", "Name", "Description" };

            IList<dynamic> segments = context.QuerySql<dynamic>(
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                SegmentID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]
                FROM [Segment] WHERE IsActive = 1 AND (Name LIKE @Keywords OR Description LIKE @Keywords)) TBL WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortDirection)
                , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });


            result.ReturnData = segments;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active segments count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the segments count</returns>
        public static FacadeResult<long> GetActiveSegmentsCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(SegmentID) 
                FROM [Segment] 
                WHERE (Name LIKE @Keywords OR Description LIKE @Keywords) AND IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active segments count
        /// </summary>
        /// <returns>Facade result containing the segments count</returns>
        public static FacadeResult<long> GetActiveSegmentsCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveSegmentsCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        #endregion

        #region Message Category Type
        /// <summary>
        /// Get active message category type  by id
        /// </summary>
        /// <param name="messageCategoryTypeID">The message category id</param>
        /// <returns>Facade result containing the message category type object</returns>
        public static FacadeResult<MessageCategoryType> GetActiveMessageCategoryTypeByMessageCategoryTypeID(byte messageCategoryTypeID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<MessageCategoryType> result = new FacadeResult<MessageCategoryType>();

            MessageCategoryType messageCategoryType = context.GetActiveMessageCategoryTypeByID(messageCategoryTypeID);

            result.ReturnData = messageCategoryType;
            result.IsSuccess = messageCategoryType == null ? false : true;
            result.ErrorMessage = messageCategoryType == null ? String.Format(TextResources.NotFound_ErrorMessage, "Message Category Type") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active message category type
        /// </summary>
        /// <returns>Facade result containing the message category type object</returns>
        public static FacadeResult<IList<MessageCategoryType>> GetAllActiveMessageCategoryTypes()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<MessageCategoryType>> result = new FacadeResult<IList<MessageCategoryType>>();

            IList<MessageCategoryType> messageCategoryTypes = context.GetAllActiveMessageCategoryTypes();

            result.ReturnData = messageCategoryTypes;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active message category type list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the message category type object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveMessageCategoryTypesList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "MessageCategoryTypeID", "Name", "Description" };

            IList<dynamic> messageCategoryType = context.QuerySql<dynamic>(
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                MessageCategoryTypeID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]
                FROM [MessageCategoryType] WHERE IsActive = 1 AND (Name LIKE @Keywords OR Description LIKE @Keywords)) TBL WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortDirection)
                , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });


            result.ReturnData = messageCategoryType;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active message category types count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the message category types count</returns>
        public static FacadeResult<long> GetActiveMessageCategoryTypesCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(MessageCategoryTypeID) 
                FROM [MessageCategoryType] 
                WHERE (Name LIKE @Keywords OR Description LIKE @Keywords) AND IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active message category types count
        /// </summary>
        /// <returns>Facade result containing the message category types count</returns>
        public static FacadeResult<long> GetActiveMessageCategoryTypesCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveMessageCategoryTypesCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        #endregion

        #region Ownership Document Type
        /// <summary>
        /// Get active ownership document type  by id
        /// </summary>
        /// <param name="ownershipDocumentTypeID">The ownership document id</param>
        /// <returns>Facade result containing the ownership type object</returns>
        public static FacadeResult<OwnershipDocumentType> GetActiveOwnershipDocumentTypeByOwnershipDocumentTypeID(int ownershipDocumentTypeID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<OwnershipDocumentType> result = new FacadeResult<OwnershipDocumentType>();

            OwnershipDocumentType ownershipDocumentType = context.GetActiveOwnershipDocumentTypeByID(ownershipDocumentTypeID);

            result.ReturnData = ownershipDocumentType;
            result.IsSuccess = ownershipDocumentType == null ? false : true;
            result.ErrorMessage = ownershipDocumentType == null ? String.Format(TextResources.NotFound_ErrorMessage, "Ownership Document Type") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active ownership document types
        /// </summary>
        /// <returns>Facade result containing the ownership document types object</returns>
        public static FacadeResult<IList<OwnershipDocumentType>> GetAllActiveOwnershipDocumentTypesWithAll()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<OwnershipDocumentType>> result = new FacadeResult<IList<OwnershipDocumentType>>();

            IList<OwnershipDocumentType> ownershipDocumentType = context.GetAllActiveOwnershipDocumentTypesWithAll();

            result.ReturnData = ownershipDocumentType;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active ownership document type
        /// </summary>
        /// <returns>Facade result containing the ownership document type object</returns>
        public static FacadeResult<IList<OwnershipDocumentType>> GetAllActiveOwnershipDocumentTypes()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<OwnershipDocumentType>> result = new FacadeResult<IList<OwnershipDocumentType>>();

            IList<OwnershipDocumentType> ownershipDocumentType = context.GetAllActiveOwnershipDocumentTypes();

            result.ReturnData = ownershipDocumentType;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active ownership document type list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the ownership document type object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveOwnershipDocumentTypesList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "OwnershipDocumentTypeID", "Name", "Description" };

            IList<dynamic> ownershipDocumentType = context.QuerySql<dynamic>(
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                OwnershipDocumentTypeID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]
                FROM [OwnershipDocumentType] WHERE IsActive = 1 AND (Name LIKE @Keywords OR Description LIKE @Keywords)) TBL WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortDirection)
                , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = ownershipDocumentType;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active ownership document types count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the active ownership document types count</returns>
        public static FacadeResult<long> GetActiveOwnershipDocumentTypesCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(OwnershipDocumentTypeID) 
                FROM [OwnershipDocumentType] 
                WHERE (Name LIKE @Keywords OR Description LIKE @Keywords) AND IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active ownership document types count
        /// </summary>
        /// <returns>Facade result containing the ownership document types count</returns>
        public static FacadeResult<long> GetActiveOwnershipDocumentTypesCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveOwnershipDocumentTypesCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        #endregion

        #region Web Content

        /// <summary>
        /// Get active web content by web content type id
        /// </summary>
        /// <param name="webContentTypeID">The web content type id</param>
        /// <returns>Facade result containing the web content object</returns>
        public static FacadeResult<WebContent> GetActiveWebContentByWebContentTypeID(int webContentTypeID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<WebContent> result = new FacadeResult<WebContent>();

            WebContent webContent = context.GetActiveWebContentByID(webContentTypeID);

            result.ReturnData = webContent;
            result.IsSuccess = webContent == null ? false : true;
            result.ErrorMessage = webContent == null ? String.Format(TextResources.NotFound_ErrorMessage, "Web Content") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get active web content type by web content type id
        /// </summary>
        /// <param name="webContentTypeID">The web content type id</param>
        /// <returns>Facade result containing the web content type object</returns>
        public static FacadeResult<WebContentType> GetActiveWebContentTypeByWebContentTypeID(int webContentTypeID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<WebContentType> result = new FacadeResult<WebContentType>();

            WebContentType webContent = context.GetActiveWebContentTypeByID(webContentTypeID);

            result.ReturnData = webContent;
            result.IsSuccess = webContent == null ? false : true;
            result.ErrorMessage = webContent == null ? String.Format(TextResources.NotFound_ErrorMessage, "Web Content Type") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active webContents
        /// </summary>
        /// <returns>Facade result containing the webContents object</returns>
        public static FacadeResult<IList<WebContent>> GetAllActiveWebContents()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<WebContent>> result = new FacadeResult<IList<WebContent>>();

            IList<WebContent> webContents = context.GetAllActiveWebContents();

            result.ReturnData = webContents;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active webContents list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the webContents object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveWebContentsList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "W.WebContentTypeID", "WT.Name" };

            IList<dynamic> webContents = context.QuerySql<dynamic>(
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                W.WebContentTypeID, WT.Name
                FROM [WebContent] W JOIN WebContentType WT ON W.WebContentTypeID = WT.WebContentTypeID WHERE W.IsActive = 1 AND (WT.Name LIKE @Keywords OR Content LIKE @Keywords)) TBL WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortDirection)
                , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = webContents;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active webContents count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the webContents count</returns>
        public static FacadeResult<long> GetActiveWebContentsCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(W.WebContentTypeID) 
                FROM [WebContent] W JOIN WebContentType WT ON W.WebContentTypeID = WT.WebContentTypeID
                WHERE (WT.Name LIKE @Keywords OR Content LIKE @Keywords) AND W.IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active webContents count
        /// </summary>
        /// <returns>Facade result containing the webContents count</returns>
        public static FacadeResult<long> GetActiveWebContentsCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveWebContentsCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        #endregion

        #region Branch
        /// <summary>
        /// Get active branch by id
        /// </summary>
        /// <param name="branchID">The branch id</param>
        /// <returns>Facade result containing the branch object</returns>
        public static FacadeResult<Branch> GetActiveBranchByBranchID(int branchID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<Branch> result = new FacadeResult<Branch>();

            Branch branch = context.GetActiveBranchByID(branchID);

            result.ReturnData = branch;
            result.IsSuccess = branch == null ? false : true;
            result.ErrorMessage = branch == null ? String.Format(TextResources.NotFound_ErrorMessage, "Branch") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active branch
        /// </summary>
        /// <returns>Facade result containing the branches object</returns>
        public static FacadeResult<IList<Branch>> GetAllActiveBranches()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<Branch>> result = new FacadeResult<IList<Branch>>();

            IList<Branch> branches = context.GetAllActiveBranches();

            result.ReturnData = branches;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active branch
        /// </summary>
        /// <returns>Facade result containing the branches object</returns>
        public static FacadeResult<IList<Branch>> GetAllActiveBranchesWithAll()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<Branch>> result = new FacadeResult<IList<Branch>>();

            IList<Branch> branches = context.GetAllActiveBranchesWithAll();

            result.ReturnData = branches;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active branches list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the branches object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveBranchesList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "BranchID", "B.Name", "B.Description", "Alias", "Address", "Phone1", "Phone2" };

            IList<dynamic> branch = context.QuerySql<dynamic>(
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                BranchID, B.Name AS BranchName, B.Description, B.Address, B.Alias, B.Phone1, B.Phone2, B.[IsActive], B.[DateTimeCreated], B.[DateTimeUpdated], B.[CreatedBy], B.[LastUpdatedBy]
                FROM Branch B
                WHERE B.IsActive = 1 AND (B.Name LIKE @Keywords OR B.Description LIKE @Keywords OR B.Address LIKE @Keywords OR B.Alias LIKE @Keywords OR B.Phone1 LIKE @Keywords OR B.Phone2 LIKE @Keywords)) TBL WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortDirection)
                , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = branch;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active branches count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the active branches count</returns>
        public static FacadeResult<long> GetActiveBranchesCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(BranchID) 
                FROM [Branch] B
                WHERE (B.Name LIKE @Keywords OR B.Description LIKE @Keywords OR B.Address LIKE @Keywords OR B.Alias LIKE @Keywords OR B.Phone1 LIKE @Keywords OR B.Phone2 LIKE @Keywords) AND B.IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active branches count
        /// </summary>
        /// <returns>Facade result containing the branches count</returns>
        public static FacadeResult<long> GetActiveBranchesCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveBranchesCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        #endregion

        #region KPKNL

        /// <summary>
        /// Get active kpknl by kpknl id
        /// </summary>
        /// <param name="provinceID">The kpknl id</param>
        /// <returns>Facade result containing the kpknl object</returns>
        public static FacadeResult<KPKNL> GetActiveKPKNLByKPKNLID(int kpknlID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<KPKNL> result = new FacadeResult<KPKNL>();

            KPKNL kpknl = context.GetActiveKPKNLByID(kpknlID);

            result.ReturnData = kpknl;
            result.IsSuccess = kpknl == null ? false : true;
            result.ErrorMessage = kpknl == null ? String.Format(TextResources.NotFound_ErrorMessage, "KPKNL") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active kpknl
        /// </summary>
        /// <returns>Facade result containing the kpknls object</returns>
        public static FacadeResult<IList<KPKNL>> GetAllActiveKPKNLs()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<KPKNL>> result = new FacadeResult<IList<KPKNL>>();

            IList<KPKNL> KPKNLs = context.GetAllActiveKPKNLs();

            result.ReturnData = KPKNLs;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active kpknls list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the kpknls object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveKPKNLsList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "KPKNLID", "Name", "Description", "Address", "Phone1", "Phone2" };

            IList<dynamic> provinces = context.QuerySql<dynamic>(
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                KPKNLID, Name, Description, Address,Phone1, Phone2, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]
                FROM [KPKNL]
                WHERE IsActive = 1 AND 
                (Name LIKE @Keywords OR Description LIKE @Keywords OR Address LIKE @Keywords OR Phone1 LIKE @Keywords OR Phone2 LIKE @Keywords)) TBL WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortDirection)
                , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = provinces;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active kpknls count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the kpknls count</returns>
        public static FacadeResult<long> GetActiveKPKNLsCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(KPKNLID) 
                FROM [KPKNL] 
                WHERE (Name LIKE @Keywords OR Description LIKE @Keywords OR Address LIKE @Keywords OR Phone1 LIKE @Keywords OR Phone2 LIKE @Keywords) AND IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active kpknls count
        /// </summary>
        /// <returns>Facade result containing the kpknls count</returns>
        public static FacadeResult<long> GetActiveKPKNLsCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveKPKNLsCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        #endregion

        #region Auction Hall
        /// <summary>
        /// Get active auction hall by auction hall id
        /// </summary>
        /// <param name="auctionHallID">The aution hall id</param>
        /// <returns>Facade result containing the auction hall object</returns>
        public static FacadeResult<AuctionHall> GetActiveAuctionHallByAuctionHallID(int auctionHallID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<AuctionHall> result = new FacadeResult<AuctionHall>();

            AuctionHall auctionHall = context.GetActiveAuctionHallByID(auctionHallID);

            result.ReturnData = auctionHall;
            result.IsSuccess = auctionHall == null ? false : true;
            result.ErrorMessage = auctionHall == null ? String.Format(TextResources.NotFound_ErrorMessage, "Auction Hall") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active auction hall
        /// </summary>
        /// <returns>Facade result containing the  auction hall object</returns>
        public static FacadeResult<IList<AuctionHall>> GetAllActiveAuctionHalls()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<AuctionHall>> result = new FacadeResult<IList<AuctionHall>>();

            IList<AuctionHall> auctionHalls = context.GetAllActiveAuctionHalls();

            result.ReturnData = auctionHalls;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active auction hall list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the auction hall object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveAuctionHallsList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "AuctionHallID", "Name", "Description", "Address", "Phone1", "Phone2" };

            IList<dynamic> provinces = context.QuerySql<dynamic>(
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                AuctionHallID, Name, Description, Address,Phone1, Phone2, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]
                FROM [AuctionHall]
                WHERE IsActive = 1 AND 
                (Name LIKE @Keywords OR Description LIKE @Keywords OR Address LIKE @Keywords OR Phone1 LIKE @Keywords OR Phone2 LIKE @Keywords)) TBL WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortDirection)
                , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = provinces;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active auction halls count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the action halls count</returns>
        public static FacadeResult<long> GetActiveAuctionHallsCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(AuctionHallID) 
                FROM [AuctionHall] 
                WHERE (Name LIKE @Keywords OR Description LIKE @Keywords OR Address LIKE @Keywords OR Phone1 LIKE @Keywords OR Phone2 LIKE @Keywords) AND IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active aution halls count
        /// </summary>
        /// <returns>Facade result containing the active aution halls count</returns>
        public static FacadeResult<long> GetActiveAuctionHallsCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveAuctionHallsCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        #endregion

        #region Min Max Price
        /// <summary>
        /// Get active auction hall by MinMaxPrice id
        /// </summary>
        /// <param name="minMaxPriceID">The MinMaxPrice id</param>
        /// <returns>Facade result containing the MinMaxPrice object</returns>
        public static FacadeResult<MinMaxPrice> GetActiveMinMaxPriceByMinMaxPriceID(int minMaxPriceID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<MinMaxPrice> result = new FacadeResult<MinMaxPrice>();

            MinMaxPrice minMaxPrice = context.GetActiveMinMaxPriceByID(minMaxPriceID);

            result.ReturnData = minMaxPrice;
            result.IsSuccess = minMaxPrice == null ? false : true;
            result.ErrorMessage = minMaxPrice == null ? String.Format(TextResources.NotFound_ErrorMessage, "Min Max Price") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active MinMaxPrice
        /// </summary>
        /// <returns>Facade result containing the MinMaxPrice object</returns>
        public static FacadeResult<IList<MinMaxPrice>> GetAllActiveMinMaxPrices()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<MinMaxPrice>> result = new FacadeResult<IList<MinMaxPrice>>();

            IList<MinMaxPrice> minMaxPrices = context.GetAllActiveMinMaxPrices();

            result.ReturnData = minMaxPrices;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active minMaxPrices list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the minMaxPrices object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveMinMaxPricesList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "MinMaxPriceID", "Name", "Description", "Value" };

            IList<dynamic> minMaxPrices = context.QuerySql<dynamic>(
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                MinMaxPriceID, Name, Description, Value, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]
                FROM [MinMaxPrice] WHERE IsActive = 1 AND 
                (Name LIKE @Keywords OR Description LIKE @Keywords) ) TBL WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortDirection)
                , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = minMaxPrices;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active minMaxPrices count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the minMaxPrices count</returns>
        public static FacadeResult<long> GetActiveMinMaxPricesCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(MinMaxPriceID) 
                FROM [MinMaxPrice] 
                WHERE (Name LIKE @Keywords OR Description LIKE @Keywords) AND IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active MinMaxPrices count
        /// </summary>
        /// <returns>Facade result containing the active MinMaxPrices count</returns>
        public static FacadeResult<long> GetActiveMinMaxPricesCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveMinMaxPricesCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active min max prices
        /// </summary>
        /// <returns>Facade result containing the min max prices object</returns>
        public static FacadeResult<IList<MinMaxPrice>> GetAllActiveMinMaxPricesWithAll()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<MinMaxPrice>> result = new FacadeResult<IList<MinMaxPrice>>();

            IList<MinMaxPrice> minMaxPrices = context.GetAllActiveMinMaxPricesWithAll();

            result.ReturnData = minMaxPrices;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        #endregion

        #region Category Template
        /// <summary>
        /// Get active auction hall by CategoryTemplate id
        /// </summary>
        /// <param name="categoryID">The CategoryTemplate id</param>
        /// <param name="fieldName"></param>
        /// <returns>Facade result containing the CategoryTemplate object</returns>
        public static FacadeResult<CategoryTemplate> GetActiveCategoryTemplateByID(int categoryID, String fieldName)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<CategoryTemplate> result = new FacadeResult<CategoryTemplate>();

            CategoryTemplate categoryTemplate = context.GetActiveCategoryTemplateByID(categoryID, fieldName);

            result.ReturnData = categoryTemplate;
            result.IsSuccess = categoryTemplate == null ? false : true;
            result.ErrorMessage = categoryTemplate == null ? String.Format(TextResources.NotFound_ErrorMessage, "Category Template") : String.Empty;

            return result;
        }

        public static FacadeResult<IList<CategoryTemplate>> GetAllActiveCategoryTemplateByCategoryID(int categoryID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<CategoryTemplate>> result = new FacadeResult<IList<CategoryTemplate>>();

            IList<CategoryTemplate> listCategoryTemplates = context.GetAllCategoryTemplatesByCategoryID(categoryID);

            result.ReturnData = listCategoryTemplates;
            result.IsSuccess = listCategoryTemplates == null ? false : true;
            result.ErrorMessage = listCategoryTemplates == null ? String.Format(TextResources.NotFound_ErrorMessage, "Category Template") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active CategoryTemplate
        /// </summary>
        /// <returns>Facade result containing the CategoryTemplate object</returns>
        public static FacadeResult<IList<CategoryTemplate>> GetAllCategoryTemplates()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<CategoryTemplate>> result = new FacadeResult<IList<CategoryTemplate>>();

            IList<CategoryTemplate> categoryTemplates = context.GetAllCategoryTemplates();

            result.ReturnData = categoryTemplates;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active categoryTemplates list
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Facade result containing the categoryTemplates object</returns>
        public static FacadeResult<IList<dynamic>> GetActiveCategoryTemplatesList(String keywords, int pageIndex, int pageSize, int sortIndex, String sortDirection)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "C.Name", "FieldName", "IsMandatory" };

            IList<dynamic> minMaxPrices = context.QuerySql<dynamic>(
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                CT.CategoryID, C.Name, FieldName, IsMandatory, CT.[IsActive], CT.[DateTimeCreated], CT.[DateTimeUpdated], CT.[CreatedBy], CT.[LastUpdatedBy]
                FROM [CategoryTemplate] CT INNER JOIN Category C ON C.CategoryID = CT.CategoryID
                WHERE CT.IsActive = 1 AND 
                (CT.FieldName LIKE @Keywords OR C.Name LIKE @Keywords)) TBL WHERE NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortDirection)
                , new { PageIndex = pageIndex, PageSize = pageSize, Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = minMaxPrices;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active CategoryTemplates count by keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns>Facade result containing the CategoryTemplates count</returns>
        public static FacadeResult<long> GetActiveCategoryTemplatesCountByKeywords(String keywords)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(CT.CategoryID) 
                FROM [CategoryTemplate] CT INNER JOIN Category C ON C.CategoryID = CT.CategoryID
                WHERE (CT.FieldName LIKE @Keywords OR C.Name LIKE @Keywords) AND CT.IsActive = 1", new { Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()) });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active CategoryTemplates count
        /// </summary>
        /// <returns>Facade result containing the active CategoryTemplates count</returns>
        public static FacadeResult<long> GetActiveCategoryTemplatesCount()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.GetActiveCategoryTemplatesCount();

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        public static FacadeResult<AssetCategoryTemplateValue> GetAllActiveCategoryTemplateValueByCategoryIDAndAssetID(long assetID, string fieldName)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<AssetCategoryTemplateValue> result = new FacadeResult<AssetCategoryTemplateValue>();

            AssetCategoryTemplateValue categoryTemplateValue = context.GetAllActiveAssetCategoryTemplateValueByIDAndFieldName(assetID, fieldName);

            result.ReturnData = categoryTemplateValue;
            result.IsSuccess = categoryTemplateValue == null ? false : true;
            result.ErrorMessage = categoryTemplateValue == null ? String.Format(TextResources.NotFound_ErrorMessage, "Category Template") : String.Empty;

            return result;
        }

        #endregion

        #region AssetStatus
        /// <summary>
        /// Get all active asset status
        /// </summary>
        /// <returns>Facade result containing the asset status object</returns>
        public static FacadeResult<IList<AssetStatus>> GetAllActiveAssetStatuses()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<AssetStatus>> result = new FacadeResult<IList<AssetStatus>>();

            IList<AssetStatus> assetStatuses = context.GetAllAssetStatuses();

            result.ReturnData = assetStatuses;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get All Asset Status By ID
        /// </summary>
        /// <param name="assetStatusID"></param>
        /// <returns></returns>
        public static FacadeResult<IList<AssetStatus>> GetAllActiveAssetStatusesByStatusID(int assetStatusID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<AssetStatus>> result = new FacadeResult<IList<AssetStatus>>();

            IList<AssetStatus> assetStatuses = context.GetAllAssetStatusesByStatusID(assetStatusID);

            result.ReturnData = assetStatuses;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        #endregion

        #region ActionSchedule

        public static FacadeResult<AuctionSchedule> GetActiveAuctionScheduleByScheduleID(long auctionScheduleID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<AuctionSchedule> result = new FacadeResult<AuctionSchedule>();

            AuctionSchedule auctionSchedule = context.GetActiveAuctionScheduleByAuctionScheduleID(auctionScheduleID);

            result.ReturnData = auctionSchedule;
            result.IsSuccess = auctionSchedule == null ? false : true;
            result.ErrorMessage = auctionSchedule == null ? String.Format(TextResources.NotFound_ErrorMessage, "AuctionSchedule") : String.Empty;

            return result;
        }

        #endregion

        #region Type

        /// <summary>
        /// Get all static types
        /// </summary>
        /// <returns>Facade result containing the types object</returns>
        public static FacadeResult<IList<dynamic>> GetAllStaticTypes()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();

            IList<dynamic> types = new List<dynamic>();
            types.Add(new { TypeID = 1, Name = "Jual Sukarela" });
            types.Add(new { TypeID = 2, Name = "Lelang" });

            result.ReturnData = types;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        #endregion

        #endregion

        #region Action

        #region Province

        /// <summary>
        /// Add new province
        /// </summary>
        /// <param name="province">The province object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the added province</returns>
        public static FacadeResult<Province> AddNewProvince(Province province, int currentUserID)
        {

            FacadeResult<Province> result = new FacadeResult<Province>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    province.CreatedBy = currentUserID;
                    context.InsertProvince(province);

                    userActivity.UserActivityTypeID = "AddNewProvince";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = province.ProvinceID.ToString();
                    context.InsertUserActivity(userActivity);

                    result.ReturnData = province;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update province
        /// </summary>
        /// <param name="province">The province object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated province</returns>
        public static FacadeResult<Province> UpdateProvince(Province province, int currentUserID)
        {
            FacadeResult<Province> result = new FacadeResult<Province>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Province updatedProvince = context.GetActiveProvinceByID(province.ProvinceID.Value);
                    if (updatedProvince != null)
                    {
                        updatedProvince.Name = province.Name;
                        updatedProvince.Description = province.Description;
                        updatedProvince.LastUpdatedBy = currentUserID;
                        updatedProvince.DateTimeUpdated = DateTime.Now;
                        context.UpdateProvince(updatedProvince);

                        userActivity.UserActivityTypeID = "UpdateProvince";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = province.ProvinceID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updatedProvince;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Province");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Delete province by id (set as inactive)
        /// </summary>
        /// <param name="provinceID"></param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the deleted province</returns>
        public static FacadeResult<Province> DeleteProvinceByID(int provinceID, int currentUserID)
        {
            FacadeResult<Province> result = new FacadeResult<Province>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Province province = context.GetProvinceByID(provinceID);
                    if (province != null)
                    {
                        province.LastUpdatedBy = currentUserID;
                        province.DateTimeUpdated = DateTime.Now;
                        province.IsActive = false;
                        context.UpdateProvince(province);

                        userActivity.UserActivityTypeID = "DeleteProvince";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = provinceID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = province;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Province");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        #endregion

        #region City

        /// <summary>
        /// Add new city
        /// </summary>
        /// <param name="city">The city object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the added city</returns>
        public static FacadeResult<City> AddNewCity(City city, int currentUserID)
        {

            FacadeResult<City> result = new FacadeResult<City>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    city.CreatedBy = currentUserID;
                    context.InsertCity(city);

                    userActivity.UserActivityTypeID = "AddNewCity";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = city.CityID.ToString();
                    context.InsertUserActivity(userActivity);

                    result.ReturnData = city;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update city
        /// </summary>
        /// <param name="city">The city object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated city</returns>
        public static FacadeResult<City> UpdateCity(City city, int currentUserID)
        {
            FacadeResult<City> result = new FacadeResult<City>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    City updatedCity = context.GetActiveCityByID(city.CityID.Value);
                    if (updatedCity != null)
                    {
                        updatedCity.Name = city.Name;
                        updatedCity.Description = city.Description;
                        updatedCity.ProvinceID = city.ProvinceID;
                        updatedCity.LastUpdatedBy = currentUserID;
                        updatedCity.DateTimeUpdated = DateTime.Now;
                        context.UpdateCity(updatedCity);

                        userActivity.UserActivityTypeID = "UpdateCity";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = city.CityID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updatedCity;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "City");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Delete city by id (set as inactive)
        /// </summary>
        /// <param name="cityID"></param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the deleted city</returns>
        public static FacadeResult<City> DeleteCityByID(int cityID, int currentUserID)
        {
            FacadeResult<City> result = new FacadeResult<City>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    City city = context.GetCityByID(cityID);
                    if (city != null)
                    {
                        city.LastUpdatedBy = currentUserID;
                        city.DateTimeUpdated = DateTime.Now;
                        city.IsActive = false;
                        context.UpdateCity(city);

                        userActivity.UserActivityTypeID = "DeleteCity";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = cityID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = city;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "City");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        #endregion

        #region MessageStatus
        /// <summary>
        /// Add new messagestatus
        /// </summary>
        /// <param name="messagestatus">The messagestatus object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the added messagestatus</returns>
        public static FacadeResult<MessageStatus> AddNewMessageStatus(MessageStatus messageStatus, int currentUserID)
        {

            FacadeResult<MessageStatus> result = new FacadeResult<MessageStatus>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    messageStatus.CreatedBy = currentUserID;
                    context.InsertMessageStatus(messageStatus);

                    userActivity.UserActivityTypeID = "AddNewMessageStatus";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = messageStatus.MessageStatusID.ToString();
                    context.InsertUserActivity(userActivity);

                    result.ReturnData = messageStatus;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }


        /// <summary>
        /// Update messagestatus
        /// </summary>
        /// <param name="messagestatus">The messagestatus object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated messagestatus</returns>
        public static FacadeResult<MessageStatus> UpdateMessageStatus(MessageStatus messageStatus, int currentUserID)
        {
            FacadeResult<MessageStatus> result = new FacadeResult<MessageStatus>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    MessageStatus updatedMessageStatus = context.GetActiveMessageStatusByID(messageStatus.MessageStatusID);
                    if (updatedMessageStatus != null)
                    {
                        updatedMessageStatus.Name = messageStatus.Name;
                        updatedMessageStatus.Description = messageStatus.Description;
                        updatedMessageStatus.IsComplete = messageStatus.IsComplete;
                        updatedMessageStatus.LastUpdatedBy = currentUserID;
                        updatedMessageStatus.DateTimeUpdated = DateTime.Now;
                        context.UpdateMessageStatus(updatedMessageStatus);

                        userActivity.UserActivityTypeID = "UpdateMessageStatus";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = messageStatus.MessageStatusID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updatedMessageStatus;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "MessageStatus");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }


        /// <summary>
        /// Delete messagestatus by id (set as inactive)
        /// </summary>
        /// <param name="messageStatusID">int messagestatus id</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the deleted messagestatus</returns>
        public static FacadeResult<MessageStatus> DeleteMessageStatusByID(int messageStatusID, int currentUserID)
        {
            FacadeResult<MessageStatus> result = new FacadeResult<MessageStatus>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    MessageStatus messageStatus = context.GetMessageStatusByID(messageStatusID);
                    if (messageStatus != null)
                    {
                        messageStatus.LastUpdatedBy = currentUserID;
                        messageStatus.DateTimeUpdated = DateTime.Now;
                        messageStatus.IsActive = false;
                        context.UpdateMessageStatus(messageStatus);

                        userActivity.UserActivityTypeID = "DeleteMessageStatus";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = messageStatusID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = messageStatus;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "MessageStatus");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }
        #endregion

        #region Video
        /// <summary>
        /// Add new video
        /// </summary>
        /// <param name="video">The video object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the added video</returns>
        public static FacadeResult<Video> AddNewVideo(Video video, int currentUserID)
        {
            FacadeResult<Video> result = new FacadeResult<Video>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    video.CreatedBy = currentUserID;
                    context.InsertVideo(video);

                    userActivity.UserActivityTypeID = "AddNewVideo";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = video.VideoID.ToString();
                    context.InsertUserActivity(userActivity);

                    result.ReturnData = video;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update video
        /// </summary>
        /// <param name="video">The video object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated video</returns>
        public static FacadeResult<Video> UpdateVideo(Video video, int currentUserID)
        {
            FacadeResult<Video> result = new FacadeResult<Video>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Video updatedVideo = context.GetActiveVideoByID(video.VideoID);
                    if (updatedVideo != null)
                    {
                        updatedVideo.Title = video.Title;
                        updatedVideo.Description = video.Description;
                        updatedVideo.ThumbnailPath = video.ThumbnailPath;
                        updatedVideo.VideoURL = video.VideoURL;
                        updatedVideo.ShowInHome = video.ShowInHome;
                        updatedVideo.LastUpdatedBy = currentUserID;
                        updatedVideo.DateTimeUpdated = DateTime.Now;
                        context.UpdateVideo(updatedVideo);

                        userActivity.UserActivityTypeID = "UpdateVideo";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = video.VideoID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updatedVideo;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Video");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Delete video by id (set as inactive)
        /// </summary>
        /// <param name="videoID">int messagestatus id</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the deleted messagestatus</returns>
        public static FacadeResult<Video> DeleteVideoByID(int videoID, int currentUserID)
        {
            FacadeResult<Video> result = new FacadeResult<Video>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Video video = context.GetVideoByID(videoID);
                    if (video != null)
                    {
                        video.LastUpdatedBy = currentUserID;
                        video.DateTimeUpdated = DateTime.Now;
                        video.IsActive = false;
                        context.UpdateVideo(video);

                        userActivity.UserActivityTypeID = "DeleteVideo";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = videoID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = video;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Video");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }
        #endregion

        #region Category
        /// <summary>
        /// Add new category
        /// </summary>
        /// <param name="category">The category object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the added category</returns>
        public static FacadeResult<Category> AddNewCategory(Category category, int currentUserID)
        {

            FacadeResult<Category> result = new FacadeResult<Category>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    category.CurrentAssetTotal = 0;
                    category.CreatedBy = currentUserID;
                    context.InsertCategory(category);

                    userActivity.UserActivityTypeID = "AddNewCategory";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = category.CategoryID.ToString();
                    context.InsertUserActivity(userActivity);

                    result.ReturnData = category;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update category
        /// </summary>
        /// <param name="category">The category object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated category</returns>
        public static FacadeResult<Category> UpdateCategory(Category category, int currentUserID)
        {
            FacadeResult<Category> result = new FacadeResult<Category>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Category updatedCategory = context.GetActiveCategoryByID(category.CategoryID.Value);
                    if (updatedCategory != null)
                    {
                        updatedCategory.Name = category.Name;
                        updatedCategory.Description = category.Description;
                        updatedCategory.LastUpdatedBy = currentUserID;
                        updatedCategory.DateTimeUpdated = DateTime.Now;
                        context.UpdateCategory(updatedCategory);

                        userActivity.UserActivityTypeID = "UpdateCategory";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = category.CategoryID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updatedCategory;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Category");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Delete category by id (set as inactive)
        /// </summary>
        /// <param name="categoryID">int category id</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the deleted category</returns>
        public static FacadeResult<Category> DeleteCategoryByID(int categoryID, int currentUserID)
        {
            FacadeResult<Category> result = new FacadeResult<Category>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Category category = context.GetCategoryByID(categoryID);
                    if (category != null)
                    {
                        category.LastUpdatedBy = currentUserID;
                        category.DateTimeUpdated = DateTime.Now;
                        category.IsActive = false;
                        context.UpdateCategory(category);

                        userActivity.UserActivityTypeID = "DeleteCategory";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = categoryID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = category;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Category");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }
        #endregion

        #region Segment
        /// <summary>
        /// Add new segment
        /// </summary>
        /// <param name="segment">The segment object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the added segment</returns>
        public static FacadeResult<Segment> AddNewSegment(Segment segment, int currentUserID)
        {

            FacadeResult<Segment> result = new FacadeResult<Segment>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    IEnumerable<Branch> branches = context.GetAllActiveBranches().AsEnumerable();
                    AutoNumbering autoNumbering1;

                    segment.CreatedBy = currentUserID;
                    context.InsertSegment(segment);

                    //Generate the auto numbering
                    foreach (Branch branch in branches)
                    {
                        autoNumbering1 = new AutoNumbering();
                        autoNumbering1.AutoNumberingTypeID = 1;
                        autoNumbering1.SegmentID = segment.SegmentID;
                        autoNumbering1.RunningNumber = 0;
                        autoNumbering1.BranchID = branch.BranchID;
                        autoNumbering1.CreatedBy = currentUserID;
                        context.InsertAutoNumbering(autoNumbering1);
                    }

                    userActivity.UserActivityTypeID = "AddNewSegment";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = segment.SegmentID.ToString();
                    context.InsertUserActivity(userActivity);

                    result.ReturnData = segment;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update segment
        /// </summary>
        /// <param name="segment">The segment object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated segment</returns>
        public static FacadeResult<Segment> UpdateSegment(Segment segment, int currentUserID)
        {
            FacadeResult<Segment> result = new FacadeResult<Segment>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Segment updatedSegment = context.GetActiveSegmentByID(segment.SegmentID);
                    if (updatedSegment != null)
                    {
                        updatedSegment.Name = segment.Name;
                        updatedSegment.Description = segment.Description;
                        updatedSegment.LastUpdatedBy = currentUserID;
                        updatedSegment.DateTimeUpdated = DateTime.Now;
                        context.UpdateSegment(updatedSegment);

                        userActivity.UserActivityTypeID = "UpdateSegment";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = segment.SegmentID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updatedSegment;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Segment");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Delete segment by id (set as inactive)
        /// </summary>
        /// <param name="segmentID">int segment id</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the deleted segment</returns>
        public static FacadeResult<Segment> DeleteSegmentByID(String segmentID, int currentUserID)
        {
            FacadeResult<Segment> result = new FacadeResult<Segment>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Segment segment = context.GetSegmentByID(segmentID);
                    if (segment != null)
                    {
                        segment.LastUpdatedBy = currentUserID;
                        segment.DateTimeUpdated = DateTime.Now;
                        segment.IsActive = false;
                        context.UpdateSegment(segment);

                        userActivity.UserActivityTypeID = "DeleteSegment";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = segmentID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = segment;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Segment");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }
        #endregion

        #region Message Category Type
        /// <summary>
        /// Add new message category type
        /// </summary>
        /// <param name="segment">The message category type object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the added message category type</returns>
        public static FacadeResult<MessageCategoryType> AddNewMessageCategoryType(MessageCategoryType messageCategoryType, int currentUserID)
        {

            FacadeResult<MessageCategoryType> result = new FacadeResult<MessageCategoryType>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    messageCategoryType.CreatedBy = currentUserID;
                    context.InsertMessageCategoryType(messageCategoryType);

                    userActivity.UserActivityTypeID = "AddNewMessageCategoryType";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = messageCategoryType.MessageCategoryTypeID.ToString();
                    context.InsertUserActivity(userActivity);

                    result.ReturnData = messageCategoryType;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update message category type
        /// </summary>
        /// <param name="messageCategoryType">The message category type object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated message category type</returns>
        public static FacadeResult<MessageCategoryType> UpdateMessageCategoryType(MessageCategoryType messageCategoryType, int currentUserID)
        {
            FacadeResult<MessageCategoryType> result = new FacadeResult<MessageCategoryType>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    MessageCategoryType updateMessageCategoryType = context.GetActiveMessageCategoryTypeByID(messageCategoryType.MessageCategoryTypeID.Value);
                    if (updateMessageCategoryType != null)
                    {
                        updateMessageCategoryType.Name = messageCategoryType.Name;
                        updateMessageCategoryType.Description = messageCategoryType.Description;
                        updateMessageCategoryType.LastUpdatedBy = currentUserID;
                        updateMessageCategoryType.DateTimeUpdated = DateTime.Now;
                        context.UpdateMessageCategoryType(updateMessageCategoryType);

                        userActivity.UserActivityTypeID = "UpdateMessageCategoryType";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = updateMessageCategoryType.MessageCategoryTypeID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updateMessageCategoryType;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Message Category Type");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Delete message category type by id (set as inactive)
        /// </summary>
        /// <param name="messageCategoryTypeID">byte message category id</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the deleted message category type</returns>
        public static FacadeResult<MessageCategoryType> DeleteMessageCategoryTypeByID(byte messageCategoryTypeID, int currentUserID)
        {
            FacadeResult<MessageCategoryType> result = new FacadeResult<MessageCategoryType>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    MessageCategoryType messageCategoryType = context.GetMessageCategoryTypeByID(messageCategoryTypeID);
                    if (messageCategoryType != null)
                    {
                        messageCategoryType.LastUpdatedBy = currentUserID;
                        messageCategoryType.DateTimeUpdated = DateTime.Now;
                        messageCategoryType.IsActive = false;
                        context.UpdateMessageCategoryType(messageCategoryType);

                        userActivity.UserActivityTypeID = "DeleteMessageCategoryType";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = messageCategoryTypeID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = messageCategoryType;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Message Category Type");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }
        #endregion

        #region Ownership Document Type
        /// <summary>
        /// Add new ownership document Type
        /// </summary>
        /// <param name="ownershipDocumentType">The ownership document Type object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the added ownership document Type</returns>
        public static FacadeResult<OwnershipDocumentType> AddNewOwnershipDocumentType(OwnershipDocumentType ownershipDocumentType, int currentUserID)
        {

            FacadeResult<OwnershipDocumentType> result = new FacadeResult<OwnershipDocumentType>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    ownershipDocumentType.CreatedBy = currentUserID;
                    context.InsertOwnershipDocumentType(ownershipDocumentType);

                    userActivity.UserActivityTypeID = "AddNewOwnershipDocumentType";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = ownershipDocumentType.OwnershipDocumentTypeID.ToString();
                    context.InsertUserActivity(userActivity);

                    result.ReturnData = ownershipDocumentType;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update ownership document type
        /// </summary>
        /// <param name="ownershipDocumentType">The ownership document type object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated ownership document type</returns>
        public static FacadeResult<OwnershipDocumentType> UpdateOwnershipDocumentType(OwnershipDocumentType ownershipDocumentType, int currentUserID)
        {
            FacadeResult<OwnershipDocumentType> result = new FacadeResult<OwnershipDocumentType>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    OwnershipDocumentType updateOwnershipDocumentType = context.GetActiveOwnershipDocumentTypeByID(ownershipDocumentType.OwnershipDocumentTypeID.Value);
                    if (updateOwnershipDocumentType != null)
                    {
                        updateOwnershipDocumentType.Name = ownershipDocumentType.Name;
                        updateOwnershipDocumentType.Description = ownershipDocumentType.Description;
                        updateOwnershipDocumentType.LastUpdatedBy = currentUserID;
                        updateOwnershipDocumentType.DateTimeUpdated = DateTime.Now;
                        context.UpdateOwnershipDocumentType(updateOwnershipDocumentType);

                        userActivity.UserActivityTypeID = "UpdateOwnershipDocumentType";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = updateOwnershipDocumentType.OwnershipDocumentTypeID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updateOwnershipDocumentType;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Ownership Document Type");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Delete ownership document type by id (set as inactive)
        /// </summary>
        /// <param name="ownershipDocumentTypeID">int ownership document type id</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the deleted ownership document type</returns>
        public static FacadeResult<OwnershipDocumentType> DeleteOwnershipDocumentTypeByID(int ownershipDocumentTypeID, int currentUserID)
        {
            FacadeResult<OwnershipDocumentType> result = new FacadeResult<OwnershipDocumentType>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    OwnershipDocumentType ownershipDocumentType = context.GetOwnershipDocumentTypeByID(ownershipDocumentTypeID);
                    if (ownershipDocumentType != null)
                    {
                        ownershipDocumentType.LastUpdatedBy = currentUserID;
                        ownershipDocumentType.DateTimeUpdated = DateTime.Now;
                        ownershipDocumentType.IsActive = false;
                        context.UpdateOwnershipDocumentType(ownershipDocumentType);

                        userActivity.UserActivityTypeID = "DeleteOwnershipDocumentType";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = ownershipDocumentTypeID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = ownershipDocumentType;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Ownership Document Type");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }
        #endregion

        #region Branch
        /// <summary>
        /// Add new branch
        /// </summary>
        /// <param name="branch">The branch object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the added branch </returns>
        public static FacadeResult<Branch> AddNewBranch(Branch branch, int currentUserID)
        {

            FacadeResult<Branch> result = new FacadeResult<Branch>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    IEnumerable<Segment> segments = context.GetAllActiveSegments().AsEnumerable();
                    AutoNumbering autoNumbering1;

                    branch.CreatedBy = currentUserID;
                    context.InsertBranch(branch);

                    //Generate the auto numbering
                    foreach (Segment segment in segments)
                    {
                        autoNumbering1 = new AutoNumbering();
                        autoNumbering1.AutoNumberingTypeID = 1;
                        autoNumbering1.SegmentID = segment.SegmentID;
                        autoNumbering1.RunningNumber = 0;
                        autoNumbering1.BranchID = branch.BranchID;
                        autoNumbering1.CreatedBy = currentUserID;
                        context.InsertAutoNumbering(autoNumbering1);
                    }

                    userActivity.UserActivityTypeID = "AddNewBranch";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = branch.BranchID.ToString();
                    context.InsertUserActivity(userActivity);

                    result.ReturnData = branch;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update branch
        /// </summary>
        /// <param name="branch">The branch object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated branch</returns>
        public static FacadeResult<Branch> UpdateBranch(Branch branch, int currentUserID)
        {
            FacadeResult<Branch> result = new FacadeResult<Branch>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Branch updateBranch = context.GetActiveBranchByID(branch.BranchID.Value);
                    if (updateBranch != null)
                    {
                        updateBranch.Name = branch.Name;
                        updateBranch.Description = branch.Description;
                        updateBranch.Alias = branch.Alias;
                        updateBranch.Address = branch.Address;
                        updateBranch.Phone1 = branch.Phone1;
                        updateBranch.Phone2 = branch.Phone2;
                        updateBranch.LastUpdatedBy = currentUserID;
                        updateBranch.DateTimeUpdated = DateTime.Now;
                        context.UpdateBranch(updateBranch);

                        userActivity.UserActivityTypeID = "UpdateBranch";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = updateBranch.BranchID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updateBranch;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Branch");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Delete branch by id (set as inactive)
        /// </summary>
        /// <param name="branchID">int branch id</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the deleted branch</returns>
        public static FacadeResult<Branch> DeleteBranchByID(int branchID, int currentUserID)
        {
            FacadeResult<Branch> result = new FacadeResult<Branch>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    Branch branch = context.GetBranchByID(branchID);
                    if (branch != null)
                    {
                        branch.LastUpdatedBy = currentUserID;
                        branch.DateTimeUpdated = DateTime.Now;
                        branch.IsActive = false;
                        context.UpdateBranch(branch);

                        userActivity.UserActivityTypeID = "DeleteBranch";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = branchID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = branch;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Branch");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        #endregion

        #region KPKNL
        /// <summary>
        /// Add new kpknl
        /// </summary>
        /// <param name="kpknl">The kpknl object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the added kpknl </returns>
        public static FacadeResult<KPKNL> AddNewKPKNL(KPKNL kpknl, int currentUserID)
        {

            FacadeResult<KPKNL> result = new FacadeResult<KPKNL>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    kpknl.CreatedBy = currentUserID;
                    context.InsertKPKNL(kpknl);

                    userActivity.UserActivityTypeID = "AddNewKPKNL";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = kpknl.KPKNLID.ToString();
                    context.InsertUserActivity(userActivity);

                    result.ReturnData = kpknl;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update kpknl
        /// </summary>
        /// <param name="kpknl">The kpknl object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated kpknl</returns>
        public static FacadeResult<KPKNL> UpdateKPKNL(KPKNL kpknl, int currentUserID)
        {
            FacadeResult<KPKNL> result = new FacadeResult<KPKNL>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    KPKNL updateKPKNL = context.GetActiveKPKNLByID(kpknl.KPKNLID.Value);
                    if (updateKPKNL != null)
                    {
                        updateKPKNL.Name = kpknl.Name;
                        updateKPKNL.Description = kpknl.Description;
                        updateKPKNL.Address = kpknl.Address;
                        updateKPKNL.Phone1 = kpknl.Phone1;
                        updateKPKNL.Phone2 = kpknl.Phone2;
                        updateKPKNL.LastUpdatedBy = currentUserID;
                        updateKPKNL.DateTimeUpdated = DateTime.Now;
                        context.UpdateKPKNL(updateKPKNL);

                        userActivity.UserActivityTypeID = "UpdateKPKNL";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = updateKPKNL.KPKNLID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updateKPKNL;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "KPKNL");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Delete kpknl by id (set as inactive)
        /// </summary>
        /// <param name="kpknlID">int kpknl id</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the deleted kpknl</returns>
        public static FacadeResult<KPKNL> DeleteKPKNLByID(int kpknlID, int currentUserID)
        {
            FacadeResult<KPKNL> result = new FacadeResult<KPKNL>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    KPKNL kpknl = context.GetKPKNLByID(kpknlID);
                    if (kpknl != null)
                    {
                        kpknl.LastUpdatedBy = currentUserID;
                        kpknl.DateTimeUpdated = DateTime.Now;
                        kpknl.IsActive = false;
                        context.UpdateKPKNL(kpknl);

                        userActivity.UserActivityTypeID = "DeleteKPKNL";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = kpknlID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = kpknl;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "KPKNL");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }
        #endregion

        #region Auction Hall
        /// <summary>
        /// Add new auction hall
        /// </summary>
        /// <param name="kpknl">The auction hall object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the added auction hall </returns>
        public static FacadeResult<AuctionHall> AddNewAuctionHall(AuctionHall auctionHall, int currentUserID)
        {

            FacadeResult<AuctionHall> result = new FacadeResult<AuctionHall>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    auctionHall.CreatedBy = currentUserID;
                    context.InsertAuctionHall(auctionHall);

                    userActivity.UserActivityTypeID = "AddNewAuctionHall";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = auctionHall.AuctionHallID.ToString();
                    context.InsertUserActivity(userActivity);

                    result.ReturnData = auctionHall;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update auction Hall
        /// </summary>
        /// <param name="auctionHall">The auctionHall object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated auctionHall</returns>
        public static FacadeResult<AuctionHall> UpdateAuctionHall(AuctionHall auctionHall, int currentUserID)
        {
            FacadeResult<AuctionHall> result = new FacadeResult<AuctionHall>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    AuctionHall updateAuctionHall = context.GetActiveAuctionHallByID(auctionHall.AuctionHallID.Value);
                    if (updateAuctionHall != null)
                    {
                        updateAuctionHall.Name = auctionHall.Name;
                        updateAuctionHall.Description = auctionHall.Description;
                        updateAuctionHall.Address = auctionHall.Address;
                        updateAuctionHall.Phone1 = auctionHall.Phone1;
                        updateAuctionHall.Phone2 = auctionHall.Phone2;
                        updateAuctionHall.LastUpdatedBy = currentUserID;
                        updateAuctionHall.DateTimeUpdated = DateTime.Now;
                        context.UpdateAuctionHall(updateAuctionHall);

                        userActivity.UserActivityTypeID = "UpdateAuctionHall";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = updateAuctionHall.AuctionHallID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updateAuctionHall;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Auction Hall");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Delete auction hall by id (set as inactive)
        /// </summary>
        /// <param name="auctionHallID">int auction hall id</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the deleted kpknl</returns>
        public static FacadeResult<AuctionHall> DeleteAuctionHallByID(int auctionHallID, int currentUserID)
        {
            FacadeResult<AuctionHall> result = new FacadeResult<AuctionHall>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    AuctionHall auctionHall = context.GetAuctionHallByID(auctionHallID);
                    if (auctionHall != null)
                    {
                        auctionHall.LastUpdatedBy = currentUserID;
                        auctionHall.DateTimeUpdated = DateTime.Now;
                        auctionHall.IsActive = false;
                        context.UpdateAuctionHall(auctionHall);

                        userActivity.UserActivityTypeID = "DeleteAuctionHall";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = auctionHallID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = auctionHall;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Auction Hall");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }
        #endregion

        #region Min Max Price

        /// <summary>
        /// Add new min max price
        /// </summary>
        /// <param name="kpknl">The min max price object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the added min max price </returns>
        public static FacadeResult<MinMaxPrice> AddNewMinMaxPrice(MinMaxPrice minMaxPrice, int currentUserID)
        {

            FacadeResult<MinMaxPrice> result = new FacadeResult<MinMaxPrice>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    minMaxPrice.CreatedBy = currentUserID;
                    context.InsertMinMaxPrice(minMaxPrice);

                    userActivity.UserActivityTypeID = "AddNewMinMaxPrice";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = minMaxPrice.MinMaxPriceID.ToString();
                    context.InsertUserActivity(userActivity);

                    result.ReturnData = minMaxPrice;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update minMaxPrice
        /// </summary>
        /// <param name="minMaxPrice">The minMaxPrice object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated minMaxPrice</returns>
        public static FacadeResult<MinMaxPrice> UpdateMinMaxPrice(MinMaxPrice minMaxPrice, int currentUserID)
        {
            FacadeResult<MinMaxPrice> result = new FacadeResult<MinMaxPrice>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    MinMaxPrice updateMinMaxPrice = context.GetActiveMinMaxPriceByID(minMaxPrice.MinMaxPriceID.Value);
                    if (updateMinMaxPrice != null)
                    {
                        updateMinMaxPrice.Name = minMaxPrice.Name;
                        updateMinMaxPrice.Description = minMaxPrice.Description;
                        updateMinMaxPrice.Value = minMaxPrice.Value;
                        updateMinMaxPrice.LastUpdatedBy = currentUserID;
                        updateMinMaxPrice.DateTimeUpdated = DateTime.Now;
                        context.UpdateMinMaxPrice(updateMinMaxPrice);

                        userActivity.UserActivityTypeID = "UpdateMinMaxPrice";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = updateMinMaxPrice.MinMaxPriceID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updateMinMaxPrice;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Min Max Price");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Delete MinMaxPrice by id (set as inactive)
        /// </summary>
        /// <param name="MinMaxPriceID">int MinMaxPrice id</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the deleted MinMaxPrice</returns>
        public static FacadeResult<MinMaxPrice> DeleteMinMaxPriceByID(int minMaxPriceID, int currentUserID)
        {
            FacadeResult<MinMaxPrice> result = new FacadeResult<MinMaxPrice>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    MinMaxPrice minMaxPrice = context.GetMinMaxPriceByID(minMaxPriceID);
                    if (minMaxPrice != null)
                    {
                        minMaxPrice.LastUpdatedBy = currentUserID;
                        minMaxPrice.DateTimeUpdated = DateTime.Now;
                        minMaxPrice.IsActive = false;
                        context.UpdateMinMaxPrice(minMaxPrice);

                        userActivity.UserActivityTypeID = "DeleteAuctionHall";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = minMaxPriceID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = minMaxPrice;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Min Max Price");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }
        #endregion

        #region Category Template
        /// <summary>
        /// Add new category template
        /// </summary>
        /// <param name="kpknl">The category template object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the added category template </returns>
        public static FacadeResult<CategoryTemplate> AddNewCategoryTemplate(CategoryTemplate categoryTemplate, int currentUserID)
        {

            FacadeResult<CategoryTemplate> result = new FacadeResult<CategoryTemplate>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    categoryTemplate.CreatedBy = currentUserID;
                    context.InsertCategoryTemplate(categoryTemplate);

                    userActivity.UserActivityTypeID = "AddNewCategoryTemplate";
                    userActivity.CreatedBy = currentUserID;
                    userActivity.ReferenceID = categoryTemplate.CategoryID.ToString();
                    context.InsertUserActivity(userActivity);

                    result.ReturnData = categoryTemplate;
                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Update CategoryTemplate
        /// </summary>
        /// <param name="CategoryTemplate">The CategoryTemplate object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated CategoryTemplate</returns>
        public static FacadeResult<CategoryTemplate> UpdateCategoryTemplate(CategoryTemplate categoryTemplate, String oldFieldName, int currentUserID)
        {
            FacadeResult<CategoryTemplate> result = new FacadeResult<CategoryTemplate>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    CategoryTemplate updateCategoryTemplate = context.GetActiveCategoryTemplateByID(categoryTemplate.CategoryID.Value, oldFieldName);
                    if (updateCategoryTemplate != null)
                    {
                        updateCategoryTemplate.CategoryID = categoryTemplate.CategoryID;
                        updateCategoryTemplate.FieldName = categoryTemplate.FieldName;
                        updateCategoryTemplate.IsMandatory = categoryTemplate.IsMandatory;
                        updateCategoryTemplate.LastUpdatedBy = currentUserID;
                        updateCategoryTemplate.DateTimeUpdated = DateTime.Now;

                        context.UpdateCategoryTemplateByCategoryIDAndOldFieldName(
                            categoryTemplate.CategoryID.Value,
                            oldFieldName,
                            categoryTemplate.FieldName,
                            categoryTemplate.IsMandatory.HasValue ? categoryTemplate.IsMandatory.Value : false);

                        updateCategoryTemplate = context.GetActiveCategoryTemplateByID(categoryTemplate.CategoryID.Value, categoryTemplate.FieldName);
                        context.UpdateCategoryTemplate(updateCategoryTemplate);

                        userActivity.UserActivityTypeID = "UpdateCategoryTemplate";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = updateCategoryTemplate.CategoryID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updateCategoryTemplate;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Category Template");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Delete CategoryTemplate by id (set as inactive)
        /// </summary>
        /// <param name="categoryID"></param>
        /// <param name="fieldName"></param>
        /// <param name="currentUserID"></param>
        /// <returns>Facade result containing the deleted CategoryTemplate</returns>
        public static FacadeResult<CategoryTemplate> DeleteCategoryTemplateByID(int categoryID, String fieldName, int currentUserID)
        {
            FacadeResult<CategoryTemplate> result = new FacadeResult<CategoryTemplate>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    CategoryTemplate categoryTemplate = context.GetActiveCategoryTemplateByID(categoryID, fieldName);
                    if (categoryTemplate != null)
                    {
                        categoryTemplate.LastUpdatedBy = currentUserID;
                        categoryTemplate.DateTimeUpdated = DateTime.Now;
                        categoryTemplate.IsActive = false;
                        context.UpdateCategoryTemplate(categoryTemplate);

                        userActivity.UserActivityTypeID = "DeleteCategoryTemplate";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = categoryID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = categoryTemplate;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Category Template");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }
        #endregion

        #region Web Content

        /// <summary>
        /// Update web content
        /// </summary>
        /// <param name="webContent">The web content object</param>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the updated web content</returns>
        public static FacadeResult<WebContent> UpdateWebContent(WebContent webContent, int currentUserID)
        {
            FacadeResult<WebContent> result = new FacadeResult<WebContent>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    WebContent updatedWebContent = context.GetActiveWebContentByID(webContent.WebContentTypeID.Value);
                    if (updatedWebContent != null)
                    {
                        updatedWebContent.Content = webContent.Content;
                        updatedWebContent.LastUpdatedBy = currentUserID;
                        updatedWebContent.DateTimeUpdated = DateTime.Now;
                        context.UpdateWebContent(updatedWebContent);

                        userActivity.UserActivityTypeID = "UpdateWebContent";
                        userActivity.CreatedBy = currentUserID;
                        userActivity.ReferenceID = webContent.WebContentTypeID.ToString();
                        context.InsertUserActivity(userActivity);

                        result.ReturnData = updatedWebContent;
                        result.IsSuccess = true;
                        result.ErrorMessage = String.Empty;
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Web Content");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        #endregion

        #endregion
    }
}
