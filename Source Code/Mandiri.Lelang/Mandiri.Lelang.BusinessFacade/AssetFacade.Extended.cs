﻿using Mandiri.Lelang.DataRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insight.Database;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.Helper;
using System.IO;
using System.Configuration;
using Mandiri.Lelang.Helper.Enum;

namespace Mandiri.Lelang.BusinessFacade
{
    public partial class AssetFacade : BaseFacade
    {
        /// <summary>
        /// Get all assets by multiple criterias for Volunteer Sell
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="categoryID"></param>
        /// <param name="provinceID"></param>
        /// <param name="cityID"></param>
        /// <param name="minPriceID"></param>
        /// <param name="maxPriceID"></param>
        /// <param name="hotPrice"></param>
        /// <param name="auctionDate"></param>
        /// <param name="documentTypeID"></param>
        /// <param name="address"></param>
        /// <param name="sortIndex"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static FacadeResult<IList<dynamic>> GetAllActiveAssetsForVolunteerSellByCriteria(String keywords = "", int categoryID = -1, int provinceID = -1, int cityID = -1, int minPriceID = -1, int maxPriceID = -1, bool hotPrice = false, DateTime? auctionDate = null, int documentTypeID = -1, String address = "", int sortIndex = 0, long pageIndex = 0, int pageSize = 12, bool isFromHome = false, long? memberID = null, byte type = 2)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "A.DateTimeCreated", "CASE WHEN A.CurrentStatusID IN (5, 7) THEN 1 ELSE 0 END ASC, CASE WHEN A.Type = 1 THEN ISNULL(A.Price, 0) ELSE ISNULL(A.NewPrice, 0) END", "CASE WHEN A.CurrentStatusID IN (5, 7) THEN 1 ELSE 0 END ASC, CASE WHEN A.Type = 1 THEN ISNULL(A.Price, 0) ELSE ISNULL(A.NewPrice, 0) END", "CASE WHEN CAST(S.ScheduleDate AS DATE) >= CAST(GETDATE() AS DATE) THEN 0 ELSE 1 END ASC, ScheduleDate" };
            String[] sortColumnDirections = new String[] { "DESC", "DESC", "ASC", "ASC" };
            decimal? price;
            decimal minPrice = 0;
            decimal maxPrice = 0;
            int flagFromHome = isFromHome ? 1 : 0;

            if (minPriceID != -1)
            {
                price = context.GetActiveMinMaxPriceByID(minPriceID).Value;
                if (price.HasValue)
                {
                    minPrice = price.Value;
                }
            }

            if (maxPriceID != -1)
            {
                price = context.GetActiveMinMaxPriceByID(maxPriceID).Value;
                if (price.HasValue)
                {
                    maxPrice = price.Value;
                }
            }

            IList<dynamic> assets = context.QuerySql<dynamic>(
                /*String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                A.AssetID, AssetCode, A.CategoryID, C.Name AS CategoryName, A.Address, A.ProvinceID, P.Name AS ProvinceName, A.CityID,
                CT.Name AS CityName, OldPrice, NewPrice, Photo1, A.AuctionScheduleID, S.ScheduleDate, S.ScheduleTime, 
                CASE WHEN S.Timezone = 1 THEN 'WIB' WHEN S.Timezone = 2 THEN 'WIT' WHEN S.Timezone = 3 THEN 'WITA' END AS Timezone,
                A.DateTimeCreated, ISNULL(ODT.Name, '') AS DocumentTypeName
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                LEFT JOIN AuctionSchedule S ON S.AuctionScheduleID = A.AuctionScheduleID
                LEFT JOIN AssetStatusHistory H ON H.AssetID = A.AssetID AND H.NewAssetStatusID IN (5, 7)
                LEFT JOIN OwnershipDocumentType ODT ON ODT.OwnershipDocumentTypeID = A.OwnershipDocumentTypeID
                WHERE A.IsActive = 1 AND A.CurrentStatusID IN (4, 5, 7) 
                AND (H.DateTimeCreated IS NULL OR (H.DateTimeCreated IS NOT NULL 
                AND (CAST(DATEADD(day, @VisibilityDaysPeriodForPaidAndSold, H.DateTimeCreated) AS DATE) >= CAST(GETDATE() AS DATE))))
                AND ((A.CategoryID = @CategoryID OR @CategoryID = -1) AND (A.ProvinceID = @ProvinceID OR @ProvinceID = -1) AND (A.CityID = @CityID OR @CityID = -1)
                AND (A.NewPrice >= @MinPrice OR @MinPriceID = -1) AND (A.NewPrice <= @MaxPrice OR @MaxPriceID = -1) 
                AND ((@HotPrice = 1 AND NewPrice IS NOT NULL AND OldPrice IS NOT NULL AND NewPrice <> 0 AND OldPrice <> 0 AND NewPrice < OldPrice) OR @HotPrice = 0)
                AND (A.OwnershipDocumentTypeID = @DocumentTypeID OR @DocumentTypeID = -1)
                AND (A.Address LIKE @Address)
                AND (S.ScheduleDate = CAST(@AuctionDate AS DATE) OR @AuctionDate = '')
                AND ((@SortIndex = 3 AND S.ScheduleDate IS NOT NULL) OR @SortIndex <> 3)
                AND (CT.Name LIKE @Keywords OR C.Name LIKE @Keywords OR A.Address LIKE @Keywords))) TBL
                WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"*/
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                A.AssetID, AssetCode, A.CategoryID, C.Name AS CategoryName, A.Address, A.ProvinceID, P.Name AS ProvinceName, A.CityID,
                CT.Name AS CityName, ISNULL(OldPrice, 0) AS OldPrice, CASE WHEN A.Type = 1 THEN ISNULL(Price, 0) ELSE ISNULL(NewPrice, 0) END AS NewPrice, Photo1, A.AuctionScheduleID, S.ScheduleDate, S.ScheduleTime, 
                CASE WHEN S.Timezone = 1 THEN 'WIB' WHEN S.Timezone = 2 THEN 'WIT' WHEN S.Timezone = 3 THEN 'WITA' END AS Timezone,
                A.DateTimeCreated, ISNULL(ODT.Name, '') AS DocumentTypeName, A.CurrentStatusID, ISNULL(A.IsEmailAuction, 0) AS IsEmailAuction,
                CAST(CASE WHEN MF.AssetID IS NOT NULL THEN 1 ELSE 0 END AS BIT) AS IsFavorite, ISNULL(A.Price, 0) AS Price, A.Type
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                LEFT JOIN AuctionSchedule S ON S.AuctionScheduleID = A.AuctionScheduleID
                LEFT JOIN (
                SELECT A.* FROM AssetStatusHistory A INNER JOIN (
                SELECT AssetID, MAX(AssetStatusHistoryID) AS MaxID FROM AssetStatusHistory WHERE NewAssetStatusID IN (5, 7) GROUP BY AssetID) B ON A.AssetID = B.AssetID AND A.AssetStatusHistoryID = B.MaxID                
                ) H ON H.AssetID = A.AssetID
                LEFT JOIN OwnershipDocumentType ODT ON ODT.OwnershipDocumentTypeID = A.OwnershipDocumentTypeID
                LEFT JOIN 
                (
                SELECT DISTINCT AssetID FROM MemberFavorite WHERE @MemberID IS NOT NULL AND MemberID = @MemberID AND IsActive = 1 AND IsFavorite = 1
                ) MF ON MF.AssetID = A.AssetID
                WHERE A.IsActive = 1 AND A.Type = @Type AND ((@FlagFromHome = 1 AND A.CurrentStatusID = 4) OR (@FlagFromHome = 0 AND A.CurrentStatusID IN (4, 5, 7)))
                AND ((@FlagFromHome = 1 AND A.Price > 0) OR @FlagFromHome = 0)                
                AND ((A.CategoryID = @CategoryID OR @CategoryID = -1) AND (A.ProvinceID = @ProvinceID OR @ProvinceID = -1) AND (A.CityID = @CityID OR @CityID = -1))
                AND (A.OwnershipDocumentTypeID = @DocumentTypeID OR @DocumentTypeID = -1)
                AND (A.Address LIKE @Address)
                AND (A.ExpiredDate <> '' AND CAST(A.ExpiredDate AS DATE) >= CAST(GETDATE() AS DATE))
                AND (CT.Name LIKE @Keywords OR C.Name LIKE @Keywords OR A.Address LIKE @Keywords OR A.DocumentNumber LIKE @Keywords OR A.AssetCode LIKE @Keywords)
                AND (@FlagFromHome = 0 OR A.ExpiredDate IS NULL OR 
                (@FlagFromHome = 1 AND A.ExpiredDate IS NOT NULL AND (DATEADD(dd, 0, DATEDIFF(dd, 0, A.ExpiredDate)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))))
                ) TBL
                WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortColumnDirections[sortIndex])
                , new
                {
                    SortIndex = sortIndex,
                    PageIndex = pageIndex * pageSize,
                    PageSize = pageSize,
                    CategoryID = categoryID,
                    ProvinceID = provinceID,
                    CityID = cityID,
                    MinPriceID = minPriceID,
                    MinPrice = minPrice,
                    MaxPrice = maxPrice,
                    MaxPriceID = maxPriceID,
                    HotPrice = hotPrice,
                    DocumentTypeID = documentTypeID,
                    VisibilityDaysPeriodForPaidAndSold = int.Parse(context.GetActiveSystemSettingByID("VisibilityDaysPeriodForPaidAndSold").Value),
                    Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()),
                    Address = String.Format("%{0}%", String.IsNullOrEmpty(address) ? String.Empty : address.Trim()),
                    AuctionDate = auctionDate.HasValue ? auctionDate.Value.ToString("yyyy-MM-dd") : "",
                    FlagFromHome = flagFromHome,
                    MemberID = memberID,
                    Type = type
                });
            result.ReturnData = assets;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }
        
        /// <summary>
        /// Get all assets by multiple criterias
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="categoryID"></param>
        /// <param name="provinceID"></param>
        /// <param name="cityID"></param>
        /// <param name="minPriceID"></param>
        /// <param name="maxPriceID"></param>
        /// <param name="hotPrice"></param>
        /// <param name="auctionDate"></param>
        /// <param name="documentTypeID"></param>
        /// <param name="address"></param>
        /// <param name="sortIndex"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static FacadeResult<IList<dynamic>> GetAllActiveAssetsByCriteria(String keywords = "", int categoryID = -1, int provinceID = -1, int cityID = -1, int minPriceID = -1, int maxPriceID = -1, bool hotPrice = false, DateTime? auctionDate = null, int documentTypeID = -1, String address = "", int sortIndex = 0, long pageIndex = 0, int pageSize = 12, bool isFromHome = false, long? memberID = null, bool isVolunteerSellType = false, bool isAuctionType = false)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            String[] sortColumnNames = new String[] { "A.DateTimeCreated", "CASE WHEN A.CurrentStatusID IN (5, 7) THEN 1 ELSE 0 END ASC, CASE WHEN A.Type = 1 THEN ISNULL(A.Price, 0) ELSE ISNULL(A.NewPrice, 0) END", "CASE WHEN A.CurrentStatusID IN (5, 7) THEN 1 ELSE 0 END ASC, CASE WHEN A.Type = 1 THEN ISNULL(A.Price, 0) ELSE ISNULL(A.NewPrice, 0) END", "CASE WHEN CAST(S.ScheduleDate AS DATE) >= CAST(GETDATE() AS DATE) THEN 0 ELSE 1 END ASC, ScheduleDate" };
            String[] sortColumnDirections = new String[] { "DESC", "DESC", "ASC", "ASC" };
            decimal? price;
            decimal minPrice = 0;
            decimal maxPrice = 0;
            int flagFromHome = isFromHome ? 1 : 0;

            if (minPriceID != -1)
            {
                price = context.GetActiveMinMaxPriceByID(minPriceID).Value;
                if (price.HasValue)
                {
                    minPrice = price.Value;
                }
            }

            if (maxPriceID != -1)
            {
                price = context.GetActiveMinMaxPriceByID(maxPriceID).Value;
                if (price.HasValue)
                {
                    maxPrice = price.Value;
                }
            }

            IList<dynamic> assets = context.QuerySql<dynamic>(
                /*String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                A.AssetID, AssetCode, A.CategoryID, C.Name AS CategoryName, A.Address, A.ProvinceID, P.Name AS ProvinceName, A.CityID,
                CT.Name AS CityName, OldPrice, NewPrice, Photo1, A.AuctionScheduleID, S.ScheduleDate, S.ScheduleTime, 
                CASE WHEN S.Timezone = 1 THEN 'WIB' WHEN S.Timezone = 2 THEN 'WIT' WHEN S.Timezone = 3 THEN 'WITA' END AS Timezone,
                A.DateTimeCreated, ISNULL(ODT.Name, '') AS DocumentTypeName
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                LEFT JOIN AuctionSchedule S ON S.AuctionScheduleID = A.AuctionScheduleID
                LEFT JOIN AssetStatusHistory H ON H.AssetID = A.AssetID AND H.NewAssetStatusID IN (5, 7)
                LEFT JOIN OwnershipDocumentType ODT ON ODT.OwnershipDocumentTypeID = A.OwnershipDocumentTypeID
                WHERE A.IsActive = 1 AND A.CurrentStatusID IN (4, 5, 7) 
                AND (H.DateTimeCreated IS NULL OR (H.DateTimeCreated IS NOT NULL 
                AND (CAST(DATEADD(day, @VisibilityDaysPeriodForPaidAndSold, H.DateTimeCreated) AS DATE) >= CAST(GETDATE() AS DATE))))
                AND ((A.CategoryID = @CategoryID OR @CategoryID = -1) AND (A.ProvinceID = @ProvinceID OR @ProvinceID = -1) AND (A.CityID = @CityID OR @CityID = -1)
                AND (A.NewPrice >= @MinPrice OR @MinPriceID = -1) AND (A.NewPrice <= @MaxPrice OR @MaxPriceID = -1) 
                AND ((@HotPrice = 1 AND NewPrice IS NOT NULL AND OldPrice IS NOT NULL AND NewPrice <> 0 AND OldPrice <> 0 AND NewPrice < OldPrice) OR @HotPrice = 0)
                AND (A.OwnershipDocumentTypeID = @DocumentTypeID OR @DocumentTypeID = -1)
                AND (A.Address LIKE @Address)
                AND (S.ScheduleDate = CAST(@AuctionDate AS DATE) OR @AuctionDate = '')
                AND ((@SortIndex = 3 AND S.ScheduleDate IS NOT NULL) OR @SortIndex <> 3)
                AND (CT.Name LIKE @Keywords OR C.Name LIKE @Keywords OR A.Address LIKE @Keywords))) TBL
                WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"*/
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                A.AssetID, AssetCode, A.CategoryID, C.Name AS CategoryName, A.Address, A.ProvinceID, P.Name AS ProvinceName, A.CityID,
                CT.Name AS CityName, ISNULL(OldPrice, 0) AS OldPrice, CASE WHEN A.Type = 1 THEN ISNULL(Price, 0) ELSE ISNULL(NewPrice, 0) END AS NewPrice, Photo1, A.AuctionScheduleID, S.ScheduleDate, S.ScheduleTime, 
                CASE WHEN S.Timezone = 1 THEN 'WIB' WHEN S.Timezone = 2 THEN 'WIT' WHEN S.Timezone = 3 THEN 'WITA' END AS Timezone,
                A.DateTimeCreated, ISNULL(ODT.Name, '') AS DocumentTypeName, A.CurrentStatusID, ISNULL(A.IsEmailAuction, 0) AS IsEmailAuction,
                CAST(CASE WHEN MF.AssetID IS NOT NULL THEN 1 ELSE 0 END AS BIT) AS IsFavorite, ISNULL(A.Price, 0) AS Price, A.Type
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                LEFT JOIN AuctionSchedule S ON S.AuctionScheduleID = A.AuctionScheduleID
                LEFT JOIN (
                SELECT A.* FROM AssetStatusHistory A INNER JOIN (
                SELECT AssetID, MAX(AssetStatusHistoryID) AS MaxID FROM AssetStatusHistory WHERE NewAssetStatusID IN (5, 7) GROUP BY AssetID) B ON A.AssetID = B.AssetID AND A.AssetStatusHistoryID = B.MaxID                
                ) H ON H.AssetID = A.AssetID
                LEFT JOIN OwnershipDocumentType ODT ON ODT.OwnershipDocumentTypeID = A.OwnershipDocumentTypeID
                LEFT JOIN 
                (
                SELECT DISTINCT AssetID FROM MemberFavorite WHERE @MemberID IS NOT NULL AND MemberID = @MemberID AND IsActive = 1 AND IsFavorite = 1
                ) MF ON MF.AssetID = A.AssetID
                WHERE A.IsActive = 1 AND (A.Type = @IsVolunteerSellType OR A.Type = @IsAuctionType) AND ((@FlagFromHome = 1 AND A.CurrentStatusID = 4) OR (@FlagFromHome = 0 AND A.CurrentStatusID IN (4, 5, 7) AND A.Type = 2) OR (@FlagFromHome = 0 AND A.CurrentStatusID = 4 AND A.Type = 1))
                AND ((@FlagFromHome = 1 AND A.NewPrice > 0) OR (@FlagFromHome = 1 AND A.Price > 0) OR @FlagFromHome = 0)
                AND ((H.DateTimeCreated IS NULL AND A.Type = 2) OR (H.DateTimeCreated IS NOT NULL
                AND (DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(day, @VisibilityDaysPeriodForPaidAndSold, H.DateTimeCreated))) > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))) OR (A.ExpiredDate IS NOT NULL AND CAST(A.ExpiredDate AS DATE) >= CAST(GETDATE() AS DATE)))
                AND ((A.CategoryID = @CategoryID OR @CategoryID = -1) AND (A.ProvinceID = @ProvinceID OR @ProvinceID = -1) AND (A.CityID = @CityID OR @CityID = -1))
                AND (A.NewPrice >= @MinPrice OR @MinPriceID = -1) AND (A.NewPrice <= @MaxPrice OR @MaxPriceID = -1) 
                AND ((@HotPrice = 1 AND NewPrice IS NOT NULL AND OldPrice IS NOT NULL AND NewPrice <> 0 AND OldPrice <> 0 AND NewPrice < OldPrice AND A.CurrentStatusID NOT IN (5,7)) OR @HotPrice = 0)
                AND (A.OwnershipDocumentTypeID = @DocumentTypeID OR @DocumentTypeID = -1)
                AND (A.Address LIKE @Address)
                AND (@AuctionDate <> '' AND S.ScheduleDate = DATEADD(dd, 0, DATEDIFF(dd, 0, @AuctionDate)) OR @AuctionDate = '')
                AND ((@SortIndex = 3 AND S.ScheduleDate IS NOT NULL AND CAST(S.ScheduleDate AS DATE) >= CAST(GETDATE() AS DATE)) OR @SortIndex <> 3)
                AND (CT.Name LIKE @Keywords OR C.Name LIKE @Keywords OR A.Address LIKE @Keywords OR A.DocumentNumber LIKE @Keywords OR A.AssetCode LIKE @Keywords)
                AND (@FlagFromHome = 0 OR S.ScheduleDate IS NULL OR 
                (@FlagFromHome = 1 AND S.ScheduleDate IS NOT NULL AND (DATEADD(dd, 0, DATEDIFF(dd, 0, S.ScheduleDate)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))))
                ) TBL
                WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortColumnDirections[sortIndex])
                , new
                {
                    SortIndex = sortIndex,
                    PageIndex = pageIndex * pageSize,
                    PageSize = pageSize,
                    CategoryID = categoryID,
                    ProvinceID = provinceID,
                    CityID = cityID,
                    MinPriceID = minPriceID,
                    MinPrice = minPrice,
                    MaxPrice = maxPrice,
                    MaxPriceID = maxPriceID,
                    HotPrice = hotPrice,
                    DocumentTypeID = documentTypeID,
                    VisibilityDaysPeriodForPaidAndSold = int.Parse(context.GetActiveSystemSettingByID("VisibilityDaysPeriodForPaidAndSold").Value),
                    Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()),
                    Address = String.Format("%{0}%", String.IsNullOrEmpty(address) ? String.Empty : address.Trim()),
                    AuctionDate = auctionDate.HasValue ? auctionDate.Value.ToString("yyyy-MM-dd") : "",
                    FlagFromHome = flagFromHome,
                    MemberID = memberID,
                    IsVolunteerSellType = isVolunteerSellType ? 1 : 0,
                    IsAuctionType = isAuctionType ? 2 : 0
                });
            result.ReturnData = assets;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all assets by multiple criterias count
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="categoryID"></param>
        /// <param name="provinceID"></param>
        /// <param name="cityID"></param>
        /// <param name="minPriceID"></param>
        /// <param name="maxPriceID"></param>
        /// <param name="hotPrice"></param>
        /// <param name="auctionDate"></param>
        /// <param name="documentTypeID"></param>
        /// <param name="address"></param>
        /// <param name="sortIndex"></param>
        /// <returns></returns>
        public static FacadeResult<long> GetAllActiveAssetsByCriteriaCount(String keywords = "", int categoryID = -1, int provinceID = -1, int cityID = -1, int minPriceID = -1, int maxPriceID = -1, bool hotPrice = false, DateTime? auctionDate = null, int documentTypeID = -1, String address = "", int sortIndex = 0, bool isFromHome = false, long? memberID = null, bool isVolunteerSellType = false, bool isAuctionType = false)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();
            decimal? price;
            decimal minPrice = 0;
            decimal maxPrice = 0;
            int flagFromHome = isFromHome ? 1 : 0;

            if (minPriceID != -1)
            {
                price = context.GetActiveMinMaxPriceByID(minPriceID).Value;
                if (price.HasValue)
                {
                    minPrice = price.Value;
                }
            }
            if (maxPriceID != -1)
            {
                price = context.GetActiveMinMaxPriceByID(maxPriceID).Value;
                if (price.HasValue)
                {
                    maxPrice = price.Value;
                }
            }
            /*@"SELECT COUNT_BIG(A.AssetID)
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                LEFT JOIN AuctionSchedule S ON S.AuctionScheduleID = A.AuctionScheduleID
                LEFT JOIN AssetStatusHistory H ON H.AssetID = A.AssetID AND H.NewAssetStatusID IN (5, 7)
                LEFT JOIN OwnershipDocumentType ODT ON ODT.OwnershipDocumentTypeID = A.OwnershipDocumentTypeID
                WHERE A.IsActive = 1 AND ((@FlagFromHome = 1 AND A.CurrentStatusID = 4) OR (@FlagFromHome = 0 AND A.CurrentStatusID IN (4, 5, 7)))
                AND (H.DateTimeCreated IS NULL OR (H.DateTimeCreated IS NOT NULL 
                AND (DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(day, @VisibilityDaysPeriodForPaidAndSold, H.DateTimeCreated))) > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))))
                AND ((A.CategoryID = @CategoryID OR @CategoryID = -1) AND (A.ProvinceID = @ProvinceID OR @ProvinceID = -1) AND (A.CityID = @CityID OR @CityID = -1))
                AND (A.NewPrice >= @MinPrice OR @MinPriceID = -1) AND (A.NewPrice <= @MaxPrice OR @MaxPriceID = -1) 
                AND ((@HotPrice = 1 AND NewPrice IS NOT NULL AND OldPrice IS NOT NULL AND NewPrice <> 0 AND OldPrice <> 0 AND NewPrice < OldPrice AND A.CurrentStatusID NOT IN (5,7)) OR @HotPrice = 0)
                AND (A.OwnershipDocumentTypeID = @DocumentTypeID OR @DocumentTypeID = -1)
                AND (A.Address LIKE @Address)
                AND (@AuctionDate <> '' AND S.ScheduleDate = DATEADD(dd, 0, DATEDIFF(dd, 0, @AuctionDate)) OR @AuctionDate = '')
                AND ((@SortIndex = 3 AND S.ScheduleDate IS NOT NULL) OR @SortIndex <> 3)
                AND (CT.Name LIKE @Keywords OR C.Name LIKE @Keywords OR A.Address LIKE @Keywords OR A.DocumentNumber LIKE @Keywords OR A.AssetCode LIKE @Keywords)
                AND (@FlagFromHome = 0 OR S.ScheduleDate IS NULL OR 
                (@FlagFromHome = 1 AND S.ScheduleDate IS NOT NULL AND (DATEADD(dd, 0, DATEDIFF(dd, 0, S.ScheduleDate)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))))"*/
            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(A.AssetID)
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                LEFT JOIN AuctionSchedule S ON S.AuctionScheduleID = A.AuctionScheduleID
                LEFT JOIN (
                SELECT A.* FROM AssetStatusHistory A INNER JOIN (
                SELECT AssetID, MAX(AssetStatusHistoryID) AS MaxID FROM AssetStatusHistory WHERE NewAssetStatusID IN (5, 7) GROUP BY AssetID) B ON A.AssetID = B.AssetID AND A.AssetStatusHistoryID = B.MaxID                
                ) H ON H.AssetID = A.AssetID
                LEFT JOIN OwnershipDocumentType ODT ON ODT.OwnershipDocumentTypeID = A.OwnershipDocumentTypeID
                LEFT JOIN 
                (
                SELECT DISTINCT AssetID FROM MemberFavorite WHERE @MemberID IS NOT NULL AND MemberID = @MemberID AND IsActive = 1 AND IsFavorite = 1
                ) MF ON MF.AssetID = A.AssetID
                WHERE A.IsActive = 1 AND (A.Type = @IsVolunteerSellType OR A.Type = @IsAuctionType) AND ((@FlagFromHome = 1 AND A.CurrentStatusID = 4) OR (@FlagFromHome = 0 AND A.CurrentStatusID IN (4, 5, 7) AND A.Type = 2) OR (@FlagFromHome = 0 AND A.CurrentStatusID = 4 AND A.Type = 1))
                AND ((@FlagFromHome = 1 AND A.NewPrice > 0) OR (@FlagFromHome = 1 AND A.Price > 0) OR @FlagFromHome = 0)
                AND ((H.DateTimeCreated IS NULL AND A.Type = 2) OR (H.DateTimeCreated IS NOT NULL
                AND (DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(day, @VisibilityDaysPeriodForPaidAndSold, H.DateTimeCreated))) > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))) OR (A.ExpiredDate IS NOT NULL AND CAST(A.ExpiredDate AS DATE) >= CAST(GETDATE() AS DATE)))
                AND ((A.CategoryID = @CategoryID OR @CategoryID = -1) AND (A.ProvinceID = @ProvinceID OR @ProvinceID = -1) AND (A.CityID = @CityID OR @CityID = -1))
                AND (A.NewPrice >= @MinPrice OR @MinPriceID = -1) AND (A.NewPrice <= @MaxPrice OR @MaxPriceID = -1) 
                AND ((@HotPrice = 1 AND NewPrice IS NOT NULL AND OldPrice IS NOT NULL AND NewPrice <> 0 AND OldPrice <> 0 AND NewPrice < OldPrice AND A.CurrentStatusID NOT IN (5,7)) OR @HotPrice = 0)
                AND (A.OwnershipDocumentTypeID = @DocumentTypeID OR @DocumentTypeID = -1)
                AND (A.Address LIKE @Address)
                AND (@AuctionDate <> '' AND S.ScheduleDate = DATEADD(dd, 0, DATEDIFF(dd, 0, @AuctionDate)) OR @AuctionDate = '')
                AND ((@SortIndex = 3 AND S.ScheduleDate IS NOT NULL AND CAST(S.ScheduleDate AS DATE) >= CAST(GETDATE() AS DATE)) OR @SortIndex <> 3)
                AND (CT.Name LIKE @Keywords OR C.Name LIKE @Keywords OR A.Address LIKE @Keywords OR A.DocumentNumber LIKE @Keywords OR A.AssetCode LIKE @Keywords)
                AND (@FlagFromHome = 0 OR S.ScheduleDate IS NULL OR 
                (@FlagFromHome = 1 AND S.ScheduleDate IS NOT NULL AND (DATEADD(dd, 0, DATEDIFF(dd, 0, S.ScheduleDate)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))))"
                , new
                {
                    SortIndex = sortIndex,
                    CategoryID = categoryID,
                    ProvinceID = provinceID,
                    CityID = cityID,
                    MinPriceID = minPriceID,
                    MinPrice = minPrice,
                    MaxPrice = maxPrice,
                    MaxPriceID = maxPriceID,
                    HotPrice = hotPrice,
                    DocumentTypeID = documentTypeID,
                    VisibilityDaysPeriodForPaidAndSold = int.Parse(context.GetActiveSystemSettingByID("VisibilityDaysPeriodForPaidAndSold").Value),
                    Keywords = String.Format("%{0}%", String.IsNullOrEmpty(keywords) ? String.Empty : keywords.Trim()),
                    Address = String.Format("%{0}%", String.IsNullOrEmpty(address) ? String.Empty : address.Trim()),
                    AuctionDate = auctionDate.HasValue ? auctionDate.Value.ToString("yyyy-MM-dd") : "",
                    FlagFromHome = flagFromHome,
                    MemberID = memberID,
                    IsVolunteerSellType = isVolunteerSellType ? 1 : 0,
                    IsAuctionType = isAuctionType ? 2 : 0
                });
            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get active asset details by asset id
        /// </summary>
        /// <param name="assetID"></param>
        /// <returns></returns>
        public static FacadeResult<dynamic> GetActiveAssetDetailsByAssetID(long assetID, long? memberID = null)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<dynamic> result = new FacadeResult<dynamic>();

            dynamic assetDetails = context.SingleSql<dynamic>(
                @"SELECT
                A.AssetID, AssetCode, A.CategoryID, C.Name AS CategoryName, A.Address, A.ProvinceID, P.Name AS ProvinceName, A.CityID,
                CT.Name AS CityName, ISNULL(OldPrice, 0) AS OldPrice, CASE WHEN A.Type = 1 THEN ISNULL(Price, 0) ELSE ISNULL(NewPrice, 0) END AS NewPrice, Photo1, ISNULL(Photo2, '') AS Photo2, ISNULL(Photo3, '') AS Photo3, ISNULL(Photo4, '') AS Photo4, ISNULL(Photo5, '') AS Photo5,
                A.AuctionScheduleID, S.ScheduleDate, S.ScheduleTime, 
                CASE WHEN S.Timezone = 1 THEN 'WIB' WHEN S.Timezone = 2 THEN 'WIT' WHEN S.Timezone = 3 THEN 'WITA' END AS Timezone,
                A.DateTimeCreated, ISNULL(ODT.Name, '') AS DocumentTypeName, ISNULL(K.Name, '') AS KPKNLName, 
                ISNULL(A.KPKNLAddress, '') AS KPKNLAddress, ISNULL(A.KPKNLPhone1, '') AS KPKNLPhone1, ISNULL(A.KPKNLPhone2, '') AS KPKNLPhone2,
                ISNULL(B.Name, '') AS BranchName, ISNULL(B.Address, '') AS BranchAddress, ISNULL(B.Phone1, '') AS BranchPhone1, ISNULL(B.Phone2, '') AS BranchPhone2,
                ISNULL(AH.Name, '') AS AuctionHallName, ISNULL(A.AuctionHallAddress, '') AS AuctionHallAddress, ISNULL(A.AuctionHallPhone1, '') AS AuctionHallPhone1, ISNULL(A.AuctionHallPhone2, '') AS AuctionHallPhone2,
                Latitude, Longitude, TransferInformation, A.CurrentStatusID, ISNULL(S.AuctionAddress, '') AS AuctionAddress, ISNULL(A.IsEmailAuction, 0) AS IsEmailAuction, 
                ISNULL(A.EmailAuctionURL, '') AS EmailAuctionURL,
                CAST(CASE WHEN MF.AssetID IS NOT NULL THEN 1 ELSE 0 END AS BIT) AS IsFavorite,
                A.Type, A.Price
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                LEFT JOIN AuctionSchedule S ON S.AuctionScheduleID = A.AuctionScheduleID
                LEFT JOIN AssetStatusHistory H ON H.AssetID = A.AssetID AND H.NewAssetStatusID IN (5, 7)
                LEFT JOIN OwnershipDocumentType ODT ON ODT.OwnershipDocumentTypeID = A.OwnershipDocumentTypeID
                LEFT JOIN Branch B ON B.BranchID = A.BranchID
                LEFT JOIN KPKNL K ON K.KPKNLID = A.KPKNLID
                LEFT JOIN AuctionHall AH ON AH.AuctionHallID = A.AuctionHallID
                LEFT JOIN 
                (
                SELECT DISTINCT AssetID FROM MemberFavorite WHERE @MemberID IS NOT NULL AND MemberID = @MemberID AND IsActive = 1 AND IsFavorite = 1
                ) MF ON MF.AssetID = A.AssetID
                WHERE A.IsActive = 1 AND A.CurrentStatusID IN (4, 5, 7) AND A.AssetID = @AssetID
                AND (H.DateTimeCreated IS NULL OR (H.DateTimeCreated IS NOT NULL 
                AND (DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(day, @VisibilityDaysPeriodForPaidAndSold, H.DateTimeCreated))) > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))))"
                , new
                {
                    AssetID = assetID,
                    VisibilityDaysPeriodForPaidAndSold = int.Parse(AdministrationFacade.GetActiveSystemSettingByID("VisibilityDaysPeriodForPaidAndSold").ReturnData.Value),
                    MemberID = memberID
                });

            if (assetDetails != null)
            {
                result.ReturnData = assetDetails;
                result.IsSuccess = true;
                result.ErrorMessage = String.Empty;
            }
            else
            {
                result.ReturnData = null;
                result.IsSuccess = false;
                result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "Asset");
            }

            return result;
        }

        /// <summary>
        /// Get active asset template values by asset id
        /// </summary>
        /// <param name="assetID"></param>
        /// <returns></returns>
        public static FacadeResult<IList<AssetCategoryTemplateValue>> GetAllActiveAssetCategoryTemplateValuesByAssetID(long assetID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<AssetCategoryTemplateValue>> result = new FacadeResult<IList<AssetCategoryTemplateValue>>();

            IList<AssetCategoryTemplateValue> categoryTemplateValues = context.GetAllActiveAssetCategoryTemplateValuesByAssetID(assetID);

            result.ReturnData = categoryTemplateValues;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active current auction schedules (>= today)
        /// </summary>
        /// <returns></returns>
        public static FacadeResult<IList<dynamic>> GetAllActiveCurrentAuctionSchedules(long pageIndex = 0, int pageSize = 12, DateTime? auctionDateFrom = null, DateTime? auctionDateTo = null, int[] kpknlID = null, int selectAll = -1)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();

            if (kpknlID == null)
            {
                kpknlID = new int[1];
            }

            IList<dynamic> schedules = context.QuerySql<dynamic>(
                @"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY A.ScheduleDate ASC, A.ScheduleTime ASC) AS NUMBER,
                A.AuctionScheduleID, A.ScheduleDate, A.ScheduleTime, 
                CASE WHEN A.Timezone = 1 THEN 'WIB' WHEN A.Timezone = 2 THEN 'WIT' WHEN A.Timezone = 3 THEN 'WITA' END AS Timezone, A.AuctionAddress, A.Phone1, A.Phone2,
                K.Name
                FROM AuctionSchedule A INNER JOIN KPKNL K ON K.KPKNLID = A.KPKNLID
                WHERE A.AuctionScheduleID IN (SELECT DISTINCT AuctionScheduleID FROM Asset WHERE CurrentStatusID = 4) AND A.IsActive = 1 AND K.IsActive = 1 AND DATEADD(dd, 0, DATEDIFF(dd, 0, A.ScheduleDate)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))
                AND (@AuctionDateFrom IS NULL OR @AuctionDateTo IS NULL OR DATEADD(dd, 0, DATEDIFF(dd, 0, A.ScheduleDate)) BETWEEN DATEADD(dd, 0, DATEDIFF(dd, 0, @AuctionDateFrom)) AND DATEADD(dd, 0, DATEDIFF(dd, 0, @AuctionDateTo)))
                AND (@SelectAll = -1 OR A.KPKNLID IN (@KPKNLID))
                ) TBL
                WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , new
                {
                    PageIndex = pageIndex * pageSize,
                    PageSize = pageSize,
                    AuctionDateFrom = auctionDateFrom,
                    AuctionDateTo = auctionDateTo,
                    SelectAll = selectAll,
                    KPKNLID = kpknlID
                });
            result.ReturnData = schedules;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        public static FacadeResult<byte[]> DownloadAllActiveCurrentAuctionSchedulesAndAssetsAsByteArray(DateTime? auctionDateFrom = null, DateTime? auctionDateTo = null, int[] kpknlID = null, int selectAll = -1)
        {
            FacadeResult<byte[]> result = new FacadeResult<byte[]>();
            MemoryStream stream = new MemoryStream();
            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = String.Format("Jadwal Lelang {0}.csv", DateTime.Today.ToString("dd MMM yyyy")),
                // always prompt the user for downloading, set to true if you want 
                // the browser to try to show the file inline
                Inline = false
            };
            StringBuilder sb = new StringBuilder();
            StreamWriter sw = new StreamWriter(stream, Encoding.UTF8);
            sb.Append(@"""No."",""Tanggal Lelang"",""Waktu Lelang"",""KPKNL"",""Alamat Pelaksanaan Lelang"",""Alamat & Telp KPKNL"",""Balai Lelang"",""Alamat & Telp Balai Lelang"",""Kode Agunan"",""Kategori"",""Propinsi"",""Kota"",""Alamat"",""Dokumen"",""Limit Lelang"",""Informasi Agunan"",""Pengelola"",""Alamat Pengelola""");
            try
            {
                var downloadResults = AssetFacade.DownloadAllActiveCurrentAuctionSchedulesAndAssets(auctionDateFrom, auctionDateTo, kpknlID, selectAll);
                if (downloadResults != null && downloadResults.IsSuccess && downloadResults.ReturnData.Count > 0)
                {
                    foreach (var download in downloadResults.ReturnData)
                    {
                        sb.AppendLine();
                        sb.Append(String.Format(@"""{0}"",""{1}"",""{2}"",""{3}"",""{4}"",""{5}", download.Number, ((DateTime)download.ScheduleDate).ToString("dd-MMM-yyyy"), String.Format("{0} {1}", ((DateTime)download.ScheduleTime).ToString("HH:mm"), download.Timezone), download.KPKNLName, download.AuctionAddress, download.KPKNLAddress));
                        if (!String.IsNullOrWhiteSpace(download.KPKNLPhone1) && !String.IsNullOrWhiteSpace(download.KPKNLPhone2))
                        {
                            sb.AppendLine();
                            sb.Append(download.KPKNLPhone1);
                            sb.AppendLine();
                            sb.Append(download.KPKNLPhone2 + @""",");
                        }
                        else if (!String.IsNullOrWhiteSpace(download.KPKNLPhone1))
                        {
                            sb.AppendLine();
                            sb.Append(download.KPKNLPhone1 + @""",");
                        }
                        else if (!String.IsNullOrWhiteSpace(download.KPKNLPhone2))
                        {
                            sb.AppendLine();
                            sb.Append(download.KPKNLPhone2 + @""",");
                        }
                        else
                        {
                            sb.Append(@""",");
                        }
                        sb.Append(String.Format(@"""{0}"",""{1}", download.AuctionHall, download.AuctionHallAddress));
                        if (!String.IsNullOrWhiteSpace(download.AuctionHallPhone1) && !String.IsNullOrWhiteSpace(download.AuctionHallPhone2))
                        {
                            sb.AppendLine();
                            sb.Append(download.AuctionHallPhone1);
                            sb.AppendLine();
                            sb.Append(download.AuctionHallPhone2 + @""",");
                        }
                        else if (!String.IsNullOrWhiteSpace(download.AuctionHallPhone1))
                        {
                            sb.AppendLine();
                            sb.Append(download.AuctionHallPhone1 + @""",");
                        }
                        else if (!String.IsNullOrWhiteSpace(download.AuctionHallPhone2))
                        {
                            sb.AppendLine();
                            sb.Append(download.AuctionHallPhone2 + @""",");
                        }
                        else
                        {
                            sb.Append(@""",");
                        }
                        sb.Append(String.Format(@"""{0}"",""{1}"",""{2}"",""{3}"",""{4}"",""{5}"",""{6}"",""", download.AssetCode, download.CategoryName, download.ProvinceName, download.CityName, download.Address, download.DocumentTypeName, download.NewPrice.ToString("n2"), ""));
                        if (!String.IsNullOrWhiteSpace(download.Information))
                        {
                            int idx = 0;
                            var informations = download.Information.Split('|');
                            foreach (var info in informations)
                            {
                                if (idx == 0)
                                {
                                    sb.AppendFormat(@"{0}", info);
                                }
                                else
                                {
                                    sb.AppendLine();
                                    sb.AppendFormat(@"{0}", info);
                                }
                                idx++;
                            }
                        }
                        sb.Append(String.Format(@""",""{0}"",""{1}""", download.Branch, download.BranchAddress));
                    }
                }
                sw.WriteLine(sb.ToString());
                sw.Flush();
                stream.Seek(0, SeekOrigin.Begin);
                var buf = stream.ToArray();
                result.ReturnData = buf;
                result.IsSuccess = true;
                result.ErrorMessage = String.Empty;
                return result;
            }
            catch
            {
                sw.WriteLine(sb.ToString());
                sw.Flush();
                stream.Seek(0, SeekOrigin.Begin);
                var buf = stream.ToArray();
                result.ReturnData = buf;
                result.IsSuccess = true;
                result.ErrorMessage = String.Empty;
                return result;
            }
            finally
            {
                try
                {
                    sw.Close();
                    sw.Dispose();
                    stream.Close();
                    stream.Dispose();
                }
                catch
                {
                }
            }
        }

        /// <summary>
        /// Download all active current auction schedules including all the assets (>= today)
        /// </summary>
        /// <returns></returns>
        public static FacadeResult<IList<dynamic>> DownloadAllActiveCurrentAuctionSchedulesAndAssets(DateTime? auctionDateFrom = null, DateTime? auctionDateTo = null, int[] kpknlID = null, int selectAll = -1)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();

            if (kpknlID == null)
            {
                kpknlID = new int[1];
            }

            IList<dynamic> schedules = context.QuerySql<dynamic>(
                @"SELECT ROW_NUMBER() OVER(ORDER BY A.AssetID ASC) AS Number,
                A.AssetID, AssetCode, A.CategoryID, C.Name AS CategoryName, A.Address, A.ProvinceID, P.Name AS ProvinceName, A.CityID,
                CT.Name AS CityName, ISNULL(OldPrice, 0) AS OldPrice, ISNULL(NewPrice, 0) AS NewPrice, Photo1, A.AuctionScheduleID, S.ScheduleDate, S.ScheduleTime, 
                CASE WHEN S.Timezone = 1 THEN 'WIB' WHEN S.Timezone = 2 THEN 'WIT' WHEN S.Timezone = 3 THEN 'WITA' END AS Timezone,
                A.DateTimeCreated, ISNULL(ODT.Name, '') AS DocumentTypeName, K.Name AS KPKNLName, ISNULL(K.Address, '') AS KPKNLAddress, ISNULL(K.Phone1, '') AS KPKNLPhone1, ISNULL(K.Phone2, '') AS KPKNLPhone2, 
                A.CurrentStatusID, ISNULL(A.IsEmailAuction, 0) AS IsEmailAuction,
                AH.Name AS AuctionHall, ISNULL(AH.Address, '') AS AuctionHallAddress, ISNULL(AH.Phone1, '') AS AuctionHallPhone1, ISNULL(AH.Phone2, '') AS AuctionHallPhone2,
                B.Name AS Branch, ISNULL(B.Address, '') AS BranchAddress, S.AuctionAddress, DF.NameValues AS Information
                FROM AuctionSchedule S
                INNER JOIN Asset A ON S.AuctionScheduleID = A.AuctionScheduleID
                INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                INNER JOIN KPKNL K ON S.KPKNLID = K.KPKNLID
                LEFT JOIN OwnershipDocumentType ODT ON ODT.OwnershipDocumentTypeID = A.OwnershipDocumentTypeID
                LEFT JOIN AuctionHall AH ON AH.AuctionHallID = A.AuctionHallID
                LEFT JOIN Branch B ON B.BranchID = A.BranchID
                LEFT JOIN
		                (
		                SELECT 
                  [AssetID],
                  STUFF((
                    SELECT '|' + [FieldName] + ': ' + [Value]
                    FROM AssetCategoryTemplateValue 
                    WHERE (AssetID = Results.AssetID) 
                    FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
                  ,1,1,'') AS NameValues
                FROM AssetCategoryTemplateValue Results
                GROUP BY AssetID
                ) DF ON A.AssetID = DF.AssetID
                WHERE A.IsActive = 1 AND A.CurrentStatusID = 4
                AND DATEADD(dd, 0, DATEDIFF(dd, 0, S.ScheduleDate)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))
                AND (@AuctionDateFrom IS NULL OR @AuctionDateTo IS NULL OR DATEADD(dd, 0, DATEDIFF(dd, 0, S.ScheduleDate)) BETWEEN DATEADD(dd, 0, DATEDIFF(dd, 0, @AuctionDateFrom)) AND DATEADD(dd, 0, DATEDIFF(dd, 0, @AuctionDateTo)))
                AND (@SelectAll = -1 OR A.KPKNLID IN (@KPKNLID))"
                , new
                {
                    AuctionDateFrom = auctionDateFrom,
                    AuctionDateTo = auctionDateTo,
                    SelectAll = selectAll,
                    KPKNLID = kpknlID
                });
            result.ReturnData = schedules;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all active current auction schedules (>= today)
        /// </summary>
        /// <returns></returns>
        /*public static FacadeResult<IList<dynamic>> GetAllActiveCurrentAuctionSchedulesWithoutPagination(DateTime? auctionDateFrom = null, DateTime? auctionDateTo = null, int kpknlID = -1)
        {        
            //Fix 28 Apr 2016, for now just return all the schedules because the mobile pagination is not working
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();

            IList<dynamic> schedules = context.QuerySql<dynamic>(
                @"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY A.ScheduleDate ASC, A.ScheduleTime ASC) AS NUMBER,
                A.AuctionScheduleID, A.ScheduleDate, A.ScheduleTime, 
                CASE WHEN A.Timezone = 1 THEN 'WIB' WHEN A.Timezone = 2 THEN 'WIT' WHEN A.Timezone = 3 THEN 'WITA' END AS Timezone, A.AuctionAddress, A.Phone1, A.Phone2,
                K.Name
                FROM AuctionSchedule A INNER JOIN KPKNL K ON K.KPKNLID = A.KPKNLID
                WHERE A.AuctionScheduleID IN (SELECT DISTINCT AuctionScheduleID FROM Asset WHERE CurrentStatusID = 4) AND A.IsActive = 1 AND K.IsActive = 1 AND DATEADD(dd, 0, DATEDIFF(dd, 0, A.ScheduleDate)) >=  DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))
                AND (@AuctionDateFrom IS NULL OR @AuctionDateTo IS NULL OR DATEADD(dd, 0, DATEDIFF(dd, 0, A.ScheduleDate)) BETWEEN DATEADD(dd, 0, DATEDIFF(dd, 0, @AuctionDateFrom)) AND DATEADD(dd, 0, DATEDIFF(dd, 0, @AuctionDateTo)))
                AND (@KPKNLID = -1 OR A.KPKNLID = @KPKNLID)
                ) TBL"
                , new
                {
                    AuctionDateFrom = auctionDateFrom,
                    AuctionDateTo = auctionDateTo,
                    KPKNLID = kpknlID
                });
            result.ReturnData = schedules;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }*/

        /// <summary>
        /// Get total count of active current auction schedules (>= today)
        /// </summary>
        /// <returns></returns>
        public static FacadeResult<long> GetCurrentAuctionSchedulesCount(DateTime? auctionDateFrom = null, DateTime? auctionDateTo = null, int[] kpknlID = null, int selectAll = -1)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            if (kpknlID == null)
            {
                kpknlID = new int[1];
            }

            long count = context.ExecuteScalarSql<long>(
                @"SELECT COUNT_BIG(A.AuctionScheduleID)
                FROM AuctionSchedule A INNER JOIN KPKNL K ON K.KPKNLID = A.KPKNLID
                WHERE A.AuctionScheduleID IN (SELECT DISTINCT AuctionScheduleID FROM Asset WHERE CurrentStatusID = 4) AND A.IsActive = 1 AND K.IsActive = 1 AND DATEADD(dd, 0, DATEDIFF(dd, 0, ScheduleDate)) >=  DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))
                AND (@AuctionDateFrom IS NULL OR @AuctionDateTo IS NULL OR DATEADD(dd, 0, DATEDIFF(dd, 0, A.ScheduleDate)) BETWEEN DATEADD(dd, 0, DATEDIFF(dd, 0, @AuctionDateFrom)) AND DATEADD(dd, 0, DATEDIFF(dd, 0, @AuctionDateTo)))
                AND (@SelectAll = -1 OR A.KPKNLID IN (@KPKNLID))"
                , new
                {
                    AuctionDateFrom = auctionDateFrom,
                    AuctionDateTo = auctionDateTo,
                    SelectAll = selectAll,
                    KPKNLID = kpknlID
                });
            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all assets by auction schedule id
        /// </summary>
        /// <param name="auctionScheduleID"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static FacadeResult<IList<dynamic>> GetAllActiveAssetsByAuctionScheduleID(long auctionScheduleID, long pageIndex = 0, int pageSize = 12, long? memberID = null)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            int sortIndex = 0;
            String[] sortColumnNames = new String[] { "A.DateTimeCreated", "NewPrice", "NewPrice", "ScheduleDate" };
            String[] sortColumnDirections = new String[] { "DESC", "DESC", "ASC", "ASC" };

            IList<dynamic> assets = context.QuerySql<dynamic>(
                /*String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                A.AssetID, AssetCode, A.CategoryID, C.Name AS CategoryName, A.Address, A.ProvinceID, P.Name AS ProvinceName, A.CityID,
                CT.Name AS CityName, ISNULL(OldPrice, 0) AS OldPrice, ISNULL(NewPrice, 0) AS NewPrice, Photo1, A.AuctionScheduleID, S.ScheduleDate, S.ScheduleTime, 
                CASE WHEN S.Timezone = 1 THEN 'WIB' WHEN S.Timezone = 2 THEN 'WIT' WHEN S.Timezone = 3 THEN 'WITA' END AS Timezone,
                A.DateTimeCreated, ISNULL(ODT.Name, '') AS DocumentTypeName, K.Name AS KPKNLName
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                LEFT JOIN AuctionSchedule S ON S.AuctionScheduleID = A.AuctionScheduleID
                LEFT JOIN AssetStatusHistory H ON H.AssetID = A.AssetID AND H.NewAssetStatusID IN (5, 7)
                LEFT JOIN OwnershipDocumentType ODT ON ODT.OwnershipDocumentTypeID = A.OwnershipDocumentTypeID
                LEFT JOIN KPKNL K ON A.KPKNLID = K.KPKNLID
                WHERE A.IsActive = 1 AND A.CurrentStatusID IN (4, 5, 7) 
                AND (H.DateTimeCreated IS NULL OR (H.DateTimeCreated IS NOT NULL 
                AND (DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(day, @VisibilityDaysPeriodForPaidAndSold, H.DateTimeCreated))) > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))))                
                AND A.AuctionScheduleID = @AuctionScheduleID) TBL
                WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"*/
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                A.AssetID, AssetCode, A.CategoryID, C.Name AS CategoryName, A.Address, A.ProvinceID, P.Name AS ProvinceName, A.CityID,
                CT.Name AS CityName, ISNULL(OldPrice, 0) AS OldPrice, ISNULL(NewPrice, 0) AS NewPrice, Photo1, A.AuctionScheduleID, S.ScheduleDate, S.ScheduleTime, 
                CASE WHEN S.Timezone = 1 THEN 'WIB' WHEN S.Timezone = 2 THEN 'WIT' WHEN S.Timezone = 3 THEN 'WITA' END AS Timezone,
                A.DateTimeCreated, ISNULL(ODT.Name, '') AS DocumentTypeName, K.Name AS KPKNLName, A.CurrentStatusID, ISNULL(A.IsEmailAuction, 0) AS IsEmailAuction,
                CAST(CASE WHEN MF.AssetID IS NOT NULL THEN 1 ELSE 0 END AS BIT) AS IsFavorite
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                LEFT JOIN AuctionSchedule S ON S.AuctionScheduleID = A.AuctionScheduleID
                LEFT JOIN OwnershipDocumentType ODT ON ODT.OwnershipDocumentTypeID = A.OwnershipDocumentTypeID
                LEFT JOIN KPKNL K ON A.KPKNLID = K.KPKNLID
                LEFT JOIN 
                (
                SELECT DISTINCT AssetID FROM MemberFavorite WHERE @MemberID IS NOT NULL AND MemberID = @MemberID AND IsActive = 1 AND IsFavorite = 1
                ) MF ON MF.AssetID = A.AssetID
                WHERE A.IsActive = 1 AND A.CurrentStatusID = 4                               
                AND A.AuctionScheduleID = @AuctionScheduleID) TBL
                WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortColumnDirections[sortIndex])
                , new
                {
                    //VisibilityDaysPeriodForPaidAndSold = int.Parse(AdministrationFacade.GetActiveSystemSettingByID("VisibilityDaysPeriodForPaidAndSold").ReturnData.Value),
                    PageIndex = pageIndex * pageSize,
                    PageSize = pageSize,
                    AuctionScheduleID = auctionScheduleID,
                    MemberID = memberID
                });
            result.ReturnData = assets;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all assets by auction schedule id count
        /// </summary>
        /// <param name="auctionScheduleID"></param>
        /// <returns></returns>
        public static FacadeResult<long> GetAllActiveAssetsByAuctionScheduleIDCount(long auctionScheduleID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<dynamic>(
                /*@"SELECT COUNT_BIG(A.AssetID)
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                LEFT JOIN AuctionSchedule S ON S.AuctionScheduleID = A.AuctionScheduleID
                LEFT JOIN AssetStatusHistory H ON H.AssetID = A.AssetID AND H.NewAssetStatusID IN (5, 7)
                LEFT JOIN OwnershipDocumentType ODT ON ODT.OwnershipDocumentTypeID = A.OwnershipDocumentTypeID
                LEFT JOIN KPKNL K ON A.KPKNLID = K.KPKNLID
                WHERE A.IsActive = 1 AND A.CurrentStatusID IN (4, 5, 7) 
                AND (H.DateTimeCreated IS NULL OR (H.DateTimeCreated IS NOT NULL 
                AND (DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(day, @VisibilityDaysPeriodForPaidAndSold, H.DateTimeCreated))) > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))))                
                AND A.AuctionScheduleID = @AuctionScheduleID"*/
                @"SELECT COUNT_BIG(A.AssetID)
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                LEFT JOIN AuctionSchedule S ON S.AuctionScheduleID = A.AuctionScheduleID
                LEFT JOIN OwnershipDocumentType ODT ON ODT.OwnershipDocumentTypeID = A.OwnershipDocumentTypeID
                LEFT JOIN KPKNL K ON A.KPKNLID = K.KPKNLID
                WHERE A.IsActive = 1 AND A.CurrentStatusID = 4                                
                AND A.AuctionScheduleID = @AuctionScheduleID"
                , new
                {
                    //VisibilityDaysPeriodForPaidAndSold = int.Parse(AdministrationFacade.GetActiveSystemSettingByID("VisibilityDaysPeriodForPaidAndSold").ReturnData.Value),
                    AuctionScheduleID = auctionScheduleID
                });
            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all favorite assets by member id
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static FacadeResult<IList<dynamic>> GetAllActiveFavoriteAssetsByMemberID(long memberID, long pageIndex = 0, int pageSize = 12)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            int sortIndex = 0;
            String[] sortColumnNames = new String[] { "A.DateTimeCreated" };
            String[] sortColumnDirections = new String[] { "DESC" };

            IList<dynamic> assets = context.QuerySql<dynamic>(
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                A.AssetID, AssetCode, A.CategoryID, C.Name AS CategoryName, A.Address, A.ProvinceID, P.Name AS ProvinceName, A.CityID,
                CT.Name AS CityName, ISNULL(OldPrice, 0) AS OldPrice, CASE WHEN A.Type = 1 THEN ISNULL(Price, 0) ELSE ISNULL(NewPrice, 0) END AS NewPrice, Photo1, A.AuctionScheduleID, S.ScheduleDate, S.ScheduleTime, 
                CASE WHEN S.Timezone = 1 THEN 'WIB' WHEN S.Timezone = 2 THEN 'WIT' WHEN S.Timezone = 3 THEN 'WITA' END AS Timezone,
                A.DateTimeCreated, ISNULL(ODT.Name, '') AS DocumentTypeName, K.Name AS KPKNLName, A.CurrentStatusID, ISNULL(A.IsEmailAuction, 0) AS IsEmailAuction,
                CAST(CASE WHEN MF.AssetID IS NOT NULL THEN 1 ELSE 0 END AS BIT) AS IsFavorite, ISNULL(A.Price, 0) AS Price, A.Type
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                LEFT JOIN AuctionSchedule S ON S.AuctionScheduleID = A.AuctionScheduleID
                LEFT JOIN OwnershipDocumentType ODT ON ODT.OwnershipDocumentTypeID = A.OwnershipDocumentTypeID
                LEFT JOIN KPKNL K ON A.KPKNLID = K.KPKNLID
                INNER JOIN 
                (
                SELECT DISTINCT AssetID FROM MemberFavorite WHERE @MemberID IS NOT NULL AND MemberID = @MemberID AND IsActive = 1 AND IsFavorite = 1
                ) MF ON MF.AssetID = A.AssetID
                WHERE A.IsActive = 1 AND A.CurrentStatusID = 4) TBL
                WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortColumnDirections[sortIndex])
                , new
                {
                    PageIndex = pageIndex * pageSize,
                    PageSize = pageSize,
                    MemberID = memberID
                });
            result.ReturnData = assets;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all favorite assets by member id count
        /// </summary>
        /// <param name="memberID"></param>
        /// <returns></returns>
        public static FacadeResult<long> GetAllActiveFavoriteAssetsByMemberIDCount(long memberID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();

            long count = context.ExecuteScalarSql<dynamic>(
                @"SELECT COUNT_BIG(A.AssetID)
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                LEFT JOIN AuctionSchedule S ON S.AuctionScheduleID = A.AuctionScheduleID
                LEFT JOIN OwnershipDocumentType ODT ON ODT.OwnershipDocumentTypeID = A.OwnershipDocumentTypeID
                LEFT JOIN KPKNL K ON A.KPKNLID = K.KPKNLID
                INNER JOIN 
                (
                SELECT DISTINCT AssetID FROM MemberFavorite WHERE @MemberID IS NOT NULL AND MemberID = @MemberID AND IsActive = 1 AND IsFavorite = 1
                ) MF ON MF.AssetID = A.AssetID
                WHERE A.IsActive = 1 AND A.CurrentStatusID = 4"
                , new
                {
                    MemberID = memberID
                });
            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get all assets by city id
        /// </summary>
        /// <param name="cityID"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static FacadeResult<IList<dynamic>> GetAllActiveAssetsByCityID(int cityID, long pageIndex = 0, int pageSize = 6, long? memberID = null)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            int sortIndex = 0;
            String[] sortColumnNames = new String[] { "A.DateTimeCreated", "NewPrice", "NewPrice", "ScheduleDate" };
            String[] sortColumnDirections = new String[] { "DESC", "DESC", "ASC", "ASC" };

            IList<dynamic> assets = context.QuerySql<dynamic>(
                /*String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                A.AssetID, AssetCode, A.CategoryID, C.Name AS CategoryName, A.Address, A.ProvinceID, P.Name AS ProvinceName, A.CityID,
                CT.Name AS CityName, ISNULL(OldPrice, 0) AS OldPrice, ISNULL(NewPrice, 0) AS NewPrice, Photo1, A.AuctionScheduleID, S.ScheduleDate, S.ScheduleTime, 
                CASE WHEN S.Timezone = 1 THEN 'WIB' WHEN S.Timezone = 2 THEN 'WIT' WHEN S.Timezone = 3 THEN 'WITA' END AS Timezone,
                A.DateTimeCreated, ISNULL(ODT.Name, '') AS DocumentTypeName, K.Name AS KPKNLName
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                LEFT JOIN AuctionSchedule S ON S.AuctionScheduleID = A.AuctionScheduleID
                LEFT JOIN AssetStatusHistory H ON H.AssetID = A.AssetID AND H.NewAssetStatusID IN (5, 7)
                LEFT JOIN OwnershipDocumentType ODT ON ODT.OwnershipDocumentTypeID = A.OwnershipDocumentTypeID
                LEFT JOIN KPKNL K ON A.KPKNLID = K.KPKNLID
                WHERE A.IsActive = 1 AND A.CurrentStatusID IN (4, 5, 7) 
                AND (H.DateTimeCreated IS NULL OR (H.DateTimeCreated IS NOT NULL 
                AND (DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(day, @VisibilityDaysPeriodForPaidAndSold, H.DateTimeCreated))) > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))))                
                AND A.CityID = @CityID
                AND (S.ScheduleDate IS NULL OR 
                (S.ScheduleDate IS NOT NULL AND (DATEADD(dd, 0, DATEDIFF(dd, 0, S.ScheduleDate)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))))
                ) TBL
                WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"*/
                String.Format(@"SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) AS NUMBER,
                A.AssetID, AssetCode, A.CategoryID, C.Name AS CategoryName, A.Address, A.ProvinceID, P.Name AS ProvinceName, A.CityID,
                CT.Name AS CityName, ISNULL(OldPrice, 0) AS OldPrice, CASE WHEN A.Type = 1 THEN ISNULL(Price, 0) ELSE ISNULL(NewPrice, 0) END AS NewPrice, Photo1, A.AuctionScheduleID, S.ScheduleDate, S.ScheduleTime, 
                CASE WHEN S.Timezone = 1 THEN 'WIB' WHEN S.Timezone = 2 THEN 'WIT' WHEN S.Timezone = 3 THEN 'WITA' END AS Timezone,
                A.DateTimeCreated, ISNULL(ODT.Name, '') AS DocumentTypeName, K.Name AS KPKNLName, A.CurrentStatusID, ISNULL(A.IsEmailAuction, 0) AS IsEmailAuction,
                CAST(CASE WHEN MF.AssetID IS NOT NULL THEN 1 ELSE 0 END AS BIT) AS IsFavorite,
                A.Type, A.Price
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                LEFT JOIN AuctionSchedule S ON S.AuctionScheduleID = A.AuctionScheduleID
                LEFT JOIN OwnershipDocumentType ODT ON ODT.OwnershipDocumentTypeID = A.OwnershipDocumentTypeID
                LEFT JOIN KPKNL K ON A.KPKNLID = K.KPKNLID
                LEFT JOIN 
                (
                SELECT DISTINCT AssetID FROM MemberFavorite WHERE @MemberID IS NOT NULL AND MemberID = @MemberID AND IsActive = 1 AND IsFavorite = 1
                ) MF ON MF.AssetID = A.AssetID
                WHERE A.IsActive = 1 AND A.CurrentStatusID = 4               
                AND A.CityID = @CityID
                ) TBL
                WHERE
                NUMBER BETWEEN (@PageIndex + 1) AND (@PageIndex + @PageSize)"
                , sortColumnNames[sortIndex], sortColumnDirections[sortIndex])
                , new
                {
                    //VisibilityDaysPeriodForPaidAndSold = int.Parse(context.GetActiveSystemSettingByID("VisibilityDaysPeriodForPaidAndSold").Value),
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    CityID = cityID,
                    MemberID = memberID
                });
            result.ReturnData = assets;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        public static FacadeResult<bool> UpdateAssetCategoryCount(int categoryID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<bool> result = new FacadeResult<bool>();

            context.ExecuteSql(
                /*@"
                UPDATE Category SET CurrentAssetTotal =
                (SELECT COUNT_BIG(A.AssetID)
                FROM Asset A
                LEFT JOIN AssetStatusHistory H ON H.AssetID = A.AssetID AND H.NewAssetStatusID IN (5, 7)
                LEFT JOIN AuctionSchedule S ON A.AuctionScheduleID = S.AuctionScheduleID
                WHERE A.IsActive = 1 AND A.CurrentStatusID IN (4, 5, 7) AND A.CategoryID = @CategoryID
                AND (H.DateTimeCreated IS NULL OR (H.DateTimeCreated IS NOT NULL 
                AND (DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(day, @VisibilityDaysPeriodForPaidAndSold, H.DateTimeCreated))) >= DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))))
                AND (S.ScheduleDate IS NULL OR 
                (S.ScheduleDate IS NOT NULL AND (DATEADD(dd, 0, DATEDIFF(dd, 0, S.ScheduleDate)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))))))
                WHERE CategoryID = @CategoryID"*/
                @"
                UPDATE Category SET CurrentAssetTotal =
                (SELECT COUNT_BIG(A.AssetID)
                FROM Asset A
                LEFT JOIN AssetStatusHistory H ON H.AssetID = A.AssetID AND H.NewAssetStatusID IN (5, 7)
                LEFT JOIN AuctionSchedule S ON A.AuctionScheduleID = S.AuctionScheduleID
                WHERE A.IsActive = 1 AND A.CurrentStatusID IN (4, 5, 7) AND A.CategoryID = @CategoryID
                AND (H.DateTimeCreated IS NULL OR (H.DateTimeCreated IS NOT NULL 
                AND (DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(day, @VisibilityDaysPeriodForPaidAndSold, H.DateTimeCreated))) > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))))
                )
                WHERE CategoryID = @CategoryID"
                , new
                {
                    VisibilityDaysPeriodForPaidAndSold = int.Parse(context.GetActiveSystemSettingByID("VisibilityDaysPeriodForPaidAndSold").Value),
                    CategoryID = categoryID
                });
            result.ReturnData = true;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get total count for inputter task list
        /// </summary>
        /// <param name="currentUserID"></param>
        /// <returns></returns>
        public static FacadeResult<IList<dynamic>> GetTaskListCountForInputter(int currentUserID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();
            int branchID;
            branchID = context.GetActiveUserByID(currentUserID).BranchID.Value;

            IList<dynamic> countPerTypes = context.QuerySql<dynamic>(
                @"
                SELECT 
	                COUNT(AssetChangesID) Total, 
	                CASE WHEN IsNew = 1 THEN 'Baru' WHEN IsUpdated = 1 THEN 'Ubah' WHEN IsDeleted = 1 THEN 'Hapus' END AS ApprovalType
                FROM 
                AssetChanges AC
                JOIN Asset A ON AC.AssetID = A.AssetID
                WHERE CurrentApprovalStatus = 2 AND (@BranchID = -1 OR BranchID = @BranchID) AND A.IsActive = 1 AND AC.IsActive = 1
                GROUP BY 
	                (CASE WHEN IsNew = 1 THEN 'Baru' WHEN IsUpdated = 1 THEN 'Ubah' WHEN IsDeleted = 1 THEN 'Hapus' END)
                "
                , new
                {
                    BranchID = branchID
                });

            result.ReturnData = countPerTypes;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get total count for updating new price
        /// </summary>
        /// <param name="currentUserID"></param>
        /// <returns></returns>
        public static FacadeResult<long> GetTaskListCountForUpdatingNewPrice(int currentUserID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<long> result = new FacadeResult<long>();
            int branchID;
            branchID = context.GetActiveUserByID(currentUserID).BranchID.Value;

            long count = context.ExecuteScalarSql<long>(
                @"
                SELECT CAST(COUNT(A.AssetID) AS BIGINT) FROM Asset A
                JOIN AuctionSchedule S ON A.AuctionScheduleID = S.AuctionScheduleID
                WHERE DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) > DATEADD(dd, 0, DATEDIFF(dd, 0, S.ScheduleDate))
                AND A.CurrentStatusID IN (4) AND (A.NewPrice = 0 OR A.NewPrice IS NULL)
                AND (@BranchID = -1 OR BranchID = @BranchID) AND A.IsActive = 1 AND S.IsActive = 1
                "
                , new
                {
                    BranchID = branchID
                });

            result.ReturnData = count;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }

        /// <summary>
        /// Get total count for inputter task list
        /// </summary>
        /// <param name="currentUserID"></param>
        /// <returns></returns>
        public static FacadeResult<IList<dynamic>> GetTaskListCountForApprover()
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<dynamic>> result = new FacadeResult<IList<dynamic>>();

            IList<dynamic> countPerTypes = context.QuerySql<dynamic>(
                @"
                SELECT 
	                COUNT(AssetChangesID) Total, 
	                CASE WHEN IsNew = 1 THEN 'Baru' WHEN IsUpdated = 1 THEN 'Ubah' WHEN IsDeleted = 1 THEN 'Hapus' END AS ApprovalType
                FROM 
                AssetChanges AC
                JOIN Asset A ON AC.AssetID = A.AssetID
                WHERE CurrentApprovalStatus = 0 AND A.IsActive = 1 AND AC.IsActive = 1 AND A.CurrentStatusID > 1
                GROUP BY 
	                (CASE WHEN IsNew = 1 THEN 'Baru' WHEN IsUpdated = 1 THEN 'Ubah' WHEN IsDeleted = 1 THEN 'Hapus' END)
                ");

            result.ReturnData = countPerTypes;
            result.IsSuccess = true;
            result.ErrorMessage = String.Empty;

            return result;
        }        
    }
}
