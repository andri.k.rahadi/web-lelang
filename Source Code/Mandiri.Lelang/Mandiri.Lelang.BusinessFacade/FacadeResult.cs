﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.BusinessFacade
{
    /// <summary>
    /// The return value for facade method
    /// </summary>
    /// <typeparam name="RETURNDATA">The return data type</typeparam>
    public class FacadeResult<RETURNDATA>
    {
        /// <summary>
        /// The return data
        /// </summary>
        public RETURNDATA ReturnData { get; set; }
        /// <summary>
        /// The success flag for the method calling
        /// </summary>
        public bool IsSuccess { get; set; }
        /// <summary>
        /// The error message (if any)
        /// </summary>
        public String ErrorMessage { get; set; }
    }
}
