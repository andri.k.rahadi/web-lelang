﻿using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.DataRepository;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.Helper.Enum;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.BusinessFacade
{
    public class UserFacade : BaseFacade
    {
        #region Get

        #region User

        /// <summary>
        /// Get active user by user id
        /// </summary>
        /// <param name="userID">The user id</param>
        /// <returns>Facade result containing the user object</returns>
        public static FacadeResult<User> GetActiveUserByUserID(int userID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<User> result = new FacadeResult<User>();

            User user = context.GetActiveUserByID(userID);

            result.ReturnData = user;
            result.IsSuccess = user == null ? false : true;
            result.ErrorMessage = user == null ? String.Format(TextResources.NotFound_ErrorMessage, "User") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get active user by user id
        /// </summary>
        /// <param name="userID">The user id</param>
        /// <returns>Facade result containing the user object</returns>
        public static FacadeResult<User> GetActiveUserByUserIDAndSessionID(int userID, String sessionID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<User> result = new FacadeResult<User>();

            User user = context.GetActiveUserByIDAndSessionID(userID, sessionID);

            result.ReturnData = user;
            result.IsSuccess = user == null ? false : true;
            result.ErrorMessage = user == null ? String.Format(TextResources.NotFound_ErrorMessage, "User") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get active user by username
        /// </summary>
        /// <param name="username">The username</param>
        /// <returns>Facade result containing the user object</returns>
        public static FacadeResult<User> GetActiveUserByUsername(String username)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<User> result = new FacadeResult<User>();

            User user = context.GetActiveUserByUsername(username);

            result.ReturnData = user;
            result.IsSuccess = user == null ? false : true;
            result.ErrorMessage = user == null ? String.Format(TextResources.NotFound_ErrorMessage, "User") : String.Empty;

            return result;
        }

        /// <summary>
        /// Get All active user by Branch id
        /// </summary>
        /// <param name="userID">The Branch id</param>
        /// <returns>Facade result containing the user object</returns>
        public static FacadeResult<IList<User>> GetAllActiveUserByBranchID(int branchID)
        {
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            FacadeResult<IList<User>> result = new FacadeResult<IList<User>>();

            IList<User> user = context.GetAllActiveUserByBranchID(branchID);

            result.ReturnData = user;
            result.IsSuccess = user == null ? false : true;
            result.ErrorMessage = user == null ? String.Format(TextResources.NotFound_ErrorMessage, "Branch ID") : String.Empty;

            return result;
        }

        #endregion

        #region Authorization

        /// <summary>
        /// Check whether the user has access to menu or not
        /// </summary>
        /// <param name="siteMapID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static bool IsUserAllowedAccess(String siteMapID, int userID)
        {
            FacadeResult<IList<SiteMap>> siteMaps = MenuFacade.GetAllowedAccessMenusByUserID(userID);
            if (siteMaps.IsSuccess && siteMaps.ReturnData.Count > 0)
            {
                if (siteMaps.ReturnData.Count(m => m.SiteMapID == siteMapID) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check whether the user has delete access to menu or not
        /// </summary>
        /// <param name="siteMapID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static bool IsUserAllowedDelete(String siteMapID, int userID)
        {
            FacadeResult<IList<SiteMap>> siteMaps = MenuFacade.GetAllowedDeleteMenusByUserID(userID);
            if (siteMaps.IsSuccess && siteMaps.ReturnData.Count > 0)
            {
                if (siteMaps.ReturnData.Count(m => m.SiteMapID == siteMapID) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check whether the user has add access to menu or not
        /// </summary>
        /// <param name="siteMapID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static bool IsUserAllowedAdd(String siteMapID, int userID)
        {
            FacadeResult<IList<SiteMap>> siteMaps = MenuFacade.GetAllowedAddMenusByUserID(userID);
            if (siteMaps.IsSuccess && siteMaps.ReturnData.Count > 0)
            {
                if (siteMaps.ReturnData.Count(m => m.SiteMapID == siteMapID) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check whether the user has update access to menu or not
        /// </summary>
        /// <param name="siteMapID"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static bool IsUserAllowedUpdate(String siteMapID, int userID)
        {
            FacadeResult<IList<SiteMap>> siteMaps = MenuFacade.GetAllowedUpdateMenusByUserID(userID);
            if (siteMaps.IsSuccess && siteMaps.ReturnData.Count > 0)
            {
                if (siteMaps.ReturnData.Count(m => m.SiteMapID == siteMapID) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        #endregion

        #endregion

        #region Action

        #region Login Logout

        /// <summary>
        /// Do login
        /// </summary>
        /// <param name="username">The username</param>
        /// <param name="password">The password</param>
        /// <returns>Facade result containing the user object</returns>
        public static FacadeResult<User> DoLogin(String username, String password, String sessionID)
        {
            FacadeResult<User> result = new FacadeResult<User>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    User user = context.GetActiveUserByUsername(username);

                    if (user != null)
                    {
                        if (true)//SecurityHelper.Hash(password, user.PasswordSalt, HashingAlgorithm.SHA256) == user.HashPassword)
                        {
                            if (user.IsLocked.Value)
                            {
                                result.ReturnData = null;
                                result.IsSuccess = false;
                                result.ErrorMessage = TextResources.UserLocked_ErrorMessage;
                            }
                            else if (user.IsLoggedIn.HasValue && user.IsLoggedIn.Value
                                && (user.LastActivity.HasValue && DateTime.Now.Subtract(user.LastActivity.Value).TotalMinutes <= 15))
                            {
                                result.ReturnData = null;
                                result.IsSuccess = false;
                                result.ErrorMessage = TextResources.AlreadyLoggedIn_ErrorMessage;
                            }
                            else if (user.LastLoggedIn.HasValue && DateTime.Today.Subtract(
                                new DateTime(user.LastLoggedIn.Value.Year, user.LastLoggedIn.Value.Month, user.LastLoggedIn.Value.Day)).TotalDays >= 90)
                            {
                                user.IsLocked = true;
                                //user.IsLoggedIn = false;
                                context.UpdateUser(user);

                                userActivity.UserActivityTypeID = "LockUser";
                                userActivity.ReferenceID = user.UserID.ToString();
                                userActivity.CreatedBy = user.UserID;
                                context.InsertUserActivity(userActivity);

                                result.ReturnData = null;
                                result.IsSuccess = false;
                                result.ErrorMessage = TextResources.UserLocked_ErrorMessage;
                            }
                            else
                            {
                                user.IsLocked = false;
                                user.IsLoggedIn = true;
                                user.SessionID = sessionID;
                                user.FailedLoginAttempt = 0;
                                user.LastLoggedIn = DateTime.Now;
                                user.LastActivity = DateTime.Now;
                                context.UpdateUser(user);

                                userActivity.UserActivityTypeID = "Login";
                                userActivity.ReferenceID = user.UserID.ToString();
                                userActivity.CreatedBy = user.UserID;
                                context.InsertUserActivity(userActivity);

                                result.ReturnData = user;
                                result.IsSuccess = true;
                                result.ErrorMessage = String.Empty;
                            }
                        }
                        else
                        {
                            //user.IsLoggedIn = false;
                            user.FailedLoginAttempt++;
                            if (user.FailedLoginAttempt >= 3)
                            {
                                user.IsLocked = true;
                            }
                            context.UpdateUser(user);

                            userActivity.UserActivityTypeID = "LockUser";
                            userActivity.ReferenceID = user.UserID.ToString();
                            userActivity.CreatedBy = user.UserID;
                            context.InsertUserActivity(userActivity);

                            if (user.IsLocked.HasValue && user.IsLocked.Value)
                            {
                                result.ReturnData = null;
                                result.IsSuccess = false;
                                result.ErrorMessage = TextResources.UserLocked_ErrorMessage;
                            }
                            else
                            {
                                result.ReturnData = null;
                                result.IsSuccess = false;
                                result.ErrorMessage = TextResources.Login_InvalidErrorMessage;
                            }
                        }
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = TextResources.Login_InvalidErrorMessage;
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;

                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        /// <summary>
        /// Do logout
        /// </summary>
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <returns>Facade result containing the user id</returns>
        public static FacadeResult<int?> DoLogout(int currentUserID, String sessionID)
        {
            FacadeResult<int?> result = new FacadeResult<int?>();
            UserActivity userActivity = new UserActivity();
            User user;
            Exception logException = null;

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    userActivity.UserActivityTypeID = "Logout";
                    if (SecurityHelper.IsAuthenticated())
                    {
                        int userID = currentUserID;
                        user = UserFacade.GetActiveUserByUserIDAndSessionID(userID, sessionID).ReturnData;
                        if (user != null)
                        {
                            user.IsLoggedIn = false;
                            user.SessionID = String.Empty;
                            context.UpdateUser(user);
                        }
                        userActivity.CreatedBy = userID;
                        userActivity.ReferenceID = userID.ToString();
                        userActivity.CreatedBy = userID;

                        context.InsertUserActivity(userActivity);

                        result.ReturnData = userID;
                    }

                    result.IsSuccess = true;
                    result.ErrorMessage = String.Empty;

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;

                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        #endregion

        #region Change Password

        /// <summary>
        /// Change user password
        /// </summary>        
        /// <param name="currentUserID">The current user id that call this function</param>
        /// <param name="password">The user password</param>        
        /// <returns>Facade result containing the updated user</returns>
        public static FacadeResult<User> ChangePassword(int currentUserID, String password)
        {
            FacadeResult<User> result = new FacadeResult<User>();
            UserActivity userActivity = new UserActivity();
            Exception logException = null;
            PasswordHistory passwordHistory = new PasswordHistory();

            using (IMandiriLelangDataRepository context = MandiriLelangDataContext.OpenWithTransaction())
            {
                try
                {
                    User updatedUser = context.GetActiveUserByID(currentUserID);
                    if (updatedUser != null)
                    {
                        if (!String.IsNullOrWhiteSpace(password))
                        {
                            String hashPassword = SecurityHelper.Hash(password, updatedUser.PasswordSalt, HashingAlgorithm.SHA256);

                            if (context.GetExistingPasswordHistoryCountByUserIDAndHashPassword(updatedUser.UserID.Value, hashPassword) > 0)
                            {
                                result.ReturnData = null;
                                result.IsSuccess = false;
                                result.ErrorMessage = TextResources.PasswordComplexity_ErrorMessage;
                            }
                            else
                            {
                                //updatedUser.PasswordSalt = SecurityHelper.GenerateSaltValue();
                                updatedUser.HashPassword = hashPassword;
                                updatedUser.LastPasswordChanged = DateTime.Now;

                                context.UpdateUser(updatedUser);

                                //Delete the lowest password history if exceeds 4 (FIFO)
                                if (context.GetPasswordHistoryCountByUserID(updatedUser.UserID.Value) >= 4)
                                {
                                    context.DeletePasswordHistoryByID(context.GetLowestPasswordHistoryIDByUserID(updatedUser.UserID.Value));
                                }

                                passwordHistory.CreatedBy = currentUserID;
                                passwordHistory.HashPassword = updatedUser.HashPassword;
                                passwordHistory.UserID = updatedUser.UserID;
                                context.InsertPasswordHistory(passwordHistory);

                                userActivity.UserActivityTypeID = "ChangePassword";
                                userActivity.ReferenceID = currentUserID.ToString();
                                userActivity.CreatedBy = currentUserID;
                                context.InsertUserActivity(userActivity);

                                result.ReturnData = updatedUser;
                                result.IsSuccess = true;
                                result.ErrorMessage = String.Empty;
                            }
                        }
                        else
                        {
                            result.ReturnData = null;
                            result.IsSuccess = false;
                            result.ErrorMessage = TextResources.PasswordComplexity_ErrorMessage;
                        }
                    }
                    else
                    {
                        result.ReturnData = null;
                        result.IsSuccess = false;
                        result.ErrorMessage = String.Format(TextResources.NotFound_ErrorMessage, "User");
                    }

                    context.Commit();
                }
                catch (Exception exception)
                {
                    logException = exception;
                    result.ReturnData = null;
                    result.IsSuccess = false;
                    result.ErrorMessage = TextResources.ActionProblemOccured_ErrorMessage;
                }
            }

            LogHelper.LogException(logException);

            return result;
        }

        #endregion

        #endregion
    }
}
