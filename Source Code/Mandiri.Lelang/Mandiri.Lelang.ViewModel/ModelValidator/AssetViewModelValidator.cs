﻿using FluentValidation;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{

    /// <summary>
    /// The validator class for asset view model
    /// </summary>
    public class AssetViewModelValidator : AbstractValidator<AssetViewModel>
    {

        /// <summary>
        /// The constructor that defines all the validation rules
        /// </summary>
        public AssetViewModelValidator()
        {
            //Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;

            //RuleFor(m => m.SegmentID).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.SegmentID).NotEmpty().When(m => String.IsNullOrEmpty(m.ApprovalType.HasValue ? "not-null" : String.Empty)).WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.CategoryID).NotEmpty().When(m => String.IsNullOrEmpty(m.ApprovalType.HasValue ? "not-null" : String.Empty)).WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.Type).NotEmpty().When(m => String.IsNullOrEmpty(m.ApprovalType.HasValue ? "not-null" : String.Empty)).WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            //RuleFor(m => m.CategoryID).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.ProvinceID).NotEmpty().When(m => String.IsNullOrEmpty(m.ApprovalType.HasValue ? "not-null" : String.Empty)).WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            //RuleFor(m => m.ProvinceID).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.CityID).NotEmpty().When(m => String.IsNullOrEmpty(m.ApprovalType.HasValue ? "not-null" : String.Empty)).WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            //RuleFor(m => m.CityID).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.BranchID).NotEmpty().When(m => String.IsNullOrEmpty(m.ApprovalType.HasValue ? "not-null" : String.Empty)).WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            //RuleFor(m => m.BranchID).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.Address).NotEmpty().When(m => String.IsNullOrEmpty(m.ApprovalType.HasValue ? "not-null" : String.Empty)).WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            //RuleFor(m => m.Address).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);

            RuleFor(m => m.OwnerShipDocumentTypeID).NotEmpty().When(m => String.IsNullOrEmpty(m.ApprovalType.HasValue ? "not-null" : String.Empty)).WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            //RuleFor(m => m.OwnerShipDocumentTypeID).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);

            RuleFor(m => m.AuctionTime).NotEmpty().When(m => !String.IsNullOrEmpty(m.AuctionDate.HasValue ? m.AuctionDate.Value.ToString() : String.Empty)).WithLocalizedMessage(() => TextResources.InvalidScheduleTime_ErrorMessage);
            RuleFor(m => m.TimeZoneID).NotEmpty().When(m => !String.IsNullOrEmpty(m.AuctionTime)).WithLocalizedMessage(() => TextResources.InvalidTimeZone_ErrorMessage);
            RuleFor(m => m.AuctionTime).Matches(@"([01]?[0-9]|2[0-3]):[0-5][0-9]").When(m => !String.IsNullOrEmpty(m.AuctionTime)).WithLocalizedMessage(() => TextResources.InvalidScheduleTime_ErrorMessage);
            RuleFor(m => m.DocumentNumber).NotEmpty().When(m => ((m.ApprovalType.HasValue && m.ApprovalType.Value == 3) || !m.ApprovalType.HasValue) && m.OwnerShipDocumentTypeID.HasValue).WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.OldPrice).InclusiveBetween(0m, 100000000000).When(m => m.Type == 2).WithLocalizedMessage(() => TextResources.InvalidRange_ErrorMessage);
            RuleFor(m => m.NewPrice).InclusiveBetween(0m, 100000000000).When(m => m.Type == 2).WithLocalizedMessage(() => TextResources.InvalidRange_ErrorMessage);
            RuleFor(m => m.Price).InclusiveBetween(0m, 100000000000).When(m => m.Type == 1).WithLocalizedMessage(() => TextResources.InvalidRange_ErrorMessage);
            RuleFor(m => m.ExpiredDate).NotEmpty().When(m => m.Type == 1).WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.Photo1).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);

            RuleFor(m => m.KPKNLPhone1).Matches(@"^\+\d+").When(m => !String.IsNullOrEmpty(m.KPKNLPhone1)).WithLocalizedMessage(() => TextResources.InvalidPhoneNumber_ErrorMessage);
            RuleFor(m => m.KPKNLPhone2).Matches(@"^\+\d+").When(m => !String.IsNullOrEmpty(m.KPKNLPhone2)).WithLocalizedMessage(() => TextResources.InvalidPhoneNumber_ErrorMessage);

            RuleFor(m => m.AuctionHallPhone1).Matches(@"^\+\d+").When(m => !String.IsNullOrEmpty(m.AuctionHallPhone1)).WithLocalizedMessage(() => TextResources.InvalidPhoneNumber_ErrorMessage);
            RuleFor(m => m.AuctionHallPhone2).Matches(@"^\+\d+").When(m => !String.IsNullOrEmpty(m.AuctionHallPhone2)).WithLocalizedMessage(() => TextResources.InvalidPhoneNumber_ErrorMessage);

            RuleFor(m => m.SoldValue).InclusiveBetween(1m, 100000000000).When(m => m.CurrentStatusID == 5).WithLocalizedMessage(() => TextResources.InvalidRange_ErrorMessage);

            //RuleFor(m => m.ApproveReason).NotEmpty().When(m => m.ApprovalType.HasValue && m.ApprovalType.Value == 1).WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.RejectReason).NotEmpty().When(m => m.ApprovalType.HasValue && m.ApprovalType.Value == 2).WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
        }
    }
}
