﻿using FluentValidation;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// The validator class for forgot password view model
    /// </summary>
    public class ForgotPasswordViewModelValidator : AbstractValidator<ForgotPasswordViewModel>
    {
        /// <summary>
        /// The constructor that defines all the validation rules
        /// </summary>
        public ForgotPasswordViewModelValidator()
        {
            //Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;

            //Email
            RuleFor(m => m.Email).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.Email).EmailAddress().WithLocalizedMessage(() => TextResources.InvalidEmail_ID_ErrorMessage);
        }
    }
}
