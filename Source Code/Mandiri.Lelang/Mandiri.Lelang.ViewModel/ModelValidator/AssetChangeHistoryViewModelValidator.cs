﻿using FluentValidation;
using Mandiri.Lelang.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    public class AssetChangeHistoryViewModelValidator : AbstractValidator<AssetChangeHistoryViewModel>
    {
        public AssetChangeHistoryViewModelValidator()
        {
            //Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;
        }
    }
}
