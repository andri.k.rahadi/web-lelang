﻿using FluentValidation;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel.WebAdmin;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// The validator class for branch view model
    /// </summary>
    public class MinMaxPriceViewModelValidator : AbstractValidator<MinMaxPriceViewModel>
    {
        /// <summary>
        /// The constructor that defines all the validation rules
        /// </summary>
        public MinMaxPriceViewModelValidator()
        {
            // Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;

            //Name
            RuleFor(m => m.Name).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.Name).Length(0, 200).WithLocalizedMessage(() => TextResources.MaxLength_ErrorMessage);

            RuleFor(m => m.Value).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            //RuleFor(m => m.MinValue).GreaterThanOrEqualTo(1).WithLocalizedMessage(() => TextResources.GreaterThanOrEqualTo_ErrorMessage);
            RuleFor(m => m.Value).InclusiveBetween(1m, 100000000000).WithLocalizedMessage(() => TextResources.InvalidRange_ErrorMessage);
            //RuleFor(m => m.MinValue).LessThanOrEqualTo(m => m.MaxValue).WithLocalizedMessage(() => TextResources.LessThanOrEqualTo_ErrorMessage);

            //RuleFor(m => m.MaxValue).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            //RuleFor(m => m.MaxValue).GreaterThanOrEqualTo(1).WithLocalizedMessage(() => TextResources.GreaterThanOrEqualTo_ErrorMessage);
            //RuleFor(m => m.MaxValue).InclusiveBetween(1m, 100000000000).WithLocalizedMessage(() => TextResources.InvalidRange_ErrorMessage);
        }
    }
}
