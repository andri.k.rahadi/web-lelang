﻿using FluentValidation;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel.WebAdmin;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// The validator class for message view model
    /// </summary>
    public class MessageViewModelValidator : AbstractValidator<MessageViewModel>
    {
        /// <summary>
        /// The constructor that defines all the validation rules
        /// </summary>
        public MessageViewModelValidator()
        {
            //Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;
            //MessageCategoryTypeID
            RuleFor(m => m.MessageCategoryTypeID).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.MessageCategoryTypeID).GreaterThan(0).WithLocalizedMessage(() => TextResources.GreaterThan_ErrorMessage);
            //Name
            RuleFor(m => m.Name).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.Name).Length(0, 200).WithLocalizedMessage(() => TextResources.MaxLength_ErrorMessage);
            //Phone
            RuleFor(m => m.Phone).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.Phone).Length(0, 200).WithLocalizedMessage(() => TextResources.MaxLength_ErrorMessage);
            //MessageStatusID
            RuleFor(m => m.MessageStatusID).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.MessageStatusID).GreaterThan(0).WithLocalizedMessage(() => TextResources.GreaterThan_ErrorMessage);
            //BranchID
            //RuleFor(m => m.BranchID).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            //RuleFor(m => m.BranchID).GreaterThan(0).WithLocalizedMessage(() => TextResources.GreaterThan_ErrorMessage);
            //Email
            RuleFor(m => m.Email).EmailAddress();
        }
    }
}
