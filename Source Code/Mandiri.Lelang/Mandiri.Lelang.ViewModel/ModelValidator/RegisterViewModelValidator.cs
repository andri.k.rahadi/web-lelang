﻿using FluentValidation;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.CustomValidator;
using Mandiri.Lelang.ViewModel.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// The validator class for register view model
    /// </summary>
    public class RegisterViewModelValidator : AbstractValidator<RegisterViewModel>
    {
        /// <summary>
        /// The constructor that defines all the validation rules
        /// </summary>
        public RegisterViewModelValidator()
        {
            //Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;

            //Email
            RuleFor(m => m.Email).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.Email).EmailAddress().WithLocalizedMessage(() => TextResources.InvalidEmail_ID_ErrorMessage);

            //Name
            RuleFor(m => m.FullName).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);

            //Mobile Phone Number
            RuleFor(m => m.MobilePhoneNumber).NotEmpty().When(m => m.MandatoryMobilePhoneNumber == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.MobilePhoneNumber).Matches(@"^\d+$").When(m => !String.IsNullOrEmpty(m.MobilePhoneNumber)).WithLocalizedMessage(() => TextResources.InvalidMobilePhoneNumber_ID_ErrorMessage);

            //Address
            RuleFor(m => m.FullAddress).NotEmpty().When(m => m.MandatoryFullAddress == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);

            //City
            RuleFor(m => m.CityID).NotEmpty().When(m => m.MandatoryCityID == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.CityID).NotNull().When(m => m.MandatoryCityID == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.CityID).GreaterThan(0).When(m => m.MandatoryCityID == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);

            //Province
            RuleFor(m => m.ProvinceID).NotEmpty().When(m => m.MandatoryProvinceID == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.ProvinceID).NotNull().When(m => m.MandatoryProvinceID == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.ProvinceID).GreaterThan(0).When(m => m.MandatoryProvinceID == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);

            //Identification Number
            RuleFor(m => m.IdentificationNumber).NotEmpty().When(m => m.MandatoryIdentificationNumber == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.IdentificationNumber).Matches(@"^\d+$").When(m => !String.IsNullOrEmpty(m.IdentificationNumber)).WithLocalizedMessage(() => TextResources.InvalidIdentificationNumber_ID_ErrorMessage);

            //Date of Birth
            RuleFor(m => m.DateOfBirth).NotEmpty().When(m => m.MandatoryDateOfBirth == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.DateOfBirth).NotNull().When(m => m.MandatoryDateOfBirth == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.DateOfBirth).LessThan(DateTime.Today).When(m => m.DateOfBirth.HasValue).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);

            //Place of Birth
            RuleFor(m => m.PlaceOfBirth).NotEmpty().When(m => m.MandatoryPlaceOfBirth == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.PlaceOfBirth).Matches(@"^[a-zA-Z., ]+$").When(m => !String.IsNullOrEmpty(m.PlaceOfBirth)).WithLocalizedMessage(() => TextResources.InvalidField_ID_ErrorMessage);

            //Password
            RuleFor(m => m.Password).NotEmpty().When(m => !m.IsEditProfile).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            //RuleFor(m => m.Password).Length(8, 100).When(m => !m.IsEditProfile).WithLocalizedMessage(() => TextResources.MinLength_ErrorMessage);
            //alfanumric, huruf besar, huruf kecil, simbol 
            //RuleFor(m => m.Password).Matches(@"^(?=(.*\d){1})(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z\d]).{8,}$").When(m => !String.IsNullOrEmpty(m.Password)).WithLocalizedMessage(() => TextResources.MemberPasswordComplexity_ID_ErrorMessage);
            if (ConfigurationManager.AppSettings["ComplexityPassword"] == "true") 
            {
                RuleFor(m => m.Password).Matches(@"^(?=(.*\d){1})(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z\d]).{8,}$").When(m => !String.IsNullOrEmpty(m.Password)).WithLocalizedMessage(() => TextResources.MemberPasswordComplexity_ID_ErrorMessage);
            }
            else
            {
                RuleFor(m => m.Password).Matches(@"^[0-9a-zA-Z ]+$").When(m => !String.IsNullOrEmpty(m.Password)).WithLocalizedMessage(() => TextResources.MemberPasswordSimple_ID_ErrorMessage);
            }
            

            //Confirm Password
            RuleFor(m => m.ConfirmPassword).NotEmpty().When(m => !m.IsEditProfile || !String.IsNullOrWhiteSpace(m.Password)).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.ConfirmPassword).Equal(m2 => m2.Password).When(m => !m.IsEditProfile || !String.IsNullOrWhiteSpace(m.Password)).WithLocalizedMessage(() => TextResources.PropertyUnmatch_ID_ErrorMessage);

            //Old Password
            RuleFor(m => m.OldPassword).NotEmpty().When(m => m.IsEditProfile && !String.IsNullOrWhiteSpace(m.Password)).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);

            //Preference
            //City
            RuleFor(m => m.PreferenceCityID).SetValidator(new ArrayOfIntegerMustContainsItemsValidator()).When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            //RuleFor(m => m.PreferenceCityID).NotEmpty().When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            //RuleFor(m => m.PreferenceCityID).NotNull().When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            //RuleFor(m => m.PreferenceCityID).GreaterThan(0).When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);

            //Province
            RuleFor(m => m.PreferenceProvinceID).SetValidator(new ArrayOfIntegerMustContainsItemsValidator()).When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            //RuleFor(m => m.PreferenceProvinceID).NotEmpty().When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            //RuleFor(m => m.PreferenceProvinceID).NotNull().When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            //RuleFor(m => m.PreferenceProvinceID).GreaterThan(0).When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);

            //Prices
            RuleFor(m => m.PreferenceMinPriceID).NotEmpty().When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.PreferenceMinPriceID).NotNull().When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.PreferenceMinPriceID).GreaterThan(0).When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.PreferenceMaxPriceID).NotEmpty().When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.PreferenceMaxPriceID).NotNull().When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.PreferenceMaxPriceID).GreaterThan(0).When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);

            //Category            
            RuleFor(m => m.PreferenceCategoryID).SetValidator(new ArrayOfIntegerMustContainsItemsValidator()).When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            //RuleFor(m => m.PreferenceCategoryID).NotEmpty().When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            //RuleFor(m => m.PreferenceCategoryID).NotNull().When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            //RuleFor(m => m.PreferenceCategoryID).GreaterThan(0).When(m => m.MandatoryPreference == true).WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);

            //Flag Email Notification            
            RuleFor(m => m.FlagEmailNotification).Equal(m => m.TrueFlagEmailNotification).WithLocalizedMessage(() => TextResources.MustAgree_ID_ErrorMessage);

            //Captcha
            //RuleFor(m => m.CaptchaCode).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
        }
    }
}
