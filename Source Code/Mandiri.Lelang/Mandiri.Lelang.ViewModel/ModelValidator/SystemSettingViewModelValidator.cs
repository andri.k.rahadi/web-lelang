﻿using FluentValidation;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// validator for view model system setting
    /// </summary>
    public class SystemSettingViewModelValidator : AbstractValidator<SystemSettingViewModel>
    {
        /// <summary>
        /// Constructor for system setting view model validator
        /// </summary>
        public SystemSettingViewModelValidator()
        {
            // Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;

            RuleFor(m => m.VisibilityDaysPeriodForPaidAndSold).GreaterThanOrEqualTo(0).WithLocalizedMessage(() => TextResources.GreaterThanOrEqualTo_ErrorMessage);
            RuleFor(m => m.EmailReminderPeriod).GreaterThanOrEqualTo(0).WithLocalizedMessage(() => TextResources.GreaterThanOrEqualTo_ErrorMessage);
            RuleFor(m => m.WebsitePopupImage).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.MobileWebsitePopupImage).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.MandatoryForMemberAddress).NotNull().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.MandatoryForMemberCity).NotNull().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.MandatoryForMemberDateOfBirth).NotNull().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.MandatoryForMemberIdentificationNumber).NotNull().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.MandatoryForMemberMobilePhoneNumber).NotNull().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.MandatoryForMemberPlaceOfBirth).NotNull().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.MandatoryForMemberPreference).NotNull().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.MandatoryForMemberProvince).NotNull().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.MobileNotificationSyncPeriod).GreaterThanOrEqualTo(0).WithLocalizedMessage(() => TextResources.GreaterThanOrEqualTo_ErrorMessage);
        }
    }
}
