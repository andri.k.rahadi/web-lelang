﻿using FluentValidation;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel.WebAdmin;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// The validator class for branch view model
    /// </summary>
    public class BranchViewModelValidator : AbstractValidator<BranchViewModel>
    {
        /// <summary>
        /// The constructor that defines all the validation rules
        /// </summary>
        public BranchViewModelValidator()
        {
            // Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;

            //Name
            RuleFor(m => m.Name).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.Name).Length(0, 200).WithLocalizedMessage(() => TextResources.MaxLength_ErrorMessage);

            //Alias
            RuleFor(m => m.Alias).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);

            RuleFor(m => m.Phone1).Matches(@"^\+\d+").When(m => !String.IsNullOrEmpty(m.Phone1)).WithLocalizedMessage(() => TextResources.InvalidPhoneNumber_ErrorMessage);
            RuleFor(m => m.Phone2).Matches(@"^\+\d+").When(m => !String.IsNullOrEmpty(m.Phone2)).WithLocalizedMessage(() => TextResources.InvalidPhoneNumber_ErrorMessage);
        }
    }
}
