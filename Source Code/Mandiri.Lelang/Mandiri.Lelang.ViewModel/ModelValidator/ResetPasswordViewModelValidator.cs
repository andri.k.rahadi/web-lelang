﻿using FluentValidation;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// The validator class for reset password view model
    /// </summary>
    public class ResetPasswordViewModelValidator : AbstractValidator<ResetPasswordViewModel>
    {
        /// <summary>
        /// The constructor that defines all the validation rules
        /// </summary>
        public ResetPasswordViewModelValidator()
        {
            //Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;
          
            //Password
            RuleFor(m => m.Password).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.Password).Length(7, 100).WithLocalizedMessage(() => TextResources.MinLength_ID_ErrorMessage);

            //Confirm Password
            RuleFor(m => m.ConfirmPassword).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.ConfirmPassword).Equal(m2 => m2.Password).WithLocalizedMessage(() => TextResources.PropertyUnmatch_ID_ErrorMessage);
        }
    }
}
