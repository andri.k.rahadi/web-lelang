﻿using FluentValidation;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.WebAdmin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// The validator class for login view model
    /// </summary>
    public class LoginViewModelValidator : AbstractValidator<LoginViewModel>
    {
        /// <summary>
        /// The constructor that defines all the validation rules
        /// </summary>
        public LoginViewModelValidator()
        {
            //Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;

            //Username
            RuleFor(m => m.Username).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);

            //Password
            RuleFor(m => m.Password).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.Password).Length(7, 100).WithLocalizedMessage(() => TextResources.MinLength_ErrorMessage);
        }
    }
}
