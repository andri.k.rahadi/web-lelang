﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    public class TaskListViewModelValidator : AbstractValidator<TaskListViewModel>
    {
        public TaskListViewModelValidator()
        {
            //Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;
        }
    }
}
