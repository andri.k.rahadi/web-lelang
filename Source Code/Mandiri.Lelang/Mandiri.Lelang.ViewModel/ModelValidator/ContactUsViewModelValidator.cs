﻿using FluentValidation;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// The validator class for contact us view model
    /// </summary>
    public class ContactUsViewModelValidator : AbstractValidator<ContactUsViewModel>
    {
        /// <summary>
        /// The constructor that defines all the validation rules
        /// </summary>
        public ContactUsViewModelValidator()
        {
            //Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;

            //Name
            RuleFor(m => m.Name).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);

            //Email
            RuleFor(m => m.Email).EmailAddress().WithLocalizedMessage(() => TextResources.InvalidEmail_ErrorMessage);
            RuleFor(m => m.Email).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);

            //Contact
            RuleFor(m => m.Contact).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.Contact).Matches(@"^\d+").When(m => !String.IsNullOrEmpty(m.Contact)).WithLocalizedMessage(() => TextResources.InvalidPhoneNumber_ErrorMessage);

            //Category
            RuleFor(m => m.MessageCategoryTypeID).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);

            //Message
            RuleFor(m => m.Message).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
        }
    }
}
