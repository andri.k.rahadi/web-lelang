﻿using FluentValidation;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// The validator class for user view model
    /// </summary>
    public class UserViewModelValidator : AbstractValidator<UserViewModel>
    {
        /// <summary>
        /// The constructor that defines all the validation rules
        /// </summary>
        public UserViewModelValidator()
        {
            //Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;

            //Username
            RuleFor(m => m.Username).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.Username).Length(0, 200).WithLocalizedMessage(() => TextResources.MaxLength_ErrorMessage);

            //Full Name
            RuleFor(m => m.FullName).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.FullName).Length(0, 200).WithLocalizedMessage(() => TextResources.MaxLength_ErrorMessage);

            //Password 
            RuleFor(m => m.Password).Length(7, 100).WithLocalizedMessage(() => TextResources.MinLength_ErrorMessage);
            RuleFor(m => m.Password).Matches(@"((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,100})").WithLocalizedMessage(() => TextResources.PasswordComplexity_ErrorMessage);

            //Confirm Password
            RuleFor(m => m.ConfirmPassword).Equal(m2 => m2.Password).WithLocalizedMessage(() => TextResources.PropertyUnmatch_ErrorMessage);

            //Email
            RuleFor(m => m.Email).EmailAddress().WithLocalizedMessage(() => TextResources.InvalidEmail_ErrorMessage);

            //Branch
            RuleFor(m => m.BranchID).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);

            RuleSet("AddUser_Password", () =>
            {
                RuleFor(m => m.Password).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
                RuleFor(m => m.ConfirmPassword).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            });
        }
    }
}
