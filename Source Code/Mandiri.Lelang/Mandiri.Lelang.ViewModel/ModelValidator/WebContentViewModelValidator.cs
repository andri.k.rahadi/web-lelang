﻿using FluentValidation;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// The validator class for web content view model
    /// </summary>
    public class WebContentViewModelValidator : AbstractValidator<WebContentViewModel>
    {
        /// <summary>
        /// The constructor that defines all the validation rules
        /// </summary>
        public WebContentViewModelValidator()
        {
            //Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;

            //Content
            RuleFor(m => m.Content).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
        }
    }
}
