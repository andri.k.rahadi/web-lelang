﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    public class AssetChangeViewModelValidator : AbstractValidator<AssetChangeViewModel>
    {
        /// <summary>
        /// Constructor for approval history validator
        /// </summary>
        public AssetChangeViewModelValidator()
        {
            //Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;
        }
    }
}
