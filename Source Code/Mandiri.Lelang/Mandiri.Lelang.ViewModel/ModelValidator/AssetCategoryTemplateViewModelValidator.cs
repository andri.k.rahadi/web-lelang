﻿using FluentValidation;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    public class AssetCategoryTemplateViewModelValidator : AbstractValidator<AssetCategoryTemplateViewModel>
    {

        public AssetCategoryTemplateViewModelValidator()
        {
            //Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;
            RuleFor(m => m.Value).NotEmpty().When(m => m.IsMandatory == true).WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
        }
    }
}
