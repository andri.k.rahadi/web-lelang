﻿using FluentValidation;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// The validator class for change password view model
    /// </summary>
    public class ChangePasswordViewModelValidator : AbstractValidator<ChangePasswordViewModel>
    {
        /// <summary>
        /// The constructor that defines all the validation rules
        /// </summary>
        public ChangePasswordViewModelValidator()
        {
            //Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;

            //Password 
            RuleFor(m => m.Password).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage).Length(7, 100).WithLocalizedMessage(() => TextResources.MinLength_ErrorMessage);
            RuleFor(m => m.Password).Matches(@"((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,100})").WithLocalizedMessage(() => TextResources.PasswordComplexity_ErrorMessage);

            //Confirm Password
            RuleFor(m => m.ConfirmPassword).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage).Equal(m2 => m2.Password).WithLocalizedMessage(() => TextResources.PropertyUnmatch_ErrorMessage);
        }
    }
}
