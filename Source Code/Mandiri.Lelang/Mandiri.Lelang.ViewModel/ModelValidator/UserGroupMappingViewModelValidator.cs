﻿using FluentValidation;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel.WebAdmin;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// The validator class for category template view model
    /// </summary>
    public class UserGroupMappingViewModelValidator : AbstractValidator<UserGroupMappingViewModel>
    {

        /// <summary>
        /// The constructor that defines all the validation rules
        /// </summary>
        public UserGroupMappingViewModelValidator()
        {
            // Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;

            //Group
            RuleFor(m => m.GroupID).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            
            //User
            RuleFor(m => m.UserID).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
        }
    }
}
