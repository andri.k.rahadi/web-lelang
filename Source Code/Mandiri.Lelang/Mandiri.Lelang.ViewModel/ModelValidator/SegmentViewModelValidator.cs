﻿using FluentValidation;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel.WebAdmin;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// The validator class for segment view model
    /// </summary>
    public class SegmentViewModelValidator : AbstractValidator<SegmentViewModel>
    {
        /// <summary>
        /// The constructor that defines all the validation rules
        /// </summary>
        public SegmentViewModelValidator()
        {
            // Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;

            RuleFor(m => m.SegmentID).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);

            //Name
            RuleFor(m => m.Name).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.Name).Length(0, 200).WithLocalizedMessage(() => TextResources.MaxLength_ErrorMessage);
        }
    }
}
