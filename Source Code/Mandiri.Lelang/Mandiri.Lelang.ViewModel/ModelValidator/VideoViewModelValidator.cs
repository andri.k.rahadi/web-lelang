﻿using FluentValidation;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// The validator class for Video view model
    /// </summary>
    /// [Validator(typeof(VideoViewModelValidator))]
    public class VideoViewModelValidator : AbstractValidator<VideoViewModel>
    {
        public VideoViewModelValidator()
        {
            //Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;
            //Title
            RuleFor(m => m.Title).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.Title).Length(0, 30).WithLocalizedMessage(() => TextResources.MaxLength_ErrorMessage);
            //ThumbnailPath
            RuleFor(m => m.ThumbnailPath).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.ThumbnailPath).Length(0, 200).WithLocalizedMessage(() => TextResources.MaxLength_ErrorMessage);
            //VideoURL
            RuleFor(m => m.VideoURL).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.VideoURL).Length(0, 200).WithLocalizedMessage(() => TextResources.MaxLength_ErrorMessage);
            //ShowInHome
            //RuleFor(m => m.ShowInHome).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
        }
    }
}
