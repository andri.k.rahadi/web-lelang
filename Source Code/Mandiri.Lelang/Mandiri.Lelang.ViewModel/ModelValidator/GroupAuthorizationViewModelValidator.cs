﻿using FluentValidation;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// The validator class for login view model
    /// </summary>
    public class GroupAuthorizationViewModelValidator : AbstractValidator<GroupAuthorizationViewModel>
    {
        /// <summary>
        /// The constructor that defines all the validation rules
        /// </summary>
        public GroupAuthorizationViewModelValidator()
        {
            //Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;

            RuleFor(m => m.GroupID).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
        }
    }
}
