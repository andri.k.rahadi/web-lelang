﻿using FluentValidation;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// The validator class for login view model
    /// </summary>
    public class WebLoginViewModelValidator : AbstractValidator<LoginViewModel>
    {
        /// <summary>
        /// The constructor that defines all the validation rules
        /// </summary>
        public WebLoginViewModelValidator()
        {
            //Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;

            //Email
            RuleFor(m => m.Email).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.Email).EmailAddress().WithLocalizedMessage(() => TextResources.InvalidEmail_ID_ErrorMessage);

            //Password
            RuleFor(m => m.Password).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
            RuleFor(m => m.Password).Length(8, 100).WithLocalizedMessage(() => TextResources.MinLength_ID_ErrorMessage);

            //Captcha
            //RuleFor(m => m.CaptchaCode).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ID_ErrorMessage);
        }
    }
}
