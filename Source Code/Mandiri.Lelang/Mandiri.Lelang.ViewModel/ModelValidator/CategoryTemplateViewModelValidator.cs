﻿using FluentValidation;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel.WebAdmin;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel.ModelValidator
{
    /// <summary>
    /// The validator class for category template view model
    /// </summary>
    public class CategoryTemplateViewModelValidator : AbstractValidator<CategoryTemplateViewModel>
    {

        /// <summary>
        /// The constructor that defines all the validation rules
        /// </summary>
        public CategoryTemplateViewModelValidator()
        {
            // Set the cascade mode to stop on first error
            this.CascadeMode = FluentValidation.CascadeMode.StopOnFirstFailure;

            RuleFor(m => m.CategoryID).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);

            //Name
            RuleFor(m => m.FieldName).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
            RuleFor(m => m.FieldName).Length(0, 200).WithLocalizedMessage(() => TextResources.MaxLength_ErrorMessage);

            //Mandatory
            //RuleFor(m => m.IsMandatory).NotEmpty().WithLocalizedMessage(() => TextResources.Required_ErrorMessage);
        }
    }
}
