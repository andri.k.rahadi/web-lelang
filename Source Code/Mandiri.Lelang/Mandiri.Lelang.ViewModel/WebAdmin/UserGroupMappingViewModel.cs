﻿using FluentValidation.Attributes;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The UserGroupMapping view model
    /// </summary>
    [Validator(typeof(UserGroupMappingViewModelValidator))]
    public class UserGroupMappingViewModel : BaseViewModel
    {
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "UserGroupMappingID_DisplayName")]
        public int UserGroupMappingID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Group_DisplayName")]
        public int GroupID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "GroupName_DisplayName")]
        public String GroupName { get; set; }
        public IEnumerable<Group> Groups { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "User_DisplayName")]
        public int UserID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Username_DisplayName")]
        public String Username { get; set; }
        public IEnumerable<User> Users { get; set; }
    }
}
