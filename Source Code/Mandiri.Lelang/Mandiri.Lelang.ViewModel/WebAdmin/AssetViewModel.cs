﻿using FluentValidation.Attributes;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;


namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The action hall view model
    /// </summary>
    [Validator(typeof(AssetViewModelValidator))]
    public class AssetViewModel : BaseViewModel
    {
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "AssetID_DisplayName")]
        public long AssetID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AssetCode_DisplayName")]
        public String AssetCode { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Segment_DisplayName")]
        public String SegmentID { get; set; }
        public IEnumerable<Segment> Segments { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "SegmentName_DisplayName")]
        public String SegmentName { get; set; }
        public String SegmentRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetAccountNumber_DisplayName")]
        public String AccountNumber { get; set; }
        public String AccountNumberRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "IsEmailAuction_DisplayName")]
        public bool IsEmailAuction { get; set; }
        public String IsEmailAuctionRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "EmailAuctionURL_DisplayName")]
        public String EmailAuctionURL { get; set; }
        public String EmailAuctionURLRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetAccountName_DisplayName")]
        public String AccountName { get; set; }
        public String AccountNameRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "Category_DisplayName")]
        public int? CategoryID { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "CategoryName_DisplayName")]
        public String CategoryName { get; set; }
        public String CategoryRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "Type_DisplayName")]
        public byte Type { get; set; }
        public IEnumerable<dynamic> Types { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Type_DisplayName")]
        public String TypeName { get; set; }
        public String TypeRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "Price_DisplayName")]
        public decimal Price { get; set; }
        public String PriceRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "ExpiredDate_DisplayName")]
        public DateTime? ExpiredDate { get; set; }
        public String ExpiredDateRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetAddress_DisplayName")]
        public String Address { get; set; }
        public String AddressRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "Province_DisplayName")]
        public int? ProvinceID { get; set; }
        public IEnumerable<Province> Provinces { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "ProvinceName_DisplayName")]
        public String ProvinceName { get; set; }
        public String ProvinceRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "City_DisplayName")]
        public int? CityID { get; set; }
        public IEnumerable<City> Cities { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "City_DisplayName")]
        public String CityName { get; set; }
        public String CityNameRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetOldPrice_DisplayName")]
        public Decimal OldPrice { get; set; }
        public String OldPriceRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetNewPrice_DisplayName")]
        public Decimal NewPrice { get; set; }
        public String NewPriceRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetMarketValue_DisplayName")]
        public Decimal MarketValue { get; set; }
        public String MarketValueRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetLiquidationValue_DisplayName")]
        public Decimal LiquidationValue { get; set; }
        public String LiquidationValueRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetSecurityRightsValue_DisplayName")]
        public Decimal SecurityRightsValue { get; set; }
        public String SecurityRightsValueRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetSoldValue_DisplayName")]
        public Decimal SoldValue { get; set; }
        public String SoldValueRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetValuationDate_DisplayName")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? ValuationDate { get; set; }
        public String ValuationDateRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "Branch_DisplayName")]
        public int? BranchID { get; set; }
        public IEnumerable<Branch> Branches { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "BranchName_DisplayName")]
        public String BranchName { get; set; }
        public String BranchRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetAuctionDate_DisplayName")]
        [DisplayFormat(DataFormatString="{0:dd/MM/yyyy}")]
        public DateTime? AuctionDate { get; set; }
        public String AuctionDateRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetAuctionTime_DisplayName")]
        public String AuctionTime { get; set; }
        public String AuctionTimeRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "TimeZone_DisplayName")]
        public byte? TimeZoneID { get; set; }
        public String TimezoneRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetTransferInformation_DisplayName")]
        public String TransferInformation { get; set; }
        public String TransferInformationRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "KPKNL_DisplayName")]
        public int? KPKNLID { get; set; }
        public IEnumerable<KPKNL> KPKNLs { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AssetKPKNLName_DisplayName")]
        public String KPKNLName { get; set; }
        public String KPKNLRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetKPKNLAddress_DisplayName")]
        public String KPKNLAddress { get; set; }
        public String KPKNLAddressRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetAuctionAddress_DisplayName")]
        public String AuctionAddress { get; set; }
        public String AuctionAddressRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetKPKNLPhone1_DisplayName")]
        public String KPKNLPhone1 { get; set; }
        public String KPKNLPhone1RejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetKPKNLPhone2_DisplayName")]
        public String KPKNLPhone2 { get; set; }
        public String KPKNLPhone2RejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AuctionSchedule_DisplayName")]
        public int? AuctionScheduleID { get; set; }
        public String AuctionScheduleRejectReason { get; set; }

        public IEnumerable<AuctionViewModel> AuctionSchedules { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AuctionHall_DisplayName")]
        public int? AuctionHallID { get; set; }
        public IEnumerable<AuctionHall> AuctionHalls { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AssetAuctionHallName_DisplayName")]
        public String AuctionHallName { get; set; }
        public String AuctionHallRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetAuctionHallAddress_DisplayName")]
        public String AuctionHallAddress { get; set; }
        public String AuctionHallAddressRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetAuctionHallPhone1_DisplayName")]
        public String AuctionHallPhone1 { get; set; }
        public String AuctionHallPhone1RejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetAuctionHallPhone2_DisplayName")]
        public String AuctionHallPhone2 { get; set; }
        public String AuctionHallPhone2RejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "Photo1_DisplayName")]
        public String Photo1 { get; set; }
        public String Photo1RejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "Photo2_DisplayName")]
        public String Photo2 { get; set; }

        public String Photo2RejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "Photo3_DisplayName")]
        public String Photo3 { get; set; }
        public String Photo3RejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "Photo4_DisplayName")]
        public String Photo4 { get; set; }
        public String Photo4RejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "Photo5_DisplayName")]
        public String Photo5 { get; set; }
        public String Photo5RejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "OwnershipDocumentType_DisplayName")]
        public int? OwnerShipDocumentTypeID { get; set; }
        public IEnumerable<OwnershipDocumentType> OwnershipDocumentTypes { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "OwnershipDocumentTypeName_DisplayName")]
        public String OwnershipDocumentTypeName { get; set; }
        public String OwnershipDocumentTypeRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "DocumentNumber_DisplayName")]
        public String DocumentNumber { get; set; }
        public String DocumentNumberRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetLatitue_DisplayName")]
        public Double? Latitude { get; set; }
        public String LatitudeRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetLongitude_DisplayName")]
        public Double? Longitude { get; set; }
        public String LongitudeRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetCurrentStatus_DisplayName")]
        public byte? CurrentStatusID { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetAdditionalNote_DisplayName")]
        public String AdditionalNotes { get; set; }
        public String AdditionalNoteRejectReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetStatus_DisplayName")]
        public byte AssetStatusID { get; set; }
        public IEnumerable<AssetStatus> AssetStatuses { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AssetStatusName_DisplayName")]
        public String AssetStatusName { get; set; }

        public IList<AssetCategoryTemplateViewModel> CategoryTemplates { get; set; }
        public int ButtonType { get; set; }
        public int? ApprovalType { get; set; } // 1 : Aprove, 2 : Reject

        [Display(ResourceType = typeof(TextResources), Name = "ApproveReason_DisplayName")]
        public String ApproveReason { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "RejectReason_DisplayName")]
        public String RejectReason { get; set; }

        public IList<AssetCategoryTemplateViewModel> CategoryTemplateReasons { get; set; }

        public String ApproveOrRejectType { get; set; }
        public int UserGroupType { get; set; }
        //public bool IsAdmin { get; set; }
        public int UpdatePrice { get; set; }
    }
}
