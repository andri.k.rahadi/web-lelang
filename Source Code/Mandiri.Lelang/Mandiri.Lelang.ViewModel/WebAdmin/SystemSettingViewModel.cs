﻿using FluentValidation.Attributes;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel
{
    [Validator(typeof(SystemSettingViewModelValidator))]
    public class SystemSettingViewModel : BaseViewModel
    {
        public class Key
        {
            public static String VisibilityDaysPeriodForPaidAndSoldKey = "VisibilityDaysPeriodForPaidAndSold";
            public static String EmailReminderPeriodKey = "EmailReminderPeriod";
            public static String MandatoryForMemberAddressKey = "MandatoryForMemberAddress";
            public static String MandatoryForMemberCityKey = "MandatoryForMemberCity";
            public static String MandatoryForMemberDateOfBirthKey = "MandatoryForMemberDateOfBirth";
            public static String MandatoryForMemberIdentificationNumberKey = "MandatoryForMemberIdentificationNumber";
            public static String MandatoryForMemberMobilePhoneNumberKey = "MandatoryForMemberMobilePhoneNumber";
            public static String MandatoryForMemberPlaceOfBirthKey = "MandatoryForMemberPlaceOfBirth";
            public static String MandatoryForMemberPreferenceKey = "MandatoryForMemberPreference";
            public static String MandatoryForMemberProvinceKey = "MandatoryForMemberProvince";
            public static String MobileNotificationSyncPeriodKey = "MobileNotificationSyncPeriod";
            public static String MobileWebsitePopupImageKey = "MobileWebsitePopupImage";
            public static String WebsitePopupImageKey = "WebsitePopupImage";
        }

        [Editable(false)]
        public String VisibilityDaysPeriodForPaidAndSoldID { get; set; }
        [Editable(false)]
        public String EmailReminderPeriodID { get; set; }
        [Editable(false)]
        public String MandatoryForMemberAddressID { get; set; }
        [Editable(false)]
        public String MandatoryForMemberCityID { get; set; }
        [Editable(false)]
        public String MandatoryForMemberDateOfBirthID { get; set; }
        [Editable(false)]
        public String MandatoryForMemberIdentificationNumberID { get; set; }
        [Editable(false)]
        public String MandatoryForMemberMobilePhoneNumberID { get; set; }
        [Editable(false)]
        public String MandatoryForMemberPlaceOfBirthID { get; set; }
        [Editable(false)]
        public String MandatoryForMemberPreferenceID { get; set; }
        [Editable(false)]
        public String MandatoryForMemberProvinceID { get; set; }
        [Editable(false)]
        public String MobileNotificationSyncPeriodID { get; set; }
        [Editable(false)]
        public String WebsitePopupImageID { get; set; }
        [Editable(false)]
        public String MobileWebsitePopupImageID { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "VisibilityDaysPeriodForPaidAndSold_DisplayName")]
        public int VisibilityDaysPeriodForPaidAndSold { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "EmailReminderPeriod_DisplayName")]
        public int EmailReminderPeriod { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MandatoryForMemberAddress_DisplayName")]
        public bool MandatoryForMemberAddress { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MandatoryForMemberCity_DisplayName")]
        public bool MandatoryForMemberCity { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MandatoryForMemberDateOfBirth_DisplayName")]
        public bool MandatoryForMemberDateOfBirth { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MandatoryForMemberIdentificationNumber_DisplayName")]
        public bool MandatoryForMemberIdentificationNumber { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MandatoryForMemberMobilePhoneNumber_DisplayName")]
        public bool MandatoryForMemberMobilePhoneNumber { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MandatoryForMemberPlaceOfBirth_DisplayName")]
        public bool MandatoryForMemberPlaceOfBirth { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MandatoryForMemberPreference_DisplayName")]
        public bool MandatoryForMemberPreference { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MandatoryForMemberProvince_DisplayName")]
        public bool MandatoryForMemberProvince { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MobileNotificationSyncPeriod_DisplayName")]
        public int MobileNotificationSyncPeriod { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "WebsitePopupImage_DisplayName")]
        public string WebsitePopupImage { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MobileWebsitePopupImage_DisplayName")]
        public string MobileWebsitePopupImage { get; set; }
    }
}
