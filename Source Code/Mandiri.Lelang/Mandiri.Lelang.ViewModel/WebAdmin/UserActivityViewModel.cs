﻿using FluentValidation.Attributes;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The UserActivity view model
    /// </summary>
    [Validator(typeof(UserActivityViewModelValidator))]
    public class UserActivityViewModel: BaseViewModel
    {
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "UserActivityID_DisplayName")]
        public int UserActivityID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "UserActivityTypeID_DisplayName")]
        public String UserActivityTypeID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "ExtraDescription_DisplayName")]
        public String ExtraDescription { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "ReferenceID_DisplayName")]
        public String ReferenceID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "CreatedBy_DisplayName")]
        public String CreatedBy { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "DateTimeCreated_DisplayName")]
        public DateTime DateTimeCreated { get; set; }
    }
}
