﻿using FluentValidation.Attributes;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The asset change view model
    /// </summary>
    [Validator(typeof(AssetChangeViewModelValidator))]
    public class AssetChangeViewModel : BaseViewModel
    {
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "AssetID_DisplayName")]
        public long AssetID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AssetCode_DisplayName")]
        public String AssetCode { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "IsNew_DisplayName")]
        public bool? IsNew {get;set;}
        [Display(ResourceType = typeof(TextResources), Name = "IsUpdated_DisplayName")]
        public bool? IsUpdated {get;set;}
        [Display(ResourceType = typeof(TextResources), Name = "IsDeleted_DisplayName")]
        public bool? IsDeleted {get;set;}
        [Display(ResourceType = typeof(TextResources), Name = "UserAdmin_DisplayName")]
        public String UserAdmin {get;set;}
        [Display(ResourceType = typeof(TextResources), Name = "Date_DisplayName")]
        public DateTime Date {get;set;}
        [Display(ResourceType = typeof(TextResources), Name = "ApproveOrReject_DisplayName")]
        public String ApproveOrReject {get;set;}
        [Display(ResourceType = typeof(TextResources), Name = "ApproveOrRejectBy_DisplayName")]
        public String ApproveOrRejectBy { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "ApproveOrRejectDate_DisplayName")]
        public DateTime ApproveOrRejectDate { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "ApproveReason_DisplayName")]
        public String ApproveReason { get; set; }
    }
}
