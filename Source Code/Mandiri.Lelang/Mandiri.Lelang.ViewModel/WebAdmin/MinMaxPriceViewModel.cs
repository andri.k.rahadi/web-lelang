﻿using FluentValidation.Attributes;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The min max price view model
    /// </summary>
    [Validator(typeof(MinMaxPriceViewModelValidator))]
    public class MinMaxPriceViewModel : BaseViewModel
    {
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "MinMaxPriceID_DisplayName")]
        public int MinMaxPriceID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Name_DisplayName")]
        public String Name { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Description_DisplayName")]
        public String Description { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Value_DisplayName")]
        public Decimal Value { get; set; }
    }
}
