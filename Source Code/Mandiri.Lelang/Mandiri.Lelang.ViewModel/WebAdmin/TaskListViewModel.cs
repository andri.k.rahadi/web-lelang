﻿using FluentValidation.Attributes;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The asset change view model
    /// </summary>
    [Validator(typeof(TaskListViewModelValidator))]
    public class TaskListViewModel : BaseViewModel
    {
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "AssetChangesID_DisplayName")]
        public long AssetChangesID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AssetID_DisplayName")]
        public long AssetID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "IsNew_DisplayName")]
        public bool? IsNew { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "IsUpdated_DisplayName")]
        public bool? IsUpdated { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "IsDeleted_DisplayName")]
        public bool? IsDeleted { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "CurrentApprovalStatus_DisplayName")]
        public int CurrentApprovalStatus { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AssetCurrentStatus_DisplayName")]
        public int OriginalAssetStatusID { get; set; }

        [Display(ResourceType = typeof(TextResources), Name = "AssetCode_DisplayName")]
        public String AssetCode { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "SegmentName_DisplayName")]
        public String SegmentName { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "City_DisplayName")]
        public String CityName { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Province_DisplayName")]
        public String ProvinceName { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "CategoryName_DisplayName")]
        public String CategoryName { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Photo1_DisplayName")]
        public String Photo1 { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Photo2_DisplayName")]
        public String Photo2 { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Photo3_DisplayName")]
        public String Photo3 { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Photo4_DisplayName")]
        public String Photo4 { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Photo5_DisplayName")]
        public String Photo5 { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AssetAdditionalNote_DisplayName")]
        public String AdditionalNote { get; set; }
    }
}
