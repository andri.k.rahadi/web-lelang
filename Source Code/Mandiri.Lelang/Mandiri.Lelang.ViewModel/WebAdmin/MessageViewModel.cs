﻿using FluentValidation.Attributes;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The message view model
    /// </summary>
    [Validator(typeof(MessageViewModelValidator))]
    public class MessageViewModel : BaseViewModel
    {
        public MessageViewModel() 
        {
            this.DateTimeCreated = DateTime.Now;
            this.CreatedDate = DateTimeCreated.ToString("dd/MM/yyyy HH:mm:ss");
        }
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "MessageID_DisplayName")]
        public int MessageID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MessageCategoryName_DisplayName")]
        public String Category { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MessageCategory_DisplayName")]
        public int MessageCategoryTypeID { get; set; }
        public IEnumerable<MessageCategoryType> MessageCategoryList { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Name_DisplayName")]
        public String Name { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MessagePhone_DisplayName")]
        public String Phone { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MessageAddress_DisplayName")]
        public String Address { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MessageEmail_DisplayName")]
        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$")]
        public String Email { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "TextMessage_DisplayName")]
        public String TextMessage { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AssetCode_DisplayName")]
        public String AssetCode { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Branch_DisplayName")]
        public int? BranchID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Branch_DisplayName")]
        public String BranchName { get; set; }
        public IEnumerable<Branch> BranchList { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "DateTimeCreated_DisplayName")]
        public DateTime DateTimeCreated { get; set; }
        public String CreatedDate { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MessageStatus_DisplayName")]
        public int MessageStatusID { get; set; }
        public IEnumerable<MessageStatus> MessageStatusList { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MessageStatusName_DisplayName")]
        public String MessageStatusName { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Remarks_DisplayName")]
        public string Remarks { get; set; }
        public int isPending { get; set; }
    }
}
