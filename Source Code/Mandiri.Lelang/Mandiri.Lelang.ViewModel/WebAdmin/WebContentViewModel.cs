﻿using FluentValidation.Attributes;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The web content view model
    /// </summary>
    [Validator(typeof(WebContentViewModelValidator))]
    public class WebContentViewModel : BaseViewModel
    {
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "WebContentTypeID_DisplayName")]
        public int WebContentTypeID { get; set; }
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "WebContentType_DisplayName")]
        public String WebContentTypeName { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Content_DisplayName")]
        public String Content { get; set; }
    }
}