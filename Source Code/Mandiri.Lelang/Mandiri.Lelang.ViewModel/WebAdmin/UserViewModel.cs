﻿using FluentValidation.Attributes;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The user view model
    /// </summary>
    [Validator(typeof(UserViewModelValidator))]
    public class UserViewModel : BaseViewModel
    {
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "UserID_DisplayName")]
        public int UserID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Username_DisplayName")]
        public String Username { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Name_DisplayName")]
        public String FullName { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Email_DisplayName")]
        public String Email { get; set; }
        public String Password { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "ConfirmPassword_DisplayName")]
        public String ConfirmPassword { get; set; }        
        [Display(ResourceType = typeof(TextResources), Name = "Branch_DisplayName")]
        public int BranchID { get; set; }
        public IEnumerable<Branch> Branches { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Branch_DisplayName")]
        public String BranchName { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "IsLocked_DisplayName")]
        public bool IsLocked { get; set; }
    }
}