﻿using Mandiri.Lelang.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// Site map view model
    /// </summary>
    public class SiteMapViewModel : BaseViewModel
    {
        public IList<SiteMap> SiteMaps { get; set; }
    }
}
