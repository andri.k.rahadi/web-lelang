﻿using FluentValidation.Attributes;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The home alert view model
    /// </summary>
    public class HomeAlertViewModel : BaseViewModel
    {
        public String AlertMessage { get; set; }
        public String LinkMessage { get; set; }
        public bool IsClickable { get; set; }
        public String ActionName { get; set; }
        public String ControllerName { get; set; }
        public Object RouteValues { get; set; }
    }
}
