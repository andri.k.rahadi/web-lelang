﻿using FluentValidation.Attributes;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The action hall view model
    /// </summary>
    [Validator(typeof(AuctionHallViewModelValidator))]
    public class AuctionHallViewModel : BaseViewModel
    {
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "AuctionHallID_DisplayName")]
        public int AuctionHallID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Name_DisplayName")]
        public String Name { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Description_DisplayName")]
        public String Description { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AuctionHallAddress_DisplayName")]
        public String Address { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Phone1_DisplayName")]
        public String Phone1 { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Phone2_DisplayName")]
        public String Phone2 { get; set; }
    }
}
