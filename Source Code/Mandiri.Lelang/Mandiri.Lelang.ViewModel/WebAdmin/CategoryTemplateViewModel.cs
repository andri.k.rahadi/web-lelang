﻿using FluentValidation.Attributes;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The category template view model
    /// </summary>
    [Validator(typeof(CategoryTemplateViewModelValidator))]
    public class CategoryTemplateViewModel : BaseViewModel
    {
        [Display(ResourceType = typeof(TextResources), Name = "Category_DisplayName")]
        public int CategoryID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Category_DisplayName")]
        public String CategoryName { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "FieldName_DisplayName")]
        public String FieldName { get; set; }
        public String OriginalFieldName { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "IsMandatory_DisplayName")]
        public bool IsMandatory { get; set; }
    }
}
