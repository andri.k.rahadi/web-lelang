﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Attributes;
using Mandiri.Lelang.ViewModel.ModelValidator;
using Mandiri.Lelang.Resources;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The change password view model
    /// </summary>
    [Validator(typeof(ChangePasswordViewModelValidator))]
    public class ChangePasswordViewModel : BaseViewModel
    {
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "UserID_DisplayName")]
        public int UserID { get; set; }
        [Editable(false)]
        public String Username { get; set; }
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "Name_DisplayName")]
        public String FullName { get; set; }
        public String Password { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "ConfirmPassword_DisplayName")]
        public String ConfirmPassword { get; set; }
    }
}
