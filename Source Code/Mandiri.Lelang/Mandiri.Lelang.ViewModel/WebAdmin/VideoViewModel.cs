﻿using FluentValidation.Attributes;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The Video view model
    /// </summary>
    [Validator(typeof(VideoViewModelValidator))]
    public class VideoViewModel : BaseViewModel
    {
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "VideoID_DisplayName")]
        public int VideoID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Title_DisplayName")]
        public string Title { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "ThumbnailPath_DisplayName")]
        public string ThumbnailPath { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Description_DisplayName")]
        public string Description { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "VideoURL_DisplayName")]
        public string VideoURL { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "ShowInHome_DisplayName")]
        public bool ShowInHome { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "DateTimeInActive_DisplayName")]
        public DateTime? DateTimeInActive { get; set; }
    }
}
