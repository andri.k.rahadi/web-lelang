﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel
{
    public class AuctionViewModel : BaseViewModel
    {
        public long AuctionScheduleID { get; set; }
        public String Name { get; set; }
    }
}
