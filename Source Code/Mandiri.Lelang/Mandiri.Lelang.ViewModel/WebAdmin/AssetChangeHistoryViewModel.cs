﻿using FluentValidation.Attributes;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The action hall view model
    /// </summary>
    [Validator(typeof(AssetChangeHistoryViewModelValidator))]
    public class AssetChangeHistoryViewModel : BaseViewModel
    {
        public long AssetChangesID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "FieldName_DisplayName")]
        public String FielName { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "OldValue_DisplayName")]
        public String OldValue { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "NewValue_DisplayName")]
        public String NewValue { get; set; }
    }
}
