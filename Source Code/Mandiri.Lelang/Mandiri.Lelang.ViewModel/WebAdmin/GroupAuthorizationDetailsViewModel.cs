﻿using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The group authorization details view model
    /// </summary>
    public class GroupAuthorizationDetailsViewModel : BaseViewModel
    {
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "SiteMapID_DisplayName")]
        public String SiteMapID { get; set; }
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "Menu_DisplayName")]
        public String Menu { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AllowAccess_DisplayName")]
        public bool AllowAccess { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AllowAdd_DisplayName")]
        public bool AllowAdd { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AllowUpdate_DisplayName")]
        public bool AllowUpdate { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AllowDelete_DisplayName")]
        public bool AllowDelete { get; set; }
    }
}
