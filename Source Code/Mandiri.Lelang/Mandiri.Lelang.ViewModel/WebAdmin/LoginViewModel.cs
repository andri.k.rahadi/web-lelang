﻿using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.ModelValidator;

namespace Mandiri.Lelang.ViewModel.WebAdmin
{
#pragma warning disable 1591
    /// <summary>
    /// The login view model
    /// </summary>
    [Validator(typeof(LoginViewModelValidator))]
    public class LoginViewModel : BaseViewModel
    {
        public String Username { get; set; }
        public String Password { get; set; }
    }
}
