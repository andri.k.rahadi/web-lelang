﻿using FluentValidation.Attributes;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The city view model
    /// </summary>
    [Validator(typeof(GroupAuthorizationViewModelValidator))]
    public class GroupAuthorizationViewModel : BaseViewModel
    {
        [Display(ResourceType = typeof(TextResources), Name = "Group_DisplayName")]
        public int GroupID { get; set; }
        public IEnumerable<Group> Groups { get; set; }
        public IList<GroupAuthorizationDetailsViewModel> GroupAuthorizationDetailList { get; set; }
        public GroupAuthorizationDetailsViewModel GroupAuthorizationDetails { get; set; }
    }
}
