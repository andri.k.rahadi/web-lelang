﻿using FluentValidation.Attributes;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591

    [Validator(typeof(AssetCategoryTemplateViewModelValidator))]
    public class AssetCategoryTemplateViewModel : BaseViewModel
    {
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "TemplateFieldName_DisplayName")]
        public String FieldName { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "TemplateValue_DisplayName")]
        public String Value { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "TemplateValue_DisplayName")]
        public Boolean IsMandatory { get; set; }
        public AssetCategoryTemplateViewModel()
        {
            Value = String.Empty;
        }
        
    }
}
