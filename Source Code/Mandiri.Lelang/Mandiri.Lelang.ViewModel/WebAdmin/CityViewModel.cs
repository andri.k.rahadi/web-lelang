﻿using FluentValidation.Attributes;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The city view model
    /// </summary>
    [Validator(typeof(CityViewModelValidator))]
    public class CityViewModel : BaseViewModel
    {
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "CityID_DisplayName")]
        public int CityID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Province_DisplayName")]
        public int ProvinceID { get; set; }
        public IEnumerable<Province> Provinces { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "ProvinceName_DisplayName")]
        public String ProvinceName { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Name_DisplayName")]
        public String Name { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Description_DisplayName")]
        public String Description { get; set; }  
    }
} 