﻿using FluentValidation.Attributes;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel.ModelValidator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The MessageStatus view model
    /// </summary>
    [Validator(typeof(MessageStatusViewModelValidator))]
    public class MessageStatusViewModel : BaseViewModel
    {
       
        [Editable(false)]
        [Display(ResourceType = typeof(TextResources), Name = "MessageStatusID_DisplayName")]
        public int MessageStatusID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Name_DisplayName")]
        public string Name { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Description_DisplayName")]
        public string Description { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "IsComplete_DisplayName")]
        public bool IsComplete { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "DateTimeInActive_DisplayName")]
        public DateTime? DateTimeInActive { get; set; }
    }
}
