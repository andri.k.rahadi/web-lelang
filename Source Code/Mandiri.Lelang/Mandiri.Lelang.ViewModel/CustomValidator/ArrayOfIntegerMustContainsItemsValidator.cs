﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using FluentValidation.Validators;

namespace Mandiri.Lelang.ViewModel.CustomValidator
{

    public class ArrayOfIntegerMustContainsItemsValidator : PropertyValidator
    {
        public ArrayOfIntegerMustContainsItemsValidator()
            : base("Property {PropertyName} must contains items!")
        {
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var list = context.PropertyValue as int[];

            if (list == null || list.Length <= 0)
            {
                return false;
            }

            return true;
        }
    }
}
