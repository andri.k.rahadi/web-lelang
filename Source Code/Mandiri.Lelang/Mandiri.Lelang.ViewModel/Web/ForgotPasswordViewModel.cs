﻿using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using FluentValidation.Attributes;
using Mandiri.Lelang.ViewModel.ModelValidator;

namespace Mandiri.Lelang.ViewModel.Web
{
    /// <summary>
    /// View model for forgot password
    /// </summary>
    [Validator(typeof(ForgotPasswordViewModelValidator))]
    public class ForgotPasswordViewModel
    {
        [Display(ResourceType = typeof(TextResources), Name = "Email_DisplayName")]
        public String Email { get; set; }       
        public bool IsForgotPasswordSuccess { get; set; }
    }
}
