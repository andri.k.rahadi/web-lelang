﻿using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using FluentValidation.Attributes;
using Mandiri.Lelang.ViewModel.ModelValidator;

namespace Mandiri.Lelang.ViewModel.Web
{
    /// <summary>
    /// View model for reset password
    /// </summary>
    [Validator(typeof(ResetPasswordViewModelValidator))]
    public class ResetPasswordViewModel
    {        
        public String Email { get; set; }        
        public String Token { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Password_DisplayName")]
        public String Password { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "ConfirmPassword_DisplayName")]
        public String ConfirmPassword { get; set; }
        public String ResetPasswordMessage { get; set; }
        public bool IsResetPasswordSuccess { get; set; }
        public bool IsResetPasswordTokenValid { get; set; }
    }
}
