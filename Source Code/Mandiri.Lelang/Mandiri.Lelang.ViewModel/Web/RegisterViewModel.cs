﻿using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using FluentValidation.Attributes;
using Mandiri.Lelang.ViewModel.ModelValidator;

namespace Mandiri.Lelang.ViewModel.Web
{
    /// <summary>
    /// View model for register
    /// </summary>
    [Validator(typeof(RegisterViewModelValidator))]
    public class RegisterViewModel
    {
        [Display(ResourceType = typeof(TextResources), Name = "FullName_DisplayName")]
        public String FullName { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "FullAddress_DisplayName")]
        public String FullAddress { get; set; }
        public bool MandatoryFullAddress { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "DateOfBirth_DisplayName")]
        public DateTime? DateOfBirth { get; set; }
        public bool MandatoryDateOfBirth { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "PlaceOfBirth_DisplayName")]
        public String PlaceOfBirth { get; set; }
        public bool MandatoryPlaceOfBirth { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Email_DisplayName")]
        public String Email { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MobilePhoneNumber_DisplayName")]
        public String MobilePhoneNumber { get; set; }
        public bool MandatoryMobilePhoneNumber { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "IdentificationNumber_DisplayName")]
        public String IdentificationNumber { get; set; }
        public bool MandatoryIdentificationNumber { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Password_DisplayName")]
        public String Password { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "OldPassword_DisplayName")]
        public String OldPassword { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "ConfirmPassword_DisplayName")]
        public String ConfirmPassword { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Province_DisplayName")]
        public int? ProvinceID { get; set; }
        public bool MandatoryProvinceID { get; set; }
        public IEnumerable<Province> Provinces { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "City_DisplayName")]
        public int? CityID { get; set; }
        public bool MandatoryCityID { get; set; }
        public IEnumerable<City> Cities { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Category_DisplayName")]
        public int[] PreferenceCategoryID { get; set; }
        public IEnumerable<Category> PreferenceCategories { get; set; }
        public int SelectAllCategories { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Province_DisplayName")]
        public int[] PreferenceProvinceID { get; set; }
        public IEnumerable<Province> PreferenceProvinces { get; set; }
        public int SelectAllProvinces { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "City_DisplayName")]
        public int[] PreferenceCityID { get; set; }
        public IEnumerable<City> PreferenceCities { get; set; }
        public int SelectAllCities { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MinValue_DisplayName")]
        public int? PreferenceMinPriceID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MaxValue_DisplayName")]
        public int? PreferenceMaxPriceID { get; set; }
        public IEnumerable<MinMaxPrice> PreferenceMinMaxPrices { get; set; }
        public bool MandatoryPreference { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "EmailNotificationMessage_DisplayName")]
        public String EmailNotificationMessage { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "FlagEmailNotification_DisplayName")]
        public bool FlagEmailNotification { get; set; }
        public bool TrueFlagEmailNotification
        {
            get
            {
                return true;
            }
        }
        public bool IsEditProfile { get; set; }
        public bool IsRegisterSuccess { get; set; }
        public String RegisterSuccessMessage { get; set; }
        //[Display(ResourceType = typeof(TextResources), Name = "CaptchaCode_DisplayName")]
        //public String CaptchaCode { get; set; }
    }
}
