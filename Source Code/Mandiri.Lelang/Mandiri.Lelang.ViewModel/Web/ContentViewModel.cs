﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel.Web
{
    /// <summary>
    /// View model for content pages
    /// </summary>
    public class ContentViewModel
    {
        public String Content { get; set; }
    }
}
