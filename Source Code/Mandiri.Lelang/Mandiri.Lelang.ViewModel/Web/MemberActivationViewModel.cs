﻿using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using FluentValidation.Attributes;
using Mandiri.Lelang.ViewModel.ModelValidator;

namespace Mandiri.Lelang.ViewModel.Web
{
    /// <summary>
    /// View model for member activation
    /// </summary>
    public class MemberActivationViewModel
    {
        public bool IsActivationSuccess { get; set; }
        public String ActivationMessage { get; set; }
    }
}
