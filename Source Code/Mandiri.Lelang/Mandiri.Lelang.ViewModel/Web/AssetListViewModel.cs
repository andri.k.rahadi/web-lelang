﻿using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel.Web
{
    /// <summary>
    /// View model for asset list
    /// </summary>
    public class AssetListViewModel
    {
        [Display(ResourceType = typeof(TextResources), Name = "Category_DisplayName")]
        public int CategoryID { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Province_DisplayName")]
        public int ProvinceID { get; set; }
        public IEnumerable<Province> Provinces { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "City_DisplayName")]
        public int CityID { get; set; }
        public IEnumerable<City> Cities { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "OwnershipDocumentType_DisplayName")]
        public int OwnershipDocumentTypeID { get; set; }
        public IEnumerable<OwnershipDocumentType> OwnershipDocumentTypes { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MinValue_DisplayName")]
        public int MinPriceID { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MaxValue_DisplayName")]
        public int MaxPriceID { get; set; }
        public IEnumerable<MinMaxPrice> MinMaxPrices { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "SortBy_DisplayName")]
        public int SortIndex { get; set; }
        public IEnumerable<dynamic> SortBy { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Keywords_DisplayName")]
        public String Keywords { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Address_DisplayName")]
        public String Address { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "HotPrice_DisplayName")]
        public bool HotPrice { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AuctionDate_DisplayName")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? AuctionDate { get; set; }
        public long ScheduleID { get; set; }
        public IList<AssetListingViewModel> Listing { get; set; }
        public long TotalRecords { get; set; }
        public long CurrentPageIndex { get; set; }
        public long TotalPage
        {
            get
            {
                return Convert.ToInt64(Math.Ceiling((double)this.TotalRecords / 12));
            }
        }
        public long StartPage
        {
            get
            {
                if (CurrentPageIndex + 1 >= 6 && (CurrentPageIndex + 1) <= (TotalPage - 5))
                {
                    if ((CurrentPageIndex + 1 - 3) < 1)
                    {
                        return 1;
                    }
                    else
                    {
                        return (CurrentPageIndex + 1 - 3);
                    }
                }
                else if ((CurrentPageIndex + 1) > (TotalPage - 5))
                {
                    if ((CurrentPageIndex + 1 - (6 - (TotalPage - (CurrentPageIndex + 1)))) < 1)
                    {
                        return 1;
                    }
                    else
                    {
                        return (CurrentPageIndex + 1 - (6 - (TotalPage - (CurrentPageIndex + 1))));
                    }
                }
                else
                {
                    return 1;
                }
                //return (((long)Math.Floor((double)CurrentPageIndex / 5)) * 5) + 1;
            }
        }
        public long EndPage
        {
            get
            {
                if (StartPage + 6 > TotalPage)
                {
                    return TotalPage;
                }
                else
                {
                    return StartPage + 6;
                }
            }
        }
        public String AuctionDateText { get; set; }
        public String KPKNLName { get; set; }

        //add for CR2 by khalid 07-09-2017
        [Display(ResourceType = typeof(TextResources), Name = "VolunteerSell_DisplayName")]
        public bool IsVolunteerSellChecked { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Auction_DisplayName")]
        public bool IsAuctionChecked { get; set; }
    }
}
