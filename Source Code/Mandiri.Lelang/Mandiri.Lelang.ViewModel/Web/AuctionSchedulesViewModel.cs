﻿using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel.Web
{
    /// <summary>
    /// View model for auction schedules
    /// </summary>
    public class AuctionSchedulesViewModel
    {
        public IList<AuctionScheduleViewModel> Schedules { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AuctionDateFrom_DisplayName")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? AuctionDateFrom { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AuctionDateTo_DisplayName")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? AuctionDateTo { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "KPKNL_DisplayName")]
        public int[] KPKNLID { get; set; }
        public IEnumerable<KPKNL> KPKNL { get; set; }
        public long TotalRecords { get; set; }
        public long CurrentPageIndex { get; set; }
        public long TotalPage
        {
            get
            {
                return Convert.ToInt64(Math.Ceiling((double)this.TotalRecords / 12));
            }
        }
        public long StartPage
        {
            get
            {
                if (CurrentPageIndex + 1 >= 6 && (CurrentPageIndex + 1) <= (TotalPage - 5))
                {
                    if ((CurrentPageIndex + 1 - 3) < 1)
                    {
                        return 1;
                    }
                    else
                    {
                        return (CurrentPageIndex + 1 - 3);
                    }
                }
                else if ((CurrentPageIndex + 1) > (TotalPage - 5))
                {
                    if ((CurrentPageIndex + 1 - (6 - (TotalPage - (CurrentPageIndex + 1)))) < 1)
                    {
                        return 1;
                    }
                    else
                    {
                        return (CurrentPageIndex + 1 - (6 - (TotalPage - (CurrentPageIndex + 1))));
                    }
                }
                else
                {
                    return 1;
                }
                //return (((long)Math.Floor((double)CurrentPageIndex / 5)) * 5) + 1;
            }
        }
        public long EndPage
        {
            get
            {
                if (StartPage + 6 > TotalPage)
                {
                    return TotalPage;
                }
                else
                {
                    return StartPage + 6;
                }
                /*if (StartPage + 4 > TotalPage)
                {
                    return TotalPage;
                }
                else
                {
                    return StartPage + 4;
                }*/
            }
        }
    }
}
