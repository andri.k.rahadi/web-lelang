﻿using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using FluentValidation.Attributes;
using Mandiri.Lelang.ViewModel.ModelValidator;

namespace Mandiri.Lelang.ViewModel.Web
{
    /// <summary>
    /// View model for login
    /// </summary>
    [Validator(typeof(WebLoginViewModelValidator))]
    public class LoginViewModel
    {
        [Display(ResourceType = typeof(TextResources), Name = "Email_DisplayName")]
        public String Email { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Password_DisplayName")]
        public String Password { get; set; }
        public bool IsLoginSuccess { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "CaptchaCode_DisplayName")]
        public String CaptchaCode { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "RememberMe_DisplayName")]
        public bool RememberMe { get; set; }
    }
}
