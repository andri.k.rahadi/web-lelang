﻿using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using FluentValidation.Attributes;
using Mandiri.Lelang.ViewModel.ModelValidator;

namespace Mandiri.Lelang.ViewModel.Web
{
    /// <summary>
    /// View model for contact us
    /// </summary>
    [Validator(typeof(ContactUsViewModelValidator))]
    public class ContactUsViewModel
    {
        [Display(ResourceType = typeof(TextResources), Name = "Name_DisplayName")]
        public String Name { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Email_DisplayName")]
        public String Email { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "ContactPhone_DisplayName")]
        public String Contact { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Message_DisplayName")]
        public String Message { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "ContactCategory_DisplayName")]
        public byte MessageCategoryTypeID { get; set; }
        public IEnumerable<MessageCategoryType> MessageCategoryTypes { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Branch_DisplayName")]
        public int? BranchID { get; set; }
        public IEnumerable<Branch> Branches { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "AssetCode_DisplayName")]
        public String AssetCode { get; set; }
        public String AddressAndContact { get; set; }
        public bool IsSaveSuccess { get; set; }
    }
}
