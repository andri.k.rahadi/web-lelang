﻿using Mandiri.Lelang.DataModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel.Web
{
    /// <summary>
    /// View model for asset listing and asset details
    /// </summary>
    public class AssetListingViewModel
    {
        private String _emailAuctionURL;

        private String AbbreviateAmount(decimal amount)
        {
            String wording = String.Empty;
            String plainAmount = amount.ToString("n0").Replace(",", "").Replace(".", "");

            if (plainAmount.Length >= 7 && plainAmount.Length < 10)
            {
                amount = amount / 1000000;
                wording = " Juta";
            }
            else if (plainAmount.Length >= 10)
            {
                amount = amount / 1000000000;
                wording = " Milyar";
            }

            amount = Decimal.Round(amount, 2);
            return String.Format("Rp. {0}{1}", amount.ToString("n2").EndsWith("0") ? amount.ToString("n1").EndsWith("0") ? amount.ToString("n0") : amount.ToString("n1") : amount.ToString("n2"), wording);
        }

        public long AssetID { get; set; }
        public String AssetCode { get; set; }
        public int CategoryID { get; set; }
        public String CategoryName { get; set; }
        public DateTime PostedDate { get; set; }
        public DateTime? AuctionDate { get; set; }
        public DateTime? AuctionTime { get; set; }
        public String Timezone { get; set; }
        public decimal? OldPrice { get; set; }
        public decimal? NewPrice { get; set; }
        public String Address { get; set; }
        public int CityID { get; set; }
        public String CityName { get; set; }
        public int ProvinceID { get; set; }
        public String ProvinceName { get; set; }
        public byte CurrentStatusID { get; set; }
        public String KPKNLName { get; set; }
        public String KPKNLAddress { get; set; }
        public String KPKNLPhone1 { get; set; }
        public String KPKNLPhone2 { get; set; }
        public String BranchName { get; set; }
        public String BranchAddress { get; set; }
        public String BranchPhone1 { get; set; }
        public String BranchPhone2 { get; set; }
        public String AuctionHallName { get; set; }
        public String AuctionHallAddress { get; set; }
        public String AuctionHallPhone1 { get; set; }
        public String AuctionHallPhone2 { get; set; }
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
        public String Photo1 { get; set; }
        public String Photo2 { get; set; }
        public String Photo3 { get; set; }
        public String Photo4 { get; set; }
        public String Photo5 { get; set; }
        public String DocumentTypeName { get; set; }
        public String TransferInformation { get; set; }
        public String Terms { get; set; }
        public String FacebookDescription { get; set; }
        public String TwitterText { get; set; }
        public String EmailText { get; set; }
        public String EmailSubject { get; set; }
        public bool IsEmailAuction { get; set; }
        public String EmailAuctionURL
        {
            get
            {
                if (IsEmailAuction)
                {
                    return this._emailAuctionURL;
                }
                else
                {
                    return String.Empty;
                }
            }
            set
            {
                this._emailAuctionURL = value;
            }
        }
        public bool IsFavorite { get; set; }
        //public String EmailUrl { get; set; }
        public IList<AssetCategoryTemplateValue> OtherFields { get; set; }
        public IList<AssetListingViewModel> RelatedAssets { get; set; }
        public String CompleteAddress
        {
            get
            {
                return String.Format("{0}<br/>{1}, {2}", Address, CityName, ProvinceName);
            }
        }
        public String CityAndProvinceText
        {
            get
            {
                return String.Format("{0}, {1}", CityName, ProvinceName);
            }
        }
        public String CompleteAddressText
        {
            get
            {
                return String.Format("{0}, {1}, {2}", Address, CityName, ProvinceName);
            }
        }
        /*public String Photo1URL
        {
            get
            {
                return String.Format("{0}{1}?img=1", ConfigurationManager.AppSettings["PhotoURL"], AssetID.ToString());
            }
        }
        public String Photo2URL
        {
            get
            {
                return String.Format("{0}{1}?img=2", ConfigurationManager.AppSettings["PhotoURL"], AssetID.ToString());
            }
        }
        public String Photo3URL
        {
            get
            {
                return String.Format("{0}{1}?img=3", ConfigurationManager.AppSettings["PhotoURL"], AssetID.ToString());
            }
        }
        public String Photo4URL
        {
            get
            {
                return String.Format("{0}{1}?img=4", ConfigurationManager.AppSettings["PhotoURL"], AssetID.ToString());
            }
        }
        public String Photo5URL
        {
            get
            {
                return String.Format("{0}{1}?img=5", ConfigurationManager.AppSettings["PhotoURL"], AssetID.ToString());
            }
        }*/
        public String NewPriceText
        {
            get
            {
                if (IsPriceCall)
                {
                    return "CALL";
                }
                else if (NewPrice.HasValue && NewPrice.Value > 0)
                {
                    return "Rp. " + NewPrice.Value.ToString("n0").Replace(',', '.');
                }
                else
                {
                    return String.Empty;
                }
            }
        }
        public String AbbreviatedNewPriceText
        {
            get
            {
                if (IsPriceCall && Type.HasValue && Type != 1)
                {
                    return "CALL";
                }
                else if (NewPrice.HasValue && NewPrice.Value > 0 && Type.HasValue)
                {
                    return AbbreviateAmount(NewPrice.Value).Replace(',', '.');
                }
                else
                {
                    return String.Empty;
                }
            }
        }
        public String OldPriceText
        {
            get
            {
                //if (OldPrice.HasValue && OldPrice.Value > 0)
                //{
                if (IsHotPrice)
                {
                    return "Rp. " + OldPrice.Value.ToString("n0").Replace(',', '.');
                }
                else
                {
                    return String.Empty;
                }
                //}
                //else
                //{
                //return String.Empty;
                //}
            }
        }
        public String AbbreviatedOldPriceText
        {
            get
            {
                if (IsHotPrice)
                {
                    return AbbreviateAmount(OldPrice.Value).Replace(',', '.');
                }
                else
                {
                    return String.Empty;
                }
            }
        }
        public bool IsSold
        {
            get
            {
                if (CurrentStatusID == 5)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool IsPaid
        {
            get
            {
                if (CurrentStatusID == 7)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool IsSoldOrPaid
        {
            get
            {
                if (CurrentStatusID == 5 || CurrentStatusID == 7)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool IsHotPrice
        {
            get
            {
                if (OldPrice.HasValue && NewPrice.HasValue && OldPrice.Value >= 0 && NewPrice.Value >= 0)
                {
                    if (NewPrice < OldPrice)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public bool IsPriceCall
        {
            get
            {
                if (NewPrice.HasValue && NewPrice.Value > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public String PostedDateText
        {
            get
            {
                return PostedDate.ToString("dd MMM yyyy HH:mm") + " WIB";
            }
        }
        public String AuctionDateText
        {
            get
            {
                if (AuctionDate.HasValue && AuctionDate.HasValue && AuctionDate.Value >= DateTime.Today)
                {
                    return String.Format("{0} {1} {2}", AuctionDate.Value.ToString("dd MMM yyyy"), AuctionTime.Value.ToString(@"HH\:mm"), Timezone);
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public byte? Type { get; set; } //added for CR2 khalid 04-09-2017
    }
}
