﻿using Mandiri.Lelang.DataModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel.Web
{
    /// <summary>
    /// View model for auction schedule
    /// </summary>
    public class AuctionScheduleViewModel
    {
        public long AuctionScheduleID { get; set; }
        public DateTime? AuctionDate { get; set; }
        public DateTime? AuctionTime { get; set; }
        public String Timezone { get; set; }
        public String KPKNLName { get; set; }
        public String KPKNLAddress { get; set; }
        public String KPKNLPhone1 { get; set; }
        public String KPKNLPhone2 { get; set; }
        public String AuctionDateText
        {
            get
            {
                if (AuctionDate.HasValue && AuctionDate.HasValue && AuctionDate.Value >= DateTime.Today)
                {
                    return String.Format("{0} {1} {2}", AuctionDate.Value.ToString("dd MMM yyyy"), AuctionTime.Value.ToString(@"HH\:mm"), Timezone);
                }
                else
                {
                    return String.Empty;
                }
            }
        }
    }
}
