﻿using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.ViewModel.Web
{
    /// <summary>
    /// View model for home page
    /// </summary>
    public class HomeViewModel
    {
        public String FullName { get; set; }
        public String CarouselImage1URL { get; set; }
        public String CarouselImage2URL { get; set; }
        public String CarouselImage3URL { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Category_DisplayName")]
        public int CategoryID { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Province_DisplayName")]
        public int ProvinceID { get; set; }
        public IEnumerable<Province> Provinces { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "MaxValue_DisplayName")]
        public int MaxPriceID { get; set; }
        public IEnumerable<MinMaxPrice> MaxPrices { get; set; }
        public int CurrentCityID { get; set; }
        public int CurrentProvinceID { get; set; }
        public IList<AssetListingViewModel> NearestAuctionDateListing { get; set; }
        public IList<AssetListingViewModel> HotPriceListing { get; set; }
        public IList<AssetListingViewModel> LatestListing { get; set; }
        public IList<AssetListingViewModel> NearestListing { get; set; }
        public IList<AssetListingViewModel> VolunteerSellDateListing { get; set; }
        public String WebPopupImageSystemSettingID { get; set; }
        public String MobileWebPopupImageSystemSettingID { get; set; }
        public String PopupText { get; set; }
        //add for CR2 by khalid 07-09-2017
        [Display(ResourceType = typeof(TextResources), Name = "VolunteerSell_DisplayName")]
        public bool IsVolunteerSellChecked { get; set; }
        [Display(ResourceType = typeof(TextResources), Name = "Auction_DisplayName")]
        public bool IsAuctionChecked { get; set; }
    }
}
