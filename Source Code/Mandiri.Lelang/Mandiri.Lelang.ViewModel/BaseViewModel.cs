﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.ViewModel
{
#pragma warning disable 1591
    /// <summary>
    /// The base class for all view model
    /// </summary>
    public class BaseViewModel
    {
        public bool IsEditMode { get; set; }
    }
}
