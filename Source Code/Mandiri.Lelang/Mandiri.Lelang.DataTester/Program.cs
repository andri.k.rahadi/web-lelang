﻿using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.DataRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insight.Database;

namespace Mandiri.Lelang.DataTester
{
    class Program
    {
        public static String GenerateAssetCode(String segmentID, int branchID)
        {
            int sequenceDigit = 4;
            int autoNumberingTypeID = 1;
            IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            //Kode alias/wilayan pengelola, kode segmen, tahun input, sequence
            String format = "{0}{1}{2}{3}";
            String assetCode = String.Empty;
            IList<long> runningNumbers;
            long runningNumber = 0;

            Segment segment = context.GetActiveSegmentByID(segmentID);
            Branch branch = context.GetActiveBranchByID(branchID);

            runningNumbers = context.QuerySql<long>("UPDATE AutoNumbering SET RunningNumber = RunningNumber + 1 OUTPUT INSERTED.RunningNumber WHERE AutoNumberingTypeID = @AutoNumberingTypeID AND SegmentID = @SegmentID AND BranchID = @BranchID",
                parameters: new { AutoNumberingTypeID = autoNumberingTypeID, SegmentID = segmentID, BranchID = branchID });

            if (runningNumbers.Count > 0)
            {
                runningNumber = runningNumbers[0];
            }
            assetCode = String.Format(format, branch.Alias.ToUpper(), segment.SegmentID.PadLeft(2, '0'), DateTime.Today.ToString("yy"), runningNumber.ToString().PadLeft(sequenceDigit, '0'));

            return assetCode;
        }

        static void Main(string[] args)
        {
            //IMandiriLelangDataRepository context = MandiriLelangDataContext.Open();
            /*Category category = new Category();
            category.Name = "Rumah";
            category.Description = "Rumah";
            category.CreatedBy = "1";
            category.DateTimeCreated = DateTime.Now;
            category.IsActive = true;
            context.InsertCategory(category);
            Console.WriteLine(category.CategoryID.ToString());*/
            //int recordsAffected = -1;
            //context.DeleteCategoryByID(2);
            //Console.WriteLine(recordsAffected.ToString());
            //context.GetCategoryByID(1);
            GenerateAssetCode("01", 2);
        }
    }
}
