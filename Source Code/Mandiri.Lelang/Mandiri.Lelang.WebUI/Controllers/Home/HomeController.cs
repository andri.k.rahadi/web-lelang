﻿using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.ViewModel.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mandiri.Lelang.WebUI.Controllers.Home
{
    /// <summary>
    /// The home controller
    /// </summary>
    [AllowAnonymous]
    public class HomeController : BaseController
    {
        /// <summary>
        /// The home page
        /// </summary>
        /// <returns>The home or index view</returns>
        [ActionName("index")]
        public ActionResult Index(String city = "")
        {
            HomeViewModel model = new HomeViewModel();
            AssetListingViewModel listing;
            City currentCity;
            IList<Province> provinces = MasterFacade.GetAllActiveProvinces().ReturnData.OrderBy(m => m.Name).ToList();
            provinces.Insert(0, new Province() { ProvinceID = -1, Name = "Semua" });
            model.Provinces = provinces.AsEnumerable();
            model.Categories = MasterFacade.GetAllActiveCategoriesWithAll().ReturnData.AsEnumerable();
            model.MaxPrices = MasterFacade.GetAllActiveMinMaxPricesWithAll().ReturnData.AsEnumerable();
            model.ProvinceID = -1;
            model.CategoryID = -1;
            model.MaxPriceID = -1;
            model.CurrentCityID = -1;
            model.CurrentProvinceID = -1;
            model.PopupText = MasterFacade.GetActiveWebContentByWebContentTypeID(9).ReturnData.Content;
            model.WebPopupImageSystemSettingID = "WebsitePopupImage";
            model.MobileWebPopupImageSystemSettingID = "MobileWebsitePopupImage";
            IList<dynamic> latestListing = AssetFacade.GetAllActiveAssetsByCriteria(isVolunteerSellType: true, isAuctionType: true, isFromHome: true, memberID: SecurityHelper.GetCurrentLoggedInMemberID()).ReturnData;
            IList<dynamic> nearestAuctionDateListing = AssetFacade.GetAllActiveAssetsByCriteria(isVolunteerSellType: false, isAuctionType: true, sortIndex: 3, isFromHome: true, memberID: SecurityHelper.GetCurrentLoggedInMemberID()).ReturnData;
            IList<dynamic> hotPriceListing = AssetFacade.GetAllActiveAssetsByCriteria(isVolunteerSellType: false, isAuctionType: true, hotPrice: true, isFromHome: true, memberID: SecurityHelper.GetCurrentLoggedInMemberID()).ReturnData;
            IList<dynamic> volunteerSellDateListing = AssetFacade.GetAllActiveAssetsByCriteria(isFromHome: true, isVolunteerSellType: true, isAuctionType: false, memberID: SecurityHelper.GetCurrentLoggedInMemberID()).ReturnData;
            
            //added for CR2 by khalid 07-09-2017
            model.IsAuctionChecked = true;
            model.IsVolunteerSellChecked = true;
            //Nearest Listing
            if (!String.IsNullOrWhiteSpace(city))
            {
                model.CurrentCityID = 0;
                model.CurrentProvinceID = 0;
                currentCity = MasterFacade.GetActiveCityByName(city).ReturnData;
                if (currentCity != null)
                {
                    model.CurrentCityID = currentCity.CityID.Value;
                    model.CurrentProvinceID = currentCity.ProvinceID.Value;
                    IList<dynamic> nearestListing = AssetFacade.GetAllActiveAssetsByCriteria(isVolunteerSellType: true, isAuctionType: true, cityID: currentCity.CityID.Value, provinceID: currentCity.ProvinceID.Value, isFromHome: true, memberID: SecurityHelper.GetCurrentLoggedInMemberID()).ReturnData;
                    model.NearestListing = new List<AssetListingViewModel>();
                    foreach (dynamic l in nearestListing)
                    {
                        //If price is not CALL
                        if (l.NewPrice > 0 || l.Type == 1)
                        {
                            listing = new AssetListingViewModel();
                            listing.AssetID = l.AssetID;
                            listing.Address = l.Address;
                            listing.CategoryName = l.CategoryName;
                            listing.OldPrice = l.OldPrice;
                            listing.NewPrice = l.NewPrice;
                            listing.AuctionDate = l.ScheduleDate;
                            listing.AuctionTime = l.ScheduleTime;
                            listing.PostedDate = l.DateTimeCreated;
                            listing.Timezone = l.Timezone;
                            listing.CityName = l.CityName;
                            listing.ProvinceName = l.ProvinceName;
                            listing.CurrentStatusID = l.CurrentStatusID;
                            listing.IsEmailAuction = l.IsEmailAuction;
                            listing.IsFavorite = l.IsFavorite;
                            listing.Type = l.Type;
                            model.NearestListing.Add(listing);
                        }
                    }
                }
            }
            else
            {
                model.NearestListing = new List<AssetListingViewModel>();
            }
            model.HotPriceListing = new List<AssetListingViewModel>();
            foreach (dynamic l in hotPriceListing)
            {
                //If price is not CALL
                //if (l.NewPrice > 0)
                //{
                listing = new AssetListingViewModel();
                listing.AssetID = l.AssetID;
                listing.Address = l.Address;
                listing.CategoryName = l.CategoryName;
                listing.OldPrice = l.OldPrice;
                listing.NewPrice = l.NewPrice;
                listing.AuctionDate = l.ScheduleDate;
                listing.AuctionTime = l.ScheduleTime;
                listing.PostedDate = l.DateTimeCreated;
                listing.Timezone = l.Timezone;
                listing.CityName = l.CityName;
                listing.ProvinceName = l.ProvinceName;
                listing.CurrentStatusID = l.CurrentStatusID;
                listing.IsEmailAuction = l.IsEmailAuction;
                listing.IsFavorite = l.IsFavorite;
                listing.Type = l.Type;
                model.HotPriceListing.Add(listing);
                //}
            }
            model.LatestListing = new List<AssetListingViewModel>();
            foreach (dynamic l in latestListing)
            {
                //If price is not CALL
                //if (l.NewPrice > 0)
                //{
                listing = new AssetListingViewModel();
                listing.AssetID = l.AssetID;
                listing.Address = l.Address;
                listing.CategoryName = l.CategoryName;
                listing.OldPrice = 0;//l.OldPrice;
                listing.NewPrice = l.NewPrice;
                listing.AuctionDate = l.ScheduleDate;
                listing.AuctionTime = l.ScheduleTime;
                listing.PostedDate = l.DateTimeCreated;
                listing.Timezone = l.Timezone;
                listing.CityName = l.CityName;
                listing.ProvinceName = l.ProvinceName;
                listing.CurrentStatusID = l.CurrentStatusID;
                listing.IsEmailAuction = l.IsEmailAuction;
                listing.IsFavorite = l.IsFavorite;
                listing.Type = l.Type;
                model.LatestListing.Add(listing);
                //}
            }
            model.NearestAuctionDateListing = new List<AssetListingViewModel>();
            foreach (dynamic l in nearestAuctionDateListing)
            {
                //If price is not CALL
                //if (l.NewPrice > 0)
                //{
                listing = new AssetListingViewModel();
                listing.AssetID = l.AssetID;
                listing.Address = l.Address;
                listing.CategoryName = l.CategoryName;
                listing.OldPrice = l.OldPrice;
                listing.NewPrice = l.NewPrice;
                listing.AuctionDate = l.ScheduleDate;
                listing.AuctionTime = l.ScheduleTime;
                listing.PostedDate = l.DateTimeCreated;
                listing.Timezone = l.Timezone;
                listing.CityName = l.CityName;
                listing.ProvinceName = l.ProvinceName;
                listing.CurrentStatusID = l.CurrentStatusID;
                listing.IsEmailAuction = l.IsEmailAuction;
                listing.IsFavorite = l.IsFavorite;
                listing.Type = l.Type;
                model.NearestAuctionDateListing.Add(listing);
                //}
            }
            model.VolunteerSellDateListing = new List<AssetListingViewModel>();
            foreach (dynamic l in volunteerSellDateListing)
            {
                //If price is not CALL
                //if (l.NewPrice > 0)
                //{
                listing = new AssetListingViewModel();
                listing.AssetID = l.AssetID;
                listing.Address = l.Address;
                listing.CategoryName = l.CategoryName;
                listing.OldPrice = l.OldPrice;
                listing.NewPrice = l.NewPrice;
                listing.AuctionDate = l.ScheduleDate;
                listing.AuctionTime = l.ScheduleTime;
                listing.PostedDate = l.DateTimeCreated;
                listing.Timezone = l.Timezone;
                listing.CityName = l.CityName;
                listing.ProvinceName = l.ProvinceName;
                listing.CurrentStatusID = l.CurrentStatusID;
                listing.IsEmailAuction = l.IsEmailAuction;
                listing.IsFavorite = l.IsFavorite;
                listing.Type = l.Type;
                model.VolunteerSellDateListing.Add(listing);
                //}
            }
            return View(model);
        }
    }
}
