﻿using Mandiri.Lelang.Attribute.ActionFilter;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.ViewModel.Web;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mandiri.Lelang.Helper.Extension;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Helper;
using System.Globalization;
//added for CR2 by khalid 08-09-2017
using System.Net;
using System.Net.Mail;
using System.Configuration;
using System.Text.RegularExpressions;

namespace Mandiri.Lelang.WebUI.Controllers.Contact
{
    /// <summary>
    /// The contact us controller
    /// </summary>
    [AllowAnonymous]
    public class ContactController : BaseController
    {
        /// <summary>
        /// The contact us form
        /// </summary>
        /// <returns>The asset list view</returns>
        [ActionName("contact-us")]
        public ActionResult ContactForm()
        {
            ContactUsViewModel model;
            if (TempData["ContactModel"] != null)
            {
                model = (ContactUsViewModel)TempData["ContactModel"];
            }
            else
            {
                model = new ContactUsViewModel();
            }
            if (TempData["ViewData"] != null)
            {
                //Keep error messages
                ViewData = (ViewDataDictionary)TempData["ViewData"];
            }
            DataModel.Asset asset = null;
            model.MessageCategoryTypes = MasterFacade.GetAllActiveMessageCategoryTypes().ReturnData.AsEnumerable();
            model.Branches = MasterFacade.GetAllActiveBranches().ReturnData.AsEnumerable();
            model.AddressAndContact = MasterFacade.GetActiveWebContentByWebContentTypeID(7).ReturnData.Content;

            if (Request.UrlReferrer != null)
            {
                String urlReferrer = Request.UrlReferrer.PathAndQuery.ToLower();
                if (urlReferrer.Contains("asset/detail"))
                {
                    String assetID = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
                    if (assetID != null)
                    {
                        asset = AssetFacade.GetActiveAssetByAssetID(long.Parse(assetID)).ReturnData;
                    }
                }
            }

            if (asset != null)
            {
                model.BranchID = asset.BranchID;
                model.AssetCode = asset.AssetCode;
            }

            if (SecurityHelper.IsAuthenticated())
            {
                var member = MemberFacade.GetActiveMemberByID(SecurityHelper.GetCurrentLoggedInMemberID().Value).ReturnData;
                model.Email = member.Email;
                model.Name = member.FullName;
            }

            return View("ContactForm", model);
        }

        [HttpPost]
        [ValidateSQLInjection]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitContactForm(ContactUsViewModel model)
        {
            Message message;
            FacadeResult<Message> result;

            model.MessageCategoryTypes = MasterFacade.GetAllActiveMessageCategoryTypes().ReturnData.AsEnumerable();
            model.Branches = MasterFacade.GetAllActiveBranches().ReturnData.AsEnumerable();
            model.AddressAndContact = MasterFacade.GetActiveWebContentByWebContentTypeID(7).ReturnData.Content;
            if (ModelState.IsValid)
            {
                message = new Message();
                message.Name = model.Name;
                message.Email = model.Email;
                message.Phone = model.Contact;
                message.TextMessage = model.Message;
                message.MessageCategoryTypeID = model.MessageCategoryTypeID;
                message.AssetCode = model.AssetCode;
                message.BranchID = model.BranchID;
                message.MessageStatusID = 1;
                //Try getting the IP Address
                try
                {
                    //The X-Forwarded-For (XFF) HTTP header field is a de facto standard for identifying the originating IP address of a 
                    //client connecting to a web server through an HTTP proxy or load balancer
                    /*String ip = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (string.IsNullOrEmpty(ip))
                    {
                        ip = Request.UserHostAddress;
                        if (string.IsNullOrEmpty(ip))
                        {
                            ip = Request.ServerVariables["REMOTE_ADDR"];
                        }
                    }
                    message.IPAddress = ip;*/
                    message.IPAddress = Session.SessionID;
                }
                catch
                {
                    message.IPAddress = String.Empty;
                }

                result = ContactFacade.AddNewMessage(message);

                ModelState.Clear();

                model.Name = String.Empty;
                model.Email = String.Empty;
                model.Contact = String.Empty;
                model.Message = String.Empty;
                model.MessageCategoryTypeID = 0;
                model.BranchID = 0;
                model.AssetCode = String.Empty;
                model.IsSaveSuccess = result.IsSuccess;

                if (!result.IsSuccess)
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            else
            {
                ModelState.SetModelValue("Name", new ValueProviderResult(String.Empty, String.Empty, CultureInfo.InvariantCulture));
                ModelState.SetModelValue("Email", new ValueProviderResult(String.Empty, String.Empty, CultureInfo.InvariantCulture));
                ModelState.SetModelValue("Contact", new ValueProviderResult(String.Empty, String.Empty, CultureInfo.InvariantCulture));
                ModelState.SetModelValue("Message", new ValueProviderResult(String.Empty, String.Empty, CultureInfo.InvariantCulture));
                ModelState.SetModelValue("MessageCategoryTypeID", new ValueProviderResult(0, String.Empty, CultureInfo.InvariantCulture));
                ModelState.SetModelValue("BranchID", new ValueProviderResult(0, String.Empty, CultureInfo.InvariantCulture));
                ModelState.SetModelValue("AssetCode", new ValueProviderResult(String.Empty, String.Empty, CultureInfo.InvariantCulture));
                ModelState.SetModelValue("IsSaveSuccess", new ValueProviderResult(false, String.Empty, CultureInfo.InvariantCulture));
            }
            TempData["ContactModel"] = model;
            TempData["ViewData"] = ViewData;
            return RedirectToAction("contact-us", "contact");
            //return View("ContactForm", model);
        }
    }
}
