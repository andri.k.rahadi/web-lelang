﻿using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mandiri.Lelang.WebUI.Controllers.Master
{
    [AllowAnonymous]
    public class MasterController : BaseController
    {
        [HttpPost]
        [ActionName("ajax_city")]
        public ActionResult GetCitiesByProvinceIDs(List<int> provinceIDs)
        {
            var result = new List<dynamic>();
            var cities = MasterFacade.GetAllActiveCitiesByProvinceIDs(provinceIDs.AsEnumerable());
            return Json(new { isSuccess = true, data = cities.ReturnData.OrderBy(m => m.Name).ToArray() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName("ajax_city_single")]
        public ActionResult GetCitiesByProvinceIDsWithPlaceholder(List<int> provinceIDs)
        {
            var result = new List<dynamic>();
            IList<City> cities = new List<City>();
            cities = MasterFacade.GetAllActiveCitiesByProvinceIDs(provinceIDs.AsEnumerable()).ReturnData;
            cities.Insert(0, new City() { CityID = null, Name = "(Pilih Kota)" });
            return Json(new { isSuccess = true, data = cities.OrderBy(m => m.Name).ToArray() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName("ajax_city_single_with_all")]
        public ActionResult GetCitiesByProvinceIDsWithAllPlaceholder(List<int> provinceIDs)
        {
            var result = new List<dynamic>();
            IList<City> cities = new List<City>();
            if (provinceIDs.Count == 1 && provinceIDs[0] == -1)
            {
                cities = MasterFacade.GetAllActiveCities().ReturnData;
            }
            else
            {
                cities = MasterFacade.GetAllActiveCitiesByProvinceIDs(provinceIDs.AsEnumerable()).ReturnData;
            }
            cities = cities.OrderBy(m => m.Name).ToList();
            cities.Insert(0, new City() { CityID = -1, Name = "Semua" });
            return Json(new { isSuccess = true, data = cities.ToArray() }, JsonRequestBehavior.AllowGet);
        }
    }
}