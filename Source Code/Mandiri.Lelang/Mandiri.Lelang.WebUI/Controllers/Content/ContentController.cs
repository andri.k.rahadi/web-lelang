﻿using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.ViewModel.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mandiri.Lelang.WebUI.Controllers.Content
{
    /// <summary>
    /// The home controller
    /// </summary>
    [AllowAnonymous]
    public class ContentController : BaseController
    {
        /// <summary>
        /// The terms and conditions page
        /// </summary>
        /// <returns>The terms and conditions view</returns>
        [ActionName("terms")]
        public ActionResult TermsAndConditions(int mobile = 0)
        {
            ContentViewModel model = new ContentViewModel();
            model.Content = MasterFacade.GetActiveWebContentByWebContentTypeID(1).ReturnData.Content;
            if (mobile == 1)
            {
                return View("MobileTermsAndConditions", model);
            }
            else
            {
                return View("TermsAndConditions", model);
            }
        }

        /// <summary>
        /// The faq page
        /// </summary>
        /// <returns>The faq view</returns>
        [ActionName("faq")]
        public ActionResult FAQ(int mobile = 0)
        {
            ContentViewModel model = new ContentViewModel();
            model.Content = MasterFacade.GetActiveWebContentByWebContentTypeID(2).ReturnData.Content;
            if (mobile == 1)
            {
                return View("MobileFAQ", model);
            }
            else
            {
                return View("FAQ", model);
            }
        }

        /// <summary>
        /// The about us page
        /// </summary>
        /// <returns>The about us view</returns>
        [ActionName("about")]
        public ActionResult AboutUs(int mobile = 0)
        {
            ContentViewModel model = new ContentViewModel();
            model.Content = MasterFacade.GetActiveWebContentByWebContentTypeID(3).ReturnData.Content;
            if (mobile == 1)
            {
                return View("MobileAboutUs", model);
            }
            else
            {
                return View("AboutUs", model);
            }
        }

        /// <summary>
        /// The video page
        /// </summary>
        /// <returns>The video view</returns>
        [ActionName("video")]
        public ActionResult Video(int mobile = 0)
        {
            ContentViewModel model = new ContentViewModel();
            model.Content = MasterFacade.GetActiveWebContentByWebContentTypeID(3).ReturnData.Content;
            if (mobile == 1)
            {
                return View("MobileVideo", model);
            }
            else
            {
                return View("Video", model);
            }
        }
    }
}
