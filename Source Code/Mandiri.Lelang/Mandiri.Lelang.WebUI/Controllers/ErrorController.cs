﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mandiri.Lelang.WebUI.Controllers
{
    /// <summary>
    /// The error controller
    /// </summary>
    [AllowAnonymous]
    public class ErrorController : BaseController
    {
        /// <summary>
        /// The error page
        /// </summary>
        /// <returns>The error view</returns>
        public ActionResult ErrorPage()
        {
            return View();
        }

        /// <summary>
        /// The not authorized page
        /// </summary>
        /// <returns>The not authorized view</returns>
        public ActionResult NotAuthorized()
        {
            //return View("ErrorPage");
            return View();
        }
    }
}
