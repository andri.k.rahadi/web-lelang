﻿using Mandiri.Lelang.Attribute.ActionFilter;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.ViewModel.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mandiri.Lelang.WebUI.Controllers.Asset
{
    /// <summary>
    /// The asset controller
    /// </summary>
    [AllowAnonymous]
    public class AssetController : BaseController
    {
        private IEnumerable<SelectListItem> GetSortByOptions()
        {
            List<SelectListItem> sortOptions = new List<SelectListItem>();
            SelectListItem sortBy;
            sortBy = new SelectListItem();
            sortBy.Value = "0";
            sortBy.Text = "Terbaru";
            sortOptions.Add(sortBy);

            sortBy = new SelectListItem();
            sortBy.Value = "1";
            sortBy.Text = "Harga Tertinggi";
            sortOptions.Add(sortBy);

            sortBy = new SelectListItem();
            sortBy.Value = "2";
            sortBy.Text = "Harga Terendah";
            sortOptions.Add(sortBy);

            sortBy = new SelectListItem();
            sortBy.Value = "3";
            sortBy.Text = "Segera Dilelang";
            sortOptions.Add(sortBy);

            return sortOptions.AsEnumerable();
        }

        /// <summary>
        /// The asset list
        /// </summary>
        /// <returns>The asset list view</returns>
        /*[ActionName("search")]
        [ValidateSQLInjection]
        public ActionResult AssetList(String keywords = "", bool hotPrice = false, int sort = 0, int categoryID = -1)
        {
            AssetListViewModel model = new AssetListViewModel();
            AssetListingViewModel listing;
            IList<Province> provinces = MasterFacade.GetAllActiveProvinces().ReturnData.OrderBy(m => m.Name).ToList();
            IList<City> cities = MasterFacade.GetAllActiveCities().ReturnData.OrderBy(m => m.Name).ToList();
            //model.Provinces = MasterFacade.GetAllActiveProvincesWithAll().ReturnData.AsEnumerable();
            provinces.Insert(0, new Province() { ProvinceID = -1, Name = "Semua" });
            model.Provinces = provinces.AsEnumerable();
            model.Categories = MasterFacade.GetAllActiveCategoriesWithAll().ReturnData.AsEnumerable();
            model.MinMaxPrices = MasterFacade.GetAllActiveMinMaxPricesWithAll().ReturnData.AsEnumerable();
            model.OwnershipDocumentTypes = MasterFacade.GetAllActiveOwnershipDocumentTypesWithAll().ReturnData.AsEnumerable();
            //model.Cities = MasterFacade.GetAllActiveCitiesWithAll().ReturnData.AsEnumerable();
            cities.Insert(0, new City() { CityID = -1, Name = "Semua" });
            model.Cities = cities.AsEnumerable();
            model.SortBy = GetSortByOptions();
            model.ProvinceID = -1;
            model.CategoryID = categoryID;
            model.MinPriceID = -1;
            model.MaxPriceID = -1;
            model.CityID = -1;
            model.SortIndex = sort;
            model.Keywords = keywords.Trim();
            model.OwnershipDocumentTypeID = -1;
            model.Address = String.Empty;
            model.HotPrice = hotPrice;
            model.TotalRecords = AssetFacade.GetAllActiveAssetsByCriteriaCount(sortIndex: sort, hotPrice: hotPrice, categoryID: categoryID, keywords: keywords.Trim()).ReturnData;
            model.CurrentPageIndex = 0;
            IList<dynamic> listingData = AssetFacade.GetAllActiveAssetsByCriteria(sortIndex: sort, hotPrice: hotPrice, categoryID: categoryID, keywords: keywords.Trim()).ReturnData;
            model.Listing = new List<AssetListingViewModel>();
            foreach (dynamic l in listingData)
            {
                listing = new AssetListingViewModel();
                listing.AssetID = l.AssetID;
                listing.Address = l.Address;
                listing.CategoryName = l.CategoryName;
                listing.OldPrice = l.OldPrice;
                listing.NewPrice = l.NewPrice;
                listing.AuctionDate = l.ScheduleDate;
                listing.AuctionTime = l.ScheduleTime;
                listing.PostedDate = l.DateTimeCreated;
                listing.Timezone = l.Timezone;
                listing.CityName = l.CityName;
                listing.ProvinceName = l.ProvinceName;
                listing.CurrentStatusID = l.CurrentStatusID;
                model.Listing.Add(listing);
            }
            return View("AssetList", model);
        }*/

        /// <summary>
        /// The post asset list (for SEO friendly)
        /// </summary>
        /// <returns>The asset list view</returns>
        [HttpPost]
        [ActionName("post-search")]
        [ValidateSQLInjection]
        public ActionResult PostAssetList(DateTime? auctionDate = null, int categoryID = -1, int provinceID = -1, int minPriceID = -1, int maxPriceID = -1, int cityID = -1, int ownershipDocumentTypeID = -1, int sortIndex = 0, bool hotPrice = false, String address = "", String keywords = "", String page = "1", long currentPageIndex = 0, bool isVolunteerSellChecked = false, bool isAuctionChecked = false)
        {
            keywords = String.IsNullOrWhiteSpace(keywords) ? "*" : keywords;
            address = String.IsNullOrWhiteSpace(address) ? "*" : address;
            String strAuctionDate = auctionDate.HasValue ? auctionDate.Value.ToString("dd-MM-yyyy") : "*";
            Category category = MasterFacade.GetActiveCategoryByCategoryID(categoryID).ReturnData;
            MinMaxPrice minPrice = MasterFacade.GetActiveMinMaxPriceByMinMaxPriceID(minPriceID).ReturnData;
            MinMaxPrice maxPrice = MasterFacade.GetActiveMinMaxPriceByMinMaxPriceID(maxPriceID).ReturnData;
            City city = MasterFacade.GetActiveCityByCityID(cityID).ReturnData;
            Province province = MasterFacade.GetActiveProvinceByProvinceID(provinceID).ReturnData;
            OwnershipDocumentType document = MasterFacade.GetActiveOwnershipDocumentTypeByOwnershipDocumentTypeID(ownershipDocumentTypeID).ReturnData;
            return RedirectToRoute("SearchAsset", new { controller = "asset", action = "search", categoryID = categoryID, categoryName = category != null ? category.Name.ToLower().Replace(" & ", "-").Replace(" ", "-") : "semua", provinceID = provinceID, provinceName = province != null ? province.Name.ToLower().Replace(" & ", "-").Replace(" ", "-") : "semua", cityID = cityID, cityName = city != null ? city.Name.ToLower().Replace(" & ", "-").Replace(" ", "-") : "semua", minPriceID = minPriceID, minPriceName = minPrice != null ? minPrice.Name.ToLower().Replace(" & ", "-").Replace(" ", "-") : "semua", maxPriceID = maxPriceID, maxPriceName = maxPrice != null ? maxPrice.Name.ToLower().Replace(" & ", "-").Replace(" ", "-") : "semua", ownershipDocumentTypeID = ownershipDocumentTypeID, ownershipDocumentTypeName = document != null ? document.Name.ToLower().Replace(" & ", "-").Replace(" ", "-") : "semua", address = address.ToLower(), auctionDate = strAuctionDate, hotPrice = hotPrice ? "ya" : "*", keywords = keywords.ToLower(), page = page, sortIndex = sortIndex, currentPageIndex = currentPageIndex, isVolunteerSellChecked = isVolunteerSellChecked, isAuctionChecked = isAuctionChecked });
        }

        /// <summary>
        /// The asset list
        /// </summary>
        /// <returns>The asset list view</returns>        
        [ActionName("search")]
        [ValidateSQLInjection]
        public ActionResult AssetList(String auctionDate = "", int categoryID = -1, int provinceID = -1, int minPriceID = -1, int maxPriceID = -1, int cityID = -1, int ownershipDocumentTypeID = -1, int sortIndex = 0, String hotPrice = "*", String address = "", String keywords = "", String page = "1", long currentPageIndex = 0, bool isVolunteerSellChecked = true, bool isAuctionChecked = true)
        {
            AssetListViewModel model = new AssetListViewModel();
            AssetListingViewModel listing;
            bool bHotPrice = String.IsNullOrWhiteSpace(hotPrice) || hotPrice.Trim() == "*" ? false : true;
            DateTime? dtAuctionDate = String.IsNullOrWhiteSpace(auctionDate) || auctionDate.Trim() == "*" ? null : (DateTime?)DateTime.ParseExact(auctionDate, "dd-MM-yyyy", null);
            address = String.IsNullOrWhiteSpace(address) || address.Trim() == "*" ? String.Empty : address;
            keywords = String.IsNullOrWhiteSpace(keywords) || keywords.Trim() == "*" ? String.Empty : keywords;
            
            IList<Province> provinces = MasterFacade.GetAllActiveProvinces().ReturnData.OrderBy(m => m.Name).ToList();
            IList<City> cities = MasterFacade.GetAllActiveCities().ReturnData.OrderBy(m => m.Name).ToList();
            provinces.Insert(0, new Province() { ProvinceID = -1, Name = "Semua" });
            model.Provinces = provinces.AsEnumerable();
            cities.Insert(0, new City() { CityID = -1, Name = "Semua" });
            model.Cities = cities.AsEnumerable();
            //model.Provinces = MasterFacade.GetAllActiveProvincesWithAll().ReturnData.AsEnumerable();
            model.Categories = MasterFacade.GetAllActiveCategoriesWithAll().ReturnData.AsEnumerable();
            model.MinMaxPrices = MasterFacade.GetAllActiveMinMaxPricesWithAll().ReturnData.AsEnumerable();
            model.OwnershipDocumentTypes = MasterFacade.GetAllActiveOwnershipDocumentTypesWithAll().ReturnData.AsEnumerable();
            //model.Cities = MasterFacade.GetAllActiveCitiesWithAll().ReturnData.AsEnumerable();
            model.SortBy = GetSortByOptions();
            model.ProvinceID = provinceID;
            model.CategoryID = categoryID;
            model.MinPriceID = minPriceID;
            model.MaxPriceID = maxPriceID;
            model.CityID = cityID;
            model.SortIndex = sortIndex;
            model.Keywords = keywords.Trim();
            model.OwnershipDocumentTypeID = ownershipDocumentTypeID;
            model.Address = address.Trim();
            model.HotPrice = bHotPrice;
            model.AuctionDate = dtAuctionDate;

            //add for CR2 by khalid 07-09-2017
            model.IsAuctionChecked = isAuctionChecked;
            model.IsVolunteerSellChecked = isVolunteerSellChecked;

            //Set record information for pagination
            model.TotalRecords = AssetFacade.GetAllActiveAssetsByCriteriaCount(provinceID: provinceID, cityID: cityID, address: address, auctionDate: dtAuctionDate, documentTypeID: ownershipDocumentTypeID, minPriceID: minPriceID, maxPriceID: maxPriceID, sortIndex: sortIndex, hotPrice: bHotPrice, categoryID: categoryID, keywords: keywords.Trim(), memberID: SecurityHelper.GetCurrentLoggedInMemberID(), isVolunteerSellType: isVolunteerSellChecked, isAuctionType: isAuctionChecked).ReturnData;
            if (page == "next")
            {
                if ((currentPageIndex + 1) < model.TotalPage)
                {
                    currentPageIndex++;
                }
            }
            else if (page == "prev")
            {
                if (currentPageIndex > 0)
                {
                    currentPageIndex--;
                }
            }
            else
            {
                currentPageIndex = long.Parse(page) - 1;
            }
            model.CurrentPageIndex = currentPageIndex;

            IList<dynamic> listingData = AssetFacade.GetAllActiveAssetsByCriteria(provinceID: provinceID, cityID: cityID, address: address, auctionDate: dtAuctionDate, documentTypeID: ownershipDocumentTypeID, minPriceID: minPriceID, maxPriceID: maxPriceID, sortIndex: sortIndex, hotPrice: bHotPrice, categoryID: categoryID, keywords: keywords.Trim(), pageIndex: currentPageIndex, memberID: SecurityHelper.GetCurrentLoggedInMemberID(), isVolunteerSellType: isVolunteerSellChecked, isAuctionType: isAuctionChecked).ReturnData;
            model.Listing = new List<AssetListingViewModel>();
            foreach (dynamic l in listingData)
            {
                listing = new AssetListingViewModel();
                listing.AssetID = l.AssetID;
                listing.Address = l.Address;
                listing.CategoryName = l.CategoryName;
                listing.OldPrice = l.OldPrice;
                listing.NewPrice = l.NewPrice;
                listing.AuctionDate = l.ScheduleDate;
                listing.AuctionTime = l.ScheduleTime;
                listing.PostedDate = l.DateTimeCreated;
                listing.Timezone = l.Timezone;
                listing.CityName = l.CityName;
                listing.ProvinceName = l.ProvinceName;
                listing.CurrentStatusID = l.CurrentStatusID;
                listing.IsEmailAuction = l.IsEmailAuction;
                listing.IsFavorite = l.IsFavorite;
                listing.Type = l.Type;

                model.Listing.Add(listing);
            }

            ModelState.Clear();
            return View("AssetList", model);
        }

        /// <summary>
        /// The asset details
        /// </summary>
        /// <returns>The asset details view</returns>
        [ActionName("detail")]
        [ValidateSQLInjection]
        public ActionResult AssetDetail(long id, int print = 0)
        {
            AssetListingViewModel model = new AssetListingViewModel();
            AssetListingViewModel listing;
            IList<AssetCategoryTemplateValue> customFields;

            dynamic assetDetails = AssetFacade.GetActiveAssetDetailsByAssetID(id, SecurityHelper.GetCurrentLoggedInMemberID()).ReturnData;
            if (assetDetails != null)
            {
                model.AssetID = assetDetails.AssetID;
                model.AssetCode = assetDetails.AssetCode;
                model.CategoryName = assetDetails.CategoryName;
                model.OldPrice = assetDetails.OldPrice;
                model.NewPrice = assetDetails.NewPrice;
                model.Address = assetDetails.Address;
                model.CityName = assetDetails.CityName;
                model.ProvinceName = assetDetails.ProvinceName;
                model.IsEmailAuction = assetDetails.IsEmailAuction;
                model.EmailAuctionURL = assetDetails.EmailAuctionURL;
                model.IsFavorite = assetDetails.IsFavorite;
                model.Photo1 = assetDetails.Photo1;
                model.Photo2 = assetDetails.Photo2;
                model.Photo3 = assetDetails.Photo3;
                model.Photo4 = assetDetails.Photo4;
                model.Photo5 = assetDetails.Photo5;
                model.PostedDate = assetDetails.DateTimeCreated;
                model.CurrentStatusID = assetDetails.CurrentStatusID;
                model.Latitude = (float?)assetDetails.Latitude;
                model.Longitude = (float?)assetDetails.Longitude;
                //add for CR2 By Khalid 06-09-2017
                model.Type = assetDetails.Type;

                //model.OtherFields = AssetFacade.GetAllActiveAssetCategoryTemplateValuesByAssetID(id).ReturnData;
                //TODO: Do not add the template, temp fix to support template changes from luas tanah to luas tanah (m2) and luas bangunan to luas bangunan (m2)
                customFields = AssetFacade.GetAllActiveAssetCategoryTemplateValuesByAssetID(id).ReturnData;
                model.OtherFields = new List<AssetCategoryTemplateValue>();
                foreach (AssetCategoryTemplateValue customField in customFields)
                {
                    if ((customField.FieldName.ToUpper().Trim() == "LUAS TANAH" && customFields.Count(m => m.FieldName.ToUpper().Trim().Contains("LUAS TANAH (M2)")) > 0)
                        || (customField.FieldName.ToUpper().Trim() == "LUAS BANGUNAN" && customFields.Count(m => m.FieldName.ToUpper().Trim().Contains("LUAS BANGUNAN (M2)")) > 0))
                    {
                        //TODO: Do not add the template, temp fix to support template changes from luas tanah to luas tanah (m2) and luas bangunan to luas bangunan (m2)
                    }
                    else
                    {
                        model.OtherFields.Add(customField);
                    }
                }

                model.TransferInformation = assetDetails.TransferInformation;
                model.Timezone = assetDetails.Timezone;
                model.AuctionDate = assetDetails.ScheduleDate;
                model.AuctionTime = assetDetails.ScheduleTime;
                model.DocumentTypeName = assetDetails.DocumentTypeName;
                model.KPKNLName = assetDetails.KPKNLName;
                //model.KPKNLAddress = assetDetails.KPKNLAddress;
                //Change KPKNL Address with Auction Address
                model.KPKNLAddress = assetDetails.AuctionAddress;
                model.KPKNLPhone1 = assetDetails.KPKNLPhone1;
                model.KPKNLPhone2 = assetDetails.KPKNLPhone2;

                model.BranchName = assetDetails.BranchName;
                model.BranchAddress = assetDetails.BranchAddress;
                model.BranchPhone1 = assetDetails.BranchPhone1;
                model.BranchPhone2 = assetDetails.BranchPhone2;

                model.AuctionHallName = assetDetails.AuctionHallName;
                model.AuctionHallAddress = assetDetails.AuctionHallAddress;
                model.AuctionHallPhone1 = assetDetails.AuctionHallPhone1;
                model.AuctionHallPhone2 = assetDetails.AuctionHallPhone2;

                model.FacebookDescription = String.Format("{0},{1}.{2},{3}", model.CategoryName, model.AbbreviatedNewPriceText, model.CityName, model.ProvinceName);
                model.TwitterText = String.Format("Kunjungi {0}.Info {1},{2}.{3},{4}.", ViewBag.RootPath, model.CategoryName, model.AbbreviatedNewPriceText, model.CityName, model.ProvinceName);
                //model.EmailUrl = String.Format(@"mailto:subject=Ada info properti di {0}&body=Ada info properti di {0}\nDi kota manapun anda butuh properti, kunjungi https://lelang.bankmandiri.co.id", Request.RawUrl);
                model.EmailSubject = String.Format("Ada info agunan di {0}/asset/details/{1}", ViewBag.RootPath, model.AssetID.ToString());
                model.EmailText = String.Format("Kunjungi {0}.Info {1},{2}.{3},{4}.{0}/asset/details/{5}", ViewBag.RootPath, model.CategoryName, model.AbbreviatedNewPriceText, model.CityName, model.ProvinceName, model.AssetID.ToString());

                model.Terms = MasterFacade.GetActiveWebContentByWebContentTypeID(8).ReturnData.Content;

                model.RelatedAssets = new List<AssetListingViewModel>();
                IList<dynamic> relatedListing = AssetFacade.GetAllActiveAssetsByCityID(assetDetails.CityID, memberID: SecurityHelper.GetCurrentLoggedInMemberID()).ReturnData;
                foreach (dynamic l in relatedListing)
                {
                    listing = new AssetListingViewModel();
                    listing.AssetID = l.AssetID;
                    listing.Address = l.Address;
                    listing.CategoryName = l.CategoryName;
                    listing.OldPrice = l.OldPrice;
                    listing.NewPrice = l.NewPrice;
                    listing.AuctionDate = l.ScheduleDate;
                    listing.AuctionTime = l.ScheduleTime;
                    listing.PostedDate = l.DateTimeCreated;
                    listing.Timezone = l.Timezone;
                    listing.CityName = l.CityName;
                    listing.ProvinceName = l.ProvinceName;
                    listing.CurrentStatusID = l.CurrentStatusID;
                    listing.IsEmailAuction = l.IsEmailAuction;
                    listing.IsFavorite = l.IsFavorite;
                    listing.Type = l.Type;
                    model.RelatedAssets.Add(listing);
                }
            }

            if (print == 0)
            {
                return View("AssetDetails", model);
            }
            else
            {
                return View("PrintAssetDetails", model);
            }
        }
    }
}
