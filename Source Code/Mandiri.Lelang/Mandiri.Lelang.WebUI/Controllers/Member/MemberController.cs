﻿using BotDetect.Web.Mvc;
using Facebook;
using Mandiri.Lelang.Attribute.ActionFilter;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.ViewModel.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;

namespace Mandiri.Lelang.WebUI.Controllers.Member
{
    /// <summary>
    /// The member controller
    /// </summary>
    [AllowAnonymous]
    public class MemberController : BaseController
    {
        private Uri RedirectUri
        {
            get
            {
                var uriBuilder = new UriBuilder(Request.Url);
                uriBuilder.Query = null;
                uriBuilder.Fragment = null;
                uriBuilder.Path = Url.Action("fb-callback");
                return uriBuilder.Uri;
            }
        }

        /// <summary>
        /// Facebook action
        /// </summary>
        /// <returns>Redirect to facebook url</returns>
        [ActionName("facebook")]
        public ActionResult Facebook()
        {
            var fb = new FacebookClient();
            var loginUrl = fb.GetLoginUrl(new
            {
                client_id = ConfigurationManager.AppSettings["FBClientID"],
                client_secret = ConfigurationManager.AppSettings["FBClientSecret"],
                redirect_uri = RedirectUri.AbsoluteUri,
                response_type = "code",
                scope = "email"
            });

            return Redirect(loginUrl.AbsoluteUri);
        }

        /// <summary>
        /// Facebook callback
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [ActionName("fb-callback")]
        public ActionResult FacebookCallback(string code)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(code))
                {
                    if (bool.Parse(ConfigurationManager.AppSettings["EnableProxy"]))
                    {
                        FacebookClient.SetDefaultHttpWebRequestFactory(uri =>
                        {
                            var request = new HttpWebRequestWrapper((HttpWebRequest)WebRequest.Create(uri));
                            request.Proxy.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["ProxyUsername"], ConfigurationManager.AppSettings["ProxyPassword"], ConfigurationManager.AppSettings["ProxyDomain"]);
                            return request;
                        });
                    }
                    var fb = new FacebookClient();
                    dynamic result = fb.Post("oauth/access_token", new
                    {
                        client_id = ConfigurationManager.AppSettings["FBClientID"],
                        client_secret = ConfigurationManager.AppSettings["FBClientSecret"],
                        redirect_uri = RedirectUri.AbsoluteUri,
                        code = code
                    });

                    var accessToken = result.access_token;

                    //Store the access token
                    Session["AccessToken"] = accessToken;

                    //Set user access token            
                    fb.AccessToken = accessToken;

                    //Get User Information
                    dynamic me = fb.Get("me?fields=name,link,first_name,middle_name,last_name,id,email");
                    string email = me.email;
                    string fullName = me.name;
                    string id = me.id;
                    string link = me.link;

                    //Register
                    String token;
                    DataModel.Member member = new DataModel.Member();
                    member.Email = email;
                    member.FullName = fullName;
                    var facadeResult = MemberFacade.RegisterMember(member, null, null, null, null, id, accessToken, link, email, MemberFacade.Source.FACEBOOK, false, HttpContext.Session.SessionID, null, out token);

                    if (facadeResult.IsSuccess)
                    {
                        //Set Auth Cookie (Login)
                        FormsAuthentication.SetAuthCookie(facadeResult.ReturnData.MemberID.ToString(), false);
                        //Check for profile completeness
                        if (MemberFacade.IsProfileComplete(facadeResult.ReturnData.MemberID).ReturnData)
                        {
                            //Redirect to home if complete
                            return RedirectToAction("index", "home");
                        }
                        else
                        {
                            //Redirect to edit profile if not complete
                            return RedirectToAction("profile", "member");
                        }
                    }
                    else
                    {
                        //TODO: Send error message
                        Session.Clear();
                        return RedirectToAction("register", "member");
                    }
                }
                else
                {
                    Session.Clear();
                    return RedirectToAction("register", "member");
                }
            }
            catch
            {
                Session.Clear();
                return RedirectToAction("register", "member");
            }
        }

        /// <summary>
        /// The activation page
        /// </summary>
        /// <returns>The activation page view</returns>
        [ActionName("activation")]
        public ActionResult Activation(String email, String token)
        {
            MemberActivationViewModel model = new MemberActivationViewModel();

            if (ModelState.IsValid && !String.IsNullOrWhiteSpace(email) && !String.IsNullOrWhiteSpace(token))
            {
                var result = MemberFacade.ActivateMember(email, token, null);
                model.IsActivationSuccess = result.IsSuccess;
                model.ActivationMessage = result.ErrorMessage;
                if (model.IsActivationSuccess)
                {
                    model.ActivationMessage = String.Format("Terima kasih {0}. Akun member anda sudah di aktivasi. Klik disini untuk login", result.ReturnData.FullName);
                }
            }
            else
            {
                model.IsActivationSuccess = false;
                model.ActivationMessage = "Link aktivasi tidak valid";
            }

            return View("Activation", model);
        }

        /// <summary>
        /// Logout
        /// </summary>
        /// <returns>Redirect to Home</returns>
        [ActionName("logout")]
        public ActionResult Logout()
        {
            if (SecurityHelper.IsAuthenticated())
            {
                var result = MemberFacade.MemberLogout(SecurityHelper.GetCurrentLoggedInMemberID().Value, false, null, null);
                if (result.IsSuccess)
                {
                    FormsAuthentication.SignOut();
                    Session.Clear();
                }
            }

            return RedirectToAction("index", "home");
        }

        /// <summary>
        /// The forgot password form
        /// </summary>
        /// <returns>The forgot password view</returns>
        [ActionName("forgot")]
        public ActionResult ForgotPasswordForm()
        {
            ForgotPasswordViewModel model;
            if (TempData["ForgotPasswordModel"] != null)
            {
                model = (ForgotPasswordViewModel)TempData["ForgotPasswordModel"];
            }
            else
            {
                model = new ForgotPasswordViewModel();
            }
            if (TempData["ViewData"] != null)
            {
                //Keep error messages
                ViewData = (ViewDataDictionary)TempData["ViewData"];
            }

            return View("ForgotPasswordForm", model);
        }

        /// <summary>
        /// The reset password form
        /// </summary>
        /// <returns>The reset password view</returns>
        [ActionName("reset")]
        public ActionResult ResetPasswordForm(String email, String token)
        {
            ResetPasswordViewModel model;

            if (TempData["ResetPasswordModel"] != null)
            {
                model = (ResetPasswordViewModel)TempData["ResetPasswordModel"];
            }
            else
            {
                model = new ResetPasswordViewModel();

                if (!String.IsNullOrWhiteSpace(email) && !String.IsNullOrWhiteSpace(token))
                {
                    var result = MemberFacade.CheckResetPasswordToken(email, token);
                    model.Email = email;
                    model.Token = token;
                    model.IsResetPasswordTokenValid = result.IsSuccess;
                    model.ResetPasswordMessage = result.ErrorMessage;
                }
                else
                {
                    model.IsResetPasswordTokenValid = false;
                    model.ResetPasswordMessage = "Link reset password tidak valid";
                }
            }
            if (TempData["ViewData"] != null)
            {
                //Keep error messages
                ViewData = (ViewDataDictionary)TempData["ViewData"];
            }

            return View("ResetPasswordForm", model);
        }

        /// <summary>
        /// The register form
        /// </summary>
        /// <returns>The register view</returns>
        [ActionName("register")]
        public ActionResult RegisterForm()
        {
            RegisterViewModel model;
            if (TempData["RegisterModel"] != null)
            {
                model = (RegisterViewModel)TempData["RegisterModel"];
            }
            else
            {
                model = new RegisterViewModel();
            }
            if (TempData["ViewData"] != null)
            {
                //Keep error messages
                ViewData = (ViewDataDictionary)TempData["ViewData"];
            }
            model.IsEditProfile = false;
            model.Password = String.Empty;
            model.ConfirmPassword = String.Empty;
            //model.CaptchaCode = String.Empty;
            model.EmailNotificationMessage = MasterFacade.GetActiveWebContentByWebContentTypeID(10).ReturnData.Content;
            model.FlagEmailNotification = false;
            model.MandatoryCityID = bool.Parse(AdministrationFacade.GetActiveSystemSettingByID("MandatoryForMemberCity").ReturnData.Value);
            model.MandatoryDateOfBirth = bool.Parse(AdministrationFacade.GetActiveSystemSettingByID("MandatoryForMemberDateOfBirth").ReturnData.Value);
            model.MandatoryFullAddress = bool.Parse(AdministrationFacade.GetActiveSystemSettingByID("MandatoryForMemberAddress").ReturnData.Value);
            model.MandatoryMobilePhoneNumber = bool.Parse(AdministrationFacade.GetActiveSystemSettingByID("MandatoryForMemberMobilePhoneNumber").ReturnData.Value);
            model.MandatoryIdentificationNumber = bool.Parse(AdministrationFacade.GetActiveSystemSettingByID("MandatoryForMemberIdentificationNumber").ReturnData.Value);
            model.MandatoryPlaceOfBirth = bool.Parse(AdministrationFacade.GetActiveSystemSettingByID("MandatoryForMemberPlaceOfBirth").ReturnData.Value);
            model.MandatoryProvinceID = bool.Parse(AdministrationFacade.GetActiveSystemSettingByID("MandatoryForMemberProvince").ReturnData.Value);
            model.MandatoryPreference = bool.Parse(AdministrationFacade.GetActiveSystemSettingByID("MandatoryForMemberPreference").ReturnData.Value);

            IList<Province> provinces = MasterFacade.GetAllActiveProvinces().ReturnData.OrderBy(m => m.Name).ToList();
            provinces.Insert(0, new Province() { ProvinceID = null, Name = "(Pilih Provinsi)" });
            model.Provinces = provinces.AsEnumerable();

            //IList<City> cities = MasterFacade.GetAllActiveCities().ReturnData.OrderBy(m => m.Name).ToList();
            var cities = new List<City>();
            cities.Insert(0, new City() { CityID = null, Name = "(Pilih Kota)" });
            model.Cities = cities.AsEnumerable();

            //IList<City> prefCities = MasterFacade.GetAllActiveCities().ReturnData.OrderBy(m => m.Name).ToList();
            IList<Province> prefProvinces = MasterFacade.GetAllActiveProvinces().ReturnData.OrderBy(m => m.Name).ToList();
            IList<MinMaxPrice> prefPrices = MasterFacade.GetAllActiveMinMaxPrices().ReturnData.OrderBy(m => m.Value).ToList();
            IList<Category> prefCategories = MasterFacade.GetAllActiveCategories().ReturnData.OrderBy(m => m.Name).ToList();
            if (model.MandatoryPreference)
            {
                //No need to differentiate optional and mandatory
                //prefCities.Insert(0, new City() { CityID = null, Name = "(Harus Diisi)" });
                //prefProvinces.Insert(0, new Province() { ProvinceID = null, Name = "(Harus Diisi)" });
                //prefCategories.Insert(0, new Category() { CategoryID = null, Name = "(Harus Diisi)" });
                prefPrices.Insert(0, new MinMaxPrice() { MinMaxPriceID = null, Name = "(Harus Diisi)" });
            }
            else
            {
                //No need to differentiate optional and mandatory
                //prefCities.Insert(0, new City() { CityID = null, Name = "(Pilih Kota)" });
                //prefProvinces.Insert(0, new Province() { ProvinceID = null, Name = "(Pilih Propinsi)" });
                //prefCategories.Insert(0, new Category() { CategoryID = null, Name = "(Pilih Kategori)" });
                prefPrices.Insert(0, new MinMaxPrice() { MinMaxPriceID = null, Name = "(Pilih Harga)" });
            }
            //model.PreferenceCities = prefCities.AsEnumerable();
            model.PreferenceCities = (new List<City>()).AsEnumerable();
            model.PreferenceProvinces = prefProvinces.AsEnumerable();
            model.PreferenceMinMaxPrices = prefPrices.AsEnumerable();
            model.PreferenceCategories = prefCategories.AsEnumerable();

            return View("RegisterForm", model);
        }

        /// <summary>
        /// The profile form
        /// </summary>
        /// <returns>The profile view</returns>
        [Authorize]
        [ActionName("profile")]
        public ActionResult ProfileForm()
        {
            RegisterViewModel model;
            if (TempData["RegisterModel"] != null)
            {
                model = (RegisterViewModel)TempData["RegisterModel"];
            }
            else
            {
                model = new RegisterViewModel();
                var resultMember = MemberFacade.GetActiveMemberByID(SecurityHelper.GetCurrentLoggedInMemberID().Value);
                if (resultMember.IsSuccess && resultMember.ReturnData != null)
                {
                    var member = resultMember.ReturnData;
                    model.CityID = member.CityID;
                    model.DateOfBirth = member.DateOfBirth;
                    model.Email = member.Email;
                    model.FlagEmailNotification = true;
                    model.FullAddress = member.Address;
                    model.FullName = member.FullName;
                    model.IdentificationNumber = member.IdentificationNumber;
                    model.MobilePhoneNumber = member.MobilePhoneNumber;
                    model.PlaceOfBirth = member.PlaceOfBirth;
                    model.ProvinceID = member.ProvinceID;

                    //Set Current Preferences
                    var resultMemberPreference = MemberFacade.GetActiveMemberPreferenceByMemberID(member.MemberID);
                    if (resultMemberPreference.IsSuccess && resultMemberPreference.ReturnData != null)
                    {
                        model.PreferenceMinPriceID = resultMemberPreference.ReturnData.MinPriceID;
                        model.PreferenceMaxPriceID = resultMemberPreference.ReturnData.MaxPriceID;

                        var resultMemberCityPreferences = MemberFacade.GetAllActiveMemberCityPreferenceByMemberPreferenceID(resultMemberPreference.ReturnData.MemberPreferenceID);
                        if (resultMemberCityPreferences.IsSuccess && resultMemberCityPreferences.ReturnData != null && resultMemberCityPreferences.ReturnData.Count > 0)
                        {
                            model.PreferenceCityID = new int[resultMemberCityPreferences.ReturnData.Count];
                            for (int i = 0; i < resultMemberCityPreferences.ReturnData.Count; i++)
                            {
                                model.PreferenceCityID[i] = resultMemberCityPreferences.ReturnData[i].CityID;
                            }
                        }

                        var resultMemberCategoryPreferences = MemberFacade.GetAllActiveMemberCategoryPreferenceByMemberPreferenceID(resultMemberPreference.ReturnData.MemberPreferenceID);
                        if (resultMemberCategoryPreferences.IsSuccess && resultMemberCategoryPreferences.ReturnData != null && resultMemberCategoryPreferences.ReturnData.Count > 0)
                        {
                            model.PreferenceCategoryID = new int[resultMemberCategoryPreferences.ReturnData.Count];
                            for (int i = 0; i < resultMemberCategoryPreferences.ReturnData.Count; i++)
                            {
                                model.PreferenceCategoryID[i] = resultMemberCategoryPreferences.ReturnData[i].CategoryID;
                            }
                        }

                        var resultMemberProvinceIDPreferences = MemberFacade.GetAllActiveMemberProvinceIDPreferenceByMemberPreferenceID(resultMemberPreference.ReturnData.MemberPreferenceID);
                        if (resultMemberProvinceIDPreferences.IsSuccess && resultMemberProvinceIDPreferences.ReturnData != null && resultMemberProvinceIDPreferences.ReturnData.Count > 0)
                        {
                            model.PreferenceProvinceID = new int[resultMemberProvinceIDPreferences.ReturnData.Count];
                            for (int i = 0; i < resultMemberProvinceIDPreferences.ReturnData.Count; i++)
                            {
                                model.PreferenceProvinceID[i] = resultMemberProvinceIDPreferences.ReturnData[i];
                            }
                        }
                    }
                }
            }
            if (TempData["ViewData"] != null)
            {
                ViewData = (ViewDataDictionary)TempData["ViewData"];
            }
            model.IsEditProfile = true;
            model.Password = String.Empty;
            model.ConfirmPassword = String.Empty;
            //model.CaptchaCode = String.Empty;
            model.EmailNotificationMessage = MasterFacade.GetActiveWebContentByWebContentTypeID(10).ReturnData.Content;
            //model.FlagEmailNotification = false;
            model.MandatoryCityID = bool.Parse(AdministrationFacade.GetActiveSystemSettingByID("MandatoryForMemberCity").ReturnData.Value);
            model.MandatoryDateOfBirth = bool.Parse(AdministrationFacade.GetActiveSystemSettingByID("MandatoryForMemberDateOfBirth").ReturnData.Value);
            model.MandatoryFullAddress = bool.Parse(AdministrationFacade.GetActiveSystemSettingByID("MandatoryForMemberAddress").ReturnData.Value);
            model.MandatoryMobilePhoneNumber = bool.Parse(AdministrationFacade.GetActiveSystemSettingByID("MandatoryForMemberMobilePhoneNumber").ReturnData.Value);
            model.MandatoryIdentificationNumber = bool.Parse(AdministrationFacade.GetActiveSystemSettingByID("MandatoryForMemberIdentificationNumber").ReturnData.Value);
            model.MandatoryPlaceOfBirth = bool.Parse(AdministrationFacade.GetActiveSystemSettingByID("MandatoryForMemberPlaceOfBirth").ReturnData.Value);
            model.MandatoryProvinceID = bool.Parse(AdministrationFacade.GetActiveSystemSettingByID("MandatoryForMemberProvince").ReturnData.Value);
            model.MandatoryPreference = bool.Parse(AdministrationFacade.GetActiveSystemSettingByID("MandatoryForMemberPreference").ReturnData.Value);

            IList<Province> provinces = MasterFacade.GetAllActiveProvinces().ReturnData.OrderBy(m => m.Name).ToList();
            provinces.Insert(0, new Province() { ProvinceID = null, Name = "(Pilih Provinsi)" });
            model.Provinces = provinces.AsEnumerable();

            if (model.ProvinceID.HasValue)
            {
                var cities = MasterFacade.GetAllActiveCitiesByProvinceIDs((new List<int>() { model.ProvinceID.Value }).AsEnumerable()).ReturnData.OrderBy(m => m.Name).ToList();
                cities.Insert(0, new City() { CityID = null, Name = "(Pilih Kota)" });
                model.Cities = cities.AsEnumerable();
            }
            else
            {
                var cities = new List<City>();
                cities.Insert(0, new City() { CityID = null, Name = "(Pilih Kota)" });
                model.Cities = cities.AsEnumerable();
            }

            //IList<City> prefCities = MasterFacade.GetAllActiveCities().ReturnData.OrderBy(m => m.Name).ToList();
            IList<Province> prefProvinces = MasterFacade.GetAllActiveProvinces().ReturnData.OrderBy(m => m.Name).ToList();
            IList<MinMaxPrice> prefPrices = MasterFacade.GetAllActiveMinMaxPrices().ReturnData.OrderBy(m => m.Value).ToList();
            IList<Category> prefCategories = MasterFacade.GetAllActiveCategories().ReturnData.OrderBy(m => m.Name).ToList();
            if (model.MandatoryPreference)
            {
                //No need to differentiate optional and mandatory
                //prefCities.Insert(0, new City() { CityID = null, Name = "(Harus Diisi)" });
                //prefProvinces.Insert(0, new Province() { ProvinceID = null, Name = "(Harus Diisi)" });
                //prefCategories.Insert(0, new Category() { CategoryID = null, Name = "(Harus Diisi)" });
                prefPrices.Insert(0, new MinMaxPrice() { MinMaxPriceID = null, Name = "(Harus Diisi)" });
            }
            else
            {
                //No need to differentiate optional and mandatory
                //prefCities.Insert(0, new City() { CityID = null, Name = "(Pilih Kota)" });
                //prefProvinces.Insert(0, new Province() { ProvinceID = null, Name = "(Pilih Propinsi)" });
                //prefCategories.Insert(0, new Category() { CategoryID = null, Name = "(Pilih Kategori)" });
                prefPrices.Insert(0, new MinMaxPrice() { MinMaxPriceID = null, Name = "(Pilih Harga)" });
            }
            //model.PreferenceCities = prefCities.AsEnumerable();
            if (model.PreferenceProvinceID != null && model.PreferenceProvinceID.Length > 0)
            {
                model.PreferenceCities = MasterFacade.GetAllActiveCitiesByProvinceIDs(model.PreferenceProvinceID.AsEnumerable()).ReturnData.OrderBy(m => m.Name);
            }
            else
            {
                model.PreferenceCities = (new List<City>()).AsEnumerable();
            }
            model.PreferenceProvinces = prefProvinces.AsEnumerable();
            model.PreferenceMinMaxPrices = prefPrices.AsEnumerable();
            model.PreferenceCategories = prefCategories.AsEnumerable();

            return View("RegisterForm", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [ValidateSQLInjection]
        [ValidateAntiForgeryToken]
        //[CaptchaValidation("CaptchaCode", "MandiriCaptcha", "Kode CAPTCHA yang di input salah!")]
        public ActionResult SubmitRegisterForm(RegisterViewModel model)
        {
            ///validation google recaptcha
            var response = Request["g-recaptcha-response"];
            string secretKey = WebConfigurationManager.AppSettings["RecaptchaPrivateKey"];
            var client = new WebClient();
            var CaptchaResult = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey , response));
            var obj = JObject.Parse(CaptchaResult);
            var status = (bool)obj.SelectToken("success");

            
            MemberPreference memberPreference = new MemberPreference();
            IList<MemberCategoryPreference> categoryPreferences = null;
            IList<MemberCityPreference> cityPreferences = null;

            //Validate min and max price
            if (model.PreferenceMinPriceID.HasValue && model.PreferenceMaxPriceID.HasValue)
            {
                var minPrice = MasterFacade.GetActiveMinMaxPriceByMinMaxPriceID(model.PreferenceMinPriceID.Value).ReturnData;
                var maxPrice = MasterFacade.GetActiveMinMaxPriceByMinMaxPriceID(model.PreferenceMaxPriceID.Value).ReturnData;
                if (minPrice.Value > maxPrice.Value)
                {
                    ModelState.AddModelError("PreferenceMinPriceID", "Harga minimum harus lebih kecil dari harga maksimum");
                    ModelState.AddModelError("PreferenceMaxPriceID", "Harga maksimum harus lebih besar dari harga minimum");
                }
            }

            if (ModelState.IsValid && status)
            {
                if (!model.IsEditProfile)
                {
                    String token;
                    DataModel.Member member = new DataModel.Member();
                    member.Email = model.Email;
                    member.FullName = model.FullName;
                    member.Address = model.FullAddress;
                    member.CityID = model.CityID;
                    member.ProvinceID = model.ProvinceID;
                    member.DateOfBirth = model.DateOfBirth;
                    member.PlaceOfBirth = model.PlaceOfBirth;
                    member.IdentificationNumber = model.IdentificationNumber;
                    member.MobilePhoneNumber = model.MobilePhoneNumber;

                    if (model.PreferenceMaxPriceID.HasValue || model.PreferenceMinPriceID.HasValue || (model.PreferenceCityID != null && model.PreferenceCityID.Length > 0) || (model.PreferenceCategoryID != null && model.PreferenceCategoryID.Length > 0))
                    {
                        memberPreference.MinPriceID = model.PreferenceMinPriceID;
                        memberPreference.MaxPriceID = model.PreferenceMaxPriceID;

                        if (model.PreferenceCategoryID != null && model.PreferenceCategoryID.Length > 0)
                        {
                            categoryPreferences = new List<MemberCategoryPreference>();
                            MemberCategoryPreference categoryPreference;
                            foreach (var preferenceCategory in model.PreferenceCategoryID)
                            {
                                categoryPreference = new MemberCategoryPreference();
                                categoryPreference.CategoryID = preferenceCategory;
                                categoryPreferences.Add(categoryPreference);
                            }
                        }

                        if (model.PreferenceCityID != null && model.PreferenceCityID.Length > 0)
                        {
                            cityPreferences = new List<MemberCityPreference>();
                            MemberCityPreference cityPreference;
                            foreach (var preferenceCity in model.PreferenceCityID)
                            {
                                cityPreference = new MemberCityPreference();
                                cityPreference.CityID = preferenceCity;
                                cityPreferences.Add(cityPreference);
                            }
                        }
                    }

                    var result = MemberFacade.RegisterMember(member, memberPreference, categoryPreferences, cityPreferences, model.Password, null, null, null, null, MemberFacade.Source.EMAIL, false, HttpContext.Session.SessionID, null, out token);

                    ModelState.Clear();

                    model.FlagEmailNotification = false;
                    model.Password = String.Empty;
                    model.ConfirmPassword = String.Empty;
                    //model.CaptchaCode = String.Empty;
                    model.IsRegisterSuccess = result.IsSuccess;

                    /*if (result.IsSuccess)
                    {
                        FormsAuthentication.SetAuthCookie(result.ReturnData.MemberID.ToString(), false);                        
                        return RedirectToAction("index", "home");
                    }
                    else
                    {
                        ModelState.AddModelError("", result.ErrorMessage);
                    }*/
                    if (!result.IsSuccess)
                    {
                        ModelState.AddModelError("", result.ErrorMessage);
                    }
                }
                else
                {
                    //Edit Profile
                    DataModel.Member member = new DataModel.Member();
                    member.MemberID = SecurityHelper.GetCurrentLoggedInMemberID().Value;
                    member.Email = model.Email;
                    member.FullName = model.FullName;
                    member.Address = model.FullAddress;
                    member.CityID = model.CityID;
                    member.ProvinceID = model.ProvinceID;
                    member.DateOfBirth = model.DateOfBirth;
                    member.PlaceOfBirth = model.PlaceOfBirth;
                    member.IdentificationNumber = model.IdentificationNumber;
                    member.MobilePhoneNumber = model.MobilePhoneNumber;

                    //Preference
                    if (model.PreferenceMaxPriceID.HasValue || model.PreferenceMinPriceID.HasValue || (model.PreferenceCityID != null && model.PreferenceCityID.Length > 0) || (model.PreferenceCategoryID != null && model.PreferenceCategoryID.Length > 0))
                    {
                        memberPreference.MinPriceID = model.PreferenceMinPriceID;
                        memberPreference.MaxPriceID = model.PreferenceMaxPriceID;

                        if (model.PreferenceCategoryID != null && model.PreferenceCategoryID.Length > 0)
                        {
                            categoryPreferences = new List<MemberCategoryPreference>();
                            MemberCategoryPreference categoryPreference;
                            foreach (var preferenceCategory in model.PreferenceCategoryID)
                            {
                                categoryPreference = new MemberCategoryPreference();
                                categoryPreference.CategoryID = preferenceCategory;
                                categoryPreferences.Add(categoryPreference);
                            }
                        }

                        if (model.PreferenceCityID != null && model.PreferenceCityID.Length > 0)
                        {
                            cityPreferences = new List<MemberCityPreference>();
                            MemberCityPreference cityPreference;
                            foreach (var preferenceCity in model.PreferenceCityID)
                            {
                                cityPreference = new MemberCityPreference();
                                cityPreference.CityID = preferenceCity;
                                cityPreferences.Add(cityPreference);
                            }
                        }
                    }

                    var isProfileChanged = false;
                    var existingMember = MemberFacade.GetActiveMemberByID(member.MemberID).ReturnData;
                    if (existingMember != null)
                    {
                        if (existingMember.Address != member.Address || existingMember.CityID != member.CityID || existingMember.DateOfBirth != member.DateOfBirth || existingMember.FullName != member.FullName || existingMember.IdentificationNumber != member.IdentificationNumber || existingMember.MobilePhoneNumber != member.MobilePhoneNumber || existingMember.PlaceOfBirth != member.PlaceOfBirth || existingMember.ProvinceID != member.ProvinceID)
                        {
                            isProfileChanged = true;
                        }
                    }
                    var existingMemberPreference = MemberFacade.GetActiveMemberPreferenceByMemberID(member.MemberID).ReturnData;
                    if (existingMemberPreference != null)
                    {
                        if (existingMemberPreference.MinPriceID != memberPreference.MinPriceID || existingMemberPreference.MaxPriceID != memberPreference.MaxPriceID)
                        {
                            isProfileChanged = true;
                        }
                        var existingMemberCategoryPreference = MemberFacade.GetAllActiveMemberCategoryPreferenceByMemberPreferenceID(existingMemberPreference.MemberPreferenceID).ReturnData;
                        if (existingMemberCategoryPreference != null && existingMemberCategoryPreference.Count > 0 && categoryPreferences == null)
                        {
                            isProfileChanged = true;
                        }
                        if (categoryPreferences != null && existingMemberCategoryPreference != null && existingMemberCategoryPreference.Count != categoryPreferences.Count)
                        {
                            isProfileChanged = true;
                        }
                        if (categoryPreferences != null && existingMemberCategoryPreference != null && existingMemberCategoryPreference.Count > 0 && categoryPreferences.Count > 0)
                        {
                            if (!existingMemberCategoryPreference.Select(m => m.CategoryID).OrderBy(m => m).SequenceEqual(categoryPreferences.Select(m => m.CategoryID).OrderBy(m => m)))
                            {
                                isProfileChanged = true;
                            }
                        }

                        var existingMemberCityPreference = MemberFacade.GetAllActiveMemberCityPreferenceByMemberPreferenceID(existingMemberPreference.MemberPreferenceID).ReturnData;
                        if (existingMemberCityPreference != null && existingMemberCityPreference.Count > 0 && cityPreferences == null)
                        {
                            isProfileChanged = true;
                        }
                        if (cityPreferences != null && existingMemberCityPreference != null && existingMemberCityPreference.Count != cityPreferences.Count)
                        {
                            isProfileChanged = true;
                        }
                        if (cityPreferences != null && existingMemberCityPreference != null && existingMemberCityPreference.Count > 0 && cityPreferences.Count > 0)
                        {
                            if (!existingMemberCityPreference.Select(m => m.CityID).OrderBy(m => m).SequenceEqual(cityPreferences.Select(m => m.CityID).OrderBy(m => m)))
                            {
                                isProfileChanged = true;
                            }
                        }
                    }

                    var result = MemberFacade.UpdateMemberProfile(member, memberPreference, categoryPreferences, cityPreferences, model.Password, model.OldPassword, isProfileChanged, null);

                    String successMessage = String.Empty;
                    if (result.IsSuccess && isProfileChanged && String.IsNullOrWhiteSpace(model.Password))
                    {
                        successMessage = "Profile anda sudah berhasil diubah.";
                    }
                    else if (result.IsSuccess && !isProfileChanged && !String.IsNullOrWhiteSpace(model.Password))
                    {
                        successMessage = "Password anda sudah berhasil diubah.";
                    }
                    else if (result.IsSuccess && isProfileChanged && !String.IsNullOrWhiteSpace(model.Password))
                    {
                        successMessage = "Profile dan password anda sudah berhasil diubah.";
                    }
                    else
                    {
                        successMessage = "Tidak ada perubahan data.";
                    }

                    ModelState.Clear();

                    model.FlagEmailNotification = true;
                    model.Password = String.Empty;
                    model.ConfirmPassword = String.Empty;
                    //model.CaptchaCode = String.Empty;
                    model.IsRegisterSuccess = result.IsSuccess;
                    model.RegisterSuccessMessage = successMessage;

                    if (!result.IsSuccess)
                    {
                        ModelState.AddModelError("", result.ErrorMessage);
                    }
                }
            }
            else
            {
                //ModelState.SetModelValue("CaptchaCode", new ValueProviderResult(String.Empty, String.Empty, CultureInfo.InvariantCulture));
                if (!model.IsEditProfile)
                {
                    ModelState.SetModelValue("FlagEmailNotification", new ValueProviderResult(false, String.Empty, CultureInfo.InvariantCulture));
                }
                else
                {
                    ModelState.SetModelValue("FlagEmailNotification", new ValueProviderResult(true, String.Empty, CultureInfo.InvariantCulture));
                }
                ModelState.SetModelValue("Password", new ValueProviderResult(String.Empty, String.Empty, CultureInfo.InvariantCulture));
                ModelState.SetModelValue("ConfirmPassword", new ValueProviderResult(String.Empty, String.Empty, CultureInfo.InvariantCulture));
                ModelState.SetModelValue("IsRegisterSuccess", new ValueProviderResult(false, String.Empty, CultureInfo.InvariantCulture));
            }

            TempData["RegisterModel"] = model;
            TempData["ViewData"] = ViewData;
            if (!model.IsEditProfile)
            {
                return RedirectToAction("register", "member");
            }
            else
            {
                return RedirectToAction("profile", "member");
            }
        }

        /// <summary>
        /// Submit reset password
        /// </summary>
        /// <returns>Redirect to reset password view</returns>
        [HttpPost]
        [ValidateSQLInjection]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitResetPasswordForm(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = MemberFacade.ResetPassword(model.Email, model.Token, model.Password, null);

                ModelState.Clear();

                model.Password = String.Empty;
                model.ConfirmPassword = String.Empty;
                model.IsResetPasswordSuccess = result.IsSuccess;
                model.IsResetPasswordTokenValid = true;

                if (model.IsResetPasswordSuccess)
                {
                    model.ResetPasswordMessage = String.Format("Terima kasih {0}. Password anda sudah berhasil di reset. Klik disini untuk login", result.ReturnData.FullName);
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            else
            {
                ModelState.SetModelValue("Password", new ValueProviderResult(String.Empty, String.Empty, CultureInfo.InvariantCulture));
                ModelState.SetModelValue("ConfirmPassword", new ValueProviderResult(String.Empty, String.Empty, CultureInfo.InvariantCulture));
                ModelState.SetModelValue("IsResetPasswordSuccess", new ValueProviderResult(false, String.Empty, CultureInfo.InvariantCulture));
                ModelState.SetModelValue("IsResetPasswordTokenValid", new ValueProviderResult(true, String.Empty, CultureInfo.InvariantCulture));
            }

            TempData["ResetPasswordModel"] = model;
            TempData["ViewData"] = ViewData;
            return RedirectToAction("reset", "member", new { email = model.Email, token = model.Token });
        }

        /// <summary>
        /// Submit forgot password
        /// </summary>
        /// <returns>Redirect to forgot password view</returns>
        [HttpPost]
        [ValidateSQLInjection]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitForgotPasswordForm(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = MemberFacade.ForgotPassword(model.Email, null);

                ModelState.Clear();

                model.Email = String.Empty;
                model.IsForgotPasswordSuccess = result.IsSuccess;

                if (!model.IsForgotPasswordSuccess)
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            else
            {
                ModelState.SetModelValue("Email", new ValueProviderResult(String.Empty, String.Empty, CultureInfo.InvariantCulture));
                ModelState.SetModelValue("IsForgotPasswordSuccess", new ValueProviderResult(false, String.Empty, CultureInfo.InvariantCulture));
            }

            TempData["ForgotPasswordModel"] = model;
            TempData["ViewData"] = ViewData;
            return RedirectToAction("forgot", "member");
        }

        /// <summary>
        /// The login form
        /// </summary>
        /// <returns>The login view</returns>
        [ActionName("login")]
        public ActionResult LoginForm()
        {
            LoginViewModel model;
            if (TempData["LoginModel"] != null)
            {
                model = (LoginViewModel)TempData["LoginModel"];
            }
            else
            {
                model = new LoginViewModel();
            }
            if (TempData["ViewData"] != null)
            {
                //Keep error messages
                ViewData = (ViewDataDictionary)TempData["ViewData"];
            }
            model.Password = String.Empty;
            model.CaptchaCode = String.Empty;

            return View("LoginForm", model);
        }

        [HttpPost]
        [ValidateSQLInjection]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitLoginForm(LoginViewModel model)
        {
            ///validation google recaptcha
            var response = Request["g-recaptcha-response"];
            string secretKey = WebConfigurationManager.AppSettings["RecaptchaPrivateKey"];
            var client = new WebClient();
            var CaptchaResult = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
            var obj = JObject.Parse(CaptchaResult);
            var status = (bool)obj.SelectToken("success");

            if (ModelState.IsValid && status)
            {
                String token;
                var result = MemberFacade.LoginMemberUsingEmail(model.Email, model.Password, false, HttpContext.Session.SessionID, null, out token);

                ModelState.Clear();

                model.Password = String.Empty;
                model.CaptchaCode = String.Empty;
                model.IsLoginSuccess = result.IsSuccess;

                if (result.IsSuccess)
                {
                    FormsAuthentication.SetAuthCookie(result.ReturnData.MemberID.ToString(), model.RememberMe);
                    //Check for profile completeness
                    if (MemberFacade.IsProfileComplete(result.ReturnData.MemberID).ReturnData)
                    {
                        //Redirect to home if complete
                        return RedirectToAction("index", "home");
                    }
                    else
                    {
                        //Redirect to edit profile if not complete
                        return RedirectToAction("profile", "member");
                    }
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            else
            {
                ModelState.SetModelValue("Password", new ValueProviderResult(String.Empty, String.Empty, CultureInfo.InvariantCulture));
                //ModelState.SetModelValue("CaptchaCode", new ValueProviderResult(String.Empty, String.Empty, CultureInfo.InvariantCulture));
                ModelState.SetModelValue("IsLoginSuccess", new ValueProviderResult(false, String.Empty, CultureInfo.InvariantCulture));
            }

            TempData["LoginModel"] = model;
            TempData["ViewData"] = ViewData;
            return RedirectToAction("login", "member");
        }

        /// <summary>
        /// The favorite asset list
        /// </summary>
        /// <returns>The favorite asset list view</returns>
        [Authorize]
        [ActionName("favorite")]
        public ActionResult FavoriteAssetList(String page = "1", long currentPageIndex = 0)
        {
            AssetListViewModel model = new AssetListViewModel();
            AssetListingViewModel listing;

            //Set record information for pagination
            var memberID = SecurityHelper.GetCurrentLoggedInMemberID();
            if (memberID.HasValue)
            {
                model.TotalRecords = AssetFacade.GetAllActiveFavoriteAssetsByMemberIDCount(memberID.Value).ReturnData;
                if (page == "next")
                {
                    if ((currentPageIndex + 1) < model.TotalPage)
                    {
                        currentPageIndex++;
                    }
                }
                else if (page == "prev")
                {
                    if (currentPageIndex > 0)
                    {
                        currentPageIndex--;
                    }
                }
                else
                {
                    currentPageIndex = long.Parse(page) - 1;
                }
                model.CurrentPageIndex = currentPageIndex;

                IList<dynamic> listingData = AssetFacade.GetAllActiveFavoriteAssetsByMemberID(memberID.Value, currentPageIndex).ReturnData;
                model.Listing = new List<AssetListingViewModel>();
                foreach (dynamic l in listingData)
                {
                    listing = new AssetListingViewModel();
                    listing.AssetID = l.AssetID;
                    listing.Address = l.Address;
                    listing.CategoryName = l.CategoryName;
                    listing.OldPrice = l.OldPrice;
                    listing.NewPrice = l.NewPrice;
                    listing.AuctionDate = l.ScheduleDate;
                    listing.AuctionTime = l.ScheduleTime;
                    listing.PostedDate = l.DateTimeCreated;
                    listing.Timezone = l.Timezone;
                    listing.CityName = l.CityName;
                    listing.ProvinceName = l.ProvinceName;
                    listing.KPKNLName = l.KPKNLName;
                    listing.CurrentStatusID = l.CurrentStatusID;
                    listing.IsEmailAuction = l.IsEmailAuction;
                    listing.IsFavorite = l.IsFavorite;
                    listing.Type = l.Type;
                    model.Listing.Add(listing);
                }
                if (model.TotalRecords > 0)
                {
                    model.AuctionDateText = model.Listing[0].AuctionDateText;
                    model.KPKNLName = model.Listing[0].KPKNLName;
                }
                return View("FavoriteAssetList", model);
            }
            return RedirectToAction("index", "home");
        }

        [Authorize]
        [HttpPost]
        [ActionName("ajax_favorite")]
        public ActionResult Favorite(String assetID)
        {
            int isSuccess;
            if (!String.IsNullOrWhiteSpace(assetID))
            {
                var result = MemberFacade.FavoriteAsset(SecurityHelper.GetCurrentLoggedInMemberID().Value, int.Parse(assetID), null);
                if (result.IsSuccess)
                {
                    isSuccess = 1;
                }
                else
                {
                    isSuccess = 0;
                }
            }
            else
            {
                isSuccess = 0;
            }
            return Json(new { isSuccess = isSuccess }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        [ActionName("ajax_page_visit")]
        public ActionResult PageVisit(String siteMapID = "", String urlReferrerSiteMapID = "", String city = "", String additionalParams = "", String assetID = null)
        {
            int isSuccess;

            if (SecurityHelper.GetCurrentLoggedInMemberID() != null)
            {
                PageVisit pageVisit = new PageVisit();
                pageVisit.MemberID = SecurityHelper.GetCurrentLoggedInMemberID();
                pageVisit.SiteMapID = siteMapID;
                try
                {
                    pageVisit.IPAddress = HttpContext.Session.SessionID;
                }
                catch
                {
                    pageVisit.IPAddress = String.Empty;
                }
                if (!String.IsNullOrWhiteSpace(city))
                {
                    var currentCity = MasterFacade.GetActiveCityByName(city).ReturnData;
                    if (currentCity != null)
                    {
                        pageVisit.CityID = currentCity.CityID;
                    }
                }
                pageVisit.AdditionalParams = additionalParams;
                if (!String.IsNullOrWhiteSpace(assetID))
                {
                    pageVisit.AssetID = long.Parse(assetID);
                }
                pageVisit.URLReferrerSiteMapID = !String.IsNullOrWhiteSpace(urlReferrerSiteMapID) ? urlReferrerSiteMapID : null;

                var result = AdministrationFacade.AddNewPageVisit(pageVisit);
                if (result.IsSuccess)
                {
                    isSuccess = 1;
                }
                else
                {
                    isSuccess = 0;
                }
            }
            else
            {
                isSuccess = 0;
            }

            return Json(new { isSuccess = isSuccess }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        [ActionName("ajax_unfavorite")]
        public ActionResult Unfavorite(String assetID)
        {
            int isSuccess;
            if (!String.IsNullOrWhiteSpace(assetID))
            {
                var result = MemberFacade.UnfavoriteAsset(SecurityHelper.GetCurrentLoggedInMemberID().Value, int.Parse(assetID), null);
                if (result.IsSuccess)
                {
                    isSuccess = 1;
                }
                else
                {
                    isSuccess = 0;
                }
            }
            else
            {
                isSuccess = 0;
            }
            return Json(new { isSuccess = isSuccess }, JsonRequestBehavior.AllowGet);
        }
    }
}
