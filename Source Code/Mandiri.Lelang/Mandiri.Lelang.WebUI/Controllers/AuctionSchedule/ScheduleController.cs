﻿using Mandiri.Lelang.Attribute.ActionFilter;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.ViewModel.Web;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Mandiri.Lelang.WebUI.Controllers.AuctionSchedule
{
    /// <summary>
    /// The auction schedule controller
    /// </summary>
    [AllowAnonymous]
    public class ScheduleController : BaseController
    {
        [NonAction]
        private ActionResult SearchAuctionScheduleList(String page = "1", long currentPageIndex = 0, DateTime? auctionDateFrom = null, DateTime? auctionDateTo = null, int[] kpknlID = null, int selectAll = -1)
        {
            AuctionSchedulesViewModel model = new AuctionSchedulesViewModel();
            AuctionScheduleViewModel schedule;

            IList<KPKNL> kpknl = MasterFacade.GetAllActiveKPKNLs().ReturnData.OrderBy(m => m.Name).ToList();
            //kpknl.Insert(0, new KPKNL() { KPKNLID = -1, Name = "Semua" });
            if (kpknlID == null || kpknlID.Length == 0)
            {
                kpknlID = new int[kpknl.Count];
                for (int i = 0; i < kpknl.Count; i++)
                {
                    kpknlID[i] = kpknl[i].KPKNLID.Value;
                }
                selectAll = -1;
            }
            else
            {
                selectAll = 0;
            }
            model.KPKNL = kpknl.AsEnumerable();
            model.KPKNLID = kpknlID;
            model.AuctionDateFrom = auctionDateFrom;
            model.AuctionDateTo = auctionDateTo;

            //Set record information for pagination            
            model.TotalRecords = AssetFacade.GetCurrentAuctionSchedulesCount(auctionDateFrom: model.AuctionDateFrom, auctionDateTo: model.AuctionDateTo, kpknlID: model.KPKNLID, selectAll: selectAll).ReturnData;
            if (page == "next")
            {
                if ((currentPageIndex + 1) < model.TotalPage)
                {
                    currentPageIndex++;
                }
            }
            else if (page == "prev")
            {
                if (currentPageIndex > 0)
                {
                    currentPageIndex--;
                }
            }
            else
            {
                currentPageIndex = long.Parse(page) - 1;
            }
            model.CurrentPageIndex = currentPageIndex;

            IList<dynamic> scheduleData = AssetFacade.GetAllActiveCurrentAuctionSchedules(currentPageIndex, auctionDateFrom: model.AuctionDateFrom, auctionDateTo: model.AuctionDateTo, kpknlID: model.KPKNLID, selectAll: selectAll).ReturnData;
            model.Schedules = new List<AuctionScheduleViewModel>();
            foreach (dynamic l in scheduleData)
            {
                schedule = new AuctionScheduleViewModel();
                schedule.AuctionScheduleID = l.AuctionScheduleID;
                schedule.AuctionDate = l.ScheduleDate;
                schedule.AuctionTime = l.ScheduleTime;
                schedule.KPKNLAddress = l.AuctionAddress;
                schedule.KPKNLName = l.Name;
                schedule.KPKNLPhone1 = l.Phone1;
                schedule.KPKNLPhone2 = l.Phone2;
                schedule.Timezone = l.Timezone;
                model.Schedules.Add(schedule);
            }
            return View("ScheduleList", model);
        }

        /// <summary>
        /// The asset list
        /// </summary>
        /// <returns>The asset list view</returns>
        [HttpGet]
        [ActionName("list")]
        [ValidateSQLInjection]
        public ActionResult AuctionScheduleList(String page = "1", long currentPageIndex = 0, DateTime? auctionDateFrom = null, DateTime? auctionDateTo = null, int[] kpknlID = null, int selectAll = -1)
        {
            return SearchAuctionScheduleList(page, currentPageIndex, auctionDateFrom, auctionDateTo, kpknlID, selectAll);
        }

        /// <summary>
        /// The asset list
        /// </summary>
        /// <returns>The asset list view</returns>        
        [HttpPost]
        [ActionName("list")]
        [ValidateSQLInjection]
        public ActionResult PostAuctionScheduleList(String page = "1", long currentPageIndex = 0, DateTime? auctionDateFrom = null, DateTime? auctionDateTo = null, int[] kpknlID = null, int selectAll = -1)
        {
            if (auctionDateFrom.HasValue && auctionDateTo.HasValue)
            {
                if (auctionDateTo.Value < auctionDateFrom.Value)
                {
                    ModelState.AddModelError("", "Sampai Tanggal Lelang harus lebih besar");
                }
            }
            if (page.ToUpper() == "DOWNLOAD")
            {
                IList<KPKNL> kpknl = MasterFacade.GetAllActiveKPKNLs().ReturnData.OrderBy(m => m.Name).ToList();
                if (kpknlID == null || kpknlID.Length == 0)
                {
                    kpknlID = new int[kpknl.Count];
                    for (int i = 0; i < kpknl.Count; i++)
                    {
                        kpknlID[i] = kpknl[i].KPKNLID.Value;
                    }
                    selectAll = -1;
                }
                else
                {
                    selectAll = 0;
                }
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = String.Format("Jadwal Lelang {0}.csv", DateTime.Today.ToString("dd MMM yyyy")),
                    // always prompt the user for downloading, set to true if you want 
                    // the browser to try to show the file inline
                    Inline = false
                };
                Response.AppendHeader("Content-Disposition", cd.ToString());
                var buf = AssetFacade.DownloadAllActiveCurrentAuctionSchedulesAndAssetsAsByteArray(auctionDateFrom, auctionDateTo, kpknlID, selectAll).ReturnData;
                return File(buf, "application/octet-stream");
            }
            else
            {
                return SearchAuctionScheduleList(page, currentPageIndex, auctionDateFrom, auctionDateTo, kpknlID, selectAll);
            }
        }

        /// <summary>
        /// The asset list by schedule id
        /// </summary>
        /// <returns>The asset list view</returns>
        [ActionName("asset")]
        public ActionResult AssetListBySchedule(long scheduleID, String page = "1", long currentPageIndex = 0)
        {
            AssetListViewModel model = new AssetListViewModel();
            AssetListingViewModel listing;

            //Set record information for pagination
            model.TotalRecords = AssetFacade.GetAllActiveAssetsByAuctionScheduleIDCount(scheduleID).ReturnData;
            if (page == "next")
            {
                if ((currentPageIndex + 1) < model.TotalPage)
                {
                    currentPageIndex++;
                }
            }
            else if (page == "prev")
            {
                if (currentPageIndex > 0)
                {
                    currentPageIndex--;
                }
            }
            else
            {
                currentPageIndex = long.Parse(page) - 1;
            }
            model.CurrentPageIndex = currentPageIndex;

            IList<dynamic> listingData = AssetFacade.GetAllActiveAssetsByAuctionScheduleID(scheduleID, currentPageIndex, memberID: SecurityHelper.GetCurrentLoggedInMemberID()).ReturnData;
            model.Listing = new List<AssetListingViewModel>();
            foreach (dynamic l in listingData)
            {
                listing = new AssetListingViewModel();
                listing.AssetID = l.AssetID;
                listing.Address = l.Address;
                listing.CategoryName = l.CategoryName;
                listing.OldPrice = l.OldPrice;
                listing.NewPrice = l.NewPrice;
                listing.AuctionDate = l.ScheduleDate;
                listing.AuctionTime = l.ScheduleTime;
                listing.PostedDate = l.DateTimeCreated;
                listing.Timezone = l.Timezone;
                listing.CityName = l.CityName;
                listing.ProvinceName = l.ProvinceName;
                listing.KPKNLName = l.KPKNLName;
                listing.CurrentStatusID = l.CurrentStatusID;
                listing.IsEmailAuction = l.IsEmailAuction;
                listing.IsFavorite = l.IsFavorite;
                model.Listing.Add(listing);
            }
            if (model.TotalRecords > 0)
            {
                model.AuctionDateText = model.Listing[0].AuctionDateText;
                model.KPKNLName = model.Listing[0].KPKNLName;
            }
            model.ScheduleID = scheduleID;
            return View("AssetListBySchedule", model);
        }
    }
}
