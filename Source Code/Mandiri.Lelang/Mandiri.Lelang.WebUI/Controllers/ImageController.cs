﻿using Mandiri.Lelang.BusinessFacade;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Mandiri.Lelang.WebUI.Controllers
{
    /// <summary>
    /// The image controller
    /// </summary>
    [AllowAnonymous]
    public class ImageController : BaseController
    {
        /// <summary>
        /// Get image
        /// </summary>
        /// <returns>The image</returns>
        [ActionName("get")]
        public void Get(long id, int idx, int resize = 0)
        {
            DataModel.Asset asset = AssetFacade.GetActiveAssetByAssetID(id).ReturnData;
            String path = ConfigurationManager.AppSettings["ImageUploadPath"];
            String fileName = String.Format("{0}no-image.jpg", path);
            //String contentType = "Image/Jpeg";
            String nameCheck = String.Empty;
            String resizeNameCheck = String.Empty;

            if (idx == 1)
            {
                if (!String.IsNullOrEmpty(asset.Photo1))
                {
                    nameCheck = String.Format("{0}{1}", path, asset.Photo1);
                    resizeNameCheck = String.Format("{0}RESIZE_{1}", path, asset.Photo1);
                }
            }
            else if (idx == 2)
            {
                if (!String.IsNullOrEmpty(asset.Photo2))
                {
                    nameCheck = String.Format("{0}{1}", path, asset.Photo2);
                    resizeNameCheck = String.Format("{0}RESIZE_{1}", path, asset.Photo2);
                }
            }
            else if (idx == 3)
            {
                if (!String.IsNullOrEmpty(asset.Photo3))
                {
                    nameCheck = String.Format("{0}{1}", path, asset.Photo3);
                    resizeNameCheck = String.Format("{0}RESIZE_{1}", path, asset.Photo3);
                }
            }
            else if (idx == 4)
            {
                if (!String.IsNullOrEmpty(asset.Photo4))
                {
                    nameCheck = String.Format("{0}{1}", path, asset.Photo4);
                    resizeNameCheck = String.Format("{0}RESIZE_{1}", path, asset.Photo4);
                }
            }
            else if (idx == 5)
            {
                if (!String.IsNullOrEmpty(asset.Photo5))
                {
                    nameCheck = String.Format("{0}{1}", path, asset.Photo5);
                    resizeNameCheck = String.Format("{0}RESIZE_{1}", path, asset.Photo5);
                }
            }

            if (resize == 1 && !String.IsNullOrEmpty(resizeNameCheck) && new FileInfo(resizeNameCheck).Exists)
            {
                fileName = resizeNameCheck;
            }
            else if (!String.IsNullOrEmpty(nameCheck) && new FileInfo(nameCheck).Exists)
            {
                fileName = nameCheck;
                /*if (resize == 1)
                {
                    WebImage image = new WebImage(fileName);
                    WebImage resizeImage = image.Resize(640, 480, false, false);
                    image.Save(resizeNameCheck);
                    image = null;
                    resizeImage = null;
                }*/

            }

            /*if (fileName.ToUpper().Contains("JPEG") || fileName.ToUpper().Contains("JPG"))
            {
                contentType = "Image/Jpeg";
            }
            else if (fileName.ToUpper().Contains("PNG"))
            {
                contentType = "Image/Png";
            }*/

            WebImage image = new WebImage(fileName);
            image.AddTextWatermark("https://lelang.bankmandiri.co.id", fontFamily: "Arial", fontColor: "Blue", fontSize: 14, fontStyle: "Bold", horizontalAlign: "Right", verticalAlign: "Bottom", opacity: 70);
            if (asset.CurrentStatusID == 5)
            {
                try
                {
                    image.AddTextWatermark("TERJUAL", fontFamily: "Stencil", fontColor: "Red", fontSize: 80, fontStyle: "Bold", horizontalAlign: "Center", verticalAlign: "Middle", opacity: 85);
                }
                catch
                {
                    image.AddTextWatermark("TERJUAL", fontColor: "Red", fontSize: 80, fontStyle: "Bold", horizontalAlign: "Center", verticalAlign: "Middle", opacity: 85);
                }
            }
            else if (asset.CurrentStatusID == 7)
            {
                try
                {
                    image.AddTextWatermark("LUNAS", fontFamily: "Stencil", fontColor: "Red", fontSize: 82, fontStyle: "Bold", horizontalAlign: "Center", verticalAlign: "Middle", opacity: 85);
                }
                catch
                {
                    image.AddTextWatermark("LUNAS", fontColor: "Red", fontSize: 82, fontStyle: "Bold", horizontalAlign: "Center", verticalAlign: "Middle", opacity: 85);
                }
            }
            image.Write();            

            //return File(fileName, contentType);
        }

        /// <summary>
        /// Get image
        /// </summary>
        /// <returns>The image</returns>
        [ActionName("setting")]
        public void GetFromSetting(String id)
        {
            String fileName = AdministrationFacade.GetActiveSystemSettingByID(id).ReturnData.Value;
            String path = ConfigurationManager.AppSettings["ImageUploadPath"];
            WebImage image = new WebImage(String.Format("{0}{1}", path, fileName));
            //image.AddTextWatermark("https://lelang.bankmandiri.co.id", fontFamily: "Arial", fontColor: "Blue", fontSize: 14, fontStyle: "Bold", horizontalAlign: "Right", verticalAlign: "Bottom", opacity: 70);            
            image.Write();
        }

        /// <summary>
        /// Get video thumbnail
        /// </summary>
        /// <returns>The video thumbnail</returns>
        [ActionName("video-thumbnail")]
        public void GetVideoThumbnail(int id = 0)
        {
            String path = ConfigurationManager.AppSettings["ImageUploadPath"];
            String nameCheck = String.Empty;
            if (id != 0)
            {
                var video = AssetFacade.GetActiveVideoByVideoID(id);
                if (video.IsSuccess)
                {
                    nameCheck = String.Format("{0}{1}", path, video.ReturnData.ThumbnailPath);
                    WebImage image = new WebImage(nameCheck);
                    image.Write();
                }
            }
        }
    }
}
