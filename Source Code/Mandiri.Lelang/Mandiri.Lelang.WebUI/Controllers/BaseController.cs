﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.Helper.Extension;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.Helper.Enum;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using FluentValidation.Results;
using Mandiri.Lelang.DataModel;

namespace Mandiri.Lelang.WebUI.Controllers
{
    /// <summary>
    /// The base class for all controller
    /// </summary>
    public class BaseController : Controller
    {
        #region Private Methods

        /// <summary>
        /// Initialize all view bag property that are available globally on all views and controllers
        /// </summary>
        private void SetViewBagWithGlobalProperty()
        {
            IList<Category> categories = MasterFacade.GetAllActiveCategories().ReturnData;
            TotalVisitor totalVisitor = AdministrationFacade.GetTotalVisitor().ReturnData;
            //add for CR2 for video gallery 20-09-2017 by khalid
            ViewBag.ThumbnailPath = ConfigurationManager.AppSettings["ImageUploadPath"];
            IList<Video> videos = MasterFacade.GetAllActiveVideos().ReturnData;
            ViewBag.Videos = videos;
            var videoSliderURL = MasterFacade.GetActiveShowInHomeVideo();
            string youtubeVideoID = "";
            if(videoSliderURL.IsSuccess)
            {
                youtubeVideoID = videoSliderURL.ReturnData.VideoURL;
                int indexVideoID = youtubeVideoID.IndexOf("?v=") + 3;
                youtubeVideoID = youtubeVideoID.Substring(indexVideoID, youtubeVideoID.Length - indexVideoID);
            }

            ViewBag.VideoSlider = "https://www.youtube.com/embed/" + youtubeVideoID;

            ViewBag.DefaultUnexpectedErrorMessage = TextResources.DefaultUnexpectedError_ErrorMessage;
            ViewBag.ConfirmationMessage = TextResources.Confirmation_Message;
            ViewBag.Categories = categories;
            ViewBag.TotalAsset = categories.Sum(m => m.CurrentAssetTotal).Value.ToString();
            ViewBag.TotalVisitorCount = totalVisitor.TotalVisitorCount;
            ViewBag.TotalTodayVisitorCount = totalVisitor.TotalTodayVisitorCount;
            ViewBag.RootPath = ConfigurationManager.AppSettings["RootPath"];
            if (SecurityHelper.IsAuthenticated())
            {
                ViewBag.FullName = Mandiri.Lelang.BusinessFacade.MemberFacade.GetActiveMemberByID(Mandiri.Lelang.Helper.SecurityHelper.GetCurrentLoggedInMemberID().Value).ReturnData.FullName;
            }
            else
            {
                ViewBag.FullName = String.Empty;
            }
        }

        /// <summary>
        /// Log visitor
        /// </summary>
        /*private void LogVisit()
        {
            AdministrationFacade.UpdateTotalVisitor();
        }*/

        #endregion

        #region Public Methods

        /// <summary>
        /// Add server side validation error message into the model state to be displayed on the form
        /// </summary>
        public void AddServerSideErrorToModelState()
        {
            IList<String> errorMessages = new List<String>();
            foreach (KeyValuePair<String, ModelState> state in ModelState.AsEnumerable())
            {
                for (int i = 0; i < state.Value.Errors.Count; i++)
                {
                    errorMessages.Add(state.Value.Errors[i].ErrorMessage);
                }
            }
            foreach (String errorMessage in errorMessages)
            {
                ModelState.AddModelError("", errorMessage);
            }
        }

        /// <summary>
        /// Add server side validation error message into the model state to be displayed on the form
        /// </summary>
        /// <param name="errors">List of errors</param>
        public void AddServerSideErrorToModelState(IList<ValidationFailure> errors)
        {
            foreach (ValidationFailure error in errors)
            {
                ModelState.AddModelError("", error.ErrorMessage);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public BaseController()
        {
            //LogVisit();
            SetViewBagWithGlobalProperty();
        }

        #endregion

        #region Override Methods

        /// <summary>
        /// Override the default handle unknown action to force redirect to the global error page
        /// </summary>
        /// <param name="actionName">The action name</param>
        protected override void HandleUnknownAction(string actionName)
        {
            if (this.HttpContext.IsCustomErrorEnabled)
            {
                //Redirect to the global error page
                TempData["GlobalErrorMessage"] = "Unknown Action";
                if (this.HttpContext.Request.IsAjaxRequest())
                {
                    this.JavaScriptRedirect("ErrorPage", "Error").ExecuteResult(this.ControllerContext);
                }
                else
                {
                    this.View("ErrorPage").ExecuteResult(this.ControllerContext);
                }
            }
        }

        /// <summary>
        /// Override the on exception to log the error and to force redirect to the global error page
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnException(ExceptionContext filterContext)
        {
            LogHelper.LogException(filterContext.Exception);

            //Output a nice error page with the exception message if the customError config set to On or if it's a remote web on RemoteOnly option
            if (filterContext.HttpContext.IsCustomErrorEnabled)
            {
                //Redirect to the global error page
                TempData["GlobalErrorMessage"] = filterContext.Exception.Source + "|" + filterContext.Exception.Message + "|" + filterContext.Exception.StackTrace;
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    if (bool.Parse(ConfigurationManager.AppSettings["ShowGrowlOnAjaxError"]))
                    {
                        base.OnException(filterContext);
                    }
                    else
                    {
                        filterContext.ExceptionHandled = true;
                        this.JavaScriptRedirect("ErrorPage", "Error").ExecuteResult(this.ControllerContext);
                    }
                }
                else
                {
                    filterContext.ExceptionHandled = true;
                    this.View("ErrorPage").ExecuteResult(this.ControllerContext);
                }
            }
            else
            {
                //Show the default error page if it's not an ajax request, and show a growl notification if it's an ajax request
                base.OnException(filterContext);
            }
        }

        #endregion
    }
}