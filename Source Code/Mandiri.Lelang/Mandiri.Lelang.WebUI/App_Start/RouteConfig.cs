﻿//using LowercaseDashedRouting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Mandiri.Lelang.WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            //routes.IgnoreRoute("favicon.ico");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            // BotDetect requests must not be routed
            routes.IgnoreRoute("{*botdetect}", new { botdetect = @"(.*)BotDetectCaptcha\.ashx" });

            /*routes.Add("SearchAllAsset", new LowercaseDashedRoute("asset/search",
                new RouteValueDictionary(
                    new { controller = "asset", action = "search" }),
                    new DashedRouteHandler()
                )
            );*/

            routes.MapRoute(
                name: "SearchAllAsset",
                url: "asset/search",
                defaults: new { controller = "asset", action = "search" }
            );

            /*routes.Add("SearchAsset", new LowercaseDashedRoute("asset/search/kategori/{categoryID}-{categoryName}/lokasi/{provinceID}/{cityID}/{address}/harga/{minPriceID}/{maxPriceID}/dokumen/{ownershipDocumentTypeID}/lelang/{auctionDate}/hot/{hotPrice}/mencari/{keywords}/sort/{sortIndex}/halaman/{page}/p/{currentPageIndex}/{*queryvalues}",
                new RouteValueDictionary(
                    new { controller = "asset", action = "search", categoryID = -1, provinceID = -1, cityID = -1, minPriceID = -1, maxPriceID = -1, ownershipDocumentTypeID = -1, address = "*", auctionDate = "*", hotPrice = false, keywords = "*", page = "1", sortIndex = 0, currentPageIndex = 0 }),
                    new DashedRouteHandler()
                )
            );*/

            routes.MapRoute(
                name: "SearchAsset",
                url: "asset/search/kategori/{categoryName}/{categoryID}/lokasi/{provinceName}/{cityName}/{address}/{provinceID}/{cityID}/harga/{minPriceName}-{maxPriceName}/{minPriceID}/{maxPriceID}/dokumen/{ownershipDocumentTypeName}/{ownershipDocumentTypeID}/lelang/{auctionDate}/hot/{hotPrice}/mencari/{keywords}/sort/{sortIndex}/p/{page}/i/{currentPageIndex}/{isVolunteerSellChecked}/{isAuctionChecked}/{*queryvalues}",
                defaults: new { controller = "asset", action = "search", categoryID = -1, provinceID = -1, cityID = -1, minPriceID = -1, maxPriceID = -1, ownershipDocumentTypeID = -1, address = "*", auctionDate = "*", hotPrice = "*", keywords = "*", page = "1", sortIndex = 0, currentPageIndex = 0, isVolunteerSellChecked = true, isAuctionChecked = true }
            );

            /*routes.MapRoute(
                name: "SearchAuctionSchedule",
                url: "schedule/list/lelang/{auctionDateFrom}-{auctionDateTo}/kpknl/{kpknl}/p/{page}/i/{currentPageIndex}/{*queryvalues}",
                defaults: new { controller = "asset", action = "search", categoryID = -1, provinceID = -1, cityID = -1, minPriceID = -1, maxPriceID = -1, ownershipDocumentTypeID = -1, address = "*", auctionDate = "*", page = "1", currentPageIndex = 0 }
            )*/

            routes.MapRoute(
                name: "Image",
                url: "image/get/{id}/{idx}/{resize}",
                defaults: new { controller = "image", action = "get", resize = 0 }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "home", action = "index", id = UrlParameter.Optional }
            );
        }
    }
}