﻿using Mandiri.Lelang.Attribute.ActionFilter;
using System.Web;
using System.Web.Mvc;

namespace Mandiri.Lelang.WebUI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute());
            filters.Add(new LogPageVisit());
        }
    }
}