using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class MemberPreference : BaseDataModel
	{ 
		public MemberPreference()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

		public MemberPreference(long memberPreferenceID, long memberID, int? minPriceID, int? maxPriceID)
		{
			this.MemberPreferenceID = memberPreferenceID;
			this.MemberID = memberID;
			this.MinPriceID = minPriceID;
			this.MaxPriceID = maxPriceID;
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

		public long MemberPreferenceID { get; set; }
		public long MemberID { get; set; }
		public int? MinPriceID { get; set; }
		public int? MaxPriceID { get; set; }
		public bool IsActive { get; set; }
		public int? CreatedBy { get; set; }
		public DateTime DateTimeCreated { get; set; }
		public int? LastUpdatedBy { get; set; }
		public DateTime? DateTimeUpdated { get; set; }
	}
}
