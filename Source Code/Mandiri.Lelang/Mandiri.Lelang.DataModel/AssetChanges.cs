using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class AssetChanges : BaseDataModel
	{ 
		public AssetChanges()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

	    public long? AssetChangesID { get; set; }
	    public long? AssetID { get; set; }
	    public bool? IsNew { get; set; }
	    public bool? IsUpdated { get; set; }
	    public bool? IsDeleted { get; set; }
        public byte? OriginalAssetStatusID { get; set; }
	    public int? CurrentApprovalStatus { get; set; }
	    public bool? IsActive { get; set; }
	    public DateTime? DateTimeCreated { get; set; }
	    public DateTime? DateTimeUpdated { get; set; }
	    public int? CreatedBy { get; set; }
	    public int? LastUpdatedBy { get; set; }
	}
}
