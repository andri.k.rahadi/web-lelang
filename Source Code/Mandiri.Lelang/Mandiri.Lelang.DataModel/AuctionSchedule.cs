using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
    public partial class AuctionSchedule : BaseDataModel
    {
        public AuctionSchedule()
        {
            this.IsActive = true;
            this.DateTimeCreated = DateTime.Now;
        }

        public long? AuctionScheduleID { get; set; }
        public int? KPKNLID { get; set; }
        public DateTime? ScheduleDate { get; set; }
        public DateTime? ScheduleTime { get; set; }
        public byte? Timezone { get; set; }
        public string AuctionAddress { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? DateTimeCreated { get; set; }
        public DateTime? DateTimeUpdated { get; set; }
        public int? CreatedBy { get; set; }
        public int? LastUpdatedBy { get; set; }
    }
}
