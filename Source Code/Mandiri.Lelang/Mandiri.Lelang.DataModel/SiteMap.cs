using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class SiteMap : BaseDataModel
	{ 
		public SiteMap()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

	    public string SiteMapID { get; set; }
	    public string MenuName { get; set; }
	    public string SiteMapParentID { get; set; }
	    public string Description { get; set; }
	    public string ActionName { get; set; }
	    public string ControllerName { get; set; }
	    public int? Level { get; set; }
	    public bool? HaveChildren { get; set; }
	    public bool? IsHiddenMenu { get; set; }
	    public int? Order { get; set; }
	    public bool? IsActive { get; set; }
	    public DateTime? DateTimeCreated { get; set; }
	    public DateTime? DateTimeUpdated { get; set; }
	    public int? CreatedBy { get; set; }
	    public string LastUpdatedBy { get; set; }
	}
}
