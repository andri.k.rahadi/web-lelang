﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
    public partial class MessageStatus : BaseDataModel
    {
        public MessageStatus()
        { 
            this.IsActive = true;
            this.DateTimeCreated = DateTime.Now;
        }
        public MessageStatus(int messageStatusID, string name, string description, bool isComplete, DateTime? dateTimeUpdated, DateTime? dateTimeInActive, int? createdBy, int? lastUpdatedBy)
        {
            this.MessageStatusID = messageStatusID;
            this.Name = name;
            this.Description = description;
            this.IsComplete = isComplete;
            this.DateTimeUpdated = dateTimeUpdated;
            this.DateTimeInActive = dateTimeInActive;
            this.CreatedBy = createdBy;
            this.LastUpdatedBy = lastUpdatedBy;
            this.IsActive = true;
            this.DateTimeCreated = DateTime.Now;
        }

        public int MessageStatusID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsComplete { get; set; }
        public bool IsActive { get; set; }
        public DateTime DateTimeCreated { get; set; }
        public DateTime? DateTimeUpdated { get; set; }
        public DateTime? DateTimeInActive { get; set; }
        public int? CreatedBy { get; set; }
        public int? LastUpdatedBy { get; set; }
    }
}
