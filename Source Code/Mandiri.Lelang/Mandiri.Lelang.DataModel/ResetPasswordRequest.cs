using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class ResetPasswordRequest : BaseDataModel
	{ 
		public ResetPasswordRequest()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

		public ResetPasswordRequest(long resetPasswordRequestID, string email, long? memberID, string requestToken, bool isUsed)
		{
			this.ResetPasswordRequestID = resetPasswordRequestID;
			this.Email = email;
			this.MemberID = memberID;
			this.RequestToken = requestToken;
			this.IsUsed = isUsed;
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

		public long ResetPasswordRequestID { get; set; }
		public string Email { get; set; }
		public long? MemberID { get; set; }
		public string RequestToken { get; set; }
		public bool IsUsed { get; set; }
		public bool IsActive { get; set; }
		public int? CreatedBy { get; set; }
		public DateTime DateTimeCreated { get; set; }
		public int? LastUpdatedBy { get; set; }
		public DateTime? DateTimeUpdated { get; set; }
	}
}
