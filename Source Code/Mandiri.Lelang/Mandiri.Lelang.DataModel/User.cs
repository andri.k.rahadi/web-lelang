using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class User : BaseDataModel
	{ 
		public User()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

	    public int? UserID { get; set; }
	    public string Username { get; set; }
	    public string FullName { get; set; }
	    public string PasswordSalt { get; set; }
	    public string HashPassword { get; set; }
	    public string Email { get; set; }
	    public int? BranchID { get; set; }
	    public byte? FailedLoginAttempt { get; set; }
	    public bool? IsLocked { get; set; }
        public DateTime? LastLoggedIn { get; set; }
        public DateTime? LastPasswordChanged { get; set; }
        public DateTime? LastActivity { get; set; }
        public bool? IsLoggedIn { get; set; }
        public String SessionID { get; set; }
	    public bool? IsActive { get; set; }
	    public DateTime? DateTimeCreated { get; set; }
	    public DateTime? DateTimeUpdated { get; set; }
	    public int? CreatedBy { get; set; }
	    public int? LastUpdatedBy { get; set; }
	}
}
