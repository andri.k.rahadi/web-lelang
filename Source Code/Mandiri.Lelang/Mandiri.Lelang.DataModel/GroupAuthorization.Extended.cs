using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class GroupAuthorization : BaseDataModel
	{
        public Group Group { get; set; }
        public SiteMap SiteMap { get; set; }
	}
}
