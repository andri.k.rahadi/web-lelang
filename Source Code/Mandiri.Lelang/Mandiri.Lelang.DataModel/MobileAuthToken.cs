using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class MobileAuthToken : BaseDataModel
	{ 
		public MobileAuthToken()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

		public MobileAuthToken(long mobileAuthTokenID, long memberID, string token, string salt, DateTime validUntilDateTime)
		{
			this.MobileAuthTokenID = mobileAuthTokenID;
			this.MemberID = memberID;
			this.Token = token;
			this.Salt = salt;
			this.ValidUntilDateTime = validUntilDateTime;
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

		public long MobileAuthTokenID { get; set; }
		public long MemberID { get; set; }
		public string Token { get; set; }
		public string Salt { get; set; }
		public DateTime ValidUntilDateTime { get; set; }
		public bool IsActive { get; set; }
		public int? CreatedBy { get; set; }
		public DateTime DateTimeCreated { get; set; }
		public int? LastUpdatedBy { get; set; }
		public DateTime? DateTimeUpdated { get; set; }
	}
}
