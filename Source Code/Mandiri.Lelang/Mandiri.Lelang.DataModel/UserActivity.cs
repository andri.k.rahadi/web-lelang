using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class UserActivity : BaseDataModel
	{ 
		public UserActivity()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

	    public long? UserActivityID { get; set; }
	    public string UserActivityTypeID { get; set; }
	    public string ExtraDescription { get; set; }
	    public string ReferenceID { get; set; }
	    public bool? IsActive { get; set; }
	    public DateTime? DateTimeCreated { get; set; }
	    public DateTime? DateTimeUpdated { get; set; }
	    public int? CreatedBy { get; set; }
	    public int? LastUpdatedBy { get; set; }
	}
}
