using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class AssetChangesHistory : BaseDataModel
	{ 
		public AssetChangesHistory()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

	    public long? AssetChangesHistoryID { get; set; }
	    public long? AssetChangesID { get; set; }
	    public string FieldOrColumnName { get; set; }
        public string LabelName { get; set; }
	    public string OldValue { get; set; }
	    public string NewValue { get; set; }
	    public bool? IsActive { get; set; }
	    public DateTime? DateTimeCreated { get; set; }
	    public DateTime? DateTimeUpdated { get; set; }
	    public int? CreatedBy { get; set; }
	    public int? LastUpdatedBy { get; set; }
	}
}
