using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
    public partial class SiteMap : BaseDataModel
    {        
        public IList<SiteMap> ChildSiteMaps { get; set; }
    }
}
