using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class AssetWorkflowHistory : BaseDataModel
	{ 
		public AssetWorkflowHistory()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

	    public long? AssetWorkflowHistoryID { get; set; }
	    public long? AssetChangesID { get; set; }
	    public bool? IsApproved { get; set; }
	    public string ApprovedBy { get; set; }
	    public bool? IsRejected { get; set; }
	    public string RejectedBy { get; set; }
	    public string Reason { get; set; }
	    public bool? IsActive { get; set; }
	    public DateTime? DateTimeCreated { get; set; }
	    public DateTime? DateTimeUpdated { get; set; }
	    public int? CreatedBy { get; set; }
	    public int? LastUpdatedBy { get; set; }
	}
}
