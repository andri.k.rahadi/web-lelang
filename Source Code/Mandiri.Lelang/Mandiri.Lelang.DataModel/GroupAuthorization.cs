using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
    public partial class GroupAuthorization : BaseDataModel
    {
        public GroupAuthorization()
        {
            this.IsActive = true;
            this.DateTimeCreated = DateTime.Now;
        }

        public int? GroupAuthorizationID { get; set; }
        public int? GroupID { get; set; }
        public string SiteMapID { get; set; }
        public bool? AllowAccess { get; set; }
        public bool? AllowAdd { get; set; }
        public bool? AllowUpdate { get; set; }
        public bool? AllowDelete { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? DateTimeCreated { get; set; }
        public DateTime? DateTimeUpdated { get; set; }
        public int? CreatedBy { get; set; }
        public int? LastUpdatedBy { get; set; }
    }
}
