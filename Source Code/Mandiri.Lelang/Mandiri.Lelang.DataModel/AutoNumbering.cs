using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class AutoNumbering : BaseDataModel
	{ 
		public AutoNumbering()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

	    public long? AutoNumberingID { get; set; }
	    public int? AutoNumberingTypeID { get; set; }
	    public String SegmentID { get; set; }
	    public int? BranchID { get; set; }
	    public long? RunningNumber { get; set; }
	    public DateTime? LastResetDateTime { get; set; }
	    public bool? IsActive { get; set; }
	    public DateTime? DateTimeCreated { get; set; }
	    public DateTime? DateTimeUpdated { get; set; }
	    public int? CreatedBy { get; set; }
	    public int? LastUpdatedBy { get; set; }
	}
}
