using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class Branch : BaseDataModel
	{ 
		public Branch()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

	    public int? BranchID { get; set; }
	    public String Alias { get; set; }
	    public string Name { get; set; }
	    public string Description { get; set; }
	    public string Address { get; set; }
	    public string Phone1 { get; set; }
	    public string Phone2 { get; set; }
	    public bool? IsActive { get; set; }
	    public DateTime? DateTimeCreated { get; set; }
	    public DateTime? DateTimeUpdated { get; set; }
	    public int? CreatedBy { get; set; }
	    public int? LastUpdatedBy { get; set; }
	}
}
