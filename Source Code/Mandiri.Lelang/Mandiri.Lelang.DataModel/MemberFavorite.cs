using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class MemberFavorite : BaseDataModel
	{ 
		public MemberFavorite()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

		public MemberFavorite(long memberID, long assetID, bool isFavorite)
		{			
			this.MemberID = memberID;
			this.AssetID = assetID;
			this.IsFavorite = isFavorite;
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

		public long MemberFavoriteID { get; set; }
		public long MemberID { get; set; }
		public long AssetID { get; set; }
		public bool IsFavorite { get; set; }
		public bool IsActive { get; set; }
		public int? CreatedBy { get; set; }
		public DateTime DateTimeCreated { get; set; }
		public int? LastUpdatedBy { get; set; }
		public DateTime? DateTimeUpdated { get; set; }
	}
}
