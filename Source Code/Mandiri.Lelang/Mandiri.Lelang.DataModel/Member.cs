using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class Member : BaseDataModel
	{ 
		public Member()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

		public Member(long memberID, string fullName, string email, string placeOfBirth, DateTime? dateOfBirth, string mobilePhoneNumber, string address, int? provinceID, int? cityID, string identificationNumber, string passwordSalt, string hashPassword, string activationToken, bool isLocked, string sessionID, DateTime? lastLoggedIn, DateTime? lastPasswordChanged)
		{
			this.MemberID = memberID;
			this.FullName = fullName;
			this.Email = email;
			this.PlaceOfBirth = placeOfBirth;
			this.DateOfBirth = dateOfBirth;
			this.MobilePhoneNumber = mobilePhoneNumber;
			this.Address = address;
			this.ProvinceID = provinceID;
			this.CityID = cityID;
			this.IdentificationNumber = identificationNumber;
			this.PasswordSalt = passwordSalt;
			this.HashPassword = hashPassword;
			this.ActivationToken = activationToken;
			this.IsLocked = isLocked;
			this.SessionID = sessionID;
			this.LastLoggedIn = lastLoggedIn;
			this.LastPasswordChanged = lastPasswordChanged;
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

		public long MemberID { get; set; }
		public string FullName { get; set; }
		public string Email { get; set; }
		public string PlaceOfBirth { get; set; }
		public DateTime? DateOfBirth { get; set; }
		public string MobilePhoneNumber { get; set; }
		public string Address { get; set; }
		public int? ProvinceID { get; set; }
		public int? CityID { get; set; }
		public string IdentificationNumber { get; set; }
		public string PasswordSalt { get; set; }
		public string HashPassword { get; set; }
		public string ActivationToken { get; set; }
		public bool IsLocked { get; set; }
		public string SessionID { get; set; }
		public DateTime? LastLoggedIn { get; set; }
		public DateTime? LastPasswordChanged { get; set; }
		public bool IsActive { get; set; }
		public int? CreatedBy { get; set; }
		public DateTime DateTimeCreated { get; set; }
		public int? LastUpdatedBy { get; set; }
		public DateTime? DateTimeUpdated { get; set; }
	}
}
