using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class MemberCityPreference : BaseDataModel
	{ 
		public MemberCityPreference()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

		public MemberCityPreference(long memberCityPreferenceID, long memberPreferenceID, int cityID)
		{
			this.MemberCityPreferenceID = memberCityPreferenceID;
			this.MemberPreferenceID = memberPreferenceID;
			this.CityID = cityID;
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

		public long MemberCityPreferenceID { get; set; }
		public long MemberPreferenceID { get; set; }
		public int CityID { get; set; }
		public bool IsActive { get; set; }
		public int? CreatedBy { get; set; }
		public DateTime DateTimeCreated { get; set; }
		public int? LastUpdatedBy { get; set; }
		public DateTime? DateTimeUpdated { get; set; }
	}
}
