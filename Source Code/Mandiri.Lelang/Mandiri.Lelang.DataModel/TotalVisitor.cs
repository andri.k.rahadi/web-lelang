using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class TotalVisitor : BaseDataModel
	{ 
		public TotalVisitor()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

	    public byte? TotalVisitorID { get; set; }
	    public long? TotalVisitorCount { get; set; }
	    public long? TotalTodayVisitorCount { get; set; }
	    public bool? IsActive { get; set; }
	    public DateTime? DateTimeCreated { get; set; }
	    public DateTime? DateTimeUpdated { get; set; }
	    public int? CreatedBy { get; set; }
	    public int? LastUpdatedBy { get; set; }
	}
}
