﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
    public partial class Video : BaseDataModel
    {
        public Video()
        { 
            this.IsActive = true;
            this.DateTimeCreated = DateTime.Now;
        }
        public Video(int videoID, string title, string thumbnailPath, string description, string videoURL, bool showInHome, DateTime? dateTimeUpdated, DateTime? dateTimeInActive, int? createdBy, int? lastUpdatedBy)
        {
            this.VideoID = videoID;
            this.Title = title;
            this.ThumbnailPath = thumbnailPath;
            this.Description = description;
            this.VideoURL = videoURL;
            this.ShowInHome = showInHome;
            this.DateTimeUpdated = dateTimeUpdated;
            this.DateTimeInActive = dateTimeInActive;
            this.CreatedBy = createdBy;
            this.LastUpdatedBy = lastUpdatedBy;
            this.IsActive = true;
            this.DateTimeCreated = DateTime.Now;
        }

        public int VideoID { get; set; }
        public string Title { get; set; }
        public string ThumbnailPath { get; set; }
        public string Description { get; set; }
        public string VideoURL { get; set; }
        public bool ShowInHome { get; set; }
        public bool IsActive { get; set; }
        public DateTime DateTimeCreated { get; set; }
        public DateTime? DateTimeUpdated { get; set; }
        public DateTime? DateTimeInActive { get; set; }
        public int? CreatedBy { get; set; }
        public int? LastUpdatedBy { get; set; }
    }
}
