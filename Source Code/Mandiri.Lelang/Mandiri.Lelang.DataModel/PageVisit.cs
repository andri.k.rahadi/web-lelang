using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class PageVisit : BaseDataModel
	{ 
		public PageVisit()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

	    public long? PageVisitID { get; set; }
	    public string IPAddress { get; set; }
	    public string URLReferrerSiteMapID { get; set; }
	    public string SiteMapID { get; set; }
	    public long? AssetID { get; set; }
	    public string AdditionalParams { get; set; }
        public long? MemberID { get; set; }
        public int? CityID { get; set; }
	    public bool? IsActive { get; set; }
	    public DateTime? DateTimeCreated { get; set; }
	    public DateTime? DateTimeUpdated { get; set; }
	    public int? CreatedBy { get; set; }
	    public int? LastUpdatedBy { get; set; }
	}
}
