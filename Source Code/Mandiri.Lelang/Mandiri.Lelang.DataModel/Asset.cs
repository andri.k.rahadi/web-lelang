using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class Asset : BaseDataModel
	{ 
		public Asset()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

	    public long? AssetID { get; set; }
	    public string AssetCode { get; set; }
	    public string AccountNumber { get; set; }
	    public string AccountName { get; set; }
	    public int? CategoryID { get; set; }
	    public string Address { get; set; }
	    public int? ProvinceID { get; set; }
	    public int? CityID { get; set; }
	    public decimal? OldPrice { get; set; }
	    public decimal? NewPrice { get; set; }
	    public int? BranchID { get; set; }
	    public decimal? MarketValue { get; set; }
	    public decimal? LiquidationValue { get; set; }
	    public decimal? SecurityRightsValue { get; set; }
        public decimal? SoldValue { get; set; }
	    public DateTime? ValuationDate { get; set; }
	    public long? AuctionScheduleID { get; set; }
	    public int? AuctionHallID { get; set; }
        public string AuctionAddress { get; set; }
        public bool? IsEmailAuction { get; set; }
        public DateTime? LastAutoUpdatedDateTime { get; set; }
        public string EmailAuctionURL { get; set; }
	    public string AuctionHallAddress { get; set; }
	    public string AuctionHallPhone1 { get; set; }
	    public string AuctionHallPhone2 { get; set; }
	    public int? KPKNLID { get; set; }
	    public string KPKNLAddress { get; set; }
	    public string KPKNLPhone1 { get; set; }
	    public string KPKNLPhone2 { get; set; }
	    public string SegmentID { get; set; }
	    public string Photo1 { get; set; }
	    public string Photo2 { get; set; }
	    public string Photo3 { get; set; }
	    public string Photo4 { get; set; }
	    public string Photo5 { get; set; }
	    public int? OwnershipDocumentTypeID { get; set; }
	    public byte? CurrentStatusID { get; set; }
	    public double? Latitude { get; set; }
	    public double? Longitude { get; set; }
	    public string TransferInformation { get; set; }
	    public string AdditionalNotes { get; set; }
        public string DocumentNumber { get; set; }
	    public bool? IsActive { get; set; }
	    public DateTime? DateTimeCreated { get; set; }
	    public DateTime? DateTimeUpdated { get; set; }
	    public int? CreatedBy { get; set; }
	    public int? LastUpdatedBy { get; set; }
        public byte Type { get; set; }
        public decimal? Price { get; set; }
        public DateTime? ExpiredDate { get; set; }
	}
}
