using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class MemberCategoryPreference : BaseDataModel
	{ 
		public MemberCategoryPreference()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

		public MemberCategoryPreference(long memberCategoryPreferenceID, long memberPreferenceID, int categoryID)
		{
			this.MemberCategoryPreferenceID = memberCategoryPreferenceID;
			this.MemberPreferenceID = memberPreferenceID;
			this.CategoryID = categoryID;
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

		public long MemberCategoryPreferenceID { get; set; }
		public long MemberPreferenceID { get; set; }
		public int CategoryID { get; set; }
		public bool IsActive { get; set; }
		public int? CreatedBy { get; set; }
		public DateTime DateTimeCreated { get; set; }
		public int? LastUpdatedBy { get; set; }
		public DateTime? DateTimeUpdated { get; set; }
	}
}
