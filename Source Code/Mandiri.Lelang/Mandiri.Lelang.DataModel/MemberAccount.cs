using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class MemberAccount : BaseDataModel
	{ 
		public MemberAccount()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

		public MemberAccount(long memberAccountID, long memberID, string socialToken, string socialID, string socialUsername, string socialURL, string source, bool isActivated)
		{
			this.MemberAccountID = memberAccountID;
			this.MemberID = memberID;
			this.SocialToken = socialToken;
			this.SocialID = socialID;
			this.SocialUsername = socialUsername;
			this.SocialURL = socialURL;
			this.Source = source;
			this.IsActivated = isActivated;
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

		public long MemberAccountID { get; set; }
		public long MemberID { get; set; }
		public string SocialToken { get; set; }
		public string SocialID { get; set; }
		public string SocialUsername { get; set; }
		public string SocialURL { get; set; }
		public string Source { get; set; }
		public bool IsActivated { get; set; }
		public bool IsActive { get; set; }
		public int? CreatedBy { get; set; }
		public DateTime DateTimeCreated { get; set; }
		public int? LastUpdatedBy { get; set; }
		public DateTime? DateTimeUpdated { get; set; }
	}
}
