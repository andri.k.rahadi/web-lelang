using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class AssetStatusHistory : BaseDataModel
	{ 
		public AssetStatusHistory()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

	    public long? AssetStatusHistoryID { get; set; }
	    public long? AssetID { get; set; }
	    public byte? PrevAssetStatusID { get; set; }
	    public byte? NewAssetStatusID { get; set; }
	    public string Notes { get; set; }
	    public bool? IsActive { get; set; }
	    public DateTime? DateTimeCreated { get; set; }
	    public DateTime? DateTimeUpdated { get; set; }
	    public int? CreatedBy { get; set; }
	    public int? LastUpdatedBy { get; set; }
	}
}
