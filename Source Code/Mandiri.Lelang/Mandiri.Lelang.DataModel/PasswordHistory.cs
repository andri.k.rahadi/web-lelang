﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
    public partial class PasswordHistory : BaseDataModel
    {
        public PasswordHistory()
        {
            this.DateTimeCreated = DateTime.Now;
        }

        public long? PasswordHistoryID { get; set; }
        public int? UserID { get; set; }
        public string HashPassword { get; set; }
        public DateTime? DateTimeCreated { get; set; }
        public DateTime? DateTimeUpdated { get; set; }
        public int? CreatedBy { get; set; }
        public int? LastUpdatedBy { get; set; }
    }
}
