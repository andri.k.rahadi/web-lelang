using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
	public partial class ErrorLog : BaseDataModel
	{ 
		public ErrorLog()
		{
			this.IsActive = true;
			this.DateTimeCreated = DateTime.Now;
		}

	    public long? ErrorLogID { get; set; }
	    public string ErrorMessage { get; set; }
	    public bool? IsActive { get; set; }
	    public DateTime? DateTimeCreated { get; set; }
	    public DateTime? DateTimeUpdated { get; set; }
	    public int? CreatedBy { get; set; }
	    public int? LastUpdatedBy { get; set; }
	}
}
