using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mandiri.Lelang.DataModel
{
    public partial class Message : BaseDataModel
    {
        public Message()
        {
            this.IsActive = true;
            this.DateTimeCreated = DateTime.Now;
        }
        public Message(long messageID, byte messageCategoryTypeID, string name, string phone, string email, string address, string textMessage, string assetCode, int? branchID, string iPAddress, DateTime? dateTimeUpdated, int? createdBy, int? lastUpdatedBy, int messageStatusID, string remarks)
        {
            this.MessageID = messageID;
            this.MessageCategoryTypeID = messageCategoryTypeID;
            this.Name = name;
            this.Phone = phone;
            this.Email = email;
            this.Address = address;
            this.TextMessage = textMessage;
            this.AssetCode = assetCode;
            this.BranchID = branchID;
            this.IPAddress = iPAddress;
            this.DateTimeUpdated = dateTimeUpdated;
            this.CreatedBy = createdBy;
            this.LastUpdatedBy = lastUpdatedBy;
            this.MessageStatusID = messageStatusID;
            this.Remarks = remarks;
            this.IsActive = true;
            this.DateTimeCreated = DateTime.Now;
        }

        public long MessageID { get; set; }
        public byte MessageCategoryTypeID { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string TextMessage { get; set; }
        public string AssetCode { get; set; }
        public int? BranchID { get; set; }
        public string IPAddress { get; set; }
        public bool IsActive { get; set; }
        public DateTime? DateTimeCreated { get; set; }
        public DateTime? DateTimeUpdated { get; set; }
        public int? CreatedBy { get; set; }
        public int? LastUpdatedBy { get; set; }
        public int MessageStatusID { get; set; }
        public string Remarks { get; set; }
    }
}
 