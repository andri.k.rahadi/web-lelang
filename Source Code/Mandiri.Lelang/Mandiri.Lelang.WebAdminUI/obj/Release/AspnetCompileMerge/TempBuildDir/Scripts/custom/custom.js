﻿var rootPath = '';

function getUrl(url) {
    return rootPath + url;
}

function bindValidator(formName) {
    $.validator.unobtrusive.parse("#" + formName);
}

function generateAjaxActionLink(text, targetUpdateId, url, routeValues) {
    url = rootPath + url;
    return '<a data-ajax="true" class="btn btn-info btn-sm" data-ajax-begin="OnAjaxRequestBegin" data-ajax-failure="OnAjaxRequestFailure" data-ajax-mode="replace" data-ajax-success="OnAjaxRequestSuccess" data-ajax-update="#' + targetUpdateId + '" href="' + url + '?' + routeValues + '">' + text + '</a>';
}

function generateActionLink(text, url, routeValues) {
    url = rootPath + url;
    return '<a class="btn btn-info btn-sm" href="' + url + '?' + routeValues + '">' + text + '</a>';
}

function ajaxRowAction(tableName, url, id, errorMessage) {
    url = rootPath + url;
    if (confirm(errorMessage)) {
        $.post(url, id)
            .done(function (data) {
                if (data.errorMessage != "") {
                    alert(data.errorMessage);
                }
                else {
                    reloadDataTables(tableName);
                }
            });
    }
}

function applySplitter() {
    $('body').layout({
        applyDemoStyles: true,
        west: {
            size: "60%"
        }
    });
}

function showUnexpectedErrorGrowlNotification(errorMessage, topMargin) {
    $.blockUI({
        message: '<div><h5>' + errorMessage + '</h5></div>',
        fadeIn: 700,
        fadeOut: 700,
        timeout: 3000,
        showOverlay: false,
        centerY: false,
        css: {
            width: '350px',
            top: topMargin,
            left: '',
            right: '10px',
            border: 'none',
            padding: '5px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .6,
            color: '#fff'
        }
    });
}

function reloadDataTables(tableName) {
    var table = $("#" + tableName).DataTable();
    var txtKeywords = $("#txtKeywords");
    txtKeywords.val("");
    table.ajax.url(table.ajax.url()).load();
}

function refreshDataTables(tableName, url, updatePrice) {
    url = rootPath + url;
    var table = $("#" + tableName).DataTable();
    var txtKeywords = $("#txtKeywords");
    txtKeywords.val("");
    if (updatePrice !== undefined && updatePrice != 0) {
        table.ajax.url(url + "?keywords=&updatePrice=1").load();        
    } else {
        table.ajax.url(url + "?keywords=").load();
    }
    
}

function refreshDataTablesforMessage(tableName, url, isPending) {
    url = rootPath + url;
    var table = $("#" + tableName).DataTable();
    var txtKeywords = $("#txtKeywords");
    txtKeywords.val("");
    if (isPending !== undefined && isPending != 0) {
        table.ajax.url(url + "?keywords=&isPending=1").load();
    } else {
        table.ajax.url(url + "?keywords=").load();
    }

}

function searchDataTables(tableName, url, updatePrice) {
    url = rootPath + url;
    var table = $("#" + tableName).DataTable();
    var txtKeywords = $("#txtKeywords");
    if (updatePrice !== undefined && updatePrice != 0) {
        table.ajax.url(url + "?keywords=" + txtKeywords.val() + "&updatePrice=1").load();
    } else {
        table.ajax.url(url + "?keywords=" + txtKeywords.val()).load();
    }

}

function searchDataTablesforMessage(tableName, url, isPending) {
    url = rootPath + url;
    var table = $("#" + tableName).DataTable();
    var txtKeywords = $("#txtKeywords");
    if (isPending !== undefined && isPending != 0) {
        table.ajax.url(url + "?keywords=" + txtKeywords.val() + "&isPending=1").load();
    } else {
        table.ajax.url(url + "?keywords=" + txtKeywords.val()).load();
    }

}

function loadDataTables(tableName, ajaxUrl, columnsOptions, defaultSortDirection) {
    ajaxUrl = rootPath + ajaxUrl;
    var dataTable = $("#" + tableName).dataTable({
        "processing": false,
        "serverSide": true,
        "deferLoading": 0,
        "order": [[0, defaultSortDirection]],
        "ordering": true,
        "searching": false,
        "paging": true,
        "columns": columnsOptions,
        "ajax": {
            url: ajaxUrl,
            type: 'POST'
        }
    });
}

/* Function added By Hengki */

function searchHistoryDataTables(tableName, url, params) {
    url = rootPath + url;
    var table = $("#" + tableName).DataTable();
    var txtKeywords = $("#txtKeywords");
    table.ajax.url(url + "?assetID=" + params + "&keywords=" + txtKeywords.val()).load();
}

function searchAssetHistoryDataTables(tableName, url, params) {
    url = rootPath + url;
    var table = $("#" + tableName).DataTable();
    var txtKeywords = $("#txtKeywords");
    table.ajax.url(url + "?assetChangesID=" + params).load();
}

function loadHistoryDataTables(tableName, ajaxUrl, columnsOptions, defaultSortDirection) {
    ajaxUrl = rootPath + ajaxUrl;
    var dataTable = $("#" + tableName).dataTable({
        "processing": false,
        "serverSide": true,
        "deferLoading": 0,
        "order": [[0, defaultSortDirection]],
        "ordering": false,
        "searching": false,
        "paging": false,
        "columns": columnsOptions,
        "ajax": {
            url: ajaxUrl,
            type: 'POST'
        }
    });
}

function generateActionLinkAsButton(text, url, routeValues, strClass) {
    url = rootPath + url;
    return '<a class="' + strClass + '" href="' + url + '?' + routeValues + '">' + text + '</a>';
}

function jqUpdateSize() {
    // Get the dimensions of the viewport
    var width = $(window).width() - 10;
    var height = $(window).height() - 120;

    $('iframe').attr("width", width);
    $('iframe').attr("height", height);
};

/* ---- End Of Added ----- */