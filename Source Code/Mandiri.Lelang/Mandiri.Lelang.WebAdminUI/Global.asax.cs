﻿using FluentValidation.Mvc;
using Mandiri.Lelang.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Mandiri.Lelang.WebAdminUI
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            FluentValidationModelValidatorProvider.Configure();
            //ModelBinders.Binders.Add(typeof(Datatables.Mvc.DataTable), new Datatables.Mvc.DataTableModelBinder());
            ModelBinders.Binders.Add(typeof(DateTime?), new IndonesianDateTimeModelBinder());
            ModelBinders.Binders.Add(typeof(DateTime), new IndonesianDateTimeModelBinder());
        }

        /// <summary>
        /// Global application error handler
        /// </summary>
        protected void Application_Error()
        {
            LogHelper.LogException(Server.GetLastError());            
        }

        protected void Session_Start()
        {
            Session["init"] = 0;
        }
    }
}