﻿using System.Web;
using System.Web.Optimization;

namespace Mandiri.Lelang.WebAdminUI
{
    /// <summary>
    /// Handling configuration for bundling and minification
    /// </summary>
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        /// <summary>
        /// Register all bundles
        /// </summary>
        /// <param name="bundles">The bundle collection object</param>
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/ie9scripts").Include(                        
                        "~/Scripts/html5shiv.js",
                        "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui-{version}.js",
                        "~/Scripts/globalize/globalize.js",
                        "~/Scripts/globalize/cultures/globalize.culture." + System.Globalization.CultureInfo.CurrentCulture.ToString() + ".js",
                        "~/Scripts/bootstrap*",
                        "~/Scripts/filebutton.js",
                        "~/Scripts/globalize-datepicker.js",
                        "~/Scripts/jquery.unobtrusive-ajax.js",
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/validation.js",
                        "~/Scripts/jquery.blockUI*",
                        "~/Scripts/DataTables-1.10.2/jquery.dataTables*",
                        "~/Scripts/DataTables-1.10.2/dataTables.bootstrap.js",
                        "~/Scripts/jquery.layout-latest.js",
                        "~/Scripts/jquery.iframe-transport.js",
                        "~/Scripts/jquery.fileupload.js",
                        "~/Scripts/sha256.js",
                        "~/Scripts/md5.js",
                        "~/Scripts/autoNumeric/autoNumeric-min.js",
                        "~/Scripts/custom/custom.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                "~/Content/themes/bootstrap/bootstrap.css",
                "~/Content/DataTables-1.10.2/css/dataTables.bootstrap.css",
                "~/Content/font-awesome.min.css",
                "~/Content/site.css"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/summernotescripts").Include(
                        "~/Scripts/SummerNote/summernote.js"));

            bundles.Add(new StyleBundle("~/Content/summernotecss").Include(
                "~/Content/SummerNote/summernote.css"));

            //Not needed yet
            /*bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));*/
        }
    }
}