﻿using Mandiri.Lelang.Attribute.ActionFilter;
using System.Web;
using System.Web.Mvc;

namespace Mandiri.Lelang.WebAdminUI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute());
            //Make all controllers only allowed for authorized users (use AllowAnonymous attribute to disable this)
            filters.Add(new AuthorizeAttribute());
            filters.Add(new PasswordExpiryValidity());
            filters.Add(new LogUserActivityDateTime());
        }
    }
}