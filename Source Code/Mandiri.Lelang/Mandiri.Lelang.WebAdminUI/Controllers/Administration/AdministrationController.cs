﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.Helper.Enum;
using Mandiri.Lelang.Helper.Extension;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.WebAdmin;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.Attribute.ActionFilter;
using Mandiri.Lelang.Attribute.Enum;
using System.Dynamic;
using Mandiri.Lelang.DataModel;
using FluentValidation.Mvc;
using System.Configuration;
using System.IO;

namespace Mandiri.Lelang.WebAdminUI.Controllers.Master
{
    public class AdministrationController : BaseController
    {
        #region Action

        #region Group
        /// <summary>
        /// A controller to return the default view for group
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The group view</returns>
        [ActionName("Group")]
        [AuthorizeAccess("Group")]
        public ActionResult GroupListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.GroupView_Title;
            GroupViewModel model = new GroupViewModel();

            return View("GroupList", model);
        }

        /// <summary>
        /// The action to view/add/edit selected group
        /// </summary>
        /// <param name="groupID">The group id</param>
        /// <returns>The group view</returns>
        [ActionName("AddOrUpdateGroup")]
        [AuthorizeAccess("Group")]
        public ActionResult AddOrUpdateGroupView(int? groupID)
        {
            ViewBag.Title = TextResources.GroupView_Title;
            GroupViewModel model = new GroupViewModel();

            if (groupID.HasValue)
            {
                DataModel.Group group = AdministrationFacade.GetActiveGroupByGroupID(groupID.Value).ReturnData;
                if (group != null)
                {
                    model.GroupID = group.GroupID.Value;
                    model.Name = group.Name;
                    model.Description = group.Description;
                    model.IsEditMode = true;
                    return View("GroupForm", model);
                }
                else
                {
                    model.IsEditMode = false;
                    return View("GroupForm", model);
                }
            }
            else
            {
                model.IsEditMode = false;
                return View("GroupForm", model);
            }
        }

        /// <summary>
        /// Add or update group
        /// </summary>
        /// <param name="groupViewModel">The group view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Group")]
        public ActionResult AddOrUpdateGroup(GroupViewModel groupViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData["Model"] = groupViewModel;
                if (groupViewModel.IsEditMode)
                {
                    return RedirectToAction("UpdateGroup");
                }
                else
                {
                    return RedirectToAction("AddGroup");
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add group
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("Group", AuthorizationActionType.AllowAdd)]
        public ActionResult AddGroup()
        {
            FacadeResult<DataModel.Group> result;
            DataModel.Group group = new DataModel.Group();
            GroupViewModel groupViewModel = (GroupViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                group.Name = groupViewModel.Name;
                group.Description = groupViewModel.Description;

                result = AdministrationFacade.AddNewGroup(group, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "Group", "Administration");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update group
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("Group", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateGroup()
        {
            FacadeResult<DataModel.Group> result;
            DataModel.Group group = new DataModel.Group();
            GroupViewModel groupViewModel = (GroupViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                group.GroupID = groupViewModel.GroupID;
                group.Name = groupViewModel.Name;
                group.Description = groupViewModel.Description;

                result = AdministrationFacade.UpdateGroup(group, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "Group", "Administration");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Get all active group list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON group data tables result</returns>
        [HttpPost]
        [ActionName("GroupList")]
        [AuthorizeAccess("Group")]
        [ValidateSQLInjection]
        public ActionResult GetGroupList(String keywords, int draw, int start, int length)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> groups = AdministrationFacade.GetActiveGroupsList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < groups.Count; i++)
            {
                data.Add(new String[] { groups[i].GroupID.ToString(), groups[i].Name, groups[i].Description, groups[i].GroupID.ToString(), String.Format("{{groupID:{0}}}", groups[i].GroupID.ToString()) });
            }
            return Json(new { draw = draw, recordsTotal = AdministrationFacade.GetActiveGroupsCount().ReturnData, recordsFiltered = AdministrationFacade.GetActiveGroupsCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// Delete group by group id
        /// </summary>
        /// <param name="groupID">The group id</param>
        /// <returns>JSON result</returns>
        [HttpPost]
        [AuthorizeAction("Group", AuthorizationActionType.AllowDelete)]
        public ActionResult DeleteGroup(int groupID)
        {
            FacadeResult<DataModel.Group> result = AdministrationFacade.DeleteGroupByID(groupID, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }

        #endregion

        #region UserGroupMapping
        /// <summary>
        /// A controller to return the default view for UserGroupMapping
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The UserGroupMapping view</returns>
        [ActionName("UserGroupMapping")]
        [AuthorizeAccess("UserGroupMapping")]
        public ActionResult UserGroupMappingListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.UserGroupMappingView_Title;
            UserGroupMappingViewModel model = new UserGroupMappingViewModel();

            return View("UserGroupMappingList", model);
        }

        /// <summary>
        /// The action to view/add/edit selected UserGroupMapping
        /// </summary>
        /// <param name="UserGroupMappingID">The UserGroupMapping id</param>
        /// <returns>The UserGroupMapping view</returns>
        [ActionName("AddOrUpdateUserGroupMapping")]
        [AuthorizeAccess("UserGroupMapping")]
        public ActionResult AddOrUpdateUserGroupMappingView(int? userGroupMappingID)
        {
            ViewBag.Title = TextResources.UserGroupMappingView_Title;
            UserGroupMappingViewModel model = new UserGroupMappingViewModel();

            model.Groups = AdministrationFacade.GetAllActiveGroups().ReturnData.AsEnumerable();
            model.Users = AdministrationFacade.GetAllActiveUsers().ReturnData.AsEnumerable();
            if (userGroupMappingID.HasValue)
            {
                DataModel.UserGroupMapping userGroupMapping = AdministrationFacade.GetActiveUserGroupMappingByUserGroupMappingID(userGroupMappingID.Value).ReturnData;
                if (userGroupMapping != null)
                {
                    model.UserGroupMappingID = userGroupMapping.UserGroupMappingID.Value;
                    model.GroupID = userGroupMapping.GroupID.Value;
                    model.UserID = userGroupMapping.UserID.Value;
                    model.IsEditMode = true;
                    return View("UserGroupMappingForm", model);
                }
                else
                {
                    model.IsEditMode = false;
                    return View("UserGroupMappingForm", model);
                }
            }
            else
            {
                model.IsEditMode = false;
                return View("UserGroupMappingForm", model);
            }
        }

        /// <summary>
        /// Add or update UserGroupMapping
        /// </summary>
        /// <param name="userGroupMappingViewModel">The UserGroupMapping view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("UserGroupMapping")]
        public ActionResult AddOrUpdateUserGroupMapping(UserGroupMappingViewModel userGroupMappingViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData["Model"] = userGroupMappingViewModel;
                if (userGroupMappingViewModel.IsEditMode)
                {
                    return RedirectToAction("UpdateUserGroupMapping");
                }
                else
                {
                    return RedirectToAction("AddUserGroupMapping");
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add UserGroupMapping
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("UserGroupMapping", AuthorizationActionType.AllowAdd)]
        public ActionResult AddUserGroupMapping()
        {
            FacadeResult<DataModel.UserGroupMapping> result;
            DataModel.UserGroupMapping userGroupMapping = new DataModel.UserGroupMapping();
            UserGroupMappingViewModel userGroupMappingViewModel = (UserGroupMappingViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                userGroupMappingViewModel.UserGroupMappingID = userGroupMappingViewModel.UserGroupMappingID;
                userGroupMapping.GroupID = userGroupMappingViewModel.GroupID;
                userGroupMapping.UserID = userGroupMappingViewModel.UserID;

                result = AdministrationFacade.AddNewUserGroupMapping(userGroupMapping, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "UserGroupMapping", "Administration");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update group
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("UserGroupMapping", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateUserGroupMapping()
        {
            FacadeResult<DataModel.UserGroupMapping> result;
            DataModel.UserGroupMapping userGroupMapping = new DataModel.UserGroupMapping();
            UserGroupMappingViewModel userGroupMappingViewModel = (UserGroupMappingViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                userGroupMapping.UserGroupMappingID = userGroupMappingViewModel.UserGroupMappingID;
                userGroupMapping.GroupID = userGroupMappingViewModel.GroupID;
                userGroupMapping.UserID = userGroupMappingViewModel.UserID;

                result = AdministrationFacade.UpdateUserGroupMapping(userGroupMapping, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "UserGroupMapping", "Administration");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Get all active UserGroupMapping list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON UserGroupMapping data tables result</returns>
        [HttpPost]
        [ActionName("UserGroupMappingList")]
        [AuthorizeAccess("UserGroupMapping")]
        [ValidateSQLInjection]
        public ActionResult GetUserGroupMappingList(String keywords, int draw, int start, int length)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> userGroupMappings = AdministrationFacade.GetActiveUserGroupMappingsList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < userGroupMappings.Count; i++)
            {
                data.Add(new String[] { userGroupMappings[i].UserGroupMappingID.ToString(), userGroupMappings[i].Name, userGroupMappings[i].UserName, userGroupMappings[i].UserGroupMappingID.ToString(), String.Format("{{userGroupMappingID:{0}}}", userGroupMappings[i].UserGroupMappingID.ToString()) });
            }
            return Json(new { draw = draw, recordsTotal = AdministrationFacade.GetActiveUserGroupMappingsCount().ReturnData, recordsFiltered = AdministrationFacade.GetActiveUserGroupMappingsCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// Delete group by UserGroupMapping id
        /// </summary>
        /// <param name="groupID">The UserGroupMapping id</param>
        /// <returns>JSON result</returns>
        [HttpPost]
        [AuthorizeAction("UserGroupMapping", AuthorizationActionType.AllowDelete)]
        public ActionResult DeleteUserGroupMapping(int userGroupMappingID)
        {
            FacadeResult<DataModel.UserGroupMapping> result = AdministrationFacade.DeleteUserGroupMappingByID(userGroupMappingID, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }
        #endregion

        #region Message
        /// <summary>
        /// A controller to return the default view for message
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The message view</returns>
        [ActionName("Message")]
        [AuthorizeAccess("Message")]
        public ActionResult MessageListView(bool cancel = false, int? isPending = 0)
        {
            ViewBag.Title = TextResources.MessageView_Title;
            MessageViewModel model = new MessageViewModel();
            model.isPending = isPending.Value;
            return View("MessageList", model);
        }

        /// <summary>
        /// Get all active Message list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON Message data tables result</returns>
        [HttpPost]
        [ActionName("MessageList")]
        [AuthorizeAccess("Message")]
        [ValidateSQLInjection]
        public ActionResult GetMessageList(String keywords, int draw, int start, int length, int isPending = 1)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> messages = AdministrationFacade.GetActiveMessagesList(keywords, start, length, sortIndex, sortDirection, SecurityHelper.GetCurrentLoggedInUserID(), isPending).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < messages.Count; i++)
            {
                data.Add(new String[] { 
                    messages[i].MessageID.ToString(), 
                    messages[i].CategoryName, 
                    messages[i].Name, 
                    messages[i].Phone.ToString(),
                    messages[i].Email,
                    messages[i].TextMessage,
                    messages[i].AssetCode,
                    messages[i].BranchName,
                    String.Format("{0:dd/MM/yyyy HH:mm:ss}", messages[i].DateTimeCreated), messages[i].Status, messages[i].Remarks, messages[i].MessageID.ToString(), String.Format("{{messageID:{0}}}", messages[i].MessageID.ToString())});
            }
            return Json(new { draw = draw, recordsTotal = AdministrationFacade.GetActiveMessagesCount().ReturnData, recordsFiltered = AdministrationFacade.GetActiveMessagesCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// The action to view/add/edit selected message
        /// </summary>
        /// <param name="messageID">The message id</param>
        /// <returns>The message view</returns>
        [ActionName("AddOrUpdateMessage")]
        [AuthorizeAccess("Message")]
        public ActionResult AddOrUpdateMessageView(int? messageID)
        {
            ViewBag.Title = TextResources.MessageView_Title;
            MessageViewModel model = new MessageViewModel();

            model.MessageStatusList = MasterFacade.GetAllActiveMessageStatus().ReturnData.AsEnumerable();
            model.MessageCategoryList = MasterFacade.GetAllActiveMessageCategoryTypes().ReturnData.AsEnumerable();
            model.BranchList = MasterFacade.GetAllActiveBranches().ReturnData.AsEnumerable();
            if (messageID.HasValue)
            {
                DataModel.Message message = AdministrationFacade.GetActiveMessageByMessageID(messageID.Value).ReturnData;
                if (message != null)
                {
                    model.MessageID = Convert.ToInt32(message.MessageID);
                    model.DateTimeCreated = message.DateTimeCreated.Value;
                    model.MessageCategoryTypeID = Convert.ToInt32(message.MessageCategoryTypeID);
                    model.Name = message.Name;
                    model.Phone = message.Phone;
                    model.Email = message.Email;
                    model.AssetCode = message.AssetCode;
                    model.BranchID = message.BranchID;
                    model.TextMessage = message.TextMessage;
                    model.MessageStatusID = message.MessageStatusID;
                    model.Remarks = message.Remarks;
                    model.IsEditMode = true;
                    return View("MessageForm", model);
                }
                else
                {
                    model.IsEditMode = false;
                    return View("MessageForm", model);
                }
            }
            else
            {
                model.IsEditMode = false;
                return View("MessageForm", model);
            }
        }

        /// <summary>
        /// Add or update message
        /// </summary>
        /// <param name="messageViewModel">The message view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Message")]
        public ActionResult AddOrUpdateMessage(MessageViewModel messageViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData["Model"] = messageViewModel;
                if (messageViewModel.IsEditMode)
                {
                    return RedirectToAction("UpdateMessage");
                }
                else
                {
                    return RedirectToAction("AddMessage");
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add message
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("Message", AuthorizationActionType.AllowAdd)]
        public ActionResult AddMessage()
        {
            FacadeResult<DataModel.Message> result;
            DataModel.Message message = new DataModel.Message();
            MessageViewModel messageViewModel = (MessageViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                message.MessageCategoryTypeID = Convert.ToByte(messageViewModel.MessageCategoryTypeID);
                message.Name = messageViewModel.Name;
                message.Phone = messageViewModel.Phone;
                message.Email = messageViewModel.Email;
                message.AssetCode = messageViewModel.AssetCode == null? string.Empty : messageViewModel.AssetCode;
                message.BranchID = messageViewModel.BranchID;
                message.TextMessage = messageViewModel.TextMessage == null? string.Empty : messageViewModel.TextMessage;
                message.MessageStatusID = messageViewModel.MessageStatusID;
                message.Remarks = messageViewModel.Remarks;

                result = ContactFacade.AddNewMessage(message, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {

                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "Message", "Administration");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update message
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("Message", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateMessage()
        {
            bool brachIdChange = false;
            FacadeResult<DataModel.Message> result;
            DataModel.Message message = new DataModel.Message();
            MessageViewModel messageViewModel = (MessageViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                message.MessageID = messageViewModel.MessageID;
                message.MessageCategoryTypeID = Convert.ToByte(messageViewModel.MessageCategoryTypeID);
                message.Name = messageViewModel.Name;
                message.Phone = messageViewModel.Phone;
                message.Email = messageViewModel.Email;
                message.AssetCode = messageViewModel.AssetCode == null? string.Empty : messageViewModel.AssetCode;
                message.BranchID = messageViewModel.BranchID;
                message.TextMessage = messageViewModel.TextMessage;
                message.MessageStatusID = messageViewModel.MessageStatusID;
                message.Remarks = messageViewModel.Remarks;

                result = AdministrationFacade.UpdateMessage(message, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "Message", "Administration");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }
        #endregion

        #region UserActivity
        /// <summary>
        /// A controller to return the default view for UserActivity
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The UserActivity view</returns>
        [ActionName("UserActivity")]
        [AuthorizeAccess("UserActivity")]
        public ActionResult UserActivityListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.UserActivityView_Title;
            UserActivityViewModel model = new UserActivityViewModel();

            return View("UserActivityList", model);
        }


        /// <summary>
        /// Get all active UserActivity list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON UserActivity data tables result</returns>
        [HttpPost]
        [ActionName("UserActivityList")]
        [AuthorizeAccess("UserActivity")]
        public ActionResult GetUserActivityList(String keywords, int draw, int start, int length)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> userActivities = AdministrationFacade.GetActiveUserActivitiesList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < userActivities.Count; i++)
            {
                data.Add(new String[] { 
                    userActivities[i].UserActivityID.ToString(), 
                    userActivities[i].UserActivityTypeID, 
                    userActivities[i].ExtraDescription, 
                    userActivities[i].ReferenceID, 
                    userActivities[i].Created,
                    String.Format("{0:dd/MM/yyyy HH:mm:ss}", userActivities[i].DateTimeCreated)});
            }
            return Json(new { draw = draw, recordsTotal = AdministrationFacade.GetActiveUserActivitiesCount().ReturnData, recordsFiltered = AdministrationFacade.GetActiveUserActivitiesByKeywords(keywords).ReturnData, data = data.ToArray() });
        }
        #endregion

        #region Group Authorization

        /// <summary>
        /// A controller to return the default view for group authorization
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The group view</returns>
        [ActionName("GroupAuthorization")]
        [AuthorizeAccess("GroupAuthorization")]
        public ActionResult AddGroupAuthorizationView(bool cancel = false)
        {
            ViewBag.Title = TextResources.GroupView_Title;
            GroupAuthorizationViewModel model = new GroupAuthorizationViewModel();
            model.GroupAuthorizationDetailList = new List<GroupAuthorizationDetailsViewModel>();
            model.IsEditMode = false;
            PopulateAllDropdownFieldsForGroupAuthorization(model);
            if (cancel)
            {
                return PartialView("GroupAuthorizationList", model);
            }
            else
            {
                return View("GroupAuthorizationList", model);
            }
        }

        /// <summary>
        /// Group authorization action, and also handle other custom command
        /// </summary>
        /// <param name="groupAuthorizationViewModel">The group authorization view model</param>
        /// <returns>The form partial view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("GroupAuthorization")]
        public ActionResult DoPostActionForGroupAuthorization(GroupAuthorizationViewModel groupAuthorizationViewModel, String Command)
        {
            if (Command == "GroupChanged")
            {
                TempData["Model"] = groupAuthorizationViewModel;
                return RedirectToAction("GroupAuthorizationGroupOnChanged");
            }
            if (ModelState.IsValid)
            {
                TempData["Model"] = groupAuthorizationViewModel;
                if (Command == "Save")
                {
                    return RedirectToAction("AddGroupAuthorization");
                }
            }
            else
            {
                if (groupAuthorizationViewModel.GroupAuthorizationDetailList == null)
                {
                    groupAuthorizationViewModel.GroupAuthorizationDetailList = new List<GroupAuthorizationDetailsViewModel>();
                }
                PopulateAllDropdownFieldsForGroupAuthorization(groupAuthorizationViewModel);
                AddServerSideErrorToModelState();
            }
            return PartialView("GroupAuthorizationList", groupAuthorizationViewModel);
        }

        /// <summary>
        /// Event for handling material type dropdown on changed
        /// </summary>
        /// <returns>The form partial view</returns>
        [ActionName("GroupAuthorizationGroupOnChanged")]
        public ActionResult GroupAuthorizationGroupOnChanged()
        {
            ViewBag.Title = TextResources.GroupView_Title;
            GroupAuthorizationViewModel groupAuthorizationViewModel = (GroupAuthorizationViewModel)TempData["Model"];
            groupAuthorizationViewModel.GroupAuthorizationDetailList = new List<GroupAuthorizationDetailsViewModel>();
            PopulateAllDropdownFieldsForGroupAuthorization(groupAuthorizationViewModel);
            BindAuthorizationsForGroupAuthorizationDetails(groupAuthorizationViewModel);
            return PartialView("GroupAuthorizationForm", groupAuthorizationViewModel);
        }

        /// <summary>
        /// Add group authorization
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("GroupAuthorization", AuthorizationActionType.AllowAdd)]
        public ActionResult AddGroupAuthorization()
        {
            FacadeResult<IList<DataModel.GroupAuthorization>> result;
            DataModel.GroupAuthorization groupAuthorization;
            GroupAuthorizationViewModel groupAuthorizationViewModel = (GroupAuthorizationViewModel)TempData["Model"];
            IList<DataModel.GroupAuthorization> groupAuthorizations = new List<DataModel.GroupAuthorization>();

            if (ModelState.IsValid)
            {
                foreach (GroupAuthorizationDetailsViewModel groupAuthorizationDetailsViewModel in groupAuthorizationViewModel.GroupAuthorizationDetailList)
                {
                    groupAuthorization = new GroupAuthorization();
                    groupAuthorization.GroupID = groupAuthorizationViewModel.GroupID;
                    groupAuthorization.SiteMapID = groupAuthorizationDetailsViewModel.SiteMapID;
                    groupAuthorization.AllowAccess = groupAuthorizationDetailsViewModel.AllowAccess;
                    groupAuthorization.AllowDelete = groupAuthorizationDetailsViewModel.AllowDelete;
                    groupAuthorization.AllowAdd = groupAuthorizationDetailsViewModel.AllowAdd;
                    groupAuthorization.AllowUpdate = groupAuthorizationDetailsViewModel.AllowUpdate;
                    groupAuthorizations.Add(groupAuthorization);
                }

                result = AdministrationFacade.AddNewGroupAuthorizations(groupAuthorizationViewModel.GroupID, groupAuthorizations, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "GroupAuthorization", "Administration");
                }
                else
                {
                    PopulateAllDropdownFieldsForGroupAuthorization(groupAuthorizationViewModel);
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("GroupAuthorizationList", groupAuthorizationViewModel);
        }

        /// <summary>
        /// Populate all dropdown fields for group authorization
        /// </summary>
        /// <param name="groupAuthorizationViewModel">The view model</param>
        private void PopulateAllDropdownFieldsForGroupAuthorization(GroupAuthorizationViewModel groupAuthorizationViewModel)
        {
            groupAuthorizationViewModel.Groups = AdministrationFacade.GetAllActiveGroups().ReturnData.AsEnumerable();
        }

        /// <summary>
        /// Bind current or empty authorization for group authorization
        /// </summary>
        /// <param name="groupAuthorizationViewModel"></param>
        private void BindAuthorizationsForGroupAuthorizationDetails(GroupAuthorizationViewModel groupAuthorizationViewModel)
        {
            IList<GroupAuthorization> groupAuthorizations = AdministrationFacade.GetCurrentGroupAuthorizationByGroupID(groupAuthorizationViewModel.GroupID).ReturnData;
            GroupAuthorizationDetailsViewModel groupAuthorizationDetailsViewModel;

            if (groupAuthorizationViewModel.GroupAuthorizationDetailList == null)
            {
                groupAuthorizationViewModel.GroupAuthorizationDetailList = new List<GroupAuthorizationDetailsViewModel>();
            }
            foreach (GroupAuthorization groupAuthorization in groupAuthorizations)
            {
                groupAuthorizationDetailsViewModel = new GroupAuthorizationDetailsViewModel();
                groupAuthorizationDetailsViewModel.AllowAccess = groupAuthorization.AllowAccess == null ? false : groupAuthorization.AllowAccess.Value;
                groupAuthorizationDetailsViewModel.AllowAdd = groupAuthorization.AllowAdd == null ? false : groupAuthorization.AllowAdd.Value;
                groupAuthorizationDetailsViewModel.AllowDelete = groupAuthorization.AllowDelete == null ? false : groupAuthorization.AllowDelete.Value;
                groupAuthorizationDetailsViewModel.AllowUpdate = groupAuthorization.AllowUpdate == null ? false : groupAuthorization.AllowUpdate.Value;
                groupAuthorizationDetailsViewModel.Menu = groupAuthorization.SiteMap == null ? String.Empty : groupAuthorization.SiteMap.MenuName;
                groupAuthorizationDetailsViewModel.SiteMapID = groupAuthorization.SiteMapID;
                groupAuthorizationViewModel.GroupAuthorizationDetailList.Add(groupAuthorizationDetailsViewModel);
            }
        }

        #endregion

        #region User
        /// <summary>
        /// A controller to return the default view for user
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The user view</returns>
        [ActionName("User")]
        [AuthorizeAccess("User")]
        public ActionResult UserListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.UserView_Title;
            UserViewModel model = new UserViewModel();

            return View("UserList", model);
        }

        /// <summary>
        /// The action to view/add/edit selected user
        /// </summary>
        /// <param name="userID">The user id</param>
        /// <returns>The user view</returns>
        [ActionName("AddNewUser")]
        [AuthorizeAccess("User")]
        [RuleSetForClientSideMessages("default", "AddUser_Password")]
        public ActionResult AddUserView()
        {
            ViewBag.Title = TextResources.UserView_Title;
            UserViewModel model = new UserViewModel();
            model.Branches = MasterFacade.GetAllActiveBranchesWithAll().ReturnData.AsEnumerable();
            model.IsEditMode = false;
            return View("UserForm", model);
        }

        /// <summary>
        /// The action to view/add/edit selected user
        /// </summary>
        /// <param name="userID">The user id</param>
        /// <returns>The user view</returns>
        [ActionName("EditUser")]
        [AuthorizeAccess("User")]
        [RuleSetForClientSideMessages("default")]
        public ActionResult UpdateUserView(int? userID)
        {
            ViewBag.Title = TextResources.UserView_Title;
            UserViewModel model = new UserViewModel();

            if (userID.HasValue)
            {
                DataModel.User user = AdministrationFacade.GetActiveUserByUserID(userID.Value).ReturnData;
                if (user != null)
                {
                    model.Branches = MasterFacade.GetAllActiveBranchesWithAll().ReturnData.AsEnumerable();
                    model.UserID = user.UserID.Value;
                    model.Username = user.Username;
                    model.FullName = user.FullName;
                    model.Email = user.Email;
                    model.BranchID = user.BranchID.HasValue ? user.BranchID.Value : 0;
                    model.IsEditMode = true;
                    return View("UserForm", model);
                }
                else
                {
                    //model.IsEditMode = false;
                    return RedirectToAction("User");
                }
            }
            else
            {
                //model.IsEditMode = false;
                return RedirectToAction("User");
            }
        }

        /// <summary>
        /// Add or update user
        /// </summary>
        /// <param name="userViewModel">The user view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("User")]
        public ActionResult AddOrUpdateUser(UserViewModel userViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData["Model"] = userViewModel;
                if (userViewModel.IsEditMode)
                {
                    return RedirectToAction("UpdateUser");
                }
                else
                {
                    return RedirectToAction("AddUser");
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add user
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("User", AuthorizationActionType.AllowAdd)]
        public ActionResult AddUser()
        {
            FacadeResult<DataModel.User> result;
            DataModel.User user = new DataModel.User();
            UserViewModel userViewModel = (UserViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                user.Username = userViewModel.Username;
                user.FullName = userViewModel.FullName;
                user.Email = userViewModel.Email;
                user.BranchID = userViewModel.BranchID;

                result = AdministrationFacade.AddNewUser(user, userViewModel.Password, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "User", "Administration");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("User", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateUser()
        {
            FacadeResult<DataModel.User> result;
            DataModel.User user = new DataModel.User();
            UserViewModel userViewModel = (UserViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                user.UserID = userViewModel.UserID;
                user.Username = userViewModel.Username;
                user.FullName = userViewModel.FullName;
                user.Email = userViewModel.Email;
                user.BranchID = userViewModel.BranchID;

                result = AdministrationFacade.UpdateUser(user, userViewModel.Password, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "User", "Administration");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Get all active user list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON user data tables result</returns>
        [HttpPost]
        [ActionName("UserList")]
        [AuthorizeAccess("User")]
        [ValidateSQLInjection]
        public ActionResult GetUserList(String keywords, int draw, int start, int length)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> users = AdministrationFacade.GetActiveUsersList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < users.Count; i++)
            {
                data.Add(new String[] { users[i].UserID.ToString(), users[i].Username, users[i].FullName, users[i].BranchName, users[i].IsLocked.ToString(), users[i].UserID.ToString(), String.Format("{{userID:{0}}}", users[i].UserID.ToString()), String.Format("{{userID:{0}}}", users[i].UserID.ToString()) });
            }
            return Json(new { draw = draw, recordsTotal = AdministrationFacade.GetActiveUsersCount().ReturnData, recordsFiltered = AdministrationFacade.GetActiveUsersCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// Delete user by user id
        /// </summary>
        /// <param name="userID">The user id</param>
        /// <returns>JSON result</returns>
        [HttpPost]
        [AuthorizeAction("User", AuthorizationActionType.AllowDelete)]
        public ActionResult DeleteUser(int userID)
        {
            FacadeResult<DataModel.User> result = AdministrationFacade.DeleteUserByID(userID, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }

        /// <summary>
        /// Unlock user by user id
        /// </summary>
        /// <param name="userID">The user id</param>
        /// <returns>JSON result</returns>
        [HttpPost]
        [AuthorizeAction("User", AuthorizationActionType.AllowUpdate)]
        public ActionResult UnlockUser(int userID)
        {
            FacadeResult<DataModel.User> result = AdministrationFacade.UnlockUserByID(userID, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }

        #endregion

        #region System Setting

        [ActionName("UploadWebsitePopupImage")]
        public ActionResult UploadWebsitePopupImage(HttpPostedFileBase fileWebsitePopupImage)
        {
            if (IsFileAllowed(fileWebsitePopupImage.FileName) && CheckImageSize(fileWebsitePopupImage.InputStream, 800, 450, true))
            {
                SessionHelper.Remove(SessionKey.FileWebsitePopupImage);
                String path = ConfigurationManager.AppSettings["ImageUploadPath"].ToString();
                String fileName = "WebsitePopupImage_" + SecurityHelper.GetCurrentLoggedInUserID().ToString() + "_" + DateTime.Today.ToString("yyyyMMddhhmmss") + "_" + new FileInfo(fileWebsitePopupImage.FileName).Name;
                fileWebsitePopupImage.SaveAs(path + fileName);
                SessionHelper.SetValue<String>(SessionKey.FileWebsitePopupImage, fileName);
                return Json(new { IsSuccess = true, FileName = fileName }, "text/plain");
            }
            else
            {
                return Json(new { IsSuccess = false, FileName = String.Empty }, "text/plain");
            }
        }

        [ActionName("UploadMobileWebsitePopupImage")]
        public ActionResult UploadMobileWebsitePopupImage(HttpPostedFileBase fileMobileWebsitePopupImage)
        {
            if (IsFileAllowed(fileMobileWebsitePopupImage.FileName) && CheckImageSize(fileMobileWebsitePopupImage.InputStream, 540, 960, true))
            {
                SessionHelper.Remove(SessionKey.FileMobileWebsitePopupImage);
                String path = ConfigurationManager.AppSettings["ImageUploadPath"].ToString();
                String fileName = "MobileWebsitePopupImage_" + SecurityHelper.GetCurrentLoggedInUserID().ToString() + "_" + DateTime.Today.ToString("yyyyMMddhhmmss") + "_" + new FileInfo(fileMobileWebsitePopupImage.FileName).Name;
                fileMobileWebsitePopupImage.SaveAs(path + fileName);
                SessionHelper.SetValue<String>(SessionKey.FileMobileWebsitePopupImage, fileName);
                return Json(new { IsSuccess = true, FileName = fileName }, "text/plain");
            }
            else
            {
                return Json(new { IsSuccess = false, FileName = String.Empty }, "text/plain");
            }
        }

        /// <summary>
        /// Get form system setting
        /// </summary>
        /// <returns></returns>
        [ActionName("SystemSetting")]
        [AuthorizeAccess("SystemSetting")]
        public ActionResult GetSystemSettingForm()
        {
            ViewBag.Title = TextResources.SystemSetting_Title;
            SystemSettingViewModel model = new SystemSettingViewModel();


            IList<SystemSetting> settings = AdministrationFacade.GetAllActiveSystemSettings().ReturnData;
            if (settings != null)
            {
                foreach (var setting in settings)
                {
                    if (setting.SystemSettingID == SystemSettingViewModel.Key.VisibilityDaysPeriodForPaidAndSoldKey)
                    {
                        model.VisibilityDaysPeriodForPaidAndSoldID = setting.SystemSettingID;
                        model.VisibilityDaysPeriodForPaidAndSold = Int32.Parse(String.IsNullOrEmpty(setting.Value) ? "0" : setting.Value);
                    }
                    else if (setting.SystemSettingID == SystemSettingViewModel.Key.EmailReminderPeriodKey)
                    {
                        model.EmailReminderPeriodID = setting.SystemSettingID;
                        model.EmailReminderPeriod = Int32.Parse(String.IsNullOrEmpty(setting.Value) ? "0" : setting.Value);
                    }
                    else if (setting.SystemSettingID == SystemSettingViewModel.Key.MandatoryForMemberAddressKey)
                    {
                        model.MandatoryForMemberAddressID = setting.SystemSettingID;
                        model.MandatoryForMemberAddress = Boolean.Parse(String.IsNullOrEmpty(setting.Value) ? "False" : setting.Value);
                    }
                    else if (setting.SystemSettingID == SystemSettingViewModel.Key.MandatoryForMemberCityKey)
                    {
                        model.MandatoryForMemberCityID = setting.SystemSettingID;
                        model.MandatoryForMemberCity = Boolean.Parse(String.IsNullOrEmpty(setting.Value) ? "False" : setting.Value);
                    }
                    else if (setting.SystemSettingID == SystemSettingViewModel.Key.MandatoryForMemberDateOfBirthKey)
                    {
                        model.MandatoryForMemberDateOfBirthID = setting.SystemSettingID;
                        model.MandatoryForMemberDateOfBirth = Boolean.Parse(String.IsNullOrEmpty(setting.Value) ? "False" : setting.Value);
                    }
                    else if (setting.SystemSettingID == SystemSettingViewModel.Key.MandatoryForMemberIdentificationNumberKey)
                    {
                        model.MandatoryForMemberIdentificationNumberID = setting.SystemSettingID;
                        model.MandatoryForMemberIdentificationNumber = Boolean.Parse(String.IsNullOrEmpty(setting.Value) ? "False" : setting.Value);
                    }
                    else if (setting.SystemSettingID == SystemSettingViewModel.Key.MandatoryForMemberMobilePhoneNumberKey)
                    {
                        model.MandatoryForMemberMobilePhoneNumberID = setting.SystemSettingID;
                        model.MandatoryForMemberMobilePhoneNumber = Boolean.Parse(String.IsNullOrEmpty(setting.Value) ? "False" : setting.Value);
                    }
                    else if (setting.SystemSettingID == SystemSettingViewModel.Key.MandatoryForMemberPlaceOfBirthKey)
                    {
                        model.MandatoryForMemberPlaceOfBirthID = setting.SystemSettingID;
                        model.MandatoryForMemberPlaceOfBirth = Boolean.Parse(String.IsNullOrEmpty(setting.Value) ? "False" : setting.Value);
                    }
                    else if (setting.SystemSettingID == SystemSettingViewModel.Key.MandatoryForMemberPreferenceKey)
                    {
                        model.MandatoryForMemberPreferenceID = setting.SystemSettingID;
                        model.MandatoryForMemberPreference = Boolean.Parse(String.IsNullOrEmpty(setting.Value) ? "False" : setting.Value);
                    }
                    else if (setting.SystemSettingID == SystemSettingViewModel.Key.MandatoryForMemberProvinceKey)
                    {
                        model.MandatoryForMemberProvinceID = setting.SystemSettingID;
                        model.MandatoryForMemberProvince = Boolean.Parse(String.IsNullOrEmpty(setting.Value) ? "False" : setting.Value);
                    }
                    else if (setting.SystemSettingID == SystemSettingViewModel.Key.MobileNotificationSyncPeriodKey)
                    {
                        model.MobileNotificationSyncPeriodID = setting.SystemSettingID;
                        model.MobileNotificationSyncPeriod = Int32.Parse(String.IsNullOrEmpty(setting.Value) ? "0" : setting.Value);
                    }
                    else if (setting.SystemSettingID == SystemSettingViewModel.Key.MobileWebsitePopupImageKey)
                    {
                        model.MobileWebsitePopupImageID = setting.SystemSettingID;
                        model.MobileWebsitePopupImage = String.IsNullOrEmpty(setting.Value) ? String.Empty : setting.Value;
                    }
                    else if (setting.SystemSettingID == SystemSettingViewModel.Key.WebsitePopupImageKey)
                    {
                        model.WebsitePopupImageID = setting.SystemSettingID;
                        model.WebsitePopupImage = String.IsNullOrEmpty(setting.Value) ? String.Empty : setting.Value;
                    }
                }
            }

            return View("SystemSettingForm", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("SystemSetting")]
        public ActionResult AddOrUpdateSystemSetting(SystemSettingViewModel systemSettingViewModel)
        {
            FacadeResult<IList<SystemSetting>> result = new FacadeResult<IList<SystemSetting>>();
            if (ModelState.IsValid)
            {
                IList<SystemSetting> settings = new List<SystemSetting>();
                settings.Add(new SystemSetting() { SystemSettingID = systemSettingViewModel.EmailReminderPeriodID, Value = systemSettingViewModel.EmailReminderPeriod.ToString() });
                settings.Add(new SystemSetting() { SystemSettingID = systemSettingViewModel.MandatoryForMemberAddressID, Value = systemSettingViewModel.MandatoryForMemberAddress.ToString() });
                settings.Add(new SystemSetting() { SystemSettingID = systemSettingViewModel.MandatoryForMemberCityID, Value = systemSettingViewModel.MandatoryForMemberCity.ToString() });
                settings.Add(new SystemSetting() { SystemSettingID = systemSettingViewModel.MandatoryForMemberDateOfBirthID, Value = systemSettingViewModel.MandatoryForMemberDateOfBirth.ToString() });
                settings.Add(new SystemSetting() { SystemSettingID = systemSettingViewModel.MandatoryForMemberIdentificationNumberID, Value = systemSettingViewModel.MandatoryForMemberIdentificationNumber.ToString() });
                settings.Add(new SystemSetting() { SystemSettingID = systemSettingViewModel.MandatoryForMemberMobilePhoneNumberID, Value = systemSettingViewModel.MandatoryForMemberMobilePhoneNumber.ToString() });
                settings.Add(new SystemSetting() { SystemSettingID = systemSettingViewModel.MandatoryForMemberPlaceOfBirthID, Value = systemSettingViewModel.MandatoryForMemberPlaceOfBirth.ToString() });
                settings.Add(new SystemSetting() { SystemSettingID = systemSettingViewModel.MandatoryForMemberPreferenceID, Value = systemSettingViewModel.MandatoryForMemberPreference.ToString() });
                settings.Add(new SystemSetting() { SystemSettingID = systemSettingViewModel.MandatoryForMemberProvinceID, Value = systemSettingViewModel.MandatoryForMemberProvince.ToString() });
                settings.Add(new SystemSetting() { SystemSettingID = systemSettingViewModel.MobileNotificationSyncPeriodID, Value = systemSettingViewModel.MobileNotificationSyncPeriod.ToString() });
                settings.Add(new SystemSetting() { SystemSettingID = systemSettingViewModel.MobileWebsitePopupImageID, Value = systemSettingViewModel.MobileWebsitePopupImage.ToString() });
                settings.Add(new SystemSetting() { SystemSettingID = systemSettingViewModel.VisibilityDaysPeriodForPaidAndSoldID, Value = systemSettingViewModel.VisibilityDaysPeriodForPaidAndSold.ToString() });
                settings.Add(new SystemSetting() { SystemSettingID = systemSettingViewModel.WebsitePopupImageID, Value = systemSettingViewModel.WebsitePopupImage.ToString() });

                result = AdministrationFacade.UpdateSystemSettings(settings, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "SystemSetting", "Administration");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }
        #endregion

        #endregion
    }
}