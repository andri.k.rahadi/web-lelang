﻿using Mandiri.Lelang.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mandiri.Lelang.Helper.Extension;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.Helper.Enum;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using FluentValidation.Results;
using System.Configuration;
using System.Drawing;
using System.IO;

namespace Mandiri.Lelang.WebAdminUI.Controllers
{
    /// <summary>
    /// The base class for all controller
    /// </summary>
    public class BaseController : Controller
    {
        #region Private Methods

        /// <summary>
        /// Get the current logged in user full name
        /// </summary>
        /// <returns></returns>
        private String GetCurrentUserFullName()
        {
            if (SecurityHelper.IsAuthenticated())
            {
                if (SessionHelper.GetValue<DataModel.User>(SessionKey.CurrentLoggedInUser) != null)
                {
                    return SessionHelper.GetValue<DataModel.User>(SessionKey.CurrentLoggedInUser).FullName;
                }
                else
                {
                    FacadeResult<DataModel.User> result = UserFacade.GetActiveUserByUserID(SecurityHelper.GetCurrentLoggedInUserID());
                    if (result.IsSuccess)
                    {
                        SessionHelper.SetValue<DataModel.User>(SessionKey.CurrentLoggedInUser, result.ReturnData);
                        return result.ReturnData.FullName;
                    }
                    else
                    {
                        return String.Empty;
                    }
                }
            }
            else
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Initialize all view bag property that are available globally on all views and controllers
        /// </summary>
        private void SetViewBagWithGlobalProperty()
        {
            ViewBag.CurrentLoggedInUserFullName = GetCurrentUserFullName();
            ViewBag.DefaultUnexpectedErrorMessage = TextResources.DefaultUnexpectedError_ErrorMessage;
            ViewBag.ConfirmationMessage = TextResources.Confirmation_Message;

            //Generate menu and save it to session
            try
            {
                SiteMapViewModel siteMap = SessionHelper.GetValue<SiteMapViewModel>(SessionKey.FirstLevelMenu);
                SiteMapViewModel authorizedSiteMap = SessionHelper.GetValue<SiteMapViewModel>(SessionKey.AuthorizedMenu);
                if (authorizedSiteMap == null)
                {
                    authorizedSiteMap = new SiteMapViewModel();
                    authorizedSiteMap.SiteMaps = MenuFacade.GetAllowedAccessMenusByUserID(SecurityHelper.GetCurrentLoggedInUserID()).ReturnData;
                    /*foreach (DataModel.SiteMap eachSiteMap in authorizedSiteMap.SiteMaps)
                    {
                        eachSiteMap.ChildSiteMaps = MenuFacade.GetAllChildMenusBySiteMapParentID(eachSiteMap.SiteMapID).ReturnData;
                    }*/
                    SessionHelper.SetValue<SiteMapViewModel>(SessionKey.AuthorizedMenu, authorizedSiteMap);
                }
                ViewBag.AuthorizedMenu = authorizedSiteMap.SiteMaps;
                if (siteMap == null)
                {
                    siteMap = new SiteMapViewModel();
                    siteMap.SiteMaps = MenuFacade.GetAllActiveMenusByLevel(1).ReturnData;
                    foreach (DataModel.SiteMap eachSiteMap in siteMap.SiteMaps)
                    {
                        eachSiteMap.ChildSiteMaps = MenuFacade.GetAllChildMenusBySiteMapParentID(eachSiteMap.SiteMapID).ReturnData;
                    }
                    SessionHelper.SetValue<SiteMapViewModel>(SessionKey.FirstLevelMenu, siteMap);
                }
                ViewBag.Menu = siteMap.SiteMaps;
            }
            catch
            {
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Check for file extension
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool IsFileAllowed(String fileName)
        {
            if (fileName.ToUpper().EndsWith(".JPG") || fileName.ToUpper().EndsWith(".JPEG") || fileName.ToUpper().EndsWith(".PNG"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check for image size
        /// </summary>
        /// <param name="file"></param>
        /// <param name="validateWidth"></param>
        /// <param name="validateHeight"></param>
        /// <returns></returns>
        public bool CheckImageSize(Stream file, int validateWidth, int validateHeight, bool exactValidation)
        {
            try
            {
                Image img = System.Drawing.Image.FromStream(file);
                int width = img.Width;
                int height = img.Height;

                if (exactValidation)
                {
                    if (width == validateWidth && height == validateHeight)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (width >= validateWidth && height >= validateHeight)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Check whether the search filter in grid is applied or not
        /// </summary>
        /// <param name="filters">Search filter list</param>
        /// <returns>Flag</returns>
        public bool IsSearchFilterApplied(IList<String> filters)
        {
            bool flag = false;
            foreach (String s in filters)
            {
                if (s.Trim() != "")
                {
                    flag = true;
                    break;
                }
            }
            return flag;
        }

        /// <summary>
        /// Add server side validation error message into the model state to be displayed on the form
        /// </summary>
        public void AddServerSideErrorToModelState()
        {
            IList<String> errorMessages = new List<String>();
            foreach (KeyValuePair<String, ModelState> state in ModelState.AsEnumerable())
            {
                for (int i = 0; i < state.Value.Errors.Count; i++)
                {
                    errorMessages.Add(state.Value.Errors[i].ErrorMessage);
                }
            }
            foreach (String errorMessage in errorMessages)
            {
                ModelState.AddModelError("", errorMessage);
            }
        }

        /// <summary>
        /// Add server side validation error message into the model state to be displayed on the form
        /// </summary>
        /// <param name="errors">List of errors</param>
        public void AddServerSideErrorToModelState(IList<ValidationFailure> errors)
        {
            foreach (ValidationFailure error in errors)
            {
                ModelState.AddModelError("", error.ErrorMessage);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public BaseController()
        {
            SetViewBagWithGlobalProperty();
        }

        #endregion

        #region Override Methods

        /// <summary>
        /// Override the default handle unknown action to force redirect to the global error page
        /// </summary>
        /// <param name="actionName">The action name</param>
        protected override void HandleUnknownAction(string actionName)
        {
            if (this.HttpContext.IsCustomErrorEnabled)
            {
                //Redirect to the global error page
                TempData["GlobalErrorMessage"] = "Unknown Action";
                if (this.HttpContext.Request.IsAjaxRequest())
                {
                    this.JavaScriptRedirect("ErrorPage", "Error").ExecuteResult(this.ControllerContext);
                }
                else
                {
                    this.View("ErrorPage").ExecuteResult(this.ControllerContext);
                }
            }
        }

        /// <summary>
        /// Override the on exception to log the error and to force redirect to the global error page
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnException(ExceptionContext filterContext)
        {
            LogHelper.LogException(filterContext.Exception);

            //Output a nice error page with the exception message if the customError config set to On or if it's a remote web on RemoteOnly option
            if (filterContext.HttpContext.IsCustomErrorEnabled)
            {
                //Redirect to the global error page
                TempData["GlobalErrorMessage"] = filterContext.Exception.Source + "|" + filterContext.Exception.Message + "|" + filterContext.Exception.StackTrace;
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    if (bool.Parse(ConfigurationManager.AppSettings["ShowGrowlOnAjaxError"]))
                    {
                        base.OnException(filterContext);
                    }
                    else
                    {
                        filterContext.ExceptionHandled = true;
                        this.JavaScriptRedirect("ErrorPage", "Error").ExecuteResult(this.ControllerContext);
                    }
                }
                else
                {
                    filterContext.ExceptionHandled = true;
                    this.View("ErrorPage").ExecuteResult(this.ControllerContext);
                }
            }
            else
            {
                //Show the default error page if it's not an ajax request, and show a growl notification if it's an ajax request
                base.OnException(filterContext);
            }
        }

        #endregion
    }
}