﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.Helper.Enum;
using Mandiri.Lelang.Helper.Extension;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.WebAdmin;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.Attribute.ActionFilter;
using Mandiri.Lelang.Attribute.Enum;
using System.Dynamic;
using Mandiri.Lelang.DataModel;
using System.Configuration;
using System.IO;
using System.Web.UI.WebControls;

namespace Mandiri.Lelang.WebAdminUI.Controllers.Master
{
    public class MasterController : BaseController
    {
        #region Action

        #region Province

        /// <summary>
        /// A controller to return the default view for province
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The province view</returns>
        [ActionName("Province")]
        [AuthorizeAccess("Province")]
        public ActionResult ProvinceListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.ProvinceView_Title;
            ProvinceViewModel model = new ProvinceViewModel();

            return View("ProvinceList", model);
        }

        /// <summary>
        /// The action to view/add/edit selected province
        /// </summary>
        /// <param name="provinceID">The province id</param>
        /// <returns>The province view</returns>
        [ActionName("AddOrUpdateProvince")]
        [AuthorizeAccess("Province")]
        public ActionResult AddOrUpdateProvinceView(int? provinceID)
        {
            ViewBag.Title = TextResources.ProvinceView_Title;
            ProvinceViewModel model = new ProvinceViewModel();

            if (provinceID.HasValue)
            {
                DataModel.Province province = MasterFacade.GetActiveProvinceByProvinceID(provinceID.Value).ReturnData;
                if (province != null)
                {
                    model.ProvinceID = province.ProvinceID.Value;
                    model.Name = province.Name;
                    model.Description = province.Description;
                    model.IsEditMode = true;
                    return View("ProvinceForm", model);
                }
                else
                {
                    model.IsEditMode = false;
                    return View("ProvinceForm", model);
                }
            }
            else
            {
                model.IsEditMode = false;
                return View("ProvinceForm", model);
            }
        }

        /// <summary>
        /// Add or update province
        /// </summary>
        /// <param name="provinceViewModel">The province view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Province")]
        public ActionResult AddOrUpdateProvince(ProvinceViewModel provinceViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData["Model"] = provinceViewModel;
                if (provinceViewModel.IsEditMode)
                {
                    return RedirectToAction("UpdateProvince");
                }
                else
                {
                    return RedirectToAction("AddProvince");
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add province
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("Province", AuthorizationActionType.AllowAdd)]
        public ActionResult AddProvince()
        {
            FacadeResult<DataModel.Province> result;
            DataModel.Province province = new DataModel.Province();
            ProvinceViewModel provinceViewModel = (ProvinceViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                province.Name = provinceViewModel.Name;
                province.Description = provinceViewModel.Description;

                result = MasterFacade.AddNewProvince(province, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "Province", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update province
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("Province", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateProvince()
        {
            FacadeResult<DataModel.Province> result;
            DataModel.Province province = new DataModel.Province();
            ProvinceViewModel provinceViewModel = (ProvinceViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                province.ProvinceID = provinceViewModel.ProvinceID;
                province.Name = provinceViewModel.Name;
                province.Description = provinceViewModel.Description;

                result = MasterFacade.UpdateProvince(province, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "Province", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Get all active province list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON province data tables result</returns>
        [HttpPost]
        [ActionName("ProvinceList")]
        [AuthorizeAccess("Province")]
        [ValidateSQLInjection]
        public ActionResult GetProvinceList(String keywords, int draw, int start, int length)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> provinces = MasterFacade.GetActiveProvincesList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < provinces.Count; i++)
            {
                data.Add(new String[] { provinces[i].ProvinceID.ToString(), provinces[i].Name, provinces[i].Description, provinces[i].ProvinceID.ToString(), String.Format("{{provinceID:{0}}}", provinces[i].ProvinceID.ToString()) });
            }
            return Json(new { draw = draw, recordsTotal = MasterFacade.GetActiveProvincesCount().ReturnData, recordsFiltered = MasterFacade.GetActiveProvincesCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// Delete province by province id
        /// </summary>
        /// <param name="provinceID">The province id</param>
        /// <returns>JSON result</returns>
        [HttpPost]
        [AuthorizeAction("Province", AuthorizationActionType.AllowDelete)]
        public ActionResult DeleteProvince(int provinceID)
        {
            FacadeResult<DataModel.Province> result = MasterFacade.DeleteProvinceByID(provinceID, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }
        #endregion

        #region City

        /// <summary>
        /// A controller to return the default view for city
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The city view</returns>
        [ActionName("City")]
        [AuthorizeAccess("City")]
        public ActionResult CityListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.CityView_Title;
            CityViewModel model = new CityViewModel();

            return View("CityList", model);
        }

        /// <summary>
        /// The action to view/add/edit selected city
        /// </summary>
        /// <param name="cityID">The city id</param>
        /// <returns>The city view</returns>
        [ActionName("AddOrUpdateCity")]
        [AuthorizeAccess("City")]
        public ActionResult AddOrUpdateCityView(int? cityID)
        {
            ViewBag.Title = TextResources.CityView_Title;
            CityViewModel model = new CityViewModel();

            model.Provinces = MasterFacade.GetAllActiveProvinces().ReturnData.AsEnumerable();
            if (cityID.HasValue)
            {
                DataModel.City city = MasterFacade.GetActiveCityByCityID(cityID.Value).ReturnData;
                if (city != null)
                {
                    model.CityID = city.CityID.Value;
                    model.Name = city.Name;
                    model.ProvinceID = city.ProvinceID.Value;
                    model.Description = city.Description;
                    model.IsEditMode = true;
                    return View("CityForm", model);
                }
                else
                {
                    model.IsEditMode = false;
                    return View("CityForm", model);
                }
            }
            else
            {
                model.IsEditMode = false;
                return View("CityForm", model);
            }
        }

        /// <summary>
        /// Add or update city
        /// </summary>
        /// <param name="cityViewModel">The city view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("City")]
        public ActionResult AddOrUpdateCity(CityViewModel cityViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData["Model"] = cityViewModel;
                if (cityViewModel.IsEditMode)
                {
                    return RedirectToAction("UpdateCity");
                }
                else
                {
                    return RedirectToAction("AddCity");
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add city
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("City", AuthorizationActionType.AllowAdd)]
        public ActionResult AddCity()
        {
            FacadeResult<DataModel.City> result;
            DataModel.City city = new DataModel.City();
            CityViewModel cityViewModel = (CityViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                city.Name = cityViewModel.Name;
                city.Description = cityViewModel.Description;
                city.ProvinceID = cityViewModel.ProvinceID;

                result = MasterFacade.AddNewCity(city, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "City", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update city
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("City", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateCity()
        {
            FacadeResult<DataModel.City> result;
            DataModel.City city = new DataModel.City();
            CityViewModel cityViewModel = (CityViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                city.CityID = cityViewModel.CityID;
                city.Name = cityViewModel.Name;
                city.Description = cityViewModel.Description;
                city.ProvinceID = cityViewModel.ProvinceID;

                result = MasterFacade.UpdateCity(city, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "City", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Get all active city list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON city data tables result</returns>
        [HttpPost]
        [ActionName("CityList")]
        [AuthorizeAccess("City")]
        [ValidateSQLInjection]
        public ActionResult GetCityList(String keywords, int draw, int start, int length)
        {

            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> cities = MasterFacade.GetActiveCitiesList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();

            for (int i = 0; i < cities.Count; i++)
            {
                data.Add(new String[] { cities[i].CityID.ToString(), cities[i].CityName, cities[i].Description, cities[i].ProvinceName.ToString(), cities[i].CityID.ToString(), String.Format("{{cityID:{0}}}", cities[i].CityID.ToString()) });
            }
            return Json(new { draw = draw, recordsTotal = MasterFacade.GetActiveCitiesCount().ReturnData, recordsFiltered = MasterFacade.GetActiveCitiesCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// Delete city by city id
        /// </summary>
        /// <param name="cityID">The city id</param>
        /// <returns>JSON result</returns>
        [HttpPost]
        [AuthorizeAction("City", AuthorizationActionType.AllowDelete)]
        public ActionResult DeleteCity(int cityID)
        {
            FacadeResult<DataModel.City> result = MasterFacade.DeleteCityByID(cityID, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }

        #endregion

        #region MessageStatus

        /// <summary>
        /// A controller to return the default view for messagestatus
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The messagestatus view</returns>
        [ActionName("MessageStatus")]
        [AuthorizeAccess("MessageStatus")]
        public ActionResult MessageStatusListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.MessageStatusView_Title;
            MessageStatusViewModel model = new MessageStatusViewModel();

            return View("MessageStatusList", model);
        }

        /// <summary>
        /// Get all active messagestatus list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON message status data tables result</returns>
        [HttpPost]
        [ActionName("MessageStatusList")]
        [AuthorizeAccess("MessageStatus")]
        [ValidateSQLInjection]
        public ActionResult GetMessageStatusList(String keywords, int draw, int start, int length)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> MessageStatus = MasterFacade.GetActiveMessageStatusList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();

            for (int i = 0; i < MessageStatus.Count; i++)
            {
                data.Add(new String[] { MessageStatus[i].MessageStatusID.ToString(), MessageStatus[i].Name, MessageStatus[i].Description, MessageStatus[i].MessageStatusID.ToString(), String.Format("{{MessageStatusID:{0}}}", MessageStatus[i].MessageStatusID.ToString()) });
            }
            return Json(new { draw = draw, recordsTotal = MasterFacade.GetActiveMessageStatusCount().ReturnData, recordsFiltered = MasterFacade.GetActiveMessageStatusCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// The action to view/add/edit selected messagestatus
        /// </summary>
        /// <param name="messagestatusID">The messagestatus id</param>
        /// <returns>The message status view</returns>
        [ActionName("AddOrUpdateMessageStatus")]
        [AuthorizeAccess("MessageStatus")]
        public ActionResult AddOrUpdateMessageStatusView(int? messageStatusID)
        {
            ViewBag.Title = TextResources.MessageStatusView_Title;
            MessageStatusViewModel model = new MessageStatusViewModel();

            if (messageStatusID.HasValue)
            {
                DataModel.MessageStatus messageStatus = MasterFacade.GetActiveMessageStatusByMessageStatusID(messageStatusID.Value).ReturnData;
                if (messageStatus != null)
                {
                    model.MessageStatusID = messageStatus.MessageStatusID;
                    model.Name = messageStatus.Name;
                    model.Description = messageStatus.Description;
                    model.IsComplete = messageStatus.IsComplete;
                    model.IsEditMode = true;
                    return View("MessageStatusForm", model);
                }
                else
                {
                    model.IsEditMode = false;
                    return View("MessageStatusForm", model);
                }
            }
            else
            {
                model.IsEditMode = false;
                return View("MessageStatusForm", model);
            }
        }

        /// <summary>
        /// Add or update messagestatus
        /// </summary>
        /// <param name="MessageStatusViewModel">The messagestatus view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("MessageStatus")]
        public ActionResult AddOrUpdateMessageStatus(MessageStatusViewModel messageStatusViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData["Model"] = messageStatusViewModel;
                if (messageStatusViewModel.IsEditMode)
                {
                    return RedirectToAction("UpdateMessageStatus");
                }
                else
                {
                    return RedirectToAction("AddMessageStatus");
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add messagestatus
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("MessageStatus", AuthorizationActionType.AllowAdd)]
        public ActionResult AddMessageStatus()
        {
            FacadeResult<DataModel.MessageStatus> result;
            DataModel.MessageStatus messageStatus = new DataModel.MessageStatus();
            MessageStatusViewModel messageStatusViewModel = (MessageStatusViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                messageStatus.Name = messageStatusViewModel.Name;
                messageStatus.Description = messageStatusViewModel.Description;
                messageStatus.IsComplete = messageStatusViewModel.IsComplete;
                result = MasterFacade.AddNewMessageStatus(messageStatus, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "MessageStatus", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update messagestatus
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("MessageStatus", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateMessageStatus()
        {
            FacadeResult<DataModel.MessageStatus> result;
            DataModel.MessageStatus messageStatus = new DataModel.MessageStatus();
            MessageStatusViewModel messageStatusViewModel = (MessageStatusViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                messageStatus.MessageStatusID = messageStatusViewModel.MessageStatusID;
                messageStatus.Name = messageStatusViewModel.Name;
                messageStatus.Description = messageStatusViewModel.Description;
                messageStatus.IsComplete = messageStatusViewModel.IsComplete;
                result = MasterFacade.UpdateMessageStatus(messageStatus, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "MessageStatus", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }


        /// <summary>
        /// Delete messagestatus by messagestatus id
        /// </summary>
        /// <param name="messageStatusID">The messagestatus id</param>
        /// <returns>JSON result</returns>
        [HttpPost]
        [AuthorizeAction("MessageStatus", AuthorizationActionType.AllowDelete)]
        public ActionResult DeleteMessageStatus(int messageStatusID)
        {
            FacadeResult<DataModel.MessageStatus> result = MasterFacade.DeleteMessageStatusByID(messageStatusID, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }
        #endregion

        #region Video

        /// <summary>
        /// A controller to return the default view for video
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The video view</returns>
        [ActionName("Video")]
        [AuthorizeAccess("Video")]
        public ActionResult VideoListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.VideoView_Title;
            VideoViewModel model = new VideoViewModel();

            return View("VideoList", model);
        }

        /// <summary>
        /// Get all active video list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON video data tables result</returns>
        [HttpPost]
        [ActionName("VideoList")]
        [AuthorizeAccess("Video")]
        [ValidateSQLInjection]
        public ActionResult GetVideoList(String keywords, int draw, int start, int length)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> Video = MasterFacade.GetActiveVideoList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();

            for (int i = 0; i < Video.Count; i++)
            {
                data.Add(new String[] {Video[i].VideoID.ToString(), Video[i].Title, 
                    Video[i].ThumbnailPath, Video[i].Description, Video[i].ShowInHome?"Ya" : "Tidak",Video[i].VideoID.ToString(), String.Format("{{VideoID:{0}}}", Video[i].VideoID.ToString()) });
            }
            return Json(new { draw = draw, recordsTotal = MasterFacade.GetActiveVideoCount().ReturnData, recordsFiltered = MasterFacade.GetActiveVideoCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// The action to view/add/edit selected video
        /// </summary>
        /// <param name="videoID">The video id</param>
        /// <returns>The video view</returns>
        [ActionName("AddOrUpdateVideo")]
        [AuthorizeAccess("Video")]
        public ActionResult AddOrUpdateVideoView(int? videoID)
        {
            ViewBag.Title = TextResources.VideoView_Title;
            VideoViewModel model = new VideoViewModel();

            if (videoID.HasValue)
            {
                DataModel.Video video = MasterFacade.GetActiveVideoByVideoID(videoID.Value).ReturnData;
                if (video != null)
                {
                    model.VideoID = video.VideoID;
                    model.Title = video.Title;
                    model.Description = video.Description;
                    model.VideoURL = video.VideoURL;
                    model.ShowInHome = video.ShowInHome;
                    model.ThumbnailPath = video.ThumbnailPath;
                    model.IsEditMode = true;
                    return View("VideoForm", model);
                }
                else
                {
                    model.IsEditMode = false;
                    return View("VideoForm", model);
                }
            }
            else
            {
                model.IsEditMode = false;
                return View("VideoForm", model);
            }
        }

        /// <summary>
        /// Add or update video
        /// </summary>
        /// <param name="videoViewModel">The video view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Video")]
        public ActionResult AddOrUpdateVideo(VideoViewModel videoViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData["Model"] = videoViewModel;
                if (videoViewModel.IsEditMode)
                {
                    return RedirectToAction("UpdateVideo");
                }
                else
                {
                    return RedirectToAction("AddVideo");
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add video
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("Video", AuthorizationActionType.AllowAdd)]
        public ActionResult AddVideo()
        {
            FacadeResult<DataModel.Video> result;
            DataModel.Video video = new DataModel.Video();
            VideoViewModel videoViewModel = (VideoViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                video.Title = videoViewModel.Title;
                video.Description = videoViewModel.Description;
                video.ThumbnailPath = videoViewModel.ThumbnailPath;
                video.VideoURL = videoViewModel.VideoURL;
                video.ShowInHome = videoViewModel.ShowInHome;

                if (video.ShowInHome)
                {
                    if (MasterFacade.GetActiveVideoCountByShowInHome().ReturnData > 0)
                    {
                        return Content("<script language='javascript' type='text/javascript'>alert('Sudah ada video yang dipilih');</script>");
                    }
                }

                result = MasterFacade.AddNewVideo(video, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "Video", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update video
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("Video", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateVideo()
        {
            FacadeResult<DataModel.Video> result;
            DataModel.Video video = new DataModel.Video();
            VideoViewModel videoViewModel = (VideoViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                video.VideoID = videoViewModel.VideoID;
                video.Title = videoViewModel.Title;
                video.Description = videoViewModel.Description;
                video.ThumbnailPath = videoViewModel.ThumbnailPath;
                video.VideoURL = videoViewModel.VideoURL;
                video.ShowInHome = videoViewModel.ShowInHome;

                if (video.ShowInHome)
                {
                    if (MasterFacade.GetActiveVideoCountByShowInHome().ReturnData > 0)
                    {
                        return Content("<script language='javascript' type='text/javascript'>alert('Sudah ada video yang dipilih');</script>");
                    }
                }

                result = MasterFacade.UpdateVideo(video, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "Video", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Ajax upload photo1 file
        /// </summary>
        /// <param name="assetPhoto1">The photo1 file</param>
        /// <returns></returns>
        [ActionName("UploadPhoto1")]
        [AuthorizeAccess("Video")]
        public ActionResult UploadPhoto1(HttpPostedFileBase videoPhoto1)
        {
            if (IsFileAllowed(videoPhoto1.FileName) && CheckImageSize(videoPhoto1.InputStream, 640, 480, true))
            {
                SessionHelper.Remove(SessionKey.AssetPhoto1);
                String path = ConfigurationManager.AppSettings["ImageUploadPath"].ToString();
                String fileName = "tmp_" + SecurityHelper.GetCurrentLoggedInUserID().ToString() + "_" + DateTime.Today.ToString("yyyyMMddhhmmss") + "_" + new FileInfo(videoPhoto1.FileName).Name;
                videoPhoto1.SaveAs(path + fileName);
                SessionHelper.SetValue<String>(SessionKey.AssetPhoto1, fileName);
                return Json(new { IsSuccess = true, FileName = fileName }, "text/plain");
            }
            else
            {
                return Json(new { IsSuccess = false, FileName = String.Empty }, "text/plain");
            }
        }

        /// <summary>
        /// Delete video by video id
        /// </summary>
        /// <param name="videoID">The video id</param>
        /// <returns>JSON result</returns>
        [HttpPost]
        [AuthorizeAction("Video", AuthorizationActionType.AllowDelete)]
        public ActionResult DeleteVideo(int videoID)
        {
            FacadeResult<DataModel.Video> result = MasterFacade.DeleteVideoByID(videoID, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }
        #endregion

        #region Category
        /// <summary>
        /// A controller to return the default view for category
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The category view</returns>
        [ActionName("Category")]
        [AuthorizeAccess("Category")]
        public ActionResult CategoryListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.CategoryView_Title;
            CategoryViewModel model = new CategoryViewModel();

            return View("CategoryList", model);
        }

        /// <summary>
        /// The action to view/add/edit selected category
        /// </summary>
        /// <param name="categoryID">The category id</param>
        /// <returns>The category view</returns>
        [ActionName("AddOrUpdateCategory")]
        [AuthorizeAccess("Category")]
        public ActionResult AddOrUpdateCategoryView(int? categoryID)
        {
            ViewBag.Title = TextResources.CategoryView_Title;
            CategoryViewModel model = new CategoryViewModel();

            if (categoryID.HasValue)
            {
                DataModel.Category category = MasterFacade.GetActiveCategoryByCategoryID(categoryID.Value).ReturnData;
                if (category != null)
                {
                    model.CategoryID = category.CategoryID.Value;
                    model.Name = category.Name;
                    model.Description = category.Description;
                    model.IsEditMode = true;
                    return View("CategoryForm", model);
                }
                else
                {
                    model.IsEditMode = false;
                    return View("CategoryForm", model);
                }
            }
            else
            {
                model.IsEditMode = false;
                return View("CategoryForm", model);
            }
        }

        /// <summary>
        /// Add or update category
        /// </summary>
        /// <param name="categoryViewModel">The category view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Category")]
        public ActionResult AddOrUpdateCategory(CategoryViewModel categoryViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData["Model"] = categoryViewModel;
                if (categoryViewModel.IsEditMode)
                {
                    return RedirectToAction("UpdateCategory");
                }
                else
                {
                    return RedirectToAction("AddCategory");
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add category
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("Category", AuthorizationActionType.AllowAdd)]
        public ActionResult AddCategory()
        {
            FacadeResult<DataModel.Category> result;
            DataModel.Category category = new DataModel.Category();
            CategoryViewModel categoryViewModel = (CategoryViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                category.Name = categoryViewModel.Name;
                category.Description = categoryViewModel.Description;

                result = MasterFacade.AddNewCategory(category, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "Category", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update category
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("Category", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateCategory()
        {
            FacadeResult<DataModel.Category> result;
            DataModel.Category category = new DataModel.Category();
            CategoryViewModel categoryViewModel = (CategoryViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                category.CategoryID = categoryViewModel.CategoryID;
                category.Name = categoryViewModel.Name;
                category.Description = categoryViewModel.Description;

                result = MasterFacade.UpdateCategory(category, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "Category", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Get all active category list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON category data tables result</returns>
        [HttpPost]
        [ActionName("CategoryList")]
        [AuthorizeAccess("Category")]
        [ValidateSQLInjection]
        public ActionResult GetCategoryList(String keywords, int draw, int start, int length)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> categories = MasterFacade.GetActiveCategoriesList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();

            for (int i = 0; i < categories.Count; i++)
            {
                data.Add(new String[] { categories[i].CategoryID.ToString(), categories[i].Name, categories[i].Description, categories[i].CategoryID.ToString(), String.Format("{{categoryID:{0}}}", categories[i].CategoryID.ToString()) });
            }
            return Json(new { draw = draw, recordsTotal = MasterFacade.GetActiveCategoriesCount().ReturnData, recordsFiltered = MasterFacade.GetActiveCategoriesCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// Delete category by category id
        /// </summary>
        /// <param name="categoryID">The category id</param>
        /// <returns>JSON result</returns>
        [HttpPost]
        [AuthorizeAction("Category", AuthorizationActionType.AllowDelete)]
        public ActionResult DeleteCategory(int categoryID)
        {
            FacadeResult<DataModel.Category> result = MasterFacade.DeleteCategoryByID(categoryID, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }
        #endregion

        #region Segment
        /// <summary>
        /// A controller to return the default view for segment
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The category view</returns>
        [ActionName("Segment")]
        [AuthorizeAccess("Segment")]
        public ActionResult SegmentListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.SegmentView_Title;
            SegmentViewModel model = new SegmentViewModel();

            return View("SegmentList", model);
        }

        /// <summary>
        /// The action to view/add/edit selected segment
        /// </summary>
        /// <param name="segmentID">The segment id</param>
        /// <returns>The segment view</returns>
        [ActionName("AddOrUpdateSegment")]
        [AuthorizeAccess("Segment")]
        public ActionResult AddOrUpdateSegmentView(String segmentID)
        {
            ViewBag.Title = TextResources.CategoryView_Title;
            SegmentViewModel model = new SegmentViewModel();

            if (!String.IsNullOrEmpty(segmentID))
            {
                DataModel.Segment segment = MasterFacade.GetActiveSegmentBySegmentID(segmentID).ReturnData;
                if (segment != null)
                {
                    model.SegmentID = segment.SegmentID;
                    model.Name = segment.Name;
                    model.Description = segment.Description;
                    model.IsEditMode = true;
                    return View("SegmentForm", model);
                }
                else
                {
                    model.IsEditMode = false;
                    return View("SegmentForm", model);
                }
            }
            else
            {
                model.IsEditMode = false;
                return View("SegmentForm", model);
            }
        }

        /// <summary>
        /// Add or update segment
        /// </summary>
        /// <param name="segmentViewModel">The segment view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Segment")]
        public ActionResult AddOrUpdateSegment(SegmentViewModel segmentViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData["Model"] = segmentViewModel;
                if (segmentViewModel.IsEditMode)
                {
                    return RedirectToAction("UpdateSegment");
                }
                else
                {
                    return RedirectToAction("AddSegment");
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add segment
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("Segment", AuthorizationActionType.AllowAdd)]
        public ActionResult AddSegment()
        {
            FacadeResult<DataModel.Segment> result;
            DataModel.Segment segment = new DataModel.Segment();
            SegmentViewModel segmentViewModel = (SegmentViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                segment.SegmentID = segmentViewModel.SegmentID;
                segment.Name = segmentViewModel.Name;
                segment.Description = segmentViewModel.Description;

                result = MasterFacade.AddNewSegment(segment, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "Segment", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update segment
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("Segment", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateSegment()
        {
            FacadeResult<DataModel.Segment> result;
            DataModel.Segment segment = new DataModel.Segment();
            SegmentViewModel segmentViewModel = (SegmentViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                segment.SegmentID = segmentViewModel.SegmentID;
                segment.Name = segmentViewModel.Name;
                segment.Description = segmentViewModel.Description;

                result = MasterFacade.UpdateSegment(segment, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "Segment", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Get all active segment list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON province data tables result</returns>
        [HttpPost]
        [ActionName("SegmentList")]
        [AuthorizeAccess("Segment")]
        [ValidateSQLInjection]
        public ActionResult GetSegmentsList(String keywords, int draw, int start, int length)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> segments = MasterFacade.GetActiveSegmentsList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < segments.Count; i++)
            {
                data.Add(new String[] { segments[i].SegmentID.ToString(), segments[i].Name, segments[i].Description, segments[i].SegmentID, String.Format("{{segmentID:'{0}'}}", segments[i].SegmentID) });
            }
            return Json(new { draw = draw, recordsTotal = MasterFacade.GetActiveSegmentsCount().ReturnData, recordsFiltered = MasterFacade.GetActiveSegmentsCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// Delete segment by segment id
        /// </summary>
        /// <param name="segmentID">The segment id</param>
        /// <returns>JSON result</returns>
        [HttpPost]
        [AuthorizeAction("Segment", AuthorizationActionType.AllowDelete)]
        public ActionResult DeleteSegment(String segmentID)
        {
            FacadeResult<DataModel.Segment> result = MasterFacade.DeleteSegmentByID(segmentID, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }
        #endregion

        #region MessageCategoryType
        /// <summary>
        /// A controller to return the default view for message category type
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The message category type view</returns>
        [ActionName("MessageCategoryType")]
        [AuthorizeAccess("MessageCategoryType")]
        public ActionResult MessageCategoryTypeListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.MessageCategoryView_Title;
            MessageCategoryTypeViewModel model = new MessageCategoryTypeViewModel();

            return View("MessageCategoryTypeList", model);
        }

        /// <summary>
        /// The action to view/add/edit selected message category type
        /// </summary>
        /// <param name="messageCategoryTypeID">The message category type id</param>
        /// <returns>The message category type view</returns>
        [ActionName("AddOrUpdateMessageCategoryType")]
        [AuthorizeAccess("MessageCategoryType")]
        public ActionResult AddOrUpdateMessageCategoryTypeView(byte? messageCategoryTypeID)
        {
            ViewBag.Title = TextResources.CategoryView_Title;
            MessageCategoryTypeViewModel model = new MessageCategoryTypeViewModel();

            if (messageCategoryTypeID.HasValue)
            {
                DataModel.MessageCategoryType messageCategoryType = MasterFacade.GetActiveMessageCategoryTypeByMessageCategoryTypeID(messageCategoryTypeID.Value).ReturnData;
                if (messageCategoryType != null)
                {
                    model.MessageCategoryTypeID = messageCategoryType.MessageCategoryTypeID.Value;
                    model.Name = messageCategoryType.Name;
                    model.Description = messageCategoryType.Description;
                    model.IsEditMode = true;
                    return View("MessageCategoryTypeForm", model);
                }
                else
                {
                    model.IsEditMode = false;
                    return View("MessageCategoryTypeForm", model);
                }
            }
            else
            {
                model.IsEditMode = false;
                return View("MessageCategoryTypeForm", model);
            }
        }

        /// <summary>
        /// Add or update message category type
        /// </summary>
        /// <param name="messageCategoryTypeViewModel">The message category type view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("MessageCategoryType")]
        public ActionResult AddOrUpdateMessageCategoryType(MessageCategoryTypeViewModel messageCategoryTypeViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData["Model"] = messageCategoryTypeViewModel;
                if (messageCategoryTypeViewModel.IsEditMode)
                {
                    return RedirectToAction("UpdateMessageCategoryType");
                }
                else
                {
                    return RedirectToAction("AddMessageCategoryType");
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add message category type
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("MessageCategoryType", AuthorizationActionType.AllowAdd)]
        public ActionResult AddMessageCategoryType()
        {
            FacadeResult<DataModel.MessageCategoryType> result;
            DataModel.MessageCategoryType messageCategoryType = new DataModel.MessageCategoryType();
            MessageCategoryTypeViewModel messageCategoryTypeViewModel = (MessageCategoryTypeViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                messageCategoryType.Name = messageCategoryTypeViewModel.Name;
                messageCategoryType.Description = messageCategoryTypeViewModel.Description;

                result = MasterFacade.AddNewMessageCategoryType(messageCategoryType, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "MessageCategoryType", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update message category type
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("MessageCategoryType", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateMessageCategoryType()
        {
            FacadeResult<DataModel.MessageCategoryType> result;
            DataModel.MessageCategoryType messageCategoryType = new DataModel.MessageCategoryType();
            MessageCategoryTypeViewModel messageCategoryTypeViewModel = (MessageCategoryTypeViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                messageCategoryType.MessageCategoryTypeID = messageCategoryTypeViewModel.MessageCategoryTypeID;
                messageCategoryType.Name = messageCategoryTypeViewModel.Name;
                messageCategoryType.Description = messageCategoryTypeViewModel.Description;

                result = MasterFacade.UpdateMessageCategoryType(messageCategoryType, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "MessageCategoryType", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Get all active message category type list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON province data tables result</returns>
        [HttpPost]
        [ActionName("MessageCategoryTypeList")]
        [AuthorizeAccess("MessageCategoryType")]
        [ValidateSQLInjection]
        public ActionResult GetMessageCategoryTypesList(String keywords, int draw, int start, int length)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> messageCategoryTypes = MasterFacade.GetActiveMessageCategoryTypesList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < messageCategoryTypes.Count; i++)
            {
                data.Add(new String[] { messageCategoryTypes[i].MessageCategoryTypeID.ToString(), messageCategoryTypes[i].Name, messageCategoryTypes[i].Description, messageCategoryTypes[i].MessageCategoryTypeID.ToString(), String.Format("{{messageCategoryTypeID:{0}}}", messageCategoryTypes[i].MessageCategoryTypeID.ToString()) });
            }
            return Json(new { draw = draw, recordsTotal = MasterFacade.GetActiveMessageCategoryTypesCount().ReturnData, recordsFiltered = MasterFacade.GetActiveMessageCategoryTypesCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// Delete  message category type by  message category type id
        /// </summary>
        /// <param name="messageCategoryTypeID">The  message category type id</param>
        /// <returns>JSON result</returns>
        [HttpPost]
        [AuthorizeAction("MessageCategoryType", AuthorizationActionType.AllowDelete)]
        public ActionResult DeleteMessageCategoryType(byte messageCategoryTypeID)
        {
            FacadeResult<DataModel.MessageCategoryType> result = MasterFacade.DeleteMessageCategoryTypeByID(messageCategoryTypeID, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }
        #endregion

        #region OwnershipDocumentType
        /// <summary>
        /// A controller to return the default view for ownership document type
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The ownership document  type view</returns>
        [ActionName("OwnershipDocumentType")]
        [AuthorizeAccess("OwnershipDocumentType")]
        public ActionResult OwnershipDocumentTypeListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.OwnershipDocumentView_Title;
            OwnershipDocumentTypeViewModel model = new OwnershipDocumentTypeViewModel();

            return View("OwnershipDocumentTypeList", model);
        }

        /// <summary>
        /// The action to view/add/edit selected ownership document  type
        /// </summary>
        /// <param name="ownershipDocumentTypeID">The ownership document type id</param>
        /// <returns>The ownership document  type view</returns>
        [ActionName("AddOrUpdateOwnershipDocumentType")]
        [AuthorizeAccess("OwnershipDocumentType")]
        public ActionResult AddOrUpdateOwnershipDocumentTypeView(int? ownershipDocumentTypeID)
        {
            ViewBag.Title = TextResources.CategoryView_Title;
            OwnershipDocumentTypeViewModel model = new OwnershipDocumentTypeViewModel();

            if (ownershipDocumentTypeID.HasValue)
            {
                DataModel.OwnershipDocumentType messageCategoryType = MasterFacade.GetActiveOwnershipDocumentTypeByOwnershipDocumentTypeID(ownershipDocumentTypeID.Value).ReturnData;
                if (messageCategoryType != null)
                {
                    model.OwnershipDocumentTypeID = messageCategoryType.OwnershipDocumentTypeID.Value;
                    model.Name = messageCategoryType.Name;
                    model.Description = messageCategoryType.Description;
                    model.IsEditMode = true;
                    return View("OwnershipDocumentTypeForm", model);
                }
                else
                {
                    model.IsEditMode = false;
                    return View("OwnershipDocumentTypeForm", model);
                }
            }
            else
            {
                model.IsEditMode = false;
                return View("OwnershipDocumentTypeForm", model);
            }
        }

        /// <summary>
        /// Add or update ownership document type 
        /// </summary>
        /// <param name="ownershipDocumentTypeViewModel">The ownership document type  view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("OwnershipDocumentType")]
        public ActionResult AddOrUpdateOwnershipDocumentType(OwnershipDocumentTypeViewModel ownershipDocumentTypeViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData["Model"] = ownershipDocumentTypeViewModel;
                if (ownershipDocumentTypeViewModel.IsEditMode)
                {
                    return RedirectToAction("UpdateOwnershipDocumentType");
                }
                else
                {
                    return RedirectToAction("AddOwnershipDocumentType");
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add message ownership document type
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("OwnershipDocumentType", AuthorizationActionType.AllowAdd)]
        public ActionResult AddOwnershipDocumentType()
        {
            FacadeResult<DataModel.OwnershipDocumentType> result;
            DataModel.OwnershipDocumentType ownershipDocumentType = new DataModel.OwnershipDocumentType();
            OwnershipDocumentTypeViewModel ownershipDocumentTypeViewModel = (OwnershipDocumentTypeViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                ownershipDocumentType.Name = ownershipDocumentTypeViewModel.Name;
                ownershipDocumentType.Description = ownershipDocumentTypeViewModel.Description;

                result = MasterFacade.AddNewOwnershipDocumentType(ownershipDocumentType, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "OwnershipDocumentType", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update ownership document type 
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("OwnershipDocumentType", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateOwnershipDocumentType()
        {
            FacadeResult<DataModel.OwnershipDocumentType> result;
            DataModel.OwnershipDocumentType ownershipDocumentType = new DataModel.OwnershipDocumentType();
            OwnershipDocumentTypeViewModel ownershipDocumentTypeViewModel = (OwnershipDocumentTypeViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                ownershipDocumentType.OwnershipDocumentTypeID = ownershipDocumentTypeViewModel.OwnershipDocumentTypeID;
                ownershipDocumentType.Name = ownershipDocumentTypeViewModel.Name;
                ownershipDocumentType.Description = ownershipDocumentTypeViewModel.Description;

                result = MasterFacade.UpdateOwnershipDocumentType(ownershipDocumentType, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "OwnershipDocumentType", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Get all active ownership document list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON province data tables result</returns>
        [HttpPost]
        [ActionName("OwnershipDocumentTypeList")]
        [AuthorizeAccess("OwnershipDocumentType")]
        [ValidateSQLInjection]
        public ActionResult GetOwnershipDocumentTypesList(String keywords, int draw, int start, int length)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> ownershipDocumentTypes = MasterFacade.GetActiveOwnershipDocumentTypesList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < ownershipDocumentTypes.Count; i++)
            {
                data.Add(new String[] { ownershipDocumentTypes[i].OwnershipDocumentTypeID.ToString(), ownershipDocumentTypes[i].Name, ownershipDocumentTypes[i].Description, ownershipDocumentTypes[i].OwnershipDocumentTypeID.ToString(), String.Format("{{ownershipDocumentTypeID:{0}}}", ownershipDocumentTypes[i].OwnershipDocumentTypeID.ToString()) });
            }
            return Json(new { draw = draw, recordsTotal = MasterFacade.GetActiveOwnershipDocumentTypesCount().ReturnData, recordsFiltered = MasterFacade.GetActiveOwnershipDocumentTypesCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// Delete ownership document type by ownership document type id
        /// </summary>
        /// <param name="messageCategoryTypeID">The ownership document type id</param>
        /// <returns>JSON result</returns>
        [HttpPost]
        [AuthorizeAction("OwnershipDocumentType", AuthorizationActionType.AllowDelete)]
        public ActionResult DeleteOwnershipDocumentType(int ownershipDocumentTypeID)
        {
            FacadeResult<DataModel.OwnershipDocumentType> result = MasterFacade.DeleteOwnershipDocumentTypeByID(ownershipDocumentTypeID, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }
        #endregion

        #region Branch
        /// <summary>
        /// A controller to return the default view for branch
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The branch view</returns>
        [ActionName("Branch")]
        [AuthorizeAccess("Branch")]
        public ActionResult BranchListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.BranchView_Title;
            BranchViewModel model = new BranchViewModel();

            return View("BranchList", model);
        }

        /// <summary>
        /// The action to view/add/edit selected branch
        /// </summary>
        /// <param name="branchID">The branch id</param>
        /// <returns>The ownership document  type view</returns>
        [ActionName("AddOrUpdateBranch")]
        [AuthorizeAccess("Branch")]
        public ActionResult AddOrUpdateBranchView(int? branchID)
        {
            ViewBag.Title = TextResources.BranchView_Title;
            BranchViewModel model = new BranchViewModel();

            if (branchID.HasValue)
            {
                DataModel.Branch branch = MasterFacade.GetActiveBranchByBranchID(branchID.Value).ReturnData;
                if (branch != null)
                {
                    model.BranchID = branch.BranchID.Value;
                    model.Name = branch.Name;
                    model.Description = branch.Description;
                    model.Address = branch.Address;
                    model.Alias = branch.Alias;
                    model.Phone1 = branch.Phone1;
                    model.Phone2 = branch.Phone2;
                    model.IsEditMode = true;
                    return View("BranchForm", model);
                }
                else
                {
                    model.IsEditMode = false;
                    return View("BranchForm", model);
                }
            }
            else
            {
                model.IsEditMode = false;
                return View("BranchForm", model);
            }
        }

        /// <summary>
        /// Add or update branch
        /// </summary>
        /// <param name="branchViewModel">The branch  view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Branch")]
        public ActionResult AddOrUpdateBranch(BranchViewModel branchViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData["Model"] = branchViewModel;
                if (branchViewModel.IsEditMode)
                {
                    return RedirectToAction("UpdateBranch");
                }
                else
                {
                    return RedirectToAction("AddBranch");
                }
            }
            else
            {
                AddServerSideErrorToModelState();
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add branch
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("Branch", AuthorizationActionType.AllowAdd)]
        public ActionResult AddBranch()
        {
            FacadeResult<DataModel.Branch> result;
            DataModel.Branch branch = new DataModel.Branch();
            BranchViewModel branchViewModel = (BranchViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                branch.Name = branchViewModel.Name;
                branch.Description = branchViewModel.Description;
                branch.Alias = branchViewModel.Alias;
                branch.Address = branchViewModel.Address;
                branch.Phone1 = branchViewModel.Phone1;
                branch.Phone2 = branchViewModel.Phone2;

                result = MasterFacade.AddNewBranch(branch, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "Branch", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update branch
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("Branch", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateBranch()
        {
            FacadeResult<DataModel.Branch> result;
            DataModel.Branch branch = new DataModel.Branch();
            BranchViewModel branchViewModel = (BranchViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                branch.BranchID = branchViewModel.BranchID;
                branch.Name = branchViewModel.Name;
                branch.Description = branchViewModel.Description;
                branch.Alias = branchViewModel.Alias;
                branch.Address = branchViewModel.Address;
                branch.Phone1 = branchViewModel.Phone1;
                branch.Phone2 = branchViewModel.Phone2;

                result = MasterFacade.UpdateBranch(branch, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "Branch", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Get all active branch list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON province data tables result</returns>
        [HttpPost]
        [ActionName("BranchList")]
        [AuthorizeAccess("Branch")]
        [ValidateSQLInjection]
        public ActionResult GetBranchesList(String keywords, int draw, int start, int length)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> branches = MasterFacade.GetActiveBranchesList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < branches.Count; i++)
            {
                data.Add(new String[] { branches[i].BranchID.ToString(), branches[i].BranchName, branches[i].Description, branches[i].Alias, branches[i].Address, branches[i].Phone1, branches[i].Phone2, branches[i].BranchID.ToString(), String.Format("{{branchID:{0}}}", branches[i].BranchID.ToString()) });
            }
            return Json(new { draw = draw, recordsTotal = MasterFacade.GetActiveBranchesCount().ReturnData, recordsFiltered = MasterFacade.GetActiveBranchesCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// Delete branch by branch id
        /// </summary>
        /// <param name="branchID">The branch id</param>
        /// <returns>JSON result</returns>
        [HttpPost]
        [AuthorizeAction("Branch", AuthorizationActionType.AllowDelete)]
        public ActionResult DeleteBranch(int branchID)
        {
            FacadeResult<DataModel.Branch> result = MasterFacade.DeleteBranchByID(branchID, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }
        #endregion

        #region KPKNL
        /// <summary>
        /// A controller to return the default view for KPKNL
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The KPKNL view</returns>
        [ActionName("KPKNL")]
        [AuthorizeAccess("KPKNL")]
        public ActionResult KPKNLListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.KPKNLView_DisplayName;
            KPKNLViewModel model = new KPKNLViewModel();

            return View("KPKNLList", model);
        }

        /// <summary>
        /// The action to view/add/edit selected branch
        /// </summary>
        /// <param name="kpknlID">The kpknl id</param>
        /// <returns>The ownership document  type view</returns>
        [ActionName("AddOrUpdateKPKNL")]
        [AuthorizeAccess("KPKNL")]
        public ActionResult AddOrUpdateKPKNLView(int? kpknlID)
        {
            ViewBag.Title = TextResources.KPKNLView_DisplayName;
            KPKNLViewModel model = new KPKNLViewModel();

            if (kpknlID.HasValue)
            {
                DataModel.KPKNL kpknl = MasterFacade.GetActiveKPKNLByKPKNLID(kpknlID.Value).ReturnData;
                if (kpknl != null)
                {
                    model.KPKNLID = kpknl.KPKNLID.Value;
                    model.Name = kpknl.Name;
                    model.Description = kpknl.Description;
                    model.Address = kpknl.Address;
                    model.Phone1 = kpknl.Phone1;
                    model.Phone2 = kpknl.Phone2;
                    model.IsEditMode = true;
                    return View("KPKNLForm", model);
                }
                else
                {
                    model.IsEditMode = false;
                    return View("KPKNLForm", model);
                }
            }
            else
            {
                model.IsEditMode = false;
                return View("KPKNLForm", model);
            }
        }

        /// <summary>
        /// Add or update kpknl
        /// </summary>
        /// <param name="kpknlViewModel">The kpknl  view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("KPKNL")]
        public ActionResult AddOrUpdateKPKNL(KPKNLViewModel kpknlViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData["Model"] = kpknlViewModel;
                if (kpknlViewModel.IsEditMode)
                {
                    return RedirectToAction("UpdateKPKNL");
                }
                else
                {
                    return RedirectToAction("AddKPKNL");
                }
            }
            else
            {
                AddServerSideErrorToModelState();
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add kpknl
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("KPKNL", AuthorizationActionType.AllowAdd)]
        public ActionResult AddKPKNL()
        {
            FacadeResult<DataModel.KPKNL> result;
            DataModel.KPKNL kpknl = new DataModel.KPKNL();
            KPKNLViewModel kpknlViewModel = (KPKNLViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                kpknl.Name = kpknlViewModel.Name;
                kpknl.Description = kpknlViewModel.Description;
                kpknl.Address = kpknlViewModel.Address;
                kpknl.Phone1 = kpknlViewModel.Phone1;
                kpknl.Phone2 = kpknlViewModel.Phone2;

                result = MasterFacade.AddNewKPKNL(kpknl, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "KPKNL", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update kpknl
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("Branch", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateKPKNL()
        {
            FacadeResult<DataModel.KPKNL> result;
            DataModel.KPKNL kpknl = new DataModel.KPKNL();
            KPKNLViewModel kpknlViewModel = (KPKNLViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                kpknl.KPKNLID = kpknlViewModel.KPKNLID;
                kpknl.Name = kpknlViewModel.Name;
                kpknl.Description = kpknlViewModel.Description;
                kpknl.Address = kpknlViewModel.Address;
                kpknl.Phone1 = kpknlViewModel.Phone1;
                kpknl.Phone2 = kpknlViewModel.Phone2;

                result = MasterFacade.UpdateKPKNL(kpknl, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "KPKNL", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Get all active kpknl list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON province data tables result</returns>
        [HttpPost]
        [ActionName("KPKNLList")]
        [AuthorizeAccess("KPKNL")]
        [ValidateSQLInjection]
        public ActionResult GetKPKNLList(String keywords, int draw, int start, int length)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> kpknls = MasterFacade.GetActiveKPKNLsList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < kpknls.Count; i++)
            {
                data.Add(new String[] { kpknls[i].KPKNLID.ToString(), kpknls[i].Name, kpknls[i].Description, kpknls[i].Address, kpknls[i].Phone1, kpknls[i].Phone2, kpknls[i].KPKNLID.ToString(), String.Format("{{kpknlID:{0}}}", kpknls[i].KPKNLID.ToString()) });
            }
            return Json(new { draw = draw, recordsTotal = MasterFacade.GetActiveKPKNLsCount().ReturnData, recordsFiltered = MasterFacade.GetActiveKPKNLsCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// Delete kpknl by kpknl id
        /// </summary>
        /// <param name="kpknlID">The kpknl id</param>
        /// <returns>JSON result</returns>
        [HttpPost]
        [AuthorizeAction("KPKNL", AuthorizationActionType.AllowDelete)]
        public ActionResult DeleteKPKNL(int kpknlID)
        {
            FacadeResult<DataModel.KPKNL> result = MasterFacade.DeleteKPKNLByID(kpknlID, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }
        #endregion

        #region AuctionHall
        /// <summary>
        /// A controller to return the default view for auctionhall
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The auctionahll view</returns>
        [ActionName("AuctionHall")]
        [AuthorizeAccess("AuctionHall")]
        public ActionResult AuctionHallListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.AuctionHall_DisplayName;
            AuctionHallViewModel model = new AuctionHallViewModel();

            return View("AuctionHallList", model);
        }

        /// <summary>
        /// The action to view/add/edit selected auctionhall
        /// </summary>
        /// <param name="auctionhallID">The auction hall id</param>
        /// <returns>The ownership document  type view</returns>
        [ActionName("AddOrUpdateAuctionHall")]
        [AuthorizeAccess("AuctionHall")]
        public ActionResult AddOrUpdateAuctionHallView(int? auctionhallID)
        {
            ViewBag.Title = TextResources.AuctionHall_DisplayName;
            AuctionHallViewModel model = new AuctionHallViewModel();

            if (auctionhallID.HasValue)
            {
                DataModel.AuctionHall auctionHall = MasterFacade.GetActiveAuctionHallByAuctionHallID(auctionhallID.Value).ReturnData;
                if (auctionHall != null)
                {
                    model.AuctionHallID = auctionHall.AuctionHallID.Value;
                    model.Name = auctionHall.Name;
                    model.Description = auctionHall.Description;
                    model.Address = auctionHall.Address;
                    model.Phone1 = auctionHall.Phone1;
                    model.Phone2 = auctionHall.Phone2;
                    model.IsEditMode = true;
                    return View("AuctionHallForm", model);
                }
                else
                {
                    model.IsEditMode = false;
                    return View("AuctionHallForm", model);
                }
            }
            else
            {
                model.IsEditMode = false;
                return View("AuctionHallForm", model);
            }
        }

        /// <summary>
        /// Add or update auction hall
        /// </summary>
        /// <param name="auctionHallViewModel">The auction hall view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("AuctionHall")]
        public ActionResult AddOrUpdateAuctionHall(AuctionHallViewModel auctionHallViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData["Model"] = auctionHallViewModel;
                if (auctionHallViewModel.IsEditMode)
                {
                    return RedirectToAction("UpdateAuctionHall");
                }
                else
                {
                    return RedirectToAction("AddAuctionHall");
                }
            }
            else
            {
                AddServerSideErrorToModelState();
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add AuctionHall
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("AuctionHall", AuthorizationActionType.AllowAdd)]
        public ActionResult AddAuctionHall()
        {
            FacadeResult<DataModel.AuctionHall> result;
            DataModel.AuctionHall auctionHall = new DataModel.AuctionHall();
            AuctionHallViewModel auctionHallViewModel = (AuctionHallViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                auctionHall.Name = auctionHallViewModel.Name;
                auctionHall.Description = auctionHallViewModel.Description;
                auctionHall.Address = auctionHallViewModel.Address;
                auctionHall.Phone1 = auctionHallViewModel.Phone1;
                auctionHall.Phone2 = auctionHallViewModel.Phone2;

                result = MasterFacade.AddNewAuctionHall(auctionHall, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "AuctionHall", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update auctionHall
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("AuctionHall", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateAuctionHall()
        {
            FacadeResult<DataModel.AuctionHall> result;
            DataModel.AuctionHall auctionHall = new DataModel.AuctionHall();
            AuctionHallViewModel auctionHallViewModel = (AuctionHallViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                auctionHall.AuctionHallID = auctionHallViewModel.AuctionHallID;
                auctionHall.Name = auctionHallViewModel.Name;
                auctionHall.Description = auctionHallViewModel.Description;
                auctionHall.Address = auctionHallViewModel.Address;
                auctionHall.Phone1 = auctionHallViewModel.Phone1;
                auctionHall.Phone2 = auctionHallViewModel.Phone2;

                result = MasterFacade.UpdateAuctionHall(auctionHall, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "AuctionHall", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Get all active auction list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON province data tables result</returns>
        [HttpPost]
        [ActionName("AuctionHallList")]
        [AuthorizeAccess("AuctionHall")]
        [ValidateSQLInjection]
        public ActionResult GetAuctionHallList(String keywords, int draw, int start, int length)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> auctionHalls = MasterFacade.GetActiveAuctionHallsList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < auctionHalls.Count; i++)
            {
                data.Add(new String[] { auctionHalls[i].AuctionHallID.ToString(), auctionHalls[i].Name, auctionHalls[i].Description, auctionHalls[i].Address, auctionHalls[i].Phone1, auctionHalls[i].Phone2, auctionHalls[i].AuctionHallID.ToString(), String.Format("{{auctionHallID:{0}}}", auctionHalls[i].AuctionHallID.ToString()) });
            }
            return Json(new { draw = draw, recordsTotal = MasterFacade.GetActiveAuctionHallsCount().ReturnData, recordsFiltered = MasterFacade.GetActiveAuctionHallsCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// Delete auction hall by auction hall id
        /// </summary>
        /// <param name="auctionHallID">The auction hall id</param>
        /// <returns>JSON result</returns>
        [HttpPost]
        [AuthorizeAction("AuctionHall", AuthorizationActionType.AllowDelete)]
        public ActionResult DeleteAuctionHall(int auctionHallID)
        {
            FacadeResult<DataModel.AuctionHall> result = MasterFacade.DeleteAuctionHallByID(auctionHallID, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }
        #endregion

        #region MinMaxPrice
        /// <summary>
        /// A controller to return the default view for MinMaxPrice
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The MinMaxPrice view</returns>
        [ActionName("MinMaxPrice")]
        [AuthorizeAccess("MinMaxPrice")]
        public ActionResult MinMaxPriceListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.MinMaxPriceView_Title;
            MinMaxPriceViewModel model = new MinMaxPriceViewModel();

            return View("MinMaxPriceList", model);
        }

        /// <summary>
        /// The action to view/add/edit selected MinMaxPrice
        /// </summary>
        /// <param name="minMaxPriceID">The MinMaxPrice id</param>
        /// <returns>The MinMaxPrice view</returns>
        [ActionName("AddOrUpdateMinMaxPrice")]
        [AuthorizeAccess("MinMaxPrice")]
        public ActionResult AddOrUpdateMinMaxPriceView(int? minMaxPriceID)
        {
            ViewBag.Title = TextResources.MinMaxPrice_DisplayName;
            MinMaxPriceViewModel model = new MinMaxPriceViewModel();

            if (minMaxPriceID.HasValue)
            {
                DataModel.MinMaxPrice minMaxPrice = MasterFacade.GetActiveMinMaxPriceByMinMaxPriceID(minMaxPriceID.Value).ReturnData;
                if (minMaxPrice != null)
                {
                    model.MinMaxPriceID = minMaxPrice.MinMaxPriceID.Value;
                    model.Name = minMaxPrice.Name;
                    model.Description = minMaxPrice.Description;
                    model.Value = minMaxPrice.Value.Value;
                    model.IsEditMode = true;
                    return View("MinMaxPriceForm", model);
                }
                else
                {
                    model.IsEditMode = false;
                    return View("MinMaxPriceForm", model);
                }
            }
            else
            {
                model.IsEditMode = false;
                return View("MinMaxPriceForm", model);
            }
        }

        /// <summary>
        /// Add or update MinMaxPrice
        /// </summary>
        /// <param name="MinMaxPriceViewModel">The MinMaxPrice view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("MinMaxPrice")]
        public ActionResult AddOrUpdateMinMaxPrice(MinMaxPriceViewModel minMaxPriceViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData["Model"] = minMaxPriceViewModel;
                if (minMaxPriceViewModel.IsEditMode)
                {
                    return RedirectToAction("UpdateMinMaxPrice");
                }
                else
                {
                    return RedirectToAction("AddMinMaxPrice");
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add MinMaxPrice
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("MinMaxPrice", AuthorizationActionType.AllowAdd)]
        public ActionResult AddMinMaxPrice()
        {
            FacadeResult<DataModel.MinMaxPrice> result;
            DataModel.MinMaxPrice minMaxPrice = new DataModel.MinMaxPrice();
            MinMaxPriceViewModel minMaxPriceViewModel = (MinMaxPriceViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                minMaxPrice.Name = minMaxPriceViewModel.Name;
                minMaxPrice.Description = minMaxPriceViewModel.Description;
                minMaxPrice.Value = minMaxPriceViewModel.Value;

                result = MasterFacade.AddNewMinMaxPrice(minMaxPrice, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "MinMaxPrice", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update MinMaxPrice
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("MinMaxPrice", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateMinMaxPrice()
        {
            FacadeResult<DataModel.MinMaxPrice> result;
            DataModel.MinMaxPrice minMaxPrice = new DataModel.MinMaxPrice();
            MinMaxPriceViewModel minMaxPriceViewModel = (MinMaxPriceViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                minMaxPrice.MinMaxPriceID = minMaxPriceViewModel.MinMaxPriceID;
                minMaxPrice.Name = minMaxPriceViewModel.Name;
                minMaxPrice.Description = minMaxPriceViewModel.Description;
                minMaxPrice.Value = minMaxPriceViewModel.Value;

                result = MasterFacade.UpdateMinMaxPrice(minMaxPrice, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "MinMaxPrice", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Get all active MinMaxPrice list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON province data tables result</returns>
        [HttpPost]
        [ActionName("MinMaxPriceList")]
        [AuthorizeAccess("MinMaxPrice")]
        [ValidateSQLInjection]
        public ActionResult GetMinMaxPricesList(String keywords, int draw, int start, int length)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> minMaxPrices = MasterFacade.GetActiveMinMaxPricesList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < minMaxPrices.Count; i++)
            {
                data.Add(new String[] { minMaxPrices[i].MinMaxPriceID.ToString(), minMaxPrices[i].Name, minMaxPrices[i].Description, minMaxPrices[i].Value.ToString(), minMaxPrices[i].MinMaxPriceID.ToString(), String.Format("{{minMaxPriceID:{0}}}", minMaxPrices[i].MinMaxPriceID.ToString()) });
            }
            return Json(new { draw = draw, recordsTotal = MasterFacade.GetActiveMinMaxPricesCount().ReturnData, recordsFiltered = MasterFacade.GetActiveMinMaxPricesCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// Delete MinMaxPrice by MinMaxPrice id
        /// </summary>
        /// <param name="MinMaxPriceID">The MinMaxPrice id</param>
        /// <returns>JSON result</returns>
        [HttpPost]
        [AuthorizeAction("MinMaxPrice", AuthorizationActionType.AllowDelete)]
        public ActionResult DeleteMinMaxPrice(int minMaxPriceID)
        {
            FacadeResult<DataModel.MinMaxPrice> result = MasterFacade.DeleteMinMaxPriceByID(minMaxPriceID, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }
        #endregion

        #region CategoryTemplate
        /// <summary>
        /// A controller to return the default view for CategoryTemplate
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The CategoryTemplate view</returns>
        [ActionName("CategoryTemplate")]
        [AuthorizeAccess("CategoryTemplate")]
        public ActionResult CategoryTemplateListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.CategoryTemplate_DisplayName;
            CategoryTemplateViewModel model = new CategoryTemplateViewModel();

            return View("CategoryTemplateList", model);
        }

        /// <summary>
        /// The action to view/add/edit selected CategoryTemplate
        /// </summary>
        /// <param name="CategoryTemplateID">The CategoryTemplate id</param>
        /// <returns>The MinMaxPrice view</returns>
        [ActionName("AddOrUpdateCategoryTemplate")]
        [AuthorizeAccess("CategoryTemplate")]
        public ActionResult AddOrUpdateCategoryTemplateView(int? categoryID, String fieldName)
        {
            ViewBag.Title = TextResources.CategoryTemplate_DisplayName;
            CategoryTemplateViewModel model = new CategoryTemplateViewModel();

            model.Categories = MasterFacade.GetAllActiveCategories().ReturnData.AsEnumerable();
            if (categoryID.HasValue)
            {
                DataModel.CategoryTemplate categoryTemplate = MasterFacade.GetActiveCategoryTemplateByID(categoryID.Value, fieldName).ReturnData;
                if (categoryTemplate != null)
                {
                    model.CategoryID = categoryTemplate.CategoryID.Value;
                    model.FieldName = categoryTemplate.FieldName;
                    model.OriginalFieldName = categoryTemplate.FieldName;
                    model.IsMandatory = categoryTemplate.IsMandatory.Value;
                    model.IsEditMode = true;
                    return View("CategoryTemplateForm", model);
                }
                else
                {
                    model.IsEditMode = false;
                    return View("CategoryTemplateForm", model);
                }
            }
            else
            {
                model.IsEditMode = false;
                return View("CategoryTemplateForm", model);
            }
        }

        /// <summary>
        /// Add or update CategoryTemplate
        /// </summary>
        /// <param name="CategoryTemplate">The CategoryTemplate view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("CategoryTemplate")]
        public ActionResult AddOrUpdateCategoryTemplate(CategoryTemplateViewModel categoryTemplateViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData["Model"] = categoryTemplateViewModel;
                if (categoryTemplateViewModel.IsEditMode)
                {
                    return RedirectToAction("UpdateCategoryTemplate");
                }
                else
                {
                    return RedirectToAction("AddCategoryTemplate");
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add CategoryTemplate
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("CategoryTemplate", AuthorizationActionType.AllowAdd)]
        public ActionResult AddCategoryTemplate()
        {
            FacadeResult<DataModel.CategoryTemplate> result;
            DataModel.CategoryTemplate categoryTemplate = new DataModel.CategoryTemplate();
            CategoryTemplateViewModel categoryTemplateViewModel = (CategoryTemplateViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                categoryTemplate.CategoryID = categoryTemplateViewModel.CategoryID;
                categoryTemplate.FieldName = categoryTemplateViewModel.FieldName;
                categoryTemplate.IsMandatory = categoryTemplateViewModel.IsMandatory;

                result = MasterFacade.AddNewCategoryTemplate(categoryTemplate, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "CategoryTemplate", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update CategoryTemplate
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("CategoryTemplate", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateCategoryTemplate()
        {
            FacadeResult<DataModel.CategoryTemplate> result;
            DataModel.CategoryTemplate categoryTemplate = new DataModel.CategoryTemplate();
            CategoryTemplateViewModel categoryTemplateViewModel = (CategoryTemplateViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                categoryTemplate.CategoryID = categoryTemplateViewModel.CategoryID;
                categoryTemplate.FieldName = categoryTemplateViewModel.FieldName;
                categoryTemplate.IsMandatory = categoryTemplateViewModel.IsMandatory;

                result = MasterFacade.UpdateCategoryTemplate(categoryTemplate, categoryTemplateViewModel.OriginalFieldName, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "CategoryTemplate", "Master");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Get all active CategoryTemplate list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON CategoryTemplate data tables result</returns>
        [HttpPost]
        [ActionName("CategoryTemplateList")]
        [AuthorizeAccess("CategoryTemplate")]
        [ValidateSQLInjection]
        public ActionResult GetCategoryTemplatesList(String keywords, int draw, int start, int length)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> categoryTemplates = MasterFacade.GetActiveCategoryTemplatesList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < categoryTemplates.Count; i++)
            {
                data.Add(new String[] { categoryTemplates[i].Name, categoryTemplates[i].FieldName, categoryTemplates[i].IsMandatory.ToString(), String.Format("{0}|{1}", categoryTemplates[i].CategoryID.ToString(), categoryTemplates[i].FieldName), String.Format("{{categoryID:{0}, fieldName:'{1}'}}", categoryTemplates[i].CategoryID.ToString(), categoryTemplates[i].FieldName) });

            }
            return Json(new { draw = draw, recordsTotal = MasterFacade.GetActiveCategoryTemplatesCount().ReturnData, recordsFiltered = MasterFacade.GetActiveCategoryTemplatesCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// Delete CategoryTemplate by CategoryTemplate id
        /// </summary>
        /// <param name="categoryID">The Category id</param>
        /// <returns>JSON result</returns>
        [HttpPost]
        [AuthorizeAction("CategoryTemplate", AuthorizationActionType.AllowDelete)]
        public ActionResult DeleteCategoryTemplate(int categoryID, String fieldName)
        {
            FacadeResult<DataModel.CategoryTemplate> result = MasterFacade.DeleteCategoryTemplateByID(categoryID, fieldName, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }
        #endregion

        #region WebContent

        /// <summary>
        /// A controller to return the default view for webContent
        /// </summary>
        /// <param name="cancel">Cancel flag</param>
        /// <returns>The webContent view</returns>
        [ActionName("WebContent")]
        [AuthorizeAccess("WebContent")]
        public ActionResult WebContentListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.WebContentView_Title;
            WebContentViewModel model = new WebContentViewModel();

            return View("WebContentList", model);
        }

        /// <summary>
        /// The action to edit selected webContent
        /// </summary>
        /// <param name="webContentID">The webContent id</param>
        /// <returns>The webContent view</returns>
        [ActionName("UpdateWebContent")]
        [AuthorizeAccess("WebContent")]
        public ActionResult UpdateWebContentView(int? webContentTypeID)
        {
            ViewBag.Title = TextResources.WebContentView_Title;
            WebContentViewModel model = new WebContentViewModel();

            if (webContentTypeID.HasValue)
            {
                DataModel.WebContent webContent = MasterFacade.GetActiveWebContentByWebContentTypeID(webContentTypeID.Value).ReturnData;
                if (webContent != null)
                {
                    model.WebContentTypeID = webContent.WebContentTypeID.Value;
                    model.WebContentTypeName = MasterFacade.GetActiveWebContentTypeByWebContentTypeID(webContentTypeID.Value).ReturnData.Name;
                    model.Content = webContent.Content;
                    model.IsEditMode = true;
                    return View("WebContentForm", model);
                }
                else
                {
                    //model.IsEditMode = false;
                    return RedirectToAction("WebContent");
                }
            }
            else
            {
                //model.IsEditMode = false;
                return RedirectToAction("WebContent");
            }
        }

        /// <summary>
        /// Add or update webContent
        /// </summary>
        /// <param name="webContentViewModel">The webContent view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("WebContent")]
        [AuthorizeAction("WebContent", AuthorizationActionType.AllowUpdate)]
        [ValidateInput(false)]
        public ActionResult UpdateWebContent(WebContentViewModel webContentViewModel)
        {
            if (ModelState.IsValid)
            {
                FacadeResult<DataModel.WebContent> result;
                DataModel.WebContent webContent = new DataModel.WebContent();

                if (ModelState.IsValid)
                {
                    webContent.WebContentTypeID = webContentViewModel.WebContentTypeID;
                    webContent.Content = webContentViewModel.Content;

                    result = MasterFacade.UpdateWebContent(webContent, SecurityHelper.GetCurrentLoggedInUserID());

                    if (result.IsSuccess)
                    {
                        return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "WebContent", "Master");
                    }
                    else
                    {
                        ModelState.AddModelError("", result.ErrorMessage);
                    }
                }
                return PartialView("_ValidationSummary");
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Get all active webContent list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON webContent data tables result</returns>
        [HttpPost]
        [ActionName("WebContentList")]
        [AuthorizeAccess("WebContent")]
        [ValidateSQLInjection]
        public ActionResult GetWebContentList(String keywords, int draw, int start, int length)
        {
            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> webContents = MasterFacade.GetActiveWebContentsList(keywords, start, length, sortIndex, sortDirection).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < webContents.Count; i++)
            {
                data.Add(new String[] { webContents[i].WebContentTypeID.ToString(), webContents[i].Name, webContents[i].WebContentTypeID.ToString() });
            }
            return Json(new { draw = draw, recordsTotal = MasterFacade.GetActiveWebContentsCount().ReturnData, recordsFiltered = MasterFacade.GetActiveWebContentsCountByKeywords(keywords).ReturnData, data = data.ToArray() });
        }

        #endregion

        #endregion
    }
}