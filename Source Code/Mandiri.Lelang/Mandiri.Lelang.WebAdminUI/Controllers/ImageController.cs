﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mandiri.Lelang.BusinessFacade;
using System.IO;
using System.Web.Helpers;

namespace Mandiri.Lelang.WebAdminUI.Controllers
{
    /// <summary>
    /// The image controller
    /// </summary>
    [AllowAnonymous]
    public class ImageController : BaseController
    {
        /// <summary>
        /// Get image
        /// </summary>
        /// <returns>The image</returns>
        public void Get(long assetID, int idx)
        {
            DataModel.Asset asset = AssetFacade.GetActiveAssetByAssetID(assetID).ReturnData;
            String path = ConfigurationManager.AppSettings["ImageUploadPath"];
            String fileName = String.Format("{0}no-image.jpg", path);
            //String contentType = "Image/Jpeg";
            String nameCheck = String.Empty;

            if (idx == 1)
            {
                if (!String.IsNullOrEmpty(asset.Photo1))
                {
                    nameCheck = String.Format("{0}{1}", path, asset.Photo1);
                }
            }
            else if (idx == 2)
            {
                if (!String.IsNullOrEmpty(asset.Photo2))
                {
                    nameCheck = String.Format("{0}{1}", path, asset.Photo2);
                }
            }
            else if (idx == 3)
            {
                if (!String.IsNullOrEmpty(asset.Photo3))
                {
                    nameCheck = String.Format("{0}{1}", path, asset.Photo3);
                }
            }
            else if (idx == 4)
            {
                if (!String.IsNullOrEmpty(asset.Photo4))
                {
                    nameCheck = String.Format("{0}{1}", path, asset.Photo4);
                }
            }
            else if (idx == 5)
            {
                if (!String.IsNullOrEmpty(asset.Photo5))
                {
                    nameCheck = String.Format("{0}{1}", path, asset.Photo5);
                }
            }

            if (!String.IsNullOrEmpty(nameCheck) && new FileInfo(nameCheck).Exists)
            {
                fileName = nameCheck;
            }

            /*if (fileName.ToUpper().Contains("JPEG") || fileName.ToUpper().Contains("JPG"))
            {
                contentType = "Image/Jpeg";
            }
            else if (fileName.ToUpper().Contains("PNG"))
            {
                contentType = "Image/Png";
            }*/

            WebImage image = new WebImage(fileName);
            if (asset.CurrentStatusID == 5)
            {
                try
                {
                    image.AddTextWatermark("TERJUAL", fontFamily: "Stencil", fontColor: "Red", fontSize: 80, fontStyle: "Bold", horizontalAlign: "Center", verticalAlign: "Middle", opacity: 85);
                }
                catch
                {
                    image.AddTextWatermark("TERJUAL", fontColor: "Red", fontSize: 80, fontStyle: "Bold", horizontalAlign: "Center", verticalAlign: "Middle", opacity: 85);
                }
            }
            else if (asset.CurrentStatusID == 7)
            {
                try
                {
                    image.AddTextWatermark("LUNAS", fontFamily: "Stencil", fontColor: "Red", fontSize: 82, fontStyle: "Bold", horizontalAlign: "Center", verticalAlign: "Middle", opacity: 85);
                }
                catch
                {
                    image.AddTextWatermark("LUNAS", fontColor: "Red", fontSize: 82, fontStyle: "Bold", horizontalAlign: "Center", verticalAlign: "Middle", opacity: 85);
                }
            }
            image.Write();

            //return File(fileName, contentType);
        }
    }
}
