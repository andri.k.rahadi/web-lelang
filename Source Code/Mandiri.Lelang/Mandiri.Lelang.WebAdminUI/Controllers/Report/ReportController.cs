﻿using Mandiri.Lelang.Attribute.ActionFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mandiri.Lelang.WebAdminUI.Controllers.Report
{
    public class ReportController : BaseController
    {
        [AuthorizeAccess("ReportDataAgunan")]
        [ActionName("ReportDataAgunan")]
        public ActionResult ReportDataAgunan()
        {
            ViewBag.ReportPath = "/Mandiri Lelang/Report Data Agunan";
            return View("Report");
        }

        [AuthorizeAccess("ReportHistoriAgunan")]
        [ActionName("ReportHistoriAgunan")]
        public ActionResult ReportHistoriAgunan()
        {
            ViewBag.ReportPath = "/Mandiri Lelang/Report Histori Agunan";
            return View("Report");
        }

        [AuthorizeAccess("ReportStatistikVisitor")]
        [ActionName("ReportStatistikVisitor")]
        public ActionResult ReportStatistikVisitor()
        {
            ViewBag.ReportPath = "/Mandiri Lelang/Report Statistik Visitor";
            return View("Report");
        }

        [AuthorizeAccess("ReportTrendVisitor")]
        [ActionName("ReportTrendVisitor")]
        public ActionResult ReportTrendVisitor()
        {
            ViewBag.ReportPath = "/Mandiri Lelang/Report Trend Visitor";
            return View("Report");
        }

        [AuthorizeAccess("ReportDailyVisitor")]
        [ActionName("ReportDailyVisitor")]
        public ActionResult ReportDailyVisitor()
        {
            ViewBag.ReportPath = "/Mandiri Lelang/Report Daily Visitor";
            return View("Report");
        }

        [AuthorizeAccess("ReportMember")]
        [ActionName("ReportMember")]
        public ActionResult ReportMember()
        {
            ViewBag.ReportPath = "/Mandiri Lelang/Report Member";
            return View("Report");
        }

        [AuthorizeAccess("ReportMemberActivity")]
        [ActionName("ReportMemberActivity")]
        public ActionResult ReportMemberActivity()
        {
            ViewBag.ReportPath = "/Mandiri Lelang/Report Detail Daily Visitor";
            return View("Report");
        }

        [AuthorizeAccess("ReportFavorite")]
        [ActionName("ReportFavorite")]
        public ActionResult ReportFavorite()
        {
            ViewBag.ReportPath = "/Mandiri Lelang/Report Favorite";
            return View("Report");
        }

        [AuthorizeAccess("ReportHubungiKami")]
        [ActionName("ReportHubungiKami")]
        public ActionResult ReportHubungiKami()
        {
            ViewBag.ReportPath = "/Mandiri Lelang/Report Hubungi Kami";
            return View("Report");
        }
    }
}
