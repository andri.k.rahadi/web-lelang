﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.Helper.Enum;
using Mandiri.Lelang.Helper.Extension;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.WebAdmin;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.Attribute.ActionFilter;
using Mandiri.Lelang.Attribute.Enum;
using System.Dynamic;
using Mandiri.Lelang.DataModel;
using System.Configuration;
using System.IO;
using System.Text;

namespace Mandiri.Lelang.WebAdminUI.Controllers.Asset
{
    public class AssetController : BaseController
    {

        #region Action

        /// <summary>
        /// A controller to return the default view for asset
        /// </summary>
        /// <param name="cancel"></param>
        /// <returns></returns>
        [ActionName("AssetData")]
        [AuthorizeAccess("AssetData")]
        public ActionResult GroupListView(bool cancel = false, int? updatePrice = 0)
        {
            ViewBag.Title = TextResources.AssetView_Title;
            AssetViewModel model = new AssetViewModel();
            model.UpdatePrice = updatePrice.Value;

            return View("AssetList", model);
        }

        /// <summary>
        /// Get all active asset list
        /// </summary>
        /// <param name="keywords">The keywords identifier</param>
        /// <param name="draw">The draw identifier</param>
        /// <param name="start">The start identifier</param>
        /// <param name="length">The length identifier</param>
        /// <returns>JSON asset data tables result</returns>
        [HttpPost]
        [ActionName("AssetList")]
        [AuthorizeAccess("AssetData")]
        [ValidateSQLInjection]
        public ActionResult GetAssetList(String keywords, int draw, int start, int length, int updatePrice = 0)
        {
            AssetFacade.ClearPhotoSession();

            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            DataModel.User user = AdministrationFacade.GetActiveUserByUserID(SecurityHelper.GetCurrentLoggedInUserID()).ReturnData;
            int branchID = user.BranchID.HasValue ? user.BranchID.Value : -1;
            IList<dynamic> assets = AssetFacade.GetActiveAssetsList(branchID, keywords, start, length, sortIndex, sortDirection, updatePrice).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < assets.Count; i++)
            {

                data.Add(new String[] { 
                    assets[i].AssetID == null ? String.Empty : assets[i].AssetID.ToString() , 
                    assets[i].AssetCode, 
                    assets[i].SegmentName,
                    assets[i].CategoryName, 
                    //assets[i].Address,
                    assets[i].ProvinceName,
                    assets[i].CityName,
                    assets[i].Type,
                    assets[i].Price.ToString(),
                    assets[i].OldPrice.ToString(),
                    assets[i].NewPrice.ToString(),
                    assets[i].AssetID == null ? String.Empty : assets[i].AssetID.ToString(),//assets[i].Photo1,
                    assets[i].Status == null ? String.Empty : assets[i].Status.ToString(),
                    assets[i].AdditionalNotes  == null ? String.Empty : assets[i].AdditionalNotes.ToString(),
                    assets[i].AssetID == null ? String.Empty : assets[i].AssetID.ToString(),
                    String.Format("{{assetID:{0}}}", assets[i].AssetID == null ? String.Empty : assets[i].AssetID.ToString()),
                    assets[i].AssetID == null ? String.Empty : assets[i].AssetID.ToString()
                });
            }
            return Json(new { draw = draw, recordsTotal = AssetFacade.GetActiveAssetsCount(branchID).ReturnData, recordsFiltered = AssetFacade.GetActiveAssetsCountByKeywords(branchID, keywords, updatePrice).ReturnData, data = data.ToArray() });
        }

        /// <summary>
        /// The action to view/add/edit selected asset
        /// </summary>
        /// <param name="assetID">The asset id</param>
        /// <returns>The group view</returns>
        [ActionName("AddOrUpdateAsset")]
        [AuthorizeAccess("AssetData")]
        public ActionResult AddOrUpdateAssetView(int? assetID, int? type)
        {
            ViewBag.Title = TextResources.AssetView_Title;
            AssetViewModel model = new AssetViewModel();

            AssetFacade.ClearPhotoSession();

            // Get Data By User Group 
            String approver = ConfigurationManager.AppSettings["Approver_GroupID"].ToString();
            String inputer = ConfigurationManager.AppSettings["Inputer_GroupID"].ToString();
            model.UserGroupType = AdministrationFacade.GetGroupIDByUserID(SecurityHelper.GetCurrentLoggedInUserID(), Int32.Parse(approver), Int32.Parse(inputer)).ReturnData;

            model.Segments = MasterFacade.GetAllActiveSegments().ReturnData.AsEnumerable();
            model.Provinces = MasterFacade.GetAllActiveProvinces().ReturnData.OrderBy(p => p.Name).AsEnumerable();
            model.KPKNLs = MasterFacade.GetAllActiveKPKNLs().ReturnData.AsEnumerable();
            model.Cities = MasterFacade.GetAllActiveCities().ReturnData.OrderBy(c => c.Name).AsEnumerable();
            model.Types = MasterFacade.GetAllStaticTypes().ReturnData.AsEnumerable();
            model.Type = 2;

            int branchID = AdministrationFacade.GetActiveUserByUserID(SecurityHelper.GetCurrentLoggedInUserID()).ReturnData.BranchID.Value;
            if (branchID == -1)
            {
                model.Branches = MasterFacade.GetAllActiveBranches().ReturnData.AsEnumerable();
            }
            else
            {
                IList<Branch> listBranch = new List<Branch>();
                listBranch.Add(MasterFacade.GetActiveBranchByBranchID(branchID).ReturnData);
                model.Branches = listBranch.AsEnumerable();
            }

            model.OwnershipDocumentTypes = MasterFacade.GetAllActiveOwnershipDocumentTypes().ReturnData.AsEnumerable();
            model.AuctionHalls = MasterFacade.GetAllActiveAuctionHalls().ReturnData.AsEnumerable();
            model.AssetStatuses = MasterFacade.GetAllActiveAssetStatuses().ReturnData.AsEnumerable();
            model.Categories = MasterFacade.GetAllActiveCategories().ReturnData.AsEnumerable();

            List<AuctionViewModel> listSchedules = new List<AuctionViewModel>();
            AuctionViewModel jadwalBaru = new AuctionViewModel();
            jadwalBaru.Name = "Jadwal Lelang Baru";
            jadwalBaru.AuctionScheduleID = -1;
            listSchedules.Add(jadwalBaru);
            IList<dynamic> listScheduleItems = AssetFacade.GetAllActiveAuctionSchedules().ReturnData;
            if (listScheduleItems != null)
            {
                for (int i = 0; i < listScheduleItems.Count; i++)
                {
                    AuctionViewModel viewModel = new AuctionViewModel();
                    viewModel.AuctionScheduleID = listScheduleItems[i].AuctionScheduleID;
                    viewModel.Name = listScheduleItems[i].ScheduleDate.ToString("dd MMM yyyy") + " " + listScheduleItems[i].ScheduleTime.ToString("HH:mm") + " " + listScheduleItems[i].TimeZone + "(" + listScheduleItems[i].Name + ")";

                    listSchedules.Add(viewModel);
                }
            }

            model.AuctionSchedules = listSchedules.AsEnumerable();
            model.ButtonType = 0;
            model.ApprovalType = type;

            if (assetID.HasValue)
            {
                DataModel.Asset asset = AssetFacade.GetActiveAssetByAssetID(assetID.Value).ReturnData;
                if (asset != null)
                {
                    model.AssetID = asset.AssetID.Value;
                    model.AssetCode = asset.AssetCode;
                    model.AccountNumber = asset.AccountNumber;
                    model.IsEmailAuction = asset.IsEmailAuction.HasValue ? asset.IsEmailAuction.Value : false;
                    model.EmailAuctionURL = asset.EmailAuctionURL;
                    model.AccountName = asset.AccountName;
                    model.CategoryID = asset.CategoryID.Value;                    
                    model.Address = asset.Address;
                    model.ProvinceID = asset.ProvinceID.Value;
                    model.CityID = asset.CityID.Value;
                    model.OldPrice = asset.OldPrice.HasValue ? asset.OldPrice.Value : 0;
                    model.NewPrice = asset.NewPrice.HasValue ? asset.NewPrice.Value : 0;
                    model.BranchID = asset.BranchID;
                    model.MarketValue = asset.MarketValue.HasValue ? asset.MarketValue.Value : 0;
                    model.LiquidationValue = asset.LiquidationValue.HasValue ? asset.LiquidationValue.Value : 0;
                    model.SecurityRightsValue = asset.SecurityRightsValue.HasValue ? asset.SecurityRightsValue.Value : 0;
                    model.SoldValue = asset.SoldValue.HasValue ? asset.SoldValue.Value : 0;
                    model.ValuationDate = asset.ValuationDate;
                    model.AuctionScheduleID = asset.AuctionScheduleID.HasValue ? (int)asset.AuctionScheduleID.Value : 0;
                    model.AuctionHallID = asset.AuctionHallID;
                    model.AuctionHallAddress = asset.AuctionHallAddress;
                    model.AuctionHallPhone1 = asset.AuctionHallPhone1;
                    model.AuctionHallPhone2 = asset.AuctionHallPhone2;
                    model.KPKNLID = asset.KPKNLID;
                    model.KPKNLAddress = asset.KPKNLAddress;
                    model.AuctionAddress = asset.AuctionAddress;
                    model.KPKNLPhone1 = asset.KPKNLPhone1;
                    model.KPKNLPhone2 = asset.KPKNLPhone2;
                    model.SegmentID = asset.SegmentID;
                    model.Photo1 = asset.Photo1;
                    SessionHelper.SetValue(SessionKey.AssetPhoto1, asset.Photo1);
                    model.Photo2 = asset.Photo2;
                    SessionHelper.SetValue(SessionKey.AssetPhoto2, asset.Photo2);
                    model.Photo3 = asset.Photo3;
                    SessionHelper.SetValue(SessionKey.AssetPhoto3, asset.Photo3);
                    model.Photo4 = asset.Photo4;
                    SessionHelper.SetValue(SessionKey.AssetPhoto4, asset.Photo4);
                    model.Photo5 = asset.Photo5;
                    SessionHelper.SetValue(SessionKey.AssetPhoto5, asset.Photo5);
                    model.OwnerShipDocumentTypeID = asset.OwnershipDocumentTypeID;
                    model.DocumentNumber = asset.DocumentNumber;
                    model.CurrentStatusID = asset.CurrentStatusID;
                    model.Latitude = asset.Latitude;
                    model.Longitude = asset.Longitude;
                    model.TransferInformation = asset.TransferInformation;
                    model.AdditionalNotes = asset.AdditionalNotes;
                    model.CategoryTemplates = GetAssetCategoryTemplateViewModelListByAssetID((int)asset.CategoryID, asset.AssetID);
                    model.CategoryTemplateReasons = GetAssetCategoryTemplateViewModelListByAssetID((int)asset.CategoryID, asset.AssetID);
                    model.ButtonType = GetButtonTypeForStatus((int)asset.CurrentStatusID);
                    if (asset.AuctionScheduleID.HasValue)
                    {
                        AuctionSchedule auctionSchedule = AssetFacade.GetActiveAuctionScheduleByAuctionScheduleID(asset.AuctionScheduleID.Value).ReturnData;
                        if (auctionSchedule != null)
                        {
                            model.AuctionDate = auctionSchedule.ScheduleDate;
                            model.AuctionTime = auctionSchedule.ScheduleTime.HasValue ? auctionSchedule.ScheduleTime.Value.ToString("HH:mm") : String.Empty;
                            model.TimeZoneID = auctionSchedule.Timezone;
                            model.KPKNLID = auctionSchedule.KPKNLID;
                            //model.KPKNLAddress = auctionSchedule.AuctionAddress;
                            model.AuctionAddress = auctionSchedule.AuctionAddress;
                            model.KPKNLPhone1 = auctionSchedule.Phone1;
                            model.KPKNLPhone2 = auctionSchedule.Phone2;
                        }
                    }
                    model.Type = asset.Type;
                    model.Price = asset.Price.HasValue ? asset.Price.Value : 0;
                    model.ExpiredDate = asset.ExpiredDate;

                    // Status Checking 
                    SetAssetStatusAndAssetStatuses(model);

                    AssetChanges changes = AssetFacade.GetActiveAssetChangeByAssetID(model.AssetID).ReturnData;
                    if (changes != null)
                    {
                        if (changes.IsNew.Value) model.ApproveOrRejectType = "Penambahan";
                        if (changes.IsUpdated.Value) model.ApproveOrRejectType = "Perubahan";
                        if (changes.IsDeleted.Value) model.ApproveOrRejectType = "Penghapusan";
                    }

                    AssetChanges checkIfEditFromReject = AssetFacade.GetActiveAssetChangeByAssetIDAndApprovalStatus(asset.AssetID, 2).ReturnData;
                    if (!model.ApprovalType.HasValue && checkIfEditFromReject != null)
                    {
                        model.ApprovalType = 3;
                        model.CurrentStatusID = checkIfEditFromReject.OriginalAssetStatusID;
                    }

                    if (model.ApprovalType.HasValue)
                    {
                        if (model.ApprovalType.Value == 3)
                        {
                            if (checkIfEditFromReject != null)
                            {
                                SetRejectReasonForModel(model, assetID.Value, checkIfEditFromReject.AssetChangesID.Value);
                            }
                        }
                    }
                    model.IsEditMode = true;
                    return View("AssetForm", model);
                }
                else
                {
                    model.IsEditMode = false;
                    model.AssetStatuses = MasterFacade.GetAllActiveAssetStatusesByStatusID(1).ReturnData.AsEnumerable();
                    model.AssetStatusID = 1;
                    return View("AssetForm", model);
                }
            }
            else
            {
                model.IsEditMode = false;
                model.AssetStatuses = MasterFacade.GetAllActiveAssetStatusesByStatusID(1).ReturnData.AsEnumerable();
                model.AssetStatusID = 1;
                return View("AssetForm", model);
            }
        }

        private void SetRejectReasonForModel(AssetViewModel model, long assetID, long assetChangesID)
        {

            IList<dynamic> reason = AssetFacade.GetActiveAssetRejectReasonList(assetID).ReturnData;
            if (reason != null)
            {
                for (int i = 0; i < reason.Count; i++)
                {
                    String fieldOrColumnName = reason[i].FieldOrColumnName;
                    String value = String.IsNullOrEmpty(reason[i].Reason) ? String.Empty : reason[i].Reason;
                    if (!String.IsNullOrEmpty(fieldOrColumnName))
                    {
                        switch (fieldOrColumnName)
                        {
                            case "SegmetID":
                                model.SegmentRejectReason = value;
                                break;
                            case "CategoryID":
                                model.CategoryRejectReason = value;
                                break;
                            case "AccountNumber":
                                model.AccountNumberRejectReason = value;
                                break;
                            case "IsEmailAuction":
                                model.IsEmailAuctionRejectReason = value;
                                break;
                            case "EmailAuctionURL":
                                model.EmailAuctionURLRejectReason = value;
                                break;
                            case "AccountName":
                                model.AccountNameRejectReason = value;
                                break;
                            case "ProvinceID":
                                model.ProvinceRejectReason = value;
                                break;
                            case "CityID":
                                model.CityNameRejectReason = value;
                                break;
                            case "Latitude":
                                model.LatitudeRejectReason = value;
                                break;
                            case "Longitude":
                                model.LongitudeRejectReason = value;
                                break;
                            case "Address":
                                model.AddressRejectReason = value;
                                break;
                            case "OldPrice":
                                model.OldPriceRejectReason = value;
                                break;
                            case "NewPrice":
                                model.NewPriceRejectReason = value;
                                break;
                            case "MarketValue":
                                model.MarketValueRejectReason = value;
                                break;
                            case "LiquidationValue":
                                model.LiquidationValueRejectReason = value;
                                break;
                            case "SecurityRightsValue":
                                model.SecurityRightsValueRejectReason = value;
                                break;
                            case "SoldValue":
                                model.SoldValueRejectReason = value;
                                break;
                            case "ValuationDate":
                                model.ValuationDateRejectReason = value;
                                break;
                            case "BranchID":
                                model.BranchRejectReason = value;
                                break;
                            case "AuctionDate":
                                model.AuctionDateRejectReason = value;
                                break;
                            case "AuctionScheduleID":
                                model.AuctionScheduleRejectReason = value;
                                break;
                            case "AuctionTime":
                                model.AuctionTimeRejectReason = value;
                                break;
                            case "TimeZoneID":
                                model.TimezoneRejectReason = value;
                                break;
                            case "TransferInformation":
                                model.TransferInformationRejectReason = value;
                                break;
                            case "KPKNLID":
                                model.KPKNLRejectReason = value;
                                break;
                            case "KPKNLAddress":
                                model.KPKNLAddressRejectReason = value;
                                break;
                            case "AuctionAddress":
                                model.AuctionAddressRejectReason = value;
                                break;
                            case "KPKNLPhone1":
                                model.KPKNLPhone1RejectReason = value;
                                break;
                            case "KPKNLPhone2":
                                model.KPKNLPhone2RejectReason = value;
                                break;
                            case "AuctionHallID":
                                model.AuctionHallRejectReason = value;
                                break;
                            case "AuctionHallAddress":
                                model.AuctionHallAddressRejectReason = value;
                                break;
                            case "AuctionHallPhone1":
                                model.AuctionHallPhone1RejectReason = value;
                                break;
                            case "AuctionHallPhone2":
                                model.AuctionHallPhone2RejectReason = value;
                                break;
                            case "Photo1":
                                model.Photo1RejectReason = value;
                                break;
                            case "Photo2":
                                model.Photo2RejectReason = value;
                                break;
                            case "Photo3":
                                model.Photo3RejectReason = value;
                                break;
                            case "Photo4":
                                model.Photo4RejectReason = value;
                                break;
                            case "Photo5":
                                model.Photo5RejectReason = value;
                                break;
                            case "OwnershipDocumentTypeID":
                                model.OwnershipDocumentTypeRejectReason = value;
                                break;
                            case "DocumentNumber":
                                model.DocumentNumberRejectReason = value;
                                break;
                            case "AdditionalNotes":
                                model.AdditionalNoteRejectReason = value;
                                break;
                            case "Type":
                                model.TypeRejectReason = value;
                                break;
                            case "Price":
                                model.PriceRejectReason = value;
                                break;
                            case "ExpiredDate":
                                model.ExpiredDateRejectReason = value;
                                break;
                        }

                        if (model.CategoryTemplates != null)
                        {
                            for (int j = 0; j < model.CategoryTemplates.Count; j++)
                            {
                                AssetCategoryTemplateViewModel m = model.CategoryTemplates[j];
                                if (m.FieldName == fieldOrColumnName)
                                {
                                    model.CategoryTemplateReasons[j].FieldName = fieldOrColumnName;
                                    model.CategoryTemplateReasons[j].Value = value;
                                }
                            }
                        }


                    }

                }

                AssetWorkflowHistory workflow = AssetFacade.GetActiveAssetWorkFlowHistoryRejectedByAssetChangesID(assetChangesID).ReturnData;
                if (workflow != null)
                {
                    model.RejectReason = workflow.Reason;
                }
            }
        }

        /// <summary>
        /// Add or update asset
        /// </summary>
        /// <param name="groupViewModel">The asset view model</param>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Asset")]
        public ActionResult AddOrUpdateAsset(AssetViewModel assetViewModel)
        {
            if (ModelState.IsValid)
            {
                //Fix photo validation
                //By Ricardo Alexander
                //Date 4 June 2015
                String photo1 = SessionHelper.GetValue<String>(SessionKey.AssetPhoto1);
                if (!String.IsNullOrEmpty(photo1))
                {
                    //Fix asset category template validation
                    //11 Apr 2016
                    bool customFieldsValid = true;
                    if ((assetViewModel.ApprovalType.HasValue && assetViewModel.ApprovalType.Value == 3) || !assetViewModel.ApprovalType.HasValue)
                    {
                        foreach (AssetCategoryTemplateViewModel catTemplate in assetViewModel.CategoryTemplates)
                        {
                            if (assetViewModel.CategoryID.HasValue)
                            {
                                CategoryTemplate categoryTemplate = MasterFacade.GetActiveCategoryTemplateByID(assetViewModel.CategoryID.Value, catTemplate.FieldName).ReturnData;
                                if (categoryTemplate.IsMandatory.HasValue && categoryTemplate.IsMandatory.Value == true && (String.IsNullOrWhiteSpace(catTemplate.Value) || String.IsNullOrEmpty(catTemplate.Value)))
                                {
                                    ModelState.AddModelError("", String.Format("{0} is required", catTemplate.FieldName));
                                    customFieldsValid = false;
                                }
                            }
                        }
                    }

                    if (customFieldsValid)
                    {
                        TempData["Model"] = assetViewModel;

                        if (assetViewModel.ApprovalType.HasValue)
                        {
                            if (assetViewModel.ApprovalType == 1)
                            {
                                return RedirectToAction("ApproveAsset");
                            }
                            else if (assetViewModel.ApprovalType == 2)
                            {
                                return RedirectToAction("RejectAsset");
                            }
                            else if (assetViewModel.ApprovalType == 3)
                            {
                                return RedirectToAction("UpdateAsset");
                            }
                        }
                        else
                        {
                            if (assetViewModel.IsEditMode)
                            {
                                return RedirectToAction("UpdateAsset");
                            }
                            else
                            {
                                return RedirectToAction("AddAsset");
                            }
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Photo 1 is required");
                }
            }
            else
            {
                AddServerSideErrorToModelState();
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Add asset
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("AssetData", AuthorizationActionType.AllowAdd)]
        public ActionResult AddAsset()
        {
            FacadeResult<DataModel.Asset> result;
            DataModel.Asset asset = new DataModel.Asset();
            AssetViewModel assetViewModel = (AssetViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                AuctionSchedule schedule = null;
                if (assetViewModel.AuctionScheduleID.HasValue)
                {
                    if (assetViewModel.AuctionScheduleID == -1)
                    {
                        schedule = new AuctionSchedule();
                        schedule.ScheduleDate = assetViewModel.AuctionDate;
                        schedule.ScheduleTime = DateTime.Parse(assetViewModel.AuctionDate.Value.ToShortDateString() + " " + assetViewModel.AuctionTime);
                        schedule.KPKNLID = assetViewModel.KPKNLID;
                        //schedule.AuctionAddress = assetViewModel.KPKNLAddress;
                        schedule.AuctionAddress = assetViewModel.AuctionAddress;
                        schedule.Timezone = assetViewModel.TimeZoneID;
                        schedule.Phone1 = assetViewModel.KPKNLPhone1;
                        schedule.Phone2 = assetViewModel.KPKNLPhone2;
                    }
                    else
                    {
                        asset.AuctionScheduleID = assetViewModel.AuctionScheduleID;
                    }
                }

                asset.AssetID = assetViewModel.AssetID;
                asset.AssetCode = NumberingFacade.GenerateAssetCode(assetViewModel.SegmentID, assetViewModel.BranchID.HasValue ? assetViewModel.BranchID.Value : 0);
                asset.AccountNumber = assetViewModel.AccountNumber;
                asset.IsEmailAuction = assetViewModel.IsEmailAuction;
                asset.EmailAuctionURL = assetViewModel.EmailAuctionURL;
                asset.AccountName = assetViewModel.AccountName;
                asset.CategoryID = assetViewModel.CategoryID;
                asset.Address = assetViewModel.Address;
                asset.ProvinceID = assetViewModel.ProvinceID;
                asset.CityID = assetViewModel.CityID;
                asset.OldPrice = assetViewModel.OldPrice;
                asset.NewPrice = assetViewModel.NewPrice;
                asset.BranchID = assetViewModel.BranchID;

                if (schedule != null)
                {
                    asset.KPKNLID = schedule.KPKNLID;
                    //asset.KPKNLAddress = schedule.AuctionAddress;
                    asset.AuctionAddress = schedule.AuctionAddress;
                    asset.KPKNLPhone1 = schedule.Phone1;
                    asset.KPKNLPhone2 = schedule.Phone2;
                }
                else
                {
                    asset.KPKNLID = assetViewModel.KPKNLID;
                    asset.KPKNLAddress = assetViewModel.KPKNLAddress;
                    asset.AuctionAddress = assetViewModel.AuctionAddress;
                    asset.KPKNLPhone1 = assetViewModel.KPKNLPhone1;
                    asset.KPKNLPhone2 = assetViewModel.KPKNLPhone2;
                }

                asset.TransferInformation = assetViewModel.TransferInformation;
                asset.MarketValue = assetViewModel.MarketValue;
                asset.LiquidationValue = assetViewModel.LiquidationValue;
                asset.SecurityRightsValue = assetViewModel.SecurityRightsValue;
                asset.SoldValue = assetViewModel.SoldValue;
                asset.ValuationDate = assetViewModel.ValuationDate;
                asset.AuctionHallID = assetViewModel.AuctionHallID;
                asset.AuctionHallAddress = assetViewModel.AuctionHallAddress;
                asset.AuctionHallPhone1 = assetViewModel.AuctionHallPhone1;
                asset.AuctionHallPhone2 = assetViewModel.AuctionHallPhone2;
                asset.SegmentID = assetViewModel.SegmentID;
                asset.OwnershipDocumentTypeID = assetViewModel.OwnerShipDocumentTypeID;
                asset.DocumentNumber = assetViewModel.DocumentNumber;
                asset.Latitude = assetViewModel.Latitude;
                asset.Longitude = assetViewModel.Longitude;
                asset.AdditionalNotes = assetViewModel.AdditionalNotes;
                asset.Type = assetViewModel.Type;
                asset.Price = assetViewModel.Price;
                asset.ExpiredDate = assetViewModel.ExpiredDate;

                List<AssetCategoryTemplateValue> listCategoryTemplateValue = null;
                if (assetViewModel.CategoryTemplates != null)
                {
                    listCategoryTemplateValue = new List<AssetCategoryTemplateValue>();
                    foreach (AssetCategoryTemplateViewModel template in assetViewModel.CategoryTemplates)
                    {
                        AssetCategoryTemplateValue templateValue = new AssetCategoryTemplateValue();
                        templateValue.Value = template.Value;
                        templateValue.FieldName = template.FieldName;
                        listCategoryTemplateValue.Add(templateValue);
                    }
                }


                result = AssetFacade.AddNewAsset(asset, schedule, listCategoryTemplateValue, SecurityHelper.GetCurrentLoggedInUserID());

                if (result.IsSuccess)
                {
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "AssetData", "Asset");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Update asset
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [AuthorizeAction("AssetData", AuthorizationActionType.AllowUpdate)]
        public ActionResult UpdateAsset()
        {
            FacadeResult<DataModel.Asset> result;
            DataModel.Asset asset = new DataModel.Asset();
            AssetViewModel assetViewModel = (AssetViewModel)TempData["Model"];

            if (ModelState.IsValid)
            {
                AuctionSchedule schedule = null;
                if (assetViewModel.AuctionScheduleID.HasValue)
                {
                    if (assetViewModel.AuctionScheduleID == -1)
                    {
                        schedule = new AuctionSchedule();
                        schedule.ScheduleDate = assetViewModel.AuctionDate;
                        schedule.ScheduleTime = DateTime.Parse(assetViewModel.AuctionDate.Value.ToShortDateString() + " " + assetViewModel.AuctionTime);
                        schedule.KPKNLID = assetViewModel.KPKNLID;
                        //schedule.AuctionAddress = assetViewModel.KPKNLAddress;
                        schedule.AuctionAddress = assetViewModel.AuctionAddress;
                        schedule.Timezone = assetViewModel.TimeZoneID;
                        schedule.Phone1 = assetViewModel.KPKNLPhone1;
                        schedule.Phone2 = assetViewModel.KPKNLPhone2;
                    }
                    else
                    {
                        asset.AuctionScheduleID = assetViewModel.AuctionScheduleID;
                    }
                }

                asset.AssetID = assetViewModel.AssetID;
                asset.AssetCode = assetViewModel.AssetCode;
                asset.AccountNumber = assetViewModel.AccountNumber;
                asset.IsEmailAuction = assetViewModel.IsEmailAuction;
                asset.EmailAuctionURL = assetViewModel.EmailAuctionURL;
                asset.AccountName = assetViewModel.AccountName;
                asset.CategoryID = assetViewModel.CategoryID;
                asset.Address = assetViewModel.Address;
                asset.ProvinceID = assetViewModel.ProvinceID;
                asset.CityID = assetViewModel.CityID;
                asset.OldPrice = assetViewModel.OldPrice;
                asset.NewPrice = assetViewModel.NewPrice;
                asset.BranchID = assetViewModel.BranchID;
                asset.MarketValue = assetViewModel.MarketValue;
                asset.LiquidationValue = assetViewModel.LiquidationValue;
                asset.SecurityRightsValue = assetViewModel.SecurityRightsValue;
                asset.SoldValue = assetViewModel.SoldValue;
                asset.ValuationDate = assetViewModel.ValuationDate;
                asset.AuctionHallID = assetViewModel.AuctionHallID;
                asset.AuctionHallAddress = assetViewModel.AuctionHallAddress;
                asset.AuctionHallPhone1 = assetViewModel.AuctionHallPhone1;
                asset.AuctionHallPhone2 = assetViewModel.AuctionHallPhone2;
                asset.SegmentID = assetViewModel.SegmentID;
                asset.KPKNLID = assetViewModel.KPKNLID;
                asset.KPKNLAddress = assetViewModel.KPKNLAddress;
                asset.AuctionAddress = assetViewModel.AuctionAddress;
                asset.KPKNLPhone1 = assetViewModel.KPKNLPhone1;
                asset.KPKNLPhone2 = assetViewModel.KPKNLPhone2;
                asset.TransferInformation = assetViewModel.TransferInformation;
                asset.OwnershipDocumentTypeID = assetViewModel.OwnerShipDocumentTypeID;
                asset.DocumentNumber = assetViewModel.DocumentNumber;
                asset.Latitude = assetViewModel.Latitude;
                asset.Longitude = assetViewModel.Longitude;
                asset.AdditionalNotes = assetViewModel.AdditionalNotes;
                asset.CurrentStatusID = assetViewModel.CurrentStatusID;
                asset.Type = assetViewModel.Type;
                asset.Price = assetViewModel.Price;
                asset.ExpiredDate = assetViewModel.ExpiredDate;

                List<AssetCategoryTemplateValue> listCategoryTemplateValue = null;
                if (assetViewModel.CategoryTemplates != null)
                {
                    listCategoryTemplateValue = new List<AssetCategoryTemplateValue>();
                    foreach (AssetCategoryTemplateViewModel template in assetViewModel.CategoryTemplates)
                    {
                        AssetCategoryTemplateValue templateValue = new AssetCategoryTemplateValue();
                        templateValue.Value = template.Value;
                        templateValue.FieldName = template.FieldName;
                        listCategoryTemplateValue.Add(templateValue);
                    }
                }
                int approvalType = assetViewModel.ApprovalType.HasValue ? assetViewModel.ApprovalType.Value : 0;
                result = AssetFacade.UpdateAsset(asset, approvalType, schedule, listCategoryTemplateValue, SecurityHelper.GetCurrentLoggedInUserID(), assetViewModel.ButtonType);

                if (result.IsSuccess)
                {
                    if (assetViewModel.ButtonType == 1)
                    {
                        //Submit
                        return this.JavaScriptAlertAndRedirect(TextResources.SubmitSuccess_Message, "AssetData", "Asset");
                    }
                    else
                    {
                        return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "AssetData", "Asset");
                    }
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        [AuthorizeAction("ApproveAsset", AuthorizationActionType.AllowUpdate)]
        public ActionResult ApproveAsset()
        {
            FacadeResult<DataModel.Asset> result;
            DataModel.Asset asset = new DataModel.Asset();
            AssetViewModel assetViewModel = (AssetViewModel)TempData["Model"];

            asset = AssetFacade.GetActiveAssetByAssetID(assetViewModel.AssetID).ReturnData;

            AssetChanges changes = AssetFacade.GetActiveAssetChangeByAssetID(asset.AssetID.Value).ReturnData;
            if (changes != null)
            {
                if (changes.IsNew.HasValue ? changes.IsNew.Value : false)
                {
                    asset.CurrentStatusID = 4;
                }

                if (changes.IsUpdated.HasValue ? changes.IsUpdated.Value : false)
                {
                    asset.CurrentStatusID = changes.OriginalAssetStatusID;
                }

                if (changes.IsDeleted.HasValue ? changes.IsDeleted.Value : false)
                {
                    asset.CurrentStatusID = changes.OriginalAssetStatusID;
                    asset.IsActive = false;
                }

                changes.CurrentApprovalStatus = 1;
                changes.LastUpdatedBy = SecurityHelper.GetCurrentLoggedInUserID();
                changes.AssetID = asset.AssetID;
                changes.DateTimeUpdated = DateTime.Now;
            }

            AssetWorkflowHistory workflowHistory = new AssetWorkflowHistory();
            workflowHistory.ApprovedBy = SecurityHelper.GetCurrentLoggedInUserID().ToString();
            workflowHistory.IsActive = true;
            workflowHistory.IsApproved = true;
            workflowHistory.Reason = String.IsNullOrEmpty(assetViewModel.ApproveReason) ? String.Empty : assetViewModel.ApproveReason;
            workflowHistory.IsRejected = false;
            workflowHistory.RejectedBy = "";
            workflowHistory.DateTimeCreated = DateTime.Now;
            workflowHistory.CreatedBy = SecurityHelper.GetCurrentLoggedInUserID();
            result = AssetFacade.ApproveAsset(asset, changes, workflowHistory, SecurityHelper.GetCurrentLoggedInUserID());

            if (result.IsSuccess)
            {
                return this.JavaScriptAlertAndRedirect(TextResources.ApproveSuccess_Message, "TaskList", "Asset");
            }
            else
            {
                ModelState.AddModelError("", result.ErrorMessage);
            }

            return PartialView("_ValidationSummary");
        }

        [AuthorizeAction("RejectAsset", AuthorizationActionType.AllowUpdate)]
        public ActionResult RejectAsset()
        {

            FacadeResult<DataModel.Asset> result = new FacadeResult<DataModel.Asset>();
            DataModel.Asset asset = new DataModel.Asset();
            AssetViewModel assetViewModel = (AssetViewModel)TempData["Model"];
            try
            {
                IList<AssetRejectReason> listReason = new List<AssetRejectReason>();
                listReason = GetListRejectReasonFromViewModel(assetViewModel);

                asset = AssetFacade.GetActiveAssetByAssetID(assetViewModel.AssetID).ReturnData;

                AssetChanges changes = AssetFacade.GetActiveAssetChangeByAssetID(asset.AssetID.Value).ReturnData;
                if (changes != null)
                {
                    if (changes.IsNew.HasValue ? changes.IsNew.Value : false)
                    {
                        asset.CurrentStatusID = 1;
                    }
                    if (changes.IsUpdated.HasValue ? changes.IsUpdated.Value : false)
                    {
                        asset.CurrentStatusID = 9;
                    }
                    if (changes.IsDeleted.HasValue ? changes.IsDeleted.Value : false)
                    {
                        asset.CurrentStatusID = 10;
                    }

                    // changes.OriginalAssetStatusID = asset.CurrentStatusID;
                    changes.CurrentApprovalStatus = 2;
                    changes.LastUpdatedBy = SecurityHelper.GetCurrentLoggedInUserID();
                    changes.DateTimeUpdated = DateTime.Now;
                }

                AssetWorkflowHistory workflowHistory = new AssetWorkflowHistory();
                workflowHistory.ApprovedBy = "";
                workflowHistory.IsActive = true;
                workflowHistory.IsApproved = false;
                workflowHistory.Reason = assetViewModel.RejectReason == null ? String.Empty : assetViewModel.RejectReason;
                workflowHistory.IsRejected = true;
                workflowHistory.RejectedBy = SecurityHelper.GetCurrentLoggedInUserID().ToString();
                workflowHistory.DateTimeCreated = DateTime.Now;
                workflowHistory.CreatedBy = SecurityHelper.GetCurrentLoggedInUserID();

                result = AssetFacade.RejectAsset(asset, changes, workflowHistory, listReason, SecurityHelper.GetCurrentLoggedInUserID());
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }

            if (result.IsSuccess)
            {
                return this.JavaScriptAlertAndRedirect(TextResources.RejectSuccess_Message, "TaskList", "Asset");
            }
            else
            {
                ModelState.AddModelError("", result.ErrorMessage);
            }

            return PartialView("_ValidationSummary");
        }

        [HttpPost]
        [AuthorizeAction("AssetData", AuthorizationActionType.AllowDelete)]
        public ActionResult DeleteAsset(int assetID)
        {
            FacadeResult<DataModel.Asset> result = AssetFacade.DeleteAssetByID(assetID, SecurityHelper.GetCurrentLoggedInUserID());
            return Json(new { errorMessage = result.ErrorMessage });
        }

        [HttpPost]
        [AuthorizeAccess("AssetData")]
        public ActionResult CancelDelete(int? assetID)
        {

            if (assetID.HasValue)
            {
                DataModel.Asset asset = AssetFacade.GetActiveAssetByAssetID(assetID.Value).ReturnData;

                AssetChanges changes = AssetFacade.GetActiveAssetChangeByAssetID(assetID.Value).ReturnData;
                asset.CurrentStatusID = changes.OriginalAssetStatusID;
                asset.IsActive = true;
                changes.CurrentApprovalStatus = 3;
                changes.LastUpdatedBy = SecurityHelper.GetCurrentLoggedInUserID();
                changes.DateTimeUpdated = DateTime.Now;

                FacadeResult<DataModel.Asset> result = AssetFacade.CancelDelete(asset, changes, SecurityHelper.GetCurrentLoggedInUserID());
                return Json(new { errorMessage = result.ErrorMessage });
            }
            return Json(new { errorMessage = String.Empty });
        }

        #endregion

        #region View Asset Change History

        [ActionName("AssetChangeHistory")]
        public ActionResult ViewAssetChangeHistory(long? assetID)
        {
            AssetChangeViewModel model = new AssetChangeViewModel();
            if (assetID != null)
            {
                model.AssetID = assetID.Value;
            }
            return View("AssetChangeHistoryList", model);
        }

        [HttpPost]
        [ActionName("AssetChangeHistoryList")]
        [AuthorizeAccess("AssetData")]
        [ValidateSQLInjection]
        public ActionResult GetAssetChangeHistoryList(int? assetID, String keywords, int draw, int start, int length)
        {

            IList<dynamic> assets = AssetFacade.GetAllActiveAssetChangeHistoryByAssetID(assetID.HasValue ? assetID.Value : 0).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < assets.Count; i++)
            {
                data.Add(new String[] { 
                    assets[i].AssetID == null ? String.Empty : assets[i].AssetID.ToString() , 
                    assets[i].AssetCode, 
                    assets[i].IsNew ? "True" : "False",
                    assets[i].IsUpdated ? "True" : "False",
                    assets[i].IsDeleted ? "True" : "False",
                    assets[i].Date == null ? String.Empty : assets[i].Date.ToString("dd MMM yyyy"),
                    assets[i].UserAdmin,
                    assets[i].ApproveOrReject,
                    assets[i].ApproveOrRejectBy,
                    assets[i].ApproveOrRejectDate == null ? String.Empty : assets[i].Date.ToString("dd MMM yyyy"),
                    assets[i].ApproveReason
                });
            }
            return Json(new { draw = draw, recordsTotal = AssetFacade.GetAllActiveAssetChangeHistoryCount(assetID.HasValue ? assetID.Value : 0).ReturnData, recordsFiltered = AssetFacade.GetAllActiveAssetChangeHistoryByAssetIDAndKeywordsCount(assetID.HasValue ? assetID.Value : 0).ReturnData, data = data.ToArray() });
        }

        #endregion

        #region Task List
        [ActionName("TaskList")]
        [AuthorizeAccess("AssetData")]
        public ActionResult TaskListView(bool cancel = false)
        {
            ViewBag.Title = TextResources.AssetView_Title;
            TaskListViewModel model = new TaskListViewModel();

            return View("TaskList", model);
        }

        [HttpPost]
        [ActionName("TaskList")]
        [AuthorizeAccess("AssetData")]
        [ValidateSQLInjection]
        public ActionResult GetTaskList(String keywords, int draw, int start, int length)
        {

            // Get Data By User Group 
            String approver = ConfigurationManager.AppSettings["Approver_GroupID"].ToString();
            String inputer = ConfigurationManager.AppSettings["Inputer_GroupID"].ToString();

            int sortIndex = int.Parse(Request["order[0][column]"]);
            String sortDirection = Request["order[0][dir]"];
            IList<dynamic> assets = new List<dynamic>();
            List<String[]> data = new List<String[]>();

            long recordTotal = 0, recordFiltered = 0;

            int branchID = 0;
            DataModel.User usr = AdministrationFacade.GetActiveUserByUserID(SecurityHelper.GetCurrentLoggedInUserID()).ReturnData;
            if (User != null)
            {
                branchID = usr.BranchID.HasValue ? usr.BranchID.Value : 0;
            }

            int groupType = AdministrationFacade.GetGroupIDByUserID(SecurityHelper.GetCurrentLoggedInUserID(), Int32.Parse(approver), Int32.Parse(inputer)).ReturnData;
            if (groupType.ToString().Equals(approver))
            {
                assets = AssetFacade.GetActiveAssetChangesList(branchID, 0, keywords, start, length, sortIndex, sortDirection).ReturnData;
                recordTotal = AssetFacade.GetActiveAssetChangesCount(branchID).ReturnData;
                recordFiltered = AssetFacade.GetActiveAssetChangesCountByKeywords(branchID, 0, keywords).ReturnData;
            }
            else if (groupType.ToString().Equals(inputer))
            {
                assets = AssetFacade.GetActiveAssetChangesList(branchID, 2, keywords, start, length, sortIndex, sortDirection).ReturnData;
                recordTotal = AssetFacade.GetActiveAssetChangesForInputterCount().ReturnData;
                recordFiltered = AssetFacade.GetActiveAssetChangesCountByKeywords(branchID, 2, keywords).ReturnData;
            }
            else if (groupType == -1) // Both 
            {
                assets = AssetFacade.GetActiveAssetChangesForInputerAndApproverList(branchID, keywords, start, length, sortIndex, sortDirection).ReturnData;
                recordTotal = AssetFacade.GetActiveAssetChangesForInputterAndApproverCount().ReturnData;
                recordFiltered = AssetFacade.GetActiveAssetChangesForInputerAndApproverCountByKeywords(branchID, keywords).ReturnData;
            }

            for (int i = 0; i < assets.Count; i++)
            {
                String changeType = "";
                if (assets[i].IsNew) changeType = "Penambahan";
                if (assets[i].IsUpdated) changeType = "Perubahan";
                if (assets[i].IsDeleted) changeType = "Penghapusan";
                data.Add(new String[] { 
                    assets[i].AssetChangesID == null ? String.Empty : assets[i].AssetChangesID.ToString(),
                    assets[i].AssetID == null ? String.Empty : assets[i].AssetID.ToString(), 
                    assets[i].AssetCode == null ? String.Empty : assets[i].AssetCode.ToString(), 
                    assets[i].Segment == null ? String.Empty : assets[i].Segment,
                    assets[i].Category == null ? String.Empty : assets[i].Category,
                    assets[i].Province == null ? String.Empty : assets[i].Province,
                    assets[i].City == null ? String.Empty : assets[i].City,
                    assets[i].AdditionalNotes == null ? String.Empty : assets[i].AdditionalNotes,
                    assets[i].AssetID == null ? String.Empty + "#" + groupType : assets[i].AssetID.ToString() + "#" + groupType,
                    assets[i].AssetID == null ? String.Empty + "#" + groupType : assets[i].AssetID.ToString() + "#" + groupType,
                    assets[i].AssetID == null ? String.Empty + "#" + groupType : assets[i].AssetID.ToString() + "#" + groupType,
                    assets[i].AssetID == null ? String.Empty + "#" + groupType : assets[i].AssetID.ToString() + "#" + groupType,
                    assets[i].AssetID == null ? String.Empty + "#" + groupType : assets[i].AssetID.ToString() + "#" + groupType,                    
                    //assets[i].IsNew ? "Yes" : "No", 
                    //assets[i].IsUpdated ? "Yes" : "No",
                    //assets[i].IsDeleted ? "Yes" : "No", 
                    assets[i].CurrentStatusID == null? String.Empty: assets[i].CurrentStatusID.ToString(),
                    assets[i].CurrentApprovalStatus == null ? String.Empty : assets[i].CurrentApprovalStatus.ToString(),
                    changeType,
                    assets[i].AssetID == null ? String.Empty : assets[i].AssetID.ToString(),
                    assets[i].AssetID == null ? String.Empty : assets[i].AssetID.ToString(),
                    assets[i].AssetID == null ? String.Empty : assets[i].AssetID.ToString(),
                    assets[i].AssetID == null ? String.Empty : assets[i].AssetID.ToString(),
                    assets[i].AssetChangesID == null ? String.Empty : assets[i].AssetChangesID.ToString()
                });
            }

            return Json(new { draw = draw, recordsTotal = recordTotal, recordsFiltered = recordFiltered, data = data.ToArray() });

        }

        #endregion

        #region Asset Change History List
        [ActionName("AssetHistory")]
        public ActionResult ViewAssetHistoryList(long? assetChangesID)
        {
            AssetChangeHistoryViewModel model = new AssetChangeHistoryViewModel();
            if (assetChangesID != null)
            {
                model.AssetChangesID = assetChangesID.Value;
            }
            return View("AssetHistoryList", model);
        }

        [HttpPost]
        [ActionName("AssetHistoryList")]
        [AuthorizeAccess("AssetData")]
        [ValidateSQLInjection]
        public ActionResult GetAssetHistoryList(int? assetChangesID, int draw, int start, int length)
        {

            IList<dynamic> assets = AssetFacade.GetAllActiveAssetChangesHistoryValueByAssetChangesID(assetChangesID.HasValue ? assetChangesID.Value : 0).ReturnData;
            List<String[]> data = new List<String[]>();
            for (int i = 0; i < assets.Count; i++)
            {
                data.Add(new String[] { 
                        assets[i].LabelName, 
                        assets[i].OldValue,
                        assets[i].NewValue
                    });

                //if (assets[i].LabelName == "Status Agunan")
                //{

                //    // Uncomment Bellow if Asset Status Want To Include TO Show
                //    //int oldStatus = String.IsNullOrEmpty(assets[i].OldValue) ? 0 : Int32.Parse(assets[i].OldValue);
                //    //int newStatus = String.IsNullOrEmpty(assets[i].NewValue) ? 0 : Int32.Parse(assets[i].NewValue);
                //    //String newStr = AssetFacade.GetActiveAssetStatusByStatusID(newStatus).ReturnData;
                //    //String oldStr = AssetFacade.GetActiveAssetStatusByStatusID(oldStatus).ReturnData;
                //    //data.Add(new String[] { 
                //    //    assets[i].LabelName, 
                //    //    oldStr,
                //    //    newStr
                //    //});
                //}
                //else
                //{
                //    data.Add(new String[] { 
                //        assets[i].LabelName, 
                //        assets[i].OldValue,
                //        assets[i].NewValue
                //    });
                //}
            }
            return Json(new { draw = draw, recordsTotal = data.Count, recordsFiltered = data.Count, data = data.ToArray() });
        }

        #endregion

        #region Asset Photo
        /// <summary>
        /// Ajax upload photo1 file
        /// </summary>
        /// <param name="assetPhoto1">The photo1 file</param>
        /// <returns></returns>
        [ActionName("UploadPhoto1")]
        public ActionResult UploadPhoto1(HttpPostedFileBase assetPhoto1)
        {
            if (IsFileAllowed(assetPhoto1.FileName) && CheckImageSize(assetPhoto1.InputStream, 640, 480, false))
            {
                SessionHelper.Remove(SessionKey.AssetPhoto1);
                String path = ConfigurationManager.AppSettings["ImageUploadPath"].ToString();
                String fileName = "tmp_" + SecurityHelper.GetCurrentLoggedInUserID().ToString() + "_" + DateTime.Today.ToString("yyyyMMddhhmmss") + "_" + new FileInfo(assetPhoto1.FileName).Name;
                assetPhoto1.SaveAs(path + fileName);
                SessionHelper.SetValue<String>(SessionKey.AssetPhoto1, fileName);
                return Json(new { IsSuccess = true, FileName = fileName }, "text/plain");
            }
            else
            {
                return Json(new { IsSuccess = false, FileName = String.Empty }, "text/plain");
            }
        }

        /// <summary>
        /// Ajax upload photo2 file
        /// </summary>
        /// <param name="assetPhoto2">The photo file</param>
        /// <returns></returns>
        [ActionName("UploadPhoto2")]
        public ActionResult UploadPhoto2(HttpPostedFileBase assetPhoto2)
        {
            if (IsFileAllowed(assetPhoto2.FileName) && CheckImageSize(assetPhoto2.InputStream, 640, 480, false))
            {
                SessionHelper.Remove(SessionKey.AssetPhoto2);
                String path = ConfigurationManager.AppSettings["ImageUploadPath"].ToString();
                String fileName = "tmp_" + SecurityHelper.GetCurrentLoggedInUserID().ToString() + "_" + DateTime.Today.ToString("yyyyMMddhhmmss") + "_" + new FileInfo(assetPhoto2.FileName).Name;
                assetPhoto2.SaveAs(path + fileName);
                SessionHelper.SetValue<String>(SessionKey.AssetPhoto2, fileName);
                return Json(new { IsSuccess = true, FileName = fileName }, "text/plain");
            }
            else
            {
                return Json(new { IsSuccess = false, FileName = String.Empty }, "text/plain");
            }
        }

        /// <summary>
        /// Ajax upload photo3 file
        /// </summary>
        /// <param name="assetPhoto3">The photo file</param>
        /// <returns></returns>
        [ActionName("UploadPhoto3")]
        public ActionResult UploadPhoto3(HttpPostedFileBase assetPhoto3)
        {
            if (IsFileAllowed(assetPhoto3.FileName) && CheckImageSize(assetPhoto3.InputStream, 640, 480, false))
            {
                SessionHelper.Remove(SessionKey.AssetPhoto3);
                String path = ConfigurationManager.AppSettings["ImageUploadPath"].ToString();
                String fileName = "tmp_" + SecurityHelper.GetCurrentLoggedInUserID().ToString() + "_" + DateTime.Today.ToString("yyyyMMddhhmmss") + "_" + new FileInfo(assetPhoto3.FileName).Name;
                assetPhoto3.SaveAs(path + fileName);
                SessionHelper.SetValue<String>(SessionKey.AssetPhoto3, fileName);
                return Json(new { IsSuccess = true, FileName = fileName }, "text/plain");
            }
            else
            {
                return Json(new { IsSuccess = false, FileName = String.Empty }, "text/plain");
            }
        }

        /// <summary>
        /// Ajax upload photo4 file
        /// </summary>
        /// <param name="assetPhoto4">The photo file</param>
        /// <returns></returns>
        [ActionName("UploadPhoto4")]
        [AuthorizeAccess("AssetData")]
        public ActionResult UploadPhoto4(HttpPostedFileBase assetPhoto4)
        {
            if (IsFileAllowed(assetPhoto4.FileName) && CheckImageSize(assetPhoto4.InputStream, 640, 480, false))
            {
                SessionHelper.Remove(SessionKey.AssetPhoto4);
                String path = ConfigurationManager.AppSettings["ImageUploadPath"].ToString();
                String fileName = "tmp_" + SecurityHelper.GetCurrentLoggedInUserID().ToString() + "_" + DateTime.Today.ToString("yyyyMMddhhmmss") + "_" + new FileInfo(assetPhoto4.FileName).Name;
                assetPhoto4.SaveAs(path + fileName);
                SessionHelper.SetValue<String>(SessionKey.AssetPhoto4, fileName);
                return Json(new { IsSuccess = true, FileName = fileName }, "text/plain");
            }
            else
            {
                return Json(new { IsSuccess = false, FileName = String.Empty }, "text/plain");
            }
        }

        /// <summary>
        /// Ajax upload photo5 file
        /// </summary>
        /// <param name="assetPhoto5">The photo file</param>
        /// <returns></returns>
        [ActionName("UploadPhoto5")]
        [AuthorizeAccess("AssetData")]
        public ActionResult UploadPhoto5(HttpPostedFileBase assetPhoto5)
        {
            if (IsFileAllowed(assetPhoto5.FileName) && CheckImageSize(assetPhoto5.InputStream, 640, 480, false))
            {
                SessionHelper.Remove(SessionKey.AssetPhoto5);
                String path = ConfigurationManager.AppSettings["ImageUploadPath"].ToString();
                String fileName = "tmp_" + SecurityHelper.GetCurrentLoggedInUserID().ToString() + "_" + DateTime.Today.ToString("yyyyMMddhhmmss") + "_" + new FileInfo(assetPhoto5.FileName).Name;
                assetPhoto5.SaveAs(path + fileName);
                SessionHelper.SetValue<String>(SessionKey.AssetPhoto5, fileName);
                return Json(new { IsSuccess = true, FileName = fileName }, "text/plain");
            }
            else
            {
                return Json(new { IsSuccess = false, FileName = String.Empty }, "text/plain");
            }
        }

        #endregion

        #region Dropdown For Asset
        [HttpPost]
        [ActionName("AssetKPKNL")]
        [AuthorizeAccess("AssetData")]
        public ActionResult GetAssetKPKNLByID(int kpknlID)
        {

            if (kpknlID > 0)
            {
                KPKNL kpknl = MasterFacade.GetActiveKPKNLByKPKNLID(kpknlID).ReturnData;
                if (kpknl != null)
                {
                    return Json(kpknl);
                }
            }
            return Json(new KPKNL());
        }

        [HttpPost]
        [ActionName("AssetBalaiLelang")]
        [AuthorizeAccess("AssetData")]
        public ActionResult GetAssetBalaiLelangByID(int auctionHallID)
        {

            if (auctionHallID > 0)
            {
                AuctionHall auctionHall = MasterFacade.GetActiveAuctionHallByAuctionHallID(auctionHallID).ReturnData;
                if (auctionHall != null)
                {
                    if (String.IsNullOrEmpty(auctionHall.Address)) auctionHall.Address = String.Empty;
                    if (String.IsNullOrEmpty(auctionHall.Phone1)) auctionHall.Phone1 = String.Empty;
                    if (String.IsNullOrEmpty(auctionHall.Phone2)) auctionHall.Phone2 = String.Empty;
                    return Json(auctionHall);
                }
            }
            return Json(new AuctionHall());
        }


        [HttpPost]
        [ActionName("AssetAuctionSchedule")]
        [AuthorizeAccess("AssetData")]
        public ActionResult GetAuctionScheduleByID(int auctionScheduleID)
        {
            String[] data = null;
            if (auctionScheduleID > 0)
            {
                IList<dynamic> listScheduleItems = AssetFacade.GetAllActiveAuctionSchedulesByID(auctionScheduleID).ReturnData;
                if (listScheduleItems != null)
                {
                    for (int i = 0; i < listScheduleItems.Count; i++)
                    {
                        data = new String[]{
                                listScheduleItems[i].ScheduleDate == null ? String.Empty : listScheduleItems[i].ScheduleDate.ToString("dd/MM/yyyy"),
                                listScheduleItems[i].ScheduleTime == null ? String.Empty : listScheduleItems[i].ScheduleTime.ToString("HH:mm"),
                                listScheduleItems[i].TimezoneID.ToString() ,
                                listScheduleItems[i].KPKNLID.ToString(),
                                listScheduleItems[i].AuctionAddress == null ? String.Empty : listScheduleItems[i].AuctionAddress ,
                                listScheduleItems[i].Phone1,
                                listScheduleItems[i].Phone2};
                    }
                    return Json(data);
                }
            }
            return Json(new { String.Empty });
        }

        [HttpPost]
        [ActionName("AssetCategoryTemplates")]
        [AuthorizeAccess("AssetData")]
        public ActionResult GetCategoryTemplatesByCategoryID(int categoryID, long? assetID)
        {
            IList<AssetCategoryTemplateViewModel> result = new List<AssetCategoryTemplateViewModel>();
            if (categoryID > 0)
            {
                result = GetAssetCategoryTemplateViewModelListByAssetID(categoryID, assetID);
            }
            return Json(result);
        }
        #endregion

        #region Private Function For Asset Status
        /// <summary>
        /// Generate Dropdown for status asset 
        /// </summary>
        /// <param name="asset"></param>
        private static void SetAssetStatusAndAssetStatuses(AssetViewModel asset)
        {
            IList<AssetStatus> list = MasterFacade.GetAllActiveAssetStatuses().ReturnData;
            IList<AssetStatus> listToAdd = new List<AssetStatus>();
            switch (asset.CurrentStatusID)
            {
                case 1: // Draf
                    asset.CurrentStatusID = 1;
                    asset.AssetStatuses = MasterFacade.GetAllActiveAssetStatusesByStatusID(1).ReturnData.AsEnumerable();
                    break;
                case 2: // Pending
                    asset.CurrentStatusID = 2;
                    asset.AssetStatuses = MasterFacade.GetAllActiveAssetStatusesByStatusID(2).ReturnData.AsEnumerable();
                    break;
                case 4: // Live
                    foreach (AssetStatus status in list)
                    {
                        if (status.AssetStatusID == 4 || status.AssetStatusID == 5 || status.AssetStatusID == 6 || status.AssetStatusID == 7)
                        {
                            listToAdd.Add(status);
                        }
                    }
                    asset.AssetStatuses = listToAdd.AsEnumerable();
                    break;
                case 5: // Terjual
                    asset.CurrentStatusID = 5;
                    asset.AssetStatuses = MasterFacade.GetAllActiveAssetStatusesByStatusID(5).ReturnData.AsEnumerable();
                    break;
                case 6: // Ditarik
                    foreach (AssetStatus status in list)
                    {
                        if (status.AssetStatusID == 4 || status.AssetStatusID == 6)
                        {
                            listToAdd.Add(status);
                        }
                    }
                    asset.AssetStatuses = listToAdd.AsEnumerable();
                    break;
                case 7: // Lunas
                    asset.CurrentStatusID = 7;
                    asset.AssetStatuses = MasterFacade.GetAllActiveAssetStatusesByStatusID(7).ReturnData.AsEnumerable();
                    break;
                case 9: // Draft Update
                    foreach (AssetStatus status in list)
                    {
                        if (status.AssetStatusID == 4 || status.AssetStatusID == 5 || status.AssetStatusID == 6 || status.AssetStatusID == 7)
                        {
                            listToAdd.Add(status);
                        }
                    }
                    asset.AssetStatuses = listToAdd.AsEnumerable();
                    break;
                default:
                    asset.AssetStatuses = MasterFacade.GetAllActiveAssetStatusesByStatusID(asset.CurrentStatusID.Value).ReturnData.AsEnumerable();
                    break;
            }
        }

        /// <summary>
        /// Get asset category template view model for dropdown Category Template
        /// </summary>
        /// <param name="categoryID"></param>
        /// <param name="assetID"></param>
        /// <returns>List of asset category template view model</returns>
        private static IList<AssetCategoryTemplateViewModel> GetAssetCategoryTemplateViewModelListByAssetID(int categoryID, long? assetID)
        {
            IList<AssetCategoryTemplateViewModel> result = new List<AssetCategoryTemplateViewModel>();
            if (categoryID > 0)
            {
                IList<CategoryTemplate> categoryTemplates = MasterFacade.GetAllActiveCategoryTemplateByCategoryID(categoryID).ReturnData;
                if (categoryTemplates != null)
                {
                    foreach (CategoryTemplate template in categoryTemplates)
                    {
                        AssetCategoryTemplateViewModel data = new AssetCategoryTemplateViewModel();
                        data.FieldName = template.FieldName;
                        data.IsMandatory = template.IsMandatory.HasValue ? template.IsMandatory.Value : false;
                        if (assetID.HasValue)
                        {
                            AssetCategoryTemplateValue tempValue = MasterFacade.GetAllActiveCategoryTemplateValueByCategoryIDAndAssetID(assetID.Value, data.FieldName).ReturnData;
                            if (tempValue != null) data.Value = tempValue.Value;
                        }
                        result.Add(data);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statusID"></param>
        /// <returns></returns>
        private static int GetButtonTypeForStatus(int statusID)
        {
            int result = 0;
            if (statusID == 1) result = 2; // Jika draf tampilkan button "submit"
            return result;
        }

        /// <summary>
        /// Get the reason field
        /// </summary>
        /// <param name="assetViewModel"></param>
        /// <returns> list of reason field</returns>
        private static IList<AssetRejectReason> GetListRejectReasonFromViewModel(AssetViewModel assetViewModel)
        {
            IList<AssetRejectReason> result = new List<AssetRejectReason>();

            int user = SecurityHelper.GetCurrentLoggedInUserID();
            DateTime tglDibuat = DateTime.Now;

            // SegmentID
            if (!String.IsNullOrEmpty(assetViewModel.SegmentRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "SegmentID";
                reasonToAdd.Reason = assetViewModel.SegmentRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // CategoryID
            if (!String.IsNullOrEmpty(assetViewModel.CategoryRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "CategoryID";
                reasonToAdd.Reason = assetViewModel.CategoryRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // AccountNumber
            if (!String.IsNullOrEmpty(assetViewModel.AccountNumberRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "AccountNumber";
                reasonToAdd.Reason = assetViewModel.AccountNumberRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // Is Email Auction
            if (!String.IsNullOrEmpty(assetViewModel.IsEmailAuctionRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "IsEmailAuction";
                reasonToAdd.Reason = assetViewModel.IsEmailAuctionRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // Email Auction URL
            if (!String.IsNullOrEmpty(assetViewModel.EmailAuctionURLRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "EmailAuctionURL";
                reasonToAdd.Reason = assetViewModel.EmailAuctionURLRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // AccountName
            if (!String.IsNullOrEmpty(assetViewModel.AccountNameRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "AccountName";
                reasonToAdd.Reason = assetViewModel.AccountNameRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // ProvinceID
            if (!String.IsNullOrEmpty(assetViewModel.ProvinceRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "ProvinceID";
                reasonToAdd.Reason = assetViewModel.ProvinceRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }


            // CityID
            if (!String.IsNullOrEmpty(assetViewModel.CityNameRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "CityID";
                reasonToAdd.Reason = assetViewModel.CityNameRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // Latitude
            if (!String.IsNullOrEmpty(assetViewModel.LatitudeRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "Latitude";
                reasonToAdd.Reason = assetViewModel.LatitudeRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // Longitude
            if (!String.IsNullOrEmpty(assetViewModel.LongitudeRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "Longitude";
                reasonToAdd.Reason = assetViewModel.LongitudeRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // Address
            if (!String.IsNullOrEmpty(assetViewModel.AddressRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "Address";
                reasonToAdd.Reason = assetViewModel.AddressRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // OldPrice
            if (!String.IsNullOrEmpty(assetViewModel.OldPriceRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "OldPrice";
                reasonToAdd.Reason = assetViewModel.OldPriceRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // NewPrice
            if (!String.IsNullOrEmpty(assetViewModel.NewPriceRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "NewPrice";
                reasonToAdd.Reason = assetViewModel.NewPriceRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // MarketValue
            if (!String.IsNullOrEmpty(assetViewModel.MarketValueRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "MarketValue";
                reasonToAdd.Reason = assetViewModel.MarketValueRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }


            // LiquidationValue
            if (!String.IsNullOrEmpty(assetViewModel.LiquidationValueRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "LiquidationValue";
                reasonToAdd.Reason = assetViewModel.LiquidationValueRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // SecurityRightsValue
            if (!String.IsNullOrEmpty(assetViewModel.SecurityRightsValueRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "SecurityRightsValue";
                reasonToAdd.Reason = assetViewModel.SecurityRightsValueRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // SoldValue
            if (!String.IsNullOrEmpty(assetViewModel.SoldValueRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "SoldValue";
                reasonToAdd.Reason = assetViewModel.SoldValueRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // ValuationDate
            if (!String.IsNullOrEmpty(assetViewModel.ValuationDateRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "ValuationDate";
                reasonToAdd.Reason = assetViewModel.ValuationDateRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // BranchID
            if (!String.IsNullOrEmpty(assetViewModel.BranchRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "BranchID";
                reasonToAdd.Reason = assetViewModel.BranchRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }


            // AuctionScheduleID
            if (!String.IsNullOrEmpty(assetViewModel.AuctionScheduleRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "AuctionScheduleID";
                reasonToAdd.Reason = assetViewModel.AuctionScheduleRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // AuctionDate
            if (!String.IsNullOrEmpty(assetViewModel.AuctionDateRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "AuctionDate";
                reasonToAdd.Reason = assetViewModel.AuctionDateRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // AuctionTime
            if (!String.IsNullOrEmpty(assetViewModel.AuctionTimeRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "AuctionTime";
                reasonToAdd.Reason = assetViewModel.AuctionTimeRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // Timezone
            if (!String.IsNullOrEmpty(assetViewModel.TimezoneRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "Timezone";
                reasonToAdd.Reason = assetViewModel.TimezoneRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // TransferInformation
            if (!String.IsNullOrEmpty(assetViewModel.TransferInformationRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "TransferInformation";
                reasonToAdd.Reason = assetViewModel.TransferInformationRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // KPKNLID
            if (!String.IsNullOrEmpty(assetViewModel.KPKNLRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "KPKNLID";
                reasonToAdd.Reason = assetViewModel.KPKNLRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // KPKNLAddress
            if (!String.IsNullOrEmpty(assetViewModel.KPKNLAddressRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "KPKNLAddress";
                reasonToAdd.Reason = assetViewModel.KPKNLAddressRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // AuctionAddress
            if (!String.IsNullOrEmpty(assetViewModel.AuctionAddressRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "AuctionAddress";
                reasonToAdd.Reason = assetViewModel.AuctionAddressRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // KPKNLPhone1
            if (!String.IsNullOrEmpty(assetViewModel.KPKNLPhone1RejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "KPKNLPhone1";
                reasonToAdd.Reason = assetViewModel.KPKNLPhone1RejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // KPKNLPhone2
            if (!String.IsNullOrEmpty(assetViewModel.KPKNLPhone2RejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "KPKNLPhone2";
                reasonToAdd.Reason = assetViewModel.KPKNLPhone2RejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // AuctionHallID
            if (!String.IsNullOrEmpty(assetViewModel.AuctionHallRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "AuctionHallID";
                reasonToAdd.Reason = assetViewModel.AuctionHallRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // AuctionHallAddress
            if (!String.IsNullOrEmpty(assetViewModel.AuctionHallAddressRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "AuctionHallAddress";
                reasonToAdd.Reason = assetViewModel.AuctionHallAddressRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // AuctionHallPhone1
            if (!String.IsNullOrEmpty(assetViewModel.AuctionHallPhone1RejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "AuctionHallPhone1";
                reasonToAdd.Reason = assetViewModel.AuctionHallPhone1RejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // AuctionHallPhone2
            if (!String.IsNullOrEmpty(assetViewModel.AuctionHallPhone2RejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "AuctionHallPhone2";
                reasonToAdd.Reason = assetViewModel.AuctionHallPhone2RejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // Photo1
            if (!String.IsNullOrEmpty(assetViewModel.Photo1RejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "Photo1";
                reasonToAdd.Reason = assetViewModel.Photo1RejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // Photo2
            if (!String.IsNullOrEmpty(assetViewModel.Photo2RejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "Photo2";
                reasonToAdd.Reason = assetViewModel.Photo2RejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // Photo3
            if (!String.IsNullOrEmpty(assetViewModel.Photo3RejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "Photo3";
                reasonToAdd.Reason = assetViewModel.Photo3RejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // Photo4
            if (!String.IsNullOrEmpty(assetViewModel.Photo4RejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "Photo4";
                reasonToAdd.Reason = assetViewModel.Photo4RejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // Photo5
            if (!String.IsNullOrEmpty(assetViewModel.Photo5RejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "Photo5";
                reasonToAdd.Reason = assetViewModel.Photo5RejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // OwnershipDocumentTypeID
            if (!String.IsNullOrEmpty(assetViewModel.OwnershipDocumentTypeRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "OwnershipDocumentTypeID";
                reasonToAdd.Reason = assetViewModel.OwnershipDocumentTypeRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // DocumentNumber
            if (!String.IsNullOrEmpty(assetViewModel.DocumentNumberRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "DocumentNumber";
                reasonToAdd.Reason = assetViewModel.DocumentNumberRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // AdditionalNote
            if (!String.IsNullOrEmpty(assetViewModel.AdditionalNoteRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "AdditionalNote";
                reasonToAdd.Reason = assetViewModel.AdditionalNoteRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // Type
            if (!String.IsNullOrEmpty(assetViewModel.TypeRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "Type";
                reasonToAdd.Reason = assetViewModel.TypeRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // Price
            if (!String.IsNullOrEmpty(assetViewModel.PriceRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "Price";
                reasonToAdd.Reason = assetViewModel.PriceRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            // ExpiredDate
            if (!String.IsNullOrEmpty(assetViewModel.ExpiredDateRejectReason))
            {
                AssetRejectReason reasonToAdd = new AssetRejectReason();
                reasonToAdd.FieldOrColumnName = "ExpiredDate";
                reasonToAdd.Reason = assetViewModel.ExpiredDateRejectReason;
                reasonToAdd.IsActive = true;
                reasonToAdd.CreatedBy = user;
                reasonToAdd.DateTimeCreated = tglDibuat;
                result.Add(reasonToAdd);
            }

            IList<AssetCategoryTemplateViewModel> listTemplates = assetViewModel.CategoryTemplateReasons;
            if (listTemplates != null)
            {
                foreach (AssetCategoryTemplateViewModel template in listTemplates)
                {
                    if (!String.IsNullOrEmpty(template.Value))
                    {
                        AssetRejectReason reasonToAdd = new AssetRejectReason();
                        reasonToAdd.FieldOrColumnName = template.FieldName;
                        reasonToAdd.Reason = template.Value;
                        reasonToAdd.IsActive = true;
                        reasonToAdd.CreatedBy = user;
                        reasonToAdd.DateTimeCreated = tglDibuat;
                        result.Add(reasonToAdd);
                    }
                }
            }

            return result;
        }
        #endregion

    }
}
