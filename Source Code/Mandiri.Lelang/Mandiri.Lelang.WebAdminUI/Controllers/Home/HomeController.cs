﻿using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mandiri.Lelang.WebAdminUI.Controllers.Home
{
    /// <summary>
    /// The home controller
    /// </summary>
    [Authorize]
    public class HomeController : BaseController
    {
        #region Private Methods

        private void BindTaskAlerts(HomeViewModel homeViewModel)
        {
            int flag = AdministrationFacade.GetGroupIDByUserID(SecurityHelper.GetCurrentLoggedInUserID(), int.Parse(ConfigurationManager.AppSettings["Approver_GroupID"]), int.Parse(ConfigurationManager.AppSettings["Inputer_GroupID"])).ReturnData;
            bool isApprover = false;
            bool isInputter = false;

            if (flag != 0)
            {
                if (flag == -1)
                {
                    isApprover = true;
                    isInputter = true;
                }
                else if (flag == int.Parse(ConfigurationManager.AppSettings["Approver_GroupID"]))
                {
                    isApprover = true;
                    isInputter = false;
                }
                else if (flag == int.Parse(ConfigurationManager.AppSettings["Inputer_GroupID"]))
                {
                    isApprover = false;
                    isInputter = true;
                }
            }
            else
            {
                isApprover = false;
                isInputter = false;
            }

            if (homeViewModel.Alerts == null)
            {
                homeViewModel.Alerts = new List<HomeAlertViewModel>();
            }

            if (isInputter)
            {
                IList<dynamic> inputterTaskLists = AssetFacade.GetTaskListCountForInputter(SecurityHelper.GetCurrentLoggedInUserID()).ReturnData;
                String message = String.Empty;
                foreach (dynamic task in inputterTaskLists)
                {
                    if (task.ApprovalType == "Baru")
                    {
                        message = String.Format("Anda memiliki {0} tugas untuk Revisi Persetujuan Agunan Baru", task.Total);
                    }
                    else if (task.ApprovalType == "Ubah")
                    {
                        message = String.Format("Anda memiliki {0} tugas untuk Revisi Persetujuan Perubahan Agunan", task.Total);
                    }
                    else if (task.ApprovalType == "Hapus")
                    {
                        message = String.Format("Anda memiliki {0} tugas untuk Revisi Persetujuan Penghapusan Agunan", task.Total);
                    }

                    homeViewModel.Alerts.Add(new HomeAlertViewModel()
                    {
                        AlertMessage = message,
                        IsClickable = true,
                        ActionName = "TaskList",
                        ControllerName = "Asset",
                        LinkMessage = " Klik Disini.",
                        RouteValues = new { }
                    });
                }

                long inputterUpdatingTaskLists = AssetFacade.GetTaskListCountForUpdatingNewPrice(SecurityHelper.GetCurrentLoggedInUserID()).ReturnData;
                if (inputterUpdatingTaskLists > 0)
                {
                    homeViewModel.Alerts.Add(new HomeAlertViewModel()
                    {
                        AlertMessage = String.Format("Anda memiliki {0} tugas untuk Revisi Limit Agunan", inputterUpdatingTaskLists),
                        IsClickable = true,
                        ActionName = "AssetData",
                        ControllerName = "Asset",
                        LinkMessage = " Klik Disini.",
                        RouteValues = new { updatePrice = 1 }
                    });
                }

                long inputterUpdatingMessage = AdministrationFacade.GetUnCompletedMessageCountbyUserBranchID(SecurityHelper.GetCurrentLoggedInUserID()).ReturnData;
                if (inputterUpdatingMessage> 0)
                {
                    homeViewModel.Alerts.Add(new HomeAlertViewModel()
                    {
                        AlertMessage = String.Format("Anda memiliki {0} tugas untuk Follow-Up Pesan", inputterUpdatingMessage),
                        IsClickable = true,
                        ActionName = "Message",
                        ControllerName = "Administration",
                        LinkMessage = " Klik Disini.",
                        RouteValues = new { isPending = 1 }
                    });
                }

                long inputterTaskExpiredDate = AdministrationFacade.GetExpiredDateCountbyUserBranchID(SecurityHelper.GetCurrentLoggedInUserID()).ReturnData;
                if (inputterTaskExpiredDate > 0)
                {
                    homeViewModel.Alerts.Add(new HomeAlertViewModel()
                    {
                        AlertMessage = String.Format("Anda memiliki {0} tugas untuk Revisi Expired Date Jual Sukarela", inputterTaskExpiredDate),
                        IsClickable = true,
                        ActionName = "AssetData",
                        ControllerName = "Asset",
                        LinkMessage = " Klik Disini.",
                        RouteValues = new { }
                    });
                }
            }

            if (isApprover)
            {
                IList<dynamic> approverTaskLists = AssetFacade.GetTaskListCountForApprover().ReturnData;
                String message = String.Empty;
                foreach (dynamic task in approverTaskLists)
                {
                    if (task.ApprovalType == "Baru")
                    {
                        message = String.Format("Anda memiliki {0} tugas untuk Persetujuan Agunan Baru", task.Total);
                    }
                    else if (task.ApprovalType == "Ubah")
                    {
                        message = String.Format("Anda memiliki {0} tugas untuk Persetujuan Perubahan Agunan", task.Total);
                    }
                    else if (task.ApprovalType == "Hapus")
                    {
                        message = String.Format("Anda memiliki {0} tugas untuk Persetujuan Penghapusan Agunan", task.Total);
                    }

                    homeViewModel.Alerts.Add(new HomeAlertViewModel()
                    {
                        AlertMessage = message,
                        IsClickable = true,
                        ActionName = "TaskList",
                        ControllerName = "Asset",
                        LinkMessage = " Klik Disini.",
                        RouteValues = new { }
                    });
                }
            }
        }

        #endregion

        /// <summary>
        /// The home page
        /// </summary>
        /// <returns>The home or index view</returns>
        public ActionResult Index()
        {
            ViewBag.Title = TextResources.HomeView_Title;
            HomeViewModel homeViewModel = new HomeViewModel();
            BindTaskAlerts(homeViewModel);
            return View(homeViewModel);
        }
    }
}
