﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.Helper.Enum;
using Mandiri.Lelang.Helper.Extension;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.WebAdmin;
using Mandiri.Lelang.BusinessFacade;

namespace Mandiri.Lelang.WebAdminUI.Controllers.User
{
    public class UserController : BaseController
    {
        #region Action

        #region Login Logout

        /// <summary>
        /// A controller to return the default view for login
        /// </summary>
        /// <param name="returnUrl">The url to redirect to after a successful login</param>
        /// <returns>The login view</returns>
        [AllowAnonymous]
        [ActionName("Login")]
        public ActionResult LoginView(String returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            ViewBag.Title = TextResources.LoginView_Title;
            return View();
        }

        /// <summary>
        /// The login action
        /// </summary>
        /// <param name="model">The login view model</param>
        /// <param name="returnUrl">The url to redirect to after a successful login</param>
        /// <returns>The error partial view or redirect to returnUrl page</returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [ActionName("Login")]
        public ActionResult DoLogin(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                FacadeResult<DataModel.User> result = UserFacade.DoLogin(model.Username, model.Password, HttpContext.Session.SessionID);
                if (result.IsSuccess)
                {
                    FormsAuthentication.SetAuthCookie(result.ReturnData.UserID.ToString(), false);
                    return this.JavaScriptRedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        /// <summary>
        /// Do logout
        /// </summary>
        /// <returns>The login view</returns>
        [ActionName("Logout")]
        public ActionResult DoLogout()
        {
            UserFacade.DoLogout(SecurityHelper.GetCurrentLoggedInUserID(), HttpContext.Session.SessionID);
            FormsAuthentication.SignOut();
            SessionHelper.Clear();
            Session.Abandon();
            /*try
            {
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            }
            catch
            {
            }*/
            return RedirectToAction("Login", "User");
        }

        #endregion

        #region Change Password

        /// <summary>
        /// A controller to return the default view for change password
        /// </summary>
        /// <returns>The change password view</returns>
        [ActionName("ChangePassword")]
        public ActionResult ChangePasswordView()
        {
            ViewBag.Title = TextResources.ChangePasswordView_Title;
            ChangePasswordViewModel model = new ChangePasswordViewModel();
            model.UserID = SecurityHelper.GetCurrentLoggedInUserID();
            DataModel.User user = UserFacade.GetActiveUserByUserID(model.UserID).ReturnData;
            model.FullName = user.FullName;
            model.Username = user.Username;
            return View(model);
        }

        /// <summary>
        /// Change password
        /// </summary>
        /// <returns>The error partial view or redirect to current view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("ChangePassword")]
        public ActionResult ChangePassword(ChangePasswordViewModel changePasswordViewModel)
        {
            if (ModelState.IsValid)
            {
                FacadeResult<DataModel.User> result;
                result = UserFacade.ChangePassword(SecurityHelper.GetCurrentLoggedInUserID(), changePasswordViewModel.Password);

                if (result.IsSuccess)
                {
                    //return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "ChangePassword", "User");
                    return this.JavaScriptAlertAndRedirect(TextResources.SaveOrAddSuccess_Message, "Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", result.ErrorMessage);
                }
            }
            return PartialView("_ValidationSummary");
        }

        #endregion

        #endregion
    }
}