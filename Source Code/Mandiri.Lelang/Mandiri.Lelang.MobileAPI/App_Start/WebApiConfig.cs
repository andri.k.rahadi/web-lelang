﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace Mandiri.Lelang.MobileAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //For Development
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                //routeTemplate: "api/{version}/{controller}/{action}/{id}",
                routeTemplate: "{version}/{controller}/{action}/{id}",
                defaults: new { version = "v3", id = RouteParameter.Optional }
            );
            //For Production
            /*config.Routes.MapHttpRoute(
                name: "DefaultApi",
                //routeTemplate: "api/{version}/{controller}/{action}/{id}",
                routeTemplate: "{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );*/

            // Change formatter to JSON instead of XML            
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            config.Formatters.Add(new FormUrlEncodedMediaTypeFormatter());
            config.Formatters.Add(new JQueryMvcFormUrlEncodedFormatter());


            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            config.EnableSystemDiagnosticsTracing();
        }
    }
}
