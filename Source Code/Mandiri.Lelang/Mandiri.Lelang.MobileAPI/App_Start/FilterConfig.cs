﻿using System.Web;
using System.Web.Mvc;
using Mandiri.Lelang.Attribute.ActionFilter;

namespace Mandiri.Lelang.MobileAPI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}