﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace Mandiri.Lelang.MobileAPI.Controllers
{
    public class BaseController : ApiController
    {
        public const String ERROR_MESSAGE = "System Error or Exception Occurred";
        public const String INVALID_TOKEN_ERROR_MESSAGE = "Access Denied or Invalid Token";

        [ApiExplorerSettings(IgnoreApi = true)]
        public long? GetCurrentMemberID()
        {
            try
            {
                if (Request.Properties != null && Request.Properties.Count() > 0 && Request.Properties.ContainsKey("Payload"))
                {
                    var payload = (IDictionary<String, Object>)Request.Properties["Payload"];
                    if (payload.ContainsKey("memberID") && payload["memberID"] != null)
                    {
                        return long.Parse(payload["memberID"].ToString());
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public String GetJWTVerificationMessage()
        {
            try
            {
                if (Request.Properties != null && Request.Properties.Count() > 0 && Request.Properties.ContainsKey("VerificationMessage") && Request.Properties["VerificationMessage"] != null && !String.IsNullOrWhiteSpace(Request.Properties["VerificationMessage"].ToString()))
                {
                    var message = Request.Properties["VerificationMessage"].ToString();
                    if (!String.IsNullOrWhiteSpace(message))
                    {
                        return message;
                    }
                    else
                    {
                        return String.Empty;
                    }
                }
                else
                {
                    return String.Empty;
                }
            }
            catch
            {
                return String.Empty;
            }
        }
    }
}