﻿using Mandiri.Lelang.Attribute.ActionFilter;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Mandiri.Lelang.MobileAPI.Controllers
{
    public class MasterController : BaseController
    {
        /// <summary>
        /// GET /master/data
        /// Ada penambahan response WebContent, dan SystemSetting.
        /// Ada return field token (isinya string) -> Kalau terisi maka token-nya perlu di update/save ke local.
        /// Ada return field isTokenExpired (isinya 1 atau 0) -> 1 berarti sudah expired dan harus di logout, di redirect ke screen popup.
        /// </summary>
        /// <param name="lastTimestamp"></param>
        /// <returns></returns>        
        [HttpGet]
        [ValidateJWTToken(checkForExpiry: false)]
        [ActionName("data")]
        public dynamic GetAllMasterData(String lastTimestamp = "1900-01-01")
        {
            //TODO: Enable check for expiry
            List<dynamic> masters = new List<dynamic>();
            List<dynamic> data;
            dynamic jsonResult = new ExpandoObject();
            dynamic master;
            dynamic resultCode = new ExpandoObject();
            IList<dynamic> results;

            try
            {
                results = MasterFacade.GetAllMasterData(lastTimestamp).ReturnData;

                dynamic distinctTypes = results.Select(m => m.Type).Distinct().ToList();

                foreach (dynamic distinctType in distinctTypes)
                {
                    dynamic dataPerTypes = results.Where(m => m.Type == distinctType).ToList();
                    master = new ExpandoObject();
                    master.masterDataType = distinctType;

                    data = new List<dynamic>();
                    foreach (dynamic dataPerType in dataPerTypes)
                    {
                        dynamic d = new ExpandoObject();
                        if (dataPerType.Type == "Category" || dataPerType.Type == "Province" || dataPerType.Type == "ContactUsType" || dataPerType.Type == "DocumentType" || dataPerType.Type == "Branch" || dataPerType.Type == "WebContent" || dataPerType.Type == "SystemSetting" || dataPerType.Type == "KPKNL")
                        {
                            d.id = dataPerType.ID;
                            d.name = dataPerType.Name;
                            d.description = dataPerType.Description;
                            d.isActive = ((bool)dataPerType.IsActive) ? 1 : 0;
                            d.flag = dataPerType.Flag;
                        }
                        else if (dataPerType.Type == "MinMaxPrice")
                        {
                            d.id = dataPerType.ID;
                            d.name = dataPerType.Name;
                            d.description = dataPerType.Description;
                            d.value = dataPerType.Value;
                            d.isActive = ((bool)dataPerType.IsActive) ? 1 : 0;
                            d.flag = dataPerType.Flag;
                        }
                        else if (dataPerType.Type == "City")
                        {
                            d.id = dataPerType.ID;
                            d.provinceID = dataPerType.provinceID;
                            d.name = dataPerType.Name;
                            d.description = dataPerType.Description;
                            d.isActive = ((bool)dataPerType.IsActive) ? 1 : 0;
                            d.flag = dataPerType.Flag;
                        }
                        data.Add(d);
                    }
                    master.data = data;
                    masters.Add(master);
                }

                resultCode.code = 0;
                resultCode.message = "Success";
            }
            catch
            {
                resultCode.code = 1;
                resultCode.message = ERROR_MESSAGE;
            }

            jsonResult.masterData = masters;
            jsonResult.lastTimestamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
            if (Request.Properties != null && Request.Properties.Count() > 0 && Request.Properties.ContainsKey("NewToken") && Request.Properties["NewToken"] != null && !String.IsNullOrWhiteSpace(Request.Properties["NewToken"].ToString()))
            {
                jsonResult.token = Request.Properties["NewToken"].ToString();
            }
            else
            {
                jsonResult.token = "";
            }
            if (!String.IsNullOrWhiteSpace(GetJWTVerificationMessage()))
            {
                jsonResult.token = "";
                jsonResult.isTokenExpired = 1;
            }
            else
            {
                jsonResult.isTokenExpired = 0;
            }
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }
    }
}