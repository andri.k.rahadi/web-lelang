﻿using Mandiri.Lelang.Attribute.ActionFilter;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Mandiri.Lelang.MobileAPI.Controllers
{
    [ValidateJWTToken]
    public class VideoController : BaseController
    {
        // GET api/video/all        
        [HttpGet]
        [ActionName("all")]
        public dynamic GetAllVideos()
        {
            dynamic jsonResult = new ExpandoObject();
            dynamic data = new ExpandoObject();
            dynamic resultCode = new ExpandoObject();
            List<dynamic> dummy = new List<dynamic>();
            List<dynamic> allVideos = new List<dynamic>();

            try
            {
                var videos = MasterFacade.GetAllActiveVideos().ReturnData;
                foreach (var video in videos)
                {
                    allVideos.Add(new { title = video.Title, description = video.Description, videoId = video.VideoURL.Substring(video.VideoURL.IndexOf("v=") + 2, video.VideoURL.Length - (video.VideoURL.IndexOf("v=") + 2)), thumbnail = String.Format("{0}/image/video-thumbnail/{1}", ConfigurationManager.AppSettings["RootPath"], video.VideoID) });
                }
                data = allVideos;                
                /*dummy.Add(new { title = "mandiri online", description = "Video Nomor 1", videoId = "qkIC9vmDRq8", thumbnail = "http://www.bankmandiri.co.id/video-images/Mandiri-Online-Thumbnail.jpg" });
                dummy.Add(new { title = "mandiri debit online", description = "Video Nomor 2", videoId = "iICGnAhfMDI", thumbnail = "http://www.bankmandiri.co.id/video-images/thumbnail-debit.jpg" });
                dummy.Add(new { title = "mandiri jakarta marathon", description = "Video Nomor 3", videoId = "ByYR2Yi_Cko", thumbnail = "http://www.bankmandiri.co.id/video-images/thumb-gorun.jpg" });
                data = dummy;*/                
                
                resultCode.code = 0;
                resultCode.message = "Success";
            }
            catch
            {
                resultCode.code = 1;
                resultCode.message = ERROR_MESSAGE;
            }

            jsonResult.data = data;
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }
    }
}