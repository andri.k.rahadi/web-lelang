﻿using Mandiri.Lelang.Attribute.ActionFilter;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.Web;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;

namespace Mandiri.Lelang.MobileAPI.Controllers
{
    public class MemberController : BaseController
    {
        public class AssetIDParam
        {
            public long? AssetID { get; set; }
        }

        public class EmailParam
        {
            public String Email { get; set; }
        }

        public class LoginParam
        {
            public String Email { get; set; }
            public String Password { get; set; }
        }

        public class FBCallbackParam
        {
            public String Email { get; set; }
            public String FullName { get; set; }
            public String Token { get; set; }
            public String URL { get; set; }
            public String ID { get; set; }
        }

        /// <summary>
        /// POST /member/register  
        /// PARAMETERS JSON.
        ///    {
        ///        "profile": {
        ///            "fullName": "Ricardo",
        ///            "email": "ricardo@gmail.com",
        ///            "placeOfBirth": "Jakarta",
        ///            "dateOfBirth": "2016-11-01",
        ///            "mobilePhoneNumber": "",
        ///            "address": "",
        ///            "provinceID": 1,
        ///            "cityID": 1,
        ///            "identificationNumber": "",
        ///            "password": ""
        ///        },
        ///        "preference": {
        ///            "minPriceID": 1,
        ///            "maxPriceID": 3,
        ///            ""categoryIDs"": [
        ///                1,
        ///                2
        ///            ],
        ///            ""cityIDs"": [
        ///                1,
        ///                2
        ///            ]
        ///        }
        ///    }
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ActionName("register")]
        public dynamic Register()
        {
            //TODO: Add Validation (Follow Validator in Website)
            //TODO: Check for Mandatory Fields
            dynamic data = new ExpandoObject();
            dynamic jsonResult = new ExpandoObject();
            dynamic resultCode = new ExpandoObject();
            MemberPreference memberPreference = new MemberPreference();
            IList<MemberCategoryPreference> categoryPreferences = null;
            IList<MemberCityPreference> cityPreferences = null;
            dynamic param = new ExpandoObject();
            /*if (!String.IsNullOrWhiteSpace(text))
            {
                param = JsonConvert.DeserializeObject<dynamic>(text);
            }
            else
            {*/
            HttpContent requestContent = Request.Content;
            string jsonContent = requestContent.ReadAsStringAsync().Result;
            param = JsonConvert.DeserializeObject<dynamic>(jsonContent);
            //}

            if (!String.IsNullOrWhiteSpace(param.profile.fullName.Value) && !String.IsNullOrWhiteSpace(param.profile.email.Value) && !String.IsNullOrWhiteSpace(param.profile.password.Value))
            {
                /*return Json.Decode(@"
                {
                    ""data"": {
                        ""token"": ""eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtZW1iZXJJRCI6MSwiZnVsbE5hbWUiOiJSaWNhcmRvIEFsZXhhbmRlciIsImVtYWlsIjoicmljYXJkb2FsZXhhbmRlcmhAZ21haWwuY29tIiwic2FsdCI6IjdvcUtQOHFqSUE9PSIsImV4cCI6MTQ4MjU3MTQ5NH0.CDXEYwZ3bIjGsYgt29u157BzaOP5beH92nddiNRdeK4"",
                        ""memberID"": 1,
                        ""fullName"": ""Ricardo Alexander"",
                        ""email"": ""ricardoalexanderh@gmail.com""
                    },
                    ""resultCode"": {
                        ""code"": 0,
                        ""message"": ""Success""
                    }
                }");*/
                String token;
                Member member = new Member();
                member.Email = param.profile.email;
                member.FullName = param.profile.fullName;
                member.Address = param.profile.address;
                member.CityID = param.profile.cityID;
                member.ProvinceID = param.profile.provinceID;
                if (!String.IsNullOrWhiteSpace(param.profile.dateOfBirth.Value))
                {
                    member.DateOfBirth = DateTime.ParseExact(param.profile.dateOfBirth.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                }
                member.PlaceOfBirth = param.profile.placeOfBirth;
                member.IdentificationNumber = param.profile.identificationNumber;
                member.MobilePhoneNumber = param.profile.mobilePhoneNumber;

                if (param.preference != null && (param.preference.minPriceID.Value != null || param.preference.maxPriceID.Value != null || (param.preference.categoryIDs != null && param.preference.categoryIDs.Count > 0) || (param.preference.cityIDs != null && param.preference.cityIDs.Count > 0)))
                {
                    if (param.preference.minPriceID.Value != null)
                    {
                        memberPreference.MinPriceID = param.preference.minPriceID;
                    }
                    if (param.preference.maxPriceID.Value != null)
                    {
                        memberPreference.MaxPriceID = param.preference.maxPriceID;
                    }

                    if (param.preference.categoryIDs != null && param.preference.categoryIDs.Count > 0)
                    {
                        categoryPreferences = new List<MemberCategoryPreference>();
                        MemberCategoryPreference categoryPreference;
                        foreach (var preferenceCategory in param.preference.categoryIDs)
                        {
                            categoryPreference = new MemberCategoryPreference();
                            categoryPreference.CategoryID = preferenceCategory;
                            categoryPreferences.Add(categoryPreference);
                        }
                    }

                    if (param.preference.cityIDs != null && param.preference.cityIDs.Count > 0)
                    {
                        cityPreferences = new List<MemberCityPreference>();
                        MemberCityPreference cityPreference;
                        foreach (var preferenceCity in param.preference.cityIDs)
                        {
                            cityPreference = new MemberCityPreference();
                            cityPreference.CityID = preferenceCity;
                            cityPreferences.Add(cityPreference);
                        }
                    }
                }

                var facadeResult = MemberFacade.RegisterMember(member, memberPreference, categoryPreferences, cityPreferences, param.profile.password.Value, null, null, null, null, MemberFacade.Source.EMAIL, true, null, null, out token);

                if (facadeResult.IsSuccess && facadeResult.ReturnData != null)
                {
                    data.token = token;
                    data.memberID = facadeResult.ReturnData.MemberID;
                    data.fullName = facadeResult.ReturnData.FullName;
                    data.email = facadeResult.ReturnData.Email;
                    resultCode.code = 0;
                    resultCode.message = "Success";
                }
                else
                {
                    resultCode.code = 1;
                    resultCode.message = facadeResult.ErrorMessage;
                }
            }
            else
            {
                resultCode.code = 1;
                resultCode.message = ERROR_MESSAGE;
            }

            jsonResult.data = data;
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }

        // POST api/member/update       
        /// <summary>
        /// POST /member/update     
        /// PARAMETERS JSON.
        ///    {
        ///        "profile": {
        ///            "fullName": "Ricardo",
        ///            "email": "ricardo@gmail.com",
        ///            "placeOfBirth": "Jakarta",
        ///            "dateOfBirth": "2016-11-01",
        ///            "mobilePhoneNumber": "",
        ///            "address": "",
        ///            "provinceID": 1,
        ///            "cityID": 1,
        ///            "identificationNumber": "",
        ///            "password": ""
        ///        },
        ///        "preference": {
        ///            "minPriceID": 1,
        ///            "maxPriceID": 3,
        ///            ""categoryIDs"": [
        ///                1,
        ///                2
        ///            ],
        ///            ""cityIDs"": [
        ///                1,
        ///                2
        ///            ]
        ///        }
        ///    }
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateJWTToken]
        [ActionName("update")]
        public dynamic UpdateProfile()
        {
            //TODO: Add Validation (Follow Validator in Website)
            //TODO: Check for Mandatory Fields
            dynamic data = new ExpandoObject();
            dynamic jsonResult = new ExpandoObject();
            dynamic resultCode = new ExpandoObject();
            MemberPreference memberPreference = new MemberPreference();
            IList<MemberCategoryPreference> categoryPreferences = null;
            IList<MemberCityPreference> cityPreferences = null;
            dynamic param = new ExpandoObject();
            /*if (!String.IsNullOrWhiteSpace(text))
            {
                param = JsonConvert.DeserializeObject<dynamic>(text);
            }
            else
            {*/
            HttpContent requestContent = Request.Content;
            string jsonContent = requestContent.ReadAsStringAsync().Result;
            param = JsonConvert.DeserializeObject<dynamic>(jsonContent);
            //}

            if (GetCurrentMemberID().HasValue)
            {
                if (!String.IsNullOrWhiteSpace(param.profile.fullName.Value) && !String.IsNullOrWhiteSpace(param.profile.email.Value))
                {
                    /*jsonResult = Json.Decode(@"
                    {
                        ""data"": {
                            ""profile"": {
                                ""memberID"": 1,
                                ""fullName"": ""Ricardo"",
                                ""email"": ""ricardoalexanderh@gmail.com"",
                                ""placeOfBirth"": ""Jakarta"",
                                ""dateOfBirth"": ""2016-11-01"",
                                ""mobilePhoneNumber"": ""08119910561"",
                                ""address"": ""Jakarta"",
                                ""provinceID"": 1,
                                ""cityID"": 1,
                                ""identificationNumber"": ""12345""  
                            },
                            ""preference"": {
                                ""minPriceID"": 1,
                                ""maxPriceID"": 3,
                                ""categoryIDs"": [
                                    1,
                                    2
                                ],
                                ""cityIDs"": [
                                    1,
                                    2
                                ]
                             }                    
                        },
                        ""resultCode"": {
                            ""code"": 0,
                            ""message"": ""Success""
                        }
                    }");*/
                    Member member = new Member();
                    member.MemberID = GetCurrentMemberID().Value;
                    member.Email = param.profile.email;
                    member.FullName = param.profile.fullName;
                    member.Address = param.profile.address;
                    member.CityID = param.profile.cityID;
                    member.ProvinceID = param.profile.provinceID;
                    if (!String.IsNullOrWhiteSpace(param.profile.dateOfBirth.Value))
                    {
                        member.DateOfBirth = DateTime.ParseExact(param.profile.dateOfBirth.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    }
                    member.PlaceOfBirth = param.profile.placeOfBirth;
                    member.IdentificationNumber = param.profile.identificationNumber;
                    member.MobilePhoneNumber = param.profile.mobilePhoneNumber;

                    if (param.preference != null && (param.preference.minPriceID.Value != null || param.preference.maxPriceID.Value != null || (param.preference.categoryIDs != null && param.preference.categoryIDs.Count > 0) || (param.preference.cityIDs != null && param.preference.cityIDs.Count > 0)))
                    {
                        if (param.preference.minPriceID.Value != null)
                        {
                            memberPreference.MinPriceID = param.preference.minPriceID;
                        }
                        if (param.preference.maxPriceID.Value != null)
                        {
                            memberPreference.MaxPriceID = param.preference.maxPriceID;
                        }

                        if (param.preference.categoryIDs != null && param.preference.categoryIDs.Count > 0)
                        {
                            categoryPreferences = new List<MemberCategoryPreference>();
                            MemberCategoryPreference categoryPreference;
                            foreach (var preferenceCategory in param.preference.categoryIDs)
                            {
                                categoryPreference = new MemberCategoryPreference();
                                categoryPreference.CategoryID = preferenceCategory;
                                categoryPreferences.Add(categoryPreference);
                            }
                        }

                        if (param.preference.cityIDs != null && param.preference.cityIDs.Count > 0)
                        {
                            cityPreferences = new List<MemberCityPreference>();
                            MemberCityPreference cityPreference;
                            foreach (var preferenceCity in param.preference.cityIDs)
                            {
                                cityPreference = new MemberCityPreference();
                                cityPreference.CityID = preferenceCity;
                                cityPreferences.Add(cityPreference);
                            }
                        }
                    }

                    var isProfileChanged = false;
                    var existingMember = MemberFacade.GetActiveMemberByID(member.MemberID).ReturnData;
                    if (existingMember != null)
                    {
                        if (existingMember.Address != member.Address || existingMember.CityID != member.CityID || existingMember.DateOfBirth != member.DateOfBirth || existingMember.FullName != member.FullName || existingMember.IdentificationNumber != member.IdentificationNumber || existingMember.MobilePhoneNumber != member.MobilePhoneNumber || existingMember.PlaceOfBirth != member.PlaceOfBirth || existingMember.ProvinceID != member.ProvinceID)
                        {
                            isProfileChanged = true;
                        }
                    }
                    var existingMemberPreference = MemberFacade.GetActiveMemberPreferenceByMemberID(member.MemberID).ReturnData;
                    if (existingMemberPreference != null)
                    {
                        if (existingMemberPreference.MinPriceID != memberPreference.MinPriceID || existingMemberPreference.MaxPriceID != memberPreference.MaxPriceID)
                        {
                            isProfileChanged = true;
                        }
                        var existingMemberCategoryPreference = MemberFacade.GetAllActiveMemberCategoryPreferenceByMemberPreferenceID(existingMemberPreference.MemberPreferenceID).ReturnData;
                        if (existingMemberCategoryPreference != null && existingMemberCategoryPreference.Count > 0 && categoryPreferences == null)
                        {
                            isProfileChanged = true;
                        }
                        if (categoryPreferences != null && existingMemberCategoryPreference != null && existingMemberCategoryPreference.Count != categoryPreferences.Count)
                        {
                            isProfileChanged = true;
                        }
                        if (categoryPreferences != null && existingMemberCategoryPreference != null && existingMemberCategoryPreference.Count > 0 && categoryPreferences.Count > 0)
                        {
                            if (!existingMemberCategoryPreference.Select(m => m.CategoryID).OrderBy(m => m).SequenceEqual(categoryPreferences.Select(m => m.CategoryID).OrderBy(m => m)))
                            {
                                isProfileChanged = true;
                            }
                        }

                        var existingMemberCityPreference = MemberFacade.GetAllActiveMemberCityPreferenceByMemberPreferenceID(existingMemberPreference.MemberPreferenceID).ReturnData;
                        if (existingMemberCityPreference != null && existingMemberCityPreference.Count > 0 && cityPreferences == null)
                        {
                            isProfileChanged = true;
                        }
                        if (cityPreferences != null && existingMemberCityPreference != null && existingMemberCityPreference.Count != cityPreferences.Count)
                        {
                            isProfileChanged = true;
                        }
                        if (cityPreferences != null && existingMemberCityPreference != null && existingMemberCityPreference.Count > 0 && cityPreferences.Count > 0)
                        {
                            if (!existingMemberCityPreference.Select(m => m.CityID).OrderBy(m => m).SequenceEqual(cityPreferences.Select(m => m.CityID).OrderBy(m => m)))
                            {
                                isProfileChanged = true;
                            }
                        }
                    }

                    var facadeResult = MemberFacade.UpdateMemberProfile(member, memberPreference, categoryPreferences, cityPreferences, param.profile.password.Value, param.profile.oldPassword.Value, isProfileChanged, null);

                    if (facadeResult.IsSuccess && facadeResult.ReturnData != null)
                    {
                        /*data.profile = new ExpandoObject();
                        data.profile.memberID = facadeResult.ReturnData.MemberID;
                        data.profile.fullName = facadeResult.ReturnData.FullName;
                        data.profile.email = facadeResult.ReturnData.Email;
                        data.profile.placeOfBirth = facadeResult.ReturnData.PlaceOfBirth;
                        data.profile.dateOfBirth = facadeResult.ReturnData.DateOfBirth;
                        data.profile.mobilePhoneNumber = facadeResult.ReturnData.MobilePhoneNumber;
                        data.profile.address = facadeResult.ReturnData.Address;
                        data.profile.provinceID = facadeResult.ReturnData.ProvinceID;
                        data.profile.cityID = facadeResult.ReturnData.CityID;
                        data.profile.identificationNumber = facadeResult.ReturnData.IdentificationNumber;*/
                        data = param;
                        resultCode.code = 0;
                        resultCode.message = "Success";
                    }
                    else
                    {
                        resultCode.code = 1;
                        resultCode.message = facadeResult.ErrorMessage;
                    }
                }
                else
                {
                    resultCode.code = 1;
                    resultCode.message = ERROR_MESSAGE;
                }
            }
            else
            {
                resultCode.code = 1;
                resultCode.message = INVALID_TOKEN_ERROR_MESSAGE;
            }

            jsonResult.data = data;
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }

        /// <summary>
        /// GET /member/profile       
        /// Tidak ada parameter.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ValidateJWTToken]
        [ActionName("profile")]
        public dynamic Profile()
        {
            dynamic data = new ExpandoObject();
            dynamic jsonResult = new ExpandoObject();
            dynamic resultCode = new ExpandoObject();

            if (GetCurrentMemberID().HasValue)
            {
                /*jsonResult = Json.Decode(@"
                {
                    ""data"": {
                        ""profile"": {
                            ""memberID"": 1,
                            ""fullName"": ""Ricardo"",
                            ""email"": ""ricardoalexanderh@gmail.com"",
                            ""placeOfBirth"": ""Jakarta"",
                            ""dateOfBirth"": ""2016-11-01"",
                            ""mobilePhoneNumber"": ""08119910561"",
                            ""address"": ""Jakarta"",
                            ""provinceID"": 1,
                            ""cityID"": 1,
                            ""identificationNumber"": ""12345""
                        },
                        ""preference"": {
                            ""minPriceID"": 1,
                            ""maxPriceID"": 3,
                            ""categoryIDs"": [
                                1,
                                2
                            ],
                            ""cityIDs"": [
                                1,
                                2
                            ]
                        }                    
                    },
                    ""resultCode"": {
                        ""code"": 0,
                        ""message"": ""Success""
                    }
                }");*/
                var resultMember = MemberFacade.GetActiveMemberByID(GetCurrentMemberID().Value);
                if (resultMember.IsSuccess && resultMember.ReturnData != null)
                {
                    var member = resultMember.ReturnData;
                    data.profile = new ExpandoObject();
                    data.profile.cityID = member.CityID;
                    data.profile.dateOfBirth = member.DateOfBirth.HasValue ? member.DateOfBirth.Value.ToString("yyyy-MM-dd") : String.Empty;
                    data.profile.email = member.Email;
                    data.profile.address = member.Address;
                    data.profile.fullName = member.FullName;
                    data.profile.identificationNumber = member.IdentificationNumber;
                    data.profile.mobilePhoneNumber = member.MobilePhoneNumber;
                    data.profile.placeOfBirth = member.PlaceOfBirth;
                    data.profile.provinceID = member.ProvinceID;

                    //Set Preferences
                    var resultMemberPreference = MemberFacade.GetActiveMemberPreferenceByMemberID(member.MemberID);
                    if (resultMemberPreference.IsSuccess && resultMemberPreference.ReturnData != null)
                    {
                        data.preference = new ExpandoObject();
                        data.preference.minPriceID = resultMemberPreference.ReturnData.MinPriceID;
                        data.preference.maxPriceID = resultMemberPreference.ReturnData.MaxPriceID;

                        var resultMemberCityPreferences = MemberFacade.GetAllActiveMemberCityPreferenceByMemberPreferenceID(resultMemberPreference.ReturnData.MemberPreferenceID);
                        if (resultMemberCityPreferences.IsSuccess && resultMemberCityPreferences.ReturnData != null && resultMemberCityPreferences.ReturnData.Count > 0)
                        {
                            data.preference.cityIDs = new int[resultMemberCityPreferences.ReturnData.Count];
                            for (int i = 0; i < resultMemberCityPreferences.ReturnData.Count; i++)
                            {
                                data.preference.cityIDs[i] = resultMemberCityPreferences.ReturnData[i].CityID;
                            }
                        }

                        var resultMemberCategoryPreferences = MemberFacade.GetAllActiveMemberCategoryPreferenceByMemberPreferenceID(resultMemberPreference.ReturnData.MemberPreferenceID);
                        if (resultMemberCategoryPreferences.IsSuccess && resultMemberCategoryPreferences.ReturnData != null && resultMemberCategoryPreferences.ReturnData.Count > 0)
                        {
                            data.preference.categoryIDs = new int[resultMemberCategoryPreferences.ReturnData.Count];
                            for (int i = 0; i < resultMemberCategoryPreferences.ReturnData.Count; i++)
                            {
                                data.preference.categoryIDs[i] = resultMemberCategoryPreferences.ReturnData[i].CategoryID;
                            }
                        }

                        /*var resultMemberProvinceIDPreferences = MemberFacade.GetAllActiveMemberProvinceIDPreferenceByMemberPreferenceID(resultMemberPreference.ReturnData.MemberPreferenceID);
                        if (resultMemberProvinceIDPreferences.IsSuccess && resultMemberProvinceIDPreferences.ReturnData != null && resultMemberProvinceIDPreferences.ReturnData.Count > 0)
                        {
                            model.PreferenceProvinceID = new int[resultMemberProvinceIDPreferences.ReturnData.Count];
                            for (int i = 0; i < resultMemberProvinceIDPreferences.ReturnData.Count; i++)
                            {
                                model.PreferenceProvinceID[i] = resultMemberProvinceIDPreferences.ReturnData[i];
                            }
                        }*/
                    }

                    resultCode.code = 0;
                    resultCode.message = "Success";
                }
                else
                {
                    resultCode.code = 1;
                    resultCode.message = resultMember.ErrorMessage;
                }
            }
            else
            {
                resultCode.code = 1;
                resultCode.message = INVALID_TOKEN_ERROR_MESSAGE;
            }

            jsonResult.data = data;
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }

        /// <summary>
        /// POST /member/fb   
        /// PARAMETERS URL Encoded.
        ///    fullName=Ricardo&amp;email=ricardo%40gmail.com&amp;token=1234567&amp;url=facebook.com&amp;id=1
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ActionName("fb")]
        public dynamic FacebookConnectCallback([FromBody] FBCallbackParam param)
        {
            dynamic data = new ExpandoObject();
            dynamic jsonResult = new ExpandoObject();
            dynamic resultCode = new ExpandoObject();

            if (!String.IsNullOrWhiteSpace(param.Email) && !String.IsNullOrWhiteSpace(param.FullName) && !String.IsNullOrWhiteSpace(param.Token))
            {
                /*return Json.Decode(@"
                {
                    ""data"": {
                        ""token"": ""eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtZW1iZXJJRCI6MSwiZnVsbE5hbWUiOiJSaWNhcmRvIEFsZXhhbmRlciIsImVtYWlsIjoicmljYXJkb2FsZXhhbmRlcmhAZ21haWwuY29tIiwic2FsdCI6IjdvcUtQOHFqSUE9PSIsImV4cCI6MTQ4MjU3MTQ5NH0.CDXEYwZ3bIjGsYgt29u157BzaOP5beH92nddiNRdeK4"",
                        ""memberID"": 1,
                        ""fullName"": ""Ricardo Alexander"",
                        ""email"": ""ricardoalexanderh@gmail.com"",
                        ""isProfileComplete"": false
                    },
                    ""resultCode"": {
                        ""code"": 0,
                        ""message"": ""Success""
                    }
                }");*/
                String token;
                Member member = new Member();
                member.FullName = param.FullName;
                member.Email = param.Email;
                var facadeResult = MemberFacade.RegisterMember(member, null, null, null, null, param.ID, param.Token, param.URL, param.Email, MemberFacade.Source.FACEBOOK, true, null, null, out token);
                if (facadeResult.IsSuccess)
                {
                    data.token = token;
                    data.memberID = facadeResult.ReturnData.MemberID;
                    data.fullName = facadeResult.ReturnData.FullName;
                    data.email = facadeResult.ReturnData.Email;
                    data.isProfileComplete = MemberFacade.IsProfileComplete(facadeResult.ReturnData.MemberID).ReturnData;
                    resultCode.code = 0;
                    resultCode.message = "Success";
                }
                else
                {
                    resultCode.code = 1;
                    resultCode.message = facadeResult.ErrorMessage;
                }
            }
            else
            {
                resultCode.code = 1;
                resultCode.message = ERROR_MESSAGE;
            }

            jsonResult.data = data;
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }

        /// <summary>
        /// POST /member/login   
        /// PARAMETERS URL Encoded.
        ///    email=ricardo%40gmail.com&amp;password=password
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ActionName("login")]
        public dynamic Login([FromBody] LoginParam param)
        {
            dynamic data = new ExpandoObject();
            dynamic jsonResult = new ExpandoObject();
            dynamic resultCode = new ExpandoObject();

            if (!String.IsNullOrWhiteSpace(param.Email) && !String.IsNullOrWhiteSpace(param.Password))
            {
                /*return Json.Decode(@"
                {
                    ""data"": {
                        ""token"": ""eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtZW1iZXJJRCI6MSwiZnVsbE5hbWUiOiJSaWNhcmRvIEFsZXhhbmRlciIsImVtYWlsIjoicmljYXJkb2FsZXhhbmRlcmhAZ21haWwuY29tIiwic2FsdCI6IjdvcUtQOHFqSUE9PSIsImV4cCI6MTQ4MjU3MTQ5NH0.CDXEYwZ3bIjGsYgt29u157BzaOP5beH92nddiNRdeK4"",
                        ""memberID"": 1,
                        ""fullName"": ""Ricardo Alexander"",
                        ""email"": ""ricardoalexanderh@gmail.com"",
                        ""isProfileComplete"": false
                    },
                    ""resultCode"": {
                        ""code"": 0,
                        ""message"": ""Success""
                    }
                }");*/
                String token;
                var facadeResult = MemberFacade.LoginMemberUsingEmail(param.Email, param.Password, true, null, null, out token);
                if (facadeResult.IsSuccess)
                {
                    data.token = token;
                    data.memberID = facadeResult.ReturnData.MemberID;
                    data.fullName = facadeResult.ReturnData.FullName;
                    data.email = facadeResult.ReturnData.Email;
                    data.isProfileComplete = MemberFacade.IsProfileComplete(facadeResult.ReturnData.MemberID).ReturnData;
                    resultCode.code = 0;
                    resultCode.message = "Success";
                }
                else
                {
                    resultCode.code = 1;
                    resultCode.message = facadeResult.ErrorMessage;
                }
            }
            else
            {
                resultCode.code = 1;
                resultCode.message = ERROR_MESSAGE;
            }

            jsonResult.data = data;
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }

        /// <summary>
        /// POST /member/logout    
        /// Tidak ada parameter.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateJWTToken]
        [ActionName("logout")]
        public dynamic Logout()
        {
            dynamic data = new ExpandoObject();
            dynamic jsonResult = new ExpandoObject();
            dynamic resultCode = new ExpandoObject();

            if (GetCurrentMemberID().HasValue)
            {
                /*jsonResult = Json.Decode(@"
                {
                    ""data"": {                    
                        ""memberID"": 1                    
                    },
                    ""resultCode"": {
                        ""code"": 0,
                        ""message"": ""Success""
                    }
                }");*/
                var facadeResult = MemberFacade.MemberLogout(GetCurrentMemberID().Value, true, Request.Properties["Token"].ToString(), null);
                if (facadeResult.IsSuccess)
                {
                    data.memberID = facadeResult.ReturnData.MemberID;
                    resultCode.code = 0;
                    resultCode.message = "Success";
                }
                else
                {
                    resultCode.code = 1;
                    resultCode.message = facadeResult.ErrorMessage;
                }
            }
            else
            {
                resultCode.code = 1;
                resultCode.message = INVALID_TOKEN_ERROR_MESSAGE;
            }

            jsonResult.data = data;
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }

        /// <summary>
        /// POST /member/reset-password    
        /// PARAMETERS URL Encoded.
        ///    email=ricardo%40gmail.com
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ActionName("reset-password")]
        public dynamic ResetPassword([FromBody] EmailParam param)
        {
            dynamic data = new ExpandoObject();
            dynamic jsonResult = new ExpandoObject();
            dynamic resultCode = new ExpandoObject();

            if (!String.IsNullOrWhiteSpace(param.Email))
            {
                /*jsonResult = Json.Decode(@"
                {
                    ""data"": {                    
                        ""email"": ""ricardoalexanderh@gmail.com""                    
                    },
                    ""resultCode"": {
                        ""code"": 0,
                        ""message"": ""Success""
                    }
                }");*/
                var facadeResult = MemberFacade.ForgotPassword(param.Email, null);
                if (facadeResult.IsSuccess)
                {
                    data.email = facadeResult.ReturnData.Email;
                    resultCode.code = 0;
                    resultCode.message = "Success";
                }
                else
                {
                    resultCode.code = 1;
                    resultCode.message = facadeResult.ErrorMessage;
                }
            }
            else
            {
                resultCode.code = 1;
                resultCode.message = ERROR_MESSAGE;
            }

            jsonResult.data = data;
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }

        /// <summary>
        /// POST /member/favorite
        /// PARAMETERS URL Encoded.
        ///    assetID=1
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateJWTToken]
        [ActionName("favorite")]
        public dynamic Favorite([FromBody] AssetIDParam param)
        {
            dynamic data = new ExpandoObject();
            dynamic jsonResult = new ExpandoObject();
            dynamic resultCode = new ExpandoObject();

            if (GetCurrentMemberID().HasValue)
            {
                if (param.AssetID.HasValue)
                {
                    var facadeResult = MemberFacade.FavoriteAsset(GetCurrentMemberID().Value, param.AssetID.Value, null);
                    if (facadeResult.IsSuccess)
                    {
                        data.assetID = facadeResult.ReturnData.AssetID;
                        resultCode.code = 0;
                        resultCode.message = "Success";
                    }
                    else
                    {
                        resultCode.code = 1;
                        resultCode.message = facadeResult.ErrorMessage;
                    }
                }
                else
                {
                    resultCode.code = 1;
                    resultCode.message = ERROR_MESSAGE;
                }
            }
            else
            {
                resultCode.code = 1;
                resultCode.message = INVALID_TOKEN_ERROR_MESSAGE;
            }

            jsonResult.data = data;
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }

        /// <summary>
        /// POST /member/unfavorite
        /// PARAMETERS URL Encoded.
        ///    assetID=1
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateJWTToken]
        [ActionName("unfavorite")]
        public dynamic Unfavorite([FromBody] AssetIDParam param)
        {
            dynamic data = new ExpandoObject();
            dynamic jsonResult = new ExpandoObject();
            dynamic resultCode = new ExpandoObject();

            if (GetCurrentMemberID().HasValue)
            {
                if (param.AssetID.HasValue)
                {
                    var facadeResult = MemberFacade.UnfavoriteAsset(GetCurrentMemberID().Value, param.AssetID.Value, null);
                    if (facadeResult.IsSuccess)
                    {
                        data.assetID = facadeResult.ReturnData.AssetID;
                        resultCode.code = 0;
                        resultCode.message = "Success";
                    }
                    else
                    {
                        resultCode.code = 1;
                        resultCode.message = facadeResult.ErrorMessage;
                    }
                }
                else
                {
                    resultCode.code = 1;
                    resultCode.message = ERROR_MESSAGE;
                }
            }
            else
            {
                resultCode.code = 1;
                resultCode.message = INVALID_TOKEN_ERROR_MESSAGE;
            }

            jsonResult.data = data;
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }
    }
}
