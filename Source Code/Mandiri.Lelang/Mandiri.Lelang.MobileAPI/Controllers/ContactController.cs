﻿using Mandiri.Lelang.Attribute.ActionFilter;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Mandiri.Lelang.MobileAPI.Controllers
{
    public class ContactController : BaseController
    {
        public class ContactParam
        {
            public String Name { get; set; }
            public String Phone { get; set; }
            public String Email { get; set; }
            public String TextMessage { get; set; }
            public byte MessageCategoryTypeID { get; set; }
            public String AssetCode { get; set; }
            public byte? BranchID { get; set; }
        }

        // POST api/contact/contactus        
        [HttpPost]
        [ActionName("contactus")]
        [ValidateSQLInjection]
        public dynamic SubmitContactUs([FromBody] ContactParam param)
        {
            Message message;
            dynamic jsonResult = new ExpandoObject();
            dynamic data = new ExpandoObject();
            dynamic resultCode = new ExpandoObject();

            try
            {
                message = new Message();
                message.Name = param.Name;
                message.Email = param.Email;
                message.Phone = param.Phone;
                message.TextMessage = param.TextMessage;
                message.MessageCategoryTypeID = param.MessageCategoryTypeID;
                message.AssetCode = param.AssetCode;
                message.BranchID = param.BranchID;
                message.MessageStatusID = 1;
                //Try getting the IP Address
                try
                {
                    //TODO: Validate
                    message.IPAddress = IPHelper.GetClientIPAddress(Request);                   
                }
                catch
                {
                    message.IPAddress = String.Empty;
                }
                ContactFacade.AddNewMessage(message);

                data.name = param.Name;
                data.phone = param.Phone;
                data.email = param.Email;
                data.messageCategoryTypeID = param.MessageCategoryTypeID;
                data.assetCode = param.AssetCode;
                data.branchID = param.BranchID;
                data.textMessage = param.TextMessage;

                resultCode.code = 0;
                resultCode.message = "Success";
            }
            catch
            {
                resultCode.code = 0;
                resultCode.message = "Success";
            }

            jsonResult.data = data;
            jsonResult.thankYouMessage = "Terima kasih. Pesan anda telah kami terima, kami akan segera menghubungi anda.";
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }
    }
}