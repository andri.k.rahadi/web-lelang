﻿using Mandiri.Lelang.Attribute.ActionFilter;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.DataModel;
using Mandiri.Lelang.ViewModel;
using Mandiri.Lelang.ViewModel.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace Mandiri.Lelang.MobileAPI.Controllers
{
    [ValidateJWTToken]
    public class AssetController : BaseController
    {
        private IList<AssetListingViewModel> MapAssetListToViewModel(IList<dynamic> assets)
        {
            IList<AssetListingViewModel> viewModels = new List<AssetListingViewModel>();
            AssetListingViewModel viewModel;

            foreach (dynamic asset in assets)
            {
                viewModel = new AssetListingViewModel();
                viewModel.AssetID = asset.AssetID;
                viewModel.Address = asset.Address;
                viewModel.CategoryID = asset.CategoryID;
                viewModel.CategoryName = asset.CategoryName;
                viewModel.OldPrice = asset.OldPrice;
                viewModel.NewPrice = asset.NewPrice;
                viewModel.AuctionDate = asset.ScheduleDate;
                viewModel.AuctionTime = asset.ScheduleTime;
                viewModel.PostedDate = asset.DateTimeCreated;
                viewModel.Timezone = asset.Timezone;
                viewModel.CityID = asset.CityID;
                viewModel.CityName = asset.CityName;
                viewModel.ProvinceID = asset.ProvinceID;
                viewModel.ProvinceName = asset.ProvinceName;
                viewModel.CurrentStatusID = asset.CurrentStatusID;
                viewModel.IsEmailAuction = asset.IsEmailAuction;
                viewModel.IsFavorite = asset.IsFavorite;
                viewModel.Type = asset.Type;
                if (asset.Type == 1)
                {
                    //Jual Sukarela
                    viewModel.OldPrice = 0;
                    viewModel.NewPrice = asset.Price;
                    viewModel.IsEmailAuction = false;
                    viewModel.AuctionDate = null;
                    viewModel.AuctionTime = null;
                }
                viewModels.Add(viewModel);
            }
            return viewModels;
        }

        private AssetListingViewModel MapAssetDetailsToViewModel(dynamic assetDetails)
        {
            AssetListingViewModel model = new AssetListingViewModel();

            model.AssetID = assetDetails.AssetID;
            model.AssetCode = assetDetails.AssetCode;
            model.CategoryName = assetDetails.CategoryName;
            model.OldPrice = assetDetails.OldPrice;
            model.NewPrice = assetDetails.NewPrice;
            model.Address = assetDetails.Address;
            model.CityName = assetDetails.CityName;
            model.ProvinceName = assetDetails.ProvinceName;
            model.Photo1 = assetDetails.Photo1;
            model.Photo2 = assetDetails.Photo2;
            model.Photo3 = assetDetails.Photo3;
            model.Photo4 = assetDetails.Photo4;
            model.Photo5 = assetDetails.Photo5;
            model.PostedDate = assetDetails.DateTimeCreated;
            model.CurrentStatusID = assetDetails.CurrentStatusID;
            model.IsEmailAuction = assetDetails.IsEmailAuction;
            model.EmailAuctionURL = assetDetails.EmailAuctionURL;
            model.IsFavorite = assetDetails.IsFavorite;
            model.Latitude = (float?)assetDetails.Latitude;
            model.Longitude = (float?)assetDetails.Longitude;

            model.OtherFields = AssetFacade.GetAllActiveAssetCategoryTemplateValuesByAssetID(model.AssetID).ReturnData;

            model.TransferInformation = assetDetails.TransferInformation;
            model.Timezone = assetDetails.Timezone;
            model.AuctionDate = assetDetails.ScheduleDate;
            model.AuctionTime = assetDetails.ScheduleTime;
            model.DocumentTypeName = assetDetails.DocumentTypeName;
            model.KPKNLName = assetDetails.KPKNLName;
            //model.KPKNLAddress = assetDetails.KPKNLAddress;
            //Replace KPKNL Address with Auction Address
            model.KPKNLAddress = assetDetails.AuctionAddress;
            model.KPKNLPhone1 = assetDetails.KPKNLPhone1;
            model.KPKNLPhone2 = assetDetails.KPKNLPhone2;

            model.BranchName = assetDetails.BranchName;
            model.BranchAddress = assetDetails.BranchAddress;
            model.BranchPhone1 = assetDetails.BranchPhone1;
            model.BranchPhone2 = assetDetails.BranchPhone2;

            model.AuctionHallName = assetDetails.AuctionHallName;
            model.AuctionHallAddress = assetDetails.AuctionHallAddress;
            model.AuctionHallPhone1 = assetDetails.AuctionHallPhone1;
            model.AuctionHallPhone2 = assetDetails.AuctionHallPhone2;

            model.TwitterText = String.Format("Kunjungi {0} . Info {1},{2} . {3},{4} . {0}/asset/detail/{5}", ConfigurationManager.AppSettings["RootPath"], model.CategoryName, model.AbbreviatedNewPriceText, model.CityName, model.ProvinceName, model.AssetID.ToString());
            model.EmailSubject = HttpUtility.UrlEncode(String.Format("Ada info agunan di {0}/asset/detail/{1}", ConfigurationManager.AppSettings["RootPath"], model.AssetID.ToString()));
            model.EmailText = HttpUtility.UrlEncode(String.Format("Kunjungi {0} . Info {1},{2}.{3},{4} . {0}/asset/detail/{5}", ConfigurationManager.AppSettings["RootPath"], model.CategoryName, model.AbbreviatedNewPriceText, model.CityName, model.ProvinceName, model.AssetID.ToString()));

            model.Terms = MasterFacade.GetActiveWebContentByWebContentTypeID(8).ReturnData.Content;

            model.Type = assetDetails.Type;
            if (assetDetails.Type == 1)
            {
                //Jual Sukarela
                model.OldPrice = 0;
                model.NewPrice = assetDetails.Price;
                model.IsEmailAuction = false;
                model.AuctionDate = null;
                model.AuctionTime = null;
            }

            return model;
        }

        private IList<AuctionScheduleViewModel> MapAuctionScheduleListToViewModel(IList<dynamic> schedules)
        {
            IList<AuctionScheduleViewModel> viewModels = new List<AuctionScheduleViewModel>();
            AuctionScheduleViewModel viewModel;

            foreach (dynamic schedule in schedules)
            {
                viewModel = new AuctionScheduleViewModel();
                viewModel.AuctionScheduleID = schedule.AuctionScheduleID;
                viewModel.AuctionDate = schedule.ScheduleDate;
                viewModel.AuctionTime = schedule.ScheduleTime;
                viewModel.KPKNLAddress = schedule.AuctionAddress;
                viewModel.KPKNLName = schedule.Name;
                viewModel.KPKNLPhone1 = schedule.Phone1;
                viewModel.KPKNLPhone2 = schedule.Phone2;
                viewModel.Timezone = schedule.Timezone;
                viewModels.Add(viewModel);
            }
            return viewModels;
        }

        /// <summary>
        /// GET /asset/all
        /// Ada penambahan parameter keyword (string).
        /// Ada penambahan field isFavorite (true/false), isEmailAuction (true/false).        
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="categoryID"></param>
        /// <param name="provinceID"></param>
        /// <param name="cityID"></param>
        /// <param name="minPriceID"></param>
        /// <param name="maxPriceID"></param>
        /// <param name="hotPrice"></param>
        /// <param name="auctionDate"></param>
        /// <param name="documentTypeID"></param>
        /// <param name="address"></param>
        /// <param name="type1">1 = Jual Sukarela, 1/0</param>
        /// <param name="type2">2 = Lelang, 1/0</param>
        /// <param name="keyword"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("all")]
        public dynamic GetAllAssets(int pageIndex = 0, int categoryID = 0, int provinceID = 0, int cityID = 0, int minPriceID = 0, int maxPriceID = 0, int hotPrice = 0, String auctionDate = "", int documentTypeID = 0, String address = "", int type1 = -1, int type2 = -1, String keyword = "", int orderBy = 1)
        {
            List<dynamic> data = new List<dynamic>();
            dynamic jsonResult = new ExpandoObject();
            dynamic asset;
            dynamic resultCode = new ExpandoObject();
            FacadeResult<IList<dynamic>> results;
            IList<AssetListingViewModel> viewModels;
            DateTime? dAuctionDate = null;

            try
            {
                categoryID = categoryID == 0 ? -1 : categoryID;
                provinceID = provinceID == 0 ? -1 : provinceID;
                cityID = cityID == 0 ? -1 : cityID;
                minPriceID = minPriceID == 0 ? -1 : minPriceID;
                maxPriceID = maxPriceID == 0 ? -1 : maxPriceID;
                documentTypeID = documentTypeID == 0 ? -1 : documentTypeID;
                type1 = type1 == -1 ? 1 : type1;
                type2 = type2 == -1 ? 1 : type2;
                orderBy = orderBy == 0 ? 1 : orderBy;

                if (!String.IsNullOrWhiteSpace(auctionDate))
                {
                    DateTime.ParseExact(auctionDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                }
                results = AssetFacade.GetAllActiveAssetsByCriteria(keyword, categoryID, provinceID, cityID, minPriceID, maxPriceID, hotPrice == 1 ? true : false, dAuctionDate, documentTypeID, address, orderBy - 1, pageIndex, 12, false, memberID: GetCurrentMemberID(), isVolunteerSellType: type1 == 1 ? true : false, isAuctionType: type2 == 1 ? true : false);
                viewModels = MapAssetListToViewModel(results.ReturnData);

                foreach (AssetListingViewModel viewModel in viewModels)
                {
                    asset = new ExpandoObject();
                    asset.assetID = viewModel.AssetID;
                    asset.postedDate = viewModel.PostedDate.ToString("yyyyMMddHHmmss");
                    asset.postedDateFormattedDateTime = viewModel.PostedDateText;
                    asset.categoryID = viewModel.CategoryID;
                    asset.categoryName = viewModel.CategoryName;
                    asset.isFavorite = viewModel.IsFavorite;
                    asset.type = viewModel.Type;

                    if (!viewModel.IsSoldOrPaid)
                    {
                        asset.auctionDate = viewModel.AuctionDate.HasValue ? viewModel.AuctionDate.Value.ToString("yyyyMMdd") + (viewModel.AuctionTime.HasValue ? viewModel.AuctionTime.Value.ToString("HHmmss") : String.Empty) : String.Empty;
                        asset.auctionDateFormattedDateTime = viewModel.AuctionDateText;
                        asset.address = viewModel.Address;
                        asset.provinceID = viewModel.ProvinceID;
                        asset.provinceName = viewModel.ProvinceName;
                        asset.cityID = viewModel.CityID;
                        asset.cityName = viewModel.CityName;
                        asset.completeAddress = viewModel.CityAndProvinceText;
                        asset.oldPrice = viewModel.AbbreviatedOldPriceText;
                        asset.newPrice = viewModel.AbbreviatedNewPriceText;
                        asset.isHotPrice = viewModel.IsHotPrice;
                        asset.isEmailAuction = viewModel.IsEmailAuction;
                    }
                    else
                    {
                        asset.auctionDate = String.Empty;
                        asset.auctionDateFormattedDateTime = String.Empty;
                        asset.address = String.Empty;
                        asset.provinceID = String.Empty;
                        asset.provinceName = String.Empty;
                        asset.cityID = String.Empty;
                        asset.cityName = String.Empty;
                        asset.completeAddress = String.Empty;
                        asset.oldPrice = String.Empty;
                        asset.newPrice = "CALL";
                        asset.isHotPrice = false;
                        asset.isEmailAuction = false;
                    }

                    asset.defaultPhotoURL = String.Format("{0}/Image/Get/{1}/1/0", ConfigurationManager.AppSettings["RootPath"], viewModel.AssetID);

                    data.Add(asset);
                }

                resultCode.code = 0;
                resultCode.message = "Success";
            }
            catch
            {
                resultCode.code = 1;
                resultCode.message = ERROR_MESSAGE;
            }

            jsonResult.data = data;
            if (resultCode.code == 0 && pageIndex == 0)
            {
                jsonResult.totalRecords = AssetFacade.GetAllActiveAssetsByCriteriaCount(keyword, categoryID, provinceID, cityID, minPriceID, maxPriceID, hotPrice == 1 ? true : false, dAuctionDate, documentTypeID, address, orderBy - 1, false, GetCurrentMemberID(), isVolunteerSellType: type1 == 1 ? true : false, isAuctionType: type2 == 1 ? true : false).ReturnData;
            }
            else
            {
                jsonResult.totalRecords = 0;
            }
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }

        /// <summary>
        /// GET /asset/soon
        /// Ada penambahan field isFavorite (true/false), isEmailAuction (true/false).
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="categoryID"></param>
        /// <param name="provinceID"></param>
        /// <param name="cityID"></param>
        /// <param name="minPriceID"></param>
        /// <param name="maxPriceID"></param>
        /// <param name="hotPrice"></param>
        /// <param name="auctionDate"></param>
        /// <param name="documentTypeID"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("soon")]
        public dynamic GetNearestAuctionDateAssets(int pageIndex = 0, int categoryID = 0, int provinceID = 0, int cityID = 0, int minPriceID = 0, int maxPriceID = 0, int hotPrice = 0, String auctionDate = "", int documentTypeID = 0, String address = "")
        {
            List<dynamic> data = new List<dynamic>();
            dynamic jsonResult = new ExpandoObject();
            dynamic asset;
            dynamic resultCode = new ExpandoObject();
            FacadeResult<IList<dynamic>> results;
            IList<AssetListingViewModel> viewModels;
            DateTime? dAuctionDate = null;

            try
            {
                categoryID = categoryID == 0 ? -1 : categoryID;
                provinceID = provinceID == 0 ? -1 : provinceID;
                cityID = cityID == 0 ? -1 : cityID;
                minPriceID = minPriceID == 0 ? -1 : minPriceID;
                maxPriceID = maxPriceID == 0 ? -1 : maxPriceID;
                documentTypeID = documentTypeID == 0 ? -1 : documentTypeID;

                if (!String.IsNullOrWhiteSpace(auctionDate))
                {
                    DateTime.ParseExact(auctionDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                }
                results = AssetFacade.GetAllActiveAssetsByCriteria(String.Empty, categoryID, provinceID, cityID, minPriceID, maxPriceID, hotPrice == 1 ? true : false, dAuctionDate, documentTypeID, address, 3, pageIndex, 12, true, GetCurrentMemberID(), false, true);
                viewModels = MapAssetListToViewModel(results.ReturnData);

                foreach (AssetListingViewModel viewModel in viewModels)
                {
                    if (viewModel.NewPrice > 0)
                    {
                        asset = new ExpandoObject();
                        asset.assetID = viewModel.AssetID;
                        asset.postedDate = viewModel.PostedDate.ToString("yyyyMMddHHmmss");
                        asset.postedDateFormattedDateTime = viewModel.PostedDateText;
                        asset.auctionDate = viewModel.AuctionDate.HasValue ? viewModel.AuctionDate.Value.ToString("yyyyMMdd") + (viewModel.AuctionTime.HasValue ? viewModel.AuctionTime.Value.ToString("HHmmss") : String.Empty) : String.Empty;
                        asset.auctionDateFormattedDateTime = viewModel.AuctionDateText;
                        asset.categoryID = viewModel.CategoryID;
                        asset.categoryName = viewModel.CategoryName;
                        asset.address = viewModel.Address;
                        asset.provinceID = viewModel.ProvinceID;
                        asset.provinceName = viewModel.ProvinceName;
                        asset.cityID = viewModel.CityID;
                        asset.cityName = viewModel.CityName;
                        asset.completeAddress = viewModel.CityAndProvinceText;
                        asset.oldPrice = viewModel.AbbreviatedOldPriceText;
                        asset.newPrice = viewModel.AbbreviatedNewPriceText;
                        asset.defaultPhotoURL = String.Format("{0}/Image/Get/{1}/1/0", ConfigurationManager.AppSettings["RootPath"], viewModel.AssetID);
                        asset.isHotPrice = viewModel.IsHotPrice;
                        asset.isEmailAuction = viewModel.IsEmailAuction;
                        asset.isFavorite = viewModel.IsFavorite;
                        asset.type = viewModel.Type;
                        data.Add(asset);
                    }
                }

                resultCode.code = 0;
                resultCode.message = "Success";
            }
            catch
            {
                resultCode.code = 1;
                resultCode.message = ERROR_MESSAGE;
            }

            jsonResult.data = data;
            if (resultCode.code == 0 && pageIndex == 0)
            {
                jsonResult.totalRecords = AssetFacade.GetAllActiveAssetsByCriteriaCount(String.Empty, categoryID, provinceID, cityID, minPriceID, maxPriceID, hotPrice == 1 ? true : false, dAuctionDate, documentTypeID, address, 3, true, GetCurrentMemberID(), isVolunteerSellType: false, isAuctionType: true).ReturnData;
            }
            else
            {
                jsonResult.totalRecords = 0;
            }
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }

        /// <summary>
        /// GET /asset/hot 
        /// Ada penambahan field isFavorite (true/false), isEmailAuction (true/false).
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="categoryID"></param>
        /// <param name="provinceID"></param>
        /// <param name="cityID"></param>
        /// <param name="minPriceID"></param>
        /// <param name="maxPriceID"></param>
        /// <param name="auctionDate"></param>
        /// <param name="documentTypeID"></param>
        /// <param name="address"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("hot")]
        public dynamic GetHotPriceAssets(int pageIndex = 0, int categoryID = 0, int provinceID = 0, int cityID = 0, int minPriceID = 0, int maxPriceID = 0, String auctionDate = "", int documentTypeID = 0, String address = "")
        {
            List<dynamic> data = new List<dynamic>();
            dynamic jsonResult = new ExpandoObject();
            dynamic asset;
            dynamic resultCode = new ExpandoObject();
            FacadeResult<IList<dynamic>> results;
            IList<AssetListingViewModel> viewModels;
            DateTime? dAuctionDate = null;

            try
            {
                categoryID = categoryID == 0 ? -1 : categoryID;
                provinceID = provinceID == 0 ? -1 : provinceID;
                cityID = cityID == 0 ? -1 : cityID;
                minPriceID = minPriceID == 0 ? -1 : minPriceID;
                maxPriceID = maxPriceID == 0 ? -1 : maxPriceID;
                documentTypeID = documentTypeID == 0 ? -1 : documentTypeID;

                if (!String.IsNullOrWhiteSpace(auctionDate))
                {
                    DateTime.ParseExact(auctionDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                }
                results = AssetFacade.GetAllActiveAssetsByCriteria(String.Empty, categoryID, provinceID, cityID, minPriceID, maxPriceID, true, dAuctionDate, documentTypeID, address, 0, pageIndex, 12, true, memberID: GetCurrentMemberID(), isVolunteerSellType: false, isAuctionType: true);
                viewModels = MapAssetListToViewModel(results.ReturnData);

                foreach (AssetListingViewModel viewModel in viewModels)
                {
                    if (viewModel.NewPrice > 0)
                    {
                        asset = new ExpandoObject();
                        asset.assetID = viewModel.AssetID;
                        asset.postedDate = viewModel.PostedDate.ToString("yyyyMMddHHmmss");
                        asset.postedDateFormattedDateTime = viewModel.PostedDateText;
                        asset.auctionDate = viewModel.AuctionDate.HasValue ? viewModel.AuctionDate.Value.ToString("yyyyMMdd") + (viewModel.AuctionTime.HasValue ? viewModel.AuctionTime.Value.ToString("HHmmss") : String.Empty) : String.Empty;
                        asset.auctionDateFormattedDateTime = viewModel.AuctionDateText;
                        asset.categoryID = viewModel.CategoryID;
                        asset.categoryName = viewModel.CategoryName;
                        asset.address = viewModel.Address;
                        asset.provinceID = viewModel.ProvinceID;
                        asset.provinceName = viewModel.ProvinceName;
                        asset.cityID = viewModel.CityID;
                        asset.cityName = viewModel.CityName;
                        asset.completeAddress = viewModel.CityAndProvinceText;
                        asset.oldPrice = viewModel.AbbreviatedOldPriceText;
                        asset.newPrice = viewModel.AbbreviatedNewPriceText;
                        asset.defaultPhotoURL = String.Format("{0}/Image/Get/{1}/1/0", ConfigurationManager.AppSettings["RootPath"], viewModel.AssetID);
                        asset.isHotPrice = viewModel.IsHotPrice;
                        asset.isEmailAuction = viewModel.IsEmailAuction;
                        asset.isFavorite = viewModel.IsFavorite;
                        asset.type = viewModel.Type;
                        data.Add(asset);
                    }
                }

                resultCode.code = 0;
                resultCode.message = "Success";
            }
            catch
            {
                resultCode.code = 1;
                resultCode.message = ERROR_MESSAGE;
            }

            jsonResult.data = data;
            if (resultCode.code == 0 && pageIndex == 0)
            {
                jsonResult.totalRecords = AssetFacade.GetAllActiveAssetsByCriteriaCount(String.Empty, categoryID, provinceID, cityID, minPriceID, maxPriceID, true, dAuctionDate, documentTypeID, address, 0, true, memberID: GetCurrentMemberID(), isVolunteerSellType: false, isAuctionType: true).ReturnData;
            }
            else
            {
                jsonResult.totalRecords = 0;
            }
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }

        /// <summary>
        /// GET /asset/latest 
        /// Ada penambahan field isFavorite (true/false), isEmailAuction (true/false).
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="categoryID"></param>
        /// <param name="provinceID"></param>
        /// <param name="cityID"></param>
        /// <param name="minPriceID"></param>
        /// <param name="maxPriceID"></param>
        /// <param name="hotPrice"></param>
        /// <param name="auctionDate"></param>
        /// <param name="documentTypeID"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("latest")]
        public dynamic GetLatestAssets(int pageIndex = 0, int categoryID = 0, int provinceID = 0, int cityID = 0, int minPriceID = 0, int maxPriceID = 0, int hotPrice = 0, String auctionDate = "", int documentTypeID = 0, String address = "")
        {
            List<dynamic> data = new List<dynamic>();
            dynamic jsonResult = new ExpandoObject();
            dynamic asset;
            dynamic resultCode = new ExpandoObject();
            FacadeResult<IList<dynamic>> results;
            IList<AssetListingViewModel> viewModels;
            DateTime? dAuctionDate = null;

            try
            {
                categoryID = categoryID == 0 ? -1 : categoryID;
                provinceID = provinceID == 0 ? -1 : provinceID;
                cityID = cityID == 0 ? -1 : cityID;
                minPriceID = minPriceID == 0 ? -1 : minPriceID;
                maxPriceID = maxPriceID == 0 ? -1 : maxPriceID;
                documentTypeID = documentTypeID == 0 ? -1 : documentTypeID;

                if (!String.IsNullOrWhiteSpace(auctionDate))
                {
                    DateTime.ParseExact(auctionDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                }
                results = AssetFacade.GetAllActiveAssetsByCriteria(String.Empty, categoryID, provinceID, cityID, minPriceID, maxPriceID, hotPrice == 1 ? true : false, dAuctionDate, documentTypeID, address, 0, pageIndex, 12, true, memberID: GetCurrentMemberID(), isVolunteerSellType: true, isAuctionType: true);
                viewModels = MapAssetListToViewModel(results.ReturnData);

                foreach (AssetListingViewModel viewModel in viewModels)
                {
                    if (viewModel.NewPrice > 0)
                    {
                        asset = new ExpandoObject();
                        asset.assetID = viewModel.AssetID;
                        asset.postedDate = viewModel.PostedDate.ToString("yyyyMMddHHmmss");
                        asset.postedDateFormattedDateTime = viewModel.PostedDateText;
                        asset.auctionDate = viewModel.AuctionDate.HasValue ? viewModel.AuctionDate.Value.ToString("yyyyMMdd") + (viewModel.AuctionTime.HasValue ? viewModel.AuctionTime.Value.ToString("HHmmss") : String.Empty) : String.Empty;
                        asset.auctionDateFormattedDateTime = viewModel.AuctionDateText;
                        asset.categoryID = viewModel.CategoryID;
                        asset.categoryName = viewModel.CategoryName;
                        asset.address = viewModel.Address;
                        asset.provinceID = viewModel.ProvinceID;
                        asset.provinceName = viewModel.ProvinceName;
                        asset.cityID = viewModel.CityID;
                        asset.cityName = viewModel.CityName;
                        asset.completeAddress = viewModel.CityAndProvinceText;
                        asset.oldPrice = String.Empty;//viewModel.OldPriceText;
                        asset.newPrice = viewModel.AbbreviatedNewPriceText;
                        asset.defaultPhotoURL = String.Format("{0}/Image/Get/{1}/1/0", ConfigurationManager.AppSettings["RootPath"], viewModel.AssetID);
                        asset.isHotPrice = false;//viewModel.IsHotPrice;
                        asset.isEmailAuction = viewModel.IsEmailAuction;
                        asset.isFavorite = viewModel.IsFavorite;
                        asset.type = viewModel.Type;
                        data.Add(asset);
                    }
                }

                resultCode.code = 0;
                resultCode.message = "Success";
            }
            catch
            {
                resultCode.code = 1;
                resultCode.message = ERROR_MESSAGE;
            }

            jsonResult.data = data;
            if (resultCode.code == 0 && pageIndex == 0)
            {
                jsonResult.totalRecords = AssetFacade.GetAllActiveAssetsByCriteriaCount(String.Empty, categoryID, provinceID, cityID, minPriceID, maxPriceID, hotPrice == 1 ? true : false, dAuctionDate, documentTypeID, address, 0, true, memberID: GetCurrentMemberID(), isVolunteerSellType: false, isAuctionType: true).ReturnData;
            }
            else
            {
                jsonResult.totalRecords = 0;
            }
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }

        /// <summary>
        /// GET /asset/direct-sell 
        /// Ada penambahan field isFavorite (true/false), isEmailAuction (true/false).
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="categoryID"></param>
        /// <param name="provinceID"></param>
        /// <param name="cityID"></param>
        /// <param name="minPriceID"></param>
        /// <param name="maxPriceID"></param>
        /// <param name="hotPrice"></param>
        /// <param name="auctionDate"></param>
        /// <param name="documentTypeID"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("direct-sell")]
        public dynamic GetDirectSellAssets(int pageIndex = 0, int categoryID = 0, int provinceID = 0, int cityID = 0, int minPriceID = 0, int maxPriceID = 0, int hotPrice = 0, String auctionDate = "", int documentTypeID = 0, String address = "")
        {
            List<dynamic> data = new List<dynamic>();
            dynamic jsonResult = new ExpandoObject();
            dynamic asset;
            dynamic resultCode = new ExpandoObject();
            FacadeResult<IList<dynamic>> results;
            IList<AssetListingViewModel> viewModels;
            DateTime? dAuctionDate = null;

            try
            {
                categoryID = categoryID == 0 ? -1 : categoryID;
                provinceID = provinceID == 0 ? -1 : provinceID;
                cityID = cityID == 0 ? -1 : cityID;
                minPriceID = minPriceID == 0 ? -1 : minPriceID;
                maxPriceID = maxPriceID == 0 ? -1 : maxPriceID;
                documentTypeID = documentTypeID == 0 ? -1 : documentTypeID;

                if (!String.IsNullOrWhiteSpace(auctionDate))
                {
                    DateTime.ParseExact(auctionDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                }
                results = AssetFacade.GetAllActiveAssetsByCriteria(String.Empty, categoryID, provinceID, cityID, minPriceID, maxPriceID, hotPrice == 1 ? true : false, dAuctionDate, documentTypeID, address, 0, pageIndex, 12, true, memberID: GetCurrentMemberID(), isVolunteerSellType: true, isAuctionType: false);
                viewModels = MapAssetListToViewModel(results.ReturnData);

                foreach (AssetListingViewModel viewModel in viewModels)
                {
                    if (viewModel.NewPrice > 0)
                    {
                        asset = new ExpandoObject();
                        asset.assetID = viewModel.AssetID;
                        asset.postedDate = viewModel.PostedDate.ToString("yyyyMMddHHmmss");
                        asset.postedDateFormattedDateTime = viewModel.PostedDateText;
                        asset.auctionDate = viewModel.AuctionDate.HasValue ? viewModel.AuctionDate.Value.ToString("yyyyMMdd") + (viewModel.AuctionTime.HasValue ? viewModel.AuctionTime.Value.ToString("HHmmss") : String.Empty) : String.Empty;
                        asset.auctionDateFormattedDateTime = viewModel.AuctionDateText;
                        asset.categoryID = viewModel.CategoryID;
                        asset.categoryName = viewModel.CategoryName;
                        asset.address = viewModel.Address;
                        asset.provinceID = viewModel.ProvinceID;
                        asset.provinceName = viewModel.ProvinceName;
                        asset.cityID = viewModel.CityID;
                        asset.cityName = viewModel.CityName;
                        asset.completeAddress = viewModel.CityAndProvinceText;
                        asset.oldPrice = String.Empty;//viewModel.OldPriceText;
                        asset.newPrice = viewModel.AbbreviatedNewPriceText;
                        asset.defaultPhotoURL = String.Format("{0}/Image/Get/{1}/1/0", ConfigurationManager.AppSettings["RootPath"], viewModel.AssetID);
                        asset.isHotPrice = false;//viewModel.IsHotPrice;
                        asset.isEmailAuction = viewModel.IsEmailAuction;
                        asset.isFavorite = viewModel.IsFavorite;
                        asset.type = viewModel.Type;
                        data.Add(asset);
                    }
                }

                resultCode.code = 0;
                resultCode.message = "Success";
            }
            catch
            {
                resultCode.code = 1;
                resultCode.message = ERROR_MESSAGE;
            }

            jsonResult.data = data;
            if (resultCode.code == 0 && pageIndex == 0)
            {
                jsonResult.totalRecords = AssetFacade.GetAllActiveAssetsByCriteriaCount(String.Empty, categoryID, provinceID, cityID, minPriceID, maxPriceID, hotPrice == 1 ? true : false, dAuctionDate, documentTypeID, address, 0, true, memberID: GetCurrentMemberID(), isVolunteerSellType: true, isAuctionType: false).ReturnData;
            }
            else
            {
                jsonResult.totalRecords = 0;
            }
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }

        /// <summary>
        /// GET /asset/nearest        
        /// Ada penambahan field isFavorite (true/false), isEmailAuction (true/false).
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="categoryID"></param>
        /// <param name="provinceID"></param>
        /// <param name="cityID"></param>
        /// <param name="minPriceID"></param>
        /// <param name="maxPriceID"></param>
        /// <param name="hotPrice"></param>
        /// <param name="auctionDate"></param>
        /// <param name="documentTypeID"></param>
        /// <param name="address"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("nearest")]
        public dynamic GetNearestAssets(int pageIndex = 0, int categoryID = 0, int provinceID = 0, int cityID = 0, int minPriceID = 0, int maxPriceID = 0, int hotPrice = 0, String auctionDate = "", int documentTypeID = 0, String address = "")
        {
            List<dynamic> data = new List<dynamic>();
            dynamic jsonResult = new ExpandoObject();
            dynamic asset;
            dynamic resultCode = new ExpandoObject();
            FacadeResult<IList<dynamic>> results;
            IList<AssetListingViewModel> viewModels;
            DateTime? dAuctionDate = null;

            try
            {
                categoryID = categoryID == 0 ? -1 : categoryID;
                //provinceID = provinceID == 0 ? -1 : provinceID;
                //cityID = cityID == 0 ? -1 : cityID;
                minPriceID = minPriceID == 0 ? -1 : minPriceID;
                maxPriceID = maxPriceID == 0 ? -1 : maxPriceID;
                documentTypeID = documentTypeID == 0 ? -1 : documentTypeID;

                if (!String.IsNullOrWhiteSpace(auctionDate))
                {
                    DateTime.ParseExact(auctionDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                }
                results = AssetFacade.GetAllActiveAssetsByCriteria(String.Empty, categoryID, provinceID, cityID, minPriceID, maxPriceID, hotPrice == 1 ? true : false, dAuctionDate, documentTypeID, address, 0, pageIndex, 12, true, memberID: GetCurrentMemberID(), isVolunteerSellType: true, isAuctionType: true);
                viewModels = MapAssetListToViewModel(results.ReturnData);

                foreach (AssetListingViewModel viewModel in viewModels)
                {
                    if (viewModel.NewPrice > 0)
                    {
                        asset = new ExpandoObject();
                        asset.assetID = viewModel.AssetID;
                        asset.postedDate = viewModel.PostedDate.ToString("yyyyMMddHHmmss");
                        asset.postedDateFormattedDateTime = viewModel.PostedDateText;
                        asset.auctionDate = viewModel.AuctionDate.HasValue ? viewModel.AuctionDate.Value.ToString("yyyyMMdd") + (viewModel.AuctionTime.HasValue ? viewModel.AuctionTime.Value.ToString("HHmmss") : String.Empty) : String.Empty;
                        asset.auctionDateFormattedDateTime = viewModel.AuctionDateText;
                        asset.categoryID = viewModel.CategoryID;
                        asset.categoryName = viewModel.CategoryName;
                        asset.address = viewModel.Address;
                        asset.provinceID = viewModel.ProvinceID;
                        asset.provinceName = viewModel.ProvinceName;
                        asset.cityID = viewModel.CityID;
                        asset.cityName = viewModel.CityName;
                        asset.completeAddress = viewModel.CityAndProvinceText;
                        asset.oldPrice = viewModel.AbbreviatedOldPriceText;
                        asset.newPrice = viewModel.AbbreviatedNewPriceText;
                        asset.defaultPhotoURL = String.Format("{0}/Image/Get/{1}/1/0", ConfigurationManager.AppSettings["RootPath"], viewModel.AssetID);
                        asset.isHotPrice = viewModel.IsHotPrice;
                        asset.isEmailAuction = viewModel.IsEmailAuction;
                        asset.isFavorite = viewModel.IsFavorite;
                        asset.type = viewModel.Type;
                        data.Add(asset);
                    }
                }

                resultCode.code = 0;
                resultCode.message = "Success";
            }
            catch
            {
                resultCode.code = 1;
                resultCode.message = ERROR_MESSAGE;
            }

            jsonResult.data = data;
            if (resultCode.code == 0 && pageIndex == 0)
            {
                jsonResult.totalRecords = AssetFacade.GetAllActiveAssetsByCriteriaCount(String.Empty, categoryID, provinceID, cityID, minPriceID, maxPriceID, hotPrice == 1 ? true : false, dAuctionDate, documentTypeID, address, 0, true, memberID: GetCurrentMemberID(), isVolunteerSellType: true, isAuctionType: true).ReturnData;
            }
            else
            {
                jsonResult.totalRecords = 0;
            }
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }

        /// <summary>
        /// POST /asset/auctionschedule
        /// Diubah menjadi POST.
        /// Ada penambahan parameter body JSON { "fromDate": "yyyy-MM-dd", "toDate": "yyyy-MM-dd", "kpknlIDs": [ 1, 2 ] }     
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("auctionschedule")]
        public dynamic GetAllAuctionSchedules(int pageIndex = 0)
        {
            List<dynamic> data = new List<dynamic>();
            dynamic jsonResult = new ExpandoObject();
            dynamic schedule;
            dynamic resultCode = new ExpandoObject();
            IList<dynamic> schedules;
            IList<AuctionScheduleViewModel> viewModels;
            DateTime? auctionDateFrom = null;
            DateTime? auctionDateTo = null;
            int selectAll = -1;
            int[] kpknlIDs = null;

            try
            {
                var body = Request.Content.ReadAsStringAsync();
                if (body != null && !String.IsNullOrWhiteSpace(body.Result))
                {
                    dynamic bodyParam = JsonConvert.DeserializeObject(body.Result);
                    if (((DateTime?)bodyParam.fromDate).HasValue && !String.IsNullOrWhiteSpace(bodyParam.fromDate.Value))
                    {
                        auctionDateFrom = DateTime.ParseExact(bodyParam.fromDate.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    }
                    if (((DateTime?)bodyParam.toDate).HasValue && !String.IsNullOrWhiteSpace(bodyParam.toDate.Value))
                    {
                        auctionDateTo = DateTime.ParseExact(bodyParam.toDate.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    }
                    if (bodyParam.kpknlIDs != null && bodyParam.kpknlIDs.Count > 0)
                    {
                        kpknlIDs = ((JArray)bodyParam.kpknlIDs).Select(jv => (int)jv).ToArray();
                        selectAll = 0;
                    }
                    else
                    {
                        IList<KPKNL> kpknl = MasterFacade.GetAllActiveKPKNLs().ReturnData.OrderBy(m => m.Name).ToList();
                        kpknlIDs = new int[kpknl.Count];
                        for (int i = 0; i < kpknl.Count; i++)
                        {
                            kpknlIDs[i] = kpknl[i].KPKNLID.Value;
                        }
                        selectAll = -1;
                    }
                }
                else
                {
                    IList<KPKNL> kpknl = MasterFacade.GetAllActiveKPKNLs().ReturnData.OrderBy(m => m.Name).ToList();
                    kpknlIDs = new int[kpknl.Count];
                    for (int i = 0; i < kpknl.Count; i++)
                    {
                        kpknlIDs[i] = kpknl[i].KPKNLID.Value;
                    }
                    selectAll = -1;
                }
            }
            catch
            {
            }

            try
            {
                schedules = AssetFacade.GetAllActiveCurrentAuctionSchedules(pageIndex, 12, auctionDateFrom: auctionDateFrom, auctionDateTo: auctionDateTo, kpknlID: kpknlIDs, selectAll: selectAll).ReturnData;
                //Fix/temp solution 28 Apr 2016, for now just return all the schedules because the mobile pagination is not working
                //schedules = AssetFacade.GetAllActiveCurrentAuctionSchedulesWithoutPagination().ReturnData;
                viewModels = MapAuctionScheduleListToViewModel(schedules);
                foreach (AuctionScheduleViewModel viewModel in viewModels)
                {
                    schedule = new ExpandoObject();
                    schedule.auctionScheduleID = viewModel.AuctionScheduleID;
                    schedule.auctionDate = viewModel.AuctionDate.HasValue ? viewModel.AuctionDate.Value.ToString("yyyyMMdd") + (viewModel.AuctionTime.HasValue ? viewModel.AuctionTime.Value.ToString("HHmmss") : String.Empty) : String.Empty;
                    schedule.auctionDateFormattedDateTime = viewModel.AuctionDateText;
                    schedule.kpknlID = 0;
                    schedule.kpknlName = viewModel.KPKNLName;
                    schedule.kpknlAddress = viewModel.KPKNLAddress;
                    schedule.kpknlPhone1 = viewModel.KPKNLPhone1;
                    schedule.kpknlPhone2 = viewModel.KPKNLPhone2;
                    data.Add(schedule);
                }

                resultCode.code = 0;
                resultCode.message = "Success";
            }
            catch
            {
                resultCode.code = 1;
                resultCode.message = ERROR_MESSAGE;
            }

            jsonResult.data = data;
            if (resultCode.code == 0 && pageIndex == 0)
            {
                jsonResult.totalRecords = AssetFacade.GetCurrentAuctionSchedulesCount(auctionDateFrom: auctionDateFrom, auctionDateTo: auctionDateTo, kpknlID: kpknlIDs, selectAll: selectAll).ReturnData;
            }
            else
            {
                jsonResult.totalRecords = 0;
            }
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }

        /// <summary>
        /// POST /asset/download-auctionschedule
        /// API Baru.
        /// PARAMETERS JSON.
        /// { "fromDate": "yyyy-MM-dd", "toDate": "yyyy-MM-dd", "kpknlIDs": [ 1, 2 ] }     
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ActionName("download-auctionschedule")]
        public HttpResponseMessage DownloadAllAuctionSchedules()
        {
            DateTime? auctionDateFrom = null;
            DateTime? auctionDateTo = null;
            int selectAll = -1;
            int[] kpknlIDs = null;

            try
            {
                var body = Request.Content.ReadAsStringAsync();
                if (body != null && !String.IsNullOrWhiteSpace(body.Result))
                {
                    dynamic bodyParam = JsonConvert.DeserializeObject(body.Result);
                    if (((DateTime?)bodyParam.fromDate).HasValue && !String.IsNullOrWhiteSpace(bodyParam.fromDate.Value))
                    {
                        auctionDateFrom = DateTime.ParseExact(bodyParam.fromDate.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    }
                    if (((DateTime?)bodyParam.toDate).HasValue && !String.IsNullOrWhiteSpace(bodyParam.toDate.Value))
                    {
                        auctionDateTo = DateTime.ParseExact(bodyParam.toDate.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    }
                    if (bodyParam.kpknlIDs != null && bodyParam.kpknlIDs.Count > 0)
                    {
                        kpknlIDs = ((JArray)bodyParam.kpknlIDs).Select(jv => (int)jv).ToArray();
                        selectAll = 0;
                    }
                    else
                    {
                        IList<KPKNL> kpknl = MasterFacade.GetAllActiveKPKNLs().ReturnData.OrderBy(m => m.Name).ToList();
                        kpknlIDs = new int[kpknl.Count];
                        for (int i = 0; i < kpknl.Count; i++)
                        {
                            kpknlIDs[i] = kpknl[i].KPKNLID.Value;
                        }
                        selectAll = -1;
                    }
                }
                else
                {
                    IList<KPKNL> kpknl = MasterFacade.GetAllActiveKPKNLs().ReturnData.OrderBy(m => m.Name).ToList();
                    kpknlIDs = new int[kpknl.Count];
                    for (int i = 0; i < kpknl.Count; i++)
                    {
                        kpknlIDs[i] = kpknl[i].KPKNLID.Value;
                    }
                    selectAll = -1;
                }
            }
            catch
            {
            }

            var buf = AssetFacade.DownloadAllActiveCurrentAuctionSchedulesAndAssetsAsByteArray(auctionDateFrom, auctionDateTo, kpknlIDs, selectAll).ReturnData;
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(buf);
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = String.Format("Jadwal Lelang {0}.csv", DateTime.Today.ToString("dd MMM yyyy"))
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }

        /// <summary>
        /// GET /asset/byschedule    
        /// Ada penambahan field isFavorite (true/false), isEmailAuction (true/false).
        /// </summary>
        /// <param name="auctionScheduleID"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("byschedule")]
        public dynamic GetAssetsBySchedule(long auctionScheduleID, int pageIndex)
        {
            List<dynamic> data = new List<dynamic>();
            dynamic jsonResult = new ExpandoObject();
            dynamic asset;
            dynamic resultCode = new ExpandoObject();
            FacadeResult<IList<dynamic>> results;
            IList<AssetListingViewModel> viewModels;

            try
            {
                results = AssetFacade.GetAllActiveAssetsByAuctionScheduleID(auctionScheduleID, pageIndex, 12, memberID: GetCurrentMemberID());
                viewModels = MapAssetListToViewModel(results.ReturnData);

                foreach (AssetListingViewModel viewModel in viewModels)
                {
                    asset = new ExpandoObject();
                    asset.assetID = viewModel.AssetID;
                    asset.postedDate = viewModel.PostedDate.ToString("yyyyMMddHHmmss");
                    asset.postedDateFormattedDateTime = viewModel.PostedDateText;
                    asset.auctionDate = viewModel.AuctionDate.HasValue ? viewModel.AuctionDate.Value.ToString("yyyyMMdd") + (viewModel.AuctionTime.HasValue ? viewModel.AuctionTime.Value.ToString("HHmmss") : String.Empty) : String.Empty;
                    asset.auctionDateFormattedDateTime = viewModel.AuctionDateText;
                    asset.categoryID = viewModel.CategoryID;
                    asset.categoryName = viewModel.CategoryName;
                    asset.address = viewModel.Address;
                    asset.provinceID = viewModel.ProvinceID;
                    asset.provinceName = viewModel.ProvinceName;
                    asset.cityID = viewModel.CityID;
                    asset.cityName = viewModel.CityName;
                    asset.completeAddress = viewModel.CityAndProvinceText;
                    asset.oldPrice = viewModel.AbbreviatedOldPriceText;
                    asset.newPrice = viewModel.AbbreviatedNewPriceText;
                    asset.defaultPhotoURL = String.Format("{0}/Image/Get/{1}/1/0", ConfigurationManager.AppSettings["RootPath"], viewModel.AssetID);
                    asset.isHotPrice = viewModel.IsHotPrice;
                    asset.isEmailAuction = viewModel.IsEmailAuction;
                    asset.isFavorite = viewModel.IsFavorite;
                    asset.type = 2;
                    data.Add(asset);
                }

                resultCode.code = 0;
                resultCode.message = "Success";
            }
            catch
            {
                resultCode.code = 1;
                resultCode.message = ERROR_MESSAGE;
            }

            jsonResult.data = data;
            if (resultCode.code == 0 && pageIndex == 0)
            {
                jsonResult.totalRecords = AssetFacade.GetAllActiveAssetsByAuctionScheduleIDCount(auctionScheduleID).ReturnData;
            }
            else
            {
                jsonResult.totalRecords = 0;
            }
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }

        /// <summary>
        /// GET /asset/favorite  
        /// API baru
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("favorite")]
        public dynamic GetFavoriteAssets(int pageIndex)
        {
            List<dynamic> data = new List<dynamic>();
            dynamic jsonResult = new ExpandoObject();
            dynamic asset;
            dynamic resultCode = new ExpandoObject();
            FacadeResult<IList<dynamic>> results;
            IList<AssetListingViewModel> viewModels;

            if (GetCurrentMemberID().HasValue)
            {
                try
                {
                    results = AssetFacade.GetAllActiveFavoriteAssetsByMemberID(GetCurrentMemberID().Value, pageIndex, 12);
                    viewModels = MapAssetListToViewModel(results.ReturnData);

                    foreach (AssetListingViewModel viewModel in viewModels)
                    {
                        asset = new ExpandoObject();
                        asset.assetID = viewModel.AssetID;
                        asset.postedDate = viewModel.PostedDate.ToString("yyyyMMddHHmmss");
                        asset.postedDateFormattedDateTime = viewModel.PostedDateText;
                        asset.auctionDate = viewModel.AuctionDate.HasValue ? viewModel.AuctionDate.Value.ToString("yyyyMMdd") + (viewModel.AuctionTime.HasValue ? viewModel.AuctionTime.Value.ToString("HHmmss") : String.Empty) : String.Empty;
                        asset.auctionDateFormattedDateTime = viewModel.AuctionDateText;
                        asset.categoryID = viewModel.CategoryID;
                        asset.categoryName = viewModel.CategoryName;
                        asset.address = viewModel.Address;
                        asset.provinceID = viewModel.ProvinceID;
                        asset.provinceName = viewModel.ProvinceName;
                        asset.cityID = viewModel.CityID;
                        asset.cityName = viewModel.CityName;
                        asset.completeAddress = viewModel.CityAndProvinceText;
                        asset.oldPrice = viewModel.AbbreviatedOldPriceText;
                        asset.newPrice = viewModel.AbbreviatedNewPriceText;
                        asset.defaultPhotoURL = String.Format("{0}/Image/Get/{1}/1/0", ConfigurationManager.AppSettings["RootPath"], viewModel.AssetID);
                        asset.isHotPrice = viewModel.IsHotPrice;
                        asset.isEmailAuction = viewModel.IsEmailAuction;
                        asset.isFavorite = viewModel.IsFavorite;
                        asset.type = viewModel.Type;
                        data.Add(asset);
                    }

                    resultCode.code = 0;
                    resultCode.message = "Success";
                }
                catch
                {
                    resultCode.code = 1;
                    resultCode.message = ERROR_MESSAGE;
                }
            }
            else
            {
                resultCode.code = 1;
                resultCode.message = INVALID_TOKEN_ERROR_MESSAGE;
            }

            jsonResult.data = data;
            if (resultCode.code == 0 && pageIndex == 0)
            {
                jsonResult.totalRecords = AssetFacade.GetAllActiveFavoriteAssetsByMemberIDCount(GetCurrentMemberID().Value).ReturnData;
            }
            else
            {
                jsonResult.totalRecords = 0;
            }
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }

        /// <summary>
        /// GET /asset/get
        /// Ada penambahan parameter cityID -> Didapatkan menggunakan GPS.
        /// Ada penambahan field isFavorite (true/false), isEmailAuction (true/false), emailAuctionURL (string).
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cityID"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("get")]
        public dynamic GetAssetDetails(long id, int? cityID = null)
        {
            List<dynamic> extraFields = new List<dynamic>();
            dynamic jsonResult = new ExpandoObject();
            dynamic asset = null;
            dynamic extraField;
            dynamic resultCode = new ExpandoObject();
            AssetListingViewModel viewModel;
            PageVisit pageVisit = new PageVisit();

            try
            {
                dynamic assetDetails = AssetFacade.GetActiveAssetDetailsByAssetID(id, memberID: GetCurrentMemberID()).ReturnData;

                if (assetDetails != null)
                {
                    //Log View Asset Details
                    try
                    {
                        pageVisit.SiteMapID = "ASSET - DETAILS (MOBILE)";
                        pageVisit.AssetID = id;
                        pageVisit.MemberID = GetCurrentMemberID();
                        pageVisit.CityID = cityID;

                        //Try getting the IP Address
                        try
                        {
                            //TODO: Validate
                            pageVisit.IPAddress = IPHelper.GetClientIPAddress(Request);
                        }
                        catch
                        {
                            pageVisit.IPAddress = String.Empty;
                        }

                        AdministrationFacade.AddNewPageVisit(pageVisit);
                    }
                    catch
                    {
                    }

                    viewModel = MapAssetDetailsToViewModel(assetDetails);

                    asset = new ExpandoObject();
                    asset.assetID = viewModel.AssetID;
                    asset.assetCode = viewModel.AssetCode;
                    asset.postedDate = viewModel.PostedDate.ToString("yyyyMMddHHmmss");
                    asset.postedDateFormattedDateTime = viewModel.PostedDateText;
                    asset.categoryID = viewModel.CategoryID;
                    asset.categoryName = viewModel.CategoryName;
                    asset.isFavorite = viewModel.IsFavorite;
                    asset.type = viewModel.Type;

                    if (!viewModel.IsSoldOrPaid)
                    {
                        asset.auctionDate = viewModel.AuctionDate.HasValue ? viewModel.AuctionDate.Value.ToString("yyyyMMdd") + (viewModel.AuctionTime.HasValue ? viewModel.AuctionTime.Value.ToString("HHmmss") : String.Empty) : String.Empty;
                        asset.auctionDateFormattedDateTime = viewModel.AuctionDateText;
                        asset.address = viewModel.Address;
                        asset.provinceID = viewModel.ProvinceID;
                        asset.provinceName = viewModel.ProvinceName;
                        asset.cityID = viewModel.CityID;
                        asset.cityName = viewModel.CityName;
                        asset.completeAddress = viewModel.CompleteAddressText;
                        asset.oldPrice = viewModel.OldPriceText;
                        asset.newPrice = viewModel.NewPriceText;
                        asset.documentTypeID = 0;
                        asset.documentTypeName = viewModel.DocumentTypeName;
                        asset.branchID = 0;
                        asset.branchName = viewModel.BranchName;
                        asset.branchAddress = viewModel.BranchAddress;
                        asset.branchPhone1 = viewModel.BranchPhone1;
                        asset.branchPhone2 = viewModel.BranchPhone2;
                        asset.kpknlID = 0;
                        asset.kpknlName = viewModel.KPKNLName;
                        asset.kpknlAddress = viewModel.KPKNLAddress;
                        asset.kpknlPhone1 = viewModel.KPKNLPhone1;
                        asset.kpknlPhone2 = viewModel.KPKNLPhone2;
                        asset.auctionHallID = 0;
                        asset.auctionHallName = viewModel.AuctionHallName;
                        asset.auctionHallAddress = viewModel.AuctionHallAddress;
                        asset.auctionHallPhone1 = viewModel.AuctionHallPhone1;
                        asset.auctionHallPhone2 = viewModel.AuctionHallPhone2;
                        asset.transferInformation = viewModel.TransferInformation;
                        asset.latitude = viewModel.Latitude.HasValue ? viewModel.Latitude.ToString() : String.Empty;
                        asset.longitude = viewModel.Longitude.HasValue ? viewModel.Longitude.ToString() : String.Empty;
                        asset.isHotPrice = viewModel.IsHotPrice;
                        asset.Terms = viewModel.Terms;
                        asset.isEmailAuction = viewModel.IsEmailAuction;
                        asset.emailAuctionURL = viewModel.EmailAuctionURL;

                        foreach (AssetCategoryTemplateValue templateValue in viewModel.OtherFields)
                        {
                            if ((templateValue.FieldName.ToUpper().Trim() == "LUAS TANAH" && viewModel.OtherFields.Count(m => m.FieldName.ToUpper().Trim().Contains("LUAS TANAH (M2)")) > 0)
                                || (templateValue.FieldName.ToUpper().Trim() == "LUAS BANGUNAN" && viewModel.OtherFields.Count(m => m.FieldName.ToUpper().Trim().Contains("LUAS BANGUNAN (M2)")) > 0))
                            {
                                //TODO: Do not add the template, temp fix to support template changes from luas tanah to luas tanah (m2) and luas bangunan to luas bangunan (m2)
                            }
                            else
                            {
                                if (templateValue.Value != null && !String.IsNullOrEmpty(templateValue.Value) && !String.IsNullOrWhiteSpace(templateValue.Value))
                                {
                                    extraField = new ExpandoObject();
                                    extraField.fieldName = templateValue.FieldName;
                                    extraField.value = templateValue.Value;
                                    extraFields.Add(extraField);
                                }
                            }
                        }
                    }
                    else
                    {
                        asset.auctionDate = String.Empty;
                        asset.auctionDateFormattedDateTime = String.Empty;
                        asset.address = String.Empty;
                        asset.provinceID = 0;
                        asset.provinceName = String.Empty;
                        asset.cityID = 0;
                        asset.cityName = String.Empty;
                        asset.completeAddress = String.Empty;
                        asset.oldPrice = String.Empty;
                        asset.newPrice = "CALL";
                        asset.documentTypeID = 0;
                        asset.documentTypeName = String.Empty;
                        asset.branchID = 0;
                        asset.branchName = String.Empty;
                        asset.branchAddress = String.Empty;
                        asset.branchPhone1 = String.Empty;
                        asset.branchPhone2 = String.Empty;
                        asset.kpknlID = 0;
                        asset.kpknlName = String.Empty;
                        asset.kpknlAddress = String.Empty;
                        asset.kpknlPhone1 = String.Empty;
                        asset.kpknlPhone2 = String.Empty;
                        asset.auctionHallID = 0;
                        asset.auctionHallName = String.Empty;
                        asset.auctionHallAddress = String.Empty;
                        asset.auctionHallPhone1 = String.Empty;
                        asset.auctionHallPhone2 = String.Empty;
                        asset.transferInformation = String.Empty;
                        asset.latitude = String.Empty;
                        asset.longitude = String.Empty;
                        asset.isHotPrice = false;
                        asset.Terms = String.Empty;
                        asset.isEmailAuction = false;
                        asset.emailAuctionURL = String.Empty;
                    }

                    if (!String.IsNullOrWhiteSpace(viewModel.Photo1))
                    {
                        asset.photo1URL = String.Format("{0}/Image/Get/{1}/1/0", ConfigurationManager.AppSettings["RootPath"], viewModel.AssetID);
                    }
                    else
                    {
                        asset.photo1URL = String.Empty;
                    }
                    if (!String.IsNullOrWhiteSpace(viewModel.Photo2))
                    {
                        asset.photo2URL = String.Format("{0}/Image/Get/{1}/2/0", ConfigurationManager.AppSettings["RootPath"], viewModel.AssetID);
                    }
                    else
                    {
                        asset.photo2URL = String.Empty;
                    }
                    if (!String.IsNullOrWhiteSpace(viewModel.Photo3))
                    {
                        asset.photo3URL = String.Format("{0}/Image/Get/{1}/3/0", ConfigurationManager.AppSettings["RootPath"], viewModel.AssetID);
                    }
                    else
                    {
                        asset.photo3URL = String.Empty;
                    }
                    if (!String.IsNullOrWhiteSpace(viewModel.Photo4))
                    {
                        asset.photo4URL = String.Format("{0}/Image/Get/{1}/4/0", ConfigurationManager.AppSettings["RootPath"], viewModel.AssetID);
                    }
                    else
                    {
                        asset.photo4URL = String.Empty;
                    }
                    if (!String.IsNullOrWhiteSpace(viewModel.Photo5))
                    {
                        asset.photo5URL = String.Format("{0}/Image/Get/{1}/5/0", ConfigurationManager.AppSettings["RootPath"], viewModel.AssetID);
                    }
                    else
                    {
                        asset.photo5URL = String.Empty;
                    }

                    asset.url = String.Format("{0}/asset/detail/{1}", ConfigurationManager.AppSettings["RootPath"], viewModel.AssetID.ToString());
                    asset.webUrl = ConfigurationManager.AppSettings["RootPath"];
                    asset.TwitterText = viewModel.TwitterText;
                    asset.EmailSubject = viewModel.EmailSubject;
                    asset.EmailText = viewModel.EmailText;

                    asset.extraFields = extraFields;

                    resultCode.code = 0;
                    resultCode.message = "Success";
                }
                else
                {
                    resultCode.code = 1;
                    resultCode.message = "Asset not Found";
                }
            }
            catch
            {
                resultCode.code = 1;
                resultCode.message = ERROR_MESSAGE;
            }

            jsonResult.data = asset != null ? asset : String.Empty;
            jsonResult.resultCode = resultCode;

            return jsonResult;
        }
    }
}