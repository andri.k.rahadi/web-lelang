﻿using Mandiri.Lelang.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insight.Database;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace Mandiri.Lelang.DataRepository
{
    public partial interface IMandiriLelangDataRepository : IDbConnection, IDbTransaction
    {
        [Sql("SELECT [AssetID], [AssetCode], [AccountNumber], [AccountName], [CategoryID], [Address], [ProvinceID], [CityID], [OldPrice], [NewPrice], [BranchID], [MarketValue], [LiquidationValue], [SecurityRightsValue], [SoldValue], [ValuationDate], [AuctionScheduleID], [AuctionHallID], [AuctionHallAddress], [AuctionHallPhone1], [AuctionHallPhone2], [KPKNLID], [KPKNLAddress], [KPKNLPhone1], [KPKNLPhone2], [SegmentID], [Photo1], [Photo2], [Photo3], [Photo4], [Photo5], [OwnershipDocumentTypeID], [CurrentStatusID], [Latitude], [Longitude], [TransferInformation], [AdditionalNotes], [DocumentNumber], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy], [AuctionAddress], [IsEmailAuction], [EmailAuctionURL], [LastAutoUpdatedDateTime], [Type], [Price], [ExpiredDate] FROM [Asset] WHERE [AssetID] = @assetID")]
        Asset GetAssetByID(Int64 assetID);
        [Sql("SELECT [AssetID], [AssetCode], [AccountNumber], [AccountName], [CategoryID], [Address], [ProvinceID], [CityID], [OldPrice], [NewPrice], [BranchID], [MarketValue], [LiquidationValue], [SecurityRightsValue], [SoldValue], [ValuationDate], [AuctionScheduleID], [AuctionHallID], [AuctionHallAddress], [AuctionHallPhone1], [AuctionHallPhone2], [KPKNLID], [KPKNLAddress], [KPKNLPhone1], [KPKNLPhone2], [SegmentID], [Photo1], [Photo2], [Photo3], [Photo4], [Photo5], [OwnershipDocumentTypeID], [CurrentStatusID], [Latitude], [Longitude], [TransferInformation], [AdditionalNotes], [DocumentNumber], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy], [AuctionAddress], [IsEmailAuction], [EmailAuctionURL], [LastAutoUpdatedDateTime], [Type], [Price], [ExpiredDate] FROM [Asset]")]
        IList<Asset> GetAllAsset();
        [Sql("UPDATE [Asset] SET [AssetCode] = @AssetCode, [AccountNumber] = @AccountNumber, [AccountName] = @AccountName, [CategoryID] = @CategoryID, [Address] = @Address, [ProvinceID] = @ProvinceID, [CityID] = @CityID, [OldPrice] = @OldPrice, [NewPrice] = @NewPrice, [BranchID] = @BranchID, [MarketValue] = @MarketValue, [LiquidationValue] = @LiquidationValue, [SecurityRightsValue] = @SecurityRightsValue, [SoldValue] = @SoldValue, [ValuationDate] = @ValuationDate, [AuctionScheduleID] = @AuctionScheduleID, [AuctionHallID] = @AuctionHallID, [AuctionHallAddress] = @AuctionHallAddress, [AuctionHallPhone1] = @AuctionHallPhone1, [AuctionHallPhone2] = @AuctionHallPhone2, [KPKNLID] = @Kpknlid, [KPKNLAddress] = @KPKNLAddress, [KPKNLPhone1] = @KPKNLPhone1, [KPKNLPhone2] = @KPKNLPhone2, [SegmentID] = @SegmentID, [Photo1] = @Photo1, [Photo2] = @Photo2, [Photo3] = @Photo3, [Photo4] = @Photo4, [Photo5] = @Photo5, [OwnershipDocumentTypeID] = @OwnershipDocumentTypeID, [CurrentStatusID] = @CurrentStatusID, [Latitude] = @Latitude, [Longitude] = @Longitude, [TransferInformation] = @TransferInformation, [AdditionalNotes] = @AdditionalNotes, [DocumentNumber] = @DocumentNumber, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy, [AuctionAddress] = @AuctionAddress, [IsEmailAuction] = @IsEmailAuction, [EmailAuctionURL] = @EmailAuctionURL, [LastAutoUpdatedDateTime] = @LastAutoUpdatedDateTime, [Type] = @Type, [Price] = @Price, [ExpiredDate] = @ExpiredDate WHERE [AssetID] = @AssetID")]
        void UpdateAsset(Asset asset);
        [Sql("INSERT INTO [Asset]([AssetCode], [AccountNumber], [AccountName], [CategoryID], [Address], [ProvinceID], [CityID], [OldPrice], [NewPrice], [BranchID], [MarketValue], [LiquidationValue], [SecurityRightsValue], [SoldValue], [ValuationDate], [AuctionScheduleID], [AuctionHallID], [AuctionHallAddress], [AuctionHallPhone1], [AuctionHallPhone2], [KPKNLID], [KPKNLAddress], [KPKNLPhone1], [KPKNLPhone2], [SegmentID], [Photo1], [Photo2], [Photo3], [Photo4], [Photo5], [OwnershipDocumentTypeID], [CurrentStatusID], [Latitude], [Longitude], [TransferInformation], [AdditionalNotes], [DocumentNumber], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy], [AuctionAddress], [IsEmailAuction], [EmailAuctionURL], [LastAutoUpdatedDateTime], [Type], [Price], [ExpiredDate]) OUTPUT Inserted.AssetID VALUES(@AssetCode, @AccountNumber, @AccountName, @CategoryID, @Address, @ProvinceID, @CityID, @OldPrice, @NewPrice, @BranchID, @MarketValue, @LiquidationValue, @SecurityRightsValue, @SoldValue, @ValuationDate, @AuctionScheduleID, @AuctionHallID, @AuctionHallAddress, @AuctionHallPhone1, @AuctionHallPhone2, @KPKNLID, @KPKNLAddress, @KPKNLPhone1, @KPKNLPhone2, @SegmentID, @Photo1, @Photo2, @Photo3, @Photo4, @Photo5, @OwnershipDocumentTypeID, @CurrentStatusID, @Latitude, @Longitude, @TransferInformation, @AdditionalNotes, @DocumentNumber, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy, @AuctionAddress, @IsEmailAuction, @EmailAuctionURL, @LastAutoUpdatedDateTime, @Type, @Price, @ExpiredDate)")]
        void InsertAsset(Asset asset);
        [Sql("DELETE FROM [Asset] WHERE [AssetID] = @assetID")]
        void DeleteAssetByID(Int64 assetID);

        [Sql("SELECT [AssetID], [FieldName], [Value], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AssetCategoryTemplateValue] WHERE [AssetID] = @assetID AND [FieldName] = @fieldName")]
        AssetCategoryTemplateValue GetAssetCategoryTemplateValueByID(Int64 assetID, String fieldName);
        [Sql("SELECT [AssetID], [FieldName][Value], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AssetCategoryTemplateValue]")]
        IList<AssetCategoryTemplateValue> GetAllAssetCategoryTemplateValue();
        [Sql("UPDATE [AssetCategoryTemplateValue] SET [Value] = @Value, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [AssetID] = @AssetID AND [FieldName] = @FieldName")]
        void UpdateAssetCategoryTemplateValue(AssetCategoryTemplateValue assetCategoryTemplateValue);
        [Sql("INSERT INTO [AssetCategoryTemplateValue]([AssetID], [FieldName], [Value], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy])  VALUES(@AssetID, @FieldName, @Value, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertAssetCategoryTemplateValue(AssetCategoryTemplateValue assetCategoryTemplateValue);
        [Sql("DELETE FROM [AssetCategoryTemplateValue] WHERE [AssetID] = @assetID AND [FieldName] = @fieldName")]
        void DeleteAssetCategoryTemplateValueByID(Int64 assetID, String fieldName);

        [Sql("SELECT [AssetChangesID], [AssetID], [IsNew], [IsUpdated], [IsDeleted], [OriginalAssetStatusID], [CurrentApprovalStatus], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AssetChanges] WHERE [AssetChangesID] = @assetChangesID")]
        AssetChanges GetAssetChangesByID(Int64 assetChangesID);
        [Sql("SELECT [AssetChangesID], [AssetID], [IsNew], [IsUpdated], [IsDeleted], [OriginalAssetStatusID], [CurrentApprovalStatus], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AssetChanges]")]
        IList<AssetChanges> GetAllAssetChanges();
        [Sql("UPDATE [AssetChanges] SET [AssetID] = @AssetID, [IsNew] = @IsNew, [IsUpdated] = @IsUpdated, [IsDeleted] = @IsDeleted,[OriginalAssetStatusID] = @OriginalAssetStatusID, [CurrentApprovalStatus] = @CurrentApprovalStatus, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [AssetChangesID] = @AssetChangesID")]
        void UpdateAssetChanges(AssetChanges assetChanges);
        [Sql("INSERT INTO [AssetChanges]([AssetID], [IsNew], [IsUpdated], [IsDeleted],[OriginalAssetStatusID], [CurrentApprovalStatus], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.AssetChangesID VALUES(@AssetID, @IsNew, @IsUpdated, @IsDeleted,@OriginalAssetStatusID, @CurrentApprovalStatus, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertAssetChanges(AssetChanges assetChanges);
        [Sql("DELETE FROM [AssetChanges] WHERE [AssetChangesID] = @assetChangesID")]
        void DeleteAssetChangesByID(Int64 assetChangesID);

        [Sql("SELECT [AssetChangesHistoryID], [AssetChangesID], [FieldOrColumnName], [LabelName], [OldValue], [NewValue], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AssetChangesHistory] WHERE [AssetChangesHistoryID] = @assetChangesHistoryID")]
        AssetChangesHistory GetAssetChangesHistoryByID(Int64 assetChangesHistoryID);
        [Sql("SELECT [AssetChangesHistoryID], [AssetChangesID], [FieldOrColumnName], [LabelName], [OldValue], [NewValue], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AssetChangesHistory]")]
        IList<AssetChangesHistory> GetAllAssetChangesHistory();
        [Sql("UPDATE [AssetChangesHistory] SET [AssetChangesID] = @AssetChangesID, [FieldOrColumnName] = @FieldOrColumnName, [LabelName] = @LabelName, [OldValue] = @OldValue, [NewValue] = @NewValue, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [AssetChangesHistoryID] = @AssetChangesHistoryID")]
        void UpdateAssetChangesHistory(AssetChangesHistory assetChangesHistory);
        [Sql("INSERT INTO [AssetChangesHistory]([AssetChangesID], [FieldOrColumnName], [LabelName], [OldValue], [NewValue], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.AssetChangesHistoryID VALUES(@AssetChangesID, @FieldOrColumnName, @LabelName, @OldValue, @NewValue, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertAssetChangesHistory(AssetChangesHistory assetChangesHistory);
        [Sql("DELETE FROM [AssetChangesHistory] WHERE [AssetChangesHistoryID] = @assetChangesHistoryID")]
        void DeleteAssetChangesHistoryByID(Int64 assetChangesHistoryID);

        [Sql("SELECT [AssetWorkflowHistoryID], [FieldOrColumnName], [Reason], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AssetRejectReason] WHERE [AssetWorkflowHistoryID] = @assetWorkflowHistoryID AND [FieldOrColumnName] = @fieldOrColumnName")]
        AssetRejectReason GetAssetRejectReasonByID(Int64 assetWorkflowHistoryID, String fieldOrColumnName);
        [Sql("SELECT [AssetWorkflowHistoryID], [FieldOrColumnName][Reason], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AssetRejectReason]")]
        IList<AssetRejectReason> GetAllAssetRejectReason();
        [Sql("UPDATE [AssetRejectReason] SET [Reason] = @Reason, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [AssetWorkflowHistoryID] = @AssetWorkflowHistoryID AND [FieldOrColumnName] = @FieldOrColumnName")]
        void UpdateAssetRejectReason(AssetRejectReason assetRejectReason);
        [Sql("INSERT INTO [AssetRejectReason]([FieldOrColumnName], [Reason], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.AssetWorkflowHistoryID VALUES(@FieldOrColumnName, @Reason, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertAssetRejectReason(AssetRejectReason assetRejectReason);
        [Sql("DELETE FROM [AssetRejectReason] WHERE [AssetWorkflowHistoryID] = @assetWorkflowHistoryID AND [FieldOrColumnName] = @fieldOrColumnName")]
        void DeleteAssetRejectReasonByID(Int64 assetWorkflowHistoryID, String fieldOrColumnName);

        [Sql("SELECT [AssetStatusID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AssetStatus] WHERE [AssetStatusID] = @assetStatusID")]
        AssetStatus GetAssetStatusByID(Byte assetStatusID);
        [Sql("SELECT [AssetStatusID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AssetStatus]")]
        IList<AssetStatus> GetAllAssetStatus();
        [Sql("UPDATE [AssetStatus] SET [Name] = @Name, [Description] = @Description, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [AssetStatusID] = @AssetStatusID")]
        void UpdateAssetStatus(AssetStatus assetStatus);
        [Sql("INSERT INTO [AssetStatus]([AssetStatusID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy])  VALUES(@AssetStatusID, @Name, @Description, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertAssetStatus(AssetStatus assetStatus);
        [Sql("DELETE FROM [AssetStatus] WHERE [AssetStatusID] = @assetStatusID")]
        void DeleteAssetStatusByID(Byte assetStatusID);

        [Sql("SELECT [AssetStatusHistoryID], [AssetID], [PrevAssetStatusID], [NewAssetStatusID], [Notes], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AssetStatusHistory] WHERE [AssetStatusHistoryID] = @assetStatusHistoryID")]
        AssetStatusHistory GetAssetStatusHistoryByID(Int64 assetStatusHistoryID);
        [Sql("SELECT [AssetStatusHistoryID], [AssetID], [PrevAssetStatusID], [NewAssetStatusID], [Notes], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AssetStatusHistory]")]
        IList<AssetStatusHistory> GetAllAssetStatusHistory();
        [Sql("UPDATE [AssetStatusHistory] SET [AssetID] = @AssetID, [PrevAssetStatusID] = @PrevAssetStatusID, [NewAssetStatusID] = @NewAssetStatusID, [Notes] = @Notes, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [AssetStatusHistoryID] = @AssetStatusHistoryID")]
        void UpdateAssetStatusHistory(AssetStatusHistory assetStatusHistory);
        [Sql("INSERT INTO [AssetStatusHistory]([AssetID], [PrevAssetStatusID], [NewAssetStatusID], [Notes], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.AssetStatusHistoryID VALUES(@AssetID, @PrevAssetStatusID, @NewAssetStatusID, @Notes, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertAssetStatusHistory(AssetStatusHistory assetStatusHistory);
        [Sql("DELETE FROM [AssetStatusHistory] WHERE [AssetStatusHistoryID] = @assetStatusHistoryID")]
        void DeleteAssetStatusHistoryByID(Int64 assetStatusHistoryID);

        [Sql("SELECT [AssetWorkflowHistoryID], [AssetChangesID], [IsApproved], [ApprovedBy], [IsRejected], [RejectedBy], [Reason], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AssetWorkflowHistory] WHERE [AssetWorkflowHistoryID] = @assetWorkflowHistoryID")]
        AssetWorkflowHistory GetAssetWorkflowHistoryByID(Int64 assetWorkflowHistoryID);
        [Sql("SELECT [AssetWorkflowHistoryID], [AssetChangesID], [IsApproved], [ApprovedBy], [IsRejected], [RejectedBy], [Reason], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AssetWorkflowHistory]")]
        IList<AssetWorkflowHistory> GetAllAssetWorkflowHistory();
        [Sql("UPDATE [AssetWorkflowHistory] SET [AssetChangesID] = @AssetChangesID, [IsApproved] = @IsApproved, [ApprovedBy] = @ApprovedBy, [IsRejected] = @IsRejected, [RejectedBy] = @RejectedBy, [Reason] = @Reason, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [AssetWorkflowHistoryID] = @AssetWorkflowHistoryID")]
        void UpdateAssetWorkflowHistory(AssetWorkflowHistory assetWorkflowHistory);
        [Sql("INSERT INTO [AssetWorkflowHistory]([AssetChangesID], [IsApproved], [ApprovedBy], [IsRejected], [RejectedBy], [Reason], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.AssetWorkflowHistoryID VALUES(@AssetChangesID, @IsApproved, @ApprovedBy, @IsRejected, @RejectedBy, @Reason, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertAssetWorkflowHistory(AssetWorkflowHistory assetWorkflowHistory);
        [Sql("DELETE FROM [AssetWorkflowHistory] WHERE [AssetWorkflowHistoryID] = @assetWorkflowHistoryID")]
        void DeleteAssetWorkflowHistoryByID(Int64 assetWorkflowHistoryID);

        [Sql("SELECT [AuctionHallID], [Name], [Description], [Address], [Phone1], [Phone2], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AuctionHall] WHERE [AuctionHallID] = @auctionHallID")]
        AuctionHall GetAuctionHallByID(Int32 auctionHallID);
        [Sql("SELECT [AuctionHallID], [Name], [Description], [Address], [Phone1], [Phone2], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AuctionHall]")]
        IList<AuctionHall> GetAllAuctionHall();
        [Sql("UPDATE [AuctionHall] SET [Name] = @Name, [Description] = @Description, [Address] = @Address, [Phone1] = @Phone1, [Phone2] = @Phone2, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [AuctionHallID] = @AuctionHallID")]
        void UpdateAuctionHall(AuctionHall auctionHall);
        [Sql("INSERT INTO [AuctionHall]([Name], [Description], [Address], [Phone1], [Phone2], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.AuctionHallID VALUES(@Name, @Description, @Address, @Phone1, @Phone2, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertAuctionHall(AuctionHall auctionHall);
        [Sql("DELETE FROM [AuctionHall] WHERE [AuctionHallID] = @auctionHallID")]
        void DeleteAuctionHallByID(Int32 auctionHallID);

        [Sql("SELECT [AuctionScheduleID], [KPKNLID], [ScheduleDate], [ScheduleTime], [Timezone], [AuctionAddress], [Phone1], [Phone2], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AuctionSchedule] WHERE [AuctionScheduleID] = @auctionScheduleID")]
        AuctionSchedule GetAuctionScheduleByID(Int64 auctionScheduleID);
        [Sql("SELECT [AuctionScheduleID], [KPKNLID], [ScheduleDate], [ScheduleTime], [Timezone], [AuctionAddress], [Phone1], [Phone2], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AuctionSchedule]")]
        IList<AuctionSchedule> GetAllAuctionSchedule();
        [Sql("UPDATE [AuctionSchedule] SET [KPKNLID] = @Kpknlid, [ScheduleDate] = @ScheduleDate, [ScheduleTime] = @ScheduleTime, [Timezone] = @Timezone, [AuctionAddress] = @AuctionAddress, [Phone1] = @Phone1, [Phone2] = @Phone2, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [AuctionScheduleID] = @AuctionScheduleID")]
        void UpdateAuctionSchedule(AuctionSchedule auctionSchedule);
        [Sql("INSERT INTO [AuctionSchedule]([KPKNLID], [ScheduleDate], [ScheduleTime], [Timezone], [AuctionAddress], [Phone1], [Phone2], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.AuctionScheduleID VALUES(@KPKNLID, @ScheduleDate, @ScheduleTime, @Timezone, @AuctionAddress, @Phone1, @Phone2, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertAuctionSchedule(AuctionSchedule auctionSchedule);
        [Sql("DELETE FROM [AuctionSchedule] WHERE [AuctionScheduleID] = @auctionScheduleID")]
        void DeleteAuctionScheduleByID(Int64 auctionScheduleID);

        [Sql("SELECT [AutoNumberingID], [AutoNumberingTypeID], [SegmentID], [BranchID], [RunningNumber], [LastResetDateTime], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AutoNumbering] WHERE [AutoNumberingID] = @autoNumberingID")]
        AutoNumbering GetAutoNumberingByID(Int64 autoNumberingID);
        [Sql("SELECT [AutoNumberingID], [AutoNumberingTypeID], [SegmentID], [BranchID], [RunningNumber], [LastResetDateTime], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AutoNumbering]")]
        IList<AutoNumbering> GetAllAutoNumbering();
        [Sql("UPDATE [AutoNumbering] SET [AutoNumberingTypeID] = @AutoNumberingTypeID, [SegmentID] = @SegmentID, [BranchID] = @BranchID, [RunningNumber] = @RunningNumber, [LastResetDateTime] = @LastResetDateTime, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [AutoNumberingID] = @AutoNumberingID")]
        void UpdateAutoNumbering(AutoNumbering autoNumbering);
        [Sql("INSERT INTO [AutoNumbering]([AutoNumberingTypeID], [SegmentID], [BranchID], [RunningNumber], [LastResetDateTime], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.AutoNumberingID VALUES(@AutoNumberingTypeID, @SegmentID, @BranchID, @RunningNumber, @LastResetDateTime, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertAutoNumbering(AutoNumbering autoNumbering);
        [Sql("DELETE FROM [AutoNumbering] WHERE [AutoNumberingID] = @autoNumberingID")]
        void DeleteAutoNumberingByID(Int64 autoNumberingID);

        [Sql("SELECT [AutoNumberingTypeID], [TypeName], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AutoNumberingType] WHERE [AutoNumberingTypeID] = @autoNumberingTypeID")]
        AutoNumberingType GetAutoNumberingTypeByID(Int32 autoNumberingTypeID);
        [Sql("SELECT [AutoNumberingTypeID], [TypeName], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AutoNumberingType]")]
        IList<AutoNumberingType> GetAllAutoNumberingType();
        [Sql("UPDATE [AutoNumberingType] SET [TypeName] = @TypeName, [Description] = @Description, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [AutoNumberingTypeID] = @AutoNumberingTypeID")]
        void UpdateAutoNumberingType(AutoNumberingType autoNumberingType);
        [Sql("INSERT INTO [AutoNumberingType]([AutoNumberingTypeID], [TypeName], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy])  VALUES(@AutoNumberingTypeID, @TypeName, @Description, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertAutoNumberingType(AutoNumberingType autoNumberingType);
        [Sql("DELETE FROM [AutoNumberingType] WHERE [AutoNumberingTypeID] = @autoNumberingTypeID")]
        void DeleteAutoNumberingTypeByID(Int32 autoNumberingTypeID);

        [Sql("SELECT [BranchID], [Name], [Alias], [Description], [Address], [Phone1], [Phone2], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Branch] WHERE [BranchID] = @branchID")]
        Branch GetBranchByID(Int32 branchID);
        [Sql("SELECT [BranchID], [Name], [Alias], [Description], [Address], [Phone1], [Phone2], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Branch]")]
        IList<Branch> GetAllBranch();
        [Sql("UPDATE [Branch] SET [Name] = @Name, [Description] = @Description, [Alias] = @Alias, [Address] = @Address, [Phone1] = @Phone1, [Phone2] = @Phone2, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [BranchID] = @BranchID")]
        void UpdateBranch(Branch branch);
        [Sql("INSERT INTO [Branch]([Name], [Description], [Alias], [Address], [Phone1], [Phone2], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.BranchID VALUES(@Name, @Description, @Alias, @Address, @Phone1, @phone2, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertBranch(Branch branch);
        [Sql("DELETE FROM [Branch] WHERE [BranchID] = @branchID")]
        void DeleteBranchByID(Int32 branchID);

        [Sql("SELECT [CategoryID], [Name], [Description], [CurrentAssetTotal], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Category] WHERE [CategoryID] = @categoryID")]
        Category GetCategoryByID(Int32 categoryID);
        [Sql("SELECT [CategoryID], [Name], [Description], [CurrentAssetTotal], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Category]")]
        IList<Category> GetAllCategory();
        [Sql("UPDATE [Category] SET [Name] = @Name, [Description] = @Description, [CurrentAssetTotal] = @CurrentAssetTotal, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [CategoryID] = @CategoryID")]
        void UpdateCategory(Category category);
        [Sql("INSERT INTO [Category]([Name], [Description], [CurrentAssetTotal], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.CategoryID VALUES(@Name, @Description, @CurrentAssetTotal, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertCategory(Category category);
        [Sql("DELETE FROM [Category] WHERE [CategoryID] = @categoryID")]
        void DeleteCategoryByID(Int32 categoryID);

        [Sql("SELECT [CategoryID], [FieldName], [IsMandatory], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [CategoryTemplate] WHERE [CategoryID] = @categoryID AND [FieldName] = @fieldName")]
        CategoryTemplate GetCategoryTemplateByID(Int32 categoryID, String fieldName);
        [Sql("SELECT [CategoryID], [FieldName][IsMandatory], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [CategoryTemplate]")]
        IList<CategoryTemplate> GetAllCategoryTemplate();
        [Sql("UPDATE [CategoryTemplate] SET [IsMandatory] = @IsMandatory, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [CategoryID] = @CategoryID AND [FieldName] = @FieldName")]
        void UpdateCategoryTemplate(CategoryTemplate categoryTemplate);
        [Sql("INSERT INTO [CategoryTemplate]([CategoryID], [FieldName], [IsMandatory], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy])  VALUES(@CategoryID, @FieldName, @IsMandatory, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertCategoryTemplate(CategoryTemplate categoryTemplate);
        [Sql("DELETE FROM [CategoryTemplate] WHERE [CategoryID] = @categoryID AND [FieldName] = @fieldName")]
        void DeleteCategoryTemplateByID(Int32 categoryID, String fieldName);

        [Sql("SELECT [CityID], [Name], [Description], [ProvinceID], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [City] WHERE [CityID] = @cityID")]
        City GetCityByID(Int32 cityID);
        [Sql("SELECT [CityID], [Name], [Description], [ProvinceID], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [City]")]
        IList<City> GetAllCity();
        [Sql("UPDATE [City] SET [Name] = @Name, [Description] = @Description, [ProvinceID] = @ProvinceID, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [CityID] = @CityID")]
        void UpdateCity(City city);
        [Sql("INSERT INTO [City]([Name], [Description], [ProvinceID], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.CityID VALUES(@Name, @Description, @ProvinceID, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertCity(City city);
        [Sql("DELETE FROM [City] WHERE [CityID] = @cityID")]
        void DeleteCityByID(Int32 cityID);

        [Sql("SELECT [ErrorLogID], [ErrorMessage], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [ErrorLog] WHERE [ErrorLogID] = @errorLogID")]
        ErrorLog GetErrorLogByID(Int64 errorLogID);
        [Sql("SELECT [ErrorLogID], [ErrorMessage], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [ErrorLog]")]
        IList<ErrorLog> GetAllErrorLog();
        [Sql("UPDATE [ErrorLog] SET [ErrorMessage] = @ErrorMessage, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [ErrorLogID] = @ErrorLogID")]
        void UpdateErrorLog(ErrorLog errorLog);
        [Sql("INSERT INTO [ErrorLog]([ErrorMessage], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.ErrorLogID VALUES(@ErrorMessage, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertErrorLog(ErrorLog errorLog);
        [Sql("DELETE FROM [ErrorLog] WHERE [ErrorLogID] = @errorLogID")]
        void DeleteErrorLogByID(Int64 errorLogID);

        [Sql("SELECT [GroupID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Group] WHERE [GroupID] = @groupID")]
        Group GetGroupByID(Int32 groupID);
        [Sql("SELECT [GroupID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Group]")]
        IList<Group> GetAllGroup();
        [Sql("UPDATE [Group] SET [Name] = @Name, [Description] = @Description, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [GroupID] = @GroupID")]
        void UpdateGroup(Group group);
        [Sql("INSERT INTO [Group]([Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.GroupID VALUES(@Name, @Description, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertGroup(Group group);
        [Sql("DELETE FROM [Group] WHERE [GroupID] = @groupID")]
        void DeleteGroupByID(Int32 groupID);

        [Sql("SELECT [GroupAuthorizationID], [GroupID], [SiteMapID], [AllowAccess], [AllowAdd], [AllowUpdate], [AllowDelete], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [GroupAuthorization] WHERE [GroupAuthorizationID] = @groupAuthorizationID")]
        GroupAuthorization GetGroupAuthorizationByID(Int32 groupAuthorizationID);
        [Sql("SELECT [GroupAuthorizationID], [GroupID], [SiteMapID], [AllowAccess], [AllowAdd], [AllowUpdate], [AllowDelete], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [GroupAuthorization]")]
        IList<GroupAuthorization> GetAllGroupAuthorization();
        [Sql("UPDATE [GroupAuthorization] SET [GroupID] = @GroupID, [SiteMapID] = @SiteMapID, [AllowAccess] = @AllowAccess, [AllowAdd] = @AllowAdd, [AllowUpdate] = @AllowUpdate, [AllowDelete] = @AllowDelete, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [GroupAuthorizationID] = @GroupAuthorizationID")]
        void UpdateGroupAuthorization(GroupAuthorization groupAuthorization);
        [Sql("INSERT INTO [GroupAuthorization]([GroupID], [SiteMapID], [AllowAccess], [AllowAdd], [AllowUpdate], [AllowDelete], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.GroupAuthorizationID VALUES(@GroupID, @SiteMapID, @AllowAccess, @AllowAdd, @AllowUpdate, @AllowDelete, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertGroupAuthorization(GroupAuthorization groupAuthorization);
        [Sql("DELETE FROM [GroupAuthorization] WHERE [GroupAuthorizationID] = @groupAuthorizationID")]
        void DeleteGroupAuthorizationByID(Int32 groupAuthorizationID);

        [Sql("SELECT [KPKNLID], [Name], [Description], [Address], [Phone1], [Phone2], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [KPKNL] WHERE [KPKNLID] = @kpknlid")]
        KPKNL GetKPKNLByID(Int32 kpknlid);
        [Sql("SELECT [KPKNLID], [Name], [Description], [Address], [Phone1], [Phone2], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [KPKNL]")]
        IList<KPKNL> GetAllKPKNL();
        [Sql("UPDATE [KPKNL] SET [Name] = @Name, [Description] = @Description, [Address] = @Address, [Phone1] = @Phone1, [Phone2] = @Phone2, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [KPKNLID] = @Kpknlid")]
        void UpdateKPKNL(KPKNL kpknl);
        [Sql("INSERT INTO [KPKNL]([Name], [Description], [Address], [Phone1], [Phone2], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.KPKNLID VALUES(@Name, @Description, @Address, @Phone1, @Phone2, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertKPKNL(KPKNL kpknl);
        [Sql("DELETE FROM [KPKNL] WHERE [KPKNLID] = @kpknlid")]
        void DeleteKPKNLByID(Int32 kpknlid);

        [Sql("SELECT [MessageCategoryTypeID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [MessageCategoryType] WHERE [MessageCategoryTypeID] = @messageCategoryTypeID")]
        MessageCategoryType GetMessageCategoryTypeByID(Byte messageCategoryTypeID);
        [Sql("SELECT [MessageCategoryTypeID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [MessageCategoryType]")]
        IList<MessageCategoryType> GetAllMessageCategoryType();
        [Sql("UPDATE [MessageCategoryType] SET [Name] = @Name, [Description] = @Description, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [MessageCategoryTypeID] = @MessageCategoryTypeID")]
        void UpdateMessageCategoryType(MessageCategoryType messageCategoryType);
        [Sql("INSERT INTO [MessageCategoryType]([Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.MessageCategoryTypeID VALUES(@Name, @Description, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertMessageCategoryType(MessageCategoryType messageCategoryType);
        [Sql("DELETE FROM [MessageCategoryType] WHERE [MessageCategoryTypeID] = @messageCategoryTypeID")]
        void DeleteMessageCategoryTypeByID(Byte messageCategoryTypeID);

        [Sql("SELECT [MinMaxPriceID], [Name], [Description], [Value], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [MinMaxPrice] WHERE [MinMaxPriceID] = @minMaxPriceID")]
        MinMaxPrice GetMinMaxPriceByID(Int32 minMaxPriceID);
        [Sql("SELECT [MinMaxPriceID], [Name], [Description], [Value], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [MinMaxPrice]")]
        IList<MinMaxPrice> GetAllMinMaxPrice();
        [Sql("UPDATE [MinMaxPrice] SET [Name] = @Name, [Description] = @Description, [Value] = @Value, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [MinMaxPriceID] = @MinMaxPriceID")]
        void UpdateMinMaxPrice(MinMaxPrice minMaxPrice);
        [Sql("INSERT INTO [MinMaxPrice]([Name], [Description], [Value], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.MinMaxPriceID VALUES(@Name, @Description, @Value, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertMinMaxPrice(MinMaxPrice minMaxPrice);
        [Sql("DELETE FROM [MinMaxPrice] WHERE [MinMaxPriceID] = @minMaxPriceID")]
        void DeleteMinMaxPriceByID(Int32 minMaxPriceID);

        [Sql("SELECT [OwnershipDocumentTypeID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [OwnershipDocumentType] WHERE [OwnershipDocumentTypeID] = @ownershipDocumentTypeID")]
        OwnershipDocumentType GetOwnershipDocumentTypeByID(Int32 ownershipDocumentTypeID);
        [Sql("SELECT [OwnershipDocumentTypeID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [OwnershipDocumentType]")]
        IList<OwnershipDocumentType> GetAllOwnershipDocumentType();
        [Sql("UPDATE [OwnershipDocumentType] SET [Name] = @Name, [Description] = @Description, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [OwnershipDocumentTypeID] = @OwnershipDocumentTypeID")]
        void UpdateOwnershipDocumentType(OwnershipDocumentType ownershipDocumentType);
        [Sql("INSERT INTO [OwnershipDocumentType]([Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.OwnershipDocumentTypeID VALUES(@Name, @Description, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertOwnershipDocumentType(OwnershipDocumentType ownershipDocumentType);
        [Sql("DELETE FROM [OwnershipDocumentType] WHERE [OwnershipDocumentTypeID] = @ownershipDocumentTypeID")]
        void DeleteOwnershipDocumentTypeByID(Int32 ownershipDocumentTypeID);

        [Sql("SELECT [PageVisitID], [IPAddress], [URLReferrerSiteMapID], [SiteMapID], [AssetID], [AdditionalParams], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy], [MemberID], [CityID] FROM [PageVisit] WHERE [PageVisitID] = @pageVisitID")]
        PageVisit GetPageVisitByID(Int64 pageVisitID);
        [Sql("SELECT [PageVisitID], [IPAddress], [URLReferrerSiteMapID], [SiteMapID], [AssetID], [AdditionalParams], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy], [MemberID], [CityID] FROM [PageVisit]")]
        IList<PageVisit> GetAllPageVisit();
        [Sql("UPDATE [PageVisit] SET [IPAddress] = @IPAddress, [URLReferrerSiteMapID] = @URLReferrerSiteMapID, [SiteMapID] = @SiteMapID, [AssetID] = @AssetID, [AdditionalParams] = @AdditionalParams, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy, [MemberID] = @MemberID, [CityID] = @CityID WHERE [PageVisitID] = @PageVisitID")]
        void UpdatePageVisit(PageVisit pageVisit);
        [Sql("INSERT INTO [PageVisit]([IPAddress], [URLReferrerSiteMapID], [SiteMapID], [AssetID], [AdditionalParams], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy], [MemberID], [CityID]) OUTPUT Inserted.PageVisitID VALUES(@IPAddress, @URLReferrerSiteMapID, @SiteMapID, @AssetID, @AdditionalParams, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy, @MemberID, @CityID)")]
        void InsertPageVisit(PageVisit pageVisit);
        [Sql("DELETE FROM [PageVisit] WHERE [PageVisitID] = @pageVisitID")]
        void DeletePageVisitByID(Int64 pageVisitID);

        [Sql("SELECT [ProvinceID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Province] WHERE [ProvinceID] = @provinceID")]
        Province GetProvinceByID(Int32 provinceID);
        [Sql("SELECT [ProvinceID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Province]")]
        IList<Province> GetAllProvince();
        [Sql("UPDATE [Province] SET [Name] = @Name, [Description] = @Description, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [ProvinceID] = @ProvinceID")]
        void UpdateProvince(Province province);
        [Sql("INSERT INTO [Province]([Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.ProvinceID VALUES(@Name, @Description, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertProvince(Province province);
        [Sql("DELETE FROM [Province] WHERE [ProvinceID] = @provinceID")]
        void DeleteProvinceByID(Int32 provinceID);

        [Sql("SELECT [SegmentID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Segment] WHERE [SegmentID] = @segmentID")]
        Segment GetSegmentByID(String segmentID);
        [Sql("SELECT [SegmentID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Segment]")]
        IList<Segment> GetAllSegment();
        [Sql("UPDATE [Segment] SET [Name] = @Name, [Description] = @Description, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [SegmentID] = @SegmentID")]
        void UpdateSegment(Segment segment);
        [Sql("INSERT INTO [Segment]([SegmentID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) VALUES(@SegmentID, @Name, @Description, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertSegment(Segment segment);
        [Sql("DELETE FROM [Segment] WHERE [SegmentID] = @segmentID")]
        void DeleteSegmentByID(String segmentID);

        [Sql("SELECT [SiteMapID], [MenuName], [SiteMapParentID], [Description], [ActionName], [ControllerName], [Level], [HaveChildren], [IsHiddenMenu], [Order], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [SiteMap] WHERE [SiteMapID] = @siteMapID")]
        SiteMap GetSiteMapByID(String siteMapID);
        [Sql("SELECT [SiteMapID], [MenuName], [SiteMapParentID], [Description], [ActionName], [ControllerName], [Level], [HaveChildren], [IsHiddenMenu], [Order], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [SiteMap]")]
        IList<SiteMap> GetAllSiteMap();
        [Sql("UPDATE [SiteMap] SET [MenuName] = @MenuName, [SiteMapParentID] = @SiteMapParentID, [Description] = @Description, [ActionName] = @ActionName, [ControllerName] = @ControllerName, [Level] = @Level, [HaveChildren] = @HaveChildren, [IsHiddenMenu] = @IsHiddenMenu, [Order] = @Order, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [SiteMapID] = @SiteMapID")]
        void UpdateSiteMap(SiteMap siteMap);
        [Sql("INSERT INTO [SiteMap]([SiteMapID], [MenuName], [SiteMapParentID], [Description], [ActionName], [ControllerName], [Level], [HaveChildren], [IsHiddenMenu], [Order], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy])  VALUES(@SiteMapID, @MenuName, @SiteMapParentID, @Description, @ActionName, @ControllerName, @Level, @HaveChildren, @IsHiddenMenu, @Order, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertSiteMap(SiteMap siteMap);
        [Sql("DELETE FROM [SiteMap] WHERE [SiteMapID] = @siteMapID")]
        void DeleteSiteMapByID(String siteMapID);

        [Sql("SELECT [TotalVisitorID], [TotalVisitorCount], [TotalTodayVisitorCount], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [TotalVisitor] WHERE [TotalVisitorID] = @totalVisitorID")]
        TotalVisitor GetTotalVisitorByID(Byte totalVisitorID);
        [Sql("SELECT [TotalVisitorID], [TotalVisitorCount], [TotalTodayVisitorCount], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [TotalVisitor]")]
        IList<TotalVisitor> GetAllTotalVisitor();
        [Sql("UPDATE [TotalVisitor] SET [TotalVisitorCount] = @TotalVisitorCount, [TotalTodayVisitorCount] = @TotalTodayVisitorCount, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [TotalVisitorID] = @TotalVisitorID")]
        void UpdateTotalVisitor(TotalVisitor totalVisitor);
        [Sql("INSERT INTO [TotalVisitor]([TotalVisitorID], [TotalVisitorCount], [TotalTodayVisitorCount], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy])  VALUES(@TotalVisitorID, @TotalVisitorCount, @TotalTodayVisitorCount, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertTotalVisitor(TotalVisitor totalVisitor);
        [Sql("DELETE FROM [TotalVisitor] WHERE [TotalVisitorID] = @totalVisitorID")]
        void DeleteTotalVisitorByID(Byte totalVisitorID);

        [Sql("SELECT [UserID], [Username], [FullName], [PasswordSalt], [HashPassword], [Email], [BranchID], [FailedLoginAttempt], [IsLocked], [LastLoggedIn], [LastPasswordChanged], [LastActivity], [IsLoggedIn], [SessionID], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [User] WHERE [UserID] = @userID")]
        User GetUserByID(Int32 userID);
        [Sql("SELECT [UserID], [Username], [FullName], [PasswordSalt], [HashPassword], [Email], [BranchID], [FailedLoginAttempt], [IsLocked], [LastLoggedIn], [LastPasswordChanged], [LastActivity], [IsLoggedIn], [SessionID], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [User]")]
        IList<User> GetAllUser();
        [Sql("UPDATE [User] SET [Username] = @Username, [FullName] = @FullName, [PasswordSalt] = @PasswordSalt, [HashPassword] = @HashPassword, [Email] = @Email, [BranchID] = @BranchID, [FailedLoginAttempt] = @FailedLoginAttempt, [IsLocked] = @IsLocked, [LastLoggedIn] = @LastLoggedIn, [LastPasswordChanged] = @LastPasswordChanged, [LastActivity] = @LastActivity, [IsLoggedIn] = @IsLoggedIn, [SessionID] = @SessionID, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [UserID] = @UserID")]
        void UpdateUser(User user);
        [Sql("INSERT INTO [User]([Username], [FullName], [PasswordSalt], [HashPassword], [Email], [BranchID], [FailedLoginAttempt], [IsLocked], [LastLoggedIn], [LastPasswordChanged], [LastActivity], [IsLoggedIn], [SessionID], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.UserID VALUES(@Username, @FullName, @PasswordSalt, @HashPassword, @Email, @BranchID, @FailedLoginAttempt, @IsLocked, @LastLoggedIn, @LastPasswordChanged, @LastActivity, @IsLoggedIn, @SessionID, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertUser(User user);
        [Sql("DELETE FROM [User] WHERE [UserID] = @userID")]
        void DeleteUserByID(Int32 userID);

        [Sql("SELECT [UserActivityID], [UserActivityTypeID], [ExtraDescription], [ReferenceID], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [UserActivity] WHERE [UserActivityID] = @userActivityID")]
        UserActivity GetUserActivityByID(Int64 userActivityID);
        [Sql("SELECT [UserActivityID], [UserActivityTypeID], [ExtraDescription], [ReferenceID], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [UserActivity]")]
        IList<UserActivity> GetAllUserActivity();
        [Sql("UPDATE [UserActivity] SET [UserActivityTypeID] = @UserActivityTypeID, [ExtraDescription] = @ExtraDescription, [ReferenceID] = @ReferenceID, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [UserActivityID] = @UserActivityID")]
        void UpdateUserActivity(UserActivity userActivity);
        [Sql("INSERT INTO [UserActivity]([UserActivityTypeID], [ExtraDescription], [ReferenceID], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.UserActivityID VALUES(@UserActivityTypeID, @ExtraDescription, @ReferenceID, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertUserActivity(UserActivity userActivity);
        [Sql("DELETE FROM [UserActivity] WHERE [UserActivityID] = @userActivityID")]
        void DeleteUserActivityByID(Int64 userActivityID);

        [Sql("SELECT [UserActivityTypeID], [ActivityName], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [UserActivityType] WHERE [UserActivityTypeID] = @userActivityTypeID")]
        UserActivityType GetUserActivityTypeByID(String userActivityTypeID);
        [Sql("SELECT [UserActivityTypeID], [ActivityName], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [UserActivityType]")]
        IList<UserActivityType> GetAllUserActivityType();
        [Sql("UPDATE [UserActivityType] SET [ActivityName] = @ActivityName, [Description] = @Description, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [UserActivityTypeID] = @UserActivityTypeID")]
        void UpdateUserActivityType(UserActivityType userActivityType);
        [Sql("INSERT INTO [UserActivityType]([UserActivityTypeID], [ActivityName], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy])  VALUES(@UserActivityTypeID, @ActivityName, @Description, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertUserActivityType(UserActivityType userActivityType);
        [Sql("DELETE FROM [UserActivityType] WHERE [UserActivityTypeID] = @userActivityTypeID")]
        void DeleteUserActivityTypeByID(String userActivityTypeID);

        [Sql("SELECT [UserGroupMappingID], [GroupID], [UserID], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [UserGroupMapping] WHERE [UserGroupMappingID] = @userGroupMappingID")]
        UserGroupMapping GetUserGroupMappingByID(Int32 userGroupMappingID);
        [Sql("SELECT [UserGroupMappingID], [GroupID], [UserID], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [UserGroupMapping]")]
        IList<UserGroupMapping> GetAllUserGroupMapping();
        [Sql("UPDATE [UserGroupMapping] SET [GroupID] = @GroupID, [UserID] = @UserID, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [UserGroupMappingID] = @UserGroupMappingID")]
        void UpdateUserGroupMapping(UserGroupMapping userGroupMapping);
        [Sql("INSERT INTO [UserGroupMapping]([GroupID], [UserID], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.UserGroupMappingID VALUES(@GroupID, @UserID, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertUserGroupMapping(UserGroupMapping userGroupMapping);
        [Sql("DELETE FROM [UserGroupMapping] WHERE [UserGroupMappingID] = @userGroupMappingID")]
        void DeleteUserGroupMappingByID(Int32 userGroupMappingID);

        [Sql("SELECT [WebContentTypeID], [Content], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [WebContent] WHERE [WebContentTypeID] = @webContentTypeID")]
        WebContent GetWebContentByID(Int32 webContentTypeID);
        [Sql("SELECT [WebContentTypeID], [Content], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [WebContent]")]
        IList<WebContent> GetAllWebContent();
        [Sql("UPDATE [WebContent] SET [Content] = @Content, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [WebContentTypeID] = @WebContentTypeID")]
        void UpdateWebContent(WebContent webContent);
        [Sql("INSERT INTO [WebContent]([WebContentTypeID], [Content], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy])  VALUES(@WebContentTypeID, @Content, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertWebContent(WebContent webContent);
        [Sql("DELETE FROM [WebContent] WHERE [WebContentTypeID] = @webContentTypeID")]
        void DeleteWebContentByID(Int32 webContentTypeID);

        [Sql("SELECT [WebContentTypeID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [WebContentType] WHERE [WebContentTypeID] = @webContentTypeID")]
        WebContentType GetWebContentTypeByID(Int32 webContentTypeID);
        [Sql("SELECT [WebContentTypeID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [WebContentType]")]
        IList<WebContentType> GetAllWebContentType();
        [Sql("UPDATE [WebContentType] SET [Name] = @Name, [Description] = @Description, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [WebContentTypeID] = @WebContentTypeID")]
        void UpdateWebContentType(WebContentType webContentType);
        [Sql("INSERT INTO [WebContentType]([WebContentTypeID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy])  VALUES(@WebContentTypeID, @Name, @Description, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertWebContentType(WebContentType webContentType);
        [Sql("DELETE FROM [WebContentType] WHERE [WebContentTypeID] = @webContentTypeID")]
        void DeleteWebContentTypeByID(Int32 webContentTypeID);

        [Sql("SELECT [SystemSettingID], [Value], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [SystemSetting] WHERE [SystemSettingID] = @systemSettingID")]
        SystemSetting GetSystemSettingByID(String systemSettingID);
        [Sql("SELECT [SystemSettingID], [Value], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [SystemSetting] WHERE [IsActive] = 1")]
        IList<SystemSetting> GetAllActiveSystemSettings();
        [Sql("UPDATE [SystemSetting] SET [Value] = @Value, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [SystemSettingID] = @SystemSettingID")]
        void UpdateSystemSetting(SystemSetting systemSetting);
        [Sql("INSERT INTO [SystemSetting]([SystemSettingID], [Value], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy])  VALUES(@SystemSettingID, @Value, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertSystemSetting(SystemSetting systemSetting);
        [Sql("DELETE FROM [SystemSetting] WHERE [SystemSettingID] = @systemSettingID")]
        void DeleteSystemSettingByID(String systemSettingID);

        [Sql("SELECT [PasswordHistoryID], [UserID], [HashPassword], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [PasswordHistory] WHERE [PasswordHistoryID] = @passwordHistoryID")]
        PasswordHistory GetPasswordHistoryByID(Int64 passwordHistoryID);
        [Sql("SELECT [PasswordHistoryID], [UserID], [HashPassword], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [PasswordHistory]")]
        IList<PasswordHistory> GetAllPasswordHistory();
        [Sql("UPDATE [PasswordHistory] SET [UserID] = @UserID, [HashPassword] = @HashPassword, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [PasswordHistoryID] = @PasswordHistoryID")]
        void UpdatePasswordHistory(PasswordHistory passwordHistory);
        [Sql("INSERT INTO [PasswordHistory]([UserID], [HashPassword], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.PasswordHistoryID VALUES(@UserID, @HashPassword, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertPasswordHistory(PasswordHistory passwordHistory);
        [Sql("DELETE FROM [PasswordHistory] WHERE [PasswordHistoryID] = @passwordHistoryID")]
        void DeletePasswordHistoryByID(Int64 passwordHistoryID);

        #region Member

        [Sql("SELECT [MemberID], [FullName], [Email], [PlaceOfBirth], [DateOfBirth], [MobilePhoneNumber], [Address], [ProvinceID], [CityID], [IdentificationNumber], [PasswordSalt], [HashPassword], [ActivationToken], [IsLocked], [SessionID], [LastLoggedIn], [LastPasswordChanged], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [Member] WHERE [MemberID] = @memberID")]
        Member GetMemberByID(long memberID);
        [Sql("SELECT [MemberID], [FullName], [Email], [PlaceOfBirth], [DateOfBirth], [MobilePhoneNumber], [Address], [ProvinceID], [CityID], [IdentificationNumber], [PasswordSalt], [HashPassword], [ActivationToken], [IsLocked], [SessionID], [LastLoggedIn], [LastPasswordChanged], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [Member] WHERE [MemberID] = @memberID AND [IsActive] = 1")]
        Member GetActiveMemberByID(long memberID);
        [Sql("SELECT [MemberID], [FullName], [Email], [PlaceOfBirth], [DateOfBirth], [MobilePhoneNumber], [Address], [ProvinceID], [CityID], [IdentificationNumber], [PasswordSalt], [HashPassword], [ActivationToken], [IsLocked], [SessionID], [LastLoggedIn], [LastPasswordChanged], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [Member]")]
        IList<Member> GetAllMember();
        [Sql("SELECT [MemberID], [FullName], [Email], [PlaceOfBirth], [DateOfBirth], [MobilePhoneNumber], [Address], [ProvinceID], [CityID], [IdentificationNumber], [PasswordSalt], [HashPassword], [ActivationToken], [IsLocked], [SessionID], [LastLoggedIn], [LastPasswordChanged], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [Member] WHERE [IsActive] = 1")]
        IList<Member> GetAllActiveMember();
        [Sql("UPDATE [Member] SET [FullName] = @FullName, [Email] = @Email, [PlaceOfBirth] = @PlaceOfBirth, [DateOfBirth] = @DateOfBirth, [MobilePhoneNumber] = @MobilePhoneNumber, [Address] = @Address, [ProvinceID] = @ProvinceID, [CityID] = @CityID, [IdentificationNumber] = @IdentificationNumber, [PasswordSalt] = @PasswordSalt, [HashPassword] = @HashPassword, [ActivationToken] = @ActivationToken, [IsLocked] = @IsLocked, [SessionID] = @SessionID, [LastLoggedIn] = @LastLoggedIn, [LastPasswordChanged] = @LastPasswordChanged, [IsActive] = @IsActive, [CreatedBy] = @CreatedBy, [DateTimeCreated] = @DateTimeCreated, [LastUpdatedBy] = @LastUpdatedBy, [DateTimeUpdated] = @DateTimeUpdated WHERE [MemberID] = @MemberID")]
        void UpdateMember(Member member);
        [Sql("INSERT INTO [Member]([FullName], [Email], [PlaceOfBirth], [DateOfBirth], [MobilePhoneNumber], [Address], [ProvinceID], [CityID], [IdentificationNumber], [PasswordSalt], [HashPassword], [ActivationToken], [IsLocked], [SessionID], [LastLoggedIn], [LastPasswordChanged], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated]) OUTPUT Inserted.MemberID VALUES(@FullName, @Email, @PlaceOfBirth, @DateOfBirth, @MobilePhoneNumber, @Address, @ProvinceID, @CityID, @IdentificationNumber, @PasswordSalt, @HashPassword, @ActivationToken, @IsLocked, @SessionID, @LastLoggedIn, @LastPasswordChanged, @IsActive, @CreatedBy, @DateTimeCreated, @LastUpdatedBy, @DateTimeUpdated)")]
        void InsertMember(Member member);
        [Sql("DELETE FROM [Member] WHERE [MemberID] = @memberID")]
        void DeleteMemberByID(long memberID);

        #endregion

        #region MemberAccount

        [Sql("SELECT [MemberAccountID], [MemberID], [SocialToken], [SocialID], [SocialUsername], [SocialURL], [Source], [IsActivated], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberAccount] WHERE [MemberAccountID] = @memberAccountID")]
        MemberAccount GetMemberAccountByID(long memberAccountID);
        [Sql("SELECT [MemberAccountID], [MemberID], [SocialToken], [SocialID], [SocialUsername], [SocialURL], [Source], [IsActivated], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberAccount] WHERE [MemberAccountID] = @memberAccountID AND [IsActive] = 1")]
        MemberAccount GetActiveMemberAccountByID(long memberAccountID);
        [Sql("SELECT [MemberAccountID], [MemberID], [SocialToken], [SocialID], [SocialUsername], [SocialURL], [Source], [IsActivated], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberAccount]")]
        IList<MemberAccount> GetAllMemberAccount();
        [Sql("SELECT [MemberAccountID], [MemberID], [SocialToken], [SocialID], [SocialUsername], [SocialURL], [Source], [IsActivated], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberAccount] WHERE [IsActive] = 1")]
        IList<MemberAccount> GetAllActiveMemberAccount();
        [Sql("UPDATE [MemberAccount] SET [MemberID] = @MemberID, [SocialToken] = @SocialToken, [SocialID] = @SocialID, [SocialUsername] = @SocialUsername, [SocialURL] = @SocialURL, [Source] = @Source, [IsActivated] = @IsActivated, [IsActive] = @IsActive, [CreatedBy] = @CreatedBy, [DateTimeCreated] = @DateTimeCreated, [LastUpdatedBy] = @LastUpdatedBy, [DateTimeUpdated] = @DateTimeUpdated WHERE [MemberAccountID] = @MemberAccountID")]
        void UpdateMemberAccount(MemberAccount memberAccount);
        [Sql("INSERT INTO [MemberAccount]([MemberID], [SocialToken], [SocialID], [SocialUsername], [SocialURL], [Source], [IsActivated], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated]) OUTPUT Inserted.MemberAccountID VALUES(@MemberID, @SocialToken, @SocialID, @SocialUsername, @SocialURL, @Source, @IsActivated, @IsActive, @CreatedBy, @DateTimeCreated, @LastUpdatedBy, @DateTimeUpdated)")]
        void InsertMemberAccount(MemberAccount memberAccount);
        [Sql("DELETE FROM [MemberAccount] WHERE [MemberAccountID] = @memberAccountID")]
        void DeleteMemberAccountByID(long memberAccountID);

        #endregion

        #region MemberCategoryPreference

        [Sql("SELECT [MemberCategoryPreferenceID], [MemberPreferenceID], [CategoryID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberCategoryPreference] WHERE [MemberCategoryPreferenceID] = @memberCategoryPreferenceID")]
        MemberCategoryPreference GetMemberCategoryPreferenceByID(long memberCategoryPreferenceID);
        [Sql("SELECT [MemberCategoryPreferenceID], [MemberPreferenceID], [CategoryID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberCategoryPreference] WHERE [MemberCategoryPreferenceID] = @memberCategoryPreferenceID AND [IsActive] = 1")]
        MemberCategoryPreference GetActiveMemberCategoryPreferenceByID(long memberCategoryPreferenceID);
        [Sql("SELECT [MemberCategoryPreferenceID], [MemberPreferenceID], [CategoryID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberCategoryPreference]")]
        IList<MemberCategoryPreference> GetAllMemberCategoryPreference();
        [Sql("SELECT [MemberCategoryPreferenceID], [MemberPreferenceID], [CategoryID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberCategoryPreference] WHERE [IsActive] = 1")]
        IList<MemberCategoryPreference> GetAllActiveMemberCategoryPreference();
        [Sql("UPDATE [MemberCategoryPreference] SET [MemberPreferenceID] = @MemberPreferenceID, [CategoryID] = @CategoryID, [IsActive] = @IsActive, [CreatedBy] = @CreatedBy, [DateTimeCreated] = @DateTimeCreated, [LastUpdatedBy] = @LastUpdatedBy, [DateTimeUpdated] = @DateTimeUpdated WHERE [MemberCategoryPreferenceID] = @MemberCategoryPreferenceID")]
        void UpdateMemberCategoryPreference(MemberCategoryPreference memberCategoryPreference);
        [Sql("INSERT INTO [MemberCategoryPreference]([MemberPreferenceID], [CategoryID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated]) OUTPUT Inserted.MemberCategoryPreferenceID VALUES(@MemberPreferenceID, @CategoryID, @IsActive, @CreatedBy, @DateTimeCreated, @LastUpdatedBy, @DateTimeUpdated)")]
        void InsertMemberCategoryPreference(MemberCategoryPreference memberCategoryPreference);
        [Sql("DELETE FROM [MemberCategoryPreference] WHERE [MemberCategoryPreferenceID] = @memberCategoryPreferenceID")]
        void DeleteMemberCategoryPreferenceByID(long memberCategoryPreferenceID);

        #endregion

        #region MemberCityPreference

        [Sql("SELECT [MemberCityPreferenceID], [MemberPreferenceID], [CityID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberCityPreference] WHERE [MemberCityPreferenceID] = @memberCityPreferenceID")]
        MemberCityPreference GetMemberCityPreferenceByID(long memberCityPreferenceID);
        [Sql("SELECT [MemberCityPreferenceID], [MemberPreferenceID], [CityID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberCityPreference] WHERE [MemberCityPreferenceID] = @memberCityPreferenceID AND [IsActive] = 1")]
        MemberCityPreference GetActiveMemberCityPreferenceByID(long memberCityPreferenceID);
        [Sql("SELECT [MemberCityPreferenceID], [MemberPreferenceID], [CityID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberCityPreference]")]
        IList<MemberCityPreference> GetAllMemberCityPreference();
        [Sql("SELECT [MemberCityPreferenceID], [MemberPreferenceID], [CityID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberCityPreference] WHERE [IsActive] = 1")]
        IList<MemberCityPreference> GetAllActiveMemberCityPreference();
        [Sql("UPDATE [MemberCityPreference] SET [MemberPreferenceID] = @MemberPreferenceID, [CityID] = @CityID, [IsActive] = @IsActive, [CreatedBy] = @CreatedBy, [DateTimeCreated] = @DateTimeCreated, [LastUpdatedBy] = @LastUpdatedBy, [DateTimeUpdated] = @DateTimeUpdated WHERE [MemberCityPreferenceID] = @MemberCityPreferenceID")]
        void UpdateMemberCityPreference(MemberCityPreference memberCityPreference);
        [Sql("INSERT INTO [MemberCityPreference]([MemberPreferenceID], [CityID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated]) OUTPUT Inserted.MemberCityPreferenceID VALUES(@MemberPreferenceID, @CityID, @IsActive, @CreatedBy, @DateTimeCreated, @LastUpdatedBy, @DateTimeUpdated)")]
        void InsertMemberCityPreference(MemberCityPreference memberCityPreference);
        [Sql("DELETE FROM [MemberCityPreference] WHERE [MemberCityPreferenceID] = @memberCityPreferenceID")]
        void DeleteMemberCityPreferenceByID(long memberCityPreferenceID);

        #endregion

        #region MemberFavorite

        [Sql("SELECT [MemberFavoriteID], [MemberID], [AssetID], [IsFavorite], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberFavorite] WHERE [MemberFavoriteID] = @memberFavoriteID")]
        MemberFavorite GetMemberFavoriteByID(long memberFavoriteID);
        [Sql("SELECT [MemberFavoriteID], [MemberID], [AssetID], [IsFavorite], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberFavorite] WHERE [MemberFavoriteID] = @memberFavoriteID AND [IsActive] = 1")]
        MemberFavorite GetActiveMemberFavoriteByID(long memberFavoriteID);
        [Sql("SELECT [MemberFavoriteID], [MemberID], [AssetID], [IsFavorite], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberFavorite]")]
        IList<MemberFavorite> GetAllMemberFavorite();
        [Sql("SELECT [MemberFavoriteID], [MemberID], [AssetID], [IsFavorite], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberFavorite] WHERE [IsActive] = 1")]
        IList<MemberFavorite> GetAllActiveMemberFavorite();
        [Sql("UPDATE [MemberFavorite] SET [MemberID] = @MemberID, [AssetID] = @AssetID, [IsFavorite] = @IsFavorite, [IsActive] = @IsActive, [CreatedBy] = @CreatedBy, [DateTimeCreated] = @DateTimeCreated, [LastUpdatedBy] = @LastUpdatedBy, [DateTimeUpdated] = @DateTimeUpdated WHERE [MemberFavoriteID] = @MemberFavoriteID")]
        void UpdateMemberFavorite(MemberFavorite memberFavorite);
        [Sql("INSERT INTO [MemberFavorite]([MemberID], [AssetID], [IsFavorite], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated]) OUTPUT Inserted.MemberFavoriteID VALUES(@MemberID, @AssetID, @IsFavorite, @IsActive, @CreatedBy, @DateTimeCreated, @LastUpdatedBy, @DateTimeUpdated)")]
        void InsertMemberFavorite(MemberFavorite memberFavorite);
        [Sql("DELETE FROM [MemberFavorite] WHERE [MemberFavoriteID] = @memberFavoriteID")]
        void DeleteMemberFavoriteByID(long memberFavoriteID);

        #endregion

        #region MemberPreference

        [Sql("SELECT [MemberPreferenceID], [MemberID], [MinPriceID], [MaxPriceID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberPreference] WHERE [MemberPreferenceID] = @memberPreferenceID")]
        MemberPreference GetMemberPreferenceByID(long memberPreferenceID);
        [Sql("SELECT [MemberPreferenceID], [MemberID], [MinPriceID], [MaxPriceID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberPreference] WHERE [MemberPreferenceID] = @memberPreferenceID AND [IsActive] = 1")]
        MemberPreference GetActiveMemberPreferenceByID(long memberPreferenceID);
        [Sql("SELECT [MemberPreferenceID], [MemberID], [MinPriceID], [MaxPriceID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberPreference]")]
        IList<MemberPreference> GetAllMemberPreference();
        [Sql("SELECT [MemberPreferenceID], [MemberID], [MinPriceID], [MaxPriceID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberPreference] WHERE [IsActive] = 1")]
        IList<MemberPreference> GetAllActiveMemberPreference();
        [Sql("UPDATE [MemberPreference] SET [MemberID] = @MemberID, [MinPriceID] = @MinPriceID, [MaxPriceID] = @MaxPriceID, [IsActive] = @IsActive, [CreatedBy] = @CreatedBy, [DateTimeCreated] = @DateTimeCreated, [LastUpdatedBy] = @LastUpdatedBy, [DateTimeUpdated] = @DateTimeUpdated WHERE [MemberPreferenceID] = @MemberPreferenceID")]
        void UpdateMemberPreference(MemberPreference memberPreference);
        [Sql("INSERT INTO [MemberPreference]([MemberID], [MinPriceID], [MaxPriceID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated]) OUTPUT Inserted.MemberPreferenceID VALUES(@MemberID, @MinPriceID, @MaxPriceID, @IsActive, @CreatedBy, @DateTimeCreated, @LastUpdatedBy, @DateTimeUpdated)")]
        void InsertMemberPreference(MemberPreference memberPreference);
        [Sql("DELETE FROM [MemberPreference] WHERE [MemberPreferenceID] = @memberPreferenceID")]
        void DeleteMemberPreferenceByID(long memberPreferenceID);

        #endregion

        #region MobileAuthToken

        [Sql("SELECT [MobileAuthTokenID], [MemberID], [Token], [Salt], [ValidUntilDateTime], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MobileAuthToken] WHERE [MobileAuthTokenID] = @mobileAuthTokenID")]
        MobileAuthToken GetMobileAuthTokenByID(long mobileAuthTokenID);
        [Sql("SELECT [MobileAuthTokenID], [MemberID], [Token], [Salt], [ValidUntilDateTime], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MobileAuthToken] WHERE [MobileAuthTokenID] = @mobileAuthTokenID AND [IsActive] = 1")]
        MobileAuthToken GetActiveMobileAuthTokenByID(long mobileAuthTokenID);
        [Sql("SELECT [MobileAuthTokenID], [MemberID], [Token], [Salt], [ValidUntilDateTime], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MobileAuthToken]")]
        IList<MobileAuthToken> GetAllMobileAuthToken();
        [Sql("SELECT [MobileAuthTokenID], [MemberID], [Token], [Salt], [ValidUntilDateTime], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MobileAuthToken] WHERE [IsActive] = 1")]
        IList<MobileAuthToken> GetAllActiveMobileAuthToken();
        [Sql("UPDATE [MobileAuthToken] SET [MemberID] = @MemberID, [Token] = @Token, [Salt] = @Salt, [ValidUntilDateTime] = @ValidUntilDateTime, [IsActive] = @IsActive, [CreatedBy] = @CreatedBy, [DateTimeCreated] = @DateTimeCreated, [LastUpdatedBy] = @LastUpdatedBy, [DateTimeUpdated] = @DateTimeUpdated WHERE [MobileAuthTokenID] = @MobileAuthTokenID")]
        void UpdateMobileAuthToken(MobileAuthToken mobileAuthToken);
        [Sql("INSERT INTO [MobileAuthToken]([MemberID], [Token], [Salt], [ValidUntilDateTime], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated]) OUTPUT Inserted.MobileAuthTokenID VALUES(@MemberID, @Token, @Salt, @ValidUntilDateTime, @IsActive, @CreatedBy, @DateTimeCreated, @LastUpdatedBy, @DateTimeUpdated)")]
        void InsertMobileAuthToken(MobileAuthToken mobileAuthToken);
        [Sql("DELETE FROM [MobileAuthToken] WHERE [MobileAuthTokenID] = @mobileAuthTokenID")]
        void DeleteMobileAuthTokenByID(long mobileAuthTokenID);

        #endregion

        #region ResetPasswordRequest

        [Sql("SELECT [ResetPasswordRequestID], [Email], [MemberID], [RequestToken], [IsUsed], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [ResetPasswordRequest] WHERE [ResetPasswordRequestID] = @resetPasswordRequestID")]
        ResetPasswordRequest GetResetPasswordRequestByID(long resetPasswordRequestID);
        [Sql("SELECT [ResetPasswordRequestID], [Email], [MemberID], [RequestToken], [IsUsed], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [ResetPasswordRequest] WHERE [ResetPasswordRequestID] = @resetPasswordRequestID AND [IsActive] = 1")]
        ResetPasswordRequest GetActiveResetPasswordRequestByID(long resetPasswordRequestID);
        [Sql("SELECT [ResetPasswordRequestID], [Email], [MemberID], [RequestToken], [IsUsed], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [ResetPasswordRequest]")]
        IList<ResetPasswordRequest> GetAllResetPasswordRequest();
        [Sql("SELECT [ResetPasswordRequestID], [Email], [MemberID], [RequestToken], [IsUsed], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [ResetPasswordRequest] WHERE [IsActive] = 1")]
        IList<ResetPasswordRequest> GetAllActiveResetPasswordRequest();
        [Sql("UPDATE [ResetPasswordRequest] SET [Email] = @Email, [MemberID] = @MemberID, [RequestToken] = @RequestToken, [IsUsed] = @IsUsed, [IsActive] = @IsActive, [CreatedBy] = @CreatedBy, [DateTimeCreated] = @DateTimeCreated, [LastUpdatedBy] = @LastUpdatedBy, [DateTimeUpdated] = @DateTimeUpdated WHERE [ResetPasswordRequestID] = @ResetPasswordRequestID")]
        void UpdateResetPasswordRequest(ResetPasswordRequest resetPasswordRequest);
        [Sql("INSERT INTO [ResetPasswordRequest]([Email], [MemberID], [RequestToken], [IsUsed], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated]) OUTPUT Inserted.ResetPasswordRequestID VALUES(@Email, @MemberID, @RequestToken, @IsUsed, @IsActive, @CreatedBy, @DateTimeCreated, @LastUpdatedBy, @DateTimeUpdated)")]
        void InsertResetPasswordRequest(ResetPasswordRequest resetPasswordRequest);
        [Sql("DELETE FROM [ResetPasswordRequest] WHERE [ResetPasswordRequestID] = @resetPasswordRequestID")]
        void DeleteResetPasswordRequestByID(long resetPasswordRequestID);

        #endregion

        #region Get Name 
        [Sql(@"SELECT Name FROM Category WHERE IsActive = 1 AND CategoryID = @categoryID")]
        String GetCategoryNameByCategoryID(Int32 categoryID);

        [Sql(@"SELECT Name FROM Province WHERE IsActive = 1 AND ProvinceID = @provinceID")]
        String GetProvinceNameByProvinceID(Int32 provinceID);

        [Sql(@"SELECT Name FROM City WHERE IsActive = 1 AND CityID = @cityID")]
        String GetCityNameByCityID(Int32 cityID);

        [Sql(@"SELECT Name FROM Segment WHERE  IsActive = 1 AND SegmentID = @segmentID")]
        String GetSegmentNameBySegmentID(String segmentID);

        [Sql(@"SELECT Name FROM KPKNL WHERE IsActive = 1 AND KPKNLID =  @kPKNLID")]
        String GetKPKNLNameByKPKNLID(Int32 kPKNLID);

        [Sql(@"SELECT Name FROM OwnershipDocumentType WHERE IsActive = 1 AND OwnershipDocumentTypeID =@ownershipDocumentType")]
        String GetOwnershipDocumentTypeNameByID(Int32 ownershipDocumentType);

        [Sql(@"SELECT Name FROM AuctionHall WHERE IsActive = 1 AND AuctionHallID =@auctionHallID")]
        String GetAuctionHallNameByID(Int32 auctionHallID);

        [Sql(@"SELECT Name FROM Branch WHERE IsActive = 1 AND BranchID =@branchID")]
        String GetBranchNameByID(Int32 branchID);

        [Sql(@"SELECT CONVERT(VARCHAR(MAX), ScheduleDate, 106) + ' ' + CONVERT(VARCHAR(MAX), ScheduleTime, 8) + 
	                CASE [Timezone] 
		                WHEN 1 THEN  ' WIB'
		                WHEN 2 THEN  ' WIT'
		                WHEN 3 THEN  ' WITA'
	                END As Name
                FROM AuctionSchedule 
                WHERE IsActive = 1 AND AuctionScheduleID = @auctionScheduleID")]
        String GetAuctionScheduleNameByID(long auctionScheduleID);
        #endregion

        #region MessageStatus

        [Sql("SELECT [MessageStatusID], [Name], [Description], [IsComplete], [IsActive], [DateTimeCreated], [DateTimeUpdated], [DateTimeInActive], [CreatedBy], [LastUpdatedBy] FROM [MessageStatus] WHERE [MessageStatusID] = @messageStatusID")]
        MessageStatus GetMessageStatusByID(int messageStatusID);
        [Sql("SELECT [MessageStatusID], [Name], [Description], [IsComplete], [IsActive], [DateTimeCreated], [DateTimeUpdated], [DateTimeInActive], [CreatedBy], [LastUpdatedBy] FROM [MessageStatus] WHERE [MessageStatusID] = @messageStatusID AND [IsActive] = 1")]
        MessageStatus GetActiveMessageStatusByID(int messageStatusID);
        [Sql("SELECT [MessageStatusID], [Name], [Description], [IsComplete], [IsActive], [DateTimeCreated], [DateTimeUpdated], [DateTimeInActive], [CreatedBy], [LastUpdatedBy] FROM [MessageStatus]")]
        IList<MessageStatus> GetAllMessageStatus();
        [Sql("SELECT [MessageStatusID], [Name], [Description], [IsComplete], [IsActive], [DateTimeCreated], [DateTimeUpdated], [DateTimeInActive], [CreatedBy], [LastUpdatedBy] FROM [MessageStatus] WHERE [IsActive] = 1")]
        IList<MessageStatus> GetAllActiveMessageStatus();
        [Sql("UPDATE [MessageStatus] SET [Name] = @Name, [Description] = @Description, [IsComplete] = @IsComplete, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [DateTimeInActive] = @DateTimeInActive, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [MessageStatusID] = @MessageStatusID")]
        void UpdateMessageStatus(MessageStatus messageStatus);
        [Sql("INSERT INTO [MessageStatus]([Name], [Description], [IsComplete], [IsActive], [DateTimeCreated], [DateTimeUpdated], [DateTimeInActive], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.MessageStatusID VALUES(@Name, @Description, @IsComplete, @IsActive, @DateTimeCreated, @DateTimeUpdated, @DateTimeInActive, @CreatedBy, @LastUpdatedBy)")]
        void InsertMessageStatus(MessageStatus messageStatus);
        [Sql("DELETE FROM [MessageStatus] WHERE [MessageStatusID] = @messageStatusID")]
        void DeleteMessageStatusByID(int messageStatusID);

        #endregion

        #region Video

        [Sql("SELECT [VideoID], [Title], [ThumbnailPath], [Description], [VideoURL], [ShowInHome], [IsActive], [DateTimeCreated], [DateTimeUpdated], [DateTimeInActive], [CreatedBy], [LastUpdatedBy] FROM [Video] WHERE [VideoID] = @videoID")]
        Video GetVideoByID(int videoID);
        [Sql("SELECT [VideoID], [Title], [ThumbnailPath], [Description], [VideoURL], [ShowInHome], [IsActive], [DateTimeCreated], [DateTimeUpdated], [DateTimeInActive], [CreatedBy], [LastUpdatedBy] FROM [Video] WHERE [VideoID] = @videoID AND [IsActive] = 1")]
        Video GetActiveVideoByID(int videoID);
        [Sql("SELECT [VideoID], [Title], [ThumbnailPath], [Description], [VideoURL], [ShowInHome], [IsActive], [DateTimeCreated], [DateTimeUpdated], [DateTimeInActive], [CreatedBy], [LastUpdatedBy] FROM [Video]")]
        IList<Video> GetAllVideo();
        [Sql("SELECT [VideoID], [Title], [ThumbnailPath], [Description], [VideoURL], [ShowInHome], [IsActive], [DateTimeCreated], [DateTimeUpdated], [DateTimeInActive], [CreatedBy], [LastUpdatedBy] FROM [Video] WHERE [IsActive] = 1")]
        IList<Video> GetAllActiveVideo();
        [Sql("UPDATE [Video] SET [Title] = @Title, [ThumbnailPath] = @ThumbnailPath, [Description] = @Description, [VideoURL] = @VideoURL, [ShowInHome] = @ShowInHome, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [DateTimeInActive] = @DateTimeInActive, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy WHERE [VideoID] = @VideoID")]
        void UpdateVideo(Video video);
        [Sql("INSERT INTO [Video]([Title], [ThumbnailPath], [Description], [VideoURL], [ShowInHome], [IsActive], [DateTimeCreated], [DateTimeUpdated], [DateTimeInActive], [CreatedBy], [LastUpdatedBy]) OUTPUT Inserted.VideoID VALUES(@Title, @ThumbnailPath, @Description, @VideoURL, @ShowInHome, @IsActive, @DateTimeCreated, @DateTimeUpdated, @DateTimeInActive, @CreatedBy, @LastUpdatedBy)")]
        void InsertVideo(Video video);
        [Sql("DELETE FROM [Video] WHERE [VideoID] = @videoID")]
        void DeleteVideoByID(int videoID);

        #endregion

        #region Message

        [Sql("SELECT [MessageID], [MessageCategoryTypeID], [Name], [Phone], [Email], [Address], [TextMessage], [AssetCode], [BranchID], [IPAddress], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy], [MessageStatusID], [Remarks] FROM [Message] WHERE [MessageID] = @messageID")]
        Message GetMessageByID(long messageID);
        [Sql(@"SELECT [MessageID], [MessageCategoryTypeID], [Name], [Phone], [Email], [Address], [TextMessage], [AssetCode], [BranchID], [IPAddress], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy], [MessageStatusID], [Remarks] FROM [Message] WHERE [MessageID] = @messageID AND [IsActive] = 1")]
        Message GetActiveMessageByID(long messageID);
        [Sql("SELECT [MessageID], [MessageCategoryTypeID], [Name], [Phone], [Email], [Address], [TextMessage], [AssetCode], [BranchID], [IPAddress], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy], [MessageStatusID], [Remarks] FROM [Message]")]
        IList<Message> GetAllMessage();
        [Sql("SELECT [MessageID], [MessageCategoryTypeID], [Name], [Phone], [Email], [Address], [TextMessage], [AssetCode], [BranchID], [IPAddress], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy], [MessageStatusID], [Remarks] FROM [Message] WHERE [IsActive] = 1")]
        IList<Message> GetAllActiveMessage();
        [Sql("UPDATE [Message] SET [MessageCategoryTypeID] = @MessageCategoryTypeID, [Name] = @Name, [Phone] = @Phone, [Email] = @Email, [Address] = @Address, [TextMessage] = @TextMessage, [AssetCode] = @AssetCode, [BranchID] = @BranchID, [IPAddress] = @IPAddress, [IsActive] = @IsActive, [DateTimeCreated] = @DateTimeCreated, [DateTimeUpdated] = @DateTimeUpdated, [CreatedBy] = @CreatedBy, [LastUpdatedBy] = @LastUpdatedBy, [MessageStatusID] = @MessageStatusID, [Remarks] = @Remarks WHERE [MessageID] = @MessageID")]
        void UpdateMessage(Message message);
        [Sql("INSERT INTO [Message]([MessageCategoryTypeID], [Name], [Phone], [Email], [Address], [TextMessage], [AssetCode], [BranchID], [IPAddress], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy], [MessageStatusID], [Remarks]) OUTPUT Inserted.MessageID VALUES(@MessageCategoryTypeID, @Name, @Phone, @Email, @Address, @TextMessage, @AssetCode, @BranchID, @IPAddress, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy, @MessageStatusID, @Remarks)")]
        void InsertMessage(Message message);
        [Sql("DELETE FROM [Message] WHERE [MessageID] = @messageID")]
        void DeleteMessageByID(long messageID);

        #endregion

    }
}
