﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insight.Database;
using System.Configuration;

namespace Mandiri.Lelang.DataRepository
{
    public class MandiriLelangDataContext
    {
        public static IMandiriLelangDataRepository Open()
        {
            return ConfigurationManager.ConnectionStrings["MandiriLelangDB"].As<IMandiriLelangDataRepository>();
        }

        public static IMandiriLelangDataRepository OpenWithTransaction()
        {
            return ConfigurationManager.ConnectionStrings["MandiriLelangDB"].OpenWithTransactionAs<IMandiriLelangDataRepository>();
        }
    }
}
