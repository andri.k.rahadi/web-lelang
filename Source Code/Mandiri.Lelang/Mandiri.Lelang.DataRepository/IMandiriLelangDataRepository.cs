﻿using Mandiri.Lelang.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Insight.Database;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using Mandiri.Lelang.ViewModel;

namespace Mandiri.Lelang.DataRepository
{
    public partial interface IMandiriLelangDataRepository : IDbConnection, IDbTransaction
    {
        [Sql("SELECT [UserID], [Username], [FullName], [PasswordSalt], [HashPassword], [Email], [BranchID], [FailedLoginAttempt], [IsLocked], [LastLoggedIn], [LastPasswordChanged], [LastActivity], [IsLoggedIn], [SessionID], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [User] WHERE [UserID] = @userID AND IsActive = 1")]
        User GetActiveUserByID(Int32 userID);
        [Sql("SELECT [UserID], [Username], [FullName], [PasswordSalt], [HashPassword], [Email], [BranchID], [FailedLoginAttempt], [IsLocked], [LastLoggedIn], [LastPasswordChanged], [LastActivity], [IsLoggedIn], [SessionID], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [User] WHERE [UserID] = @userID AND [SessionID] = @sessionID AND IsActive = 1")]
        User GetActiveUserByIDAndSessionID(Int32 userID, String sessionID);
        [Sql("SELECT [UserID], [Username], [FullName], [PasswordSalt], [HashPassword], [Email], [BranchID], [FailedLoginAttempt], [IsLocked], [LastLoggedIn], [LastPasswordChanged], [LastActivity], [IsLoggedIn], [SessionID], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [User] WHERE UPPER([Username]) = UPPER(@username) AND IsActive = 1")]
        User GetActiveUserByUsername(String username);

        [Sql("SELECT [SiteMapID], [MenuName], [SiteMapParentID], [Description], [ActionName], [ControllerName], [Level], [HaveChildren], [IsHiddenMenu], [Order], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [SiteMap] WHERE IsActive = 1 AND Level = @level AND IsHiddenMenu = 0 ORDER BY [Order] ASC")]
        IList<SiteMap> GetAllActiveMenusByLevel(int level);
        [Sql("SELECT [SiteMapID], [MenuName], [SiteMapParentID], [Description], [ActionName], [ControllerName], [Level], [HaveChildren], [IsHiddenMenu], [Order], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [SiteMap] WHERE IsActive = 1 AND Level = 2 AND [SiteMapParentID] = @siteMapParentID AND IsHiddenMenu = 0 ORDER BY [Order] ASC")]
        IList<SiteMap> GetAllChildMenusBySiteMapParentID(String siteMapParentID);
        [Sql("SELECT [SiteMapID], [MenuName], [SiteMapParentID], [Description], [ActionName], [ControllerName], [Level], [HaveChildren], [IsHiddenMenu], [Order], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [SiteMap] WHERE IsActive = 1 AND Level = 2 AND [SiteMapParentID] IN (@siteMapParentIDs) AND IsHiddenMenu = 0 ORDER BY [Order] ASC")]
        IList<SiteMap> GetAllChildMenusBySiteMapParentID(IList<String> siteMapParentIDs);
        [Sql(
        @"SELECT DISTINCT S.[SiteMapID], [MenuName], [SiteMapParentID], [Description], [ActionName], 
        [ControllerName], [Level], [HaveChildren], [IsHiddenMenu], [Order], S.[IsActive], 
        S.[DateTimeCreated], S.[DateTimeUpdated], S.[CreatedBy], S.[LastUpdatedBy] 
        FROM
        UserGroupMapping UG 
        INNER JOIN GroupAuthorization GA ON UG.GroupID = GA.GroupID 
        INNER JOIN SiteMap S ON S.SiteMapID = GA.SiteMapID 
        WHERE UG.IsActive = 1 AND GA.IsActive = 1 AND S.IsActive = 1 AND GA.AllowAccess = 1 AND UG.UserID = @userID")]
        IList<SiteMap> GetAllowedAccessMenusByUserID(int userID);
        [Sql(
        @"SELECT DISTINCT S.[SiteMapID], [MenuName], [SiteMapParentID], [Description], [ActionName], 
        [ControllerName], [Level], [HaveChildren], [IsHiddenMenu], [Order], S.[IsActive], 
        S.[DateTimeCreated], S.[DateTimeUpdated], S.[CreatedBy], S.[LastUpdatedBy] 
        FROM
        UserGroupMapping UG 
        INNER JOIN GroupAuthorization GA ON UG.GroupID = GA.GroupID 
        INNER JOIN SiteMap S ON S.SiteMapID = GA.SiteMapID 
        WHERE UG.IsActive = 1 AND GA.IsActive = 1 AND S.IsActive = 1 AND GA.AllowDelete = 1 AND UG.UserID = @userID")]
        IList<SiteMap> GetAllowedDeleteMenusByUserID(int userID);
        [Sql(
        @"SELECT DISTINCT S.[SiteMapID], [MenuName], [SiteMapParentID], [Description], [ActionName], 
        [ControllerName], [Level], [HaveChildren], [IsHiddenMenu], [Order], S.[IsActive], 
        S.[DateTimeCreated], S.[DateTimeUpdated], S.[CreatedBy], S.[LastUpdatedBy] 
        FROM
        UserGroupMapping UG 
        INNER JOIN GroupAuthorization GA ON UG.GroupID = GA.GroupID 
        INNER JOIN SiteMap S ON S.SiteMapID = GA.SiteMapID 
        WHERE UG.IsActive = 1 AND GA.IsActive = 1 AND S.IsActive = 1 AND GA.AllowUpdate = 1 AND UG.UserID = @userID")]
        IList<SiteMap> GetAllowedUpdateMenusByUserID(int userID);
        [Sql(
        @"SELECT DISTINCT S.[SiteMapID], [MenuName], [SiteMapParentID], [Description], [ActionName], 
        [ControllerName], [Level], [HaveChildren], [IsHiddenMenu], [Order], S.[IsActive], 
        S.[DateTimeCreated], S.[DateTimeUpdated], S.[CreatedBy], S.[LastUpdatedBy] 
        FROM
        UserGroupMapping UG 
        INNER JOIN GroupAuthorization GA ON UG.GroupID = GA.GroupID 
        INNER JOIN SiteMap S ON S.SiteMapID = GA.SiteMapID 
        WHERE UG.IsActive = 1 AND GA.IsActive = 1 AND S.IsActive = 1 AND GA.AllowAdd = 1 AND UG.UserID = @userID")]
        IList<SiteMap> GetAllowedAddMenusByUserID(int userID);

        [Sql("SELECT ProvinceID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Province] WHERE [ProvinceID] = @provinceID AND IsActive = 1")]
        Province GetActiveProvinceByID(Int32 provinceID);
        [Sql("SELECT ProvinceID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Province] WHERE IsActive = 1")]
        IList<Province> GetAllActiveProvinces();
        [Sql("SELECT -1 AS ProvinceID, 'Semua' AS Name, 'Semua' AS Description, 1 AS [IsActive], NULL AS [DateTimeCreated], GETDATE() AS [DateTimeUpdated], '' AS [CreatedBy], '' AS [LastUpdatedBy] UNION ALL SELECT ProvinceID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Province] WHERE IsActive = 1")]
        IList<Province> GetAllActiveProvincesWithAll();
        [Sql("SELECT COUNT_BIG(ProvinceID) FROM [Province] WHERE IsActive = 1")]
        long GetActiveProvincesCount();


        [Sql("UPDATE [TotalVisitor] SET [TotalVisitorCount] = [TotalVisitorCount] + 1, [TotalTodayVisitorCount] = CASE WHEN DATEDIFF(day, CAST(DATEADD(dd, 0, DATEDIFF(dd, 0, DateTimeUpdated)) AS DATE), CAST(DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) AS DATE)) >= 1 THEN 1 ELSE [TotalTodayVisitorCount] + 1 END, [DateTimeUpdated] = GETDATE() WHERE [TotalVisitorID] = @totalVisitorID")]
        void UpdateTotalVisitorCount(int totalVisitorID);

        [Sql("SELECT [AssetID], [FieldName], [Value], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AssetCategoryTemplateValue] WHERE [AssetID] = @assetID")]
        IList<AssetCategoryTemplateValue> GetAllActiveAssetCategoryTemplateValuesByAssetID(Int64 assetID);

        //Fix 11 Apr 2016 - Delete all asset category template first before adding the new one, to avoid unrelated asset category template
        [Sql("DELETE FROM [AssetCategoryTemplateValue] WHERE [AssetID] = @assetID")]
        void DeleteAssetCategoryTemplateValueByAssetID(Int64 assetID);

        #region Web Content

        [Sql("SELECT [WebContentTypeID], [Content], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [WebContent] WHERE [WebContentTypeID] = @webContentTypeID AND IsActive = 1")]
        WebContent GetActiveWebContentByID(Int32 webContentTypeID);
        [Sql("SELECT [WebContentTypeID], [Content], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [WebContent] WHERE IsActive = 1")]
        IList<WebContent> GetAllActiveWebContents();
        [Sql("SELECT COUNT_BIG([WebContentTypeID]) FROM WebContent WHERE IsActive = 1")]
        long GetActiveWebContentsCount();
        [Sql("SELECT [WebContentTypeID], [Name], [Description], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [WebContentType] WHERE [WebContentTypeID] = @webContentTypeID AND IsActive = 1")]
        WebContentType GetActiveWebContentTypeByID(Int32 webContentTypeID);

        #endregion

        #region City
        [Sql("SELECT CityID, ProvinceID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [City] WHERE [CityID] = @cityID AND IsActive = 1")]
        City GetActiveCityByID(Int32 cityID);
        [Sql("SELECT CityID, ProvinceID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [City] WHERE [ProvinceID] IN (@provinceIDs) AND IsActive = 1")]
        IList<City> GetAllActiveCitiesByProvinceIDs(IEnumerable<int> provinceIDs);
        [Sql("SELECT CityID, ProvinceID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [City] WHERE IsActive = 1")]
        IList<City> GetAllActiveCities();
        [Sql("SELECT -1 AS CityID, -1 AS ProvinceID, 'Semua' AS Name, 'Semua' AS Description, 1 AS [IsActive], NULL AS [DateTimeCreated], GETDATE() AS [DateTimeUpdated], '' AS [CreatedBy], '' AS [LastUpdatedBy] UNION ALL SELECT CityID, ProvinceID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [City] WHERE IsActive = 1")]
        IList<City> GetAllActiveCitiesWithAll();
        [Sql("SELECT COUNT_BIG(CityID) FROM [City] WHERE IsActive = 1")]
        long GetActiveCitiesCount();
        #endregion

        #region Category
        [Sql("SELECT CategoryID,  Name, Description, CurrentAssetTotal, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Category] WHERE [CategoryID] = @categoryID AND IsActive = 1")]
        Category GetActiveCategoryByID(Int32 categoryID);
        [Sql("SELECT CategoryID, Name, Description, CurrentAssetTotal, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Category] WHERE IsActive = 1")]
        IList<Category> GetAllActiveCategories();
        [Sql("SELECT COUNT_BIG(CategoryID) FROM [Category] WHERE IsActive = 1")]
        long GetActiveCategoriesCount();
        [Sql("SELECT -1 AS CategoryID, 'Semua' AS Name, 'Semua' AS Description, 1 AS [IsActive], NULL AS [DateTimeCreated], GETDATE() AS [DateTimeUpdated], '' AS [CreatedBy], '' AS [LastUpdatedBy] UNION ALL SELECT CategoryID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Category] WHERE IsActive = 1")]
        IList<Category> GetAllActiveCategoriesWithAll();
        #endregion

        #region MessageStatus
        [Sql("SELECT COUNT_BIG(MessageStatusID) FROM [MessageStatus] WHERE IsActive = 1")]
        long GetActiveMessageStatusCount();
        #endregion

        #region Video
        [Sql("SELECT COUNT_BIG(VideoID) FROM [Video] WHERE IsActive = 1")]
        long GetActiveVideoCount();

        [Sql("SELECT [VideoID],[Title],[ThumbnailPath],[Description],[VideoURL] FROM [dbo].[Video] WHERE [IsActive] = 1")]
        List<VideoViewModel> GetVideoActiveList();

        [Sql("SELECT TOP 1 [VideoID], [Title], [ThumbnailPath], [Description], [VideoURL], [ShowInHome], [IsActive], [DateTimeCreated], [DateTimeUpdated], [DateTimeInActive], [CreatedBy], [LastUpdatedBy] FROM [Video] WHERE [ShowInHome] = 1 AND [IsActive] = 1")]
        Video GetActiveShowInHomeVideo();
        #endregion

        #region Segment
        [Sql("SELECT SegmentID,  Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Segment] WHERE [SegmentID] = @segmentID AND IsActive = 1")]
        Segment GetActiveSegmentByID(String segmentID);
        [Sql("SELECT SegmentID, Name, Description,  [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Segment] WHERE IsActive = 1")]
        IList<Segment> GetAllActiveSegments();
        [Sql("SELECT COUNT_BIG(SegmentID) FROM [Segment] WHERE IsActive = 1")]
        long GetActiveSegmentsCount();
        #endregion

        #region Message Category Type
        [Sql("SELECT MessageCategoryTypeID,  Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [MessageCategoryType] WHERE [MessageCategoryTypeID] = @messageCategoryTypeID AND IsActive = 1")]
        MessageCategoryType GetActiveMessageCategoryTypeByID(byte messageCategoryTypeID);
        [Sql("SELECT MessageCategoryTypeID, Name, Description,  [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [MessageCategoryType] WHERE IsActive = 1")]
        IList<MessageCategoryType> GetAllActiveMessageCategoryTypes();
        [Sql("SELECT COUNT_BIG(MessageCategoryTypeID) FROM [MessageCategoryType] WHERE IsActive = 1")]
        long GetActiveMessageCategoryTypesCount();
        #endregion

        #region Ownership Document Type
        [Sql("SELECT OwnershipDocumentTypeID,  Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [OwnershipDocumentType] WHERE [OwnershipDocumentTypeID] = @ownershipDocumentTypeID AND IsActive = 1")]
        OwnershipDocumentType GetActiveOwnershipDocumentTypeByID(Int32 ownershipDocumentTypeID);
        [Sql("SELECT OwnershipDocumentTypeID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [OwnershipDocumentType] WHERE IsActive = 1")]
        IList<OwnershipDocumentType> GetAllActiveOwnershipDocumentTypes();
        [Sql("SELECT COUNT_BIG(OwnershipDocumentTypeID) FROM [OwnershipDocumentType] WHERE IsActive = 1")]
        long GetActiveOwnershipDocumentTypesCount();
        [Sql("SELECT -1 AS OwnershipDocumentTypeID, 'Semua' AS Name, 'Semua' AS Description, 1 AS [IsActive], NULL AS [DateTimeCreated], GETDATE() AS [DateTimeUpdated], '' AS [CreatedBy], '' AS [LastUpdatedBy] UNION ALL SELECT OwnershipDocumentTypeID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [OwnershipDocumentType] WHERE IsActive = 1")]
        IList<OwnershipDocumentType> GetAllActiveOwnershipDocumentTypesWithAll();
        #endregion

        #region Group
        [Sql("SELECT GroupID,  Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Group] WHERE [GroupID] = @groupID AND IsActive = 1")]
        Group GetActiveGroupByID(Int32 groupID);
        [Sql("SELECT GroupID, Name, Description, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Group] WHERE IsActive = 1")]
        IList<Group> GetAllActiveGroups();
        [Sql("SELECT COUNT_BIG(GroupID) FROM [Group] WHERE IsActive = 1")]
        long GetActiveGroupsCount();
        #endregion

        #region Branch
        [Sql("SELECT BranchID,  Name, Description,Address,Alias, Phone1, Phone2, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Branch] WHERE [BranchID] = @branchID AND IsActive = 1")]
        Branch GetActiveBranchByID(Int32 branchID);
        [Sql("SELECT BranchID, Name, Description, Address,Alias, Phone1, Phone2, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Branch] WHERE IsActive = 1")]
        IList<Branch> GetAllActiveBranches();
        [Sql("SELECT -1 AS BranchID, 'Semua' AS Name, 'Semua' AS Description, '' AS Address, '' AS Alias, '' AS Phone1, '' AS Phone2, 1 AS [IsActive], NULL AS [DateTimeCreated], GETDATE() AS [DateTimeUpdated], '' AS [CreatedBy], '' AS [LastUpdatedBy] UNION ALL SELECT BranchID, Name, Description, Address,Alias, Phone1, Phone2, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [Branch] WHERE IsActive = 1")]
        IList<Branch> GetAllActiveBranchesWithAll();
        [Sql("SELECT COUNT_BIG(BranchID) FROM [Branch] WHERE IsActive = 1")]
        long GetActiveBranchesCount();
        #endregion

        #region KPKNL
        [Sql("SELECT KPKNLID,  Name, Description,Address, Phone1, Phone2, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [KPKNL] WHERE [KPKNLID] = @kpknlID AND IsActive = 1")]
        KPKNL GetActiveKPKNLByID(Int32 kpknlID);
        [Sql("SELECT KPKNLID, Name, Description, Address, Phone1, Phone2, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [KPKNL] WHERE IsActive = 1")]
        IList<KPKNL> GetAllActiveKPKNLs();
        [Sql("SELECT COUNT_BIG(KPKNLID) FROM [KPKNL] WHERE IsActive = 1")]
        long GetActiveKPKNLsCount();
        #endregion

        #region AuctionHall
        [Sql("SELECT AuctionHallID,  Name, Description,Address, Phone1, Phone2, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AuctionHall] WHERE [AuctionHallID] = @auctionHallID AND IsActive = 1")]
        AuctionHall GetActiveAuctionHallByID(Int32 auctionHallID);
        [Sql("SELECT AuctionHallID, Name, Description, Address, Phone1, Phone2, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [AuctionHall] WHERE IsActive = 1")]
        IList<AuctionHall> GetAllActiveAuctionHalls();
        [Sql("SELECT COUNT_BIG(AuctionHallID) FROM [AuctionHall] WHERE IsActive = 1")]
        long GetActiveAuctionHallsCount();
        #endregion

        #region MinMaxPrice
        [Sql("SELECT MinMaxPriceID,  Name, Description, Value, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [MinMaxPrice] WHERE [MinMaxPriceID] = @minMaxPriceID AND IsActive = 1")]
        MinMaxPrice GetActiveMinMaxPriceByID(Int32 minMaxPriceID);
        [Sql("SELECT MinMaxPriceID, Name, Description, Value, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [MinMaxPrice] WHERE IsActive = 1")]
        IList<MinMaxPrice> GetAllActiveMinMaxPrices();
        [Sql("SELECT COUNT_BIG(MinMaxPriceID) FROM [MinMaxPrice] WHERE IsActive = 1")]
        long GetActiveMinMaxPricesCount();
        [Sql("SELECT -1 AS MinMaxPriceID, 'Semua' AS Name, 'Semua' AS Description, -1 AS Value, 1 AS [IsActive], NULL AS [DateTimeCreated], GETDATE() AS [DateTimeUpdated], '' AS [CreatedBy], '' AS [LastUpdatedBy] UNION ALL SELECT MinMaxPriceID, Name, Description, Value, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [MinMaxPrice] WHERE IsActive = 1")]
        IList<MinMaxPrice> GetAllActiveMinMaxPricesWithAll();
        #endregion

        #region CategoryTemplate
        [Sql("SELECT CategoryID,  FieldName, [IsMandatory], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [CategoryTemplate] WHERE [CategoryID] = @categoryID AND [FieldName] = @fieldName AND IsActive = 1")]
        CategoryTemplate GetActiveCategoryTemplateByID(Int32 categoryID, String fieldName);
        [Sql("SELECT CategoryID, FieldName, [IsMandatory], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [CategoryTemplate] WHERE IsActive = 1")]
        IList<CategoryTemplate> GetAllCategoryTemplates();
        [Sql("SELECT CategoryID, FieldName, [IsMandatory], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [CategoryTemplate] WHERE [CategoryID] = @categoryID AND IsActive = 1")]
        IList<CategoryTemplate> GetAllCategoryTemplatesByCategoryID(Int32 categoryID);
        [Sql("SELECT COUNT_BIG(CategoryID) FROM [CategoryTemplate] WHERE IsActive = 1")]
        long GetActiveCategoryTemplatesCount();

        [Sql(@"UPDATE [CategoryTemplate] SET [FieldName] = @newFieldName, [IsMandatory] = @isMandatory WHERE CategoryID = @categoryID AND FieldName = @oldFieldName AND IsActive = 1")]
        void UpdateCategoryTemplateByCategoryIDAndOldFieldName(Int32 categoryID, String oldFieldName, String newFieldName, bool isMandatory);
        #endregion

        #region UserGroupMapping
        [Sql("SELECT UserGroupMappingID,  GroupID, UserID, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [UserGroupMapping] WHERE [UserGroupMappingID] = @userGroupMappingID AND IsActive = 1")]
        UserGroupMapping GetActiveUserGroupMappingByID(Int32 userGroupMappingID);
        [Sql("SELECT UserGroupMappingID, GroupID, UserID, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [UserGroupMapping] WHERE IsActive = 1")]
        IList<UserGroupMapping> GetAllActiveUserGroupMappings();
        [Sql("SELECT COUNT_BIG(UserGroupMappingID) FROM [UserGroupMapping] WHERE IsActive = 1")]
        long GetActiveUserGroupMappingsCount();
        #endregion

        #region User
        [Sql("SELECT UserID, UserName, Fullname, [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [User] WHERE IsActive = 1")]
        IList<User> GetAllActiveUsers();
        [Sql("SELECT COUNT_BIG(UserID) FROM [User] WHERE IsActive = 1")]
        long GetActiveUsersCount();
        [Sql("SELECT [UserID], [Username], [FullName], [PasswordSalt], [HashPassword], [Email], [BranchID], [FailedLoginAttempt], [IsLocked], [LastLoggedIn], [LastPasswordChanged], [LastActivity], [IsLoggedIn], [SessionID], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [User] WHERE [BranchID] = @branchID AND IsActive = 1")]
        IList<User> GetAllActiveUserByBranchID(Int32 branchID);
        #endregion

        #region Message
        [Sql("SELECT COUNT_BIG(MessageID) FROM [Message] WHERE IsActive = 1")]
        long GetActiveMessagesCount();

        [Sql(@"select   MessageID, MCT.Name AS CategoryName , M.Name AS Name, Phone, Email, TextMessage, M.[IsActive]
				, M.[DateTimeCreated], ISNULL(B.Name, '') AS BranchName, M.AssetCode, M.MessageStatusID,M.BranchID,
				M.MessageCategoryTypeID, ISNULL(m.Remarks,'') as Remarks, ISNULL(MS.Name,'') AS Status
                FROM [Message] M 
                    JOIN [MessageCategoryType] MCT ON M.MessageCategoryTypeID = MCT.MessageCategoryTypeID 
                    LEFT JOIN Branch B ON M.BranchID = B.BranchID
					LEFT JOIN MessageStatus MS ON m.MessageStatusID = ms.MessageStatusID
                WHERE M.IsActive = 1 and M.MessageID =@messageID")]
        Message GetActiveMessageByID(Int32 messageID);
        #endregion

        #region UserActivity
        [Sql("SELECT COUNT_BIG(UserActivityID) FROM [UserActivity] WHERE IsActive = 1")]
        long GetActiveUserActivitiesCount();
        #endregion

        #region SiteMap
        [Sql("SELECT SiteMapID, MenuName, SiteMapParentID, [Description], [ActionName], [ControllerName], [Level], [HaveChildren],[IsHiddenMenu],[Order], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy] FROM [SiteMap] WHERE IsActive = 1 AND HaveChildren = 0")]
        IList<SiteMap> GetActiveSiteMaps();
        #endregion

        #region GroupAuthorization
        [Sql("SELECT [GroupAuthorizationID],[GroupID],[SiteMapID],[AllowAccess],[AllowAdd],[AllowUpdate],[AllowDelete],[IsActive],[DateTimeCreated],[DateTimeUpdated],[CreatedBy],[LastUpdatedBy] FROM [GroupAuthorization] WHERE GroupID = @groupID AND IsActive = 1")]
        IList<GroupAuthorization> GetActiveGroupAuthorizationByGroupID(Int32 groupID);
        #endregion

        #region Asset
        [Sql(@"SELECT [AssetID],[AssetCode],[AccountNumber],[AccountName],[CategoryID],[Address],[ProvinceID],[CityID],[OldPrice],[NewPrice],[BranchID],[MarketValue],
            [LiquidationValue],[SecurityRightsValue],[SoldValue],[ValuationDate],[AuctionScheduleID],[AuctionHallID],[AuctionHallAddress],[AuctionHallPhone1],[AuctionHallPhone2],[KPKNLID],
            [KPKNLAddress],[KPKNLPhone1],[KPKNLPhone2],[SegmentID],[Photo1],[Photo2],[Photo3],[Photo4],[Photo5],[OwnershipDocumentTypeID],[CurrentStatusID],[Latitude],[Longitude],[TransferInformation],
            [AdditionalNotes],[DocumentNumber],[IsActive],[DateTimeCreated],[DateTimeUpdated],[CreatedBy],[LastUpdatedBy], [AuctionAddress], [IsEmailAuction], [EmailAuctionURL], [LastAutoUpdatedDateTime], [Type], [Price], [ExpiredDate] FROM [Asset]
                WHERE IsActive = 1 AND AssetID = @assetID")]
        Asset GetActiveAssetByID(long assetID);

        [Sql(@"SELECT [AssetID],[AssetCode],[AccountNumber],[AccountName],[CategoryID],[Address],[ProvinceID],[CityID],[OldPrice],[NewPrice],[BranchID],[MarketValue],
            [LiquidationValue],[SecurityRightsValue],[SoldValue],[ValuationDate],[AuctionScheduleID],[AuctionHallID],[AuctionHallAddress],[AuctionHallPhone1],[AuctionHallPhone2],[KPKNLID],
            [KPKNLAddress],[KPKNLPhone1],[KPKNLPhone2],[SegmentID],[Photo1],[Photo2],[Photo3],[Photo4],[Photo5],[OwnershipDocumentTypeID],[CurrentStatusID],[Latitude],[Longitude],[TransferInformation],
            [AdditionalNotes],[DocumentNumber],[IsActive],[DateTimeCreated],[DateTimeUpdated],[CreatedBy],[LastUpdatedBy], [AuctionAddress], [IsEmailAuction], [EmailAuctionURL], [LastAutoUpdatedDateTime], [Type], [Price], [ExpiredDate] FROM [Asset]
                WHERE IsActive = 1 AND AssetID = @assetID AND CurrentStatusID IN (4)")]
        Asset GetActiveAndLiveAssetByID(long assetID);

        [Sql(@"SELECT COUNT_BIG(AssetID) FROM (SELECT 
                A.AssetID, AssetCode,A.SegmentID,SG.Name AS SegmentName, A.CategoryID, C.Name AS CategoryName, A.Address, A.ProvinceID, P.Name AS ProvinceName, A.CityID,
                CT.Name AS CityName, OldPrice, NewPrice, Photo1 , A.CurrentStatusID, A.AdditionalNotes, A.DateTimeCreated
                FROM Asset A INNER JOIN Category C ON C.CategoryID = A.CategoryID
                INNER JOIN AssetStatus AU ON A.CurrentStatusID = AU.AssetStatusID 
                INNER JOIN Province P ON P.ProvinceID = A.ProvinceID
                INNER JOIN City CT ON CT.CityID = A.CityID
                INNER JOIN Segment SG ON A.SegmentID = SG.SegmentID
                WHERE A.IsActive = 1 AND A.BranchID = @branchID OR @branchID = -1) TBL")]
        long GetActiveAssetsCount(int branchID);
        #endregion

        #region AssetStatus
        [Sql("SELECT * FROM [AssetStatus] WHERE IsActive = 1")]
        IList<AssetStatus> GetAllAssetStatuses();
        [Sql("SELECT * FROM [AssetStatus] WHERE IsActive = 1 AND AssetStatusID=@statusID")]
        IList<AssetStatus> GetAllAssetStatusesByStatusID(Int32 statusID);
        #endregion

        #region AuctionSchedule
        [Sql(@"SELECT [AuctionScheduleID],[KPKNLID],[ScheduleDate],[ScheduleTime],[Timezone],[AuctionAddress],[Phone1],[Phone2],[IsActive],[DateTimeCreated],[DateTimeUpdated],[CreatedBy],[LastUpdatedBy] FROM [AuctionSchedule] WHERE IsActive = 1 AND AuctionScheduleID = @auctionScheduleID")]
        AuctionSchedule GetActiveAuctionScheduleByAuctionScheduleID(long auctionScheduleID);
        #endregion

        #region SystemSetting
        [Sql(@"SELECT [SystemSettingID],[Value],[IsActive],[DateTimeCreated],[DateTimeUpdated],[CreatedBy],[LastUpdatedBy] FROM [SystemSetting] WHERE [IsActive] = 1 AND SystemSettingID = @systemSettingID")]
        SystemSetting GetActiveSystemSettingByID(string systemSettingID);
        #endregion

        #region Asset Changes
        [Sql(@"SELECT AC.AssetID, A.AssetCode, IsNew, IsUpdated, IsDeleted, UA.Username AS UserAdmin,
                AC.DateTimeCreated AS [Date], 
                CASE 
	                WHEN W.IsApproved IS NULL AND W.IsRejected IS NULL THEN '' 
	                WHEN W.IsApproved = 1 THEN 'Approve'
	                WHEN W.IsRejected = 1 THEN 'Reject' 
                END AS ApproveOrReject,
                CASE 
	                WHEN W.IsApproved IS NULL AND W.IsRejected IS NULL THEN '' 
	                WHEN W.IsApproved = 1 THEN UAP.Username
	                WHEN W.IsRejected = 1 THEN UAR.Username 
                END AS ApproveOrRejectBy,
                W.DateTimeCreated AS ApproveOrRejectDate, ISNULL(W.Reason,'') AS ApproveReason
                FROM AssetChanges AC JOIN Asset A ON A.AssetID = AC.AssetID
                LEFT JOIN AssetWorkflowHistory W ON AC.AssetChangesID = W.AssetChangesID
                JOIN [User] UA ON AC.CreatedBy = UA.UserID
                LEFT JOIN [User] UAP ON W.ApprovedBy = UAP.UserID
                LEFT JOIN [User] UAR ON W.RejectedBy = UAR.UserID
                WHERE A.IsActive = 1 AND AC.IsActive = 1 AND W.IsActive = 1 AND A.AssetID = @assetID
                ORDER BY AC.DateTimeCreated DESC, W.DateTimeCreated ASC")]
        IList<AssetChanges> GetAllAssetChangeByAssetID(long assetID);

        [Sql(@"SELECT [AssetID],[FieldName],[Value],[IsActive] ,[DateTimeCreated] ,[DateTimeUpdated] ,[CreatedBy] ,[LastUpdatedBy] FROM [AssetCategoryTemplateValue] WHERE IsActive = 1 AND [AssetID] = @assetID AND [FieldName] = @fieldName")]
        AssetCategoryTemplateValue GetAllActiveAssetCategoryTemplateValueByIDAndFieldName(long assetID, string fieldName);

        [Sql(@"SELECT [AssetChangesID],[AssetID],[IsNew],[IsUpdated],[IsDeleted],[OriginalAssetStatusID], [CurrentApprovalStatus],[IsActive],[DateTimeCreated],[DateTimeUpdated],[CreatedBy],[LastUpdatedBy]FROM [AssetChanges] WHERE AssetID = @AssetID AND [CurrentApprovalStatus] = @approvalStatus")]
        AssetChanges GetActiveAssetChangeByAssetIDAndApprovalStatus(long? assetID, int approvalStatus);

        [Sql(@"SELECT COUNT_BIG(AssetChangesID) FROM (SELECT AssetChangesID, AC.AssetID, IsNew, IsUpdated, IsDeleted, CurrentApprovalStatus, OriginalAssetStatusID,
                C.Name As Category, A.AssetCode, CT.Name AS City, P.Name AS Province , A.Photo1, S.Name AS Segment, A.AdditionalNotes
                FROM AssetChanges AC 
                INNER JOIN Asset A ON A.AssetID = AC.AssetID
                INNER JOIN Category C ON A.CategoryID = C.CategoryID
                INNER JOIN Province P ON A.ProvinceID = P.ProvinceID
                INNER JOIN City CT ON A.CityID =  CT.CityID 
                INNER JOIN Segment S ON A.SegmentID = S.SegmentID
                WHERE AC.IsActive = 1 AND CurrentApprovalStatus = 0 AND A.CurrentStatusID >= 2
                AND ( A.BranchID = @branchID OR @branchID = -1)) TBL")]
        long GetActiveAssetChangesCount(int branchID);

        [Sql(@"SELECT COUNT_BIG(AssetChangesID) FROM [AssetChanges] WHERE IsActive = 1 AND CurrentApprovalStatus = 2")]
        long GetActiveAssetChangesForInputterCount();

        [Sql(@"SELECT COUNT_BIG(AssetChangesID) FROM [AssetChanges] WHERE IsActive = 1 AND CurrentApprovalStatus in (0,2)")]
        long GetActiveAssetChangesForInputterAndApproverCount();

        #endregion

        #region AssetChangesHistory
        [Sql(@"SELECT COUNT_BIG(AssetID) FROM (SELECT AC.AssetID, A.AssetCode, IsNew, IsUpdated, IsDeleted, UA.Username AS UserAdmin,
                AC.DateTimeCreated AS [Date], 
                CASE 
	                WHEN W.IsApproved IS NULL AND W.IsRejected IS NULL THEN '' 
	                WHEN W.IsApproved = 1 THEN 'Approve'
	                WHEN W.IsRejected = 1 THEN 'Reject' 
                END AS ApproveOrReject,
                CASE 
	                WHEN W.IsApproved IS NULL AND W.IsRejected IS NULL THEN '' 
	                WHEN W.IsApproved = 1 THEN UAP.Username
	                WHEN W.IsRejected = 1 THEN UAR.Username 
                END AS ApproveOrRejectBy,
                W.DateTimeCreated AS ApproveOrRejectDate, ISNULL(W.Reason,'') AS ApproveReason
                FROM AssetChanges AC JOIN Asset A ON A.AssetID = AC.AssetID
                LEFT JOIN AssetWorkflowHistory W ON AC.AssetChangesID = W.AssetChangesID
                JOIN [User] UA ON AC.CreatedBy = UA.UserID
                LEFT JOIN [User] UAP ON W.ApprovedBy = UAP.UserID
                LEFT JOIN [User] UAR ON W.RejectedBy = UAR.UserID
                WHERE A.IsActive = 1 AND AC.IsActive = 1 AND W.IsActive = 1 AND A.AssetID = @assetID) TBL")]
        long GetActiveAssetAssetChangeHistoryCount(long assetID);
        #endregion


        [Sql(@"SELECT UGM.GroupID FROM [User] U INNER JOIN UserGroupMapping UGM on U.UserID = UGM.UserID WHERE UGM.IsActive = 1 AND U.UserID = @userID AND (UGM.GroupID = @approvalGroupID OR UGM.GroupID = @inputterGroupID)")]
        IList<int> GetGroupIDByUserID(int userID, int approvalGroupID, int inputterGroupID);

        [Sql("INSERT INTO [AssetRejectReason]([AssetWorkflowHistoryID], [FieldOrColumnName], [Reason], [IsActive], [DateTimeCreated], [DateTimeUpdated], [CreatedBy], [LastUpdatedBy]) VALUES(@AssetWorkflowHistoryID ,@FieldOrColumnName, @Reason, @IsActive, @DateTimeCreated, @DateTimeUpdated, @CreatedBy, @LastUpdatedBy)")]
        void InsertAssetRejectReasonNew(AssetRejectReason assetRejectReason);

        [Sql(@"SELECT[AssetWorkflowHistoryID] ,[AssetChangesID] ,[IsApproved] ,[ApprovedBy] ,[IsRejected] ,[RejectedBy] ,[Reason] ,[IsActive] ,[DateTimeCreated] ,[DateTimeUpdated] ,[CreatedBy] ,[LastUpdatedBy] FROM [AssetWorkflowHistory] WHERE IsActive = 1 AND IsRejected = 1 AND AssetChangesID = @assetChangesID")]
        AssetWorkflowHistory GetActiveAssetWorkFlowHistoryRejectedByAssetChangesID(long assetChangesID);

        [Sql(@"SELECT Name from AssetStatus WHERE AssetStatusID = @assetStatusID AND IsActive = 1")]
        String GetActiveAssetStatusByStatusID(int assetStatusID);

        [Sql(@"SELECT [AssetChangesHistoryID] ,[AssetChangesID] ,[FieldOrColumnName] ,[LabelName],[OldValue] ,[NewValue],[IsActive],[DateTimeCreated],[DateTimeUpdated],[CreatedBy],[LastUpdatedBy] FROM [AssetChangesHistory] WHERE AssetChangesID = @assetChangesID AND FieldOrColumnName = @fieldOrColumnName")]
        AssetChangesHistory GetActiveAssetChangesHistory(long assetChangesID, String fieldOrColumnName);

        [Sql(@"UPDATE [AssetChangesHistory] SET [NewValue] = @newValue WHERE AssetChangesID = @assetChangesID AND FieldOrColumnName = @fieldOrColumnName")]
        void UpdateAssetChangesHistoryNewValue(long assetChangesID, String fieldOrColumnName, String newValue);

        [Sql("SELECT MIN([PasswordHistoryID]) FROM [PasswordHistory] WHERE [UserID] = @userID")]
        long GetLowestPasswordHistoryIDByUserID(Int32 userID);

        [Sql("SELECT COUNT([PasswordHistoryID]) FROM [PasswordHistory] WHERE [UserID] = @userID")]
        int GetPasswordHistoryCountByUserID(Int32 userID);

        [Sql("SELECT COUNT([PasswordHistoryID]) FROM [PasswordHistory] WHERE [UserID] = @userID AND [HashPassword] = @hashPassword")]
        int GetExistingPasswordHistoryCountByUserIDAndHashPassword(Int32 userID, String hashPassword);

        [Sql(@"SELECT [AuctionScheduleID],[KPKNLID],[ScheduleDate],[ScheduleTime],[Timezone],[AuctionAddress],[Phone1],[Phone2],[IsActive],[DateTimeCreated],[DateTimeUpdated],[CreatedBy],[LastUpdatedBy] FROM [AuctionSchedule] WHERE IsActive = 1 AND ScheduleDate = @scheduleDate AND ScheduleTime = @scheduleTime AND Timezone = @timezone AND KPKNLID = @kpknlID")]
        AuctionSchedule GetActiveAuctionScheduleBySchedule(DateTime scheduleDate, DateTime scheduleTime, byte timezone, int kpknlID);

        #region Mobile Auth Token

        [Sql("SELECT TOP 1 [MobileAuthTokenID], [MemberID], [Token], [Salt], [ValidUntilDateTime], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MobileAuthToken] WHERE [MemberID] = @memberID AND [IsActive] = 1 AND [Token] = @token")]
        MobileAuthToken GetActiveMobileAuthTokenByMemberIDAndToken(long memberID, string token);
        [Sql("UPDATE [MobileAuthToken] SET [IsActive] = 0, [LastUpdatedBy] = @lastUpdatedBy, [DateTimeUpdated] = GETDATE() WHERE [MemberID] = @memberID AND [IsActive] = 1 AND [Token] = @token")]
        void DeactivatedMobileAuthTokenByMemberIDAndToken(long memberID, string token, int? lastUpdatedBy);
        [Sql("SELECT TOP 1 [MobileAuthTokenID], [MemberID], [Token], [Salt], [ValidUntilDateTime], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MobileAuthToken] WHERE [MemberID] = @memberID AND [IsActive] = 1")]
        MobileAuthToken GetActiveMobileAuthTokenByMemberID(long memberID);

        #endregion

        #region Member Favorite

        [Sql("SELECT [MemberFavoriteID], [MemberID], [AssetID], [IsFavorite], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberFavorite] WHERE [MemberID] = @memberID AND [AssetID] = @assetID AND [IsActive] = 1")]
        MemberFavorite GetActiveMemberFavoriteByMemberIDAndAssetID(long memberID, long assetID);

        #endregion

        #region Member

        [Sql("SELECT [MemberID], [FullName], [Email], [PlaceOfBirth], [DateOfBirth], [MobilePhoneNumber], [Address], [ProvinceID], [CityID], [IdentificationNumber], [PasswordSalt], [HashPassword], [ActivationToken], [IsLocked], [SessionID], [LastLoggedIn], [LastPasswordChanged], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [Member] WHERE LTRIM(RTRIM(UPPER([Email]))) = @email AND [IsActive] = 1")]
        Member GetActiveMemberByEmail(String email);

        #endregion

        #region Member Account

        [Sql("SELECT TOP 1 [MemberAccountID], [MemberID], [SocialToken], [SocialID], [SocialUsername], [SocialURL], [Source], [IsActivated], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberAccount] WHERE [MemberID] = @memberID AND [Source] = @source AND [IsActive] = 1")]
        MemberAccount GetActiveMemberAccountByMemberIDAndSource(long memberID, String source);

        #endregion

        #region Reset Password Request

        [Sql("SELECT [ResetPasswordRequestID], [Email], [MemberID], [RequestToken], [IsUsed], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [ResetPasswordRequest] WHERE [MemberID] = @memberID AND [IsActive] = 1")]
        ResetPasswordRequest GetActiveResetPasswordRequestByMemberID(long memberID);
        [Sql("UPDATE [ResetPasswordRequest] SET [IsActive] = 0, [LastUpdatedBy] = NULL, [DateTimeUpdated] = GETDATE() WHERE [MemberID] = @memberID")]
        void DisableAllResetPasswordRequestByMemberID(long memberID);

        #endregion

        #region Preference

        [Sql("SELECT [MemberPreferenceID], [MemberID], [MinPriceID], [MaxPriceID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberPreference] WHERE [MemberID] = @memberID AND [IsActive] = 1")]
        MemberPreference GetActiveMemberPreferenceByMemberID(long memberID);

        [Sql("SELECT [MemberCityPreferenceID], [MemberPreferenceID], [CityID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberCityPreference] WHERE [IsActive] = 1 AND [MemberPreferenceID] = @memberPreferenceID")]
        IList<MemberCityPreference> GetAllActiveMemberCityPreferenceByMemberPreferenceID(long memberPreferenceID);
        [Sql("DELETE FROM [MemberCityPreference] WHERE [MemberPreferenceID] = @memberPreferenceID")]
        void DeleteAllMemberCityPreferenceByMemberPreferenceID(long memberPreferenceID);

        [Sql("SELECT [MemberCategoryPreferenceID], [MemberPreferenceID], [CategoryID], [IsActive], [CreatedBy], [DateTimeCreated], [LastUpdatedBy], [DateTimeUpdated] FROM [MemberCategoryPreference] WHERE [IsActive] = 1 AND [MemberPreferenceID] = @memberPreferenceID")]
        IList<MemberCategoryPreference> GetAllActiveMemberCategoryPreferenceByMemberPreferenceID(long memberPreferenceID);
        [Sql("DELETE FROM [MemberCategoryPreference] WHERE [MemberPreferenceID] = @memberPreferenceID")]
        void DeleteAllMemberCategoryPreferenceByMemberPreferenceID(long memberPreferenceID);

        [Sql("SELECT DISTINCT C.ProvinceID FROM [MemberCityPreference] MCP INNER JOIN [City] C ON MCP.CityID = C.CityID WHERE MCP.[IsActive] = 1 AND C.IsActive = 1 AND MCP.[MemberPreferenceID] = @memberPreferenceID")]
        IList<int> GetAllActiveMemberProvincePreferenceByMemberPreferenceID(long memberPreferenceID);

        #endregion
    }
}
