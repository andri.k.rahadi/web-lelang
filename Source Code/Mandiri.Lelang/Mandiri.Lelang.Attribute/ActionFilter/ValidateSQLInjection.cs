﻿using Mandiri.Lelang.Attribute.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Mandiri.Lelang.Helper.Extension;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.BusinessFacade;

namespace Mandiri.Lelang.Attribute.ActionFilter
{
    /// <summary>
    /// Action filter to validate sql injection in params
    /// </summary>
    public class ValidateSQLInjection : ActionFilterAttribute, IActionFilter
    {
        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            foreach (String param in filterContext.HttpContext.Request.QueryString)
            {
                //If is string
                if (!param.Contains("RequestVerificationToken"))
                {
                    if (SecurityHelper.ValidateSQLInjection(filterContext.HttpContext.Request.QueryString[param].Trim()))
                    {
                        LogHelper.LogException(new Exception(String.Format("Someone tried to do SQL Injection with this param: {0}", filterContext.HttpContext.Request.QueryString[param].Trim())));
                        filterContext.Controller.TempData["NotAuthorizedMessage"] = TextResources.NotAuthorizedAccess_Message;
                        if (filterContext.HttpContext.Request.IsAjaxRequest())
                        {
                            filterContext.Result = new JavaScriptResult() { Script = String.Format("window.location = '{0}'", ((Controller)filterContext.Controller).Url.Action("NotAuthorized", "Error")) };
                        }
                        else
                        {
                            filterContext.Result = new ViewResult() { ViewName = "NotAuthorized", TempData = filterContext.Controller.TempData };
                        }
                    }
                }
            }

            foreach (String param in filterContext.HttpContext.Request.Form)
            {
                if (!param.Contains("RequestVerificationToken"))
                {
                    //If is string
                    if (SecurityHelper.ValidateSQLInjection(filterContext.HttpContext.Request.Form[param].Trim()))
                    {
                        LogHelper.LogException(new Exception(String.Format("Someone tried to do SQL Injection with this param: {0}", filterContext.HttpContext.Request.Form[param].Trim())));
                        filterContext.Controller.TempData["NotAuthorizedMessage"] = TextResources.NotAuthorizedAccess_Message;
                        if (filterContext.HttpContext.Request.IsAjaxRequest())
                        {
                            filterContext.Result = new JavaScriptResult() { Script = String.Format("window.location = '{0}'", ((Controller)filterContext.Controller).Url.Action("NotAuthorized", "Error")) };
                        }
                        else
                        {
                            filterContext.Result = new ViewResult() { ViewName = "NotAuthorized", TempData = filterContext.Controller.TempData };
                        }
                    }
                }
            }

            this.OnActionExecuting(filterContext);
        }

        void IActionFilter.OnActionExecuted(ActionExecutedContext filterContext)
        {
            this.OnActionExecuted(filterContext);
        }
    }
}
