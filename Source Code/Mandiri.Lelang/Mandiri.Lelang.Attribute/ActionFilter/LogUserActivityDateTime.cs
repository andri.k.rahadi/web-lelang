﻿using Mandiri.Lelang.Attribute.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Mandiri.Lelang.Helper.Extension;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.Resources;
using System.Web.Routing;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.DataModel;
using System.Web;
using System.Web.Security;

namespace Mandiri.Lelang.Attribute.ActionFilter
{
    /// <summary>
    /// Action filter to log all user activity date time
    /// </summary>
    public class LogUserActivityDateTime : ActionFilterAttribute, IActionFilter
    {
        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(HttpContext.Current.User.Identity.Name)
                    && SecurityHelper.IsAuthenticated()
                    && !filterContext.ActionDescriptor.ActionName.Contains("Login"))
                {
                    int currentUserID = SecurityHelper.GetCurrentLoggedInUserID();
                    User currentUser = UserFacade.GetActiveUserByUserID(currentUserID).ReturnData;
                    if (currentUser != null)
                    {
                        if ((currentUser.SessionID != null && currentUser.SessionID != filterContext.HttpContext.Session.SessionID)
                            || (currentUser.LastActivity.HasValue && DateTime.Now.Subtract(currentUser.LastActivity.Value).TotalMinutes > 15))
                        {
                            //Force Logout
                            UserFacade.DoLogout(SecurityHelper.GetCurrentLoggedInUserID(), filterContext.HttpContext.Session.SessionID);
                            FormsAuthentication.SignOut();
                            SessionHelper.Clear();
                            filterContext.HttpContext.Session.Abandon();
                            if (filterContext.HttpContext.Request.IsAjaxRequest())
                            {
                                filterContext.Result = new JavaScriptResult() { Script = String.Format("window.location = '{0}'", ((Controller)filterContext.Controller).Url.Action("Login", "User")) };
                            }
                            else
                            {
                                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "Login", controller = "User" }));
                            }
                        }

                        AdministrationFacade.UpdateUserLastActivity(currentUserID);
                    }
                }
            }
            catch
            {
            }

            this.OnActionExecuting(filterContext);
        }

        void IActionFilter.OnActionExecuted(ActionExecutedContext filterContext)
        {
            this.OnActionExecuted(filterContext);
        }
    }
}
