﻿using Mandiri.Lelang.Attribute.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mandiri.Lelang.Helper.Extension;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.Resources;
using System.Web.Routing;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.DataModel;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;

namespace Mandiri.Lelang.Attribute.ActionFilter
{
    /// <summary>
    /// Action filter to validate JWT token
    /// </summary>
    public class ValidateJWTToken : ActionFilterAttribute
    {
        private bool _checkForExpiry { get; set; }

        public ValidateJWTToken(bool checkForExpiry = false)
        {
            this._checkForExpiry = checkForExpiry;
        }

        private String Base64UrlDecode(string input)
        {
            var output = input;
            output = output.Replace('-', '+'); // 62nd char of encoding
            output = output.Replace('_', '/'); // 63rd char of encoding
            switch (output.Length % 4) // Pad with trailing '='s
            {
                case 0: break; // No pad chars in this case
                case 2: output += "=="; break; // Two pad chars
                case 3: output += "="; break;  // One pad char
                default: throw new FormatException("Illegal base64url string!");
            }
            var converted = Convert.FromBase64String(output); // Standard base64 decoder
            return Encoding.UTF8.GetString(converted);
        }

        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            try
            {
                if (filterContext.Request != null && filterContext.Request.Headers != null && filterContext.Request.Headers.Count() > 0 && filterContext.Request.Headers.Contains("Authorization") && filterContext.Request.Headers.GetValues("Authorization") != null)
                {
                    var headerAuth = filterContext.Request.Headers.GetValues("Authorization");
                    var token = String.Empty;
                    if (headerAuth.Count() > 0 && headerAuth.First().Contains("Bearer"))
                    {
                        token = headerAuth.First().Replace("Bearer ", "");
                    }
                    if (!String.IsNullOrWhiteSpace(token))
                    {
                        try
                        {
                            var verification = SecurityHelper.VerifyJWTToken(token, ConfigurationManager.AppSettings["JWTSecretKey"], _checkForExpiry);
                            filterContext.Request.Properties["Token"] = token;
                            filterContext.Request.Properties["Payload"] = null;
                            filterContext.Request.Properties["NewToken"] = String.Empty;
                            filterContext.Request.Properties["VerificationMessage"] = String.Empty;
                            if (verification != null)
                            {
                                //Validate blacklist
                                var mobileAuthToken = MemberFacade.GetActiveMobileAuthTokenByMemberIDAndToken(long.Parse(verification["memberID"].ToString()), token);
                                if (mobileAuthToken.IsSuccess && mobileAuthToken.ReturnData != null)
                                {
                                    //memberID, fullName, email, salt
                                    filterContext.Request.Properties["Payload"] = verification;

                                    //Renew token
                                    if (this._checkForExpiry)
                                    {
                                        try
                                        {
                                            var newToken = MemberFacade.RenewMobileAuthToken(long.Parse(verification["memberID"].ToString()), token, null);
                                            if (newToken.IsSuccess && newToken.ReturnData != null)
                                            {
                                                //Set the new token
                                                filterContext.Request.Properties["NewToken"] = newToken.ReturnData.Token;
                                            }
                                            else
                                            {
                                                //Set the old token
                                                filterContext.Request.Properties["NewToken"] = token;
                                                filterContext.Request.Properties["VerificationMessage"] = "Renewing token failed";
                                            }
                                        }
                                        catch
                                        {
                                            //Set the old token
                                            filterContext.Request.Properties["NewToken"] = token;
                                            filterContext.Request.Properties["VerificationMessage"] = "Renewing token failed";
                                        }
                                    }
                                }
                                else
                                {
                                    filterContext.Request.Properties["VerificationMessage"] = "Access denied or invalid token";
                                }
                            }
                            else
                            {
                                filterContext.Request.Properties["VerificationMessage"] = "Access denied or invalid token";
                            }
                        }
                        catch
                        {
                            //Token has expired
                            var payload = Base64UrlDecode(token.Split('.')[1]);
                            var jsonPayload = JObject.Parse(payload);
                            var updatedToken = MemberFacade.MemberLogout(long.Parse(jsonPayload.GetValue("memberID").ToString()), true, token, null);
                            filterContext.Request.Properties["NewToken"] = String.Empty;
                            filterContext.Request.Properties["VerificationMessage"] = "Token has expired";
                        }
                    }
                }
            }
            catch
            {
            }

            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(HttpActionExecutedContext filterContext)
        {
            //Dispose all properties
            try
            {
                filterContext.Request.Properties["Token"] = null;
                filterContext.Request.Properties["Payload"] = null;
                filterContext.Request.Properties["NewToken"] = null;
                filterContext.Request.Properties["VerificationMessage"] = null;
            }
            catch
            {
            }

            base.OnActionExecuted(filterContext);
        }
    }
}
