﻿using Mandiri.Lelang.Attribute.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Mandiri.Lelang.Helper.Extension;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.Resources;
using Mandiri.Lelang.BusinessFacade;

namespace Mandiri.Lelang.Attribute.ActionFilter
{
    /// <summary>
    /// Action filter to manage group access authorization and setting all the Allow* flag
    /// </summary>
    public class AuthorizeAccess : ActionFilterAttribute, IActionFilter
    {
        /// <summary>
        /// The site map id or menu id
        /// </summary>
        public String SiteMapID { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="siteMapID">The site map id or menu id</param>
        public AuthorizeAccess(String siteMapID)
        {
            this.SiteMapID = siteMapID;
        }

        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (SecurityHelper.IsAuthenticated())
            {
                //Check whether the user is allowed or not
                if (!UserFacade.IsUserAllowedAccess(this.SiteMapID, SecurityHelper.GetCurrentLoggedInUserID()))
                {
                    filterContext.Controller.TempData["NotAuthorizedMessage"] = TextResources.NotAuthorizedAccess_Message;
                    filterContext.Result = new ViewResult() { ViewName = "NotAuthorized", TempData = filterContext.Controller.TempData };
                }
                //TODO: Add ViewBag.AllowDelete, ViewBag.AllowUpdate, ViewBag.AllowDelete           
            }

            this.OnActionExecuting(filterContext);
        }

        void IActionFilter.OnActionExecuted(ActionExecutedContext filterContext)
        {
            this.OnActionExecuted(filterContext);
        }
    }
}
