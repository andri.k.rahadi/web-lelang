﻿using Mandiri.Lelang.Attribute.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Mandiri.Lelang.Helper.Extension;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.Resources;
using System.Web.Routing;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.DataModel;

namespace Mandiri.Lelang.Attribute.ActionFilter
{
    /// <summary>
    /// Action filter to log all page visit
    /// </summary>
    public class LogPageVisit : ActionFilterAttribute, IActionFilter
    {
        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            PageVisit pageVisit = new PageVisit();

            try
            {
                if (filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToUpper() != "IMAGE" && !filterContext.ActionDescriptor.ActionName.ToUpper().Contains("AJAX"))
                {
                    pageVisit.SiteMapID = String.Format("{0} - {1}", filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToUpper(), filterContext.ActionDescriptor.ActionName.ToUpper());
                    if (filterContext.HttpContext.Request.UrlReferrer != null)
                    {
                        String urlReferrer = filterContext.HttpContext.Request.UrlReferrer.PathAndQuery.ToLower();
                        if (urlReferrer.Contains("asset/detail"))
                        {
                            pageVisit.URLReferrerSiteMapID = "ASSET - DETAIL";
                            String assetID = filterContext.HttpContext.Request.UrlReferrer.Segments[filterContext.HttpContext.Request.UrlReferrer.Segments.Length - 1];
                            pageVisit.AssetID = long.Parse(assetID);
                        }
                        else if (urlReferrer.Contains("/asset/search"))
                        {
                            pageVisit.URLReferrerSiteMapID = "ASSET - SEARCH";
                        }
                        else if (urlReferrer.Contains("/schedule/list"))
                        {
                            pageVisit.URLReferrerSiteMapID = "SCHEDULE - LIST";
                        }
                        else if (urlReferrer.Contains("/schedule/asset"))
                        {
                            pageVisit.URLReferrerSiteMapID = "SCHEDULE - ASSET";
                        }
                        else if (urlReferrer.Contains("/content/terms"))
                        {
                            pageVisit.URLReferrerSiteMapID = "CONTENT - TERMS";
                        }
                        else if (urlReferrer.Contains("/content/faq"))
                        {
                            pageVisit.URLReferrerSiteMapID = "CONTENT - FAQ";
                        }
                        else if (urlReferrer.Contains("/content/about"))
                        {
                            pageVisit.URLReferrerSiteMapID = "CONTENT - ABOUT";
                        }
                        else if (urlReferrer.Contains("/contact/contact-us"))
                        {
                            pageVisit.URLReferrerSiteMapID = "CONTACT - CONTACT US";
                        }
                        else if (urlReferrer.Contains("/member/login"))
                        {
                            pageVisit.URLReferrerSiteMapID = "MEMBER - LOGIN";
                        }
                        else if (urlReferrer.Contains("/member/register"))
                        {
                            pageVisit.URLReferrerSiteMapID = "MEMBER - REGISTER";
                        }
                        else if (urlReferrer.Contains("/member/favorite"))
                        {
                            pageVisit.URLReferrerSiteMapID = "MEMBER - FAVORITE";
                        }
                        else if (urlReferrer.Contains("/home"))
                        {
                            pageVisit.URLReferrerSiteMapID = "HOME - INDEX";
                        }
                        else
                        {
                            pageVisit.URLReferrerSiteMapID = "HOME - INDEX";
                        }
                    }
                    if (pageVisit.SiteMapID.Contains("DETAIL"))
                    {
                        String assetID = filterContext.HttpContext.Request.Url.Segments[filterContext.HttpContext.Request.Url.Segments.Length - 1];
                        pageVisit.AssetID = long.Parse(assetID);
                    }
                    //Try getting the IP Address
                    try
                    {
                        //The X-Forwarded-For (XFF) HTTP header field is a de facto standard for identifying the originating IP address of a 
                        //client connecting to a web server through an HTTP proxy or load balancer
                        //String ip = filterContext.HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] + "," + filterContext.HttpContext.Request.UserHostAddress + "," + filterContext.HttpContext.Request.ServerVariables["REMOTE_ADDR"];
                        /*if (string.IsNullOrEmpty(ip))
                        {
                            ip = filterContext.HttpContext.Request.UserHostAddress;
                            if (string.IsNullOrEmpty(ip))
                            {
                                ip = filterContext.HttpContext.Request.ServerVariables["REMOTE_ADDR"];
                            }
                        }*/
                        pageVisit.IPAddress = filterContext.HttpContext.Session.SessionID;//ip;
                    }
                    catch
                    {
                        pageVisit.IPAddress = String.Empty;
                    }
                    pageVisit.AdditionalParams = filterContext.HttpContext.Request.Url.Query;

                    AdministrationFacade.AddNewPageVisit(pageVisit);
                    AdministrationFacade.UpdateTotalVisitor();

                    TotalVisitor totalVisitor = AdministrationFacade.GetTotalVisitor().ReturnData;
                    filterContext.Controller.ViewBag.TotalVisitorCount = totalVisitor.TotalVisitorCount;
                    filterContext.Controller.ViewBag.TotalTodayVisitorCount = totalVisitor.TotalTodayVisitorCount;
                }
            }
            catch
            {
            }

            this.OnActionExecuting(filterContext);
        }

        void IActionFilter.OnActionExecuted(ActionExecutedContext filterContext)
        {
            this.OnActionExecuted(filterContext);
        }
    }
}
