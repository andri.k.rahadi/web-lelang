﻿using Mandiri.Lelang.Attribute.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Mandiri.Lelang.Helper.Extension;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.Resources;
using System.Web.Routing;
using Mandiri.Lelang.BusinessFacade;

namespace Mandiri.Lelang.Attribute.ActionFilter
{
    /// <summary>
    /// Action filter to manage group access action authorization
    /// </summary>
    public class AuthorizeAction : ActionFilterAttribute, IActionFilter
    {
        /// <summary>
        /// The site map id or menu id
        /// </summary>
        public String SiteMapID { get; set; }

        /// <summary>
        /// The action type
        /// </summary>
        public AuthorizationActionType ActionType { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="siteMapID">The site map id or menu id</param>
        /// <param name="actionType">The action type</param>
        public AuthorizeAction(String siteMapID, AuthorizationActionType actionType)
        {
            this.SiteMapID = siteMapID;
            this.ActionType = actionType;
        }

        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool isAuthorized = true;
            if (SecurityHelper.IsAuthenticated())
            {
                //Check whether the user is allowed or not
                if (ActionType == AuthorizationActionType.AllowAdd)
                {
                    if (!UserFacade.IsUserAllowedAdd(this.SiteMapID, SecurityHelper.GetCurrentLoggedInUserID()))
                    {
                        filterContext.Controller.TempData["NotAuthorizedMessage"] = TextResources.NotAuthorizedAdd_Message;
                        isAuthorized = false;
                    }
                }
                else if (ActionType == AuthorizationActionType.AllowDelete)
                {
                    if (!UserFacade.IsUserAllowedDelete(this.SiteMapID, SecurityHelper.GetCurrentLoggedInUserID()))
                    {
                        filterContext.Controller.TempData["NotAuthorizedMessage"] = TextResources.NotAuthorizedDelete_Message;
                        isAuthorized = false;
                    }
                }
                else if (ActionType == AuthorizationActionType.AllowUpdate)
                {
                    if (!UserFacade.IsUserAllowedUpdate(this.SiteMapID, SecurityHelper.GetCurrentLoggedInUserID()))
                    {
                        filterContext.Controller.TempData["NotAuthorizedMessage"] = TextResources.NotAuthorizedUpdate_Message;
                        isAuthorized = false;
                    }
                }
                if (!isAuthorized)
                {
                    if (filterContext.HttpContext.Request.IsAjaxRequest())
                    {
                        filterContext.Result = new JavaScriptResult() { Script = String.Format("window.location = '{0}'", ((Controller)filterContext.Controller).Url.Action("NotAuthorized", "Error")) };
                    }
                    else
                    {
                        filterContext.Result = new ViewResult() { ViewName = "NotAuthorized", TempData = filterContext.Controller.TempData };
                    }
                }                
            }

            this.OnActionExecuting(filterContext);
        }

        void IActionFilter.OnActionExecuted(ActionExecutedContext filterContext)
        {
            this.OnActionExecuted(filterContext);
        }
    }
}
