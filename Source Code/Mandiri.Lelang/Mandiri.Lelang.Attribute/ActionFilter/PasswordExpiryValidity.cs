﻿using Mandiri.Lelang.Attribute.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Mandiri.Lelang.Helper.Extension;
using Mandiri.Lelang.Helper;
using Mandiri.Lelang.Resources;
using System.Web.Routing;
using Mandiri.Lelang.BusinessFacade;
using Mandiri.Lelang.DataModel;
using System.Web;

namespace Mandiri.Lelang.Attribute.ActionFilter
{
    /// <summary>
    /// Action filter to check for password expiry validity
    /// </summary>
    public class PasswordExpiryValidity : ActionFilterAttribute, IActionFilter
    {
        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(HttpContext.Current.User.Identity.Name)
                    && SecurityHelper.IsAuthenticated()
                    && !filterContext.ActionDescriptor.ActionName.Contains("ChangePassword")
                    && !filterContext.ActionDescriptor.ActionName.Contains("Logout")
                    && !filterContext.ActionDescriptor.ActionName.Contains("Login"))
                {
                    int currentUserID = SecurityHelper.GetCurrentLoggedInUserID();
                    User currentUser = UserFacade.GetActiveUserByUserID(currentUserID).ReturnData;
                    if (!currentUser.LastPasswordChanged.HasValue ||
                        DateTime.Today.Subtract(new DateTime(currentUser.LastPasswordChanged.Value.Year,
                            currentUser.LastPasswordChanged.Value.Month,
                            currentUser.LastPasswordChanged.Value.Day)).TotalDays >= 90)
                    {
                        if (filterContext.HttpContext.Request.IsAjaxRequest())
                        {
                            filterContext.Result = new JavaScriptResult() { Script = String.Format("window.location = '{0}'", ((Controller)filterContext.Controller).Url.Action("ChangePassword", "User")) };
                        }
                        else
                        {
                            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "ChangePassword", controller = "User" }));
                        }
                    }
                }
            }
            catch
            {
            }

            this.OnActionExecuting(filterContext);
        }

        void IActionFilter.OnActionExecuted(ActionExecutedContext filterContext)
        {
            this.OnActionExecuted(filterContext);
        }
    }
}
