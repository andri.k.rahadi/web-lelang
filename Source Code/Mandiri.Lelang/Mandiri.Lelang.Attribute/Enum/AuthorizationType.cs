﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mandiri.Lelang.Attribute.Enum
{
    /// <summary>
    /// Group authorization type enum
    /// </summary>
    public enum AuthorizationActionType
    {
        /// <summary>
        /// Add action
        /// </summary>
        AllowAdd,
        /// <summary>
        /// Delete action
        /// </summary>
        AllowDelete,
        /// <summary>
        /// Update action
        /// </summary>
        AllowUpdate
    }
}
