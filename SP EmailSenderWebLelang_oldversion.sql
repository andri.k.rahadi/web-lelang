USE [MandiriLelangDB]
GO
/****** Object:  StoredProcedure [dbo].[SP_SendEmailAssetReminder]    Script Date: 2/9/2017 1:54:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SP Reminder
CREATE PROCEDURE [dbo].[SP_SendEmailAssetReminder]	
AS
BEGIN
-- Start T-SQL
DECLARE @lastEmailSentDateTime DATETIME
DECLARE @recipients VARCHAR(MAX)
DECLARE @subject VARCHAR(MAX)
DECLARE @message VARCHAR(MAX)
DECLARE @message_details_favorite VARCHAR(MAX)
DECLARE @message_details_preference VARCHAR(MAX)
DECLARE @memberID BIGINT
DECLARE @fullName VARCHAR(MAX)
DECLARE @email VARCHAR(MAX)
DECLARE @emailReminderPeriod INT
DECLARE @contentAvailable INT
DECLARE @MinPrice DECIMAL(18, 2)
DECLARE @MaxPrice DECIMAL(18, 2)

SELECT @lastEmailSentDateTime = CAST(Value AS DATETIME) FROM SystemSetting WHERE SystemSettingID = 'LastEmailSentDateTime' 
SELECT @emailReminderPeriod = CAST(Value AS INT) FROM SystemSetting WHERE SystemSettingID = 'EmailReminderPeriod' 

DECLARE db_cursor CURSOR FOR  
SELECT DISTINCT MemberID, FullName, Email 
FROM Member
WHERE IsActive = 1

BEGIN TRY

OPEN db_cursor   
FETCH NEXT FROM db_cursor INTO @memberID, @fullName, @email   

WHILE @@FETCH_STATUS = 0   
BEGIN   
	--INIT VARIABLES
	SET @message_details_favorite = ''
	SET @message_details_preference = ''
	SET @message = ''
	SET @recipients = @email
	SET @subject = 'Bank Mandiri Web Lelang'
	SET @contentAvailable = 0

	SET @message = N'<html><head>
		<style type="text/css">
		#box-table
		{
		font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
		font-size: 12px;
		text-align: center;
		border-collapse: collapse;
		}
		#box-table th
		{
		font-size: 13px;
		font-weight: normal;
		background: #0F2B5B;
		color: white;
		}
		#box-table td
		{
		border-right: 1px solid #aabcfe;
		border-left: 1px solid #aabcfe;
		border-bottom: 1px solid #aabcfe;
		color: #669;
		}
		tr:nth-child(odd) { background-color:#eee; }
		tr:nth-child(even) { background-color:#fff; }
		</style>
		</head>Hi ' + @fullName + '<br/>'

	--START FAVORITE
	IF (SELECT COUNT(A.AssetID)
		FROM Asset A
		INNER JOIN AuctionSchedule S ON A.AuctionScheduleID = S.AuctionScheduleID
		INNER JOIN Category C ON A.CategoryID = C.CategoryID
		INNER JOIN MemberFavorite F ON A.AssetID = F.AssetID
		INNER JOIN Member M ON M.MemberID = F.MemberID
		WHERE DATEADD(dd, 0, DATEDIFF(dd, 0, S.ScheduleDate)) > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))
		AND A.CurrentStatusID IN (4) AND A.NewPrice > 0 AND A.NewPrice IS NOT NULL
		AND A.IsActive = 1 AND S.IsActive = 1 AND F.IsFavorite = 1 AND F.IsActive = 1 AND M.IsActive = 1 AND M.MemberID = @memberID
		AND (DATEADD(dd, 0, DATEDIFF(dd, 0, ISNULL(A.DateTimeUpdated, A.DateTimeCreated))) > DATEADD(dd, 0, DATEDIFF(dd, 0, @lastEmailSentDateTime))
		OR (DATEDIFF(dd, GETDATE(), S.ScheduleDate) >= 0 AND DATEDIFF(dd, GETDATE(), S.ScheduleDate) <= @emailReminderPeriod))) > 0
	BEGIN  
		SET @message_details_favorite = '<h1>Daftar Agunan Favorit Anda</h1><table id="box-table"><tr><th>Foto</th><th>Kategori</th><th>Limit Lelang</th><th>Alamat Pelaksanaan Lelang</th><th>Jadwal Lelang</th><th></th></tr>'

		SELECT @message_details_favorite = @message_details_favorite + '<tr><td>' + '<img src=''http://lelangdev.bankmandiri.co.id/image/get/' + CAST(A.AssetID AS VARCHAR(MAX)) + '/1/1'' style=''width: 120px; height: 120px;'' alt=''Foto'' />' + '</td><td>' + C.Name + '</td><td>' + 'Rp. ' + FORMAT(A.NewPrice, 'N') + '</td><td>' + S.AuctionAddress + '</td><td>' + CAST(FORMAT(S.ScheduleDate, 'dd MMM yyyy') AS VARCHAR(20)) + ' ' + CAST(FORMAT(S.ScheduleTime, 'HH:mm') AS VARCHAR(20)) + ' ' + CASE WHEN S.Timezone = 1 THEN 'WIB' WHEN S.Timezone = 2 THEN 'WIT' WHEN S.Timezone = 3 THEN 'WITA' END + '</td><td>' + '<a href=''http://lelangdev.bankmandiri.co.id/asset/detail/' + CAST(A.AssetID AS VARCHAR(MAX)) + '''>Detil Agunan</a>' + '</td></tr>'
		FROM Asset A
		INNER JOIN AuctionSchedule S ON A.AuctionScheduleID = S.AuctionScheduleID
		INNER JOIN Category C ON A.CategoryID = C.CategoryID
		INNER JOIN MemberFavorite F ON A.AssetID = F.AssetID
		INNER JOIN Member M ON M.MemberID = F.MemberID
		WHERE DATEADD(dd, 0, DATEDIFF(dd, 0, S.ScheduleDate)) > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))
		AND A.CurrentStatusID IN (4) AND A.NewPrice > 0 AND A.NewPrice IS NOT NULL
		AND A.IsActive = 1 AND S.IsActive = 1 AND F.IsFavorite = 1 AND F.IsActive = 1 AND M.IsActive = 1 AND M.MemberID = @memberID
		AND (DATEADD(dd, 0, DATEDIFF(dd, 0, ISNULL(A.DateTimeUpdated, A.DateTimeCreated))) > DATEADD(dd, 0, DATEDIFF(dd, 0, @lastEmailSentDateTime))
		OR (DATEDIFF(dd, GETDATE(), S.ScheduleDate) >= 0 AND DATEDIFF(dd, GETDATE(), S.ScheduleDate) <= @emailReminderPeriod))		

		SET @message = CONCAT(@message, @message_details_favorite, '</table>')

		SET @contentAvailable = @contentAvailable + 1
	END
	--END FAVORITE

	--START PREFERENCE
	SELECT @MinPrice = MinP.Value, @MaxPrice = MaxP.Value FROM
	MemberPreference M
	LEFT JOIN MinMaxPrice MinP ON M.MinPriceID = MinP.MinMaxPriceID
	LEFT JOIN MinMaxPrice MaxP ON M.MaxPriceID = MaxP.MinMaxPriceID
	WHERE MemberID = @memberID

	IF (SELECT COUNT(A.AssetID)
		FROM Asset A
		INNER JOIN AuctionSchedule S ON A.AuctionScheduleID = S.AuctionScheduleID
		INNER JOIN Category C ON A.CategoryID = C.CategoryID
		LEFT JOIN MemberFavorite F ON A.AssetID = F.AssetID	AND F.IsFavorite = 1 AND F.IsActive = 1	and f.memberid = @memberid
		LEFT JOIN Member M ON M.MemberID = F.MemberID AND M.MemberID = @memberID AND M.IsActive = 1
		WHERE DATEADD(dd, 0, DATEDIFF(dd, 0, S.ScheduleDate)) > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))
		AND A.CurrentStatusID IN (4) AND A.NewPrice > 0 AND A.NewPrice IS NOT NULL
		AND A.IsActive = 1 AND S.IsActive = 1
		AND (DATEADD(dd, 0, DATEDIFF(dd, 0, ISNULL(A.DateTimeUpdated, A.DateTimeCreated))) > DATEADD(dd, 0, DATEDIFF(dd, 0, @lastEmailSentDateTime))
		OR (DATEDIFF(dd, GETDATE(), S.ScheduleDate) >= 0 AND DATEDIFF(dd, GETDATE(), S.ScheduleDate) <= @emailReminderPeriod))
		AND F.AssetID IS NULL
		AND ((@minprice is not null and @maxprice is not null and A.NewPrice BETWEEN @MinPrice AND @MaxPrice) or (@maxprice is not null and a.newprice <= @maxprice) or (@minprice is not null and  a.newprice >= @minprice) or (@minprice is null and @maxprice is null))
		AND A.CityID IN (SELECT DISTINCT CityID FROM MemberCityPreference A JOIN MemberPreference B ON A.MemberPreferenceID = B.MemberPreferenceID WHERE A.IsActive = 1 AND B.IsActive = 1 AND B.MemberID = @memberID)
		AND A.CategoryID IN (SELECT DISTINCT CategoryID FROM MemberCategoryPreference A JOIN MemberPreference B ON A.MemberPreferenceID = B.MemberPreferenceID WHERE A.IsActive = 1 AND B.IsActive = 1 AND B.MemberID = @memberID)) > 0
	BEGIN  
		SET @message_details_preference = '<h1>Daftar Agunan Sesuai Minat Anda</h1><table id="box-table"><tr><th>Foto</th><th>Kategori</th><th>Limit Lelang</th><th>Alamat Pelaksanaan Lelang</th><th>Jadwal Lelang</th><th></th></tr>'		

		SELECT @message_details_preference = @message_details_preference + '<tr><td>' + '<img src=''http://lelangdev.bankmandiri.co.id/image/get/' + CAST(A.AssetID AS VARCHAR(MAX)) + '/1/1'' style=''width: 120px; height: 120px;'' alt=''Foto'' />' + '</td><td>' + C.Name + '</td><td>' + 'Rp. ' + FORMAT(A.NewPrice, 'N') + '</td><td>' + S.AuctionAddress + '</td><td>' + CAST(FORMAT(S.ScheduleDate, 'dd MMM yyyy') AS VARCHAR(20)) + ' ' + CAST(FORMAT(S.ScheduleTime, 'HH:mm') AS VARCHAR(20)) + ' ' + CASE WHEN S.Timezone = 1 THEN 'WIB' WHEN S.Timezone = 2 THEN 'WIT' WHEN S.Timezone = 3 THEN 'WITA' END + '</td><td>' + '<a href=''http://lelangdev.bankmandiri.co.id/asset/detail/' + CAST(A.AssetID AS VARCHAR(MAX)) + '''>Detil Agunan</a>' + '</td></tr>'
		FROM Asset A
		INNER JOIN AuctionSchedule S ON A.AuctionScheduleID = S.AuctionScheduleID
		INNER JOIN Category C ON A.CategoryID = C.CategoryID
		LEFT JOIN MemberFavorite F ON A.AssetID = F.AssetID	AND F.IsFavorite = 1 AND F.IsActive = 1	and f.memberid = @memberid
		LEFT JOIN Member M ON M.MemberID = F.MemberID AND M.MemberID = @memberID AND M.IsActive = 1
		WHERE DATEADD(dd, 0, DATEDIFF(dd, 0, S.ScheduleDate)) > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))
		AND A.CurrentStatusID IN (4) AND A.NewPrice > 0 AND A.NewPrice IS NOT NULL
		AND A.IsActive = 1 AND S.IsActive = 1
		AND (DATEADD(dd, 0, DATEDIFF(dd, 0, ISNULL(A.DateTimeUpdated, A.DateTimeCreated))) > DATEADD(dd, 0, DATEDIFF(dd, 0, @lastEmailSentDateTime))
		OR (DATEDIFF(dd, GETDATE(), S.ScheduleDate) >= 0 AND DATEDIFF(dd, GETDATE(), S.ScheduleDate) <= @emailReminderPeriod))
		AND F.AssetID IS NULL
		AND ((@minprice is not null and @maxprice is not null and A.NewPrice BETWEEN @MinPrice AND @MaxPrice) or (@maxprice is not null and a.newprice <= @maxprice) or (@minprice is not null and  a.newprice >= @minprice) or (@minprice is null and @maxprice is null))
		AND A.CityID IN (SELECT DISTINCT CityID FROM MemberCityPreference A JOIN MemberPreference B ON A.MemberPreferenceID = B.MemberPreferenceID WHERE A.IsActive = 1 AND B.IsActive = 1 AND B.MemberID = @memberID)
		AND A.CategoryID IN (SELECT DISTINCT CategoryID FROM MemberCategoryPreference A JOIN MemberPreference B ON A.MemberPreferenceID = B.MemberPreferenceID WHERE A.IsActive = 1 AND B.IsActive = 1 AND B.MemberID = @memberID)

		SET @message = CONCAT(@message, @message_details_preference, '</table>')

		SET @contentAvailable = @contentAvailable + 1
	END
	--END PREFERENCE

	SET @message = CONCAT(@message, '</html>')

	IF @contentAvailable > 0
	BEGIN
		EXEC [msdb]..sp_send_dbmail
			@profile_name = 'Web Lelang Mailer',
			@recipients = @recipients,
			@subject = @subject,
			@body = @message,
			@body_format = 'HTML'
	END

	FETCH NEXT FROM db_cursor INTO @memberID, @fullName, @email   
END   

END TRY
BEGIN CATCH
END CATCH

CLOSE db_cursor   
DEALLOCATE db_cursor

UPDATE SystemSetting SET Value = CAST(FORMAT(GETDATE(), 'yyyy-MM-dd') AS VARCHAR(10)) WHERE SystemSettingID = 'LastEmailSentDateTime'

END