USE [MandiriLelangDB]
GO
/****** Object:  StoredProcedure [dbo].[SP_SendEmailMessageReminder]    Script Date: 10/2/2017 12:37:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_SendEmailMessageReminder]	
AS
BEGIN
-- Start T-SQL
DECLARE @recipients VARCHAR(MAX)
DECLARE @subject VARCHAR(MAX)
DECLARE @message VARCHAR(MAX)
DECLARE @userID BIGINT
DECLARE @fullName VARCHAR(MAX)
DECLARE @email VARCHAR(MAX)
DECLARE @branchID VARCHAR(MAX)
DECLARE @messageCount INT

DECLARE db_cursor CURSOR FOR  
SELECT DISTINCT U.UserID, U.FullName, U.Email, U.BranchID, COUNT(M.MessageStatusID)
FROM [MandiriLelangDB].[dbo].[User] U
INNER JOIN Message M ON U.BranchID = M.BranchID
INNER JOIN MessageStatus MS ON M.MessageStatusID = MS.MessageStatusID
WHERE MS.IsComplete != 1 AND U.IsActive = 1 AND U.BranchID != -1
AND GETDATE() > DATEADD(dd, 1, M.DateTimeCreated) AND M.IsActive = 1	
GROUP BY U.UserID, U.FullName, U.Email, U.BranchID

BEGIN TRY

OPEN db_cursor   
FETCH NEXT FROM db_cursor INTO @userID, @fullName, @email, @branchID, @messageCount

WHILE @@FETCH_STATUS = 0   
BEGIN   
	--INIT VARIABLES
	SET @message = ''
	SET @recipients = @email
	SET @subject = 'Bank Mandiri Web Lelang'

	SET @message = N'<html><head></head><body>Hi ' + @fullName + '<br/><br/>' +
					'Anda memiliki ' + LTRIM(RTRIM(STR(@messageCount))) + ' pesan yang harus di follow up.'
	
	SET @message = CONCAT(@message, '</body></html>')
	
	IF @messageCount > 0
	BEGIN
		EXEC [msdb]..sp_send_dbmail
			@profile_name = 'Mandiri Lelang',
			@recipients = @recipients,
			@subject = @subject,
			@body = @message,
			@body_format = 'HTML'
	END

	FETCH NEXT FROM db_cursor INTO @userID, @fullName, @email, @branchID, @messageCount
END   

END TRY
BEGIN CATCH
END CATCH

CLOSE db_cursor   
DEALLOCATE db_cursor

--UPDATE SystemSetting SET Value = CAST(FORMAT(GETDATE(), 'yyyy-MM-dd') AS VARCHAR(10)) WHERE SystemSettingID = 'LastEmailSentDateTime'

END