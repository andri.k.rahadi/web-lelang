//
//  AppConfig.h
//  Shiocase
//
//  Created by Asep Mulyana on 1/28/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Accounts/Accounts.h>

#define kAPISecret              @"my1Mand1r1"
#define kAPIPrivateKey          @"86342850c8fbf6219ea754dfb37ab97fb27881daa391a3590d8c81430671d70e"

//#define ROOT_URL                @"https://10.204.35.147" // internal mandiri
#define ROOT_URL                @"http://ricardoalexander-001-site2.etempurl.com/v3"
//#define ROOT_URL                @"https://lelang.bankmandiri.co.id"

#define kURLMain                ROOT_URL @"/" //@"/api/v2/"
#define kURLMasterData          kURLMain @"master/data"
#define KURLAsset               kURLMain @"asset/all"
#define KURLAssetSegera         kURLMain @"asset/soon"
#define KURLAssetHot            kURLMain @"asset/hot"
#define KURLAssetTerbaru        kURLMain @"asset/latest"
#define KURLAssetTerdekat       kURLMain @"asset/nearest"
#define KURLAssetSukarela       kURLMain @"asset/direct-sell"
#define kURLSchedule            kURLMain @"asset/auctionschedule"
#define kURLScheduleDetail      kURLMain @"asset/byschedule?auctionScheduleID=%@"
#define kURLDetailAsset         kURLMain @"asset/get/%@?cityID=%zd"
#define kURLContactUs           kURLMain @"contact/contactus"
#define kURLFacebook            kURLMain @"member/fb"
#define kURLRegister            kURLMain @"member/register"
#define kURLLogin               kURLMain @"member/login"
#define kURLForgot              kURLMain @"member/reset-password"
#define kURLProfile             kURLMain @"member/profile"
#define kURLUpdateProfile       kURLMain @"member/update"
#define kURLLogout              kURLMain @"member/logout"
#define kURLAddFavorite         kURLMain @"member/favorite"
#define kURLRemoveFavorite      kURLMain @"member/unfavorite"
#define kURLDownloadSchedule    kURLMain @"asset/download-auctionschedule"
#define kURLListFavorite        kURLMain @"asset/favorite"
#define kURLVideo               kURLMain @"video/all"


#define MandiriLelangMasterDataDidUpdatedNotification     @"com.mandiri.lelang.masterdata.updatedNotification"
#define MandiriLelangLocationNameDidUpdatedNotification   @"com.mandiri.lelang.locationname.updatedNotification"

typedef enum {
    FontRegular,
    FontCondensed,
    FontButtonKotak,
    FontLight
} AppFont;


static inline NSString *urlDecode(NSString *str){
    NSString *decoded = (NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (CFStringRef)str, CFSTR(""), kCFStringEncodingUTF8);
    return [decoded autorelease];
}

static inline NSString *urlEncoded(NSString *str){
    NSString *encoded = (NSString *)CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)str, CFSTR(""), CFSTR(""), kCFStringEncodingUTF8);
    return encoded;
}

static inline NSString *removeSpace(NSString *str){
    return [[str stringByReplacingOccurrencesOfString:@" " withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

static inline UIFont* FONT(AppFont type, CGFloat size){
    UIFont *font = [UIFont fontWithName:@"LeagueGothic-CondensedRegular" size:size];
    switch (type) {
        case FontRegular:{
            font = [UIFont fontWithName:@"LeagueGothic-Regular" size:size];
        }
            break;
        case FontCondensed:{
            font = [UIFont fontWithName:@"LeagueGothic-CondensedRegular" size:size];
        }
            break;
        case FontButtonKotak:{
            font = [UIFont fontWithName:@"Gotham-Medium" size:size];
        }
            break;
        case FontLight:{
            font = [UIFont fontWithName:@"Gotham-Book" size:size];
        }
            break;
            
        default:{
        }
            break;
    }
    
    return font;
}

@interface AppConfig : NSObject

+(AppConfig *)sharedConfig;
+(NSString *)createUUID;

+(NSData *)dataForImage:(UIImage *)image;

-(void)requestTwitterAccountWithCompletion:(void (^)(BOOL, ACAccount *))completion;

@property (nonatomic, retain) NSString *deviceIdentifier;
@property (nonatomic, retain) NSString *latitude;
@property (nonatomic, retain) NSString *longitude;
@property (nonatomic, retain) NSNumber *cityID;
@property (nonatomic, retain) NSNumber *provID;
@property (nonatomic, retain) NSString *locationName;

@property (nonatomic, unsafe_unretained) BOOL loginFromFB;
@property (nonatomic, unsafe_unretained) NSString *username;
@property (nonatomic, unsafe_unretained) NSString *sessionID;
@property (nonatomic, unsafe_unretained) NSDictionary *loginInfo;
@property (nonatomic, unsafe_unretained) ACAccount *twAccount;

@property (nonatomic, unsafe_unretained) NSString *masterdataTimestamp;

@end
