//
//  AppConfig.m
//  Shiocase
//
//  Created by Asep Mulyana on 1/28/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "AppConfig.h"
#import <CoreLocation/CoreLocation.h>
#import <Accounts/Accounts.h>
#import "MLDatabase.h"
#import "ASIHTTPRequest+Mandiri.h"
#import "JSON.h"

#define GOOGLE_API_KEY      @"AIzaSyAzlAI-1OmG5HYr3k6VG7Q8BDHZMQW15b4"

static AppConfig *sharedObject = nil;

@interface AppConfig()<CLLocationManagerDelegate>{
    CLLocationManager *locationManager;
}

@property (nonatomic, retain) NSDate *lastHit;

@end


@implementation AppConfig

@synthesize deviceIdentifier, latitude, longitude;
@synthesize username, sessionID;
@synthesize lastHit;
@synthesize twAccount;
@synthesize masterdataTimestamp;
@synthesize cityID, provID, locationName;

+(AppConfig *)sharedConfig{
    if(sharedObject == nil){
        sharedObject = [[AppConfig alloc] init];
    }
    
    return sharedObject;
}

+(NSString *)createUUID{
    CFUUIDRef uuid = CFUUIDCreate(nil);
    NSString *uuidString = (NSString *)CFUUIDCreateString(nil, uuid);
    CFRelease(uuid);
    return [uuidString autorelease];
}

+(NSData *)dataForImage:(UIImage *)image{
    UIGraphicsBeginImageContext(image.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1., -1);
    CGContextDrawImage(context, CGRectMake(0, 0, image.size.width, image.size.height), image.CGImage);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *data = UIImageJPEGRepresentation(newImage, 1.);
    return data;
}

#pragma  mark -
-(void)dealloc{
    [deviceIdentifier release];
    [latitude release];
    [longitude release];
    [lastHit release];
    
    self.cityID = nil;
    self.provID = nil;
    self.locationName = nil;
    
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) {
        self.latitude = @"0.0";
        self.longitude = @"0.0";
        self.lastHit = [NSDate dateWithTimeIntervalSince1970:0];
        
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [locationManager requestWhenInUseAuthorization];
            [locationManager requestAlwaysAuthorization];
        }
        [locationManager setDesiredAccuracy:kCLLocationAccuracyKilometer];
        [locationManager startUpdatingLocation];
    }
    return self;
}

-(NSString *)deviceIdentifier{
    NSString *device = [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceIdentifier"];
    
    if(device == nil){
        if([[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)]){
            device = [UIDevice currentDevice].identifierForVendor.UUIDString;
        }else{
            device = [AppConfig createUUID];
        }
        self.deviceIdentifier = device;
    }
    
    return device;
}

-(void)setDeviceIdentifier:(NSString *)_deviceIdentifier{
    return [[NSUserDefaults standardUserDefaults] setValue:_deviceIdentifier forKey:@"deviceIdentifier"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - delegate Location Manager
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    [locationManager stopUpdatingLocation];
    
    self.latitude = [NSString stringWithFormat:@"%lf", newLocation.coordinate.latitude];
    self.longitude = [NSString stringWithFormat:@"%lf", newLocation.coordinate.longitude];
    
    [locationManager performSelector:@selector(startUpdatingLocation) withObject:nil afterDelay:60.*5];
    NSLog(@"location did update %lf %lf", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
//    CLGeocoder *coder = [[CLGeocoder alloc] init];
//    [coder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
//        for(CLPlacemark *one in placemarks){
//            NSLog(@"hahasyduf %@ %@", one.subLocality, one);
//            
//            NSString *p = one.administrativeArea.length>0?one.administrativeArea:@"";
//            NSString *c = one.subAdministrativeArea.length>0?one.subAdministrativeArea:p;
//            
//            self.provID = [[MLProvince sharedData] provIDforName:p];
//            self.cityID = [[MLCity sharedData] cityIDforName:c];
//            self.locationName = c==p?p:[[c stringByAppendingString:@", "] stringByAppendingString:p];
//            
//            [[NSNotificationCenter defaultCenter] postNotificationName:MandiriLelangLocationNameDidUpdatedNotification object:nil];
//            
//            break;
//        }
//        [coder release];
//    }];
    
//    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:newLocation.coordinate completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
//        for(GMSAddress* addressObj in [response results])
//        {
//            NSLog(@"%@", addressObj);
//            
//            NSString *p = addressObj.administrativeArea.length>0?addressObj.administrativeArea:@"";
//            NSString *c = addressObj.subLocality.length>0?addressObj.administrativeArea:p;
//            
//            self.provID = [[MLProvince sharedData] provIDforName:p];
//            self.cityID = [[MLCity sharedData] cityIDforName:c];
//            self.locationName = c==p?p:[[c stringByAppendingString:@", "] stringByAppendingString:p];
//            
//            [[NSNotificationCenter defaultCenter] postNotificationName:MandiriLelangLocationNameDidUpdatedNotification object:nil];
//            
//            break;
//        }
//    }];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?language=id&latlng=%lf,%lf&key=%@", newLocation.coordinate.latitude, newLocation.coordinate.longitude, GOOGLE_API_KEY]]];
    [request setCompletionBlock:^{
        NSLog(@"geocode %@", request.responseString);
        NSArray *results = [[request.responseString JSONValue] valueForKey:@"results"];
        for(NSDictionary *dict in results){
            NSString *p = @"";
            NSString *c = @"";
            
            NSArray *comp = [dict valueForKey:@"address_components"];
            for(NSDictionary *com in comp){
                if([self dictResult:com hasType:@"administrative_area_level_1"]){
                    p = [[com valueForKey:@"long_name"] retain];
                }else if([self dictResult:com hasType:@"administrative_area_level_2"]){
                    c = [[com valueForKey:@"long_name"] retain];
                }
            }

            self.provID = [[MLProvince sharedData] provIDforName:p];
            self.cityID = [[MLCity sharedData] cityIDforName:c];
            self.locationName = c;

            if(c.length > 0 && p.length > 0){
                [c release];
                [p release];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:MandiriLelangLocationNameDidUpdatedNotification object:nil];
            
                return;
            }
            
            [c release];
            [p release];
        }
    }];
    [request startAsynchronous];
}

-(BOOL)dictResult:(NSDictionary *)dict hasType:(NSString *)_str{
    NSArray *arr = [dict valueForKey:@"types"];
    
    for(NSString *str in arr){
        if([str isEqualToString:_str]){
            return YES;
        }
    }
    
    return NO;
}

#pragma custom property

-(void)setLoginFromFB:(BOOL)loginFromFB{
    [[NSUserDefaults standardUserDefaults] setValue:@1 forKey:@"fb"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)loginFromFB{
    NSString *usr = [[NSUserDefaults standardUserDefaults] valueForKey:@"fb"];
    return usr.boolValue;
}

-(void)setUsername:(NSString *)newusername{
    if(newusername){
        [[NSUserDefaults standardUserDefaults] setValue:newusername forKey:@"username"];
    }else{
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"username"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString *)username{
    NSString *usr = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    
    if(usr == nil){
        return @"anonim";
    }
    
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
}

-(NSString *)sessionID{
    NSString *session = [[NSUserDefaults standardUserDefaults] valueForKey:@"sessionID"];
    
    if(session == nil){
        session = @"";
    }
    
    return session;
}

-(void)setSessionID:(NSString *)newsessionID{
    if(newsessionID){
        [[NSUserDefaults standardUserDefaults] setValue:newsessionID forKey:@"sessionID"];
    }else{
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"sessionID"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(ACAccount *)twAccount{
    NSString *accID = [[NSUserDefaults standardUserDefaults] valueForKey:@"twAccount"];
    
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccount *acc = [account accountWithIdentifier:accID];
    ACAccountType *account_type_twitter = [account accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    acc.accountType = account_type_twitter;
    [account release];
    
    return acc;
}

-(void)setTwAccount:(ACAccount *)newtwAccount{
    if(newtwAccount){
        [[NSUserDefaults standardUserDefaults] setValue:newtwAccount.identifier forKey:@"twAccount"];
    }else{
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"twAccount"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSDictionary *)loginInfo{
    NSDictionary *info = [[NSUserDefaults standardUserDefaults] valueForKey:@"logininfo"];
    return info;
}

-(void)setLoginInfo:(NSDictionary *)_loginInfo{
    if(_loginInfo){
        [[NSUserDefaults standardUserDefaults] setValue:_loginInfo forKey:@"logininfo"];
    }else{
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"logininfo"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];

}

-(void)setMasterdataTimestamp:(NSString *)newmasterdataTimestamp{
    if(newmasterdataTimestamp){
        [[NSUserDefaults standardUserDefaults] setValue:newmasterdataTimestamp forKey:@"mdts"];
    }else{
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"mdts"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString *)masterdataTimestamp{
    NSString *usr = [[NSUserDefaults standardUserDefaults] valueForKey:@"mdts"];
    
    if(usr == nil){
        return @"";
    }
    
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"mdts"];
}

#pragma mark - implement method
-(void)requestTwitterAccountWithCompletion:(void (^)(BOOL, ACAccount *))completion{
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    // Request access from the user to use their Twitter accounts.
    [account requestAccessToAccountsWithType:accountType withCompletionHandler:^(BOOL granted, NSError *error)
         {
             if (granted == YES)
             {
                 // Populate array with all available Twitter accounts
                 NSArray *arrayOfAccounts = [account accountsWithAccountType:accountType];
                 
                 if(arrayOfAccounts.count < 1){
                     completion(NO, nil);
                 }else{
                     ACAccount *acc = [arrayOfAccounts lastObject];
                     [AppConfig sharedConfig].twAccount = acc;
                     completion(YES, acc);

                 }
             }else{
                 completion(NO, nil);
             }
             
             [account release];
             NSLog(@"minta permisi twitter selesai");
         }];
}

@end
