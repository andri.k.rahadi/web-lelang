//
//  ASIHTTPRequest+Mandiri.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/24/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "ASIHTTPRequest+Mandiri.h"
#import "AppConfig.h"
#import "HMACUtils.h"

@implementation ASIHTTPRequest (Mandiri)

-(void)encryptParams{
    NSDateFormatter *format = [[[NSDateFormatter alloc] init] autorelease];
    [format setDateFormat:@"yyyyMMddHHmmss"];
    
    NSString *query = self.url.query;
    NSString *qAddition = [NSString stringWithFormat:@"&nonce=%@&timestamp=%@", [AppConfig sharedConfig].deviceIdentifier, [format stringFromDate:[NSDate date]]];
    NSString *completequery = [NSString stringWithFormat:@"%@%@", query, qAddition];
    NSString *token = [HMACUtils encodedStringWithMessage:[NSString stringWithFormat:@"%@|%@", completequery, kAPISecret] andKey:kAPIPrivateKey];
    
    NSString *newURL = [NSString stringWithFormat:@"%@%@&token=%@", self.url.absoluteString, qAddition, token];
    self.url = [NSURL URLWithString:newURL];
}

+ (id)requestAuthWithURL:(NSURL *)newURL {
    ASIHTTPRequest *req = [ASIHTTPRequest requestWithURL:newURL];
    if ([AppConfig sharedConfig].sessionID.length > 0) {
        [req addRequestHeader:@"Authorization" value:[NSString stringWithFormat:@"Bearer %@", [AppConfig sharedConfig].sessionID]];
    }
    return req;
}

@end

@implementation ASIFormDataRequest (Mandiri)

-(void)encryptParams{
}

+ (id)requestAuthWithURL:(NSURL *)newURL {
    ASIFormDataRequest *req = [ASIFormDataRequest requestWithURL:newURL];
    if ([AppConfig sharedConfig].sessionID.length > 0) {
        [req addRequestHeader:@"Authorization" value:[NSString stringWithFormat:@"Bearer %@", [AppConfig sharedConfig].sessionID]];
    }
    return req;
}

@end
