//
//  HMACUtils.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/19/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HMACUtils : NSObject

+ (NSString *)encodedStringWithMessage:(NSString *)message andKey:(NSString *)key;

@end
