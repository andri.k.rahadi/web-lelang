//
//  ASIHTTPRequest+Mandiri.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/24/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@interface ASIHTTPRequest (Mandiri)

-(void)encryptParams;
+ (id)requestAuthWithURL:(NSURL *)newURL;

@end

@interface ASIFormDataRequest (Mandiri)

-(void)encryptParams;
+ (id)requestAuthWithURL:(NSURL *)newURL;

@end
