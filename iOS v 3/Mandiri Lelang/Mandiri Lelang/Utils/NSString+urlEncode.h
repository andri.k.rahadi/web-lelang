//
//  NSString+urlEncode.h
//  USeeTV
//
//  Created by Asep Mulyana on 4/6/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (urlEncode)
- (NSString *)urlencode;
@end
