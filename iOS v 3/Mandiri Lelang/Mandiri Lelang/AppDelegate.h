//
//  AppDelegate.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/9/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IIViewDeckController;

@interface AppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (retain, nonatomic) IIViewDeckController *mainViewController;
@property (retain, nonatomic) UINavigationController *homeViewController;

+(AppDelegate *)currentDelegate;
-(void)openMenu;
-(void)closeMenu;

@end
