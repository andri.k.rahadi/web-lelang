//
//  AppDelegate.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/9/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "AppDelegate.h"
#import "IIViewDeckController.h"
#import "LeftMenuViewController.h"
#import "ViewController.h"
#import "SplashscreenViewController.h"
#import "AppConfig.h"
#import "ASIHTTPRequest+Mandiri.h"
#import "JSON.h"
#import "NSString+urlEncode.h"
#import "MLDatabase.h"
#import "GAI.h"
#import "LaunchscreenViewController.h"
#import "NSString+sha256.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@implementation AppDelegate 

@synthesize mainViewController = _mainViewController;
@synthesize homeViewController = _homeViewController;

- (void)dealloc
{
    self.mainViewController = nil;
    self.homeViewController = nil;
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Create tracker instance.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-55127332-2"];
    
    SplashscreenViewController *splash = [[[SplashscreenViewController alloc] initWithNibName:@"SplashscreenViewController" bundle:nil] autorelease];
    
    self.window.rootViewController = splash;
    
    [self.window makeKeyAndVisible];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    [MLDatabase initiateDatabase];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    [self fetchMasterData];

    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

#pragma mark - custom property
-(void)nextScreen{
    if([AppConfig sharedConfig].sessionID.length > 0){
        [AppDelegate currentDelegate].window.rootViewController = (UIViewController *)[AppDelegate currentDelegate].mainViewController;
    }else{
        LaunchscreenViewController *launch = [[[LaunchscreenViewController alloc] initWithNibName:@"LaunchscreenViewController" bundle:nil] autorelease];
        UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:launch] autorelease];
        nav.navigationBarHidden = YES;
        
        self.window.rootViewController = nav;
    }
}

+(AppDelegate *)currentDelegate{
    return [UIApplication sharedApplication].delegate;
}

-(void)openMenu{
    [self.mainViewController openLeftViewAnimated:YES];
}

-(void)closeMenu{
    [self.mainViewController closeLeftViewAnimated:YES];
}

-(UINavigationController *)homeViewController{
    if(_homeViewController == nil){
        ViewController *home = [[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
        UINavigationController *homeNav = [[[UINavigationController alloc] initWithRootViewController:home] autorelease];
        homeNav.navigationBarHidden = YES;
        
        self.homeViewController = homeNav;
    }
    
    return _homeViewController;
}

-(IIViewDeckController *)mainViewController{
    if(_mainViewController == nil){
        LeftMenuViewController *menu = [[[LeftMenuViewController alloc] initWithNibName:@"LeftMenuViewController" bundle:nil] autorelease];
        IIViewDeckController *deck = [[[IIViewDeckController alloc] initWithCenterViewController:self.homeViewController leftViewController:menu] autorelease];
        deck.centerhiddenInteractivity = IIViewDeckCenterHiddenNotUserInteractiveWithTapToClose;
        deck.leftLedge = 64.;
        
        self.mainViewController = deck;
    }
    
    return _mainViewController;
}

-(void)fetchMasterData{
    NSString *url = [NSString stringWithFormat:@"%@?lastTimestamp=%@", kURLMasterData, @""];
    ASIHTTPRequest *request = [ASIHTTPRequest requestAuthWithURL:[NSURL URLWithString:url]];
    [request encryptParams];
    [request setCompletionBlock:^{
        NSLog(@"master data: %@\n %@", request.url.absoluteString, request.responseString);
        NSDictionary *root = [request.responseString JSONValue];
        [AppConfig sharedConfig].masterdataTimestamp = [root valueForKey:@"lastTimestamp"];
        
        if ([[root valueForKey:@"isTokenExpired"] boolValue]) {
            [AppConfig sharedConfig].sessionID = root[@"token"];
        }
        
//        dispatch_queue_t lowQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
//
//        dispatch_async(lowQueue, ^{
            NSArray *master = [root valueForKey:@"masterData"];
            for(NSDictionary *dict in master){
                NSString *type = [dict valueForKey:@"masterDataType"];
                
                if([type isEqualToString:@"Category"]){
                    [[MLCategory sharedData] loadData:[dict valueForKey:@"data"]];
                }else if([type isEqualToString:@"Province"]){
                    [[MLProvince sharedData] loadData:[dict valueForKey:@"data"]];
                }else if([type isEqualToString:@"City"]){
                    [[MLCity sharedData] loadData:[dict valueForKey:@"data"]];
                }else if([type isEqualToString:@"MinMaxPrice"]){
                    [[MLPrice sharedData] loadData:[dict valueForKey:@"data"]];
                }else if([type isEqualToString:@"DocumentType"]){
                    [[MLDocumentType sharedData] loadData:[dict valueForKey:@"data"]];
                }else if([type isEqualToString:@"ContactUsType"]){
                    [[MLContactUsType sharedData] loadData:[dict valueForKey:@"data"]];
                }else if([type isEqualToString:@"Branch"]){
                    [[MLBranch sharedData] loadData:[dict valueForKey:@"data"]];
                }else if([type isEqualToString:@"WebContent"]){
                    [[MLWebContent sharedData] loadData:[dict valueForKey:@"data"]];
                }else if([type isEqualToString:@"SystemSetting"]){
                    [[MLSettings sharedData] loadData:[dict valueForKey:@"data"]];
                }else if([type isEqualToString:@"KPKNL"]){
                    [[MLKnpnl sharedData] loadData:[dict valueForKey:@"data"]];
                }
            }
            
            dispatch_async(mainQueue, ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:MandiriLelangMasterDataDidUpdatedNotification object:nil];
                [self nextScreen];
            });
//        });
    }];
    [request setFailedBlock:^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please check your internet connection." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }];
    [request startAsynchronous];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    exit(0);
}

@end
