//
//  UICellRefreshLoading.m
//  Logbook
//
//  Created by Asep Mulyana on 11/25/13.
//  Copyright (c) 2013 asepmoels. All rights reserved.
//

#import "UICellRefreshLoading.h"
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>

@interface UICellRefreshLoading(){
    IBOutlet UIView *subView;
    //AVAudioPlayer *sound;
    
    BOOL isLoading;
}

@property (nonatomic) BOOL asHeader;
@property (nonatomic, unsafe_unretained) IBOutlet UIImageView *loading;

@end

@implementation UICellRefreshLoading

@synthesize asHeader, loading, delegate, enabled;

- (void)dealloc
{
//    [sound release];
    [super dealloc];
}

+(UICellRefreshLoading *)createNewAsHeader:(BOOL)isHeader forView:(UIView *)view{
    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"UICellRefreshLoading" owner:self options:nil];
    
    UICellRefreshLoading *me = nil;
    for(UIView *one in arr){
        if([one isKindOfClass:[self class]]){
            me = (UICellRefreshLoading *)one;
        }
    }
    
    if(isHeader){
        CGRect frame = me.frame;
        frame.origin.y = - frame.size.height;
        frame.size.width = view.frame.size.width;
        me.frame = frame;
    }else{
        CGRect frame = me.frame;
        frame.origin.y = 1000.;
        frame.size.width = view.frame.size.width;
        me.frame = frame;
    }
    /*
    UIImage *img = [UIImage animatedImageWithAnimatedGIFURL:[[NSBundle mainBundle] URLForResource:@"489" withExtension:@"GIF"]];
    me.loading.image = [img.images objectAtIndex:0];
    me.loading.animationImages = img.images;
    me.loading.animationDuration = 0.3;
    me.loading.animationRepeatCount = -1;*/
    me.enabled = YES;
    
    me.asHeader = isHeader;
    [view addSubview:me];
    
    return me;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder]){
//        NSError *error = nil;
//        NSURL *soundURL = [[NSBundle mainBundle] URLForResource:@"refresh" withExtension:@"wav"];
//        
//        sound = [[AVAudioPlayer alloc] initWithContentsOfURL:soundURL error:&error];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(void)setBackgroundColor:(UIColor *)backgroundColor{
    [super setBackgroundColor:[UIColor clearColor]];
    subView.backgroundColor = backgroundColor;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)setAsHeader:(BOOL)_asHeader{
    asHeader = _asHeader;
    if(asHeader){
        subView.layer.anchorPoint = CGPointMake(0.5, 1.);
        subView.layer.position = CGPointMake(self.center.x, self.frame.size.height);
    }else{
        subView.layer.anchorPoint = CGPointMake(0.5, 0.);
        subView.layer.position = CGPointMake(self.center.x, 0);
    }
}

-(void)setEnabled:(BOOL)_enabled{
    enabled = _enabled;
    
    self.hidden = !enabled;
}

-(void)putarLoading{
    if(!isLoading)return;
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationRepeatCount:INT16_MAX];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(putarLoading)];
    CGAffineTransform t = self.loading.transform;
    t = CGAffineTransformRotate(t, M_PI*-0.99);
    self.loading.transform = t;
    [UIView commitAnimations];
}

-(void)startLoading{NSLog(@"start loading");
    isLoading = YES;
    [self putarLoading];
    
    UIScrollView *scroll = (UIScrollView *)self.superview;
    if(self.asHeader){
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDelay:0.25];
        scroll.contentInset = UIEdgeInsetsMake(self.frame.size.height, 0, 0, 0);
        [UIView commitAnimations];
    }else{
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDelay:0.25];
        scroll.contentInset = UIEdgeInsetsMake(0, 0, self.frame.size.height, 0);
        [UIView commitAnimations];
    }
//    [sound play];
}

-(void)stopLoading{
    [self performSelector:@selector(stop) withObject:nil afterDelay:0.5];
}

-(void)stop{
    [loading stopAnimating];
    
    UIScrollView *scroll = (UIScrollView *)self.superview;
    
    if(asHeader){
        [scroll setContentOffset:CGPointZero animated:YES];
    }else{
        
    }
    [self performSelector:@selector(setNoLoading) withObject:nil afterDelay:0.28];
}

-(void)setNoLoading{
    UIScrollView *scroll = (UIScrollView *)self.superview;
    
    isLoading = NO;
    [self.loading.layer removeAllAnimations];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(UICellRefreshLoadingDidFinishLoading:)]){
        [self.delegate UICellRefreshLoadingDidFinishLoading:self];
    }
    
    if(!asHeader){
        [self checkLoadingPosition];
    }
    
    scroll.contentInset = UIEdgeInsetsZero;
}

-(BOOL)isLoading{
    return isLoading;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(isLoading || !self.enabled)return;
    //NSLog(@"offset => %@", NSStringFromCGPoint(scrollView.contentOffset));
    if(asHeader){
        CGFloat offset = scrollView.contentOffset.y;
        offset = MIN(offset, 0);
        
        CGFloat prosen = offset / -self.frame.size.height;
        
        [CATransaction begin];
        [CATransaction setDisableActions:YES];
        CATransform3D t = CATransform3DIdentity;
        t = CATransform3DScale(t, MIN(prosen, 1), MIN(prosen, 1), 1.);
        t = CATransform3DRotate(t, (1.-prosen) * 2 * M_PI, 0, 0, -1.);
        self.loading.layer.transform = t;
        [CATransaction commit];
    }else{
        CGFloat offset = scrollView.contentOffset.y - (scrollView.contentSize.height - scrollView.frame.size.height);
        offset = MAX(offset, 0);

        CGFloat prosen = offset / self.frame.size.height;
        
        [CATransaction begin];
        [CATransaction setDisableActions:YES];
        CATransform3D t = CATransform3DIdentity;
        t = CATransform3DScale(t, MIN(prosen, 1), MIN(prosen, 1), 1.);
        t = CATransform3DRotate(t, (1.-prosen) * 2 * M_PI, 0, 0, -1.);
        self.loading.layer.transform = t;
        [CATransaction commit];
        
        [self checkLoadingPosition];
    }
}

-(void)checkLoadingPosition{
    UIScrollView *scroll = (UIScrollView *)self.superview;
    CGRect frame = self.frame;
    
    if(scroll.contentSize.height < scroll.frame.size.height){
        if(frame.origin.y != scroll.frame.size.height){
            frame.origin.y = scroll.frame.size.height;
            frame.size.width = scroll.frame.size.width;
            self.frame = frame;
        }
    }else{
        if(frame.origin.y != scroll.contentSize.height){
            frame.origin.y = scroll.contentSize.height;
            frame.size.width = scroll.frame.size.width;
            self.frame = frame;
        }
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if(isLoading || !self.enabled)return;
    //NSLog(@"%@ %d", NSStringFromCGPoint(scrollView.contentOffset), decelerate);
    if(asHeader){
        if(scrollView.contentOffset.y <= -self.frame.size.height){
            [self startLoading];
            
            if(self.delegate && [self.delegate respondsToSelector:@selector(UICellRefreshLoadingDidBeginLoading:)]){
                [self.delegate UICellRefreshLoadingDidBeginLoading:self];
            }
        }else if(scrollView.contentOffset.y <= 0){
            [scrollView setContentOffset:CGPointZero animated:YES];
        }
    }else{
        CGFloat selisih = (scrollView.contentSize.height - scrollView.frame.size.height);
        if(scrollView.contentOffset.y >= selisih + self.frame.size.height){
            [self startLoading];
            
            if(self.delegate && [self.delegate respondsToSelector:@selector(UICellRefreshLoadingDidBeginLoading:)]){
                [self.delegate UICellRefreshLoadingDidBeginLoading:self];
            }
        }else if(selisih < scrollView.contentOffset.y){
            [scrollView setContentOffset:CGPointMake(0, selisih) animated:YES];
        }
    }
}

@end
