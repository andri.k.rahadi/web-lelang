//
//  VideoCell.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 9/27/17.
//  Copyright © 2017 Asep Mulyana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"
#import "YTPlayerView.h"

@interface VideoCell : UICollectionViewCell

@property (retain, nonatomic) IBOutlet EGOImageView *imgThumb;
@property (retain, nonatomic) IBOutlet UILabel *lblTitle;
@property (retain, nonatomic) IBOutlet YTPlayerView *preview;

@end
