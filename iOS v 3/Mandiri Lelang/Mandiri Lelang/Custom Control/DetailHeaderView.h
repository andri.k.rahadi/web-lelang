//
//  DetailHederView.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/25/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailHeaderView : UIView

@property (nonatomic, unsafe_unretained) IBOutlet UILabel *titleLabel;

@end
