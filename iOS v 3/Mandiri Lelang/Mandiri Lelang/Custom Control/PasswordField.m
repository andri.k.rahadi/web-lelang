//
//  PasswordField.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 1/25/17.
//  Copyright © 2017 Asep Mulyana. All rights reserved.
//

#import "PasswordField.h"

@implementation PasswordField

- (void)awakeFromNib {
    [super awakeFromNib];
    
    CGRect frame = CGRectMake(0, 0, self.frame.size.height, self.frame.size.height);
    UIButton *btn = [[UIButton alloc] initWithFrame:frame];
    [btn setImage:[UIImage imageNamed:@"icon-show-password-active"] forState:UIControlStateSelected];
    [btn setImage:[UIImage imageNamed:@"icon-show-password"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(toggleShowPassword:) forControlEvents:UIControlEventTouchUpInside];
    self.rightView = btn;
    self.rightViewMode = UITextFieldViewModeAlways;
    [btn release];
}

- (void)toggleShowPassword:(UIButton *) sender {
    sender.selected = !sender.selected;
    self.secureTextEntry = !sender.selected;
    self.font = nil;
    self.font = [UIFont systemFontOfSize:14];
}

@end
