//
//  UICellRefreshLoading.h
//  Logbook
//
//  Created by Asep Mulyana on 11/25/13.
//  Copyright (c) 2013 asepmoels. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UICellRefreshLoading;

@protocol UICellRefreshLoadingDelegate <NSObject>
-(void)UICellRefreshLoadingDidBeginLoading:(UICellRefreshLoading *)view;
-(void)UICellRefreshLoadingDidFinishLoading:(UICellRefreshLoading *)view;
@end

@interface UICellRefreshLoading : UIView

+(UICellRefreshLoading *)createNewAsHeader:(BOOL)isHeader forView:(UIView *)view;
-(void)startLoading;
-(void)stopLoading;
-(void)scrollViewDidScroll:(UIScrollView *)scrollView;
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
-(BOOL)isLoading;

@property (nonatomic, unsafe_unretained) id<UICellRefreshLoadingDelegate>delegate;
@property (nonatomic) BOOL enabled;

@end


