//
//  VideoCell.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 9/27/17.
//  Copyright © 2017 Asep Mulyana. All rights reserved.
//

#import "VideoCell.h"

@implementation VideoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)dealloc {
    [_imgThumb release];
    [_lblTitle release];
    [_preview release];
    [super dealloc];
}
@end
