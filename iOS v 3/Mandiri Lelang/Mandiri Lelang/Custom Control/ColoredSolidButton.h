//
//  ColoredSolidButton.h
//  Homservis
//
//  Created by MobileForce UX on 10/29/15.
//
//

#import <UIKit/UIKit.h>

@interface ColoredSolidButton : UIButton

@property (nonatomic) BOOL dontRoundedCorner;

@end
