//
//  DetailHederView.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/25/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "DetailHeaderView.h"

@implementation DetailHeaderView

@synthesize titleLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
