//
//  ItemCell.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/9/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "ItemCell.h"

@implementation ItemCell

@synthesize thumb, name, detail, date, category, hargaAwal, tanggal, hargaAwalContainer, hotPrice;

- (void)awakeFromNib
{
    // Initialization code
    self.hotPrice.layer.masksToBounds = YES;
    self.hotPrice.layer.cornerRadius = self.hotPrice.frame.size.height * 0.5;
    
    self.eAuction.layer.masksToBounds = YES;
    self.eAuction.layer.cornerRadius = self.hotPrice.frame.size.height * 0.5;
    self.eAuction.backgroundColor = [UIColor colorWithRed:0x5b/255. green:0xc0/255. blue:0xde/255. alpha:1];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateLayout{
    self.name.frame = CGRectMake(11, 218, 299, 21);
    [self.name sizeToFit];
    
    CGRect frame = self.hotPrice.frame;
    frame.origin.x = self.name.frame.origin.x + self.name.frame.size.width + 3;
    self.hotPrice.frame = frame;
    
    frame = self.eAuction.frame;
    if (self.hotPrice.hidden){
        frame.origin.x = self.name.frame.origin.x + self.name.frame.size.width + 3;
    } else {
        frame.origin.x = self.hotPrice.frame.origin.x + self.hotPrice.frame.size.width + 3;
    }
    self.eAuction.frame = frame;
}

@end
