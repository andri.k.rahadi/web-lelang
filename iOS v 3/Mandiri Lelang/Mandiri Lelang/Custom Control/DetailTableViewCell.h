//
//  DetailTableViewCell.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/9/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TTTAttributedLabel;

@interface DetailTableViewCell : UITableViewCell

@property (nonatomic, unsafe_unretained) IBOutlet UILabel *titleLabel;
@property (nonatomic, unsafe_unretained) IBOutlet TTTAttributedLabel *valueLabel;

@end
