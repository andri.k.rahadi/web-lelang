//
//  ColoredSolidButton.m
//  Homservis
//
//  Created by MobileForce UX on 10/29/15.
//
//

#import "ColoredSolidButton.h"
#import "Tools.h"

@implementation ColoredSolidButton

-(void)awakeFromNib{
    [self setBackgroundImage:[Tools solidImageForColor:self.backgroundColor withSize:self.frame.size ] forState:UIControlStateNormal];
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = _dontRoundedCorner?0:3.;
    self.dontRoundedCorner = YES;
}

- (void)setDontRoundedCorner:(BOOL)dontRoundedCorner{
    _dontRoundedCorner = dontRoundedCorner;
    self.layer.cornerRadius = dontRoundedCorner?0:3.;
}

@end
