//
//  JadwalTableViewCell.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/17/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "JadwalTableViewCell.h"
#import "TTTAttributedLabel.h"

@interface JadwalTableViewCell(){
    IBOutlet UILabel *telpLabel;
}

@end

@implementation JadwalTableViewCell

@synthesize tanggal, penyelenggara, alamat, telepon, separator;

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateLayout{
    CGRect frame = penyelenggara.frame;
    frame.size.width = 293.;
    penyelenggara.frame = frame;
    
    frame = alamat.frame;
    frame.size.width = 293.;
    alamat.frame = frame;
    
    frame = telepon.frame;
    frame.size.width = 293.;
    telepon.frame = frame;
    
    [penyelenggara sizeToFit];
    [alamat sizeToFit];
    [telepon sizeToFit];
    
    frame = self.alamat.frame;
    frame.origin.y = penyelenggara.frame.origin.y + penyelenggara.frame.size.height + 3;
    self.alamat.frame = frame;
    
    frame = telpLabel.frame;
    frame.origin.y = alamat.frame.origin.y + alamat.frame.size.height + 7;
    telpLabel.frame = frame;
    
    frame = telepon.frame;
    frame.origin.y = telpLabel.frame.origin.y + telpLabel.frame.size.height + 3;
    frame.size.width = 293.;
    telepon.frame = frame;
}

-(CGFloat)height{
    return 19. + telepon.frame.size.height + telepon.frame.origin.y;
}

@end
