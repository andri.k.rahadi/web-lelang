//
//  ItemCell.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/9/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EGOImageView;
@class TTTAttributedLabel;

@interface ItemCell : UITableViewCell

@property (nonatomic, unsafe_unretained) IBOutlet EGOImageView *thumb;
@property (nonatomic, unsafe_unretained) IBOutlet UILabel *name;
@property (nonatomic, unsafe_unretained) IBOutlet UILabel *detail;
@property (nonatomic, unsafe_unretained) IBOutlet UILabel *date;
@property (nonatomic, unsafe_unretained) IBOutlet UILabel *category;
@property (nonatomic, unsafe_unretained) IBOutlet TTTAttributedLabel *hargaAwal;
@property (nonatomic, unsafe_unretained) IBOutlet UILabel *tanggal;
@property (nonatomic, unsafe_unretained) IBOutlet UIView *hargaAwalContainer;
@property (nonatomic, unsafe_unretained) IBOutlet UILabel *hotPrice;
@property (nonatomic, unsafe_unretained) IBOutlet UILabel *eAuction;
@property (nonatomic, unsafe_unretained) IBOutlet UIButton *btnFav;

-(void)updateLayout;

@end
