//
//  JadwalTableViewCell.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/17/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//


#import <UIKit/UIKit.h>

@class TTTAttributedLabel;

@interface JadwalTableViewCell : UITableViewCell

@property (nonatomic, unsafe_unretained) IBOutlet UILabel *tanggal;
@property (nonatomic, unsafe_unretained) IBOutlet UILabel *penyelenggara;
@property (nonatomic, unsafe_unretained) IBOutlet UILabel *alamat;
@property (nonatomic, unsafe_unretained) IBOutlet TTTAttributedLabel *telepon;
@property (nonatomic, unsafe_unretained) IBOutlet UIView *separator;

-(void)updateLayout;
-(CGFloat)height;

@end
