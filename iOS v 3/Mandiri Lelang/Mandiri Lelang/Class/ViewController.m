//
//  ViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/24/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "ViewController.h"
#import "Tools.h"
#import "SearchViewController.h"
#import "SukarelaViewController.h"
#import "SegeraViewController.h"
#import "HotPriceViewController.h"
#import "MoreViewController.h"

@interface ViewController (){
    IBOutlet UIView *tabContainer;
    IBOutlet UIView *mainView;
    
    UINavigationController *currentVC;
}

@property (nonatomic, retain) UINavigationController *searchVC;
@property (nonatomic, retain) UINavigationController *segeraVC;
@property (nonatomic, retain) UINavigationController *hotPriceVC;
@property (nonatomic, retain) UINavigationController *terbaruVC;
@property (nonatomic, retain) UINavigationController *terdekatVC;

-(IBAction)switchTab:(id)sender;

@end

@implementation ViewController

@synthesize searchVC, segeraVC, hotPriceVC, terbaruVC, terdekatVC;

- (void)dealloc
{
    self.searchVC = nil;
    self.segeraVC = nil;
    self.hotPriceVC = nil;
    self.terbaruVC = nil;
    self.terdekatVC = nil;
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    tabContainer.backgroundColor = [UIColor colorWithRed:0. green:60./255 blue:120./255 alpha:1.];
    
    for(UIButton *btn in tabContainer.subviews){
        if([btn isKindOfClass:[UIButton class]]){
            [btn setBackgroundImage:[Tools solidImageForColor:[UIColor colorWithWhite:1. alpha:0.1] withSize:btn.frame.size] forState:UIControlStateSelected];
            btn.titleLabel.numberOfLines = 0;
            [btn.titleLabel sizeToFit];
            btn.titleLabel.textAlignment = NSTextAlignmentCenter;
            CGPoint center = btn.titleLabel.center;
            center.x = 32.;
            btn.titleLabel.center = center;
        }
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(currentVC == nil){
        [self switchToTab:0];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions
-(void)switchToTab:(NSInteger )tab{
    for(UIButton *btn in tabContainer.subviews){
        if([btn isKindOfClass:[UIButton class]]){
            btn.selected = btn.tag == tab+1;
        }
    }
    
    switch (tab) {
        case 0:{
            if(self.searchVC == nil){
                SearchViewController *search = [[[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil] autorelease];
                UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:search] autorelease];
                nav.navigationBarHidden = YES;
                self.searchVC = nav;
            }
            
            if(currentVC == self.searchVC){
                [self.searchVC popToRootViewControllerAnimated:YES];
                return;
            }
            
            currentVC = self.searchVC;
            
            [currentVC.view removeFromSuperview];
            self.searchVC.view.frame = mainView.bounds;
            [mainView addSubview:self.searchVC.view];
            [mainView setNeedsLayout];
        }
            break;
        case 1:{
            if(self.terbaruVC == nil){
                SukarelaViewController *terbaru = [[[SukarelaViewController alloc] initWithNibName:@"SukarelaViewController" bundle:nil] autorelease];
                UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:terbaru] autorelease];
                nav.navigationBarHidden = YES;
                self.terbaruVC = nav;
            }
            
            if(currentVC == self.terbaruVC){
                [self.terbaruVC popToRootViewControllerAnimated:YES];
                return;
            }
            
            currentVC = self.terbaruVC;
            
            [currentVC.view removeFromSuperview];
            self.terbaruVC.view.frame = mainView.bounds;
            [mainView addSubview:self.terbaruVC.view];
            [mainView setNeedsLayout];
        }
            break;
        case 2:{
            if(self.segeraVC == nil){
                SegeraViewController *segera = [[[SegeraViewController alloc] initWithNibName:@"SegeraViewController" bundle:nil] autorelease];
                UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:segera] autorelease];
                nav.navigationBarHidden = YES;
                self.segeraVC = nav;
            }
            
            if(currentVC == self.segeraVC){
                [self.segeraVC popToRootViewControllerAnimated:YES];
                return;
            }
            
            currentVC = self.segeraVC;
            
            [currentVC.view removeFromSuperview];
            self.segeraVC.view.frame = mainView.bounds;
            [mainView addSubview:self.segeraVC.view];
            [mainView setNeedsLayout];
        }
            break;
        case 3:{
            if(self.hotPriceVC == nil){
                HotPriceViewController *hot = [[[HotPriceViewController alloc] initWithNibName:@"HotPriceViewController" bundle:nil] autorelease];
                UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:hot] autorelease];
                nav.navigationBarHidden = YES;
                self.hotPriceVC = nav;
            }
            
            if(currentVC == self.hotPriceVC){
                [self.hotPriceVC popToRootViewControllerAnimated:YES];
                return;
            }
            
            currentVC = self.hotPriceVC;
            
            [currentVC.view removeFromSuperview];
            self.hotPriceVC.view.frame = mainView.bounds;
            [mainView addSubview:self.hotPriceVC.view];
            [mainView setNeedsLayout];
        }
            break;
        case 4:{
            if(self.terdekatVC == nil){
                MoreViewController *dekat = [[[MoreViewController alloc] initWithNibName:@"MoreViewController" bundle:nil] autorelease];
                UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:dekat] autorelease];
                nav.navigationBarHidden = YES;
                self.terdekatVC = nav;
            }
            
            if(currentVC == self.terdekatVC){
                [self.terdekatVC popToRootViewControllerAnimated:YES];
                return;
            }
            
            currentVC = self.terdekatVC;
            
            [currentVC.view removeFromSuperview];
            self.terdekatVC.view.frame = mainView.bounds;
            [mainView addSubview:self.terdekatVC.view];
            [mainView setNeedsLayout];
        }
            break;
            
            
        default:
            break;
    }
}

-(void)switchTab:(UIButton *)sender{
    [self switchToTab:sender.tag-1];
}

@end
