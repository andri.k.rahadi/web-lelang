//
//  ProfileViewController.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 10/29/16.
//  Copyright © 2016 Asep Mulyana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController

@property (nonatomic) BOOL fromRegister;
@property (nonatomic) BOOL fromMenu;

@end
