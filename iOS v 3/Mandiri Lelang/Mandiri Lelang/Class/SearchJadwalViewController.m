//
//  SearchJadwalViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 10/30/16.
//  Copyright © 2016 Asep Mulyana. All rights reserved.
//

#import "SearchJadwalViewController.h"
#import "ComboTableViewController.h"
#import "MLDatabase.h"

@interface SearchJadwalViewController () <ComboTableViewControllerDelegate>

@property (retain, nonatomic) IBOutlet UIControl *vireForm;
@property (retain, nonatomic) IBOutlet UITextField *txtFrom;
@property (retain, nonatomic) IBOutlet UITextField *txtTo;
@property (retain, nonatomic) IBOutlet UITextField *txtkpknl;
@property (retain, nonatomic) IBOutlet UIDatePicker *datePickerFrom;
@property (retain, nonatomic) IBOutlet UIDatePicker *datePickerTo;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *constScrollBottom;

@property (nonatomic, retain) NSArray *branchesArray;

@end



@implementation SearchJadwalViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI:_vireForm];
    
    _txtFrom.inputView =  _datePickerFrom;
    _txtTo.inputView = _datePickerTo;
    
    self.branchesArray = [[MLKnpnl sharedData] kpknls];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if (self.selectedBranches) {
        NSMutableArray *kotaNames = [NSMutableArray array];
        for (NSNumber *num in self.selectedBranches) {
            MKnpnl *ct = [[MLKnpnl sharedData] kpknlWithID:num.integerValue];
            [kotaNames addObject:ct.name];
        }
        _txtkpknl.text = [kotaNames componentsJoinedByString:@", "];
    }
    
    if(self.defFromDate) {
        _datePickerFrom.date = self.defFromDate;
        [self dateChanged:_datePickerFrom];
    }
    
    if(self.defToDate) {
        _datePickerTo.date = self.defToDate;
        [self dateChanged:_datePickerTo];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setupUI:(UIView *)view {
    for(UITextField *txt in view.subviews){
        if([txt isKindOfClass:[UITextField class]]){
            //            if(txt.tag > 0){
            //                UIPickerView *picker = [[[UIPickerView alloc] init] autorelease];
            //                picker.backgroundColor = [UIColor lightGrayColor];
            //                picker.delegate = self;
            //                picker.dataSource = self;
            //                picker.showsSelectionIndicator = YES;
            //                picker.tag = txt.tag;
            //                txt.inputView = picker;
            //            }
            
            txt.layer.masksToBounds = YES;
            txt.layer.cornerRadius = 3.;
            txt.layer.borderColor = [UIColor grayColor].CGColor;
            txt.layer.borderWidth = 1.;
            
            UIView *v = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 8., 10)] autorelease];
            v.backgroundColor = [UIColor clearColor];
            txt.leftView = v;
            txt.leftViewMode = UITextFieldViewModeAlways;
        }//else if([txt isKindOfClass:[UIButton class]]){
        //            UIButton *btn = (UIButton *)txt;
        //            btn.layer.masksToBounds = YES;
        //            btn.layer.cornerRadius = 3.;
        //            [btn setBackgroundImage:[Tools solidImageForColor:btn.backgroundColor withSize:btn.frame.size] forState:UIControlStateNormal];
        //        }
    }
}

- (IBAction)dateChanged:(UIDatePicker *)sender{
    NSDateFormatter *format = [[[NSDateFormatter alloc] init] autorelease];
    [format setDateFormat:@"dd MMMM yyyy"];
    
    if (sender.tag == 0) {
        _txtFrom.text = [format stringFromDate:sender.date];
        _datePickerTo.minimumDate = _datePickerFrom.date;
    } else {
        _txtTo.text = [format stringFromDate:sender.date];
    }
}

-(IBAction)showCombo:(UIButton *)sender{
    [self hideKeyboard:nil];
    
    ComboTableViewController *combo = [[ComboTableViewController alloc] initWithNibName:@"ComboTableViewController" bundle:nil];
    combo.multipleSelect = YES;
    combo.delegate = self;
    combo.data = self.branchesArray;
    combo.selectedRows = self.selectedBranches;
    
    [self.navigationController pushViewController:combo animated:YES];
    [combo release];
}

- (IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)reset:(id)sender {
    _txtFrom.text = @"";
    _txtTo.text = @"";
    _txtkpknl.text = @"";
}

- (IBAction)search:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (self.delegate) {
        NSDate *from = (_txtFrom.text.length > 0) ? _datePickerFrom.date : nil;
        NSDate *to = (_txtTo.text.length > 0) ? _datePickerTo.date : nil;
        NSArray *kpknls = (_txtkpknl.text.length > 0) ? self.selectedBranches : nil;
        [self.delegate didSetStartDate:from endDate:to kpknls:kpknls];
    }
}


#pragma mark - delegate combo
- (void)combo:(ComboTableViewController *)controller didSelectItemAtIndexes:(NSMutableArray *)indexes {
    NSMutableArray *kotaNames = [NSMutableArray array];
    self.selectedBranches = [NSMutableArray array];
    for (NSNumber *num in indexes) {
        MKnpnl *ct = self.branchesArray[num.integerValue];
        [kotaNames addObject:ct.name];
        [self.selectedBranches addObject:ct.identifier];
    }
    
    _txtkpknl.text = [kotaNames componentsJoinedByString:@", "];
}


#pragma mark - Observer keyboard
-(void)keyboardDidHide:(NSNotification *)sender{
    _constScrollBottom.constant = 0;
}

-(void)keyboardDidShow:(NSNotification *)sender{
    CGRect keyboardRect = [[sender.userInfo valueForKeyPath:@"UIKeyboardBoundsUserInfoKey"] CGRectValue];
    
    _constScrollBottom.constant = keyboardRect.size.height - 42;
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [_defFromDate release];
    [_defToDate release];
    [_branchesArray release];
    [_vireForm release];
    [_txtFrom release];
    [_txtTo release];
    [_txtkpknl release];
    [_datePickerFrom release];
    [_datePickerTo release];
    [_constScrollBottom release];
    [super dealloc];
}
@end
