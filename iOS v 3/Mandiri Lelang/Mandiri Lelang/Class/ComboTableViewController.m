//
//  ComboTableViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 9/14/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "ComboTableViewController.h"

@interface ComboTableViewController ()<UITableViewDataSource, UITableViewDelegate>{
    IBOutlet UITableView *table;
}

@property (retain, nonatomic) IBOutlet UIButton *btnSelesai;

-(IBAction)back:(id)sender;

@end

@implementation ComboTableViewController

@synthesize delegate, selectedRow, data, tag;

- (void)dealloc
{
    self.data = nil;
    [_btnSelesai release];
    [_selectedRows release];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"Terpilih %zd", self.selectedRows.count);
    [super viewDidLoad];
    
    if (!self.multipleSelect) {
        _btnSelesai.hidden = YES;
    }
    
    if (_selectedRows == nil) {
        self.selectedRows = [NSMutableArray array];
    } else {
        NSMutableArray *newSelected = [NSMutableArray array];
        
        for (int i=0; i<data.count; i++){
            for (int j=0; j<_selectedRows.count; j++) {
                id one = [data[i] valueForKey:@"identifier"];
                NSInteger selected = [_selectedRows[j] integerValue];
                
                if ([one integerValue] == selected){
//                    [_selectedRows replaceObjectAtIndex:j withObject:@(i + (_withSemua ? 1 : 0))];
                    [newSelected addObject:@(i + (_withSemua ? 1 : 0))];
                }
            }
        }
        self.selectedRows = newSelected;
        
        if (_withSemua && self.selectedRows.count == self.data.count) {
            [self.selectedRows addObject:@0];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions
-(void)back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)selesai:(id)sender {
    if(self.delegate){
        if (_withSemua) {
            NSMutableArray *temp = [NSMutableArray array];
            
            for (NSNumber *num in self.selectedRows) {
                if ([num integerValue] > 0) {
                    [temp addObject:@(num.integerValue-1)];
                }
            }
            
            self.selectedRows = temp;
        }
        [self.delegate combo:self didSelectItemAtIndexes:self.selectedRows];
    }
    [self performSelector:@selector(back:) withObject:nil afterDelay:0.25];
}

#pragma mark - delegate dan data source table
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.data.count + (_withSemua ? 1 : 0);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"combo"];
    
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"combo"] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        cell.textLabel.font = [UIFont systemFontOfSize:14.];
    }
    
    if (_withSemua) {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Semua";
        } else {
            NSDictionary *dict = [self.data objectAtIndex:indexPath.row - 1];
            cell.textLabel.text = [dict valueForKey:@"name"];
        }
    } else {
        NSDictionary *dict = [self.data objectAtIndex:indexPath.row];
        cell.textLabel.text = [dict valueForKey:@"name"];
    }
    
    if (self.multipleSelect) {
        BOOL terpilih = NO;
        
        for (NSNumber *num in self.selectedRows) {
            if (num.integerValue == indexPath.row) {
                terpilih = YES;
            }
        }
        
        cell.accessoryType = terpilih ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    } else {
        cell.accessoryType = indexPath.row == self.selectedRow ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.multipleSelect) {
        if (_withSemua) {
            if (indexPath.row == 0) {
                if ([self.selectedRows count] >= self.data.count+1) {
                    [self.selectedRows removeAllObjects];
                } else {
                    [self.selectedRows removeAllObjects];
                
                    for (int i=0; i<= self.data.count; i++){
                        [self.selectedRows addObject:@(i)];
                    }
                }
            } else {
                NSInteger indexAda = -1;
                
                for (NSNumber *num in self.selectedRows) {
                    if (num.integerValue == indexPath.row) {
                        indexAda = [self.selectedRows indexOfObject:num];
                        break;
                    }
                }
                
                if (indexAda > -1) {
                    [self.selectedRows removeObjectAtIndex:indexAda];
                } else {
                    [self.selectedRows addObject:@(indexPath.row)];
                }
            }
            
            [tableView reloadData];
        } else {
            NSInteger indexAda = -1;
            
            for (NSNumber *num in self.selectedRows) {
                if (num.integerValue == indexPath.row) {
                    indexAda = [self.selectedRows indexOfObject:num];
                    break;
                }
            }
            
            if (indexAda > -1) {
                [self.selectedRows removeObjectAtIndex:indexAda];
            } else {
                [self.selectedRows addObject:@(indexPath.row)];
            }
            
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    } else {
        if(self.delegate){
            [self.delegate combo:self didSelectItemAtIndex:indexPath.row];
        }
        
        NSInteger oldSelectedRow = self.selectedRow;
        self.selectedRow = indexPath.row;
        if(self.selectedRow != oldSelectedRow){
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, [NSIndexPath indexPathForRow:oldSelectedRow inSection:0], nil] withRowAnimation:UITableViewRowAnimationNone];
        }
        [self performSelector:@selector(back:) withObject:nil afterDelay:0.25];
    }
}

@end
