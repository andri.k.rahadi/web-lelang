//
//  FilterViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/17/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "FilterViewController.h"
#import "Tools.h"
#import "ComboTableViewController.h"
#import "MLDatabase.h"
#import "SearchResultViewController.h"

@interface FilterViewController ()<UIPickerViewDataSource, UIPickerViewDelegate, ComboTableViewControllerDelegate>{
    IBOutlet UIScrollView *scroll;
    IBOutlet UIView *formView;
    IBOutlet UIButton *hotPriceButton;
    IBOutlet UIDatePicker *datePicker;
    
    IBOutlet UIButton *urutButton;
    IBOutlet UIButton *propinsiButton;
    IBOutlet UIButton *cityButton;
    IBOutlet UITextField *urut;
    IBOutlet UITextField *kategori;
    IBOutlet UITextField *propinsi;
    IBOutlet UITextField *kota;
    IBOutlet UITextField *alamat;
    IBOutlet UITextField *hargamin;
    IBOutlet UITextField *hargamax;
    IBOutlet UITextField *lain;
    IBOutlet UITextField *tanggal;
    IBOutlet UITextField *keyword;
    IBOutlet UIButton *btnSukarela;
    IBOutlet UIButton *btnLelang;
    
    BOOL appearFromCombo;
}

@property (nonatomic, retain) NSArray *sortByArray;
@property (nonatomic, retain) NSArray *kategoriArray;
@property (nonatomic, retain) NSArray *propinsiArray;
@property (nonatomic, retain) NSArray *kotaArray;
@property (nonatomic, retain) NSArray *hargaMinarray;
@property (nonatomic, retain) NSArray *hargaMaxArray;
@property (nonatomic, retain) NSArray *lainLainArray;

-(IBAction)toggleMe:(id)sender;
-(IBAction)hideKeyboard:(id)sender;
-(IBAction)back:(id)sender;
-(IBAction)reset:(id)sender;
-(IBAction)apply:(id)sender;
-(IBAction)dateChanged:(id)sender;
-(IBAction)showCombo:(id)sender;

@end

@implementation FilterViewController

@synthesize sortByArray, kategoriArray, propinsiArray, kotaArray, hargaMaxArray, hargaMinarray, lainLainArray;
@synthesize filterType;
@synthesize initParams;

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    scroll.delegate = nil;
    self.sortByArray = nil;
    self.kategoriArray = nil;
    self.propinsiArray = nil;
    self.kotaArray = nil;
    self.hargaMinarray = nil;
    self.hargaMaxArray = nil;
    self.lainLainArray = nil;
    [keyword release];
    [btnSukarela release];
    [btnLelang release];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    
    
    [scroll addSubview:formView];
    scroll.contentSize = formView.frame.size;
    
    for(UITextField *txt in formView.subviews){
        if([txt isKindOfClass:[UITextField class]]){
            if(txt == tanggal){
                txt.inputView = datePicker;
            }
            
            txt.layer.masksToBounds = YES;
            txt.layer.cornerRadius = 3.;
            txt.layer.borderColor = [UIColor colorWithWhite:0.7 alpha:0.7].CGColor;
            txt.layer.borderWidth = 1.;
            txt.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.2];
            
            UIView *v = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 8., 10)] autorelease];
            v.backgroundColor = [UIColor clearColor];
            txt.leftView = v;
            txt.leftViewMode = UITextFieldViewModeAlways;
        }else if([txt isKindOfClass:[UIButton class]]){
            UIButton *btn = (UIButton *)txt;
            if(btn != hotPriceButton){
                btn.layer.masksToBounds = YES;
                //                btn.layer.cornerRadius = 3.;
                [btn setBackgroundImage:[Tools solidImageForColor:[UIColor colorWithWhite:0.5 alpha:0.3] withSize:btn.frame.size] forState:UIControlStateDisabled];
            }
        }
    }
    
    for(UITextField *txt in self.view.subviews){
        if([txt isKindOfClass:[UIButton class]]){
            UIButton *btn = (UIButton *)txt;
            if(btn != hotPriceButton){
                btn.layer.masksToBounds = YES;
                //                btn.layer.cornerRadius = 3.;
                [btn setBackgroundImage:[Tools solidImageForColor:btn.backgroundColor withSize:btn.frame.size] forState:UIControlStateNormal];
            }
        }
    }
    
//    [urutButton setBackgroundImage:[Tools solidImageForColor:[UIColor colorWithWhite:0.5 alpha:0.3] withSize:urutButton.frame.size] forState:UIControlStateDisabled];
    
    self.sortByArray = [NSArray arrayWithObjects:@{@"name":@"Terbaru", @"identifier": @1}, @{@"name":@"Harga Tertinggi", @"identifier": @2}, @{@"name":@"Harga Terendah", @"identifier": @3}, @{@"name":@"Segera Dilelang", @"identifier": @4}, nil];
    self.kategoriArray = [MLCategory sharedData].categories;
    self.propinsiArray = [MLProvince sharedData].provinces;
    self.kotaArray = [[MLCity sharedData] citiesForProvince:@0];
    self.hargaMinarray = [MLPrice sharedData].prices;
    self.hargaMaxArray = [MLPrice sharedData].prices;
    self.lainLainArray = [MLDocumentType sharedData].documentTypes;
    
    [self reset:nil];
}

-(NSString *)nameFromArray:(NSArray *)arr withID:(int)_id{
    for(NSDictionary *dict in arr){
        NSNumber *ide = [dict valueForKey:@"identifier"];
        if(ide.intValue == _id){
            return [dict valueForKey:@"name"];
        }
    }
    
    return @"";
}

-(NSInteger)indexFromArray:(NSArray *)arr withID:(int)_id{
    int i = 0;
    for(NSDictionary *dict in arr){
        NSNumber *ide = [dict valueForKey:@"identifier"];
        if(ide.intValue == _id){
            return i;
        }
        
        i++;
    }
    
    return i;
}

-(void)checkInitialParams{
    if(self.initParams){
        NSString *val = [self.initParams valueForKey:@"orderBy"];
        if(val){
            urut.tag = [self indexFromArray:self.sortByArray withID:val.intValue];
            urut.text = [self nameFromArray:self.sortByArray withID:val.intValue];
        }
        
        val = [self.initParams valueForKey:@"categoryID"];
        if(val){
            kategori.tag = [self indexFromArray:self.kategoriArray withID:val.intValue];
            kategori.text = [self nameFromArray:self.kategoriArray withID:val.intValue];
        }
        
        val = [self.initParams valueForKey:@"provinceID"];
        if(val){
            propinsi.tag = [self indexFromArray:self.propinsiArray withID:val.intValue];
            propinsi.text = [self nameFromArray:self.propinsiArray withID:val.intValue];
        }
        
        val = [self.initParams valueForKey:@"cityID"];
        if(val){
            kota.tag = [self indexFromArray:self.kotaArray withID:val.intValue];
            kota.text = [self nameFromArray:self.kotaArray withID:val.intValue];
        }
        
        val = [self.initParams valueForKey:@"minPriceID"];
        if(val){
            hargamin.tag = [self indexFromArray:self.hargaMinarray withID:val.intValue];
            hargamin.text = [self nameFromArray:self.hargaMinarray withID:val.intValue];
        }
        
        val = [self.initParams valueForKey:@"maxPriceID"];
        if(val){
            hargamax.tag = [self indexFromArray:self.hargaMaxArray withID:val.intValue];
            hargamax.text = [self nameFromArray:self.hargaMaxArray withID:val.intValue];
        }
        
        val = [self.initParams valueForKey:@"documentTypeID"];
        if(val){
            lain.tag = [self indexFromArray:self.lainLainArray withID:val.intValue];
            lain.text = [self nameFromArray:self.lainLainArray withID:val.intValue];
        }
        
        val = [self.initParams valueForKey:@"hotPrice"];
        if(val){
            hotPriceButton.selected = val.intValue > 0;
        }
        
        val = [self.initParams valueForKey:@"address"];
        if(val.length > 0){
            alamat.text = val;
        }
        
        val = [self.initParams valueForKey:@"keyword"];
        if(val.length > 0){
            keyword.text = val;
        }
        
        val = [self.initParams valueForKey:@"auctionDate"];
        if(val.length > 0){
            NSDateFormatter *f = [[[NSDateFormatter alloc] init] autorelease];
            [f setDateFormat:@"yyyyMMdd"];
            datePicker.date = [f dateFromString:val];
            [self dateChanged:datePicker];
        }
    }
}

-(void)viewWillAppear:(BOOL)animated{
    if(!appearFromCombo)
        [self checkInitialParams];
    
    appearFromCombo = NO;
    
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - picker view
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    switch (pickerView.tag) {
        case 1:
            return self.sortByArray.count;
            break;
        case 2:
            return self.kategoriArray.count;
            break;
        case 3:
            return self.propinsiArray.count;
            break;
        case 4:
            return self.kotaArray.count;
            break;
        case 5:
            return self.hargaMinarray.count;
            break;
        case 6:
            return self.hargaMaxArray.count;
            break;
        case 7:
            return self.lainLainArray.count;
            break;
            
        default:
            break;
    }
    
    return 0;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    switch (pickerView.tag) {
        case 1:
            return [[self.sortByArray objectAtIndex:row] valueForKey:@"name"];
            break;
        case 2:
            return [[self.kategoriArray objectAtIndex:row] valueForKey:@"name"];
            break;
        case 3:
            return [[self.propinsiArray objectAtIndex:row] valueForKey:@"name"];
            break;
        case 4:
            return [[self.kotaArray objectAtIndex:row] valueForKey:@"name"];
            break;
        case 5:
            return [[self.hargaMinarray objectAtIndex:row] valueForKey:@"name"];
            break;
        case 6:
            return [[self.hargaMaxArray objectAtIndex:row] valueForKey:@"name"];
            break;
        case 7:
            return [[self.lainLainArray objectAtIndex:row] valueForKey:@"name"];
            break;
            
        default:
            break;
    }
    
    return 0;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    UITextField *txt = (UITextField *)[formView viewWithTag:pickerView.tag];
    txt.text = [self pickerView:pickerView titleForRow:row forComponent:component];
}

#pragma mark - Actions
-(IBAction)toggleMe:(UIButton *)sender{
    sender.selected = !sender.selected;
}

-(void)hideKeyboard:(id)sender{
    for(UITextField *txt in formView.subviews){
        if([txt isKindOfClass:[UITextField class]]){
            [txt resignFirstResponder];
        }
    }
}

-(void)reset:(id)sender{
    [self hideKeyboard:nil];
    for(UITextField *txt in formView.subviews){
        if([txt isKindOfClass:[UITextField class]]){
            txt.text = @"";
            txt.enabled = YES;
            txt.tag = 0;
        }
    }
    hotPriceButton.selected = NO;
    hotPriceButton.userInteractionEnabled = YES;
    keyword.text = @"";
    
    if (self.filterType == FilterTypeSegera){
        urut.text = @"Segera Dilelang";
        urut.tag = 3;
        urutButton.enabled = NO;
        urut.enabled = NO;
        
        btnSukarela.selected = NO;
        btnSukarela.userInteractionEnabled = NO;
        btnSukarela.alpha = 0.7;
        btnLelang.userInteractionEnabled = NO;
        btnLelang.selected = YES;
    } else if(self.filterType == FilterTypeHotPrice){
        hotPriceButton.selected = YES;
        hotPriceButton.userInteractionEnabled = NO;
        
        btnSukarela.selected = NO;
        btnSukarela.userInteractionEnabled = NO;
        btnSukarela.alpha = 0.7;
        btnLelang.userInteractionEnabled = NO;
        btnLelang.selected = YES;
    } else if(self.filterType == FilterTypeTerbaru){
        urut.text = @"Terbaru";
        urut.tag = 0;
        urutButton.enabled = NO;
        urut.enabled = NO;
    } else if(self.filterType == FilterTypeTerdekat){
        propinsiButton.enabled = NO;
        propinsi.enabled = NO;
        cityButton.enabled = NO;
        kota.enabled = NO;
        alamat.enabled = NO;
        alamat.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.4];
    } else if(self.filterType == FilterTypeSukarela){
        hotPriceButton.selected = YES;
        hotPriceButton.userInteractionEnabled = NO;
        
        btnSukarela.selected = YES;
        btnSukarela.userInteractionEnabled = NO;
        btnLelang.alpha = 0.7;
        btnLelang.userInteractionEnabled = NO;
        btnLelang.selected = NO;
        
        urut.text = @"Terbaru";
        urut.tag = 0;
        urutButton.enabled = NO;
        urut.enabled = NO;
    }
}

-(void)back:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)apply:(id)sender{
    if(self.delegate){
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:[[self.sortByArray objectAtIndex:urut.tag] valueForKey:@"identifier"] forKey:@"orderBy"];
        [dict setValue:[[self.kategoriArray objectAtIndex:kategori.tag] valueForKey:@"identifier"] forKey:@"categoryID"];
        [dict setValue:[[self.propinsiArray objectAtIndex:propinsi.tag] valueForKey:@"identifier"] forKey:@"provinceID"];
        [dict setValue:[[self.kotaArray objectAtIndex:kota.tag] valueForKey:@"identifier"] forKey:@"cityID"];
        [dict setValue:[[self.hargaMinarray objectAtIndex:hargamin.tag] valueForKey:@"identifier"] forKey:@"minPriceID"];
        [dict setValue:[[self.hargaMaxArray objectAtIndex:hargamax.tag] valueForKey:@"identifier"] forKey:@"maxPriceID"];
        [dict setValue:[[self.lainLainArray objectAtIndex:lain.tag] valueForKey:@"identifier"] forKey:@"documentTypeID"];
        [dict setValue:alamat.text forKey:@"address"];
        [dict setValue:keyword.text forKey:@"keyword"];
        [dict setValue:hotPriceButton.selected?@1:@0 forKey:@"hotPrice"];
        
        if(tanggal.text.length > 0){
            NSDateFormatter *f = [[[NSDateFormatter alloc] init] autorelease];
            [f setDateFormat:@"yyyyMMdd"];
            [dict setValue:[f stringFromDate:datePicker.date] forKey:@"auctionDate"];
        }else{
            [dict setValue:@"" forKey:@"auctionDate"];
        }
        
//        [self.delegate filterViewController:self didApplyFilter:dict];
//        [self back:nil];
        
        SearchResultViewController *search = [[[SearchResultViewController alloc] initWithNibName:@"SearchResultViewController" bundle:nil] autorelease];
        search.params = dict;
        search.enableFilter = NO;
        search.type = self.filterType;
        [self.navigationController pushViewController:search animated:YES];
    }
}

-(void)dateChanged:(UIDatePicker *)sender{
    NSDateFormatter *format = [[[NSDateFormatter alloc] init] autorelease];
    [format setDateFormat:@"dd MMMM yyyy"];
    tanggal.text = [format stringFromDate:sender.date];
}

-(void)showCombo:(UIButton *)sender{
    appearFromCombo = YES;
    
    ComboTableViewController *combo = [[ComboTableViewController alloc] initWithNibName:@"ComboTableViewController" bundle:nil];
    combo.tag = sender.tag;
    combo.delegate = self;
    
    if(sender.tag == 0){
        combo.data = self.sortByArray;
        combo.selectedRow = urut.tag;
    }else if(sender.tag == 1){
        combo.data = self.kategoriArray;
        combo.selectedRow = kategori.tag;
    }else if(sender.tag == 2){
        combo.data = self.propinsiArray;
        combo.selectedRow = propinsi.tag;
    }else if(sender.tag == 3){
        combo.data = self.kotaArray;
        combo.selectedRow = kota.tag;
    }else if(sender.tag == 4){
        combo.data = self.hargaMinarray;
        combo.selectedRow = hargamin.tag;
    }else if(sender.tag == 5){
        combo.data = self.hargaMaxArray;
        combo.selectedRow = hargamax.tag;
    }else if(sender.tag == 6){
        combo.data = self.lainLainArray;
        combo.selectedRow = lain.tag;
    }
    
    [self.navigationController pushViewController:combo animated:YES];
    [combo release];
}

#pragma mark - Observer keyboard
-(void)keyboardDidHide:(NSNotification *)sender{
    CGRect frame = scroll.frame;
    frame.size.height = self.view.frame.size.height - scroll.frame.origin.y - 43;
    scroll.frame = frame;
}

-(void)keyboardDidShow:(NSNotification *)sender{
    CGRect keyboardRect = [[sender.userInfo valueForKeyPath:@"UIKeyboardBoundsUserInfoKey"] CGRectValue];
    
    CGRect frame = scroll.frame;
    frame.size.height = self.view.frame.size.height - scroll.frame.origin.y - keyboardRect.size.height;
    scroll.frame = frame;
}

#pragma mark - delegate combo
-(void)combo:(ComboTableViewController *)controller didSelectItemAtIndex:(NSInteger)index{
    if(controller.tag == 0){
        urut.text = [[self.sortByArray objectAtIndex:index] valueForKey:@"name"];
        urut.tag = index;
    }else if(controller.tag == 1){
        kategori.text = [[self.kategoriArray objectAtIndex:index] valueForKey:@"name"];
        kategori.tag = index;
    }else if(controller.tag == 2){
        MProvince *prov = [self.propinsiArray objectAtIndex:index];
        propinsi.text = prov.name;
        propinsi.tag = index;
        self.kotaArray = [[MLCity sharedData] citiesForProvince:prov.identifier];
        kota.tag = 0;
        kota.text = [[self.kotaArray objectAtIndex:0] valueForKey:@"name"];
    }else if(controller.tag == 3){
        kota.text = [[self.kotaArray objectAtIndex:index] valueForKey:@"name"];
        kota.tag = index;
    }else if(controller.tag == 4){
        MPrice *price = [self.hargaMinarray objectAtIndex:index];
        hargamin.text = [price valueForKey:@"name"];
        hargamin.tag = index;
        self.hargaMaxArray = [[MLPrice sharedData] pricesBeginFromID:price.identifier];
        hargamax.text = @"";
        hargamax.tag = 0;
    }else if(controller.tag == 5){
        hargamax.text = [[self.hargaMaxArray objectAtIndex:index] valueForKey:@"name"];
        hargamax.tag = index;
    }else if(controller.tag == 6){
        lain.text = [[self.lainLainArray objectAtIndex:index] valueForKey:@"name"];
        lain.tag = index;
    }
}

@end
