//
//  AboutViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/9/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "AboutViewController.h"
#import "IIViewDeckController.h"
#import "AppDelegate.h"

@interface AboutViewController ()<UIWebViewDelegate>{
    IBOutlet UILabel *titleLabel;
    IBOutlet UIWebView *webView;
    IBOutlet UIActivityIndicatorView *loading;
    IBOutlet UIButton *btnBack;
    IBOutlet UIButton *btnMenu;
}

-(IBAction)openMenu:(id)sender;

@end

@implementation AboutViewController

@synthesize url, isNavMode;

- (void)dealloc
{
    self.url = nil;
    webView.delegate = nil;
    [webView stopLoading];
    [btnBack release];
    [btnMenu release];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    titleLabel.text = self.title;
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
    [webView loadRequest:req];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if(!isNavMode) {
        btnBack.hidden = YES;
        btnMenu.hidden = NO;
    } else {
        btnBack.hidden = NO;
        btnMenu.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
-(void)openMenu:(id)sender{
    if([AppDelegate currentDelegate].mainViewController.leftControllerIsClosed)
        [[AppDelegate currentDelegate] openMenu];
    else
        [[AppDelegate currentDelegate] closeMenu];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - delegate webView
-(void)webViewDidStartLoad:(UIWebView *)webView{
    [loading startAnimating];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [loading stopAnimating];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [loading stopAnimating];
}

@end
