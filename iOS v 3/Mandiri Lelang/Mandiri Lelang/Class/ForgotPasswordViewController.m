//
//  ForgotPasswordViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 10/29/16.
//  Copyright © 2016 Asep Mulyana. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "MBProgressHUD.h"
#import "ASIHTTPRequest+Mandiri.h"
#import "AppConfig.h"
#import "JSON.h"

@interface ForgotPasswordViewController () <MBProgressHUDDelegate> {
    NSInteger requestCount;
}

@property (retain, nonatomic) IBOutlet UITextField *txtEmail;

@property (nonatomic, retain) MBProgressHUD *loading;

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Actions

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)isFormValid {
    if (_txtEmail.text.length < 1){
        [self alert:@"Mohon isi Email"];
        return NO;
    }
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    if(![predicate evaluateWithObject:_txtEmail.text]) {
        [self alert:@"Mohon isi email yang valid"];
        return NO;
    }
    
    return YES;
}

-(void)alert:(NSString *)msg{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [alert release];
}

- (IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)kirim:(id)sender {
    if (![self isFormValid]) {
        return;
    }
    
    [self hideKeyboard:nil];
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:kURLForgot]];
    [request addPostValue:_txtEmail.text forKey:@"email"];
    [request encryptParams];
    request.delegate = self;
    request.tag = 1;
    [request startAsynchronous];
}

- (void)setupUI:(UIView *)view {
    for(UITextField *txt in view.subviews){
        if([txt isKindOfClass:[UITextField class]]){
            //            if(txt.tag > 0){
            //                UIPickerView *picker = [[[UIPickerView alloc] init] autorelease];
            //                picker.backgroundColor = [UIColor lightGrayColor];
            //                picker.delegate = self;
            //                picker.dataSource = self;
            //                picker.showsSelectionIndicator = YES;
            //                picker.tag = txt.tag;
            //                txt.inputView = picker;
            //            }
            
            txt.layer.masksToBounds = YES;
            txt.layer.cornerRadius = 3.;
            txt.layer.borderColor = [UIColor grayColor].CGColor;
            txt.layer.borderWidth = 1.;
            
            UIView *v = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 8., 10)] autorelease];
            v.backgroundColor = [UIColor clearColor];
            txt.leftView = v;
            txt.leftViewMode = UITextFieldViewModeAlways;
        }//else if([txt isKindOfClass:[UIButton class]]){
        //            UIButton *btn = (UIButton *)txt;
        //            btn.layer.masksToBounds = YES;
        //            btn.layer.cornerRadius = 3.;
        //            [btn setBackgroundImage:[Tools solidImageForColor:btn.backgroundColor withSize:btn.frame.size] forState:UIControlStateNormal];
        //        }
    }
}


#pragma mark - delegate ASIHTTP
-(void)requestStarted:(ASIHTTPRequest *)request{
    [self showLoading];
}

- (void)showLoading{
    requestCount++;
    if(!self.loading){
        self.loading = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.loading.labelText = @"Loading..";
        self.loading.delegate = self;
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request{
    [self countDownRequest];
    
    [self alert:@"Tidak terdapat koneksi internet."];
}

-(void)requestFinished:(ASIHTTPRequest *)request{
    [self countDownRequest];
    NSLog(@"\n==========\n forgot %@ : %@\n\n", request.url.absoluteString, request.responseString);
    
    if (request.tag == 1) {
        NSDictionary *root = [request.responseString JSONValue];
        NSDictionary *result = [root valueForKey:@"resultCode"];
        
        [self alert:result[@"message"]];
        
        if ([[result valueForKey:@"code"] integerValue] == 0){
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

-(void)countDownRequest{
    requestCount--;
    
    if(requestCount < 1){
        [self.loading hide:NO];
    }
}

#pragma mark - delegate loading
-(void)hudWasHidden:(MBProgressHUD *)hud{
    self.loading = nil;
}

- (void)dealloc {
    [_txtEmail release];
    [super dealloc];
}
@end
