//
//  KuesionerViewController.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 10/29/16.
//  Copyright © 2016 Asep Mulyana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KuesionerViewController : UIViewController

@property (nonatomic, retain) NSMutableDictionary *data;
@property (nonatomic) BOOL fromRegister;
@property (nonatomic) BOOL fromMenu;
@property (nonatomic, retain) NSDictionary *defaultPreference;

@end
