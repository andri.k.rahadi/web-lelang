//
//  JadwalViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/17/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "JadwalViewController.h"
#import "AppDelegate.h"
#import "IIViewDeckController.h"
#import "JadwalTableViewCell.h"
#import "DetailJadwalViewController.h"
#import "UICellRefreshLoading.h"
#import "ASIHTTPRequest+Mandiri.h"
#import "MBProgressHUD.h"
#import "JSON.h"
#import "AppConfig.h"
#import "TTTAttributedLabel.h"
#import "SearchJadwalViewController.h"

@interface JadwalViewController ()<ASIHTTPRequestDelegate, MBProgressHUDDelegate, UICellRefreshLoadingDelegate, SearchJadwalViewControllerDelegate ,UIDocumentInteractionControllerDelegate>{
    IBOutlet UITableView *table;
    IBOutlet UITableViewCell *blankCell;
    IBOutlet UIButton *btnDownload;
    
    NSOperationQueue *queue;
    NSInteger requestCount;
    UICellRefreshLoading *refreshViewHeader;
    BOOL isrefresh;
    int currentPage;
    BOOL needLoadMore;
}

-(IBAction)openMenu:(id)sender;

@property (nonatomic, retain) NSMutableArray *data;
@property (nonatomic, retain) MBProgressHUD *loading;
@property (nonatomic, retain) NSMutableDictionary *postParams;
@property (nonatomic, retain) UIDocumentInteractionController *documentInteractionController;

@end

@implementation JadwalViewController

@synthesize data, loading;

- (void)dealloc
{
    table.delegate = nil;
    self.data = nil;
    self.loading = nil;
    self.postParams = nil;
    [_documentInteractionController release];
    
    for(ASIHTTPRequest *req in queue.operations){
        req.delegate = nil;
    }
    [queue cancelAllOperations];
    [queue release];
    
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    queue = [[NSOperationQueue alloc] init];
    
    refreshViewHeader = [UICellRefreshLoading createNewAsHeader:YES forView:table];
    refreshViewHeader.delegate = self;
    
    [self makeRequest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
-(void)openMenu:(id)sender{
    if([AppDelegate currentDelegate].mainViewController.leftControllerIsClosed)
        [[AppDelegate currentDelegate] openMenu];
    else
        [[AppDelegate currentDelegate] closeMenu];
}

-(void)makeRequest{
    NSString *url = [kURLSchedule stringByAppendingFormat:@"?pageIndex=%d", currentPage];
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestAuthWithURL:[NSURL URLWithString:url]];
    [request encryptParams];
    request.tag = currentPage;
    request.delegate = self;
    requestCount++;
    
    if(self.postParams) {
        NSString *params = [self.postParams JSONRepresentation];
//        request.allowCompressedResponse = NO;
//        request.useCookiePersistence = NO;
//        request.shouldCompressRequestBody = NO;
        [request setPostBody:[NSMutableData dataWithData:[params  dataUsingEncoding:NSUTF8StringEncoding]]];
        NSLog(@"%@", params);
    }
    
    
    [queue addOperation:request];
}

- (IBAction)download:(id)sender {
    ASIFormDataRequest *request = [ASIFormDataRequest requestAuthWithURL:[NSURL URLWithString:kURLDownloadSchedule]];
    [request encryptParams];
    request.tag = -1;
    request.delegate = self;
    requestCount++;
    
    if(self.postParams) {
        NSString *params = [self.postParams JSONRepresentation];
        request.allowCompressedResponse = NO;
        request.useCookiePersistence = NO;
        request.shouldCompressRequestBody = NO;
        [request setPostBody:[NSMutableData dataWithData:[params  dataUsingEncoding:NSUTF8StringEncoding]]];
    }
    
    [queue addOperation:request];
}

-(void)alert:(NSString *)msg{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [alert release];
}

- (IBAction)search:(id)sender {
    SearchJadwalViewController *cari = [[[SearchJadwalViewController alloc] initWithNibName:@"SearchJadwalViewController" bundle:nil] autorelease];
    UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:cari] autorelease];
    nav.navigationBarHidden = YES;
    cari.delegate = self;
    
    NSArray *arr = self.postParams[@"kpknlIDs"];
    cari.selectedBranches = [NSMutableArray arrayWithArray:arr];
    
    NSDateFormatter *f = [[[NSDateFormatter alloc] init] autorelease];
    f.dateFormat = @"yyyy-MM-dd";
    
    NSDate *from = [f dateFromString:[self.postParams valueForKey:@"fromDate"]];
    cari.defFromDate = from;
    
    NSDate *to = [f dateFromString:[self.postParams valueForKey:@"toDate"]];
    cari.defToDate = to;
    
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark - tableView delegate dan datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.data && self.data.count < 1)
        return 1;
    
    return self.data.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.data && self.data.count < 1)
        return blankCell.frame.size.height;
    
    JadwalTableViewCell *cell = (JadwalTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.height;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.data && self.data.count < 1)
        return blankCell;
    
    JadwalTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JadwalTableViewCell"];
    
    if(cell == nil){
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"JadwalTableViewCell" owner:self options:nil];
        
        for(id one in arr){
            if([one isKindOfClass:[JadwalTableViewCell class]]){
                cell = (JadwalTableViewCell *)one;
            }
        }
    }
    
    NSDictionary *dict = [self.data objectAtIndex:indexPath.row];
    cell.tanggal.text = [dict valueForKey:@"auctionDateFormattedDateTime"];
    cell.alamat.text = [dict valueForKey:@"kpknlAddress"];
    cell.penyelenggara.text = [dict valueForKey:@"kpknlName"];
    cell.separator.hidden = NO;
    
    NSString *txt = [[NSString stringWithFormat:@"%@\n%@", [dict valueForKey:@"kpknlPhone1"], [dict valueForKey:@"kpknlPhone2"]] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    cell.telepon.text = txt;
    [cell updateLayout];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row >= self.data.count-1 && needLoadMore){
        isrefresh = YES;
        currentPage++;
        [self makeRequest];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dict = [self.data objectAtIndex:indexPath.row];
    DetailJadwalViewController *detail = [[DetailJadwalViewController alloc] initWithNibName:@"DetailJadwalViewController" bundle:nil];
    detail.jadwalID = [dict valueForKey:@"auctionScheduleID"];
    detail.tanggal = [dict valueForKey:@"auctionDateFormattedDateTime"];
    detail.kpknl= [dict valueForKey:@"kpknlName"];
    [self.navigationController pushViewController:detail animated:YES];
    [detail release];
}

#pragma mark - delegate ASIHTTP
-(void)requestStarted:(ASIHTTPRequest *)request{
    if(!self.loading && !isrefresh){
        self.loading = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.loading.labelText = @"Loading..";
        self.loading.delegate = self;
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request{
    [self countDownRequest];
    
    [self alert:@"Tidak terdapat koneksi internet."];
}

-(void)requestFinished:(ASIHTTPRequest *)request{
    [self countDownRequest];
    NSLog(@"\n==========\n jadwal %@ : %@\n\n", request.url.absoluteString, request.responseString);
    
    if (request.tag == -1){
        NSArray *pahts = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docDir = [pahts objectAtIndex:0];
        NSString *filePath = [docDir stringByAppendingPathComponent:@"Jadwal_Lelang.csv"];
        [request.responseData writeToFile:filePath atomically:YES];
        
        NSURL *url = [NSURL fileURLWithPath:filePath];
        self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        [self.documentInteractionController setDelegate:self];
        [self.documentInteractionController presentOptionsMenuFromRect:btnDownload.frame inView:self.view animated:YES];
    } else {
        NSDictionary *root = [request.responseString JSONValue];
        
        NSMutableArray *datas = [root valueForKey:@"data"];
        if(request.tag == 0){
            self.data = datas;
        }else{
            [self.data addObjectsFromArray:datas];
        }
    //    self.data = [NSArray array];
        
        needLoadMore = datas.count > 0;
        
        [table reloadData];
    }
}

-(void)countDownRequest{
    requestCount--;
    
    if(requestCount < 1){
        [self.loading hide:NO];
        
        if(isrefresh && refreshViewHeader.isLoading){
            [refreshViewHeader stopLoading];
        }
    }
}

#pragma mark - delegate loading
-(void)hudWasHidden:(MBProgressHUD *)hud{
    self.loading = nil;
}

#pragma mark - delegate scroll
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [refreshViewHeader scrollViewDidScroll:scrollView];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [refreshViewHeader scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}

#pragma mark - delegate refresh pull
-(void)UICellRefreshLoadingDidBeginLoading:(UICellRefreshLoading *)view{
    isrefresh = YES;
    table.contentSize = table.frame.size;
    currentPage = 0;
    [self makeRequest];
}

-(void)UICellRefreshLoadingDidFinishLoading:(UICellRefreshLoading *)view{
    [table reloadData];
}

#pragma mark - delegate search
- (void)didSetStartDate:(NSDate *)from endDate:(NSDate *)to kpknls:(NSArray *)kpknls {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    NSDateFormatter *f = [[[NSDateFormatter alloc] init] autorelease];
    f.dateFormat = @"yyyy-MM-dd";
    
    if (from){
        [dict setValue:[f stringFromDate:from] forKey:@"fromDate"];
    }
    if (to){
        [dict setValue:[f stringFromDate:to] forKey:@"toDate"];
    }
    if (kpknls) {
        [dict setValue:kpknls forKey:@"kpknlIDs"];
    }
    
    self.postParams = dict;
    currentPage = 0;
    [self makeRequest];
}

#pragma mark - delegate uidocument interaction

- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
    return self;
}

@end
