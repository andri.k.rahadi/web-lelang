//
//  VideoViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 9/26/17.
//  Copyright © 2017 Asep Mulyana. All rights reserved.
//

#import "VideoViewController.h"
#import "AppDelegate.h"
#import "IIViewDeckController.h"
#import "AppConfig.h"
#import "ASIHTTPRequest+Mandiri.h"
#import "MBProgressHUD.h"
#import "JSON.h"
#import "AppDelegate.h"
#import "UICellRefreshLoading.h"
#import "VideoCell.h"

@interface VideoViewController () <ASIHTTPRequestDelegate, MBProgressHUDDelegate, UICellRefreshLoadingDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout> {
    IBOutlet UICollectionView *collection;
    IBOutlet UILabel *blankMessage;
    
    NSOperationQueue *queue;
    NSInteger requestCount;
    UICellRefreshLoading *refreshViewHeader;
    BOOL isrefresh;
}

@property (nonatomic, retain) NSMutableArray *data;
@property (nonatomic, retain) MBProgressHUD *loading;

@end

@implementation VideoViewController

@synthesize data, loading;

- (void)dealloc
{
    collection.delegate = nil;
    self.data = nil;
    self.loading = nil;
    
    for(ASIHTTPRequest *req in queue.operations){
        req.delegate = nil;
    }
    [queue cancelAllOperations];
    [queue release];
    
    [super dealloc];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    queue = [[NSOperationQueue alloc] init];
    
    refreshViewHeader = [UICellRefreshLoading createNewAsHeader:YES forView:collection];
    refreshViewHeader.delegate = self;
    
    UINib *nib = [UINib nibWithNibName:@"VideoCell" bundle:nil];
    [collection registerNib:nib forCellWithReuseIdentifier:@"VideoCell"];
    
    [self makeRequest];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Actions

-(void)makeRequest{
    NSString *url = kURLVideo;
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:url]];
//    [request encryptParams];
    request.delegate = self;
    requestCount++;
    [queue addOperation:request];
}

-(IBAction)openMenu:(UIButton *)sender{
    if([AppDelegate currentDelegate].mainViewController.leftControllerIsClosed)
        [[AppDelegate currentDelegate] openMenu];
    else
        [[AppDelegate currentDelegate] closeMenu];
}

-(void)alert:(NSString *)msg{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [alert release];
}

#pragma mark - delegate ASIHTTP
-(void)requestStarted:(ASIHTTPRequest *)request{
    if(!self.loading && !isrefresh){
        self.loading = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.loading.labelText = @"Loading..";
        self.loading.delegate = self;
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request{
    [self countDownRequest];
    
    [self alert:@"Tidak terdapat koneksi internet."];
}

-(void)requestFinished:(ASIHTTPRequest *)request{
    [self countDownRequest];
    NSLog(@"\n==========\n video %@ : %@\n\n", request.url.absoluteString, request.responseString);
    
    NSDictionary *root = [request.responseString JSONValue];
    
    NSMutableArray *datas = [root valueForKey:@"data"];
//    if(request.tag == 0){
        self.data = datas;
//    }else{
//        [self.data addObjectsFromArray:datas];
//    }
    
    if (self.data.count < 1) {
        blankMessage.hidden = NO;
    }
    [collection reloadData];
}

-(void)countDownRequest{
    requestCount--;
    
    if(requestCount < 1){
        [self.loading hide:NO];
        
        if(isrefresh && refreshViewHeader.isLoading){
            [refreshViewHeader stopLoading];
        }
    }
}

#pragma mark - delegate loading
-(void)hudWasHidden:(MBProgressHUD *)hud{
    self.loading = nil;
}

#pragma mark - delegate scroll
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [refreshViewHeader scrollViewDidScroll:scrollView];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [refreshViewHeader scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}

#pragma mark - delegate refresh pull
-(void)UICellRefreshLoadingDidBeginLoading:(UICellRefreshLoading *)view{
    isrefresh = YES;
    collection.contentSize = collection.frame.size;
    [self makeRequest];
}

-(void)UICellRefreshLoadingDidFinishLoading:(UICellRefreshLoading *)view{
    [collection reloadData];
}


#pragma mark - delegate dan datasource collection

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return data.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    VideoCell *cell = [collection dequeueReusableCellWithReuseIdentifier:@"VideoCell" forIndexPath:indexPath];
    
    NSDictionary *dict = self.data[indexPath.row];
    cell.lblTitle.text = dict[@"title"];
    cell.imgThumb.placeholderImage = [UIImage imageNamed:@"img-default"];
    cell.imgThumb.imageURL = [NSURL URLWithString:[dict valueForKey:@"thumbnail"]];
    
    NSString *video = dict[@"videoId"];
    [cell.preview loadWithVideoId:video];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat w = (collection.frame.size.width - 30 ) * 0.5;
    return CGSizeMake(w, 148. / 158 * w);
}

@end
