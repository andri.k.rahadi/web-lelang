//
//  SearchResultViewController.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/9/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterViewController.h"

@interface SearchResultViewController : UIViewController

@property (nonatomic, retain) NSMutableDictionary *params;
@property (nonatomic) FilterType type;
@property (nonatomic) BOOL enableFilter;

@end
