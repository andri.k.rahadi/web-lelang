//
//  ComboTableViewController.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 9/14/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ComboTableViewControllerDelegate;

@interface ComboTableViewController : UIViewController

@property (nonatomic) BOOL multipleSelect;

@property (nonatomic, unsafe_unretained) id<ComboTableViewControllerDelegate>delegate;
@property (nonatomic) NSInteger selectedRow;
@property (nonatomic, retain) NSMutableArray *selectedRows;
@property (nonatomic, retain) NSArray *data;
@property (nonatomic) NSInteger tag;
@property (nonatomic) BOOL withSemua;

@end


@protocol ComboTableViewControllerDelegate <NSObject>

@optional
-(void)combo:(ComboTableViewController *)controller didSelectItemAtIndex:(NSInteger)index;
-(void)combo:(ComboTableViewController *)controller didSelectItemAtIndexes:(NSMutableArray *)indexes;

@end
