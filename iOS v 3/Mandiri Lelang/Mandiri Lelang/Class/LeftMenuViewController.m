//
//  LeftMenuViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/9/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "AppDelegate.h"
#import "AboutViewController.h"
#import "IIViewDeckController.h"
#import "HubungiViewController.h"
#import "JadwalViewController.h"
#import "ProfileViewController.h"
#import "LoginRegisterViewController.h"
#import "AppConfig.h"
#import "MBProgressHUD.h"
#import "ASIHTTPRequest+Mandiri.h"
#import "JSON.h"
#import "FavoriteViewController.h"
#import "VideoViewController.h"

@interface MenuTableViewCell: UITableViewCell
@end

@implementation MenuTableViewCell
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame = self.imageView.frame;
    frame.origin.x = self.indentationLevel*self.indentationWidth + 15;
    self.imageView.frame = frame;
    
    UIView *separ = [self viewWithTag:9];
    frame = separ.frame;
    frame.origin.x = self.indentationLevel*self.indentationWidth;
    separ.frame = frame;
}
@end


@interface LeftMenuViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, MBProgressHUDDelegate>{
    NSArray *menus;
    NSArray *submenus;
    NSInteger requestCount;
    BOOL subMenuOpened;
}

@property (nonatomic, retain) MBProgressHUD *loading;
@property (retain, nonatomic) IBOutlet UITableView *table;

@end

@implementation LeftMenuViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [menus release];
    [submenus release];
    [_table release];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    menus = [[NSArray arrayWithObjects:@"Home", @"Daftar Favorite", @"Jadwal Lelang", @"Informasi Lelang", @"Hubungi Kami", @"Logout", nil] retain];
    
    submenus = [[NSArray arrayWithObjects: @"Syarat & Ketentuan", @"FAQ", @"Tentang Kami", @"Video Gallery", nil] retain];
    
    [[NSNotificationCenter defaultCenter] addObserver:self.table selector:@selector(reloadData) name:@"LoginSuccess" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.table reloadData];
}

#pragma mark - Actions

- (void)logout {
    ASIFormDataRequest *r = [ASIFormDataRequest requestAuthWithURL:[NSURL URLWithString:kURLLogout]];
    r.delegate = self;
    r.tag = 1;
    [r startAsynchronous];
}

-(void)alert:(NSString *)msg{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [alert release];
}

- (void)refresh {
    [self.table reloadData];
}


#pragma mark - tableView delegat & datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return menus.count - 2;
    } else if (section == 2) {
        return submenus.count;
    }
    
    BOOL loggedIn = [AppConfig sharedConfig].sessionID.length > 0;
    return 2 - (loggedIn ? 0 : 1);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        BOOL loggedIn = [AppConfig sharedConfig].sessionID.length > 0;
        return loggedIn ? 64 : 0;
    } else if (indexPath.section == 2) {
        return subMenuOpened ? 46 : 0 ;
    }
    
    return 52.;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"testMenu"];
    
    if(cell == nil){
        cell = [[[MenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"testMenu"] autorelease];
        
        UIView *separator = [[[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height-1, 320., 1.)] autorelease];
        separator.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        separator.backgroundColor = [UIColor grayColor];
        separator.tag = 9;
        [cell addSubview:separator];
        
        cell.textLabel.font = [UIFont systemFontOfSize:14.];
        cell.textLabel.textColor = [UIColor grayColor];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    
    if (indexPath.section == 1) {
        BOOL loggedIn = [AppConfig sharedConfig].sessionID.length > 0;
        
        if (indexPath.row == 1) {
            if (loggedIn) {
                cell.imageView.image = [UIImage imageNamed:@"menu1"];
                cell.textLabel.text = @"DAFTAR FAVORIT";
            } else {
                cell.imageView.image = [UIImage imageNamed:@"menu-sign"];
                cell.textLabel.text = @"DAFTAR / MASUK";
            }
        } else {
            if (indexPath.row == 3) {
                cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", [[menus objectAtIndex:indexPath.row] uppercaseString], subMenuOpened ? @"   🔽" : @"   ▶️"];
            } else {
                cell.textLabel.text = [[menus objectAtIndex:indexPath.row] uppercaseString];
            }
            cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"menu%zd", indexPath.row]];
        }
    } else if (indexPath.section == 2) {
        cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"menu-%zd", indexPath.row]];
        cell.textLabel.text = [[submenus objectAtIndex:indexPath.row] uppercaseString];
    } else if (indexPath.section == 3) {
        cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"menu%zd", indexPath.row + 4]];
        cell.textLabel.text = [[menus objectAtIndex:indexPath.row + 4] uppercaseString];
    } else {
        cell.imageView.image = nil;
        cell.textLabel.text = [AppConfig sharedConfig].loginInfo[@"fullName"];
    }
    
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
        return 3;
    }
    
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 2 && indexPath.section == 3 ){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Logout?" message:@"Anda yakin akan logout?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Logout", nil];
        [alert show];
        return;
    } else if (indexPath.section == 1 && indexPath.row == 3) {
        subMenuOpened = !subMenuOpened;
        
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [tableView beginUpdates];
        [tableView endUpdates];
        return;
    }
    
    [[AppDelegate currentDelegate].mainViewController closeLeftViewBouncing:^(IIViewDeckController *controller) {
        if (indexPath.section == 0) {
            ProfileViewController *profile = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
            profile.fromMenu = YES;
            UINavigationController *nav = (UINavigationController *)[AppDelegate currentDelegate].mainViewController.centerController;
            [nav pushViewController:profile animated:YES];
        } else if (indexPath.section == 2) {
            if (indexPath.row < 3) {
                AboutViewController *about = [[[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil] autorelease];
                about.title = [menus objectAtIndex:indexPath.row];
                if(indexPath.row == 0){
                    about.url = [NSString stringWithFormat:@"%@/content/terms?mobile=1", ROOT_URL];
                }else if(indexPath.row == 1){
                    about.url = [NSString stringWithFormat:@"%@/content/faq?mobile=1", ROOT_URL];
                }else if(indexPath.row == 2){
                    about.url = [NSString stringWithFormat:@"%@/content/about?mobile=1", ROOT_URL];
                }
                
                UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:about] autorelease];
                nav.navigationBarHidden = YES;
                
                [AppDelegate currentDelegate].mainViewController.centerController = nav;
            } else {
                VideoViewController *hubungi = [[[VideoViewController alloc] initWithNibName:@"VideoViewController" bundle:nil] autorelease];
                
                UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:hubungi] autorelease];
                nav.navigationBarHidden = YES;
                
                [AppDelegate currentDelegate].mainViewController.centerController = nav;
            }
        } else if (indexPath.section == 1) {
            if(indexPath.row == 0){
                [AppDelegate currentDelegate].mainViewController.centerController = [AppDelegate currentDelegate].homeViewController;
            } else if(indexPath.row == 1){
                BOOL loggedIn = [AppConfig sharedConfig].sessionID.length > 0;
                
                if (loggedIn) {
                    FavoriteViewController *hubungi = [[[FavoriteViewController alloc] initWithNibName:@"FavoriteViewController" bundle:nil] autorelease];
                    
                    UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:hubungi] autorelease];
                    nav.navigationBarHidden = YES;
                    
                    [AppDelegate currentDelegate].mainViewController.centerController = nav;
                } else {
                    LoginRegisterViewController *profile = [[[LoginRegisterViewController alloc] initWithNibName:@"LoginRegisterViewController" bundle:nil] autorelease];
                    UINavigationController *nav = (UINavigationController *)[AppDelegate currentDelegate].mainViewController.centerController;
                    UINavigationController *navP = [[[UINavigationController alloc] initWithRootViewController:profile] autorelease];
                    navP.navigationBarHidden = YES;
                    [nav presentViewController:navP animated:YES completion:nil];
                }
            } else if(indexPath.row == 2){
                JadwalViewController *about = [[[JadwalViewController alloc] initWithNibName:@"JadwalViewController" bundle:nil] autorelease];
                
                UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:about] autorelease];
                nav.navigationBarHidden = YES;
                
                [AppDelegate currentDelegate].mainViewController.centerController = nav;
            }
        } else {
            HubungiViewController *hubungi = [[[HubungiViewController alloc] initWithNibName:@"HubungiViewController" bundle:nil] autorelease];
            
            UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:hubungi] autorelease];
            nav.navigationBarHidden = YES;
            
            [AppDelegate currentDelegate].mainViewController.centerController = nav;
        }
    }];
}


#pragma mark - alert delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1){
        [self logout];
    }
    [alertView release];
}


#pragma mark - delegate ASIHTTP
-(void)requestStarted:(ASIHTTPRequest *)request{
    [self showLoading];
}

- (void)showLoading{
    requestCount++;
    if(!self.loading){
        self.loading = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.loading.labelText = @"Loading..";
        self.loading.delegate = self;
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request{
    [self countDownRequest];
    
    [self alert:@"Tidak terdapat koneksi internet."];
}

-(void)requestFinished:(ASIHTTPRequest *)request{
    [self countDownRequest];
    NSLog(@"\n==========\n logout %@ : %@\n\n", request.url.absoluteString, request.responseString);
    
    NSDictionary *root = [request.responseString JSONValue];
    
    if ([[root valueForKeyPath:@"resultCode.code"] integerValue] == 0) {
        [AppConfig sharedConfig].sessionID = nil;
        [[AppDelegate currentDelegate].mainViewController closeLeftView];
        [self.table reloadData];
    } else {
        [self alert:[root valueForKeyPath:@"resultCode.message"]];
    }
}

-(void)countDownRequest{
    requestCount--;
    
    if(requestCount < 1){
        [self.loading hide:NO];
    }
}

#pragma mark - delegate loading
-(void)hudWasHidden:(MBProgressHUD *)hud{
    self.loading = nil;
}

@end
