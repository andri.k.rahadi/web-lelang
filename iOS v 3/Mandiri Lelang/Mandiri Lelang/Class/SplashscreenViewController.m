//
//  SplashscreenViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 9/11/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "SplashscreenViewController.h"

@interface SplashscreenViewController (){
    IBOutlet UIImageView *imageView;
}

@end

@implementation SplashscreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(self.view.frame.size.height > 480.){
        imageView.image = [UIImage imageNamed:@"Splash0.png"];
    }else{
        imageView.image = [UIImage imageNamed:@"SplashScreen"];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
