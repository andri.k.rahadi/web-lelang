//
//  HubungiViewController.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/17/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HubungiViewController : UIViewController

@property (nonatomic, strong) NSString *kodeAgunan;
@property (nonatomic, strong) NSString *pengelolaNama;
@property (nonatomic) BOOL withBackButton;

@end
