//
//  LoginRegisterViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 10/29/16.
//  Copyright © 2016 Asep Mulyana. All rights reserved.
//

#import "LoginRegisterViewController.h"
#import "ForgotPasswordViewController.h"
#import "AboutViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "MBProgressHUD.h"
#import "ASIHTTPRequest+Mandiri.h"
#import "AppConfig.h"
#import "JSON.h"
#import "AppDelegate.h"
#import "ProfileViewController.h"
#import "MLDatabase.h"
#import "ComboTableViewController.h"
#import "KuesionerViewController.h"

@interface LoginRegisterViewController () <MBProgressHUDDelegate, ComboTableViewControllerDelegate> {
    NSInteger requestCount;
}

@property (retain, nonatomic) IBOutlet UIView *viewRegisterForm;
@property (retain, nonatomic) IBOutlet UIView *viewFormLogin;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *constRegisterLeft;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *constIndicatorLeft;

@property (nonatomic, retain) MBProgressHUD *loading;
@property (retain, nonatomic) IBOutlet UITextField *txtRegNama;
@property (retain, nonatomic) IBOutlet UITextField *txtRegEmail;
@property (retain, nonatomic) IBOutlet UITextField *txtRegTglLahir;
@property (retain, nonatomic) IBOutlet UITextField *txtRegTempLahir;
@property (retain, nonatomic) IBOutlet UITextField *txtRegNoHP;
@property (retain, nonatomic) IBOutlet UITextField *txtRegAlamat;
@property (retain, nonatomic) IBOutlet UITextField *txtRegProvinsi;
@property (retain, nonatomic) IBOutlet UITextField *txtRegKota;
@property (retain, nonatomic) IBOutlet UITextField *txtRegKTP;
@property (retain, nonatomic) IBOutlet UITextField *txtRegPassword;
@property (retain, nonatomic) IBOutlet UITextField *txtRegRePassword;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *constRegBottom;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *constLoginBottom;
@property (retain, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (retain, nonatomic) IBOutlet UIButton *btnLogin;

@property (retain, nonatomic) IBOutlet UITextField *txtLoginEmail;
@property (retain, nonatomic) IBOutlet UITextField *txtLoginPassword;

@property (nonatomic, retain) NSMutableArray *propinsiArray;
@property (nonatomic, retain) NSMutableArray *kotaArray;

@property (retain, nonatomic) IBOutlet UILabel *lblTempatLahir;
@property (retain, nonatomic) IBOutlet UILabel *lblTglLahir;
@property (retain, nonatomic) IBOutlet UILabel *lblAlamat;
@property (retain, nonatomic) IBOutlet UILabel *lblProvince;
@property (retain, nonatomic) IBOutlet UILabel *lblCity;
@property (retain, nonatomic) IBOutlet UILabel *lblKTP;
@property (retain, nonatomic) IBOutlet UILabel *lblHP;


@end

@implementation LoginRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI:_viewRegisterForm];
    [self setupUI:_viewFormLogin];
    
    _datePicker.maximumDate = [[NSDate date] dateByAddingTimeInterval:-24 * 3600];
    _txtRegTglLahir.inputView = _datePicker;
    
    _constRegisterLeft.constant = 0;
    
    self.propinsiArray = [MLProvince sharedData].provinces;
    [self.propinsiArray removeObjectAtIndex:0];
    self.kotaArray = [[MLCity sharedData] citiesForProvince:@0];
    [self.kotaArray removeObjectAtIndex:0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerSucceed:) name:@"RegisterSuccess" object:nil];
    
    NSArray *settings = [[MLSettings sharedData] settings];
    
    for (MSettings *setting in settings) {
        if ([setting.identifier isEqualToString:@"MandatoryForMemberPlaceOfBirth"] && [setting.name isEqualToString:@"True"]) {
            _lblTempatLahir.text = @"Tempat Lahir *";
        } else if ([setting.identifier isEqualToString:@"MandatoryForMemberMobilePhoneNumber"] && [setting.name isEqualToString:@"True"]) {
            _lblHP.text = @"Nomor Handphone *";
        } else if ([setting.identifier isEqualToString:@"MandatoryForMemberDateOfBirth"] && [setting.name isEqualToString:@"True"]) {
            _lblTglLahir.text = @"Tanggal Lahir *";
        } else if ([setting.identifier isEqualToString:@"MandatoryForMemberAddress"] && [setting.name isEqualToString:@"True"]) {
            _lblAlamat.text = @"Alamat Tempat Tinggal *";
        } else if ([setting.identifier isEqualToString:@"MandatoryForMemberProvince"] && [setting.name isEqualToString:@"True"]) {
            _lblProvince.text = @"Provinsi *";
        } else if ([setting.identifier isEqualToString:@"MandatoryForMemberCity"] && [setting.name isEqualToString:@"True"]) {
            _lblCity.text = @"Kota *";
        } else if ([setting.identifier isEqualToString:@"MandatoryForMemberIdentificationNumber"] && [setting.name isEqualToString:@"True"]) {
            _lblKTP.text = @"Nomor KTP *";
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Actions

- (void)registerSucceed:(NSNotification *)notif {
    [self switchTo:_btnLogin];
    
    for (UITextField *one in _viewRegisterForm.subviews) {
        if ([one isKindOfClass:[UITextField class]]) {
            one.text = @"";
        }
    }
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)switchTo:(UIButton *)sender {
    if (sender.tag == 1) {
        _constIndicatorLeft.constant = sender.frame.origin.x;
        _constRegisterLeft.constant = -self.view.frame.size.width;
    } else {
        _constIndicatorLeft.constant = 0;
        _constRegisterLeft.constant = 0;
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
    }];
}

- (void)setupUI:(UIView *)view {
    for(UITextField *txt in view.subviews){
        if([txt isKindOfClass:[UITextField class]]){
            //            if(txt.tag > 0){
            //                UIPickerView *picker = [[[UIPickerView alloc] init] autorelease];
            //                picker.backgroundColor = [UIColor lightGrayColor];
            //                picker.delegate = self;
            //                picker.dataSource = self;
            //                picker.showsSelectionIndicator = YES;
            //                picker.tag = txt.tag;
            //                txt.inputView = picker;
            //            }
            
            txt.layer.masksToBounds = YES;
            txt.layer.cornerRadius = 3.;
            txt.layer.borderColor = [UIColor grayColor].CGColor;
            txt.layer.borderWidth = 1.;
            
            UIView *v = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 8., 10)] autorelease];
            v.backgroundColor = [UIColor clearColor];
            txt.leftView = v;
            txt.leftViewMode = UITextFieldViewModeAlways;
        }//else if([txt isKindOfClass:[UIButton class]]){
        //            UIButton *btn = (UIButton *)txt;
        //            btn.layer.masksToBounds = YES;
        //            btn.layer.cornerRadius = 3.;
        //            [btn setBackgroundImage:[Tools solidImageForColor:btn.backgroundColor withSize:btn.frame.size] forState:UIControlStateNormal];
        //        }
    }
}

- (IBAction)forgot:(id)sender {
    ForgotPasswordViewController *forgot = [[[ForgotPasswordViewController alloc] initWithNibName:@"ForgotPasswordViewController" bundle:nil] autorelease];
    [self.navigationController pushViewController:forgot animated:YES];
}

- (IBAction)syarat:(id)sender {
    AboutViewController *about = [[[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil] autorelease];
    about.url = [NSString stringWithFormat:@"%@/content/terms?mobile=1", ROOT_URL];
    about.isNavMode = YES;
    
    [self.navigationController pushViewController:about animated:YES];
}

- (IBAction)loginFB:(id)sender {
    [self registerFB:nil];
}

- (IBAction)registerFB:(id)sender {
    [self showLoading];
    FBSDKLoginManager *manager = [[FBSDKLoginManager alloc] init];
    [manager logOut];
//    manager.loginBehavior = FBSDKLoginBehaviorWeb;
    [manager logInWithReadPermissions:@[@"email", @"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (result.isCancelled) {
            [self alert:@"Login facebook dibatalkan."];
        } else {
            if ([result.grantedPermissions containsObject:@"email"]) {
                [self getFBInfo];
            } else {
                [self alert:@"Tidak dapat membaca email dari facebook Anda."];
            }
        }
        
        [self countDownRequest];
        [manager release];
    }];
}

- (void)getFBInfo {
    FBSDKGraphRequest *req = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields" : @"id,name,email,website"}];
    [req startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        NSLog(@"Fetch result %@", result);
        
        NSString *token = [FBSDKAccessToken currentAccessToken].tokenString;
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:kURLFacebook]];
        [request addPostValue:result[@"name"] forKey:@"fullName"];
        [request addPostValue:result[@"email"] forKey:@"email"];
        [request addPostValue:token forKey:@"token"];
        [request addPostValue:result[@"website"] forKey:@"url"];
        [request addPostValue:result[@"id"] forKey:@"id"];
        [request encryptParams];
        request.delegate = self;
        request.tag = 1;
        [request startAsynchronous];
        
        [req release];
    }];
}

- (BOOL)isLoginFormValid {
    if (_txtLoginEmail.text.length < 1){
        [self alert:@"Mohon isi Email"];
        return NO;
    }
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    if(![predicate evaluateWithObject:_txtLoginEmail.text]) {
        [self alert:@"Mohon isi email yang valid"];
        return NO;
    }
    
    if (_txtLoginPassword.text.length < 1){
        [self alert:@"Mohon isi Password"];
        return NO;
    }
    
    NSRange lowerRange = [_txtLoginPassword.text rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]];
    //    NSRange upperRange = [_txtLoginPassword.text rangeOfCharacterFromSet:[NSCharacterSet uppercaseLetterCharacterSet]];
    NSRange numberRange = [_txtLoginPassword.text rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]];
    //    NSRange simbolRange = [_txtLoginPassword.text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"]];
    
    if (_txtLoginPassword.text.length < 8 || lowerRange.length < 1 /*|| upperRange.length < 1*/ || numberRange.length < 1 /*|| simbolRange.length < 1*/) {
        //        NSLog(@"lower=%d upper=%d number=%d simbol=%d", lowerRange.length < 1, upperRange.length < 1, numberRange.length < 1, simbolRange.length < 1);
        [self alert:@"Password harus terdiri dari minimum 8 karakter, mengandung huruf dan angka"];
        return NO;
    }
    
    return YES;
}

- (IBAction)login:(id)sender {
    if (![self isLoginFormValid]) {
        return;
    }
    
    [self hideKeyboard:nil];
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:kURLLogin]];
    [request addPostValue:_txtLoginEmail.text forKey:@"email"];
    [request addPostValue:_txtLoginPassword.text forKey:@"password"];
    [request encryptParams];
    request.delegate = self;
    request.tag = 2;
    [request startAsynchronous];
}

-(void)alert:(NSString *)msg{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [alert release];
}

- (BOOL)isRegFormValid {
    if (_txtRegNama.text.length < 1){
        [self alert:@"Mohon isi Nama Lengkap"];
        return NO;
    }
    if (_txtRegEmail.text.length < 1){
        [self alert:@"Mohon isi Email"];
        return NO;
    }
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    if(![predicate evaluateWithObject:_txtRegEmail.text]) {
        [self alert:@"Mohon isi email yang valid"];
        return NO;
    }
    
    NSString *alphabetRegex = @"[A-Za-z ]+";
    NSPredicate *alphabetPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", alphabetRegex];
    
    if(![alphabetPredicate evaluateWithObject:_txtRegTempLahir.text] && _txtRegTempLahir.text.length > 0) {
        [self alert:@"Mohon isi tempat lahir yang valid (hanya terdiri dari huruf dan spasi)"];
        return NO;
    }
    
    MSettings *setting = [[MLSettings sharedData] settingsWithID:@"MandatoryForMemberPlaceOfBirth"];
    if ([setting.name isEqualToString:@"True"] && _txtRegTempLahir.text.length < 1) {
        [self alert:@"Mohon isi Tempat Lahir"];
        return NO;
    }
    
    setting = [[MLSettings sharedData] settingsWithID:@"MandatoryForMemberDateOfBirth"];
    if ([setting.name isEqualToString:@"True"] && _txtRegTglLahir.text.length < 1) {
        [self alert:@"Mohon isi Tanggal Lahir"];
        return NO;
    }
    
    setting = [[MLSettings sharedData] settingsWithID:@"MandatoryForMemberMobilePhoneNumber"];
    if ([setting.name isEqualToString:@"True"] && _txtRegNoHP.text.length < 1) {
        [self alert:@"Mohon isi Nomor Handphone"];
        return NO;
    }
    
    setting = [[MLSettings sharedData] settingsWithID:@"MandatoryForMemberAddress"];
    if ([setting.name isEqualToString:@"True"] && _txtRegAlamat.text.length < 1) {
        [self alert:@"Mohon isi Alamat Tempat Tinggal"];
        return NO;
    }
    
    setting = [[MLSettings sharedData] settingsWithID:@"MandatoryForMemberProvince"];
    if ([setting.name isEqualToString:@"True"] && _txtRegProvinsi.text.length < 1) {
        [self alert:@"Mohon isi Provinsi"];
        return NO;
    }
    
    setting = [[MLSettings sharedData] settingsWithID:@"MandatoryForMemberCity"];
    if ([setting.name isEqualToString:@"True"] && _txtRegKota.text.length < 1) {
        [self alert:@"Mohon isi Provinsi dan Kota"];
        return NO;
    }
    
    setting = [[MLSettings sharedData] settingsWithID:@"MandatoryForMemberIdentificationNumber"];
    if ([setting.name isEqualToString:@"True"] && _txtRegKTP.text.length < 1) {
        [self alert:@"Mohon isi Nomor KTP"];
        return NO;
    }
    
    if (_txtRegPassword.text.length < 1){
        [self alert:@"Mohon isi Password"];
        return NO;
    }
    
    NSRange lowerRange = [_txtLoginPassword.text rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]];
    //    NSRange upperRange = [_txtLoginPassword.text rangeOfCharacterFromSet:[NSCharacterSet uppercaseLetterCharacterSet]];
        NSRange numberRange = [_txtLoginPassword.text rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]];
    //    NSRange simbolRange = [_txtLoginPassword.text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"]];
    
    if (_txtLoginPassword.text.length < 8 || lowerRange.length < 1 /*|| upperRange.length < 1*/ || numberRange.length < 1 /*|| simbolRange.length < 1*/) {
        //        NSLog(@"lower=%d upper=%d number=%d simbol=%d", lowerRange.length < 1, upperRange.length < 1, numberRange.length < 1, simbolRange.length < 1);
        [self alert:@"Password harus terdiri dari minimum 8 karakter, mengandung huruf dan angka"];
        return NO;
    }
    
    if (![_txtRegRePassword.text isEqualToString:_txtRegPassword.text]){
        [self alert:@"Konfirmasi Password harus sama dengan Password"];
        return NO;
    }
    
    return YES;
}

- (IBAction)next:(id)sender {
    if (![self isRegFormValid]) {
        return;
    }
    
    [self hideKeyboard:nil];
    
    NSString *tgl = @"";
    
    if (_txtRegTglLahir.text.length > 0) {
        NSDateFormatter *f = [[[NSDateFormatter alloc] init] autorelease];
        f.dateFormat = @"dd MMMM yyyy";
        NSDate *date = [f dateFromString:_txtRegTglLahir.text];
        f.dateFormat = @"yyyy-MM-dd";
        tgl = [f stringFromDate:date];
    }
    
    NSNumber *pid = _txtRegProvinsi.tag < 0 ? @0 : [[self.propinsiArray objectAtIndex:_txtRegProvinsi.tag] valueForKey:@"identifier"];
    NSNumber *kid = _txtRegKota.tag < 0 ? @0 : [[self.kotaArray objectAtIndex:_txtRegKota.tag] valueForKey:@"identifier"];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSMutableDictionary *profile = [NSMutableDictionary dictionary];
    profile[@"fullName"] = _txtRegNama.text;
    profile[@"email"] = _txtRegEmail.text;
    profile[@"placeOfBirth"] = _txtRegTempLahir.text;
    profile[@"dateOfBirth"] = tgl;
    profile[@"mobilePhoneNumber"] = _txtRegNoHP.text;
    profile[@"address"] = _txtRegAlamat.text;
    profile[@"provinceID"] = pid.integerValue < 1 ? [NSNull null] : pid;
    profile[@"cityID"] = kid.integerValue < 1 ? [NSNull null] : kid;
    profile[@"identificationNumber"] = _txtRegKTP.text;
    profile[@"password"] = _txtRegPassword.text;
    [dict setValue:profile forKey:@"profile"];
    
    KuesionerViewController *kuis = [[[KuesionerViewController alloc] initWithNibName:@"KuesionerViewController" bundle:nil] autorelease];
    kuis.data = dict;
    kuis.fromRegister = YES;
    [self.navigationController pushViewController:kuis animated:YES];
}

- (IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

-(IBAction)dateChanged:(UIDatePicker *)sender{
    NSDateFormatter *format = [[[NSDateFormatter alloc] init] autorelease];
    [format setDateFormat:@"dd MMMM yyyy"];
    _txtRegTglLahir.text = [format stringFromDate:sender.date];
}

-(IBAction)showCombo:(UIButton *)sender{
    ComboTableViewController *combo = [[ComboTableViewController alloc] initWithNibName:@"ComboTableViewController" bundle:nil];
    combo.tag = sender.tag;
    combo.delegate = self;
    
    if(sender.tag == 0){
        combo.data = self.propinsiArray;
        combo.selectedRow = _txtRegProvinsi.tag;
    }else if(sender.tag == 1){
        combo.data = self.kotaArray;
        combo.selectedRow = _txtRegKota.tag;
    }
    
    [self.navigationController pushViewController:combo animated:YES];
    [combo release];
}


#pragma mark - delegate combo
-(void)combo:(ComboTableViewController *)controller didSelectItemAtIndex:(NSInteger)index{
    if(controller.tag == 0){
        _txtRegProvinsi.text = [[self.propinsiArray objectAtIndex:index] valueForKey:@"name"];
        _txtRegProvinsi.tag = index;
        
        NSNumber *pid = [[self.propinsiArray objectAtIndex:index] valueForKey:@"identifier"];
        self.kotaArray = [[MLCity sharedData] citiesForProvince:pid];
        [self.kotaArray removeObjectAtIndex:0];
        
        _txtRegKota.tag = -1;
        _txtRegKota.text = @"";
    }else if(controller.tag == 1){
        _txtRegKota.text = [[self.kotaArray objectAtIndex:index] valueForKey:@"name"];
        _txtRegKota.tag = index;
    }
}

#pragma mark - delegate ASIHTTP
-(void)requestStarted:(ASIHTTPRequest *)request{
    [self showLoading];
}

- (void)showLoading{
    requestCount++;
    if(!self.loading){
        self.loading = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.loading.labelText = @"Loading..";
        self.loading.delegate = self;
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request{
    [self countDownRequest];
    
    [self alert:@"Tidak terdapat koneksi internet."];
}

-(void)requestFinished:(ASIHTTPRequest *)request{
    [self countDownRequest];
    NSLog(@"\n==========\n reg %@ : %@\n\n", request.url.absoluteString, request.responseString);
    
    if (request.tag == 1) {
        [AppConfig sharedConfig].loginFromFB = YES;
    } else {
        [AppConfig sharedConfig].loginFromFB = NO;
    }
    
    NSDictionary *root = [request.responseString JSONValue];
    
    NSDictionary *result = [root valueForKey:@"resultCode"];
    if ([[result valueForKey:@"code"] integerValue] == 0) {
        NSDictionary *data = root[@"data"];
        [AppConfig sharedConfig].loginInfo = data;
        [AppConfig sharedConfig].sessionID = data[@"token"];
        
        if ([[data valueForKey:@"isProfileComplete"] boolValue]) {
            [self.navigationController popToRootViewControllerAnimated:NO];
            [self dismissViewControllerAnimated:YES completion:nil];
            [AppDelegate currentDelegate].window.rootViewController = (UIViewController *)[AppDelegate currentDelegate].mainViewController;
        } else {
            ProfileViewController *profile = [[[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil] autorelease];
            profile.fromRegister = YES;
            [self.navigationController pushViewController:profile animated:YES];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginSuccess" object:nil];
    }else {
        [self alert:result[@"message"]];
    }
}

-(void)countDownRequest{
    requestCount--;
    
    if(requestCount < 1){
        [self.loading hide:NO];
    }
}

#pragma mark - delegate loading
-(void)hudWasHidden:(MBProgressHUD *)hud{
    self.loading = nil;
}


#pragma mark - Observer keyboard
-(void)keyboardDidHide:(NSNotification *)sender{
    _constRegBottom.constant = 0;
    _constLoginBottom.constant = 0;
}

-(void)keyboardDidShow:(NSNotification *)sender{
    CGRect keyboardRect = [[sender.userInfo valueForKeyPath:@"UIKeyboardBoundsUserInfoKey"] CGRectValue];
    
    _constRegBottom.constant = keyboardRect.size.height - 42;
    _constLoginBottom.constant = keyboardRect.size.height - 42;
}


- (void)dealloc {
    [_viewRegisterForm release];
    [_constRegisterLeft release];
    [_constIndicatorLeft release];
    [_viewFormLogin release];
    [_txtRegNama release];
    [_txtRegEmail release];
    [_txtRegTglLahir release];
    [_txtRegTempLahir release];
    [_txtRegNoHP release];
    [_txtRegAlamat release];
    [_txtRegProvinsi release];
    [_txtRegKota release];
    [_txtRegKTP release];
    [_txtRegRePassword release];
    [_txtRegPassword release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [_constRegBottom release];
    [_constLoginBottom release];
    [_datePicker release];
    
    [_propinsiArray release];
    [_kotaArray release];
    
    [_txtLoginEmail release];
    [_txtLoginPassword release];
    [_btnLogin release];
    [_lblTempatLahir release];
    [_lblTglLahir release];
    [_lblAlamat release];
    [_lblProvince release];
    [_lblCity release];
    [_lblKTP release];
    [_lblHP release];
    [super dealloc];
}
@end
