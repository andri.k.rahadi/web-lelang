//
//  DetailViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/9/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "DetailViewController.h"
#import "DetailTableViewCell.h"
#import "EGOImageView.h"
#import "DetailHeaderView.h"
#import "JadwalTableViewCell.h"
#import "TableViewCell.h"
#import "UICellRefreshLoading.h"
#import "ASIHTTPRequest+Mandiri.h"
#import "MBProgressHUD.h"
#import "JSON.h"
#import "AppConfig.h"
#import "TTTAttributedLabel.h"
#import "EGOImageButton.h"
#import "XHImageViewer.h"
#import <MapKit/MapKit.h>
#import "MapsViewController.h"
#import "LoginRegisterViewController.h"
#import "HubungiViewController.h"

@interface DetailViewController ()<UITableViewDataSource, UITableViewDelegate, UICellRefreshLoadingDelegate, ASIHTTPRequestDelegate, MBProgressHUDDelegate, TTTAttributedLabelDelegate, XHImageViewerDelegate, MKAnnotation>{
    IBOutlet UILabel *judul;
    IBOutlet EGOImageButton *image;
    IBOutlet UILabel *header2;
    IBOutlet UITableViewCell *thumbCell;
    IBOutlet UITableView *table;
    IBOutlet TTTAttributedLabel *hargaLama;
    IBOutlet UILabel *hargaBaru;
    IBOutlet UILabel *hotPriceTag;
    IBOutlet UITableViewCell *mapCell;
    IBOutlet MKMapView *maps;
    IBOutlet UILabel *eAuctionTag;
    IBOutlet UIButton *btnFav;
    IBOutlet UIButton *btnFav2;
    IBOutlet UILabel *lelangTag;
    IBOutlet UILabel *sukarelaTag;
    
    NSArray *details;
    
    NSOperationQueue *queue;
    NSInteger requestCount;
    UICellRefreshLoading *refreshViewHeader;
    BOOL isrefresh;
    BOOL hasLocation;
}

-(IBAction)back:(id)sender;
-(IBAction)zoomImage:(id)sender;
-(IBAction)share:(id)sender;
-(IBAction)openMap:(id)sender;

@property (nonatomic, retain) MBProgressHUD *loading;

@end

@implementation DetailViewController

@synthesize dataDetail, loading, assetID, data;

- (void)dealloc
{
    table.delegate = nil;
    self.dataDetail = nil;
    self.loading = nil;
    self.assetID = nil;
    self.data = nil;
    
    for(ASIHTTPRequest *req in queue.operations){
        req.delegate = nil;
    }
    [queue cancelAllOperations];
    [queue release];
    
    [eAuctionTag release];
    [lelangTag release];
    [sukarelaTag release];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    hotPriceTag.layer.masksToBounds = YES;
    hotPriceTag.layer.cornerRadius = 0.5 * hotPriceTag.frame.size.height;
    
    eAuctionTag.layer.masksToBounds = YES;
    eAuctionTag.layer.cornerRadius = 0.5 * eAuctionTag.frame.size.height;
    eAuctionTag.backgroundColor = [UIColor colorWithRed:0x5b/255. green:0xc0/255. blue:0xde/255. alpha:1];
    
    lelangTag.layer.masksToBounds = YES;
    lelangTag.layer.cornerRadius = 0.5 * lelangTag.frame.size.height;
    lelangTag.backgroundColor = [UIColor colorWithRed:0xff/255. green:0xd7/255. blue:0x00/255. alpha:1];
    
    sukarelaTag.layer.masksToBounds = YES;
    sukarelaTag.layer.cornerRadius = 0.5 * sukarelaTag.frame.size.height;
    sukarelaTag.backgroundColor = [UIColor colorWithRed:0x5c/255. green:0xb8/255. blue:0x5c/255. alpha:1];
    
    details = [[NSArray arrayWithObjects:@"Lokasi", @"Dokumen", nil] retain];
    
    judul.text = @"";
    image.imageURL = [NSURL URLWithString:[self.data valueForKey:@"defaultPhotoURL"]];
    image.imageView.contentMode = UIViewContentModeScaleToFill;
    btnFav.selected = [self.data[@"isFavorite"] boolValue];
    btnFav2.selected = [self.data[@"isFavorite"] boolValue];
    
    queue = [[NSOperationQueue alloc] init];
    
    refreshViewHeader = [UICellRefreshLoading createNewAsHeader:YES forView:table];
    refreshViewHeader.delegate = self;
    
    [self makeRequest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
-(void)back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)berminat:(id)sender {
    HubungiViewController *hub = [[HubungiViewController alloc] initWithNibName:@"HubungiViewController" bundle:nil];
    NSLog(@"data %@", self.dataDetail);
    hub.kodeAgunan = self.dataDetail[@"assetCode"];
    hub.pengelolaNama = self.dataDetail[@"branchName"];
    hub.withBackButton = YES;
    [self.navigationController pushViewController:hub animated:YES];
}

-(void)zoomImage:(id)sender{
    NSMutableArray *arr = [NSMutableArray array];
    CGRect frameAsal = [self.view convertRect:image.frame fromView:image.superview];
    
    for(int i=1; i<=5; i++){
        NSString *urlImg = [self.dataDetail valueForKey:[NSString stringWithFormat:@"photo%dURL", i]];
        
        if(urlImg.length > 5){
            EGOImageView *img = [[[EGOImageView alloc] initWithFrame:frameAsal] autorelease];
            img.placeholderImage = [UIImage imageNamed:@"img-default"];
            img.imageURL = [NSURL URLWithString:urlImg];
            [arr addObject:img];
        }
    }
    
    XHImageViewer *imageViewer = [[XHImageViewer alloc] init];
    imageViewer.delegate = self;
    [imageViewer showWithImageViews:arr selectedView:[arr objectAtIndex:0]];
}

-(void)makeRequest{
    NSString *url = [NSString stringWithFormat:kURLDetailAsset, self.assetID, [[AppConfig sharedConfig].cityID integerValue]];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:url]];
    [request encryptParams];
    request.delegate = self;
    requestCount++;
    [queue addOperation:request];
}

- (IBAction)addRemoveFav:(UIButton *)sender {
    BOOL loggedIn = [AppConfig sharedConfig].sessionID.length > 0;
    
    if (loggedIn) {
        NSString *url = sender.selected ? kURLRemoveFavorite : kURLAddFavorite;
        ASIFormDataRequest *request = [ASIFormDataRequest requestAuthWithURL:[NSURL URLWithString:url]];
        [request setPostValue:self.assetID forKey:@"assetID"];
        request.delegate = self;
        request.tag = sender.selected ? 4 : 3;
        requestCount++;
        [queue addOperation:request];
    } else {
        LoginRegisterViewController *login = [[LoginRegisterViewController alloc] initWithNibName:@"LoginRegisterViewController" bundle:nil];
        [self presentViewController:login animated:YES completion:nil];
        [login release];
    }
}

-(void)share:(id)sender{
//    NSString *title = @"Ada info property di http://lelang.bankmandiri.co.id/listings/";
    NSString *text = [self.dataDetail valueForKey:@"EmailText"];
    
    NSString *decoded = (NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (CFStringRef)text, CFSTR(""), kCFStringEncodingUTF8);
    
    NSMutableArray *sharingItems = [NSMutableArray new];
    if (text) {
        [sharingItems addObject:[decoded stringByReplacingOccurrencesOfString:@"+" withString:@" "]];
    }
//    if (url) {
        [sharingItems addObject:[NSURL URLWithString:[self.dataDetail valueForKey:@"webUrl"]]];
//    }
    UIActivityViewController *activityController = [[[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil] autorelease];
    [self presentViewController:activityController animated:YES completion:nil];
}

-(void)openMap:(id)sender{
//    MapsViewController *map = [[[MapsViewController alloc] initWithNibName:@"MapsViewController" bundle:nil] autorelease];
//    [self.navigationController pushViewController:map animated:YES];
    NSString *strMap = [NSString stringWithFormat:@"http://maps.apple.com/?ll=%lf,%lf&saddr=%lf,%lf&daddr=%lf,%lf", [self coordinate].latitude, [self coordinate].longitude, [self coordinate].latitude-0.01, [self coordinate].longitude-0.01, [self coordinate].latitude, [self coordinate].longitude];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strMap]];
}

-(void)alert:(NSString *)msg{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [alert release];
}

- (IBAction)openEauction:(id)sender {
    NSString *str = self.dataDetail[@"emailAuctionURL"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

#pragma mark - tableView delegate dan datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 1)return 5;
    else if(section == 2)return 3;
    else if(section == 3)return 4;
    
    return details.count + 2 + [[self.dataDetail valueForKey:@"extraFields"] count] - (hasLocation?0:1);
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 47.;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"DetailHeaderView" owner:self options:nil];
    DetailHeaderView *view = nil;
    
    for(id one in arr){
        if([one isKindOfClass:[DetailHeaderView class]]){
            view = (DetailHeaderView *)one;
        }
    }
    
    if(section == 0){
        view.titleLabel.text = @"INFORMASI";
    }else if(section == 1){
        view.titleLabel.text = @"JADWAL LELANG";
    }else if(section == 2){
        view.titleLabel.text = @"PENGELOLA";
    }else if(section == 3){
        view.titleLabel.text = @"SYARAT DAN KETENTUAN";
    }
    
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        if(indexPath.row == 0){
//            if (eAuctionTag.hidden) {
//                return 363;
//            }
            
            return 388;
        }else if(hasLocation && indexPath.row == [self tableView:tableView numberOfRowsInSection:indexPath.section]-1){
            return mapCell.frame.size.height;
        }
    }else if(indexPath.section == 1){
        if(indexPath.row == 0){
            JadwalTableViewCell *cell = (JadwalTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
            return cell.height;
        }
    }else if(indexPath.section == 3){
        TableViewCell *cell = (TableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        CGSize size = [cell.textLabel.text sizeWithFont:cell.textLabel.font constrainedToSize:CGSizeMake(272., 2000.) lineBreakMode:NSLineBreakByWordWrapping];
        
        return 35 - 20. + size.height;
    }
    
    DetailTableViewCell *cell = (DetailTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    CGSize size = [cell.valueLabel.text sizeWithFont:cell.valueLabel.font constrainedToSize:CGSizeMake(cell.valueLabel.frame.size.width, 2000.) lineBreakMode:NSLineBreakByWordWrapping];
    
    return 28 + size.height;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        if(indexPath.row == 0){
            return thumbCell;
        }else if(hasLocation && indexPath.row == [self tableView:tableView numberOfRowsInSection:indexPath.section]-1){
            return mapCell;
        }
    }else if(indexPath.section == 1 && indexPath.row == 0){
        JadwalTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JadwalTableViewCell"];
        
        if(cell == nil){
            NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"JadwalTableViewCell" owner:self options:nil];
            
            for(id one in arr){
                if([one isKindOfClass:[JadwalTableViewCell class]]){
                    cell = (JadwalTableViewCell *)one;
                }
            }
        }
        
        cell.tanggal.text = [self.dataDetail valueForKey:@"auctionDateFormattedDateTime"];
        cell.alamat.text = [self.dataDetail valueForKey:@"kpknlAddress"];
        cell.penyelenggara.text = [self.dataDetail valueForKey:@"kpknlName"];
        
        NSString *txt = [[NSString stringWithFormat:@"%@\n%@", [self.dataDetail valueForKey:@"kpknlPhone1"], [self.dataDetail valueForKey:@"kpknlPhone2"]] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        cell.telepon.text = txt;
        cell.separator.hidden = YES;
        [cell updateLayout];
        
        NSString *telp1 = [self.dataDetail valueForKey:@"kpknlPhone1"];
        if(telp1.length > 2){
            cell.telepon.text = [cell.telepon.text stringByReplacingOccurrencesOfString:telp1 withString:[NSString stringWithFormat:@"📞 %@", telp1]];
        }
        telp1 = [self.dataDetail valueForKey:@"kpknlPhone2"];
        if(telp1.length > 2){
            cell.telepon.text = [cell.telepon.text stringByReplacingOccurrencesOfString:telp1 withString:[NSString stringWithFormat:@"📞 %@", telp1]];
        }
        
        telp1 = [self.dataDetail valueForKey:@"kpknlPhone1"];
        if(telp1.length > 2){
            [cell.telepon addLinkToURL:[NSURL URLWithString:[NSString stringWithFormat:@"telpon://%@", removeSpace(telp1)]] withRange:[cell.telepon.text rangeOfString:telp1]];
            cell.telepon.delegate = self;
            cell.telepon.userInteractionEnabled = YES;
        }
        telp1 = [self.dataDetail valueForKey:@"kpknlPhone2"];
        if(telp1.length > 2){
            [cell.telepon addLinkToURL:[NSURL URLWithString:[NSString stringWithFormat:@"telpon://%@", removeSpace(telp1)]] withRange:[cell.telepon.text rangeOfString:telp1]];
            cell.telepon.delegate = self;
            cell.telepon.userInteractionEnabled = YES;
        }
        
        return cell;
    }else if(indexPath.section == 3){
        TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableViewCell"];
        
        if(cell == nil){
            NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"TableViewCell" owner:self options:nil];
            
            for(id one in arr){
                if([one isKindOfClass:[TableViewCell class]]){
                    cell = (TableViewCell *)one;
                }
            }
        }
        
        cell.numberLabel.text = [NSString stringWithFormat:@"%zd", indexPath.row + 1];
        
        if(indexPath.row == 0){
            cell.textLabel.text = @"Lokasi yang ditunjukkan dalam google map atau peta hanya merupakan alat bantu perkiraan lokasi agunan.";
        }else if(indexPath.row == 1){
            cell.textLabel.text = @"Harga limit lelang yang tercantum hanya berlaku pada tanggal pelaksanaan lelang tersebut.";
        }else if(indexPath.row == 2){
            cell.textLabel.text = @"Apabila terdapat perbedaan informasi yang terdapat dalam website dengan pengumuman lelang di media massa, maka yang digunakan adalah pengumuman lelang dalam media massa yang berlaku untuk pelaksanaan lelang tersebut.";
        }else if(indexPath.row == 3){
            cell.textLabel.text = @"Agunan dijual secara apa adanya (as is) sesuai kondisi pada saat pelaksanaan lelang.";
        }
        
        return cell;
    }
    
    DetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailTableViewCell"];
    
    if(cell == nil){
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"DetailTableViewCell" owner:self options:nil];
        
        for(id one in arr){
            if([one isKindOfClass:[DetailTableViewCell class]]){
                cell = (DetailTableViewCell *)one;
            }
        }
    }
    
    NSDictionary *dict = (NSDictionary *)self.dataDetail;
    
    if(indexPath.section == 0){
        switch (indexPath.row-1) {
            case 0:
                cell.titleLabel.text = [details objectAtIndex:0];
                cell.valueLabel.text = [dict valueForKey:@"completeAddress"];
                break;
            case 1:
                cell.titleLabel.text = [details objectAtIndex:1];
                cell.valueLabel.text = [dict valueForKey:@"documentTypeName"];
                break;
            default:{
                NSArray *arr = [self.dataDetail valueForKey:@"extraFields"];
                NSDictionary *d = [arr objectAtIndex:indexPath.row - 3];
                cell.titleLabel.text = [d valueForKey:@"fieldName"];
                cell.valueLabel.text = [d valueForKey:@"value"];
            }
                break;
        }
    }else if(indexPath.section == 1){
        if(indexPath.row == 1){
            cell.titleLabel.text = @"Balai Lelang :";
            NSString *txt = [self.dataDetail valueForKey:@"auctionHallName"];
            cell.valueLabel.text = txt.length<1?@"-":txt;
        }else if(indexPath.row == 2){
            cell.titleLabel.text = @"Lokasi";
            NSString *txt = [self.dataDetail valueForKey:@"auctionHallAddress"];
            cell.valueLabel.text = txt.length<1?@"-":txt;
        }else if(indexPath.row == 3){
            cell.titleLabel.text = @"Telepon";
            NSString *txt = [[NSString stringWithFormat:@"%@\n%@", [self.dataDetail valueForKey:@"auctionHallPhone1"], [self.dataDetail valueForKey:@"auctionHallPhone2"]] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            cell.valueLabel.text = txt.length<1?@"-":txt;
            
            NSString *telp1 = [self.dataDetail valueForKey:@"auctionHallPhone1"];
            if(telp1.length > 2){
                cell.valueLabel.text = [cell.valueLabel.text stringByReplacingOccurrencesOfString:telp1 withString:[NSString stringWithFormat:@"📞 %@", telp1]];
            }
            telp1 = [self.dataDetail valueForKey:@"auctionHallPhone2"];
            if(telp1.length > 2){
                cell.valueLabel.text = [cell.valueLabel.text stringByReplacingOccurrencesOfString:telp1 withString:[NSString stringWithFormat:@"📞 %@", telp1]];
            }
            
            telp1 = [self.dataDetail valueForKey:@"auctionHallPhone1"];
            if(telp1.length > 2){
                [cell.valueLabel addLinkToURL:[NSURL URLWithString:[NSString stringWithFormat:@"telpon://%@", removeSpace(telp1)]] withRange:[cell.valueLabel.text rangeOfString:telp1]];
                cell.valueLabel.delegate = self;
                cell.valueLabel.userInteractionEnabled = YES;
            }
            telp1 = [self.dataDetail valueForKey:@"auctionHallPhone2"];
            if(telp1.length > 2){
                [cell.valueLabel addLinkToURL:[NSURL URLWithString:[NSString stringWithFormat:@"telpon://%@", removeSpace(telp1)]] withRange:[cell.valueLabel.text rangeOfString:telp1]];
                cell.valueLabel.delegate = self;
                cell.valueLabel.userInteractionEnabled = YES;
            }
            
        }else if(indexPath.row == 4){
            cell.titleLabel.text = @"Informasi";
            NSString *txt = [self.dataDetail valueForKey:@"transferInformation"];
            cell.valueLabel.text = txt.length<1?@"-":txt;
        }
    }else if(indexPath.section == 2){
        if(indexPath.row == 0){
            cell.titleLabel.text = @"Dikelola Oleh";
            cell.valueLabel.text = [self.dataDetail valueForKey:@"branchName"];
        }else if(indexPath.row == 1){
            cell.titleLabel.text = @"Alamat";
            cell.valueLabel.text = [self.dataDetail valueForKey:@"branchAddress"];
        }else if(indexPath.row == 2){
            cell.titleLabel.text = @"Telepon";
            cell.valueLabel.text = [[NSString stringWithFormat:@"%@\n%@", [self.dataDetail valueForKey:@"branchPhone1"], [self.dataDetail valueForKey:@"branchPhone2"]] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *telp1 = [self.dataDetail valueForKey:@"branchPhone1"];
            if(telp1.length > 2){
                cell.valueLabel.text = [cell.valueLabel.text stringByReplacingOccurrencesOfString:telp1 withString:[NSString stringWithFormat:@"📞 %@", telp1]];
            }
            telp1 = [self.dataDetail valueForKey:@"branchPhone2"];
            if(telp1.length > 2){
                cell.valueLabel.text = [cell.valueLabel.text stringByReplacingOccurrencesOfString:telp1 withString:[NSString stringWithFormat:@"📞 %@", telp1]];
            }
            
            telp1 = [self.dataDetail valueForKey:@"branchPhone1"];
            if(telp1.length > 2){
                [cell.valueLabel addLinkToURL:[NSURL URLWithString:[NSString stringWithFormat:@"telpon://%@", removeSpace(telp1)]] withRange:[cell.valueLabel.text rangeOfString:telp1]];
                cell.valueLabel.delegate = self;
                cell.valueLabel.userInteractionEnabled = YES;
            }
            telp1 = [self.dataDetail valueForKey:@"branchPhone2"];
            if(telp1.length > 2){
                [cell.valueLabel addLinkToURL:[NSURL URLWithString:[NSString stringWithFormat:@"telpon://%@", removeSpace(telp1)]] withRange:[cell.valueLabel.text rangeOfString:telp1]];
                cell.valueLabel.delegate = self;
                cell.valueLabel.userInteractionEnabled = YES;
            }
        }
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - delegate ASIHTTP
-(void)requestStarted:(ASIHTTPRequest *)request{
    if(!self.loading && !isrefresh){
        self.loading = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.loading.labelText = @"Loading..";
        self.loading.delegate = self;
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request{
    [self countDownRequest];
    
    [self alert:@"Tidak terdapat koneksi internet."];
}

-(void)requestFinished:(ASIHTTPRequest *)request{
    [self countDownRequest];
    NSLog(@"\n==========\n detail %@ : %@\n\n", request.url.absoluteString, request.responseString);
    
    NSDictionary *root = [request.responseString JSONValue];
    
    if (request.tag == 3) {
        btnFav.selected = YES;
        btnFav2.selected = YES;
        self.data[@"isFavorite"] = @1;
        
        return;
        
    } else if (request.tag == 4) {
        btnFav.selected = NO;
        btnFav2.selected = NO;
        self.data[@"isFavorite"] = @0;
        
        return;
    }
    
    self.dataDetail = [root valueForKey:@"data"];
    
    judul.text = [NSString stringWithFormat:@"Kode Agunan: %@", [self.dataDetail valueForKey:@"assetCode"]];
    hargaBaru.text = [self.dataDetail valueForKey:@"newPrice"];
    [hargaBaru sizeToFit];
    
    NSString *text = [self.dataDetail valueForKey:@"oldPrice"];
    hargaLama.superview.hidden = text.length < 3;
    [hargaLama setText:text afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        // Core Text APIs use C functions without a direct bridge to UIFont. See Apple's "Core Text Programming Guide" to learn how to configure string attributes.
        UIFont *boldSystemFont = hargaLama.font;
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        if (font) {
            [mutableAttributedString addAttribute:kTTTStrikeOutAttributeName value:[NSNumber numberWithBool:YES] range:[text rangeOfString:text]];
            CFRelease(font);
        }
        
        return mutableAttributedString;
    }];
    
    hargaBaru.textColor = [UIColor blackColor];
    hotPriceTag.hidden = YES;
    eAuctionTag.hidden = ![self.dataDetail[@"isEmailAuction"] boolValue];
    
    if([[hargaBaru.text lowercaseString] isEqualToString:@"call"]){
        hargaBaru.textColor = [UIColor redColor];
    } else if(text.length > 3){
        hotPriceTag.hidden = NO;
        CGRect frame = hotPriceTag.frame;
        frame.origin.x = hargaBaru.frame.origin.x + hargaBaru.frame.size.width + 10.;
        hotPriceTag.frame = frame;
    }
    
    if (!eAuctionTag.hidden) {
        CGRect frame = eAuctionTag.frame;
        if (hotPriceTag.hidden){
            frame.origin.x = hargaBaru.frame.origin.x + hargaBaru.frame.size.width + 3;
        } else {
            frame.origin.x = hotPriceTag.frame.origin.x + hotPriceTag.frame.size.width + 3;
        }
        eAuctionTag.frame = frame;
    }
    
    NSInteger t = [self.dataDetail[@"type"] integerValue];
    sukarelaTag.hidden = t == 2;
    lelangTag.hidden = t == 1;
    
    hasLocation = [[self.dataDetail valueForKey:@"latitude"] doubleValue] > 0 && [[self.dataDetail valueForKey:@"longitude"] doubleValue] > 0;
    if(hasLocation){
        [maps removeAnnotation:self];
        [maps addAnnotation:self];
        [maps setRegion:MKCoordinateRegionMakeWithDistance([self coordinate], 4000., 4000.)];
    }
    
    [table reloadData];
}

-(void)countDownRequest{
    requestCount--;
    
    if(requestCount < 1){
        [self.loading hide:NO];
        
        if(isrefresh){
            [refreshViewHeader stopLoading];
        }
    }
}

#pragma mark - delegate loading
-(void)hudWasHidden:(MBProgressHUD *)hud{
    self.loading = nil;
}

#pragma mark - delegate scroll
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [refreshViewHeader scrollViewDidScroll:scrollView];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [refreshViewHeader scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}

#pragma mark - delegate refresh pull
-(void)UICellRefreshLoadingDidBeginLoading:(UICellRefreshLoading *)view{
    isrefresh = YES;
    table.contentSize = table.frame.size;
    [self makeRequest];
}

-(void)UICellRefreshLoadingDidFinishLoading:(UICellRefreshLoading *)view{
    [table reloadData];
}

#pragma mark - delegate TTT
-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url{NSLog(@"asdfasdfasdfdsaf %@", url.absoluteString);
    if([[url scheme] isEqualToString:@"telpon"]){NSLog(@"%@", [url host]);
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", [url host]]]];
    }
}

#pragma mark - delegate XHImageViewer
-(void)imageViewer:(XHImageViewer *)imageViewer didDismissWithSelectedView:(UIImageView *)selectedView{
    [imageViewer removeFromSuperview];
    [selectedView removeFromSuperview];
    [imageViewer release];
}

#pragma mark - Annotation
-(CLLocationCoordinate2D)coordinate{
    if(hasLocation){
        return CLLocationCoordinate2DMake([[self.dataDetail valueForKey:@"latitude"] doubleValue], [[self.dataDetail valueForKey:@"longitude"] doubleValue]);
    }
    
    return CLLocationCoordinate2DMake(-6.1775669, 106.8289902);
}

@end
