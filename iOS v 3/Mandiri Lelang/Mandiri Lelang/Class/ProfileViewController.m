//
//  ProfileViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 10/29/16.
//  Copyright © 2016 Asep Mulyana. All rights reserved.
//

#import "ProfileViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "ASIHTTPRequest+Mandiri.h"
#import "AppConfig.h"
#import "JSON.h"
#import "MLDatabase.h"
#import "ComboTableViewController.h"
#import "KuesionerViewController.h"
#import "LoginRegisterViewController.h"
#import "AppDelegate.h"
#import "IIViewDeckController.h"

@interface ProfileViewController () <MBProgressHUDDelegate, ComboTableViewControllerDelegate> {
    NSInteger requestCount;
}

@property (retain, nonatomic) IBOutlet UIView *viewForm;
@property (retain, nonatomic) IBOutlet UIButton *btnCancel;

@property (nonatomic, retain) MBProgressHUD *loading;

@property (retain, nonatomic) IBOutlet UITextField *txtRegNama;
@property (retain, nonatomic) IBOutlet UITextField *txtRegEmail;
@property (retain, nonatomic) IBOutlet UITextField *txtRegTglLahir;
@property (retain, nonatomic) IBOutlet UITextField *txtRegTempLahir;
@property (retain, nonatomic) IBOutlet UITextField *txtRegNoHP;
@property (retain, nonatomic) IBOutlet UITextField *txtRegAlamat;
@property (retain, nonatomic) IBOutlet UITextField *txtRegProvinsi;
@property (retain, nonatomic) IBOutlet UITextField *txtRegKota;
@property (retain, nonatomic) IBOutlet UITextField *txtRegKTP;
@property (retain, nonatomic) IBOutlet UITextField *txtRegPasswordOld;
@property (retain, nonatomic) IBOutlet UITextField *txtRegPassword;
@property (retain, nonatomic) IBOutlet UITextField *txtRegRePassword;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *constRegBottom;
@property (retain, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (retain, nonatomic) IBOutlet NSLayoutConstraint *constContentHeight;

@property (nonatomic, retain) NSMutableArray *propinsiArray;
@property (nonatomic, retain) NSMutableArray *kotaArray;
@property (nonatomic, retain) NSMutableDictionary *data;

@property (retain, nonatomic) IBOutlet UILabel *lblTempatLahir;
@property (retain, nonatomic) IBOutlet UILabel *lblTglLahir;
@property (retain, nonatomic) IBOutlet UILabel *lblAlamat;
@property (retain, nonatomic) IBOutlet UILabel *lblProvince;
@property (retain, nonatomic) IBOutlet UILabel *lblCity;
@property (retain, nonatomic) IBOutlet UILabel *lblKTP;
@property (retain, nonatomic) IBOutlet UILabel *lblPasswordLama;
@property (retain, nonatomic) IBOutlet UILabel *lblHP;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.fromRegister) {
        [_btnCancel setTitle:@"Skip" forState:UIControlStateNormal];
    }
    
    [self setupUI:_viewForm];
    
    _datePicker.maximumDate = [[NSDate date] dateByAddingTimeInterval:-24 * 3600];
    _txtRegTglLahir.inputView = _datePicker;
    
    self.propinsiArray = [MLProvince sharedData].provinces;
    [self.propinsiArray removeObjectAtIndex:0];
    self.kotaArray = [[MLCity sharedData] citiesForProvince:@0];
    [self.kotaArray removeObjectAtIndex:0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if (self.fromRegister) {
        self.constContentHeight.constant = 690;
        _lblPasswordLama.hidden = YES;
        _txtRegPasswordOld.hidden = YES;
    } else {
        self.constContentHeight.constant = 890;
    }
    
    NSArray *settings = [[MLSettings sharedData] settings];
    
    for (MSettings *setting in settings) {
        if ([setting.identifier isEqualToString:@"MandatoryForMemberPlaceOfBirth"] && [setting.name isEqualToString:@"True"]) {
            _lblTempatLahir.text = @"Tempat Lahir *";
        } else if ([setting.identifier isEqualToString:@"MandatoryForMemberMobilePhoneNumber"] && [setting.name isEqualToString:@"True"]) {
            _lblHP.text = @"Nomor Handphone *";
        } else if ([setting.identifier isEqualToString:@"MandatoryForMemberDateOfBirth"] && [setting.name isEqualToString:@"True"]) {
            _lblTglLahir.text = @"Tanggal Lahir *";
        } else if ([setting.identifier isEqualToString:@"MandatoryForMemberAddress"] && [setting.name isEqualToString:@"True"]) {
            _lblAlamat.text = @"Alamat Tempat Tinggal *";
        } else if ([setting.identifier isEqualToString:@"MandatoryForMemberProvince"] && [setting.name isEqualToString:@"True"]) {
            _lblProvince.text = @"Provinsi *";
        } else if ([setting.identifier isEqualToString:@"MandatoryForMemberCity"] && [setting.name isEqualToString:@"True"]) {
            _lblCity.text = @"Kota *";
        } else if ([setting.identifier isEqualToString:@"MandatoryForMemberIdentificationNumber"] && [setting.name isEqualToString:@"True"]) {
            _lblKTP.text = @"Nomor KTP *";
        }
    }
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Actions

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)skip:(id)sender {
    [AppDelegate currentDelegate].window.rootViewController = (UIViewController *)[AppDelegate currentDelegate].mainViewController;
    [self back:nil];
}

- (void)setupUI:(UIView *)view {
    for(UITextField *txt in view.subviews){
        if([txt isKindOfClass:[UITextField class]]){
            //            if(txt.tag > 0){
            //                UIPickerView *picker = [[[UIPickerView alloc] init] autorelease];
            //                picker.backgroundColor = [UIColor lightGrayColor];
            //                picker.delegate = self;
            //                picker.dataSource = self;
            //                picker.showsSelectionIndicator = YES;
            //                picker.tag = txt.tag;
            //                txt.inputView = picker;
            //            }
            
            txt.layer.masksToBounds = YES;
            txt.layer.cornerRadius = 3.;
            txt.layer.borderColor = [UIColor grayColor].CGColor;
            txt.layer.borderWidth = 1.;
            
            UIView *v = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 8., 10)] autorelease];
            v.backgroundColor = [UIColor clearColor];
            txt.leftView = v;
            txt.leftViewMode = UITextFieldViewModeAlways;
        }//else if([txt isKindOfClass:[UIButton class]]){
        //            UIButton *btn = (UIButton *)txt;
        //            btn.layer.masksToBounds = YES;
        //            btn.layer.cornerRadius = 3.;
        //            [btn setBackgroundImage:[Tools solidImageForColor:btn.backgroundColor withSize:btn.frame.size] forState:UIControlStateNormal];
        //        }
    }
}

- (void)loadData {
    ASIHTTPRequest *r = [ASIHTTPRequest requestAuthWithURL:[NSURL URLWithString:kURLProfile]];
    r.delegate = self;
    r.tag = 1;
    [r startAsynchronous];
}

-(void)alert:(NSString *)msg{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [alert release];
}

- (BOOL)isRegFormValid {
    if (_txtRegNama.text.length < 1){
        [self alert:@"Mohon isi Nama Lengkap"];
        return NO;
    }
    if (_txtRegEmail.text.length < 1){
        [self alert:@"Mohon isi Email"];
        return NO;
    }
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    if(![predicate evaluateWithObject:_txtRegEmail.text]) {
        [self alert:@"Mohon isi email yang valid"];
        return NO;
    }
    
    NSString *alphabetRegex = @"[A-Za-z ]+";
    NSPredicate *alphabetPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", alphabetRegex];
    
    if(![alphabetPredicate evaluateWithObject:_txtRegTempLahir.text] && _txtRegTempLahir.text.length > 0) {
        [self alert:@"Mohon isi tempat lahir yang valid (hanya terdiri dari huruf dan spasi)"];
        return NO;
    }
    
    MSettings *setting = [[MLSettings sharedData] settingsWithID:@"MandatoryForMemberPlaceOfBirth"];
    if ([setting.name isEqualToString:@"True"] && _txtRegTempLahir.text.length < 1) {
        [self alert:@"Mohon isi Tempat Lahir"];
        return NO;
    }
    
    setting = [[MLSettings sharedData] settingsWithID:@"MandatoryForMemberDateOfBirth"];
    if ([setting.name isEqualToString:@"True"] && _txtRegTglLahir.text.length < 1) {
        [self alert:@"Mohon isi Tanggal Lahir"];
        return NO;
    }
    
    setting = [[MLSettings sharedData] settingsWithID:@"MandatoryForMemberMobilePhoneNumber"];
    if ([setting.name isEqualToString:@"True"] && _txtRegNoHP.text.length < 1) {
        [self alert:@"Mohon isi Nomor Handphone"];
        return NO;
    }
    
    setting = [[MLSettings sharedData] settingsWithID:@"MandatoryForMemberAddress"];
    if ([setting.name isEqualToString:@"True"] && _txtRegAlamat.text.length < 1) {
        [self alert:@"Mohon isi Alamat Tempat Tinggal"];
        return NO;
    }
    
    setting = [[MLSettings sharedData] settingsWithID:@"MandatoryForMemberProvince"];
    if ([setting.name isEqualToString:@"True"] && _txtRegProvinsi.text.length < 1) {
        [self alert:@"Mohon isi Provinsi"];
        return NO;
    }
    
    setting = [[MLSettings sharedData] settingsWithID:@"MandatoryForMemberCity"];
    if ([setting.name isEqualToString:@"True"] && _txtRegKota.text.length < 1) {
        [self alert:@"Mohon isi Provinsi dan Kota"];
        return NO;
    }
    
    setting = [[MLSettings sharedData] settingsWithID:@"MandatoryForMemberIdentificationNumber"];
    if ([setting.name isEqualToString:@"True"] && _txtRegKTP.text.length < 1) {
        [self alert:@"Mohon isi Nomor KTP"];
        return NO;
    }
    
    if (self.fromRegister){
        return YES;
    }
    
    if (self.fromMenu && _txtRegPassword.text.length < 1 && _txtRegRePassword.text.length < 1 && _txtRegPasswordOld.text.length < 1){
        return YES;
    }
    
    if (_txtRegPasswordOld.text.length < 1){
        [self alert:@"Mohon isi Password Lama"];
        return NO;
    }
    
    if (_txtRegPassword.text.length < 1){
        [self alert:@"Mohon isi Password Baru"];
        return NO;
    }
    
    NSRange lowerRange = [_txtRegPassword.text rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    NSRange upperRange = [_txtRegPassword.text rangeOfCharacterFromSet:[NSCharacterSet uppercaseLetterCharacterSet]];
    NSRange numberRange = [_txtRegPassword.text rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]];
    NSRange simbolRange = [_txtRegPassword.text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"]];
    
    if (_txtRegPassword.text.length < 8 || lowerRange.length < 1 || upperRange.length < 1 || numberRange.length < 1 || simbolRange.length < 1) {
        NSLog(@"lower=%d upper=%d number=%d simbol=%d", lowerRange.length < 1, upperRange.length < 1, numberRange.length < 1, simbolRange.length < 1);
        [self alert:@"Password Baru harus terdiri dari minimum 8 karakter, mengandung huruf kecil, huruf kapital, angka dan simbol"];
        return NO;
    }
    
    if (![_txtRegRePassword.text isEqualToString:_txtRegPassword.text]){
        [self alert:@"Konfirmasi Password Baru harus sama dengan Password Baru"];
        return NO;
    }
    
    return YES;
}

- (IBAction)next:(id)sender {
    if (![self isRegFormValid]) {
        return;
    }
    
    [self hideKeyboard:nil];
    
    NSString *tgl = @"";
    
    if (_txtRegTglLahir.text.length > 0) {
        NSDateFormatter *f = [[[NSDateFormatter alloc] init] autorelease];
        f.dateFormat = @"dd MMMM yyyy";
        NSDate *date = [f dateFromString:_txtRegTglLahir.text];
        f.dateFormat = @"yyyy-MM-dd";
        tgl = [f stringFromDate:date];
    }
    
    
    NSNumber *pid = _txtRegProvinsi.tag < 0 ? @0 : [[self.propinsiArray objectAtIndex:_txtRegProvinsi.tag] valueForKey:@"identifier"];
    NSNumber *kid = _txtRegKota.tag < 0 ? @0 : [[self.kotaArray objectAtIndex:_txtRegKota.tag] valueForKey:@"identifier"];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSMutableDictionary *profile = [NSMutableDictionary dictionary];
    profile[@"fullName"] = _txtRegNama.text;
    profile[@"email"] = _txtRegEmail.text;
    profile[@"placeOfBirth"] = _txtRegTempLahir.text;
    profile[@"dateOfBirth"] = tgl;
    profile[@"mobilePhoneNumber"] = _txtRegNoHP.text;
    profile[@"address"] = _txtRegAlamat.text;
    profile[@"provinceID"] = pid.integerValue < 1 ? [NSNull null] : pid;
    profile[@"cityID"] = kid.integerValue < 1 ? [NSNull null] : kid;
    profile[@"identificationNumber"] = _txtRegKTP.text;
    profile[@"password"] = _txtRegPassword.text;
//    profile[@"old_password"] = _txtRegPasswordOld.text;
    profile[@"oldPassword"] = _txtRegPasswordOld.text;
    [dict setValue:profile forKey:@"profile"];
    
    KuesionerViewController *kuis = [[[KuesionerViewController alloc] initWithNibName:@"KuesionerViewController" bundle:nil] autorelease];
    kuis.data = dict;
    kuis.fromMenu = self.fromMenu;
    kuis.defaultPreference = self.data[@"preference"];
    [self.navigationController pushViewController:kuis animated:YES];
}

- (IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

-(IBAction)dateChanged:(UIDatePicker *)sender{
    NSDateFormatter *format = [[[NSDateFormatter alloc] init] autorelease];
    [format setDateFormat:@"dd MMMM yyyy"];
    _txtRegTglLahir.text = [format stringFromDate:sender.date];
}

-(IBAction)showCombo:(UIButton *)sender{
    ComboTableViewController *combo = [[ComboTableViewController alloc] initWithNibName:@"ComboTableViewController" bundle:nil];
    combo.tag = sender.tag;
    combo.delegate = self;
    
    if(sender.tag == 0){
        combo.data = self.propinsiArray;
        combo.selectedRow = _txtRegProvinsi.tag;
    }else if(sender.tag == 1){
        combo.data = self.kotaArray;
        combo.selectedRow = _txtRegKota.tag;
    }
    
    [self.navigationController pushViewController:combo animated:YES];
    [combo release];
}

- (void)updateUI {
    NSDictionary *profile = self.data[@"profile"];
    _txtRegNama.text = [profile valueForKey:@"fullName"];
    _txtRegEmail.text = [profile valueForKey:@"email"];
    _txtRegTempLahir.text = [profile valueForKey:@"placeOfBirth"];
    _txtRegNoHP.text = [profile valueForKey:@"mobilePhoneNumber"];
    _txtRegAlamat.text = [profile valueForKey:@"address"];
    _txtRegKTP.text = [profile valueForKey:@"identificationNumber"];
    
    NSString *dt = profile[@"dateOfBirth"];
    NSDateFormatter *format = [[[NSDateFormatter alloc] init] autorelease];
    [format setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [format dateFromString:dt];
    
    if (date) {
        [format setDateFormat:@"dd MMMM yyyy"];
        _txtRegTglLahir.text = [format stringFromDate:date];
        _datePicker.date = date;
    }
    
    NSInteger prov = [profile[@"provinceID"] integerValue];
    NSInteger city = [profile[@"cityID"] integerValue];
    
    for (MProvince *p in self.propinsiArray) {
        if ([p.identifier integerValue] == prov) {
            NSInteger idx = [self.propinsiArray indexOfObject:p];
            _txtRegProvinsi.tag = idx;
            _txtRegProvinsi.text = p.name;
            
            self.kotaArray = [[MLCity sharedData] citiesForProvince:p.identifier];
            [self.kotaArray removeObjectAtIndex:0];
            
            for (MCity *c in self.kotaArray) {
                if ([c.identifier integerValue] == city) {
                    NSInteger idx = [self.kotaArray indexOfObject:c];
                    _txtRegKota.tag = idx;
                    _txtRegKota.text = c.name;
                }
            }
        }
    }
}

#pragma mark - delegate combo
-(void)combo:(ComboTableViewController *)controller didSelectItemAtIndex:(NSInteger)index{
    if(controller.tag == 0){
        _txtRegProvinsi.text = [[self.propinsiArray objectAtIndex:index] valueForKey:@"name"];
        _txtRegProvinsi.tag = index;
        
        NSNumber *pid = [[self.propinsiArray objectAtIndex:index] valueForKey:@"identifier"];
        self.kotaArray = [[MLCity sharedData] citiesForProvince:pid];
        [self.kotaArray removeObjectAtIndex:0];
        
        _txtRegKota.tag = -1;
        _txtRegKota.text = @"";
    }else if(controller.tag == 1){
        _txtRegKota.text = [[self.kotaArray objectAtIndex:index] valueForKey:@"name"];
        _txtRegKota.tag = index;
    }
}

- (void)dealloc {
    [_viewForm release];
    [_btnCancel release];
    [_txtRegNama release];
    [_txtRegEmail release];
    [_txtRegTglLahir release];
    [_txtRegTempLahir release];
    [_txtRegNoHP release];
    [_txtRegAlamat release];
    [_txtRegProvinsi release];
    [_txtRegKota release];
    [_txtRegKTP release];
    [_txtRegRePassword release];
    [_txtRegPassword release];
    [_constRegBottom release];
    [_datePicker release];
    [_data release];
    
    [_propinsiArray release];
    [_kotaArray release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [_constContentHeight release];
    [_txtRegPasswordOld release];
    
    [_lblTempatLahir release];
    [_lblTglLahir release];
    [_lblAlamat release];
    [_lblProvince release];
    [_lblCity release];
    [_lblKTP release];
    
    [_lblPasswordLama release];
    [_lblHP release];
    [super dealloc];
}

#pragma mark - delegate ASIHTTP
-(void)requestStarted:(ASIHTTPRequest *)request{
    [self showLoading];
}

- (void)showLoading{
    requestCount++;
    if(!self.loading){
        self.loading = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.loading.labelText = @"Loading..";
        self.loading.delegate = self;
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request{
    [self countDownRequest];
    
    [self alert:@"Tidak terdapat koneksi internet."];
}

-(void)requestFinished:(ASIHTTPRequest *)request{
    [self countDownRequest];
    NSLog(@"\n==========\n profile %@ : %@\n\n", request.url.absoluteString, request.responseString);
    
    if (request.tag == 1) {
        // {"data":{},"resultCode":{"code":1,"message":"...bla bla TOKEN bla bla......"}}
        NSDictionary *dict = [request.responseString JSONValue];
        if ([[[dict valueForKeyPath:@"resultCode.message"] uppercaseString] rangeOfString:@"TOKEN"].length > 4) {
            [self alert:@"Sesi login anda sudah habis, mohon melakukan login ulang"];
            [AppConfig sharedConfig].sessionID = nil;
            
            LoginRegisterViewController *profile = [[[LoginRegisterViewController alloc] initWithNibName:@"LoginRegisterViewController" bundle:nil] autorelease];
            UINavigationController *nav = (UINavigationController *)[AppDelegate currentDelegate].mainViewController.centerController;
            UINavigationController *navP = [[[UINavigationController alloc] initWithRootViewController:profile] autorelease];
            navP.navigationBarHidden = YES;
            [nav presentViewController:navP animated:YES completion:nil];
            
            return;
        }
        
        self.data = [dict valueForKey:@"data"];
        [self updateUI];
    }
}

-(void)countDownRequest{
    requestCount--;
    
    if(requestCount < 1){
        [self.loading hide:NO];
    }
}

#pragma mark - delegate loading
-(void)hudWasHidden:(MBProgressHUD *)hud{
    self.loading = nil;
}


#pragma mark - Observer keyboard
-(void)keyboardDidHide:(NSNotification *)sender{
    _constRegBottom.constant = 0;
}

-(void)keyboardDidShow:(NSNotification *)sender{
    CGRect keyboardRect = [[sender.userInfo valueForKeyPath:@"UIKeyboardBoundsUserInfoKey"] CGRectValue];
    
    _constRegBottom.constant = keyboardRect.size.height - 42;
}

@end
