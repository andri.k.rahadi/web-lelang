//
//  SearchViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/24/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "SearchViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Tools.h"
#import "AppDelegate.h"
#import "IIViewDeckController.h"
#import "SearchResultViewController.h"
#import "ComboTableViewController.h"
#import "MLDatabase.h"
#import "AppConfig.h"

@interface SearchViewController ()<ComboTableViewControllerDelegate>{
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *subView;
    IBOutlet UITextField *kategori;
    IBOutlet UITextField *lokasi;
    IBOutlet UITextField *harga;
    IBOutlet UIPickerView *kategoriPicker;
    IBOutlet UIPickerView *lokasiPicker;
    IBOutlet UIPickerView *hargaPicker;
    IBOutlet UIButton *searchButton;
    IBOutlet UIButton *btnSukarela;
    IBOutlet UIButton *btnLelang;
}

-(IBAction)hideKeyboard:(id)sender;
-(IBAction)openMenu:(id)sender;
-(IBAction)search:(id)sender;
-(IBAction)showCombo:(id)sender;

@property (nonatomic, retain) NSArray *kategoriData;
@property (nonatomic, retain) NSArray *lokasiData;
@property (nonatomic, retain) NSArray *hargaData;
@property (retain, nonatomic) IBOutlet UITextField *txtKeyword;

@end

@implementation SearchViewController

@synthesize lokasiData, kategoriData, hargaData;

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    self.lokasiData = nil;
    self.hargaData = nil;
    self.kategoriData = nil;
    
    [_txtKeyword release];
    [btnSukarela release];
    [btnLelang release];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(masterdataUpdated:) name:MandiriLelangMasterDataDidUpdatedNotification object:nil];
    
    scrollView.contentSize = subView.frame.size;
    
    self.kategoriData = [MLCategory sharedData].categories;
	self.lokasiData = [MLProvince sharedData].provinces;
	self.hargaData = [MLPrice sharedData].prices;
    
    lokasi.inputView = lokasiPicker;
    harga.inputView = hargaPicker;
    kategori.inputView = kategoriPicker;
    
    for(UITextField *txt in subView.subviews){
        if([txt isKindOfClass:[UITextField class]]){
            txt.layer.masksToBounds = YES;
            txt.layer.cornerRadius = 3.;
            txt.layer.borderColor = [UIColor colorWithWhite:0.7 alpha:0.7].CGColor;
            txt.layer.borderWidth = 1.;
            txt.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.2];
            
            UIView *v = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 8., 10)] autorelease];
            v.backgroundColor = [UIColor clearColor];
            txt.leftView = v;
            txt.leftViewMode = UITextFieldViewModeAlways;
        }
    }
    
    searchButton.layer.masksToBounds = YES;
    searchButton.layer.cornerRadius = 3.;
    [searchButton setBackgroundImage:[Tools solidImageForColor:searchButton.backgroundColor withSize:searchButton.frame.size] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

#pragma mark - Actions
-(void)openMenu:(id)sender{
    if([AppDelegate currentDelegate].mainViewController.leftControllerIsClosed)
        [[AppDelegate currentDelegate] openMenu];
    else
        [[AppDelegate currentDelegate] closeMenu];
}

-(void)hideKeyboard:(id)sender{
    [kategori resignFirstResponder];
    [lokasi resignFirstResponder];
    [harga resignFirstResponder];
    [_txtKeyword resignFirstResponder];
}

-(void)search:(id)sender{
    SearchResultViewController *search = [[[SearchResultViewController alloc] initWithNibName:@"SearchResultViewController" bundle:nil] autorelease];
    
    NSNumber *cat = [[self.kategoriData objectAtIndex:kategori.tag] valueForKey:@"identifier"];
    NSNumber *city = [[self.lokasiData objectAtIndex:lokasi.tag] valueForKey:@"identifier"];
    NSNumber *price = [[self.hargaData objectAtIndex:harga.tag] valueForKey:@"identifier"];
    
    search.params = [NSMutableDictionary dictionaryWithObjectsAndKeys:cat, @"categoryID", city, @"provinceID", price, @"maxPriceID", _txtKeyword.text, @"keyword", @(btnSukarela.selected), @"type1", @(btnLelang.selected), @"type2", nil];
    search.enableFilter = YES;
    [self.navigationController pushViewController:search animated:YES];
}

- (IBAction)toggleMe:(UIButton *)sender {
    sender.selected = !sender.selected;
}

-(void)showCombo:(UIButton *)sender{
    ComboTableViewController *combo = [[ComboTableViewController alloc] initWithNibName:@"ComboTableViewController" bundle:nil];
    combo.tag = sender.tag;
    combo.delegate = self;
    
    if(sender.tag == 0){
        combo.data = self.kategoriData;
        combo.selectedRow = kategori.tag;
    }else if(sender.tag == 1){
        combo.data = self.lokasiData;
        combo.selectedRow = lokasi.tag;
    }else if(sender.tag == 2){
        combo.data = self.hargaData;
        combo.selectedRow = harga.tag;
    }
    
    [self.navigationController pushViewController:combo animated:YES];
    [combo release];
}

#pragma mark - delegate dan datasource picker
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView == hargaPicker)return self.hargaData.count;
    else if(pickerView == kategoriPicker)return self.kategoriData.count;
    
    return self.lokasiData.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView == lokasiPicker){
        return [self.lokasiData objectAtIndex:row];
    }else if(pickerView == hargaPicker){
        return [self.hargaData objectAtIndex:row];
    }else{
        return [self.kategoriData objectAtIndex:row];
    }
    
    return @"";
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView == lokasiPicker){
        lokasi.text = [self.lokasiData objectAtIndex:row];
    }else if(pickerView == hargaPicker){
        harga.text = [self.hargaData objectAtIndex:row];
    }else{
        kategori.text = [self.kategoriData objectAtIndex:row];
    }
}

#pragma mark - Observer keyboard
-(void)keyboardDidHide:(NSNotification *)sender{
    CGRect frame = scrollView.frame;
    frame.size.height = self.view.frame.size.height - frame.origin.y + 60;
    scrollView.frame = frame;
}

-(void)keyboardDidShow:(NSNotification *)sender{
    CGRect keyboardRect = [[sender.userInfo valueForKeyPath:@"UIKeyboardBoundsUserInfoKey"] CGRectValue];
    
    CGRect frame = scrollView.frame;
    frame.size.height = self.view.frame.size.height - frame.origin.y - keyboardRect.size.height + 60;
    scrollView.frame = frame;
}

#pragma mark - delegate combo
-(void)combo:(ComboTableViewController *)controller didSelectItemAtIndex:(NSInteger)index{
    if(controller.tag == 0){
        kategori.text = [[self.kategoriData objectAtIndex:index] valueForKey:@"name"];
        kategori.tag = index;
    }else if(controller.tag == 1){
        lokasi.text = [[self.lokasiData objectAtIndex:index] valueForKey:@"name"];
        lokasi.tag = index;
    }else if(controller.tag == 2){
        harga.text = [[self.hargaData objectAtIndex:index] valueForKey:@"name"];
        harga.tag = index;
    }
}

#pragma mark - masterdata observer
-(void)masterdataUpdated:(NSNotification *)sender{
    self.kategoriData = [MLCategory sharedData].categories;
	self.lokasiData = [MLCity sharedData].cities;
	self.hargaData = [MLPrice sharedData].prices;
}

@end
