//
//  MapsViewController.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 9/11/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapsViewController : UIViewController

@property (nonatomic) CLLocationCoordinate2D coordinate;

@end
