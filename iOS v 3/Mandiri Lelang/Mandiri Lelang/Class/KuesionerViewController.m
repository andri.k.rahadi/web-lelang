//
//  KuesionerViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 10/29/16.
//  Copyright © 2016 Asep Mulyana. All rights reserved.
//

#import "KuesionerViewController.h"
#import "MLDatabase.h"
#import "ComboTableViewController.h"
#import "MBProgressHUD.h"
#import "ASIHTTPRequest+Mandiri.h"
#import "AppConfig.h"
#import "JSON.h"
#import "AppDelegate.h"
#import "LoginRegisterViewController.h"
#import "IIViewDeckController.h"

@interface KuesionerViewController () <ComboTableViewControllerDelegate, MBProgressHUDDelegate>{
    NSInteger requestCount;
}

@property (retain, nonatomic) IBOutlet UIView *viewForm;
@property (retain, nonatomic) IBOutlet UITextField *txtCategory;
@property (retain, nonatomic) IBOutlet UITextField *txtProvinsi;
@property (retain, nonatomic) IBOutlet UITextField *txtKota;
@property (retain, nonatomic) IBOutlet UITextField *txtMinPrice;
@property (retain, nonatomic) IBOutlet UITextField *txtMaxPrice;
@property (retain, nonatomic) IBOutlet UILabel *lblInfo;
@property (retain, nonatomic) IBOutlet UIButton *btnConfirm;
@property (retain, nonatomic) IBOutlet UIButton *btnDaftar;
@property (retain, nonatomic) IBOutlet UIButton *btnSimpan;
@property (retain, nonatomic) IBOutlet UIButton *btnSkip;

@property (nonatomic, retain) NSMutableArray *kategoriArray;
@property (nonatomic, retain) NSMutableArray *propinsiArray;
@property (nonatomic, retain) NSMutableArray *kotaArray;
@property (nonatomic, retain) NSMutableArray *hargaMinarray;
@property (nonatomic, retain) NSMutableArray *hargaMaxArray;

@property (nonatomic, retain) NSMutableArray *selectedProvinsi;
@property (nonatomic, retain) NSMutableArray *selectedKota;
@property (nonatomic, retain) NSMutableArray *selectedKategori;

@property (nonatomic, retain) MBProgressHUD *loading;

@end

@implementation KuesionerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    MWebContent *content = [[MLWebContent sharedData] webcontentWithID:10];
    _lblInfo.text = content.name;
    
    self.kategoriArray = [MLCategory sharedData].categories;
    self.propinsiArray = [MLProvince sharedData].provinces;
    self.kotaArray = [[MLCity sharedData] citiesForProvince:@0];
    self.hargaMinarray = [MLPrice sharedData].prices;
    self.hargaMaxArray = [MLPrice sharedData].prices;
    
    [self.kategoriArray removeObjectAtIndex:0];
    [self.propinsiArray removeObjectAtIndex:0];
    [self.kotaArray removeObjectAtIndex:0];
    [self.hargaMinarray removeObjectAtIndex:0];
    [self.hargaMaxArray removeObjectAtIndex:0];
    
    [self setupUI:_viewForm];
    
    if (self.fromRegister) {
        self.btnDaftar.hidden = NO;
        self.btnSimpan.hidden = YES;
        self.btnSkip.hidden = YES;
    } else {
        self.btnDaftar.hidden = YES;
        self.btnSimpan.hidden = NO;
        self.btnSkip.hidden = NO;
    }
    
    if (self.fromMenu) {
        [_btnSkip setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    
    [self loadData];
    NSLog(@"Masa sih bisa %@", self.data);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setupUI:(UIView *)view {
    for(UITextField *txt in view.subviews){
        if([txt isKindOfClass:[UITextField class]]){
            //            if(txt.tag > 0){
            //                UIPickerView *picker = [[[UIPickerView alloc] init] autorelease];
            //                picker.backgroundColor = [UIColor lightGrayColor];
            //                picker.delegate = self;
            //                picker.dataSource = self;
            //                picker.showsSelectionIndicator = YES;
            //                picker.tag = txt.tag;
            //                txt.inputView = picker;
            //            }
            
            txt.layer.masksToBounds = YES;
            txt.layer.cornerRadius = 3.;
            txt.layer.borderColor = [UIColor grayColor].CGColor;
            txt.layer.borderWidth = 1.;
            
            UIView *v = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 8., 10)] autorelease];
            v.backgroundColor = [UIColor clearColor];
            txt.leftView = v;
            txt.leftViewMode = UITextFieldViewModeAlways;
        }//else if([txt isKindOfClass:[UIButton class]]){
        //            UIButton *btn = (UIButton *)txt;
        //            btn.layer.masksToBounds = YES;
        //            btn.layer.cornerRadius = 3.;
        //            [btn setBackgroundImage:[Tools solidImageForColor:btn.backgroundColor withSize:btn.frame.size] forState:UIControlStateNormal];
        //        }
    }
}

- (void)loadData {
    self.selectedKategori = [NSMutableArray array];
    self.selectedKota = [NSMutableArray array];
    
    if (self.defaultPreference) {
        if (![self.defaultPreference[@"minPriceID"] isKindOfClass:[NSNull class]] && self.defaultPreference[@"minPriceID"] != nil) {
            NSInteger idMinPrice = [self.defaultPreference[@"minPriceID"] integerValue];
            for (MPrice *p in self.hargaMinarray) {
                if (p.identifier.integerValue == idMinPrice) {
                    _txtMinPrice.text = p.name;
                    _txtMinPrice.tag = [self.hargaMinarray indexOfObject:p];
                    NSLog(@"dapat tag %zd", _txtMinPrice.tag);
                }
            }
        }
        
        if (![self.defaultPreference[@"maxPriceID"] isKindOfClass:[NSNull class]] && self.defaultPreference[@"maxPriceID"] != nil) {
            NSInteger idMaxPrice = [self.defaultPreference[@"maxPriceID"] integerValue];
            for (MPrice *p in self.hargaMaxArray) {
                if (p.identifier.integerValue == idMaxPrice) {
                    _txtMaxPrice.text = p.name;
                    _txtMaxPrice.tag = [self.hargaMaxArray indexOfObject:p];
                    NSLog(@"dapat tag %zd", _txtMinPrice.tag);
                }
            }
        }
        
        NSMutableArray *catIDs = self.defaultPreference[@"categoryIDs"];
        NSMutableArray *katNames = [NSMutableArray array];
        
        self.selectedKategori = catIDs;
        for (NSNumber *num in catIDs) {
            MCategory *cat = [[MLCategory sharedData] categoryWithID:num.integerValue];
            [katNames addObject:cat.name];
        }
        _txtCategory.text = [katNames componentsJoinedByString:@", "];
        
        NSMutableArray *cityIDs = self.defaultPreference[@"cityIDs"];
        NSMutableArray *kotaNames = [NSMutableArray array];
        NSMutableArray *provIDs = [NSMutableArray array];
        self.selectedKota = cityIDs;
        for (NSNumber *num in cityIDs) {
            MCity *ct = [[MLCity sharedData] cityForID:num];
            [kotaNames addObject:ct.name];
            [provIDs addObject:ct.provinceID];
        }
        NSLog(@"Dipilih Kota %zd", cityIDs.count);
        _txtKota.text = [kotaNames componentsJoinedByString:@", "];
        
        NSMutableArray *kotas = [NSMutableArray array];
        NSMutableArray *provNames = [NSMutableArray array];
        self.selectedProvinsi = [self removeDuplicates:provIDs];
        for (NSNumber *num in self.selectedProvinsi) {NSLog(@"Provinsi %@", num);
            MProvince *ct = [[MLProvince sharedData] provinceWithID:num.integerValue];
            [provNames addObject:ct.name];
            
            NSMutableArray *arr = [[MLCity sharedData] citiesForProvince:ct.identifier];
            [arr removeObjectAtIndex:0];
            [kotas addObjectsFromArray:arr];
        }
        _txtProvinsi.text = [provNames componentsJoinedByString:@", "];
        
        [kotas sortUsingComparator:^NSComparisonResult(MCity *obj1, MCity *obj2) {
            return [obj1.name compare:obj2.name];
        }];
        
        self.kotaArray = kotas;
    }
}

- (NSMutableArray *)removeDuplicates:(NSMutableArray *)arr {
    [arr sortUsingComparator:^NSComparisonResult(NSNumber * obj1, NSNumber * obj2) {
        return obj1.integerValue < obj2.integerValue;
    }];
    
    NSMutableArray *baru = [NSMutableArray array];
    
    NSInteger last = -1;
    for (NSNumber *n in arr) {
        if (n.integerValue != last){
            [baru addObject:n];
        }
        last = n.integerValue;
    }
    
    return baru;
}

-(IBAction)showCombo:(UIButton *)sender{
    ComboTableViewController *combo = [[ComboTableViewController alloc] initWithNibName:@"ComboTableViewController" bundle:nil];
    combo.tag = sender.tag;
    combo.delegate = self;
    
    if(sender.tag == 0){
        combo.data = self.kategoriArray;
        combo.multipleSelect = YES;
        combo.withSemua = YES;
        combo.selectedRows = self.selectedKategori;
    }else if(sender.tag == 1){
        combo.data = self.propinsiArray;
        combo.multipleSelect = YES;
        combo.withSemua = YES;
        combo.selectedRows = self.selectedProvinsi;
    }else if(sender.tag == 2){
        combo.data = self.kotaArray;
        combo.multipleSelect = YES;
        combo.withSemua = YES;
        combo.selectedRows = self.selectedKota;
    }else if(sender.tag == 3){
        combo.data = self.hargaMinarray;
        combo.selectedRow = _txtMinPrice.tag;
    }else if(sender.tag == 4){
        combo.data = self.hargaMaxArray;
        combo.selectedRow = _txtMaxPrice.tag;
    }
    
    [self.navigationController pushViewController:combo animated:YES];
    [combo release];
}

- (IBAction)toggle:(UIButton *)sender {
    _btnConfirm.selected = !_btnConfirm.selected;
}

-(void)alert:(NSString *)msg{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [alert release];
}

- (BOOL)isRegFormValid {
    MSettings *setting = [[MLSettings sharedData] settingsWithID:@"MandatoryForMemberPreference"];
    
    if ([setting.name isEqualToString:@"True"]) {
        if (_txtCategory.text.length < 1){
            [self alert:@"Mohon pilih Kategori agunan yang anda minati"];
            return NO;
        }
        if (_txtProvinsi.text.length < 1){
            [self alert:@"Mohon pilih Provinsi"];
            return NO;
        }
        if (_txtKota.text.length < 1){
            [self alert:@"Mohon pilih Kota"];
            return NO;
        }
        if (_txtMinPrice.text.length < 1){
            [self alert:@"Mohon pilih Harga Minimal"];
            return NO;
        }
        if (_txtMaxPrice.text.length < 1){
            [self alert:@"Mohon pilih Harga Maksimal"];
            return NO;
        }
    }
    
    
    if (_txtMinPrice.text.length > 1 && _txtMaxPrice.text.length > 1){
        MPrice *min = [self.hargaMinarray objectAtIndex:_txtMinPrice.tag];
        MPrice *max = [self.hargaMaxArray objectAtIndex:_txtMaxPrice.tag];
        
        if ([max.value longLongValue] < [min.value longLongValue]) {
            [self alert:@"Harga maksimum harus lebih besar dari harga minimum"];
            return NO;
        }
    }
    
    if (!_btnConfirm.selected) {
        [self alert:@"Anda harus menyetujui syarat & ketentuan"];
        return NO;
    }
    
    return YES;
}

- (IBAction)skip:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    [AppDelegate currentDelegate].window.rootViewController = (UIViewController *)[AppDelegate currentDelegate].mainViewController;
}

- (IBAction)daftar:(UIButton *)sender {
    if (![self isRegFormValid]) {
        return;
    }
    
    NSMutableDictionary *pref = [NSMutableDictionary dictionary];
    
    if (_txtMinPrice.tag >= 0) {
        [pref setValue:[[self.hargaMinarray objectAtIndex:_txtMinPrice.tag] valueForKey:@"identifier"] forKey:@"minPriceID"];
    } else {
        [pref setValue:[NSNull null] forKey:@"minPriceID"];
    }
    
    if (_txtMaxPrice.tag >= 0) {
        [pref setValue:[[self.hargaMaxArray objectAtIndex:_txtMaxPrice.tag] valueForKey:@"identifier"] forKey:@"maxPriceID"];
    } else {
        [pref setValue:[NSNull null] forKey:@"maxPriceID"];
    }
    
    [pref setValue:self.selectedKategori forKey:@"categoryIDs"];
    [pref setValue:self.selectedKota forKey:@"cityIDs"];
    
    [self.data setValue:pref forKey:@"preference"];
    NSString *params = [self.data JSONRepresentation];
    
    NSLog(@"Ini params %@", params);
    
    NSString *url = sender.tag == 1 ? kURLUpdateProfile : kURLRegister ;
    ASIFormDataRequest *request = [ASIFormDataRequest requestAuthWithURL:[NSURL URLWithString:url]];
    [request encryptParams];
    request.delegate = self;
    request.tag = sender.tag;
    
//    request.allowCompressedResponse = NO;
//    request.useCookiePersistence = NO;
//    request.shouldCompressRequestBody = NO;
    [request setPostBody:[NSMutableData dataWithData:[params dataUsingEncoding:NSUTF8StringEncoding]]];
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request encryptParams];
    NSLog(@"Header saya %@", request.requestHeaders);
    [request startAsynchronous];
}


#pragma mark - delegate combo
-(void)combo:(ComboTableViewController *)controller didSelectItemAtIndex:(NSInteger)index{
    if(controller.tag == 3){
        MPrice *price = [self.hargaMinarray objectAtIndex:index];
        _txtMinPrice.text = [price valueForKey:@"name"];
        _txtMinPrice.tag = index;
        self.hargaMaxArray = [[MLPrice sharedData] pricesBeginFromID:price.identifier];
        
        _txtMaxPrice.text = @"";
        _txtMaxPrice.tag = 0;
    }else if(controller.tag == 4){
        _txtMaxPrice.text = [[self.hargaMaxArray objectAtIndex:index] valueForKey:@"name"];
        _txtMaxPrice.tag = index;
    }
}

- (void)combo:(ComboTableViewController *)controller didSelectItemAtIndexes:(NSMutableArray *)indexes{
    if(controller.tag == 0){
        NSMutableArray *katNames = [NSMutableArray array];
        self.selectedKategori = [NSMutableArray array];
        for (NSNumber *num in indexes) {
            MCategory *cat = self.kategoriArray[num.integerValue];
            [katNames addObject:cat.name];
            [self.selectedKategori addObject:cat.identifier];
        }
        
        _txtCategory.text = [katNames componentsJoinedByString:@", "];
    } else if(controller.tag == 1){
        NSMutableArray *propNames = [NSMutableArray array];
        self.selectedProvinsi = [NSMutableArray array];
        for (NSNumber *num in indexes) {
            MProvince *prov = self.propinsiArray[num.integerValue];
            [propNames addObject:prov.name];
            [self.selectedProvinsi addObject:prov.identifier];
        }
        
        _txtProvinsi.text = [propNames componentsJoinedByString:@", "];
        
        _txtKota.text = @"";
        self.selectedKota = [NSMutableArray array];
        
        NSMutableArray *kotas = [NSMutableArray array];
        for (NSNumber *provID in self.selectedProvinsi) {
            NSMutableArray *arr = [[MLCity sharedData] citiesForProvince:provID];
            [arr removeObjectAtIndex:0];
            [kotas addObjectsFromArray:arr];
        }
        
        [kotas sortUsingComparator:^NSComparisonResult(MCity *obj1, MCity *obj2) {
            return [obj1.name compare:obj2.name];
        }];
        
        self.kotaArray = kotas;
        
    } else if(controller.tag == 2){
        NSMutableArray *kotaNames = [NSMutableArray array];
        self.selectedKota = [NSMutableArray array];
        for (NSNumber *num in indexes) {
            MCity *ct = self.kotaArray[num.integerValue];
            [kotaNames addObject:ct.name];
            [self.selectedKota addObject:ct.identifier];
        }
        NSLog(@"Dipilih Kota %zd", self.selectedKota.count);
        _txtKota.text = [kotaNames componentsJoinedByString:@", "];
    }
}


#pragma mark - delegate ASIHTTP
-(void)requestStarted:(ASIHTTPRequest *)request{
    [self showLoading];
}

- (void)showLoading{
    requestCount++;
    if(!self.loading){
        self.loading = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.loading.labelText = @"Loading..";
        self.loading.delegate = self;
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request{
    [self countDownRequest];
    
    [self alert:@"Tidak terdapat koneksi internet."];
}

-(void)requestFinished:(ASIHTTPRequest *)request{
    [self countDownRequest];
    NSLog(@"\n==========\n reg %@ : %@\n\n", request.url.absoluteString, request.responseString);
    
    NSDictionary *root = [request.responseString JSONValue];
    
    if ([[[root valueForKeyPath:@"resultCode.message"] uppercaseString] rangeOfString:@"TOKEN"].length > 4) {
        [self alert:@"Sesi login anda sudah habis, mohon melakukan login ulang"];
        [AppConfig sharedConfig].sessionID = nil;
        
        LoginRegisterViewController *profile = [[[LoginRegisterViewController alloc] initWithNibName:@"LoginRegisterViewController" bundle:nil] autorelease];
        UINavigationController *nav = (UINavigationController *)[AppDelegate currentDelegate].mainViewController.centerController;
        UINavigationController *navP = [[[UINavigationController alloc] initWithRootViewController:profile] autorelease];
        navP.navigationBarHidden = YES;
        [nav presentViewController:navP animated:YES completion:nil];
        
        return;
    }
    
    NSDictionary *result = [root valueForKey:@"resultCode"];
    if ([[result valueForKey:@"code"] integerValue] == 0 && result != nil) {
        if (self.fromMenu) {
            NSString *name = [root valueForKeyPath:@"data.profile.fullName"];
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[AppConfig sharedConfig].loginInfo];
            [dict setValue:name forKey:@"fullName"];
            [AppConfig sharedConfig].loginInfo = dict;
            
            [self alert:[root valueForKeyPath:@"resultCode.message"]];
            [self.navigationController popToRootViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginSuccess" object:nil];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            if  (request.tag == 0) {
                [self alert:@"Proses registrasi berhasil, silahkan cek email anda untuk melakukan aktivasi"];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"RegisterSuccess" object:nil];
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [self alert:[root valueForKeyPath:@"resultCode.message"]];
                
                [self dismissViewControllerAnimated:YES completion:nil];
                [AppDelegate currentDelegate].window.rootViewController = (UIViewController *)[AppDelegate currentDelegate].mainViewController;
            }
            
//            [self dismissViewControllerAnimated:YES completion:nil];
//            [AppDelegate currentDelegate].window.rootViewController = (UIViewController *)[AppDelegate currentDelegate].mainViewController;
        }
    }else {
        [self alert:result[@"message"]];
    }
}

-(void)countDownRequest{
    requestCount--;
    
    if(requestCount < 1){
        [self.loading hide:NO];
    }
}

#pragma mark - delegate loading
-(void)hudWasHidden:(MBProgressHUD *)hud{
    self.loading = nil;
}


- (void)dealloc {
    [_viewForm release];
    [_data release];
    
    [_txtCategory release];
    [_txtProvinsi release];
    [_txtKota release];
    [_txtMinPrice release];
    [_txtMaxPrice release];
    [_lblInfo release];
    [_btnConfirm release];
    
    [_kategoriArray release];
    [_propinsiArray release];
    [_kotaArray release];
    [_hargaMinarray release];
    [_hargaMaxArray release];
    
    [_selectedKota release];
    [_selectedKategori release];
    [_selectedProvinsi release];
    
    [_btnDaftar release];
    [_btnSimpan release];
    [_btnSkip release];
    [super dealloc];
}
@end
