//
//  TerdekatViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/24/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "TerdekatViewController.h"
#import "ItemCell.h"
#import "DetailViewController.h"
#import "FilterViewController.h"
#import "EGOImageView.h"
#import "AppConfig.h"
#import "ASIHTTPRequest+Mandiri.h"
#import "MBProgressHUD.h"
#import "JSON.h"
#import "AppDelegate.h"
#import "IIViewDeckController.h"
#import "UICellRefreshLoading.h"
#import "TTTAttributedLabel.h"
#import "LoginRegisterViewController.h"
#import "MLDatabase.h"

@interface TerdekatViewController ()<ASIHTTPRequestDelegate, MBProgressHUDDelegate, UICellRefreshLoadingDelegate, FilterViewControllerDelegate>{
    IBOutlet UITableView *table;
    IBOutlet UITableViewCell *blankCell;
    IBOutlet UILabel *headerLabel;
    
    NSOperationQueue *queue;
    NSInteger requestCount;
    UICellRefreshLoading *refreshViewHeader;
    BOOL isrefresh;
    BOOL needLoadMore;
    NSInteger currentPage;
}

-(IBAction)openMenu:(id)sender;
-(IBAction)filter:(id)sender;

@property (nonatomic, retain) NSMutableArray *data;
@property (nonatomic, retain) MBProgressHUD *loading;
@property (nonatomic, retain) NSMutableDictionary *params;


@end

@implementation TerdekatViewController

@synthesize data, loading, params;

- (void)dealloc
{
    table.delegate = nil;
    self.data = nil;
    self.loading = nil;
    self.params = nil;
    
    for(ASIHTTPRequest *req in queue.operations){
        req.delegate = nil;
    }
    [queue cancelAllOperations];
    [queue release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lokasiUpdate:) name:MandiriLelangLocationNameDidUpdatedNotification object:nil];
    
    queue = [[NSOperationQueue alloc] init];
    
    refreshViewHeader = [UICellRefreshLoading createNewAsHeader:YES forView:table];
    refreshViewHeader.delegate = self;
    
    if([AppConfig sharedConfig].locationName.length > 0){
        headerLabel.text = [@"   Lokasi anda saat ini: " stringByAppendingString:[AppConfig sharedConfig].locationName];
        headerLabel.hidden = NO;
        table.frame = CGRectMake(0, headerLabel.frame.origin.y + headerLabel.frame.size.height, 320., self.view.frame.size.height - headerLabel.frame.origin.y - headerLabel.frame.size.height);
    }else{
        headerLabel.hidden = YES;
        headerLabel.text = @"   Lokasi anda saat ini: Tidak diketahui";
        table.frame = CGRectMake(0, headerLabel.frame.origin.y, 320., self.view.frame.size.height - headerLabel.frame.origin.y);
    }
    
    needLoadMore = YES;
    
    if(self.params == nil){
        self.params = [NSMutableDictionary dictionaryWithObject:@1 forKey:@"orderBy"];
    }
    
    [self makeRequest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [table reloadData];
}

#pragma mark - Actions
-(void)openMenu:(id)sender{
    if([AppDelegate currentDelegate].mainViewController.leftControllerIsClosed)
        [[AppDelegate currentDelegate] openMenu];
    else
        [[AppDelegate currentDelegate] closeMenu];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)makeRequest{
    NSString *url = KURLAssetTerdekat;
    url = [url stringByAppendingFormat:@"?pageIndex=%zd", currentPage];
    
    NSString *locName = [[[AppConfig sharedConfig].locationName lowercaseString] stringByReplacingOccurrencesOfString:@"kota" withString:@""];
    locName = [locName stringByReplacingOccurrencesOfString:@"administrasi" withString:@""];
    locName = [locName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSNumber *city = [[MLCity sharedData] cityIDforName:locName];
    MCity *cityData = [[MLCity sharedData] cityForID:city];
    
    // pengecekan value
    NSArray *keys = [NSArray arrayWithObjects:@"categoryID", @"provinceID", @"cityID", @"minPriceID", @"maxPriceID", @"hotPrice", @"auctionDate", @"documentTypeID", @"address", @"orderBy", @"keyword", nil];
    for(NSString *key in keys){
        id val = [self.params valueForKey:key];
        
        if(val == nil){
            if([key isEqualToString:@"address"] || [key isEqualToString:@"auctionDate"] || [key isEqualToString:@"keyword"])
                [self.params setValue:@"" forKey:key];
            else if([key isEqualToString:@"provinceID"])
                [self.params setValue:cityData.provinceID forKey:key];
            else if([key isEqualToString:@"cityID"])
                [self.params setValue:city forKey:key];
            else
                [self.params setValue:[key isEqualToString:@"orderBy"]?@"4":@"0" forKey:key];
        }
    }
    
    // membuat params
    for(NSString *key in self.params.keyEnumerator){
        NSString *val = [self.params valueForKey:key];
        
        if([val isKindOfClass:[NSString class]]){
            url = [url stringByAppendingFormat:@"&%@=%@", key, val];
        }else{
            url = [url stringByAppendingFormat:@"&%@=%d", key, val.intValue];
        }
    }
    
    NSLog(@"Params-nya %@", params);
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestAuthWithURL:[NSURL URLWithString:url]];
    [request encryptParams];
    request.delegate = self;
    request.tag = currentPage==0?0:1;
    requestCount++;
    [queue addOperation:request];
}

-(void)filter:(id)sender{
    FilterViewController *filter = [[FilterViewController alloc] initWithNibName:@"FilterViewController" bundle:nil];
    filter.delegate = self;
    filter.filterType = FilterTypeTerdekat;
    filter.initParams = self.params;
    
    UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:filter] autorelease];
    nav.navigationBarHidden = YES;
    
    [[AppDelegate currentDelegate].window.rootViewController presentViewController:nav animated:YES completion:^{
        
    }];
    [filter release];
}

-(void)alert:(NSString *)msg{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [alert release];
}

- (void)addRemoveFav:(UIButton *)sender {
    BOOL loggedIn = [AppConfig sharedConfig].sessionID.length > 0;
    
    if (loggedIn) {
        NSDictionary *dict = self.data[sender.tag];
        [self.view.layer setValue:@(sender.tag) forKey:@"fav"];
        
        NSString *url = sender.selected ? kURLRemoveFavorite : kURLAddFavorite;
        ASIFormDataRequest *request = [ASIFormDataRequest requestAuthWithURL:[NSURL URLWithString:url]];
        [request setPostValue:dict[@"assetID"] forKey:@"assetID"];
        request.delegate = self;
        request.tag = sender.selected ? 4 : 3;
        requestCount++;
        [queue addOperation:request];
    } else {
        LoginRegisterViewController *login = [[LoginRegisterViewController alloc] initWithNibName:@"LoginRegisterViewController" bundle:nil];
        [self presentViewController:login animated:YES completion:nil];
        [login release];
    }
    
}

#pragma mark - tableView delegate dan datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.data && self.data.count < 1)
        return 1;
    
    return self.data.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.data && self.data.count < 1)
        return blankCell.frame.size.height;
    
    NSDictionary *dict = [self.data objectAtIndex:indexPath.row];
    NSString *tglLelang = [dict valueForKey:@"auctionDateFormattedDateTime"];
    NSInteger t = [[dict valueForKey:@"type"] integerValue];
    
    NSString *text = [dict valueForKey:@"completeAddress"];
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:12.] constrainedToSize:CGSizeMake(299, 2000.) lineBreakMode:NSLineBreakByWordWrapping];
    return ((tglLelang.length>0 || t==1)?290.:272.) - 20 + size.height;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.data && self.data.count < 1)
        return blankCell;
    
    NSDictionary *dict = [self.data objectAtIndex:indexPath.row];
    NSString *tglLelang = [dict valueForKey:@"auctionDateFormattedDateTime"];
    NSInteger t = [[dict valueForKey:@"type"] integerValue];
    NSString *identifier = (tglLelang.length>0 || t==1 )?@"ItemCell":@"ItemCellNoTanggal";
    
    ItemCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(cell == nil){
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:identifier owner:self options:nil];
        
        for(id one in arr){
            if([one isKindOfClass:[ItemCell class]]){
                cell = (ItemCell *)one;
            }
        }
        
        [cell.btnFav addTarget:self action:@selector(addRemoveFav:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    NSString *newPrice = [dict valueForKey:@"newPrice"];
    
    cell.thumb.placeholderImage = [UIImage imageNamed:@"img-default"];
    cell.thumb.imageURL = [NSURL URLWithString:[dict valueForKey:@"defaultPhotoURL"]];
    cell.name.text = newPrice;
    cell.detail.text = [dict valueForKey:@"completeAddress"];
    cell.date.text = [dict valueForKey:@"postedDateFormattedDateTime"];
    cell.category.text = [dict valueForKey:@"categoryName"];
    
    if (t == 1) {NSLog(@"%@, %@, %zd", cell.tanggal, identifier, t);
        cell.tanggal.text = @"Jual Sukarela";
    } else {
        cell.tanggal.text = [NSString stringWithFormat:@"Tanggal Lelang: %@", tglLelang];
    }
    
    cell.name.textColor = [UIColor blackColor];
    cell.hotPrice.hidden = YES;
    cell.eAuction.hidden = ![dict[@"isEmailAuction"] boolValue];
    
    cell.btnFav.selected = [dict[@"isFavorite"] boolValue];
    cell.btnFav.tag = indexPath.row;
    
    NSString *text = [dict valueForKey:@"oldPrice"];
    
    if([[cell.name.text lowercaseString] isEqualToString:@"call"]){
        cell.name.textColor = [UIColor redColor];
    }else if(text.length > 3){
        cell.hotPrice.hidden = NO;
    }
    [cell updateLayout];
    
    cell.hargaAwalContainer.hidden = text.length < 3;
    [cell.hargaAwal setText:text afterInheritingLabelAttributesAndConfiguringWithBlock:^ NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        // Core Text APIs use C functions without a direct bridge to UIFont. See Apple's "Core Text Programming Guide" to learn how to configure string attributes.
        UIFont *boldSystemFont = cell.hargaAwal.font;
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        if (font) {
            [mutableAttributedString addAttribute:kTTTStrikeOutAttributeName value:[NSNumber numberWithBool:YES] range:[text rangeOfString:text]];
            CFRelease(font);
        }
        
        return mutableAttributedString;
    }];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row >= self.data.count-1 && needLoadMore){
        isrefresh = YES;
        currentPage++;
        [self makeRequest];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSMutableDictionary *dict = [self.data objectAtIndex:indexPath.row];
    
    DetailViewController *detail = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
    detail.data = dict;
    detail.assetID = [dict valueForKey:@"assetID"];
    [[AppDelegate currentDelegate].homeViewController pushViewController:detail animated:YES];
    [detail release];
}

#pragma mark - delegate ASIHTTP
-(void)requestStarted:(ASIHTTPRequest *)request{
    if(!self.loading && !isrefresh){
        self.loading = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.loading.labelText = @"Loading..";
        self.loading.delegate = self;
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request{
    [self countDownRequest];
    
    [self alert:@"Tidak terdapat koneksi internet."];
}

-(void)requestFinished:(ASIHTTPRequest *)request{
    [self countDownRequest];
    NSLog(@"\n==========\n home %@ : %@\n\n", request.url.absoluteString, request.responseString);
    
    NSDictionary *root = [request.responseString JSONValue];
    
    if ([[[root valueForKeyPath:@"resultCode.message"] uppercaseString] rangeOfString:@"TOKEN"].length > 4) {
        [self alert:@"Sesi login anda sudah habis, mohon melakukan login ulang"];
        [AppConfig sharedConfig].sessionID = nil;
        
        LoginRegisterViewController *profile = [[[LoginRegisterViewController alloc] initWithNibName:@"LoginRegisterViewController" bundle:nil] autorelease];
        UINavigationController *nav = (UINavigationController *)[AppDelegate currentDelegate].mainViewController.centerController;
        UINavigationController *navP = [[[UINavigationController alloc] initWithRootViewController:profile] autorelease];
        navP.navigationBarHidden = YES;
        [nav presentViewController:navP animated:YES completion:nil];
        
        return;
    }
    
    if (request.tag == 3) {
        NSInteger row = [[self.view.layer valueForKey:@"fav"] integerValue];
        NSMutableDictionary *dict = [self.data objectAtIndex:row];
        [dict setValue:@(true) forKey:@"isFavorite"];
        
        [table reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else if (request.tag == 4) {
        NSInteger row = [[self.view.layer valueForKey:@"fav"] integerValue];
        NSMutableDictionary *dict = [self.data objectAtIndex:row];
        [dict setValue:@(false) forKey:@"isFavorite"];
        
        [table reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else {
        NSMutableArray *datas = [root valueForKey:@"data"];
        if(request.tag == 0){
            self.data = datas;
        }else{
            [self.data addObjectsFromArray:datas];
        }
        
        needLoadMore = datas.count > 0;
        
        [table reloadData];
    }
}

-(void)countDownRequest{
    requestCount--;
    
    if(requestCount < 1){
        [self.loading hide:NO];
        
        if(isrefresh && refreshViewHeader.isLoading){
            [refreshViewHeader stopLoading];
        }
    }
}

#pragma mark - delegate loading
-(void)hudWasHidden:(MBProgressHUD *)hud{
    self.loading = nil;
}

#pragma mark - delegate scroll
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [refreshViewHeader scrollViewDidScroll:scrollView];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [refreshViewHeader scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}

#pragma mark - delegate refresh pull
-(void)UICellRefreshLoadingDidBeginLoading:(UICellRefreshLoading *)view{
    isrefresh = YES;
    table.contentSize = table.frame.size;
    currentPage = 0;
    [self makeRequest];
}

-(void)UICellRefreshLoadingDidFinishLoading:(UICellRefreshLoading *)view{
    [table reloadData];
}

#pragma mark - delegate filter
-(void)filterViewController:(FilterViewController *)filter didApplyFilter:(NSDictionary *)dict{
    isrefresh = NO;
    currentPage = 0;
    
    for(NSString *key in [dict keyEnumerator]){
        [self.params setValue:[dict valueForKey:key] forKey:key];
    }
    
    [self makeRequest];
}

#pragma mark - observer
-(void)lokasiUpdate:(NSNotification *)notif{
    
    if([AppConfig sharedConfig].locationName.length > 0){
        headerLabel.text = [@"   Lokasi anda saat ini: " stringByAppendingString:[AppConfig sharedConfig].locationName];
        headerLabel.hidden = NO;
        table.frame = CGRectMake(0, headerLabel.frame.origin.y + headerLabel.frame.size.height, 320., self.view.frame.size.height - headerLabel.frame.origin.y + headerLabel.frame.size.height);
        
        currentPage = 0;
        [self makeRequest];
    }else{
        headerLabel.hidden = YES;
        headerLabel.text = @"   Lokasi anda saat ini: Tidak diketahui";
        table.frame = CGRectMake(0, headerLabel.frame.origin.y, 320., self.view.frame.size.height - headerLabel.frame.origin.y);
    }
}

@end
