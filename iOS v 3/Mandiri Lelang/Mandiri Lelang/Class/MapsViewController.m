//
//  MapsViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 9/11/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "MapsViewController.h"

@interface MapsViewController ()<MKAnnotation>

-(IBAction)back:(id)sender;

@end

@implementation MapsViewController

@synthesize coordinate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
-(void)back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
