//
//  AboutViewController.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/9/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController

@property (nonatomic, retain) NSString *url;
@property (nonatomic) BOOL isNavMode;

@end
