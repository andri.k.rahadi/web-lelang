//
//  SearchJadwalViewController.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 10/30/16.
//  Copyright © 2016 Asep Mulyana. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchJadwalViewControllerDelegate <NSObject>
- (void)didSetStartDate:(NSDate *)from endDate:(NSDate *)to kpknls:(NSArray *)kpknls;
@end


@interface SearchJadwalViewController : UIViewController

@property (nonatomic, assign) id<SearchJadwalViewControllerDelegate> delegate;
@property (nonatomic, retain) NSMutableArray *selectedBranches;
@property (nonatomic, retain) NSDate *defFromDate;
@property (nonatomic, retain) NSDate *defToDate;

@end
