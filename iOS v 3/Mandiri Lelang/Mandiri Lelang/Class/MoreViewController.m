//
//  MoreViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 9/26/17.
//  Copyright © 2017 Asep Mulyana. All rights reserved.
//

#import "MoreViewController.h"
#import "TerbaruViewController.h"
#import "TerdekatViewController.h"
#import "AppDelegate.h"
#import "IIViewDeckController.h"

@interface MoreViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation MoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - delegate dan datasourece table

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"moreCell"];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"moreCell"] autorelease];
        
        UIView *separator = [[[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height-1, 320., 1.)] autorelease];
        separator.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        separator.backgroundColor = [UIColor grayColor];
        [cell addSubview:separator];
        
        cell.textLabel.font = [UIFont systemFontOfSize:14.];
        cell.textLabel.textColor = [UIColor grayColor];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    
    cell.imageView.tintColor = [UIColor colorWithRed:.02 green:.24 blue:.46 alpha:1];
    
    if (indexPath.row == 0) {
        cell.textLabel.text = @"Terbaru";
        cell.imageView.image = [[UIImage imageNamed:@"tab-terbaru"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    } else {
        cell.textLabel.text = @"Terdekat";
        cell.imageView.image = [[UIImage imageNamed:@"tab-terdekat"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        TerbaruViewController *baru = [[TerbaruViewController alloc] initWithNibName:@"TerbaruViewController" bundle:nil];
        [self.navigationController pushViewController:baru animated:YES];
    } else {
        TerdekatViewController *baru = [[TerdekatViewController alloc] initWithNibName:@"TerdekatViewController" bundle:nil];
        [self.navigationController pushViewController:baru animated:YES];
    }
}


#pragma mark - Actions

-(void)openMenu:(UIButton *)sender{
    if([AppDelegate currentDelegate].mainViewController.leftControllerIsClosed)
        [[AppDelegate currentDelegate] openMenu];
    else
        [[AppDelegate currentDelegate] closeMenu];
}

@end
