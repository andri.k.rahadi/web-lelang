//
//  HubungiViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/17/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import "HubungiViewController.h"
#import "AppDelegate.h"
#import "IIViewDeckController.h"
#import "Tools.h"
#import "ComboTableViewController.h"
#import "MLDatabase.h"
#import "AppConfig.h"
#import "MBProgressHUD.h"
#import "ASIHTTPRequest+Mandiri.h"
#import "JSON.h"
#import "MLWebContent.h"

@interface HubungiViewController ()<ComboTableViewControllerDelegate, ASIHTTPRequestDelegate, MBProgressHUDDelegate, UIAlertViewDelegate>{
    IBOutlet UIScrollView *scroll;
    IBOutlet UIView *formView;
    IBOutlet UIButton *kirimButton;
    
    IBOutlet UITextField *nama;
    IBOutlet UITextField *telp;
    IBOutlet UITextField *email;
    IBOutlet UITextField *kategori;
    IBOutlet UIButton *kategoriButton;
    IBOutlet UITextField *kode;
    IBOutlet UITextField *pengelola;
    IBOutlet UIButton *pengelolaButton;
    IBOutlet UITextField *pesan;
    IBOutlet UILabel *lblKet;
    IBOutlet UIButton *btnBack;
    IBOutlet UIButton *btnMenu;
    
    NSOperationQueue *queue;
    NSInteger requestCount;
}

-(IBAction)hideKeyboard:(id)sender;
-(IBAction)openMenu:(id)sender;
-(IBAction)reset:(id)sender;
-(IBAction)showCombo:(UIButton *)sender;
-(IBAction)submit:(id)sender;

@property (nonatomic, retain) NSMutableArray *kategoriArray;
@property (nonatomic, retain) NSMutableArray *pengelolaArray;
@property (nonatomic, retain) MBProgressHUD *loading;

@end

@implementation HubungiViewController

@synthesize kategoriArray, pengelolaArray, loading;

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    self.kategoriArray = nil;
    self.pengelolaArray = nil;
    self.loading = nil;
    
    for(ASIHTTPRequest *req in queue.operations){
        req.delegate = nil;
    }
    [queue cancelAllOperations];
    [queue release];
    
    [lblKet release];
    [btnBack release];
    [btnMenu release];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    
    btnMenu.hidden = self.withBackButton;
    btnBack.hidden = !self.withBackButton;
    
    queue = [[NSOperationQueue alloc] init];
    
    self.pengelolaArray = [MLBranch sharedData].branches;
    self.kategoriArray = [MLContactUsType sharedData].contactUsTypes;
    
//    requestCount++;
//    [self showLoading];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        MWebContent *cont = [[MLWebContent sharedData] webcontentWithID:7];
        NSString *oldStr = [cont.name stringByAppendingString:@"<style type=\"text/css\">*{font-family: Helvetica}</style>"];
        NSMutableAttributedString *str = [[[NSMutableAttributedString alloc] initWithData:[oldStr dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil] autorelease];
        lblKet.attributedText = str;
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [lblKet sizeToFit];
            
            CGRect frame = formView.frame;
            frame.size.height = lblKet.frame.size.height + lblKet.frame.origin.y + 30;
            formView.frame = frame;
            scroll.contentSize = frame.size;
//            [self countDownRequest];
        });
    });
    
    
    [scroll addSubview:formView];
    scroll.contentSize = formView.frame.size;
    scroll.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    scroll.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    for(UITextField *txt in formView.subviews){
        if([txt isKindOfClass:[UITextField class]]){
//            if(txt.tag > 0){
//                UIPickerView *picker = [[[UIPickerView alloc] init] autorelease];
//                picker.backgroundColor = [UIColor lightGrayColor];
//                picker.delegate = self;
//                picker.dataSource = self;
//                picker.showsSelectionIndicator = YES;
//                picker.tag = txt.tag;
//                txt.inputView = picker;
//            }
            
            txt.layer.masksToBounds = YES;
            txt.layer.cornerRadius = 3.;
            txt.layer.borderColor = [UIColor grayColor].CGColor;
            txt.layer.borderWidth = 1.;
            
            UIView *v = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 8., 10)] autorelease];
            v.backgroundColor = [UIColor clearColor];
            txt.leftView = v;
            txt.leftViewMode = UITextFieldViewModeAlways;
        }//else if([txt isKindOfClass:[UIButton class]]){
//            UIButton *btn = (UIButton *)txt;
//            btn.layer.masksToBounds = YES;
//            btn.layer.cornerRadius = 3.;
//            [btn setBackgroundImage:[Tools solidImageForColor:btn.backgroundColor withSize:btn.frame.size] forState:UIControlStateNormal];
//        }
    }
    
    [kirimButton setBackgroundImage:[Tools solidImageForColor:kirimButton.backgroundColor withSize:kirimButton.frame.size] forState:UIControlStateNormal];
    
    NSLog(@"%@", [AppConfig sharedConfig].loginInfo);
    
    if ([AppConfig sharedConfig].sessionID.length > 0) {
        nama.text = [AppConfig sharedConfig].loginInfo[@"fullName"];
        email.text = [AppConfig sharedConfig].loginInfo[@"email"];
    }
    
    kode.text = self.kodeAgunan;
    
    for (MBranch *dict in pengelolaArray) {
        NSString *name = dict.name;
        
        if ([name isEqualToString:self.pengelolaNama]) {
            pengelola.tag = [pengelolaArray indexOfObject:dict];
            pengelola.text = dict.name;
            break;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions
-(void)hideKeyboard:(id)sender{
    for(UITextField *txt in formView.subviews){
        if([txt isKindOfClass:[UITextField class]]){
            [txt resignFirstResponder];
        }
    }
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)reset:(id)sender{
    [self hideKeyboard:nil];
    for(UITextField *txt in formView.subviews){
        if([txt isKindOfClass:[UITextField class]]){
            txt.text = @"";
            txt.tag = -1;
        }
    }
}

-(void)openMenu:(id)sender{
    if([AppDelegate currentDelegate].mainViewController.leftControllerIsClosed)
        [[AppDelegate currentDelegate] openMenu];
    else
        [[AppDelegate currentDelegate] closeMenu];
}

-(void)showCombo:(UIButton *)sender{
    [self hideKeyboard:nil];
    
    ComboTableViewController *combo = [[ComboTableViewController alloc] initWithNibName:@"ComboTableViewController" bundle:nil];
    combo.tag = sender.tag;
    combo.delegate = self;
    
    if(sender.tag == 0){
        combo.data = self.kategoriArray;
        combo.selectedRow = kategori.tag;
    }else if(sender.tag == 1){
        combo.data = self.pengelolaArray;
        combo.selectedRow = pengelola.tag;
    }
    
    [self.navigationController pushViewController:combo animated:YES];
    [combo release];
}

-(void)alert:(NSString *)msg{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [alert release];
}

-(BOOL)formIsValid{
    if(nama.text.length < 1){
        [self alert:@"Mohon isi nama anda."];
        [nama becomeFirstResponder];
        [scroll scrollRectToVisible:nama.frame animated:YES];
        return NO;
    }
    
    if(telp.text.length < 1){
        [self alert:@"Mohon isi nomor telpon anda."];
        [telp becomeFirstResponder];
        [scroll scrollRectToVisible:telp.frame animated:YES];
        return NO;
    }
    
    if(email.text.length < 1){
        [self alert:@"Mohon isi email anda."];
        [email becomeFirstResponder];
        [scroll scrollRectToVisible:email.frame animated:YES];
        return NO;
    }
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    if(![emailTest evaluateWithObject:email.text] || email.text.length < 5){
        [self alert:@"Mohon masukkan alamat email yang valid."];
        [email becomeFirstResponder];
        [scroll scrollRectToVisible:email.frame animated:YES];
        return NO;
    }
    
    if(kategori.text.length < 1){
        [self alert:@"Mohon pilih kategori."];
        [scroll scrollRectToVisible:kategori.frame animated:YES];
        return NO;
    }
    
    if(pesan.text.length < 1){
        [self alert:@"Mohon isi pesan anda."];
        [pesan becomeFirstResponder];
        [scroll scrollRectToVisible:pesan.frame animated:YES];
        return NO;
    }
    
    return YES;
}

-(void)submit:(id)sender{
    [self hideKeyboard:nil];
    
    if(![self formIsValid])return;
    
    NSNumber *categoryID = [[self.kategoriArray objectAtIndex:kategori.tag] valueForKey:@"identifier"];
    NSNumber *branchID = [NSNumber numberWithInt:0];
    if(pengelola.tag > -1){
        branchID = [[self.pengelolaArray objectAtIndex:pengelola.tag] valueForKey:@"identifier"];
    }
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:kURLContactUs]];
    [request addPostValue:nama.text forKey:@"name"];
    [request addPostValue:telp.text forKey:@"phone"];
    [request addPostValue:email.text forKey:@"email"];
    [request addPostValue:categoryID forKey:@"messageCategoryTypeID"];
    [request addPostValue:kode.text forKey:@"assetCode"];
    [request addPostValue:branchID forKey:@"branchID"];
    [request addPostValue:pesan.text forKey:@"textMessage"];
    [request encryptParams];
    requestCount++;
    request.delegate = self;
    [queue addOperation:request];
}

#pragma mark - Observer keyboard
-(void)keyboardDidHide:(NSNotification *)sender{
    CGRect frame = scroll.frame;
    frame.size.height = self.view.frame.size.height - scroll.frame.origin.y - 46;
    scroll.frame = frame;
}

-(void)keyboardDidShow:(NSNotification *)sender{
    CGRect keyboardRect = [[sender.userInfo valueForKeyPath:@"UIKeyboardBoundsUserInfoKey"] CGRectValue];
    
    CGRect frame = scroll.frame;
    frame.size.height = self.view.frame.size.height - scroll.frame.origin.y - keyboardRect.size.height;
    scroll.frame = frame;
}

#pragma mark - delegate combo
-(void)combo:(ComboTableViewController *)controller didSelectItemAtIndex:(NSInteger)index{
    if(controller.tag == 0){
        kategori.text = [[self.kategoriArray objectAtIndex:index] valueForKey:@"name"];
        kategori.tag = index;
    }else if(controller.tag == 1){
        pengelola.text = [[self.pengelolaArray objectAtIndex:index] valueForKey:@"name"];
        pengelola.tag = index;
    }
}

- (void)showLoading {
    self.loading = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.loading.labelText = @"Loading..";
    self.loading.delegate = self;
}

#pragma mark - delegate ASIHTTP
-(void)requestStarted:(ASIHTTPRequest *)request{
    if(!self.loading){
        self.loading = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.loading.labelText = @"Loading..";
        self.loading.delegate = self;
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request{
    [self countDownRequest];
    
    [self alert:@"Tidak terdapat koneksi internet."];
}

-(void)requestFinished:(ASIHTTPRequest *)request{
    [self countDownRequest];
    NSLog(@"\n==========\n kontak us %@ : %@\n\n", request.url.absoluteString, request.responseString);
    
    NSDictionary *root = [request.responseString JSONValue];
    NSString *message = [root valueForKey:@"thankYouMessage"];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

-(void)countDownRequest{
    requestCount--;
    
    if(requestCount < 1){
        [self.loading hide:NO];
    }
}

#pragma mark - delegate alert
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    [self reset:nil];
    [alertView release];
}

@end
