//
//  SearchResultViewController.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/9/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailJadwalViewController : UIViewController

@property (nonatomic, copy) NSString *jadwalID;
@property (nonatomic, copy) NSString *kpknl;
@property (nonatomic, copy) NSString *tanggal;

@end
