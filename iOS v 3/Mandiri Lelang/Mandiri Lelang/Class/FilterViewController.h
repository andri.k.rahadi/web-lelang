//
//  FilterViewController.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 8/17/14.
//  Copyright (c) 2014 Asep Mulyana. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    FilterTypeNone,
    FilterTypeSegera,
    FilterTypeHotPrice,
    FilterTypeTerbaru,
    FilterTypeTerdekat,
    FilterTypeSukarela
}FilterType;

@protocol FilterViewControllerDelegate;

@interface FilterViewController : UIViewController

@property (nonatomic) FilterType filterType;
@property (nonatomic, unsafe_unretained) id<FilterViewControllerDelegate>delegate;
@property (nonatomic, unsafe_unretained) NSDictionary *initParams;

@end


@protocol FilterViewControllerDelegate <NSObject>

-(void)filterViewController:(FilterViewController *)filter didApplyFilter:(NSDictionary *)dict;

@end
