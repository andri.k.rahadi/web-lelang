//
//  LaunchscreenViewController.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 10/29/16.
//  Copyright © 2016 Asep Mulyana. All rights reserved.
//

#import "LaunchscreenViewController.h"
#import "AppDelegate.h"
#import "LoginRegisterViewController.h"
#import "MLWebContent.h"

@interface LaunchscreenViewController ()

@property (retain, nonatomic) IBOutlet UILabel *lblMessage;
@property (retain, nonatomic) IBOutlet UIImageView *imgLaunching;

@end

@implementation LaunchscreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self dataTerupdate:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataTerupdate:) name:@"dataWebContentTerupdate" object:nil];
    
    if(self.view.frame.size.height > 480.){
        _imgLaunching.image = [UIImage imageNamed:@"Splash0.png"];
    }else{
        _imgLaunching.image = [UIImage imageNamed:@"SplashScreen"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#pragma mark - Actions

- (IBAction)skip:(id)sender {
    [AppDelegate currentDelegate].window.rootViewController = (UIViewController *)[AppDelegate currentDelegate].mainViewController;
}

- (IBAction)login:(id)sender {
    LoginRegisterViewController *login = [[[LoginRegisterViewController alloc] initWithNibName:@"LoginRegisterViewController" bundle:nil] autorelease];
    [self.navigationController pushViewController:login animated:YES];
}

- (void)dataTerupdate:(NSNotification *)notif{
    dispatch_async(dispatch_get_main_queue(), ^{
        MWebContent *content = [[MLWebContent sharedData] webcontentWithID:9];
        _lblMessage.text = content.name;
    });
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [_lblMessage release];
    [_imgLaunching release];
    [super dealloc];
}
@end
