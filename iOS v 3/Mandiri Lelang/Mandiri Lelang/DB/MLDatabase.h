//
//  SDDatabase.h
//  OneTouch
//
//  Created by asepmoels on 1/2/13.
//  Copyright (c) 2013 Better-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"
#import "MLCategory.h"
#import "MLProvince.h"
#import "MLCity.h"
#import "MLPrice.h"
#import "MLDocumentType.h"
#import "MLContactUsType.h"
#import "MLBranch.h"
#import "MLWebContent.h"
#import "MLSettings.h"
#import "MLKpknl.h"

@interface MLDatabase : NSObject
+(void)initiateDatabase;
+(NSString *)dataFilePath;
+(sqlite3 *)openDatabase;
+(void)closeDatabase:(sqlite3 *)db;
+(NSObject *)locker;
@end
