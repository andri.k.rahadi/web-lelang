//
//  MLWebContent.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 11/9/16.
//  Copyright © 2016 Asep Mulyana. All rights reserved.
//

#import "MLWebContent.h"
#import "MLDatabase.h"

static MLWebContent *sharedData = nil;

@implementation MLWebContent

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

-(void)addCategory:(NSDictionary *)dict{NSLog(@"Nambahin webcontent");
    @synchronized([MLDatabase locker])
    {
        sqlite3 *database = [MLDatabase openDatabase];
        
        if(database != nil){
            NSString *query = @"INSERT INTO webcontent VALUES(?, ?, ?, ?, ?);";
            sqlite3_stmt *statement = nil;
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
                sqlite3_bind_int(statement, 1, [[dict valueForKey:@"id"] intValue]);
                sqlite3_bind_text(statement, 2, [[dict valueForKey:@"name"] UTF8String], -1, NULL);
                sqlite3_bind_text(statement, 3, [[dict valueForKey:@"description"] UTF8String], -1, NULL);
                sqlite3_bind_int(statement, 4, [[dict valueForKey:@"isActive"] intValue]);
                sqlite3_bind_text(statement, 5, [[dict valueForKey:@"flag"] UTF8String], -1, NULL);
            }
            int kode = sqlite3_step(statement);
            if(kode != SQLITE_DONE)
                NSLog(@"Error ketika menambah category %d", kode);
            sqlite3_finalize(statement);
        }
        
        [MLDatabase closeDatabase:database];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"dataWebContentTerupdate" object:nil];
    }
}

-(void)deleteWebContent:(MWebContent *)cat{
    @synchronized([MLDatabase locker])
    {
        sqlite3 *database = [MLDatabase openDatabase];
        
        if(database != nil){
            NSString *query = @"DELETE FROM webcontent WHERE id=?;";
            sqlite3_stmt *statement = nil;
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
                sqlite3_bind_int(statement, 1, cat.identifier.intValue);
            }
            int kode = sqlite3_step(statement);
            if(kode != SQLITE_DONE)
                NSLog(@"Error ketika menghapus category %d", kode);
            sqlite3_finalize(statement);
        }
        
        [MLDatabase closeDatabase:database];
    }
}

-(BOOL)recordIsExistWithID:(int)_id{
    @synchronized([MLDatabase locker])
    {
        sqlite3 *database = [MLDatabase openDatabase];
        
        BOOL exist = NO;
        if(database != nil){
            NSString *query = [NSString stringWithFormat:@"SELECT * FROM webcontent WHERE id=%d LIMIT 1", _id];
            sqlite3_stmt *statement;
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
                while(sqlite3_step(statement) == SQLITE_ROW){
                    exist = YES;
                }
            }
            sqlite3_finalize(statement);
        }
        
        [MLDatabase closeDatabase:database];
        
        return exist;
    }
}

-(void)loadData:(NSMutableArray *)datas{
    for(NSDictionary *dict in datas){
        int iid = [[dict valueForKey:@"id"] intValue];
        if([self recordIsExistWithID:iid]){
            MWebContent *branch = [[[MWebContent alloc] init] autorelease];
            branch.identifier = [NSNumber numberWithInt:iid];
            [self deleteWebContent:branch];
        }
        [self addCategory:dict];
    }
}

-(NSMutableArray *)webcontents{
    @synchronized([MLDatabase locker])
    {
        NSMutableArray *data = [NSMutableArray array];
        sqlite3 *database = [MLDatabase openDatabase];
        
        if(database != nil){
            NSString *query = @"SELECT * FROM webcontent WHERE isActive=1 ORDER BY id COLLATE NOCASE ASC";
            sqlite3_stmt *statement;
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
                while(sqlite3_step(statement) == SQLITE_ROW){
                    int _id = sqlite3_column_int(statement, 0);
                    char *name = (char *)sqlite3_column_text(statement, 1);
                    char *desc = (char *)sqlite3_column_text(statement, 2);
                    
                    MWebContent *add = [[MWebContent alloc] init];
                    add.name = [NSString stringWithUTF8String:name];
                    add.identifier = [NSNumber numberWithInt:_id];
                    add.description = [NSString stringWithUTF8String:desc];
                    [data addObject:add];
                    [add release];
                }
            }
            sqlite3_finalize(statement);
        }
        
        [MLDatabase closeDatabase:database];
        
        return data;
    }
}

-(MWebContent *)webcontentWithID:(int)_id {
    @synchronized([MLDatabase locker])
    {
        MWebContent *result = nil;
        sqlite3 *database = [MLDatabase openDatabase];
        
        if(database != nil){
            NSString *query = @"SELECT * FROM webcontent WHERE isActive=1 AND id=?";
            sqlite3_stmt *statement;
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
                sqlite3_bind_int(statement, 1, _id);
                
                while(sqlite3_step(statement) == SQLITE_ROW){
                    int _id = sqlite3_column_int(statement, 0);
                    char *name = (char *)sqlite3_column_text(statement, 1);
                    char *desc = (char *)sqlite3_column_text(statement, 2);
                    
                    MWebContent *add = [[MWebContent alloc] init];
                    add.name = [NSString stringWithUTF8String:name];
                    add.identifier = [NSNumber numberWithInt:_id];
                    add.description = [NSString stringWithUTF8String:desc];
                    result = add;
                }
            }
            sqlite3_finalize(statement);
        }
        
        [MLDatabase closeDatabase:database];
        
        return [result autorelease];
    }
}

// class method

+(void)initiateTable{
    @synchronized([MLDatabase locker])
    {
        char *error;
        sqlite3 *database = [MLDatabase openDatabase];
        NSString *createUser = @"CREATE TABLE IF NOT EXISTS webcontent (id INTEGER PRIMARY KEY, name TEXT, description TEXT, isActive INTEGER, flag TEXT);";
        if(sqlite3_exec(database, [createUser UTF8String], NULL, NULL, &error) != SQLITE_OK){
            NSLog(@"Error ketika membuat tabel category %s", error);
        }
        [MLDatabase closeDatabase:database];
    }
}

+(MLWebContent *)sharedData{
    if(sharedData == nil){
        sharedData = [[MLWebContent alloc] init];
    }
    return sharedData;
}

@end


@implementation MWebContent

@synthesize identifier, name, description;

- (void)dealloc
{
    self.identifier = nil;
    self.name = nil;
    self.description = nil;
    [super dealloc];
}

@end