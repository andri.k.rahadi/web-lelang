//
//  SDPhone.h
//  OneTouch
//
//  Created by asepmoels on 1/2/13.
//  Copyright (c) 2013 Better-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@class MCategory;

@interface MLCategory : NSObject
+(void)initiateTable;
+(MLCategory *)sharedData;

-(void)loadData:(NSMutableArray *)datas;
-(NSMutableArray *)categories;
-(MCategory *)categoryWithID:(NSInteger)cid;

@end

@interface MCategory : NSObject

@property (nonatomic, retain) NSNumber *identifier;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;

@end
