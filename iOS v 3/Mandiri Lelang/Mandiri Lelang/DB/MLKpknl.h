//
//  SDPhone.h
//  OneTouch
//
//  Created by asepmoels on 1/2/13.
//  Copyright (c) 2013 Better-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@class MKnpnl;

@interface MLKnpnl : NSObject
+(void)initiateTable;
+(MLKnpnl *)sharedData;

-(void)loadData:(NSMutableArray *)datas;
-(MKnpnl *)kpknlWithID:(NSInteger)kid;
-(NSMutableArray *)kpknls;

@end

@interface MKnpnl : NSObject

@property (nonatomic, retain) NSNumber *identifier;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;

@end
