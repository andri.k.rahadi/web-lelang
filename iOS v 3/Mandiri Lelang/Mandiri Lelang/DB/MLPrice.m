//
//  SDPhone.m
//  OneTouch
//
//  Created by asepmoels on 1/2/13.
//  Copyright (c) 2013 Better-B. All rights reserved.
//

#import "MLPrice.h"
#import "MLDatabase.h"
#import "sqlite3.h"

static MLPrice *sharedData = nil;

@implementation MLPrice

- (id)init
{
    self = [super init];
    if (self) {
        if([self prices].count < 1){
            [self addPrice:@{@"id":@0,
                            @"name":@"Semua",
                            @"description":@"",
                            @"isActive":@1,
                             @"value":@0,
                            @"flag":@"Add"
                            }];
        }
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

-(void)addPrice:(NSDictionary *)dict{
    @synchronized([MLDatabase locker])
    {
    sqlite3 *database = [MLDatabase openDatabase];
    
    if(database != nil){
        NSString *query = @"INSERT INTO prices VALUES(?, ?, ?, ?, ?, ?);";
        sqlite3_stmt *statement = nil;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            sqlite3_bind_int(statement, 1, [[dict valueForKey:@"id"] intValue]);
            sqlite3_bind_int64(statement, 4, [[dict valueForKey:@"value"] longLongValue]);
            sqlite3_bind_text(statement, 2, [[dict valueForKey:@"name"] UTF8String], -1, NULL);
            sqlite3_bind_text(statement, 3, [[dict valueForKey:@"description"] UTF8String], -1, NULL);
            sqlite3_bind_int(statement, 5, [[dict valueForKey:@"isActive"] intValue]);
            sqlite3_bind_text(statement, 6, [[dict valueForKey:@"flag"] UTF8String], -1, NULL);
        }
        int kode = sqlite3_step(statement);
        if(kode != SQLITE_DONE)
            NSLog(@"Error ketika menambah prices %d", kode);
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    }
}

-(void)deletePrice:(MPrice *)cat{
    @synchronized([MLDatabase locker])
    {
    sqlite3 *database = [MLDatabase openDatabase];
    
    if(database != nil){
        NSString *query = @"DELETE FROM prices WHERE id=?;";
        sqlite3_stmt *statement = nil;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            sqlite3_bind_int(statement, 1, cat.identifier.intValue);
        }
        int kode = sqlite3_step(statement);
        if(kode != SQLITE_DONE)
            NSLog(@"Error ketika menghapus prices %d", kode);
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    }
}

-(BOOL)recordIsExistWithID:(int)_id{
    @synchronized([MLDatabase locker])
    {
    sqlite3 *database = [MLDatabase openDatabase];
    
    BOOL exist = NO;
    if(database != nil){
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM prices WHERE id=%d LIMIT 1", _id];
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                exist = YES;
            }
        }
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    
    return exist;
    }
}

-(void)loadData:(NSMutableArray *)datas{
    for(NSDictionary *dict in datas){
        int iid = [[dict valueForKey:@"id"] intValue];
        if([self recordIsExistWithID:iid]){
            MPrice *branch = [[[MPrice alloc] init] autorelease];
            branch.identifier = [NSNumber numberWithInt:iid];
            [self deletePrice:branch];
        }
        [self addPrice:dict];
    }
}

-(NSMutableArray *)prices{
    @synchronized([MLDatabase locker])
    {
    NSMutableArray *data = [NSMutableArray array];
    sqlite3 *database = [MLDatabase openDatabase];
    
    if(database != nil){
        NSString *query = @"SELECT * FROM prices WHERE isActive=1 ORDER BY id COLLATE NOCASE ASC";
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                int _id = sqlite3_column_int(statement, 0);
                long long pr = sqlite3_column_int64(statement, 3);
                char *name = (char *)sqlite3_column_text(statement, 1);
                char *desc = (char *)sqlite3_column_text(statement, 2);
                
                MPrice *add = [[MPrice alloc] init];
                add.name = [NSString stringWithUTF8String:name];
                add.identifier = [NSNumber numberWithInt:_id];
                add.value = [NSNumber numberWithLongLong:pr];
                add.description = [NSString stringWithUTF8String:desc];
                [data addObject:add];
                [add release];
            }
        }
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    
    return data;
    }
}

-(NSMutableArray *)pricesBeginFromID:(NSNumber *)idp{
    @synchronized([MLDatabase locker])
    {
    NSMutableArray *data = [NSMutableArray array];
    sqlite3 *database = [MLDatabase openDatabase];
    
    if(database != nil){
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM prices WHERE isActive=1 AND id>=%d ORDER BY id COLLATE NOCASE ASC", idp.intValue];
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                int _id = sqlite3_column_int(statement, 0);
                long long pr = sqlite3_column_int64(statement, 3);
                char *name = (char *)sqlite3_column_text(statement, 1);
                char *desc = (char *)sqlite3_column_text(statement, 2);
                
                MPrice *add = [[MPrice alloc] init];
                add.name = [NSString stringWithUTF8String:name];
                add.identifier = [NSNumber numberWithInt:_id];
                add.value = [NSNumber numberWithLongLong:pr];
                add.description = [NSString stringWithUTF8String:desc];
                [data addObject:add];
                [add release];
            }
        }
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    
    return data;
    }
}

// class method

+(void)initiateTable{
    @synchronized([MLDatabase locker])
    {
    char *error;
    sqlite3 *database = [MLDatabase openDatabase];
    NSString *createUser = @"CREATE TABLE IF NOT EXISTS prices (id INTEGER PRIMARY KEY, name TEXT, description TEXT, val UNSIGNED BIG INT, isActive INTEGER, flag TEXT);";
    if(sqlite3_exec(database, [createUser UTF8String], NULL, NULL, &error) != SQLITE_OK){
        NSLog(@"Error ketika membuat tabel price %s", error);
    }
    [MLDatabase closeDatabase:database];
    }
}

+(MLPrice *)sharedData{
    if(sharedData == nil){
        sharedData = [[MLPrice alloc] init];
    }
    return sharedData;
}

@end

@implementation MPrice

@synthesize identifier, name, description, value;

- (void)dealloc
{
    self.value = nil;
    self.identifier = nil;
    self.name = nil;
    self.description = nil;
    [super dealloc];
}

@end