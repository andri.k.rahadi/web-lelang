//
//  SDPhone.h
//  OneTouch
//
//  Created by asepmoels on 1/2/13.
//  Copyright (c) 2013 Better-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@interface MLContactUsType : NSObject
+(void)initiateTable;
+(MLContactUsType *)sharedData;

-(void)loadData:(NSMutableArray *)datas;
-(NSMutableArray *)contactUsTypes;
@end

@interface MContactUsType : NSObject

@property (nonatomic, retain) NSNumber *identifier;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;

@end