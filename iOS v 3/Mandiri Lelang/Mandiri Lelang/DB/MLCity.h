//
//  SDPhone.h
//  OneTouch
//
//  Created by asepmoels on 1/2/13.
//  Copyright (c) 2013 Better-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@class MCity;

@interface MLCity: NSObject
+(void)initiateTable;
+(MLCity *)sharedData;

-(void)loadData:(NSMutableArray *)datas;
-(NSMutableArray *)citiesForProvince:(NSNumber *)provinceID;
-(NSNumber *)cityIDforName:(NSString *)name;
-(MCity *)cityForID:(NSNumber *)name;
-(NSMutableArray *)cities;

@end

@interface MCity : NSObject

@property (nonatomic, retain) NSNumber *identifier;
@property (nonatomic, retain) NSNumber *provinceID;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;

@end