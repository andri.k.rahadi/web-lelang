//
//  SDPhone.h
//  OneTouch
//
//  Created by asepmoels on 1/2/13.
//  Copyright (c) 2013 Better-B. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MLDocumentType: NSObject
+(void)initiateTable;
+(MLDocumentType *)sharedData;

-(void)loadData:(NSMutableArray *)datas;
-(NSMutableArray *)documentTypes;
@end

@interface MDocumentType : NSObject

@property (nonatomic, retain) NSNumber *identifier;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;

@end