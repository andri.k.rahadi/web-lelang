//
//  SDPhone.h
//  OneTouch
//
//  Created by asepmoels on 1/2/13.
//  Copyright (c) 2013 Better-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@class MBranch;

@interface MLBranch : NSObject
+(void)initiateTable;
+(MLBranch *)sharedData;

-(void)loadData:(NSMutableArray *)datas;
-(NSMutableArray *)branches;
-(MBranch *)brancheWithID:(NSInteger)bid;

@end

@interface MBranch : NSObject

@property (nonatomic, retain) NSNumber *identifier;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;

@end
