//
//  SDPhone.m
//  OneTouch
//
//  Created by asepmoels on 1/2/13.
//  Copyright (c) 2013 Better-B. All rights reserved.
//

#import "MLCategory.h"
#import "MLDatabase.h"

static MLCategory *sharedData = nil;

@implementation MLCategory

- (id)init
{
    self = [super init];
    if (self) {
        if([self categories].count < 1){
            [self addCategory:@{@"id":@0,
                                @"name":@"Semua",
                                @"description":@"",
                                @"isActive":@1,
                                @"flag":@"Add"
                                }];
        }
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

-(void)addCategory:(NSDictionary *)dict{
    @synchronized([MLDatabase locker])
    {
    sqlite3 *database = [MLDatabase openDatabase];
    
    if(database != nil){
        NSString *query = @"INSERT INTO category VALUES(?, ?, ?, ?, ?);";
        sqlite3_stmt *statement = nil;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            sqlite3_bind_int(statement, 1, [[dict valueForKey:@"id"] intValue]);
            sqlite3_bind_text(statement, 2, [[dict valueForKey:@"name"] UTF8String], -1, NULL);
            sqlite3_bind_text(statement, 3, [[dict valueForKey:@"description"] UTF8String], -1, NULL);
            sqlite3_bind_int(statement, 4, [[dict valueForKey:@"isActive"] intValue]);
            sqlite3_bind_text(statement, 5, [[dict valueForKey:@"flag"] UTF8String], -1, NULL);
        }
        int kode = sqlite3_step(statement);
        if(kode != SQLITE_DONE)
            NSLog(@"Error ketika menambah category %d", kode);
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    }
}

-(void)deleteCategory:(MCategory *)cat{
    @synchronized([MLDatabase locker])
    {
    sqlite3 *database = [MLDatabase openDatabase];
    
    if(database != nil){
        NSString *query = @"DELETE FROM category WHERE id=?;";
        sqlite3_stmt *statement = nil;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            sqlite3_bind_int(statement, 1, cat.identifier.intValue);
        }
        int kode = sqlite3_step(statement);
        if(kode != SQLITE_DONE)
            NSLog(@"Error ketika menghapus category %d", kode);
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    }
}

-(BOOL)recordIsExistWithID:(int)_id{
    @synchronized([MLDatabase locker])
    {
    sqlite3 *database = [MLDatabase openDatabase];
    
    BOOL exist = NO;
    if(database != nil){
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM category WHERE id=%d LIMIT 1", _id];
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                exist = YES;
            }
        }
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    
    return exist;
    }
}

-(void)loadData:(NSMutableArray *)datas{
    for(NSDictionary *dict in datas){
        int iid = [[dict valueForKey:@"id"] intValue];
        if([self recordIsExistWithID:iid]){
            MCategory *branch = [[[MCategory alloc] init] autorelease];
            branch.identifier = [NSNumber numberWithInt:iid];
            [self deleteCategory:branch];
        }
        [self addCategory:dict];
    }
}

-(NSMutableArray *)categories{
    @synchronized([MLDatabase locker])
    {
    NSMutableArray *data = [NSMutableArray array];
    sqlite3 *database = [MLDatabase openDatabase];
    
    if(database != nil){
        NSString *query = @"SELECT * FROM category WHERE isActive=1 ORDER BY id COLLATE NOCASE ASC";
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                int _id = sqlite3_column_int(statement, 0);
                char *name = (char *)sqlite3_column_text(statement, 1);
                char *desc = (char *)sqlite3_column_text(statement, 2);
                
                MCategory *add = [[MCategory alloc] init];
                add.name = [NSString stringWithUTF8String:name];
                add.identifier = [NSNumber numberWithInt:_id];
                add.description = [NSString stringWithUTF8String:desc];
                [data addObject:add];
                [add release];
            }
        }
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    
    return data;
    }
}

-(MCategory *)categoryWithID:(NSInteger)cid{
    @synchronized([MLDatabase locker])
    {
        MCategory *add = nil;
        sqlite3 *database = [MLDatabase openDatabase];
        
        if(database != nil){
            NSString *query = [NSString stringWithFormat:@"SELECT * FROM category WHERE id=%zd AND isActive=1 ORDER BY id COLLATE NOCASE ASC", cid];
            sqlite3_stmt *statement;
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
                while(sqlite3_step(statement) == SQLITE_ROW){
                    int _id = sqlite3_column_int(statement, 0);
                    char *name = (char *)sqlite3_column_text(statement, 1);
                    char *desc = (char *)sqlite3_column_text(statement, 2);
                    
                    add = [[MCategory alloc] init];
                    add.name = [NSString stringWithUTF8String:name];
                    add.identifier = [NSNumber numberWithInt:_id];
                    add.description = [NSString stringWithUTF8String:desc];
                }
            }
            sqlite3_finalize(statement);
        }
        
        [MLDatabase closeDatabase:database];
        
        return [add autorelease];
    }
}

// class method

+(void)initiateTable{
    @synchronized([MLDatabase locker])
    {
    char *error;
    sqlite3 *database = [MLDatabase openDatabase];
    NSString *createUser = @"CREATE TABLE IF NOT EXISTS category (id INTEGER PRIMARY KEY, name TEXT, description TEXT, isActive INTEGER, flag TEXT);";
    if(sqlite3_exec(database, [createUser UTF8String], NULL, NULL, &error) != SQLITE_OK){
        NSLog(@"Error ketika membuat tabel category %s", error);
    }
    [MLDatabase closeDatabase:database];
    }
}

+(MLCategory *)sharedData{
    if(sharedData == nil){
        sharedData = [[MLCategory alloc] init];
    }
    return sharedData;
}

@end

@implementation MCategory

@synthesize identifier, name, description;

- (void)dealloc
{
    self.identifier = nil;
    self.name = nil;
    self.description = nil;
    [super dealloc];
}

@end
