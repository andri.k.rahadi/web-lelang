//
//  SDPhone.h
//  OneTouch
//
//  Created by asepmoels on 1/2/13.
//  Copyright (c) 2013 Better-B. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@class MProvince;

@interface MLProvince: NSObject
+(void)initiateTable;
+(MLProvince *)sharedData;

-(void)loadData:(NSMutableArray *)datas;
-(NSMutableArray *)provinces;
-(NSNumber *)provIDforName:(NSString *)name;
-(MProvince *)provinceWithID:(NSInteger)pid;

@end

@interface MProvince : NSObject

@property (nonatomic, retain) NSNumber *identifier;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;

@end
