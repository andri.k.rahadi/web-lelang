//
//  MLWebContent.m
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 11/9/16.
//  Copyright © 2016 Asep Mulyana. All rights reserved.
//

#import "MLSettings.h"
#import "MLDatabase.h"

static MLSettings *sharedData = nil;

@implementation MLSettings

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

-(void)addSettings:(NSDictionary *)dict{
    @synchronized([MLDatabase locker])
    {
        sqlite3 *database = [MLDatabase openDatabase];
        
        if(database != nil){
            NSString *query = @"INSERT INTO systemsettings VALUES(?, ?, ?, ?, ?);";
            sqlite3_stmt *statement = nil;
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
                sqlite3_bind_text(statement, 1, [[dict valueForKey:@"id"] UTF8String], -1, NULL);
                sqlite3_bind_text(statement, 2, [[dict valueForKey:@"name"] UTF8String], -1, NULL);
                sqlite3_bind_text(statement, 3, [[dict valueForKey:@"description"] UTF8String], -1, NULL);
                sqlite3_bind_int(statement, 4, [[dict valueForKey:@"isActive"] intValue]);
                sqlite3_bind_text(statement, 5, [[dict valueForKey:@"flag"] UTF8String], -1, NULL);
            }
            int kode = sqlite3_step(statement);
            if(kode != SQLITE_DONE)
                NSLog(@"Error ketika menambah settings %d", kode);
            sqlite3_finalize(statement);
        }
        
        [MLDatabase closeDatabase:database];
    }
}

-(void)deleteSettings:(MSettings *)cat{
    @synchronized([MLDatabase locker])
    {
        sqlite3 *database = [MLDatabase openDatabase];
        
        if(database != nil){
            NSString *query = [NSString stringWithFormat:@"DELETE FROM systemsettings WHERE id=\'%@\';", cat.identifier];
            sqlite3_stmt *statement = nil;
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            }
            int kode = sqlite3_step(statement);
            if(kode != SQLITE_DONE)
                NSLog(@"Error ketika menghapus settings %d", kode);
            sqlite3_finalize(statement);
        }
        
        [MLDatabase closeDatabase:database];
    }
}

-(BOOL)recordIsExistWithID:(NSString *)_id{
    @synchronized([MLDatabase locker])
    {
        sqlite3 *database = [MLDatabase openDatabase];
        
        BOOL exist = NO;
        if(database != nil){
            NSString *query = [NSString stringWithFormat:@"SELECT * FROM systemsettings WHERE id=\'%@\' LIMIT 1", _id];
            NSLog(@"Query hapus %@", query);
            sqlite3_stmt *statement;
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
                while(sqlite3_step(statement) == SQLITE_ROW){
                    exist = YES;
                }
            }
            sqlite3_finalize(statement);
        }
        
        [MLDatabase closeDatabase:database];
        
        return exist;
    }
}

-(void)loadData:(NSMutableArray *)datas{
    for(NSDictionary *dict in datas){
        NSString *iid = [dict valueForKey:@"id"];
        if([self recordIsExistWithID:iid]){
            MSettings *branch = [[[MSettings alloc] init] autorelease];
            branch.identifier = iid;
            [self deleteSettings:branch];
        }
        [self addSettings:dict];
    }
}

-(NSMutableArray *)settings{
    @synchronized([MLDatabase locker])
    {
        NSMutableArray *data = [NSMutableArray array];
        sqlite3 *database = [MLDatabase openDatabase];
        
        if(database != nil){
            NSString *query = @"SELECT * FROM systemsettings WHERE isActive=1 ORDER BY id COLLATE NOCASE ASC";
            sqlite3_stmt *statement;
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
                while(sqlite3_step(statement) == SQLITE_ROW){
                    char *_id = (char *)sqlite3_column_text(statement, 0);
                    char *name = (char *)sqlite3_column_text(statement, 1);
                    char *desc = (char *)sqlite3_column_text(statement, 2);
                    
                    MSettings *add = [[MSettings alloc] init];
                    add.name = [NSString stringWithUTF8String:name];
                    add.identifier = [NSString stringWithUTF8String:_id];
                    add.description = [NSString stringWithUTF8String:desc];
                    [data addObject:add];
                    [add release];
                }
            }
            sqlite3_finalize(statement);
        }
        
        [MLDatabase closeDatabase:database];
        
        return data;
    }
}

-(MSettings *)settingsWithID:(NSString *)_id {
    @synchronized([MLDatabase locker])
    {
        MSettings *result = nil;
        sqlite3 *database = [MLDatabase openDatabase];
        
        if(database != nil){
            NSString *query = [NSString stringWithFormat:@"SELECT * FROM systemsettings WHERE isActive=1 AND id=\'%@\'", _id];
            sqlite3_stmt *statement;
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
                
                while(sqlite3_step(statement) == SQLITE_ROW){
                    char *_id = (char *)sqlite3_column_text(statement, 0);
                    char *name = (char *)sqlite3_column_text(statement, 1);
                    char *desc = (char *)sqlite3_column_text(statement, 2);
                    
                    MSettings *add = [[MSettings alloc] init];
                    add.name = [NSString stringWithUTF8String:name];
                    add.identifier = [NSString stringWithUTF8String:_id];
                    add.description = [NSString stringWithUTF8String:desc];
                    result = add;
                }
            }
            sqlite3_finalize(statement);
        }
        
        [MLDatabase closeDatabase:database];
        
        return [result autorelease];
    }
}

// class method

+(void)initiateTable{
    @synchronized([MLDatabase locker])
    {
        char *error;
        sqlite3 *database = [MLDatabase openDatabase];
        NSString *createUser = @"CREATE TABLE IF NOT EXISTS systemsettings (id TEXT, name TEXT, description TEXT, isActive INTEGER, flag TEXT);";
        if(sqlite3_exec(database, [createUser UTF8String], NULL, NULL, &error) != SQLITE_OK){
            NSLog(@"Error ketika membuat tabel category %s", error);
        }
        [MLDatabase closeDatabase:database];
    }
}

+(MLSettings *)sharedData{
    if(sharedData == nil){
        sharedData = [[MLSettings alloc] init];
    }
    return sharedData;
}

@end


@implementation MSettings

@synthesize identifier, name, description;

- (void)dealloc
{
    self.identifier = nil;
    self.name = nil;
    self.description = nil;
    [super dealloc];
}

@end
