//
//  SDPhone.m
//  OneTouch
//
//  Created by asepmoels on 1/2/13.
//  Copyright (c) 2013 Better-B. All rights reserved.
//

#import "MLCity.h"
#import "MLDatabase.h"

static MLCity *sharedData = nil;

@implementation MLCity

- (id)init
{
    self = [super init];
    if (self) {
        if([self citiesForProvince:0].count < 1){
            [self addCity:@{@"id":@0,
                            @"name":@"Semua",
                            @"description":@"",
                            @"provinceID":@0,
                            @"isActive":@1,
                            @"flag":@"Add"
                            }];
        }
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

-(void)addCity:(NSDictionary *)dict{
    @synchronized([MLDatabase locker])
    {
    sqlite3 *database = [MLDatabase openDatabase];
    
    if(database != nil){
        NSString *query = @"INSERT INTO city VALUES(?, ?, ?, ?, ?, ?);";
        sqlite3_stmt *statement = nil;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            sqlite3_bind_int(statement, 1, [[dict valueForKey:@"id"] intValue]);
            sqlite3_bind_int(statement, 2, [[dict valueForKey:@"provinceID"] intValue]);
            sqlite3_bind_text(statement, 3, [[dict valueForKey:@"name"] UTF8String], -1, NULL);
            sqlite3_bind_text(statement, 4, [[dict valueForKey:@"description"] UTF8String], -1, NULL);
            sqlite3_bind_int(statement, 5, [[dict valueForKey:@"isActive"] intValue]);
            sqlite3_bind_text(statement, 6, [[dict valueForKey:@"flag"] UTF8String], -1, NULL);
        }
        int kode = sqlite3_step(statement);
        if(kode != SQLITE_DONE)
            NSLog(@"Error ketika menambah province %d", kode);
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    }
}

-(void)deleteCity:(MCity *)cat{
    @synchronized([MLDatabase locker])
    {
    sqlite3 *database = [MLDatabase openDatabase];
    
    if(database != nil){
        NSString *query = @"DELETE FROM city WHERE id=?;";
        sqlite3_stmt *statement = nil;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            sqlite3_bind_int(statement, 1, cat.identifier.intValue);
        }
        int kode = sqlite3_step(statement);
        if(kode != SQLITE_DONE)
            NSLog(@"Error ketika menghapus city %d", kode);
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    }
}

-(BOOL)recordIsExistWithID:(int)_id{
    @synchronized([MLDatabase locker])
    {
    sqlite3 *database = [MLDatabase openDatabase];
    
    BOOL exist = NO;
    if(database != nil){
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM city WHERE id=%d LIMIT 1", _id];
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                exist = YES;
            }
        }
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    
    return exist;
    }
}

-(void)loadData:(NSMutableArray *)datas{
    for(NSDictionary *dict in datas){
        int iid = [[dict valueForKey:@"id"] intValue];
        if([self recordIsExistWithID:iid]){
            MCity *branch = [[[MCity alloc] init] autorelease];
            branch.identifier = [NSNumber numberWithInt:iid];
            [self deleteCity:branch];
        }
        [self addCity:dict];
    }
}

-(NSMutableArray *)citiesForProvince:(NSNumber*)provinceID{
    @synchronized([MLDatabase locker])
    {
    NSMutableArray *data = [NSMutableArray array];
    sqlite3 *database = [MLDatabase openDatabase];
    
    MCity *add = [[MCity alloc] init];
    add.name = @"Semua";
    add.identifier = @0;
    add.provinceID = provinceID;
    add.description = @"";
    [data addObject:add];
    
    if(database != nil){
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM city WHERE isActive=1 AND provinceID=%d ORDER BY id COLLATE NOCASE ASC", provinceID.intValue];
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                int _id = sqlite3_column_int(statement, 0);
                int pid = sqlite3_column_int(statement, 1);
                char *name = (char *)sqlite3_column_text(statement, 2);
                char *desc = (char *)sqlite3_column_text(statement, 3);
                
                MCity *add = [[MCity alloc] init];
                add.name = [NSString stringWithUTF8String:name];
                add.identifier = [NSNumber numberWithInt:_id];
                add.provinceID = [NSNumber numberWithInt:pid];
                add.description = [NSString stringWithUTF8String:desc];
                [data addObject:add];
                [add release];
            }
        }
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    
    return data;
    }
}

-(NSMutableArray *)cities{
    @synchronized([MLDatabase locker])
    {
        NSMutableArray *data = [NSMutableArray array];
        sqlite3 *database = [MLDatabase openDatabase];
        
        MCity *add = [[MCity alloc] init];
        add.name = @"Semua";
        add.identifier = @0;
        add.provinceID = @0;
        add.description = @"";
        [data addObject:add];
        
        if(database != nil){
            NSString *query = [NSString stringWithFormat:@"SELECT * FROM city WHERE isActive=1 ORDER BY name COLLATE NOCASE ASC"];
            sqlite3_stmt *statement;
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
                while(sqlite3_step(statement) == SQLITE_ROW){
                    int _id = sqlite3_column_int(statement, 0);
                    int pid = sqlite3_column_int(statement, 1);
                    char *name = (char *)sqlite3_column_text(statement, 2);
                    char *desc = (char *)sqlite3_column_text(statement, 3);
                    
                    MCity *add = [[MCity alloc] init];
                    add.name = [NSString stringWithUTF8String:name];
                    add.identifier = [NSNumber numberWithInt:_id];
                    add.provinceID = [NSNumber numberWithInt:pid];
                    add.description = [NSString stringWithUTF8String:desc];
                    [data addObject:add];
                    [add release];
                }
            }
            sqlite3_finalize(statement);
        }
        
        [MLDatabase closeDatabase:database];
        
        return data;
    }
}

- (MCity *)cityForID:(NSNumber *)name{
    @synchronized([MLDatabase locker])
    {
        sqlite3 *database = [MLDatabase openDatabase];
        
        MCity *add = [[[MCity alloc] init] autorelease];
        
        if(database != nil){
            NSString *query = [NSString stringWithFormat:@"SELECT * FROM city WHERE isActive=1 AND id=%zd", name.integerValue];
            sqlite3_stmt *statement;
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
                while(sqlite3_step(statement) == SQLITE_ROW){
                    int _id = sqlite3_column_int(statement, 0);
                    int pid = sqlite3_column_int(statement, 1);
                    char *name = (char *)sqlite3_column_text(statement, 2);
                    char *desc = (char *)sqlite3_column_text(statement, 3);
                    
                    add.name = [NSString stringWithUTF8String:name];
                    add.identifier = [NSNumber numberWithInt:_id];
                    add.provinceID = [NSNumber numberWithInt:pid];
                    add.description = [NSString stringWithUTF8String:desc];
                }
            }
            sqlite3_finalize(statement);
        }
        
        [MLDatabase closeDatabase:database];
        
        return add;
    }
}

-(NSNumber *)cityIDforName:(NSString *)name{
    @synchronized([MLDatabase locker])
    {
    sqlite3 *database = [MLDatabase openDatabase];
    
    int result = 0;
    if(database != nil){
        NSString *query = @"SELECT * FROM city WHERE isActive=1 AND name LIKE ?";
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            sqlite3_bind_text(statement, 1, [[[NSString stringWithFormat:@"%%%@%%", name] lowercaseString] UTF8String], -1, NULL);
            while(sqlite3_step(statement) == SQLITE_ROW){
                int _id = sqlite3_column_int(statement, 0);
                
                result = _id;
            }
        }
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    
    return [NSNumber numberWithInt:result];
    }
}

// class method

+(void)initiateTable{
    @synchronized([MLDatabase locker])
    {
    char *error;
    sqlite3 *database = [MLDatabase openDatabase];
    NSString *createUser = @"CREATE TABLE IF NOT EXISTS city (id INTEGER PRIMARY KEY, provinceID INTEGER, name TEXT, description TEXT, isActive INTEGER, flag TEXT);";
    if(sqlite3_exec(database, [createUser UTF8String], NULL, NULL, &error) != SQLITE_OK){
        NSLog(@"Error ketika membuat tabel city %s", error);
    }
    [MLDatabase closeDatabase:database];
    }
}

+(MLCity *)sharedData{
    if(sharedData == nil){
        sharedData = [[MLCity alloc] init];
    }
    return sharedData;
}

@end

@implementation MCity

@synthesize identifier, provinceID, name, description;

- (void)dealloc
{
    self.identifier = nil;
    self.name = nil;
    self.description = nil;
    self.provinceID = nil;
    [super dealloc];
}

@end