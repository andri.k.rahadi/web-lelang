//
//  MLWebContent.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 11/9/16.
//  Copyright © 2016 Asep Mulyana. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@class MSettings;

@interface MLSettings : NSObject

+(void)initiateTable;
+(MLSettings *)sharedData;

-(void)loadData:(NSMutableArray *)datas;
-(NSMutableArray *)settings;
-(MSettings *)settingsWithID:(NSString *)_id;

@end


@interface MSettings : NSObject

@property (nonatomic, retain) NSString *identifier;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;

@end