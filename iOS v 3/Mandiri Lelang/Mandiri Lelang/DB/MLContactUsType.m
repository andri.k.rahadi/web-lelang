//
//  SDPhone.m
//  OneTouch
//
//  Created by asepmoels on 1/2/13.
//  Copyright (c) 2013 Better-B. All rights reserved.
//

#import "MLContactUsType.h"
#import "MLDatabase.h"

static MLContactUsType *sharedData = nil;

@implementation MLContactUsType

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

-(void)addContactUsType:(NSDictionary *)dict{
    @synchronized([MLDatabase locker])
    {
    sqlite3 *database = [MLDatabase openDatabase];
    
    if(database != nil){
        NSString *query = @"INSERT INTO contactustype VALUES(?, ?, ?, ?, ?);";
        sqlite3_stmt *statement = nil;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            sqlite3_bind_int(statement, 1, [[dict valueForKey:@"id"] intValue]);
            sqlite3_bind_text(statement, 2, [[dict valueForKey:@"name"] UTF8String], -1, NULL);
            sqlite3_bind_text(statement, 3, [[dict valueForKey:@"description"] UTF8String], -1, NULL);
            sqlite3_bind_int(statement, 4, [[dict valueForKey:@"isActive"] intValue]);
            sqlite3_bind_text(statement, 5, [[dict valueForKey:@"flag"] UTF8String], -1, NULL);
        }
        int kode = sqlite3_step(statement);
        if(kode != SQLITE_DONE)
            NSLog(@"Error ketika menambah contactustype %d", kode);
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    }
}

-(void)deleteContactUs:(MContactUsType *)cat{
    @synchronized([MLDatabase locker])
    {
    sqlite3 *database = [MLDatabase openDatabase];
    
    if(database != nil){
        NSString *query = @"DELETE FROM contactustype WHERE id=?;";
        sqlite3_stmt *statement = nil;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            sqlite3_bind_int(statement, 1, cat.identifier.intValue);
        }
        int kode = sqlite3_step(statement);
        if(kode != SQLITE_DONE)
            NSLog(@"Error ketika menghapus contactustype %d", kode);
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    }
}

-(BOOL)recordIsExistWithID:(int)_id{
    @synchronized([MLDatabase locker])
    {
    sqlite3 *database = [MLDatabase openDatabase];
    
    BOOL exist = NO;
    if(database != nil){
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM contactustype WHERE id=%d LIMIT 1", _id];
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                exist = YES;
            }
        }
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    
    return exist;
    }
}

-(void)loadData:(NSMutableArray *)datas{
    for(NSDictionary *dict in datas){
        int iid = [[dict valueForKey:@"id"] intValue];
        if([self recordIsExistWithID:iid]){
            MContactUsType *branch = [[[MContactUsType alloc] init] autorelease];
            branch.identifier = [NSNumber numberWithInt:iid];
            [self deleteContactUs:branch];
        }
        [self addContactUsType:dict];
    }
}

-(NSMutableArray *)contactUsTypes{
    @synchronized([MLDatabase locker])
    {
    NSMutableArray *data = [NSMutableArray array];
    sqlite3 *database = [MLDatabase openDatabase];
    
    if(database != nil){
        NSString *query = @"SELECT * FROM contactustype WHERE isActive=1 ORDER BY id COLLATE NOCASE ASC";
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                int _id = sqlite3_column_int(statement, 0);
                char *name = (char *)sqlite3_column_text(statement, 1);
                char *desc = (char *)sqlite3_column_text(statement, 2);
                
                MContactUsType *add = [[MContactUsType alloc] init];
                add.name = [NSString stringWithUTF8String:name];
                add.identifier = [NSNumber numberWithInt:_id];
                add.description = [NSString stringWithUTF8String:desc];
                [data addObject:add];
                [add release];
            }
        }
        sqlite3_finalize(statement);
    }
    
    [MLDatabase closeDatabase:database];
    
    return data;
    }
}

// class method

+(void)initiateTable{
    @synchronized([MLDatabase locker])
    {
    char *error;
    sqlite3 *database = [MLDatabase openDatabase];
    NSString *createUser = @"CREATE TABLE IF NOT EXISTS contactustype (id INTEGER PRIMARY KEY, name TEXT, description TEXT, isActive INTEGER, flag TEXT);";
    if(sqlite3_exec(database, [createUser UTF8String], NULL, NULL, &error) != SQLITE_OK){
        NSLog(@"Error ketika membuat tabel contactustype %s", error);
    }
    [MLDatabase closeDatabase:database];
    }
}

+(MLContactUsType *)sharedData{
    if(sharedData == nil){
        sharedData = [[MLContactUsType alloc] init];
    }
    return sharedData;
}

@end

@implementation MContactUsType

@synthesize identifier, name, description;

- (void)dealloc
{
    self.identifier = nil;
    self.name = nil;
    self.description = nil;
    [super dealloc];
}

@end