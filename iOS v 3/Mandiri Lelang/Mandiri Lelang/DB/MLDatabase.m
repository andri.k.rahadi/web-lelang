//
//  SDDatabase.m
//  OneTouch
//
//  Created by asepmoels on 1/2/13.
//  Copyright (c) 2013 Better-B. All rights reserved.
//

#import "MLDatabase.h"

@implementation MLDatabase

static NSObject *databaseLocker = nil;

+(void)initiateDatabase{
    [MLCategory initiateTable];
    [MLProvince initiateTable];
    [MLCity initiateTable];
    [MLPrice initiateTable];
    [MLDocumentType initiateTable];
    [MLContactUsType initiateTable];
    [MLBranch initiateTable];
    [MLWebContent initiateTable];
    [MLSettings initiateTable];
    [MLKnpnl initiateTable];
}

+(NSObject *)locker{
    if(databaseLocker == nil){
        databaseLocker = [[NSObject alloc] init];
    }
    
    return databaseLocker;
}

+(NSString *)dataFilePath{
    NSArray *pahts = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [pahts objectAtIndex:0];
    return [docDir stringByAppendingPathComponent:@"LocalData.sqlite3"];
}

+(sqlite3 *)openDatabase{
    sqlite3 *database;
    if(sqlite3_open([[MLDatabase dataFilePath] UTF8String], &database) != SQLITE_OK){
        NSLog(@"Error open database");
        return nil;
    }
    return database;
}

+(void)closeDatabase:(sqlite3 *)db{
    sqlite3_close(db);
}

@end
