//
//  SDPhone.h
//  OneTouch
//
//  Created by asepmoels on 1/2/13.
//  Copyright (c) 2013 Better-B. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MLPrice: NSObject
+(void)initiateTable;
+(MLPrice *)sharedData;

-(void)loadData:(NSMutableArray *)datas;
-(NSMutableArray *)prices;
-(NSMutableArray *)pricesBeginFromID:(NSNumber *)idp;
@end

@interface MPrice : NSObject

@property (nonatomic, retain) NSNumber *identifier;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSNumber *value;
@property (nonatomic, retain) NSString *description;

@end