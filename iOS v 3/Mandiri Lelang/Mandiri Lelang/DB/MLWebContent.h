//
//  MLWebContent.h
//  Mandiri Lelang
//
//  Created by Asep Mulyana on 11/9/16.
//  Copyright © 2016 Asep Mulyana. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@class MWebContent;

@interface MLWebContent : NSObject

+(void)initiateTable;
+(MLWebContent *)sharedData;

-(void)loadData:(NSMutableArray *)datas;
-(NSMutableArray *)webcontents;
-(MWebContent *)webcontentWithID:(int)_id;

@end


@interface MWebContent : NSObject

@property (nonatomic, retain) NSNumber *identifier;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;

@end